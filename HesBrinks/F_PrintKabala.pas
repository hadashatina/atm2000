unit F_PrintKabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls,Inifiles, QRPrntr;

type
  TFrm_PrintKabala = class(TForm)
    QuickRep1: TQuickRep;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel_AddressLak: TQRLabel;
    QRBand5: TQRBand;
    QRBand6: TQRBand;
    QRLabel9: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRSubDetail1: TQRSubDetail;
    GroupHeaderBand1: TQRBand;
    QRShape36: TQRShape;
    ChildBand1: TQRChildBand;
    QRShape23: TQRShape;
    QRSubDetail2: TQRSubDetail;
    QRShape43: TQRShape;
    GroupFooterBand1: TQRBand;
    QRLabel33: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel40: TQRLabel;
    QRShape38: TQRShape;
    QRShape35: TQRShape;
    QRShape40: TQRShape;
    QRDBText17: TQRDBText;
    QRDBText16: TQRDBText;
    QRShape50: TQRShape;
    QRDBText26: TQRDBText;
    QRShape51: TQRShape;
    QRDBText27: TQRDBText;
    QRShape52: TQRShape;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape37: TQRShape;
    QRShape41: TQRShape;
    QRLabel37: TQRLabel;
    QRDBText31: TQRDBText;
    QRShape2: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRDBText3: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText4: TQRDBText;
    QrLogo: TQRBand;
    ChildBand2: TQRChildBand;
    QRMemo_CompanyDetails: TQRLabel;
    QRLabel_Makor: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel18: TQRLabel;
    QRImage1: TQRImage;
    QRLabel5: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel1: TQRLabel;
    QlbLak: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText2: TQRDBText;
    QRShape1: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QlbHan1: TQRLabel;
    QRLabel14: TQRLabel;
    ChildBand3: TQRChildBand;
    ChildBand4: TQRChildBand;
    QRLabel17: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape16: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRExpr2: TQRExpr;
    QRLabel13: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape8: TQRShape;
    QRShape7: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel4: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRLabel5Print(sender: TObject; var Value: String);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText_CheckNumberPrint(sender: TObject; var Value: String);
    procedure QRLabel8Print(sender: TObject; var Value: String);
    procedure QuickRep1AfterPrint(Sender: TObject);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure QRMemo_CompanyDetailsPrint(sender: TObject;
      var Value: String);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRLabel21Print(sender: TObject; var Value: String);
    procedure QRDBText17Print(sender: TObject; var Value: String);
    procedure QRDBText18Print(sender: TObject; var Value: String);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure GroupHeaderBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText25Print(sender: TObject; var Value: String);
    procedure QRExpr1Print(sender: TObject; var Value: String);
    procedure GroupFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRExpr2Print(sender: TObject; var Value: String);
    procedure QRLabel37Print(sender: TObject; var Value: String);
    procedure QRLabel39Print(sender: TObject; var Value: String);
    procedure QRDBText29Print(sender: TObject; var Value: String);
    procedure QrLogoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText27Print(sender: TObject; var Value: String);
    procedure QRLabel11Print(sender: TObject; var Value: String);
    procedure QlbHan1Print(sender: TObject; var Value: String);
    procedure QRLabel14Print(sender: TObject; var Value: String);
    procedure QlbSik1Print(sender: TObject; var Value: String);
    procedure QRLabel16Print(sender: TObject; var Value: String);
    procedure QlbHan2Print(sender: TObject; var Value: String);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    CompayName:String;
  public
    { Public declarations }
  end;

var
  Frm_PrintKabala: TFrm_PrintKabala;
  Total_sum,Total,
  Achoz1,Achoz2,
  Anach1,Anach2     : Real;
  Kodmatbea         : Integer;
implementation

uses DMKupa, AtmRutin, AtmConst;

{$R *.DFM}

Function  NoMean(St1 : String) : Boolean;
  Var
    Hb: Boolean;
    I: Integer;
  Begin
    Hb:=True;
    for I:=1 to Length(St1) do
      if (St1[I]<>' ') and (St1[I]<>#0) then
        Hb:=False;
    Result:=Hb;
  End;


procedure TFrm_PrintKabala.FormCreate(Sender: TObject);
begin
{  DM_Kupa.Qry_Mname.Open;
  CompayName:=DM_Kupa.Qry_Mname.FieldByName('CompanyName').AsString;
  CompayName:=DoEncryption(CompayName,Mname_Key,MnameDecryptKey,False);
  DM_Kupa.Qry_Mname.Close;
}
end;

procedure TFrm_PrintKabala.QRLabel5Print(sender: TObject;
  var Value: String);
begin
  Value:=CompayName;
end;

Procedure TFrm_PrintKabala.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
Var
Bait    :  Shortstring;
CodeMaslul  :  Integer;
Begin
  Case Dm_Kupa.AtmRxQuery_ZikuyAvKabalaKind.AsInteger OF // Kabala_type Of
    0 : QRLabel18.Caption :='������� ���� �� ����';
    1 : QRLabel18.Caption:='������� ����� �� ����';
    2 : QRLabel18.Caption:='������� �� ����';
  End;

  QRLabel33.Caption:=ShemMadpis;
  Total_sum:=0; Total:=0;
  Anach1:=0 ; Anach2:=0;
  Achoz1:=0 ; Achoz2:=0;  
  With Dm_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select FromNum');
    Sql.Add('From Zikuy');
    Sql.Add('Where KabalaNumber='+Dm_Kupa.AtmRxQuery_KabHesKabalaNumber.AsString);
    Sql.Add('And KabalaYear='+Dm_Kupa.AtmRxQuery_KabHesKabalaYear.AsString);
    Sql.Add('And (FromNum<>0) And (Not FromNum Is null)');
    Sql.Add('Order By Tnua');
    Sql.SaveToFile('C:\Hana.Sql');
    Open;
    If Not Eof And Bof Then
       Achoz1:=FieldByName('FromNum').AsFloat;
  End;
  With Dm_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select ToNum');
    Sql.Add('From Zikuy');
    Sql.Add('Where KabalaNumber='+Dm_Kupa.AtmRxQuery_KabHesKabalaNumber.AsString);
    Sql.Add('And KabalaYear='+Dm_Kupa.AtmRxQuery_KabHesKabalaYear.AsString);
    Sql.Add('And (ToNum<>0) And (Not ToNum Is null)');
    Sql.Add('Order By Tnua');
    Sql.SaveToFile('C:\Hana1.Sql');
    Open;
    If Not Eof And Bof Then
       Achoz2:=FieldByName('ToNum').AsFloat;
  End;

{  If Not Nomean(Dm_Kupa.AtmRxQuery_KabHesFromNum.AsString) Then
    Achoz1:=Dm_Kupa.AtmRxQuery_KabHesFromNum.AsFloat
  Else
    Achoz1:=0;
  If Not (Nomean(Dm_Kupa.AtmRxQuery_KabHesToNum.AsString)) And
         (Dm_Kupa.AtmRxQuery_KabHesToNum.AsInteger<>0)
  Then
    Achoz2:=Dm_Kupa.AtmRxQuery_KabHesToNum.AsFloat
  Else
    Achoz2:=0;
}
  With Dm_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
//  If Kabala_type=10 Then
    If Dm_kupa.AtmRxQuery_ZikuyAvHesbonChiuv.AsString='���' Then
    Begin
      Sql.Add('Select Ktovet_1,Ir_1,Mikud_1 From Nehag');
      Sql.Add('Where Kod_Nehag = '+DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').AsString);
    End
    Else
    Begin
      CodeMaslul:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').AsInteger*10000+
        +DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('MisparHavara').AsInteger;
//    Sql.Add('Select Ktovet_1,Mis_Bait1,Ir_1,Mikud_1 From Lakoach');
//    Sql.Add('Where Kod_Lakoach = '+DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').AsString);
      Sql.Add('Select Motza , Yaad From Maslul');
      Sql.Add('Where Code_Maslul = '+IntToStr(CodeMaslul) );
    End;
    Open;
    QRLabel_AddressLak.Caption:=FieldByName('Motza').AsString+' '+
       FieldByName('Yaad').AsString;
//  If Kabala_type<>10 Then Bait:=FieldByName('Mis_Bait1').AsString
{
    If Dm_kupa.AtmRxQuery_ZikuyAvHesbonChiuv.AsString='����' Then
       Bait:=FieldByName('Mis_Bait1').AsString
    Else
       Bait:='';

    QRLabel_AddressLak.Caption:=FieldByName('Ktovet_1').AsString+' '+
                                Bait+' '+
                                FieldByName('Ir_1').AsString+' '+
                                FieldByName('Mikud_1').AsString+' ';
}
    Close;
  End;
end;

procedure TFrm_PrintKabala.QRDBText6Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
end;

procedure TFrm_PrintKabala.QRDBText_CheckNumberPrint(sender: TObject;
  var Value: String);
begin
  if TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsInteger=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QRLabel8Print(sender: TObject;
  var Value: String);
begin
  if CompareText('����',QRLabel_Makor.Caption)=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QuickRep1AfterPrint(Sender: TObject);
begin
  //DM_Kupa.Qry_Mname.Close;
end;

procedure TFrm_PrintKabala.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRDBText14Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRMemo_CompanyDetailsPrint(sender: TObject;
  var Value: String);
begin
  Value :=DoEncryption(DM_Kupa.Qry_Mname.FieldByName('CompanyDetail').AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
Var
OkFlag    :   boolean;
IniFile   :  Tinifile;
Lak       :  LongInt;

begin
    QlbLak.Caption:=IntToStr(
       Dm_kupa.AtmRxQuery_ZikuyAvCodeLakoach.asinteger*10000+
       Dm_kupa.AtmRxQuery_ZikuyAvMisparHavara.AsInteger);
//  DM_Kupa.Qry_Mname.Open;
//TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString
{    If PrintOsek='�' Then
    Begin
       QRLabel15.Enabled:=False;
       QRLabel16.Enabled:=False;
       QRDBText8.Enabled:=False;
       QRDBText9.Enabled:=False;
    End;
}
    QuickRep1.Page.TopMargin:=TopLines;
    Kodmatbea:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('Kodmatbea').AsInteger;
    if CompareText('����',QRLabel_Makor.Caption)=0 Then OkFlag:=False
    Else OkFlag:=True;

    IniFile := Tinifile.Create (KupaDllScriptsDir+'Op'+DM_Kupa.Database_Kupa.AliasName+'.ini');
    QuickRep1.Page.PaperSize := TQRPaperSize (IniFile.ReadInteger ('Printer','PaperSize',Ord (TQRPaperSize(A4))));
    if QuickRep1.Page.PaperSize = Custom then
    begin
      QuickRep1.Page.Length := IniFile.ReadInteger ('Printer','PaperLength',100);
      QuickRep1.Page.Width := IniFile.ReadInteger ('Printer','PaperWidth',100);
    end;
    IniFile.Free;
end;

procedure TFrm_PrintKabala.QRLabel21Print(sender: TObject;
  var Value: String);
Var
Matbea   :   Integer;
begin
  If Kodmatbea{Kabala_type}=0 Then Matbea:=0 Else Matbea:=2;
  Value:=NumberToHebWords(DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat,True,True,Matbea);
end;

procedure TFrm_PrintKabala.QRDBText17Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText18Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRSubDetail1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.GroupHeaderBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.QRDBText25Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRExpr1Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
      Value:=FormatFloat('0.00',StrToFloat(Value))
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.GroupFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;

end;

procedure TFrm_PrintKabala.QRExpr2Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

procedure TFrm_PrintKabala.QRLabel37Print(sender: TObject;
  var Value: String);
Var
Sum   :   real;

begin
    Sum:=DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ASfloat-
         DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumTashlom').ASfloat;
    Value:=FormatFloat('0.00',(Sum));
    Total_sum:=Total_sum+sum;
end;

procedure TFrm_PrintKabala.QRLabel39Print(sender: TObject;
  var Value: String);
begin
    Value:=FormatFloat('0.00',(Total_Sum));
end;

procedure TFrm_PrintKabala.QRDBText29Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

procedure TFrm_PrintKabala.QrLogoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
   IniFile : Tinifile;
   Filen   : String;
   Prok    : Boolean;
begin
    IniFile := Tinifile.Create (KupaDllScriptsDir+'Op'+DM_Kupa.Database_Kupa.AliasName+'.ini');
    QRImage1.Picture.LoadFromFile (IniFile.ReadString ('Logo','FileName',''));
    Filen:=IniFile.ReadString ('Logo','FileName','');
    If FileN='' Then Prok:=False Else Prok:=True;
    IniFile.Free;
//  PrintBand := QRImage1.Picture <> nil;
    PrintBand :=Prok;
    QrLogo.Height := QRImage1.Height;
//  QRMemo_CompanyDetails.Enabled := Not (QRImage1.Picture <> nil);
    QRMemo_CompanyDetails.Enabled :=Not Prok;

    If Prok=True Then
    Begin
      ChildBand2.Height:=ChildBand2.Tag-QRMemo_CompanyDetails.Height;
//    QRDBText8.top:=QRDBText8.tag-QRMemo_CompanyDetails.Height;
//    QRLabel15.Top:=QRLabel15.Tag-QRMemo_CompanyDetails.Height;
//    QRDBText9.top:=QRDBText9.tag-QRMemo_CompanyDetails.Height;
//    QRLabel16.Top:=QRLabel16.Tag-QRMemo_CompanyDetails.Height;
      QRLabel_Makor.Top:=QRLabel_Makor.Tag-QRMemo_CompanyDetails.Height;
      QRDBText12.top:=QRDBText12.tag-QRMemo_CompanyDetails.Height;
      QRLabel18.Top:=QRLabel18.Tag-QRMemo_CompanyDetails.Height;
    End;
end;

procedure TFrm_PrintKabala.QRDBText27Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat
           +Dm_Kupa.AtmRxQuery_KabHeskamutlemeholel.AsFloat);
     Total:=Total+StrToFloat(Value);
     If Achoz1<>0 Then
       Anach1:=Anach1+Round(
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat *
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Schumhesbonit').AsFloat *
        (Achoz1/100)*100)/100;

     If Achoz2<>0 Then Anach2:=Anach2+Round(
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat *
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Schumhesbonit').AsFloat *
        (1-Achoz1/100)*
        (Achoz2/100)*100)/100;

end;

procedure TFrm_PrintKabala.QRLabel11Print(sender: TObject;
  var Value: String);
begin
    Value:=FormatFloat('0.00',(Total));
end;

procedure TFrm_PrintKabala.QlbHan1Print(sender: TObject;
  var Value: String);
begin
    Value:=FormatFloat('-0.00',(Anach1));
end;

procedure TFrm_PrintKabala.QRLabel14Print(sender: TObject;
  var Value: String);
begin
    Value:='('+FloatToStr(Achoz1)+' %)';
end;

procedure TFrm_PrintKabala.QlbSik1Print(sender: TObject;
  var Value: String);
begin
   Value:=FormatFloat('0.00',(Total-Anach1));
end;

procedure TFrm_PrintKabala.QRLabel16Print(sender: TObject;
  var Value: String);
begin
    Value:='('+FloatToStr(Achoz2)+' %)';
end;

procedure TFrm_PrintKabala.QlbHan2Print(sender: TObject;
  var Value: String);
begin
    Value:=FormatFloat('-0.00',(Anach2));
end;

procedure TFrm_PrintKabala.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
     If Achoz2=0  Then
       PrintBand:=False
     Else
       PrintBand:=True;
end;

end.

library HesZikuy;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ExceptionLog,
  SysUtils,
  Classes,
  AtmRutin in 'K:\TIMCHUR\COMMON\AtmRutin.pas',
  MslDllInterface in 'K:\TIMCHUR\COMMON\MslDllInterface.pas',
  AtmConst in 'K:\TIMCHUR\COMMON\AtmConst.pas',
  F_Kabala in 'F_Kabala.pas' {Frm_Kabala},
  F_PrintKabala in 'F_PrintKabala.pas' {Frm_PrintKabala},
  DMKupa in 'DMKupa.pas' {DM_Kupa: TDataModule};

{$R *.RES}
{
Procedure Hafkada(AUserName,APassword,AAlias,Kabalatype:PChar);
Begin
  InitHafkadaForms(AUserName,APassword,AAlias,KabalaType);
  DoneHafkadaForms;
//Kabala_type:=2 ����
//0 = ���
End;

}

Procedure InitKupaDll(IniFileName,ScriptsDir :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);//(c:\Program files\atm\atm2000\AtmCrt.ini) �� ���� ������
  KupaDllScriptsDir:=StrPas(ScriptsDir);
End;

Procedure InitKupaDllFromMlay(IniFileName,ScriptsDir,HesNum,YearNum :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);
  KupaDllScriptsDir:=StrPas(ScriptsDir);
  HesFromMlay:=StrToint(StrPas(HesNum));
  YearFromMlay:=StrToint(StrPas(YearNum));
End;

Procedure InitKupaDllFromAtmCrt(IniFileName,ScriptsDir,HesNum,YearNum :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);
  KupaDllScriptsDir:=StrPas(ScriptsDir);
  HesFromCrt:=StrToint(StrPas(HesNum));
  YearFromCrt:=StrToint(StrPas(YearNum));
End;

Procedure InitKupaDllFromEska(IniFileName,ScriptsDir,HesNum,YearNum :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);
  KupaDllScriptsDir:=StrPas(ScriptsDir);
  HesFromEska:=StrToint(StrPas(HesNum));
  YearFromEska:=StrToint(StrPas(YearNum));
End;


Exports
  InitKupaDll,
  DoKabala,
  InitKupaDllFromAtmCrt,
  InitKupaDllFromMlay,
  InitKupaDllFromEska;

begin
  KupaDllIniFileName:='';
  KupaDllScriptsDir:='';
  HesFromCrt:=0;
  YearFromCrt:=0;
  HesFromMlay:=0;
  YearFromMlay:=0;
  HesFromEska:=0;
  YearFromEska:=0;
end.

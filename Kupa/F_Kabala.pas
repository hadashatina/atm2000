unit F_Kabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, ToolWin, StdCtrls, Mask, DBCtrls, AtmComp, AdvSearch,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, Menus, Grids, DBGrids, ActnList, DB,
  ImgList, Buttons, DBTables, Scale250, DBFilter, RxQuery,
  AtmTabSheetBuild,inifiles;

type
  TFrm_Kabala = class(TForm)
    PageControl1: TPageControl;
    ToolBar1: TToolBar;
    TS_Main: TTabSheet;
    Panel_Kabala: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    AtmDbHEdit_KabalaNo: TAtmDbHEdit;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    AtmDBDateEdit1: TAtmDBDateEdit;
    Label2: TLabel;
    Label_CodeLakoach: TLabel;
    KabCodeLakoach: TAtmDbHEdit;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmAdvSearch_lakoach: TAtmAdvSearch;
    Lbl_zkut: TLabel;
    ChebZikuy: TAtmDbHEdit;
    Lbl_Chiuv: TLabel;
    DBComboBox_HeshbonHiuv: TDBComboBox;
    Lbl_masmakor: TLabel;
    ChenMasMakor: TAtmDbHEdit;
    Lbl_kodbitzua: TLabel;
    KodBitzou: TAtmDbHEdit;
    KabPratim: TAtmDbHEdit;
    Label4: TLabel;
    RxDBGrid_Checks: TRxDBGrid;
    DBNavigator1: TDBNavigator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ActionList1: TActionList;
    Action_duplicateRecord: TAction;
    Action_SumTotals: TAction;
    Action_OpenSearch: TAction;
    Action_SelectNextControl: TAction;
    Action_SelectPrevControl: TAction;
    AtmAdvSearch_KodBank: TAtmAdvSearch;
    AtmAdvSearch_SugTashlum: TAtmAdvSearch;
    N2: TMenuItem;
    Panel2: TPanel;
    Splitter1: TSplitter;
    BitBtn1: TBitBtn;
    Act_ShowOpenHeshbonitForLak: TAction;
    Panel3: TPanel;
    RxDBGrid_KabHes: TRxDBGrid;
    Spb_CloseCurHeshbonit: TSpeedButton;
    Act_SelectOneHeshbonitForClose: TAction;
    SpeedButton1: TSpeedButton;
    Act_CloseHeshbonitSequence: TAction;
    Panel_HeshForLak: TPanel;
    Btn_Edkun: TBitBtn;
    Panel5: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Edit_ItratLakoach: TEdit;
    DBEdit_TotalKabala: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    AtmAdvSearch_BankForHafkada: TAtmAdvSearch;
    Act_PrintKabala: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Act_NewKabala: TAction;
    ToolButton3: TToolButton;
    N7: TMenuItem;
    N21: TMenuItem;
    Act_ChangeYehusYear: TAction;
    N8: TMenuItem;
    BitBtn2: TBitBtn;
    Scale1: TScale;
    Act_UpdateFiles: TAction;
    ToolButton4: TToolButton;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    DS_TSB: TDataSource;
    RxQuery_TSB: TRxQuery;
    RxDBFilter_TSB: TRxDBFilter;
    Spb_ShowOpenHeshbonitForLakGroup: TSpeedButton;
    Act_CloseOpenHeshboniot: TAction;
    BitBtn3: TBitBtn;
    Act_SelectOnlyOpenKabalot: TAction;
    ActSelectOnlyOpenKabalot1: TMenuItem;
    AtmTabSheetBuild_HeshForLak: TAtmTabSheetBuild;
    RxDBFilter1: TRxDBFilter;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    N9: TMenuItem;
    AtmAdvSearch_Remarks: TAtmAdvSearch;
    N10: TMenuItem;
    N11: TMenuItem;
    AtmAdvSearch_Sapak: TAtmAdvSearch;
    procedure Action_OpenSearchExecute(Sender: TObject);
    procedure AtmAdvSearch_KodBankAfterExecute(Sender: TObject);
    procedure Action_OpenSearchUpdate(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Act_ShowOpenHeshbonitForLakExecute(Sender: TObject);
    procedure Panel_KabalaExit(Sender: TObject);
    procedure Panel_KabalaEnter(Sender: TObject);
    procedure RxDBGrid_ChecksEnter(Sender: TObject);
    procedure RxDBGrid_OpenHeshbonitEnter(Sender: TObject);
    procedure AtmAdvSearch_SugTashlumAfterExecute(Sender: TObject);
    procedure Act_SelectOneHeshbonitForCloseExecute(Sender: TObject);
    procedure Act_CloseHeshbonitSequenceExecute(Sender: TObject);
    procedure RxDBGrid_KabHesEnter(Sender: TObject);
    procedure Edit_ItratLakoachChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AtmAdvSearch_BankForHafkadaAfterExecute(Sender: TObject);
    procedure RxDBGrid_ChecksExit(Sender: TObject);
    procedure RxDBGrid_KabHesExit(Sender: TObject);
    procedure Act_PrintKabalaExecute(Sender: TObject);
    procedure DBNavigator1BeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure KodBitzouChange(Sender: TObject);
    procedure Label_CodeLakoachDblClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure Act_NewKabalaExecute(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure Act_ChangeYehusYearExecute(Sender: TObject);
    procedure RxDBGrid_OpenHeshbonitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RxDBGrid_OpenHeshbonitExit(Sender: TObject);
    procedure Act_UpdateFilesExecute(Sender: TObject);
    procedure Action_duplicateRecordUpdate(Sender: TObject);
    procedure Action_duplicateRecordExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RxDBGrid_KabHesGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure Act_CloseOpenHeshboniotExecute(Sender: TObject);
    procedure Act_SelectOnlyOpenKabalotExecute(Sender: TObject);
    procedure RxDBGrid_ChecksKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure KabCodeLakoachBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure RxDBGrid_ChecksColEnter(Sender: TObject);
    procedure AtmTabSheetBuild1GridDblClick(Sender: TObject;
      ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
  private
    { Private declarations }
    Procedure FillComboHeshbonHiuv;
  public
    { Public declarations }
    Procedure InitKabala;
    Procedure UpdateAllFiles(KodBitzua:LongInt);
    Procedure DoUpdateInFiles;
    Procedure PrintCurKabala;

  end;

var
  Frm_Kabala: TFrm_Kabala;
//  Hadpasa   : String[1];

implementation

uses DMKupa, AtmConst, AtmRutin, F_PrintKabala;

{$R *.DFM}

Procedure TFrm_Kabala.FillComboHeshbonHiuv;
Begin
  DBComboBox_HeshbonHiuv.Items.Clear;
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_HeshbonLehiuv));
    Open;
    First;
    While Not Eof Do
    Begin
      DBComboBox_HeshbonHiuv.Items.Add(FieldByName('Teur_Tavla').AsString);
      Next;
    End;
    Close;
  End;
End;

procedure TFrm_Kabala.Action_OpenSearchExecute(Sender: TObject);
Var
   Bool :Boolean;
Begin
     if ActiveControl is TAtmDbHEdit Then
       (ActiveControl As TAtmDbHEdit).ExecSearch
     Else
      if ActiveControl=RxDBGrid_Checks Then
      Begin
        if RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank') Then
          AtmAdvSearch_KodBank.Execute;
        if RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum') Then
          AtmAdvSearch_SugTashlum.Execute;
        if (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeBankHafkada')) Or
           (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSnifHafkada')) Or
           (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('MisHeshbonHafkada')) Then
                AtmAdvSearch_BankForHafkada.Execute;
      End;
end;

procedure TFrm_Kabala.AtmAdvSearch_KodBankAfterExecute(Sender: TObject);
begin
  if DM_Kupa.AtmRxQuery_Checks.State=dsBrowse Then
    DM_Kupa.AtmRxQuery_Checks.Edit;
  DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank').AsString:=AtmAdvSearch_KodBank.ReturnString;
end;

procedure TFrm_Kabala.Action_OpenSearchUpdate(Sender: TObject);
Begin
  Try
     if ActiveControl is TAtmDbHEdit Then
        (Sender As TAction).Enabled:=(ActiveControl As TAtmDbHEdit).EnableSearch
     Else
      if ActiveControl=RxDBGrid_Checks Then
      Begin
        (Sender As TAction).Enabled:=(RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank')) Or
                                     (RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum')) Or
                                     (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeBankHafkada')) Or
                                     (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSnifHafkada')) Or
                                     (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('MisHeshbonHafkada'));
      End
      Else
         (Sender As TAction).Enabled:=False;
  Except On Exception Do;
  End;
end;

Procedure TFrm_Kabala.InitKabala;
Var
  yy,mm,dd :Word;
  HStr     :shortstring;
Begin
  DecodeDate(Now,YY,MM,DD);
  CurKabalaYear:=YY;
  DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Kabala.Open;
  If Kabala_type MOD 10=0 Then HStr:='������ ' Else HStr:='����� ';
  Caption:='����� '+HStr+IntToStr(CurKabalaYear);
  If Kabala_type=10 Then
  Begin
    If TypeOfFile='���' Then
      Label_CodeLakoach.Caption:='<���>'
    Else
      Label_CodeLakoach.Caption:='<���>'
  End
  Else
    Label_CodeLakoach.Caption:='<����>';
End;

procedure TFrm_Kabala.N2Click(Sender: TObject);
begin
  Close;
end;

procedure TFrm_Kabala.Act_ShowOpenHeshbonitForLakExecute(Sender: TObject);
begin
  If TypeOfFile<>'���' Then
  Begin
    DM_Kupa.ShowOpenHeshbonitForLak(DM_Kupa.AtmRxQuery_KabalaCodeLakoach.AsInteger,Spb_ShowOpenHeshbonitForLakGroup.Down);
    DM_Kupa.ItraForLak:=DM_Kupa.CalcItratLakoach(DM_Kupa.AtmRxQuery_KabalaCodeLakoach.AsString);
    Edit_ItratLakoach.Text:=FloatToStr(DM_Kupa.ItraForLak);
  End;  
end;

procedure TFrm_Kabala.Panel_KabalaExit(Sender: TObject);
Var
  NewKabala :Boolean;
begin

  NewKabala:=DM_Kupa.AtmRxQuery_Kabala.State=dsInsert;
  if DM_Kupa.AtmRxQuery_Kabala.State in [dsInsert,dsEdit] Then
    DM_Kupa.AtmRxQuery_Kabala.Post;
  if NewKabala Then
    DM_Kupa.AtmRxQuery_Checks.Append;
end;

procedure TFrm_Kabala.Panel_KabalaEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_Kabala;
end;

procedure TFrm_Kabala.RxDBGrid_ChecksEnter(Sender: TObject);
begin
  If Dm_kupa.AtmRxQuery_KabalaCodeLakoach.IsNull Then
  Begin
     Showmessage('��� ��� ����');
     Dm_kupa.AtmRxQuery_KabalaCodeLakoach.FocusControl;
     Abort;
  End;

  DBNavigator1.DataSource:=DM_Kupa.DS_Checks;
  RxDBGrid_Checks.Col:=1;
  StatusBar1.SimpleText:='F6 - ����� ����� ���';
end;

procedure TFrm_Kabala.RxDBGrid_OpenHeshbonitEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_HeshForLak;
  StatusBar1.SimpleText:='F6 - ����� ����� ���, Enter - ������';
end;

procedure TFrm_Kabala.AtmAdvSearch_SugTashlumAfterExecute(Sender: TObject);
begin
  if AtmAdvSearch_SugTashlum.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Checks.State=dsBrowse Then
      DM_Kupa.AtmRxQuery_Checks.Edit;
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString:=AtmAdvSearch_SugTashlum.ReturnString;
  End;
end;



procedure TFrm_Kabala.Act_SelectOneHeshbonitForCloseExecute(
  Sender: TObject);
begin
  With DM_Kupa Do
  Begin
    if AtmRxQuery_Checks.State in [dsEdit,dsInsert] Then
      AtmRxQuery_Checks.Post;
    if HesbonitInKabHes(AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger,
                        AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger,
                        RxQuery_HeshForLak.FieldByName('HesbonitNumber').AsInteger,
                        RxQuery_HeshForLak.FieldByName('YehusYear').AsInteger,
                        RxQuery_HeshForLak.FieldByName('HesbonitKind').AsInteger) Then Exit;


    With AtmRxQuery_KabHes Do
    Begin
      Append;
      FieldByName('KabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').Asinteger;
      FieldByName('DateKabala').AsDateTime:=AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime;
      FieldByName('CodeLakoach').AsInteger:=AtmRxQuery_Kabala.FieldByName('CodeLakoach').Asinteger;
      FieldByName('SchumHesbonit').AsFloat:=RxQuery_HeshForLak.FieldByName('TotalHesbonitWithMAM').AsFloat;
      FieldByName('DateHesbonit').AsDateTime:=RxQuery_HeshForLak.FieldByName('HesbonitDate').AsDateTime;
      FieldByName('ShemLakoach').AsString:=AtmRxQuery_Kabala.FieldByName('ShemLakoach').AsString;
      FieldByName('HesbonitNumber').AsInteger:=RxQuery_HeshForLak.FieldByName('HesbonitNumber').Asinteger;
      FieldByName('SchumTashlom').AsFloat:=RxQuery_HeshForLak.FieldByName('Itra').AsFloat;
      FieldByName('HesbonitYear').AsInteger:=RxQuery_HeshForLak.FieldByName('YehusYear').AsInteger;
      FieldByName('HesbonitKind').AsInteger:=RxQuery_HeshForLak.FieldByName('HesbonitKind').AsInteger;
      FieldByName('CodeBitzua').AsInteger:=0;
      Post;
    End;//With AtmRxQuery_KabHes
  End; // With
end;

procedure TFrm_Kabala.Act_CloseHeshbonitSequenceExecute(Sender: TObject);
begin
  With DM_Kupa Do
  Begin
    RxQuery_HeshForLak.First;
    While Not RxQuery_HeshForLak.Eof Do
    Begin
      Act_SelectOneHeshbonitForCloseExecute(Nil);
      RxQuery_HeshForLak.Next;
    End;
  End;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_KabHes;
  if DM_Kupa.KabHesWasInsert Then
  Begin
    if DM_Kupa.AtmRxQuery_KabHes.State in [dsEdit,dsInsert] Then
      DM_Kupa.AtmRxQuery_KabHes.Post;
    DM_Kupa.AtmRxQuery_KabHes.Close;
    DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
    DM_Kupa.AtmRxQuery_KabHes.Open;
    DM_Kupa.KabHesWasInsert:=False;
  End;
end;

procedure TFrm_Kabala.Edit_ItratLakoachChange(Sender: TObject);
begin
  if DM_Kupa.ItraForLak >= 0 Then
    Edit_ItratLakoach.Font.Color:=clBlue
  Else
    Edit_ItratLakoach.Font.Color:=clRed;
end;

procedure TFrm_Kabala.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyboardManageForTableAction(Self,Key,Shift,DBNavigator1.DataSource.DataSet);
  if Key=VK_F6 Then
    SelectNext(ActiveControl,Not (ssShift in Shift),True);
  if Key=VK_F1 Then
    Act_CloseHeshbonitSequenceExecute(Nil);
End;

Procedure TFrm_Kabala.UpdateAllFiles(KodBitzua:LongInt);
Begin
  if DM_Kupa.AtmRxQuery_Checks.State in [dsEdit,dsInsert] Then
    DM_Kupa.AtmRxQuery_Checks.Post;
  DM_Kupa.Query_InsertKupa.Open;
  DM_Kupa.Database_Kupa.StartTransaction;
  Try
    DoUpdateInFiles;
  Except On E:Exception Do
    Begin
      ShowMessage(e.Message);
      DM_Kupa.Database_Kupa.Rollback;
      DM_Kupa.Query_InsertKupa.Close;
      Raise;
    End;
  End;
  DM_Kupa.Query_InsertKupa.ApplyUpdates;
  DM_Kupa.Database_Kupa.Commit;
  DM_Kupa.Query_InsertKupa.Close;
  //����� ��� ����� 2
  DM_Kupa.AtmRxQuery_Kabala.Edit;
  DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger:=KodBitzua;
  DM_Kupa.AtmRxQuery_Kabala.SavePressed:=True;
  DM_Kupa.AtmRxQuery_Kabala.Post;
  DM_Kupa.AtmRxQuery_Checks.Database.ApplyUpdates([DM_Kupa.AtmRxQuery_Checks]);
End;

Procedure TFrm_Kabala.DoUpdateInFiles;
Begin
  if DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then
    Exit;
  Screen.Cursor:=crSQLWait;
  Try
    If TypeOfFile<>'���' Then
    Begin
      DM_Kupa.BuildLineInMakavForCurKabala;
      DM_Kupa.UpdateMakavForCloseHesbonit;
      DM_Kupa.BuildLinesInKopaForCurChecks;
    End;
  Finally
    Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_Kabala.AtmAdvSearch_BankForHafkadaAfterExecute(
  Sender: TObject);
Var
  StrH :String;
begin
  if AtmAdvSearch_BankForHafkada.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Checks.State = dsBrowse Then
      DM_Kupa.AtmRxQuery_Checks.Edit;

    StrH:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,2);//Teur_Tavla
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeBankHafkada').AsString:=GetFieldFromSeparateString(StrH,',',1);
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSnifHafkada').AsString:=GetFieldFromSeparateString(StrH,',',2);
    DM_Kupa.AtmRxQuery_Checks.FieldByName('MisHeshbonHafkada').AsString:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,1);//Teur_Tavla

    if DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger=stHavaraBank Then
    Begin
      StrH:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,3); //���� ���"� �� ��� ������
      if (Trim(StrH)<>'') And (CompareText(DM_Kupa.AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString,StrH)<>0) Then
        if MessageDlg('����� ����� ���� ����� ���"� �� ���� ������. ��� ����',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
        Begin
          if DM_Kupa.AtmRxQuery_Kabala.State = dsBrowse Then
            DM_Kupa.AtmRxQuery_Kabala.Edit;
          DM_Kupa.AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString:=StrH;
        End;
    End;

  End;
end;

procedure TFrm_Kabala.RxDBGrid_ChecksExit(Sender: TObject);
begin
  if TCustomDbGrid(Sender).DataSource.DataSet.State in [dsInsert,dsEdit] Then
    TCustomDbGrid(Sender).DataSource.DataSet.Post;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesExit(Sender: TObject);
begin
  if TCustomDbGrid(Sender).DataSource.DataSet.State in [dsInsert,dsEdit] Then
    TCustomDbGrid(Sender).DataSource.DataSet.Post;
end;

Procedure TFrm_Kabala.PrintCurKabala;
Var
  StrH :String;
  NumOfCopy,Code :LongInt;
Begin
(*
  StrH :=InputBox('����� ����','���� ������','2');
  Val(StrH,NumOfCopy,Code);
  if Code<>0 Then Exit;
  Frm_kabprt:=Tfrm_kabprt.Create(Nil);
  Try
//    If Frm_copy.showModal = mrOk Then
    Begin
     With DM_Kupa Do
     Begin
{      Frm_kabprt.QRLabel_CompayName.Caption:=ComName;
      Frm_kabprt.QrLbl_CompNum.Caption:=ComNum;
      Frm_kabprt.Qrlbl_OsekMorshe.Caption := OsekMorske;
      Frm_kabprt.QR_title.Caption:=Detail;}
      If (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Or
         (AtmRxQuery_Kabala.FieldByName('KodBItzua').IsNull) Or
         (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=2)
      Then
      Begin
        Frm_kabprt.QRLabel4.Caption:='����';
        Print_Kabala;
        Frm_kabprt.QRLabel4.Caption:='����';
      End
      Else
         If Tbl_KabalaKodBItzua.AsInteger=9
      Then
         Frm_kabprt.QRLabel4.Caption:='�����'
      Else
         Frm_kabprt.QRLabel4.Caption:='����';
    // ����� ������
       For Ix1:=1 To NumOfCopy Do
          Frm_kabprt.QuickRep1.Preview; {Print}{preview;}
       If (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Or
          (AtmRxQuery_Kabala.FieldByName('KodBItzua').IsNull) Then
             UpdateAllFiles(cbkPrintSource);
     End; {With Frm_DbKab}
    End;      //mrOk
  Finally
    Frm_kabprt.Free;
    Frm_kabprt:=Nil;
  End;

*)
End;

procedure TFrm_Kabala.Act_PrintKabalaExecute(Sender: TObject);
Var
  StrH :String;
  NumOfCopy,Code :LongInt;
Begin
  If (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=0)
     And
     (DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger<>9)
  Then
  Begin
     Showmessage('���� ��� ���� �� ����');
     Exit;
  End;
  Repeat
    StrH:=CopysKabala ; //'1';
    if Not InputQuery('����� ����','���� ������',StrH) Then
      Exit;    Val(StrH,NumOfCopy,Code);
  Until Code=0;
  DM_Kupa.AtmRxQuery_KabalaAfterScroll(Nil); //refresh all qreries
  Frm_PrintKabala:=TFrm_PrintKabala.Create(Nil);
  Try
    Case DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger Of
      0:
        Begin
          StatusBar1.SimpleText:='����� �����...';
          Frm_PrintKabala.QRLabel_Makor.Caption:='����';
          UpdateAllFiles(cbkPrintSource);
          NumOfCopy:=NumOfCopy+1;
        End;
      1 : Frm_PrintKabala.QRLabel_Makor.Caption:='����';
      2 :    //����� ��� ����� 2 �-1
        Begin
          Frm_PrintKabala.QRLabel_Makor.Caption:='����';
          DM_Kupa.AtmRxQuery_Kabala.Edit;
          DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger:=1;
          DM_Kupa.AtmRxQuery_Kabala.SavePressed:=True;
          DM_Kupa.AtmRxQuery_Kabala.Post;
        End;
      9 : Frm_PrintKabala.QRLabel_Makor.Caption:='�����';
    End;

    For Code:=1 To NumOfCopy Do
    Begin
      DM_Kupa.AtmRxQuery_Checks.First;
      Kodmatbea:=DM_Kupa.AtmRxQuery_Checks.FieldByName('KodMatbea').AsInteger;
// David Preview  <> Print
      Frm_PrintKabala.QuickRep1.Print; //Preview; // David
      If DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=9
      Then
        Frm_PrintKabala.QRLabel_Makor.Caption:='�����'
      Else
        Frm_PrintKabala.QRLabel_Makor.Caption:='����';
    End;
    DM_Kupa.AtmRxQuery_Kabala.Append;
  Finally
    Frm_PrintKabala.Release; //  Free;
    Frm_PrintKabala:=Nil;
    StatusBar1.SimpleText:='';
  End;
end;

procedure TFrm_Kabala.DBNavigator1BeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
// TabSheetBuild -����� ��� �� ����� ������ ��
    If DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger<>
       CurKabalaYear Then
    Begin
      DM_Kupa.AtmRxQuery_Kabala.Close;
      DM_Kupa.AtmRxQuery_Checks.Close;
      DM_Kupa.AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=CurKabalaYear;
      DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
      DM_Kupa.AtmRxQuery_Kabala.Open;
    End;

  Case Button Of
    nbRefresh:Begin
                if DBNavigator1.DataSource.DataSet is TQuery Then
                Begin
                  TQuery(DBNavigator1.DataSource.DataSet).Close;
                  if TQuery(DBNavigator1.DataSource.DataSet).Params.FindParam('PKabalaNumber')<>Nil Then
                    TQuery(DBNavigator1.DataSource.DataSet).ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
                  if TQuery(DBNavigator1.DataSource.DataSet).Params.FindParam('PKabalaYear')<>Nil Then
                    TQuery(DBNavigator1.DataSource.DataSet).ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
                  TQuery(DBNavigator1.DataSource.DataSet).Open;
                  Abort;
                End;
              End;
      Else
        If Dm_Kupa.NewKabalaSavedNumber=-1 Then
          Frm_Kabala.DBNavigator1.DataSource.DataSet.Cancel;
  End; //Case;
end;

procedure TFrm_Kabala.KodBitzouChange(Sender: TObject);
begin
  Act_UpdateFiles.Enabled:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0;
  Act_CloseOpenHeshboniot.Enabled:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger in [1,2];
end;

procedure TFrm_Kabala.Label_CodeLakoachDblClick(Sender: TObject);
begin
  If Kabala_type=10 Then
  Begin
    If TypeOfFile='���' Then
       OpenCrt(fnNehag,KabCodeLakoach.Text)
    Else
//     OpenCrt(fnSapak,KabCodeLakoach.Text)
  End
  Else
  Begin
    If Atm='Y' Then
      OpenCrt(fnLakoachAtm,KabCodeLakoach.Text)
    Else
      OpenCrt(fnLakoach,KabCodeLakoach.Text);
  End;
end;

procedure TFrm_Kabala.N4Click(Sender: TObject);
begin
  OpenCrt(fnMakav,'');
end;

procedure TFrm_Kabala.N5Click(Sender: TObject);
begin
    If Atm='Y' Then
      OpenCrt(fnLakoachAtm,KabCodeLakoach.Text)
    Else
      OpenCrt(fnLakoach,'');
end;

procedure TFrm_Kabala.N6Click(Sender: TObject);
begin
  OpenCrt(fnNehag,'');
end;

 Procedure TFrm_Kabala.Act_NewKabalaExecute(Sender: TObject);
Begin
  If (DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0)
     And (DM_Kupa.AtmRxQuery_KabalaTotalSumKabla.AsFloat<>0)
  Then
  Begin
    Showmessage('!! ����� �� ������ ');
    Abort;
  End;

  DM_Kupa.AtmRxQuery_Kabala.Append;
end;

procedure TFrm_Kabala.N7Click(Sender: TObject);
begin
  OpenCrt(fnTavla,'');
end;

procedure TFrm_Kabala.N21Click(Sender: TObject);
begin
  OpenCrt(fnTavla2,'');
end;

procedure TFrm_Kabala.Act_ChangeYehusYearExecute(Sender: TObject);
Var
  StrH,HStr :String;
  Code      :longInt;
begin
  Repeat
    StrH:=IntToStr(CurKabalaYear);
    if Not InputQuery('����� ��� ����� �����','��� �����',StrH) Then
      Exit;
    Val(StrH,CurKabalaYear,Code);
  Until Code=0;

  DM_Kupa.AtmRxQuery_Kabala.Close;
  DM_Kupa.AtmRxQuery_Checks.Close;
  DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Kabala.Open;
  If Kabala_type MOD 10=0 Then HStr:='������ ' Else HStr:='����� ';
  Caption:='����� '+HStr+IntToStr(CurKabalaYear);
  DM_Kupa.GetLastKabalaNumber;
end;

procedure TFrm_Kabala.RxDBGrid_OpenHeshbonitKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_RETURN Then
    Act_SelectOneHeshbonitForCloseExecute(Nil);
end;

procedure TFrm_Kabala.RxDBGrid_OpenHeshbonitExit(Sender: TObject);
begin
  StatusBar1.SimpleText:='';
end;

procedure TFrm_Kabala.Act_UpdateFilesExecute(Sender: TObject);
begin
  If (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=0)
     And
     (DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger<>9)
  Then
  Begin
     Showmessage('���� ��� ���� �� ����');
     Exit;
  End;
  DM_Kupa.AtmRxQuery_KabalaAfterScroll(Nil); //refresh all qreries
  UpdateAllFiles(cbkUpdateOnly);
  DM_Kupa.AtmRxQuery_Kabala.Append;
end;

procedure TFrm_Kabala.Action_duplicateRecordUpdate(Sender: TObject);
Begin
  Action_duplicateRecord.Enabled:=ActiveControl=RxDBGrid_Checks;
end;

procedure TFrm_Kabala.Action_duplicateRecordExecute(Sender: TObject);
begin
  if (Action_duplicateRecord.Enabled) And (DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Then
  Begin
    DuplicateOneRecord(DM_Kupa.AtmRxQuery_Checks,False,True);
    RxDBGrid_Checks.Fields[4].asstring := IntTostr(RxDBGrid_Checks.Fields[4].Asinteger+1);
    RxDBGrid_Checks.Fields[3].asDateTime := IncMonth(RxDBGrid_Checks.Fields[3].AsDateTime,1);
  End
end;

Procedure TFrm_Kabala.FormCreate(Sender: TObject);
Var
//Atmini   : Tinifile;
  CurYear  : Word;
  Res,Code : LongInt;
  StrH     : String;
  yy,mm,dd : Word;

begin
Application.BiDiMode := bdRightToLeft;
{
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  Hadpasa:=Atmini.readString('Hafkada','FormAzor','N');
  Atmini.WriteString('Hafkada','FormAzor',Hadpasa);
  Atmini.free;
}
  AtmTabSheetBuild1.ScrFileName:=KupaDllScriptsDir+AtmTabSheetBuild1.ScrFileName;
  AtmTabSheetBuild1.SqlFileName:=KupaDllScriptsDir+AtmTabSheetBuild1.SqlFileName;
  AtmTabSheetBuild_HeshForLak.ScriptsDir:=KupaDllScriptsDir;
  AtmTabSheetBuild1.BuildTabsForGrids;
  FillComboHeshbonHiuv;
  Strh:=ExtractFilePath(Application.ExeName)+'Frm_GlbKabala.Ini';
  AtmAdvSearch_lakoach.IniFileName:=Strh;
  AtmAdvSearch_Nehag.IniFileName:=Strh;
  AtmAdvSearch_KodBank.IniFileName:=Strh;
  AtmAdvSearch_BankForHafkada.IniFileName:=Strh;
  AtmAdvSearch_SugTashlum.IniFileName:=Strh;
  AtmAdvSearch_Remarks.IniFileName:=Strh;
  DecodeDate(Now,YY,MM,DD);
  CurKabalaYear:=YY;
//DM_Kupa.GetNextKabalaNumber(+0);
//Result:=0;
  CurYear:=CurKabalaYear;
  With Dm_kupa.Qry_AtmIndexKabala Do
  Begin
    ParamByName('PYehusYear').AsInteger:=CurYear;
    Open;
    if Eof And Bof Then //��� ����
    Begin
    StrH:='1';
    Repeat
      if Not InputQuery('��� ���� ','������ ����� ���� ',StrH) Then
        Exit;     Val(StrH,Res,Code);
    Until Code=0;
      Insert;
      FieldByName('Last_Number').AsInteger:=Res-1; // 1
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=aiKabala;
      FieldByName('YehusYear').AsInteger:=CurYear;
      Post;
    End
  End;
end;

procedure TFrm_Kabala.AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
begin

  if TQuery(AtmTabSheetBuild1.Query).Params.FindParam('PLakNum')<>Nil Then
    TQuery(AtmTabSheetBuild1.Query).ParamByName('PLakNum').AsString:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString;

//if Not FileExists (KupaDllScriptsDir+'KabalaS.sql') Then
  Case Kabala_type Of
  0 : Dm_kupa.RxQuery_HeshForLak.sql.LoadFromfile(KupaDllScriptsDir+'KabalaS.sql');
  1 : Dm_kupa.RxQuery_HeshForLak.sql.LoadFromfile(KupaDllScriptsDir+'KabalaM.sql');
 10 : Dm_kupa.RxQuery_HeshForLak.sql.LoadFromfile(KupaDllScriptsDir+'KabalaN.sql');
  End;

//   showmessage(AtmTabSheetBuild1.SqlFileName+'<>'+KupaDllScriptsDir);
end;

procedure TFrm_Kabala.FormShow(Sender: TObject);
Var
  NewKabala:Boolean;
  HStr     :shortstring;  
Begin
  If HesFromCrt<>0 Then
  Begin
    DM_Kupa.AtmRxQuery_Kabala.Locate('KabalaNumber;YehusYear',VarArrayOf([HesFromCrt,YearFromCrt]),[]);
    CurKabalaYear:=YearFromCrt;
    Kabala_type:=Dm_kupa.AtmRxQuery_KabalaKabalaKind.AsInteger;
    If Kabala_type=0 Then HStr:='������ ' Else HStr:='����� ';
    Caption:='����� '+HStr+IntToStr(CurKabalaYear);
    Exit;
  End;

  DM_Kupa.AtmRxQuery_Kabala.Last;
  NewKabala:=True;
  if (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=0)
     And (DM_Kupa.AtmRxQuery_Kabala.RecordCount<>0)  Then
    NewKabala:=MessageDlg('����� ������� ����� ��� ���� ��� ������ ������ ������ ����',mtInformation,[mbYes,mbNo],0)=mrNo;
  if NewKabala Then
    DM_Kupa.AtmRxQuery_Kabala.Append;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if RxDBGrid_KabHes.DataSource.DataSet.FieldByName('CodeBitzua').AsInteger=1 Then
    Background:=clRed;
end;

procedure TFrm_Kabala.Act_CloseOpenHeshboniotExecute(Sender: TObject);
begin
  If TypeOfFile='���' Then Exit;
  Screen.Cursor:=crSQLWait;
  Try
    if DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger in [1,2] Then
    Begin
      DM_Kupa.UpdateMakavForCloseHesbonit;
      DM_Kupa.UpdateKabalaForCloseHesbonit;
    End
    Else
      MessageDlg('�� ���� ���� �������� �� ���� ��� ����',mtError,[mbOk],0);
  Finally
    Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_Kabala.Act_SelectOnlyOpenKabalotExecute(Sender: TObject);
begin
  Act_SelectOnlyOpenKabalot.Checked:= Not Act_SelectOnlyOpenKabalot.Checked;
  DM_Kupa.AtmRxQuery_Kabala.Close;
  if Act_SelectOnlyOpenKabalot.Checked Then
  Begin
    DM_Kupa.AtmRxQuery_Kabala.MacroByName('MFilter').AsString:='TotalZikuy<>TotalSumKabla And CodeLakoach In '+
                                                               '(Select Distinct CodeLakoach From Makav Where TotalHesbonitWithMAM-Total_Zikuy <> 0 And Status Between 0 And 10)';
  End
  Else
    DM_Kupa.AtmRxQuery_Kabala.MacroByName('MFilter').AsString:='0=0';
  DM_Kupa.AtmRxQuery_Kabala.Open;
{Select CodeLakoach From Kabala
Where TotalZikuy<>TotalSumKabla
And CodeLakoach In
(Select Distinct CodeLakoach From Makav
Where TotalHesbonitWithMAM-Total_Zikuy <> 0)}

end;

procedure TFrm_Kabala.RxDBGrid_ChecksKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
Var
  IntH : LongInt;
  Sum  : Real;
begin
  Frm_Kabala.OnKeyDown:=nil;
  if (Key=VK_F5) And
    (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('SchumCheck'))
  Then
  Begin
    Sum:=StrToFloat(Edit_ItratLakoach.Text)-StrToFloat(DBEdit_TotalKabala.Text);
    If Sum>0 Then
      DM_Kupa.AtmRxQuery_Checks.FieldByName('SchumCheck').AsCurrency:=Sum
    Else
      Showmessage('��� ���� �����');
  End;
  If Key=VK_F9 Then
  Begin
    if TDbGrid(Sender).SelectedField.FieldKind=fkData Then
    With DM_Kupa.Qry_Temp DO
    Begin
      If (StrToFloat(DBEdit_TotalKabala.Text)=0) And
         (TDbGrid(Sender).SelectedField.FieldName='MisparCheshbon')
      Then
      Begin
        Close;
        Sql.Clear;
        Sql.Add('Select C.MisparCheshbon,C.KodBank,C.SnifNumber');
        Sql.Add('From CHECKS C,KABALA K');
        Sql.Add('Where K.YEHUSYEAR=C.KABALAYEAR');
        Sql.Add('AND C.CODESUGTASHLUM=0');
        Sql.Add('AND C.MisparCheshbon IS NOT NULL');
        Sql.Add('AND K.KABALANUMBER=C.KABALANUMBER');
        Sql.Add('AND K.CODELAKOACH='+DM_Kupa.AtmRxQuery_KabalaCodeLakoach.Asstring);
        Sql.Add('Order By Tnua');
        Open;
        if Not Eof And BOF Then
        Begin
          Last;
          DM_Kupa.AtmRxQuery_Checks.FieldByName('MisparCheshbon').AsString:=
            DM_Kupa.Qry_Temp.FieldByName('MisparCheshbon').AsString;
          DM_Kupa.AtmRxQuery_Checks.FieldByName('SnifNumber').AsString:=
            DM_Kupa.Qry_Temp.FieldByName('SnifNumber').AsString;
          DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank').AsString:=
            DM_Kupa.Qry_Temp.FieldByName('KodBank').AsString;
          TDbGrid(Sender).SelectedIndex:=TDbGrid(Sender).SelectedIndex+3;
        End; //Not Eof And BOF
        Exit;
      End; //IntH=0
      Close;
      Sql.Clear;
      Sql.Add('Select Max(TNUA) SN From CHECKS');
      Open;
      IntH:=FieldByName('SN').AsInteger;
      Close;

      If IntH<>0 Then
      Begin
        Sql.Clear;
        Sql.Add('Select '+TDbGrid(Sender).SelectedField.FieldName);
        Sql.Add('From CHECKS Where TNUA='+IntToStr(IntH));
        Open;
        if Not Eof And BOF Then
        Begin
          DM_Kupa.AtmRxQuery_Checks.Edit;
          TDbGrid(Sender).SelectedField.AsString:=FieldByName(TDbGrid(Sender).SelectedField.FieldName).AsString;
          If TDbGrid(Sender).SelectedField.FieldName='CheckNumber' Then
             TDbGrid(Sender).SelectedField.AsString:=IntTostr(TDbGrid(Sender).SelectedField.Asinteger+1);
          If TDbGrid(Sender).SelectedField.FieldName='DatePiraon' Then
             TDbGrid(Sender).SelectedField.AsDateTime:=IncMonth(TDbGrid(Sender).SelectedField.AsDateTime,1);
          If TDbGrid(Sender).SelectedField.FieldName='SchumCheck' Then
             TDbGrid(Sender).SelectedField.AsString:=TDbGrid(Sender).SelectedField.AsString;

          TDbGrid(Sender).SelectedIndex:=TDbGrid(Sender).SelectedIndex+1;
        End;
        Close;
      End;
    End;
  End;
  Frm_Kabala.OnKeyDown:=Frm_Kabala.FormKeyDown;
end;

procedure TFrm_Kabala.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  With DM_Kupa Do
  Begin
    if (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
       (AtmRxQuery_KabalaTotalSumKabla.AsFloat=0) Then
      Case MessageDlg('���� ����� ��� ������ ��� ����� ����',mtConfirmation,[mbYes,mbNo,mbCancel],0) Of
        mrYes:AtmRxQuery_Kabala.Delete;
        mrCancel:Action:=caNone;
      End; //Case

     If (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
        (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0)
     Then
     Begin
       Showmessage('���� �� ������ ');
       Abort;
     End;

  End;
end;

procedure TFrm_Kabala.KabCodeLakoachBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
   If Kabala_type=10 Then
   Begin
     If TypeOfFile='���' Then
       KabCodeLakoach.SearchComponent:=AtmAdvSearch_Nehag
     Else
       KabCodeLakoach.SearchComponent:=AtmAdvSearch_Sapak;
   End
   Else
     KabCodeLakoach.SearchComponent:=AtmAdvSearch_lakoach;
end;

procedure TFrm_Kabala.RxDBGrid_ChecksColEnter(Sender: TObject);
begin
    If TDbGrid(Sender).SelectedField.FieldName='SchumCheck' Then
       StatusBar1.SimpleText:='F5 ����� ���� ���'
    Else
       StatusBar1.SimpleText:='';
end;

procedure TFrm_Kabala.AtmTabSheetBuild1GridDblClick(Sender: TObject;
  ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
Var
Kab,Shana  :  ShortString;
begin
    Kab:=AtmTabSheetBuild1.Query.FieldByName('KabalaNumber').Asstring;
    Shana:=AtmTabSheetBuild1.Query.FieldByName('YehusYear').Asstring;
    PageControl1.ActivePage:=TS_Main;
    If DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger<>
      StrToInt(Shana) Then
    Begin
      DM_Kupa.AtmRxQuery_Kabala.Close;
      DM_Kupa.AtmRxQuery_Checks.Close;
      DM_Kupa.AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=StrToInt(Shana);
      DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=StrToInt(Shana);
      DM_Kupa.AtmRxQuery_Kabala.Open;
    End;
    Frm_Kabala.DBNavigator1.DataSource.DataSet.Cancel;
    DM_Kupa.AtmRxQuery_Kabala.Locate('KabalaNumber;YehusYear',VarArrayOf
       ([Kab,Shana]),[]);

end;

procedure TFrm_Kabala.N9Click(Sender: TObject);
Var
StrH,HStr :String;

begin
   Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
   Strh:=Atmini.readString('Hafkada','SugTashlumItra','');
   if Not InputQuery('��� ����� ������ ����','����� ��� ����� ������ ����',Strh) Then
      Exit;
   SugTashlumItra:=Strh;
   Atmini.WriteString('Hafkada','SugTashlumItra',Strh);
   Atmini.free;

end;

procedure TFrm_Kabala.N10Click(Sender: TObject);
Var
StrH,HStr :String;

begin
   Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
   Strh:=IntToStr(Atmini.readInteger('Hafkada','TopLinesKab',10) );
   if Not InputQuery('������ �������','����� ������ �������',Strh) Then
      Exit;
   TopLinKab:=StrToInt(Strh);
   Atmini.WriteInteger('Hafkada','TopLinesKab',TopLinKab);
   Atmini.free;

end;

procedure TFrm_Kabala.N11Click(Sender: TObject);
Var
  StrH,HStr :String;
Begin
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  Strh:=Atmini.readString('Kopa','Sug_File','���');
  Repeat
   if Not InputQuery('����� ����� [���/���]','����� �����',Strh) Then
      Exit;
  Until (Strh='���') Or (Strh='���');
  Atmini.WriteString('Kopa','Sug_File',Strh);
  Atmini.free;
  TypeOfFile:=Strh;
  If Kabala_type=10 Then
  Begin
    If TypeOfFile='���' Then
      Label_CodeLakoach.Caption:='<���>'
    Else
      Label_CodeLakoach.Caption:='<���>';
  End;

end;

end.

unit RptTofesHafkada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, ExtCtrls, QuickRpt, Qrctrls;

type
  TRpt_TofesHafkada = class(TForm)
    QuickRep1: TQuickRep;
    Qry_TofesHafkada: TQuery;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRDBText_MisHafkada: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel_Snif: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText_DateHafkada: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRBand2: TQRBand;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRBand4: TQRBand;
    QRLabel14: TQRLabel;
    QRShape16: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel20: TQRLabel;
    Qry_TotalCash: TQuery;
    QRDBText_TotalCash: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText_TotalChecks: TQRDBText;
    QRLabel_Cancel: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel_Totals: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRDBText12: TQRDBText;
    Qry_TotalCheck: TQuery;
    QRLabel22: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape19: TQRShape;
    QRShape22: TQRShape;
    QRLabel23: TQRLabel;
    Query_tmp: TQuery;
    QRL_Shem: TQRLabel;
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure Qry_TofesHafkadaBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QRLabel5Print(sender: TObject; var Value: String);
    procedure Qry_TofesHafkadaAfterOpen(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel_TotalsPrint(sender: TObject; var Value: String);
    procedure QRDBText11Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRDBText10Print(sender: TObject; var Value: String);
    procedure QRLabel2Print(sender: TObject; var Value: String);
    procedure QRLabel_SnifPrint(sender: TObject; var Value: String);
    procedure QRLabel4Print(sender: TObject; var Value: String);
    procedure QRLabel3Print(sender: TObject; var Value: String);
    procedure QRLabel1Print(sender: TObject; var Value: String);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel22Print(sender: TObject; var Value: String);
    procedure QRL_ShemPrint(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
    CompayName:String;
    DShemBank,DDate,DKodSnif,DMisHesh,DMisHafkada:String
  end;

  Procedure PrintHafkada(CurHafkadaNum,YehusYear,TopLine,NumOfCopy:LongInt;PrintToPrinter :Boolean;ShemBank,TheDate,KodSnif,MisHesh:String);
//Procedure PrintHafkada(CurHafkadaNum,YehusYear:LongInt;PrintToPrinter :Boolean;ShemBank,TheDate,KodSnif,MisHesh:String);
  Procedure InitPrintHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear,PrintToPrinter,TopLines,NumOfCopys:PChar);

var
  Rpt_TofesHafkada: TRpt_TofesHafkada;

implementation

uses AtmRutin, AtmConst, DMKupa;

{$R *.DFM}

procedure TRpt_TofesHafkada.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea=0 Then // Kabala_type
     Value:=FormatFloat('0.00',Abs(TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat))
  Else
     Value:=FormatFloat('"$"0.00',Abs(TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat))
end;

procedure TRpt_TofesHafkada.Qry_TofesHafkadaBeforeOpen(DataSet: TDataSet);
begin
  Screen.Cursor:=crSqlWait;
end;

procedure TRpt_TofesHafkada.FormCreate(Sender: TObject);
begin
  DM_Kupa.Qry_Mname.Open;
  CompayName:=DM_Kupa.Qry_Mname.FieldByName('CompanyName').AsString;
  CompayName:=DoEncryption(CompayName,Mname_Key,MnameDecryptKey,False);
  DM_Kupa.Qry_Mname.Close;
end;

procedure TRpt_TofesHafkada.QRLabel5Print(sender: TObject;
  var Value: String);
begin
  Value:=CompayName;
end;

procedure TRpt_TofesHafkada.Qry_TofesHafkadaAfterOpen(DataSet: TDataSet);
begin
  Screen.Cursor:=crDefault;
end;

procedure TRpt_TofesHafkada.FormDestroy(Sender: TObject);
Var
  I :longInt;
begin
  For I:=0 To ComponentCount-1 Do
    if Components[i] is TDataset Then
      TDataset(Components[i]).Close;

end;

Procedure PrintHafkada(CurHafkadaNum,YehusYear,TopLine,NumOfCopy:LongInt;PrintToPrinter :Boolean;ShemBank,TheDate,KodSnif,MisHesh:String);
Var
{NumOfCopy,}Code :LongInt;
Begin
  Rpt_TofesHafkada:=TRpt_TofesHafkada.Create(Nil);
  With Rpt_TofesHafkada Do
  Begin
    QuickRep1.Page.TopMargin:=TopLine;
    DShemBank:=ShemBank;
    DDate:=TheDate;
    DKodSnif:=KodSnif;
    DMisHesh:=MisHesh;
    DMisHafkada:=IntToStr(CurHafkadaNum);
  End;

//NumOfCopy:=2;
  For Code:=1 To NumOfCopy Do
  Begin
    Rpt_TofesHafkada.Qry_TofesHafkada.ParamByName('PMisHafkada').AsInteger:=CurHafkadaNum;
    Rpt_TofesHafkada.Qry_TofesHafkada.ParamByName('PYehusYear').AsInteger:=YehusYear;
    Rpt_TofesHafkada.Qry_TotalCash.ParamByName('PMisHafkada').AsInteger:=CurHafkadaNum;
    Rpt_TofesHafkada.Qry_TotalCash.ParamByName('PYehusYear').AsInteger:=YehusYear;
    Rpt_TofesHafkada.Qry_TotalCheck.ParamByName('PMisHafkada').AsInteger:=CurHafkadaNum;
    Rpt_TofesHafkada.Qry_TotalCheck.ParamByName('PYehusYear').AsInteger:=YehusYear;
    Rpt_TofesHafkada.Qry_TofesHafkada.Open;
    Rpt_TofesHafkada.Qry_TotalCash.Open;

    Rpt_TofesHafkada.Qry_TofesHafkada.First;
    Kodmatbea:=Rpt_TofesHafkada.Qry_TofesHafkada.FieldByName('KodMatbea').AsInteger;

    Rpt_TofesHafkada.Qry_TotalCheck.Open;
    if PrintToPrinter Then
      Rpt_TofesHafkada.QuickRep1.Print
    Else
      Rpt_TofesHafkada.QuickRep1.Preview;

  End; // NumOfCopy

    Rpt_TofesHafkada.Release;
    Rpt_TofesHafkada:=Nil;
End;


Procedure InitPrintHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear,PrintToPrinter,TopLines,NumOfCopys:PChar);
Var
  FreeDM :Boolean;
  BoolH :Boolean;
  IntH,i :LongInt;
Begin
  KupaAlias:=StrPas(AAlias);
  KupaDBUserName:=StrPas(AUserName);
  KupaDBPassword:=StrPas(APassword);
  FreeDM:=False;
  if DM_Kupa=Nil Then
  Begin
    DM_Kupa:=TDm_Kupa.Create(Nil);
    FreeDM:=True;
  End;
  IntH:=StrToInt(HafkadaNum);
  BoolH:=CompareText(PrintToPrinter,'True')=0;
//PrintHafkada(IntH,StrToInt(YehusYear),10,BoolH,'','','','');
//For i:=? to ? Do   // david 28-5-2002
//Begin
// IntH:=i;
   PrintHafkada(IntH,StrToInt(YehusYear),StrToInt(TopLines),StrToInt(NumOfCopyS),BoolH,'','','','');
//End;
  if FreeDM Then
//if DM_Kupa<>Nil Then
  Begin
    DM_Kupa.Free;
    DM_Kupa:=Nil;
  End;
End;

procedure TRpt_TofesHafkada.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel_Cancel.Enabled:=Qry_TofesHafkada.FieldByName('KodBitzua').AsInteger=9;
end;

procedure TRpt_TofesHafkada.QRLabel_TotalsPrint(sender: TObject;
  var Value: String);
begin
  If Kodmatbea=0 Then
    Value:=FormatFloat('0.00',Abs(QRDBText_TotalCash.DataSet.FieldByName(QRDBText_TotalCash.DataField).AsFloat)+Abs(QRDBText_TotalChecks.DataSet.FieldByName(QRDBText_TotalChecks.DataField).AsFloat))
  Else
    Value:=FormatFloat('"$"0.00',Abs(QRDBText_TotalCash.DataSet.FieldByName(QRDBText_TotalCash.DataField).AsFloat)+Abs(QRDBText_TotalChecks.DataSet.FieldByName(QRDBText_TotalChecks.DataField).AsFloat))
end;

procedure TRpt_TofesHafkada.QRDBText11Print(sender: TObject;
  var Value: String);
begin
  if Trim(Value)='' Then
    Value:=DShemBank;
end;

procedure TRpt_TofesHafkada.QRDBText9Print(sender: TObject;
  var Value: String);
begin
  if Trim(Value)='' Then
    Value:=DKodSnif;
end;

procedure TRpt_TofesHafkada.QRDBText10Print(sender: TObject;
  var Value: String);
begin
  if Trim(Value)='' Then
    Value:=DMisHesh;
end;

procedure TRpt_TofesHafkada.QRLabel2Print(sender: TObject;
  var Value: String);
begin
  if Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof Then
    Value:='����� '+DShemBank;
end;

procedure TRpt_TofesHafkada.QRLabel_SnifPrint(sender: TObject;
  var Value: String);
begin
  if Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof Then
    Value:='���� '+DKodSnif;
end;

procedure TRpt_TofesHafkada.QRLabel4Print(sender: TObject;
  var Value: String);
begin
  if Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof Then
    Value:='����� ����� ���� '+DMisHesh;
end;

procedure TRpt_TofesHafkada.QRLabel3Print(sender: TObject;
  var Value: String);
begin
  if Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof Then
    Value:='����� ����� '+DDate;
end;

procedure TRpt_TofesHafkada.QRLabel1Print(sender: TObject;
  var Value: String);
begin
  if Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof Then
    Value:='����� ���� '+DMisHafkada;
end;

procedure TRpt_TofesHafkada.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=Not (Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof);
  PrintBand:=PrintBand And (Qry_TotalCheck.FieldByName('TotalCheck').AsFloat<>0);
end;

procedure TRpt_TofesHafkada.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=Not (Qry_TofesHafkada.Eof And Qry_TofesHafkada.Bof);
  PrintBand:=PrintBand And (Qry_TofesHafkada.FieldByName('CodeSugTashlum').AsInteger<>1);
end;

procedure TRpt_TofesHafkada.QRLabel22Print(sender: TObject;
  var Value: String);
begin
  if Qry_TofesHafkada.FieldByName('SugHafkada').AsInteger = 1 Then
    Value:='����� ������'
  Else
    Value:='����� �������';
end;

procedure TRpt_TofesHafkada.QRL_ShemPrint(sender: TObject;
  var Value: String);
begin
  With Query_Tmp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Shemlakoach from Kabala');
    Sql.Add('Where KabalaNumber = '+Qry_TofesHafkada.FieldByName('Misparkabala').Asstring);
    Sql.Add('And YehusYear= '+Qry_TofesHafkada.FieldByName('YehusYear').Asstring) ;
    Open;
    if Query_Tmp.RecordCount>0 Then
       Value:=Query_Tmp.FieldByName('Shemlakoach').AsString
    Else
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Shemlakoach from KabalaMas');
      Sql.Add('Where KabalaNumber = '+Qry_TofesHafkada.FieldByName('Misparkabala').Asstring);
      Sql.Add('And YehusYear= '+Qry_TofesHafkada.FieldByName('YehusYear').Asstring) ;
      Open;
      if Query_Tmp.RecordCount>0 Then
       Value:=Query_Tmp.FieldByName('Shemlakoach').AsString;
    End;
  End;
end;

end.
{
SELECT Distinct
  Kopa.Misparkabala, Kopa.Schom, Kopa.MisparCheck, Kopa.KodBank, Kopa.Snif, Kopa.TarichPeraon,  Kopa.MisparChesbon, Kopa.TarichHafkada, Kopa.MisparHafkada, Kopa.CodeLakoach, Kopa.CodeSugTashlum, Kopa.KodMatbea,
  Kabala.ShemLakoach,Kopa.ChesbonBankHafkada, Kopa.KodBankHafkada, Kopa.SnifHafkada,Kopa.KodBitzua,Kopa.SugHafkada,KodTavla.Teur_Tavla,KT2.Teur_Tavla SugTashlum
FROM Kopa,KodTavla,KodTavla KT2 ,Kabala
  Where
  (Kabala.KabalaNumber = Kopa.Misparkabala)
  And (Kabala.YehusYear=:PYehusYear)
  AND KodTavla.Sug_Tavla=101
  AND Kopa.YehusYear = :PYehusYear
  AND KT2.Sug_Tavla=107
  AND KT2.Kod_Tavla=Kopa.CodeSugTashlum
  AND KodTavla.Kod_Tavla=Kopa.KodBankHafkada
  AND Kopa.MisparHafkada = :PMisHafkada
  ORDER BY Kopa.TarichPeraon,Kopa.Misparkabala
}

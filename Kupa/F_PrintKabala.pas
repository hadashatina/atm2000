unit F_PrintKabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls,Inifiles, QRPrntr;

type
  TFrm_PrintKabala = class(TForm)
    QuickRep1: TQuickRep;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel_AddressLak: TQRLabel;
    QRDBText7: TQRDBText;
    QRBand5: TQRBand;
    QRDBText8: TQRDBText;
    QRLabel3: TQRLabel;
    QRBand6: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRShape13: TQRShape;
    QRShape15: TQRShape;
    QRShape17: TQRShape;
    QRShape12: TQRShape;
    QRDBText5: TQRDBText;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    GroupHeaderBand1: TQRBand;
    QRShape36: TQRShape;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    ChildBand1: TQRChildBand;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRSubDetail2: TQRSubDetail;
    ChildBand2: TQRChildBand;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    GroupHeaderBand2: TQRBand;
    QRShape34: TQRShape;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel46: TQRLabel;
    QRShape42: TQRShape;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRDBText25: TQRDBText;
    QRShape43: TQRShape;
    QRShape41: TQRShape;
    GroupFooterBand1: TQRBand;
    QRLabel27: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QrLogo: TQRBand;
    QRImage1: TQRImage;
    ChildBand3: TQRChildBand;
    QRLabel1: TQRLabel;
    QRDBText_MisHafkada: TQRDBText;
    QRLabel_Makor: TQRLabel;
    QRLabel12: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText14: TQRDBText;
    QRMemo_CompanyDetails: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText26: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel10: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRLabel5Print(sender: TObject; var Value: String);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText_CheckNumberPrint(sender: TObject; var Value: String);
    procedure QRLabel8Print(sender: TObject; var Value: String);
    procedure QuickRep1AfterPrint(Sender: TObject);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure QRMemo_CompanyDetailsPrint(sender: TObject;
      var Value: String);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRLabel21Print(sender: TObject; var Value: String);
    procedure QRDBText17Print(sender: TObject; var Value: String);
    procedure QRDBText18Print(sender: TObject; var Value: String);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure GroupHeaderBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText25Print(sender: TObject; var Value: String);
    procedure QRExpr1Print(sender: TObject; var Value: String);
    procedure GroupFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRExpr2Print(sender: TObject; var Value: String);
    procedure QRLabel37Print(sender: TObject; var Value: String);
    procedure QRLabel39Print(sender: TObject; var Value: String);
    procedure QrLogoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRShape36BeforePrint(sender: TObject);
  private
    { Private declarations }
    CompayName:String;
  public
    { Public declarations }
  end;

var
  Frm_PrintKabala: TFrm_PrintKabala;
  Total_sum      : Real;

implementation

uses DMKupa, AtmRutin, AtmConst;

{$R *.DFM}

procedure TFrm_PrintKabala.FormCreate(Sender: TObject);
begin
{  DM_Kupa.Qry_Mname.Open;
  CompayName:=DM_Kupa.Qry_Mname.FieldByName('CompanyName').AsString;
  CompayName:=DoEncryption(CompayName,Mname_Key,MnameDecryptKey,False);
  DM_Kupa.Qry_Mname.Close;
}
    QuickRep1.Page.TopMargin:=TopLinKab;
end;

procedure TFrm_PrintKabala.QRLabel5Print(sender: TObject;
  var Value: String);
begin
  Value:=CompayName;
end;

procedure TFrm_PrintKabala.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
Var
Bait   :  Shortstring;
begin
  QRLabel33.Caption:=ShemMadpis;
  Total_sum:=0;
  With Dm_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type=10 Then
    Begin
      Sql.Add('Select Ktovet_1,Ir_1,Mikud_1 From Nehag');
      Sql.Add('Where Kod_Nehag = '+DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
    End
    Else
    Begin
      Sql.Add('Select Ktovet_1,Mis_Bait1,Ir_1,Mikud_1 From Lakoach');
      Sql.Add('Where Kod_Lakoach = '+DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
    End;
    Open;
    If Kabala_type<>10 Then Bait:=FieldByName('Mis_Bait1').AsString
    Else Bait:='';

    QRLabel_AddressLak.Caption:=FieldByName('Ktovet_1').AsString+' '+
                                Bait+' '+
                                FieldByName('Ir_1').AsString+' '+
                                FieldByName('Mikud_1').AsString+' ';
    Close;
  End;
end;

procedure TFrm_PrintKabala.QRDBText6Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
end;

procedure TFrm_PrintKabala.QRDBText_CheckNumberPrint(sender: TObject;
  var Value: String);
begin
  if TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsInteger=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QRLabel8Print(sender: TObject;
  var Value: String);
begin
  if CompareText('����',QRLabel_Makor.Caption)=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QuickRep1AfterPrint(Sender: TObject);
begin
  //DM_Kupa.Qry_Mname.Close;
end;

procedure TFrm_PrintKabala.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRDBText14Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRMemo_CompanyDetailsPrint(sender: TObject;
  var Value: String);
begin
  Value :=DoEncryption(DM_Kupa.Qry_Mname.FieldByName('CompanyDetail').AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
var
  IniFile : Tinifile;
  OkFlag  : boolean;
begin
    if CompareText('����',QRLabel_Makor.Caption)=0 Then OkFlag:=False
    Else OkFlag:=True;
    QRLabel6.Enabled:=OkFlag;
    QRLabel7.Enabled:=OkFlag;
    QRLabel8.Enabled:=OkFlag;
    QRDBText2.Enabled:=OkFlag;
    QRDBText9.Enabled:=OkFlag;
    QRDBText26.Enabled:=OkFlag;

    IniFile := Tinifile.Create (KupaDllScriptsDir+'Op'+DM_Kupa.Database_Kupa.AliasName+'.ini');
//  DM_Kupa.Qry_Mname.Open;
   QuickRep1.Page.PaperSize := TQRPaperSize (IniFile.ReadInteger ('Printer','PaperSize',Ord (TQRPaperSize(A4))));
   if QuickRep1.Page.PaperSize = Custom then
   begin
      QuickRep1.Page.Length := IniFile.ReadInteger ('Printer','PaperLength',100);
      QuickRep1.Page.Width := IniFile.ReadInteger ('Printer','PaperWidth',100);
   end;
   IniFile.Free;
end;

procedure TFrm_PrintKabala.QRLabel21Print(sender: TObject;
  var Value: String);
Var
Matbea   :   Integer;
begin
  If Kodmatbea{Kabala_type}=0 Then Matbea:=0 Else Matbea:=2;
  Value:=NumberToHebWords(DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat,True,True,Matbea);
end;

procedure TFrm_PrintKabala.QRDBText17Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText18Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRSubDetail1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.GroupHeaderBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.QRDBText25Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRExpr1Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

procedure TFrm_PrintKabala.GroupFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.QRExpr2Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

Procedure TFrm_PrintKabala.QRLabel37Print(sender: TObject;
  var Value: String);
Var
Sum   :   real;

begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then Exit;

    With DM_Kupa.Qry_Temp DO
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select TotalHesbonitWithMAM-Total_Zikuy Itra From Makav');
      Sql.Add('Where Makav.HesbonitNumber=' +
         DM_Kupa.AtmRxQuery_KabHes.FieldByName('HesbonitNumber').Asstring);
      Sql.Add('And Makav.YehusYear=' +
         DM_Kupa.AtmRxQuery_KabHes.FieldByName('HesbonitYear').Asstring);
//Sql.SaveToFile('c:\Kabh');
      Open;
    End;
    Sum:=DM_Kupa.Qry_Temp.FieldByName('Itra').AsFloat;
{
    Sum:=DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ASfloat-
         DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumTashlom').ASfloat;
}
    Value:=FormatFloat('0.00',(Sum));
    Total_sum:=Total_sum+sum;
end;

procedure TFrm_PrintKabala.QRLabel39Print(sender: TObject;
  var Value: String);
begin
    Value:=FormatFloat('0.00',(Total_Sum));
end;

procedure TFrm_PrintKabala.QrLogoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  IniFile : Tinifile;
  Filen   : String;
  Prok    : Boolean;
begin
    IniFile := Tinifile.Create (KupaDllScriptsDir+'Op'+DM_Kupa.Database_Kupa.AliasName+'.ini');
    QRImage1.Picture.LoadFromFile (IniFile.ReadString ('Logo','FileName',''));
    Filen:=IniFile.ReadString ('Logo','FileName','');
    IniFile.Free;
    If FileN='' Then Prok:=False Else Prok:=True;
//  PrintBand := QRImage1.Picture <> nil;
    PrintBand :=Prok;
    QrLogo.Height := QRImage1.Height;
//  QRMemo_CompanyDetails.Enabled := Not (QRImage1.Picture <> nil);
    QRMemo_CompanyDetails.Enabled :=Not Prok;
end;

procedure TFrm_PrintKabala.QRShape36BeforePrint(sender: TObject);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N') Then
   Begin
     QRShape36.Enabled:=False;
     QRShape38.Enabled:=False;
     QRShape39.Enabled:=False;
     QRShape40.Enabled:=False;
     QRShape41.Enabled:=False;
     QRShape43.Enabled:=False;
     QRShape24.Enabled:=False;
     QRShape22.Enabled:=False;
     QRShape23.Enabled:=False;               
   End;
end;

end.


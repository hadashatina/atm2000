unit DMKupa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, RxQuery, AtmRxQuery,IniFiles;

type
  TDM_Kupa = class(TDataModule)
    Database_Kupa: TDatabase;
    Qry_Temp: TQuery;
    Qry_CancelHafkadaFromKupa: TRxQuery;
    AtmRxQuery_Kabala: TAtmRxQuery;
    DS_Kabala: TDataSource;
    UpdateSQL_Kabala: TUpdateSQL;
    AtmRxQuery_Checks: TAtmRxQuery;
    DS_Checks: TDataSource;
    UpdateSQL_Checks: TUpdateSQL;
    AtmRxQuery_ChecksTnua: TIntegerField;
    AtmRxQuery_ChecksKabalaYear: TIntegerField;
    AtmRxQuery_ChecksKabalaNumber: TIntegerField;
    AtmRxQuery_ChecksCodeSugTashlum: TIntegerField;
    AtmRxQuery_ChecksKodBank: TIntegerField;
    AtmRxQuery_ChecksSnifNumber: TIntegerField;
    AtmRxQuery_ChecksDatePiraon: TDateTimeField;
    AtmRxQuery_ChecksSchumCheck: TBCDField;
    AtmRxQuery_ChecksMisparCheshbon: TStringField;
    AtmRxQuery_ChecksKodMatbea: TIntegerField;
    AtmRxQuery_ChecksKamutShtarot: TIntegerField;
    AtmRxQuery_ChecksDateHamara: TDateTimeField;
    AtmRxQuery_ChecksSchumDollar: TBCDField;
    AtmRxQuery_ChecksShuraKopa: TIntegerField;
    AtmRxQuery_ChecksStatus: TIntegerField;
    AtmRxQuery_ChecksSugStar: TIntegerField;
    AtmRxQuery_ChecksSchumMatbea: TBCDField;
    AtmRxQuery_ChecksMisparHafkada: TIntegerField;
    AtmRxQuery_ChecksLookShemBank: TStringField;
    AtmRxQuery_KabalaYehusYear: TIntegerField;
    AtmRxQuery_KabalaKabalaNumber: TIntegerField;
    AtmRxQuery_KabalaKabalaKind: TIntegerField;
    AtmRxQuery_KabalaDateKabala: TDateTimeField;
    AtmRxQuery_KabalaCodeLakoach: TIntegerField;
    AtmRxQuery_KabalaShemLakoach: TStringField;
    AtmRxQuery_KabalaKtovet: TStringField;
    AtmRxQuery_KabalaHesbonChiuv: TStringField;
    AtmRxQuery_KabalaHesbonZikuy: TStringField;
    AtmRxQuery_KabalaHesbonMasMakor: TStringField;
    AtmRxQuery_KabalaKodBItzua: TIntegerField;
    AtmRxQuery_KabalaMisparHavara: TIntegerField;
    AtmRxQuery_KabalaTotalSumKabla: TBCDField;
    AtmRxQuery_KabalaPratim: TStringField;
    AtmRxQuery_KabalaHaavaraBankait: TIntegerField;
    AtmRxQuery_KabalaShuraKopa: TIntegerField;
    AtmRxQuery_KabalaCodeHaavaraToHan: TIntegerField;
    AtmRxQuery_KabalaLastUpdate: TDateTimeField;
    AtmRxQuery_KabalaMaklidName: TStringField;
    RxQuery_HeshForLak: TRxQuery;
    DS_HeshForLak: TDataSource;
    RxQuery_HeshForLakHesbonitNumber: TIntegerField;
    RxQuery_HeshForLakCodeLakoach: TIntegerField;
    RxQuery_HeshForLakLakoachGroup: TIntegerField;
    RxQuery_HeshForLakHesbonitDate: TDateTimeField;
    RxQuery_HeshForLakTotalHesbonitWithMAM: TBCDField;
    RxQuery_HeshForLakItra: TBCDField;
    Qry_AtmIndexKabala: TQuery;
    UpdateSQL_AtmIndex: TUpdateSQL;
    AtmRxQuery_KabHes: TAtmRxQuery;
    DS_KabHes: TDataSource;
    UpdateSQL_KabHes: TUpdateSQL;
    AtmRxQuery_KabalaTotalZikuy: TBCDField;
    AtmRxQuery_KabHesTnua: TIntegerField;
    AtmRxQuery_KabHesKabalaNumber: TIntegerField;
    AtmRxQuery_KabHesKabalaYear: TIntegerField;
    AtmRxQuery_KabHesSchumHesbonit: TBCDField;
    AtmRxQuery_KabHesDateHesbonit: TDateTimeField;
    AtmRxQuery_KabHesSchumTashlom: TBCDField;
    AtmRxQuery_KabHesDateKabala: TDateTimeField;
    AtmRxQuery_KabHesCodeLakoach: TIntegerField;
    AtmRxQuery_KabHesShemLakoach: TStringField;
    AtmRxQuery_KabHesHesbonitNumber: TIntegerField;
    AtmRxQuery_KabHesLakoachGroup: TIntegerField;
    AtmRxQuery_KabHesHesbonitYear: TIntegerField;
    RxQuery_HeshForLakYehusYear: TIntegerField;
    RxQuery_HeshForLakHesbonitKind: TIntegerField;
    RxQuery_HeshForLakTotal_Zikuy: TBCDField;
    AtmRxQuery_KabHesHesbonitKind: TIntegerField;
    RxQuery_MakavHesbon: TRxQuery;
    UpdateSQL_Makav: TUpdateSQL;
    AtmRxQuery_ChecksCodeBankHafkada: TIntegerField;
    AtmRxQuery_ChecksMisHeshbonHafkada: TStringField;
    AtmRxQuery_ChecksCodeSnifHafkada: TIntegerField;
    Qry_Mname: TQuery;
    AtmRxQuery_ChecksCheckNumber: TStringField;
    AtmRxQuery_ChecksNumberOfPayment: TIntegerField;
    AtmRxQuery_ChecksCreditKind: TIntegerField;
    AtmRxQuery_ChecksValidMonth: TIntegerField;
    AtmRxQuery_ChecksValidYear: TIntegerField;
    AtmRxQuery_ChecksAprovalCode: TIntegerField;
    AtmRxQuery_ChecksPhoneNumber: TStringField;
    AtmRxQuery_ChecksLookSugTashlum: TStringField;
    AtmRxQuery_KabHesCodeBitzua: TIntegerField;
    Query_InsertKupa: TQuery;
    UpdateSQL_InsertKupa: TUpdateSQL;
    UpdateSQL_MakavNhg: TUpdateSQL;
    procedure Database_KupaLogin(Database: TDatabase;
      LoginParams: TStrings);
    procedure DataModuleDestroy(Sender: TObject);
    procedure AtmRxQuery_KabalaAfterScroll(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterOpen(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaCodeLakoachValidate(Sender: TField);
    procedure AtmRxQuery_ChecksBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterPost(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesAfterPost(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesSchumTashlomValidate(Sender: TField);
    procedure AtmRxQuery_KabalaBeforeInsert(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksBeforeEdit(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesBeforeEdit(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaBeforeDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure AtmRxQuery_KabalaDateKabalaValidate(Sender: TField);
    procedure RxQuery_HeshForLakAfterPost(DataSet: TDataSet);
    procedure Database_KupaAfterConnect(Sender: TObject);
    procedure AtmRxQuery_KabalaBeforeScroll(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksBeforeDelete(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterDelete(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksCodeSugTashlumValidate(Sender: TField);
    procedure AtmRxQuery_ChecksKodBankValidate(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    NewKabalaSavedNumber:LongInt;
    ItraForLak :Real;
    KabHesWasInsert :Boolean;
    Procedure CancelHafkada(HafkadaNum,YehusYear:LongInt);
    Procedure FillKabalaFrmLookupFields;
    Procedure ShowOpenHeshbonitForLak(LakCode :LongInt;AllGroup :Boolean);
    Function  GetNextKabalaNumber(Increment :Integer):LongInt;
    Function  GetLastKabalaNumber:LongInt;
    Function  HesbonitInKabHes(KabalaNo,KabalaYear,HeshbonitNo,HesbonitYear,HesbonitKind :LongInt):Boolean;
    Procedure UpdateTotalKabala;
    Function  CalcItratLakoach(CodeLak :String) :Real;
    Function  CalcTotalForSgira:Real;
    procedure BuildLineInMakavForCurKabala;
    Procedure UpdateMakavForCloseHesbonit;
    Procedure UpdateKabalaForCloseHesbonit;
    Procedure BuildLinesInKopaForCurChecks;
  end;

  Procedure InitHafkadaForms(AUserName,APassword,AAlias,KabalaType:PChar);
  Procedure DoneHafkadaForms;
  Procedure DoCancelHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear:PChar);
  Procedure DoKabala(AUserName,APassword,AAlias,ScriptsDir,TypeKabala:PChar);

  Procedure InitAtmCrt(AliasName,DirForIni:ShortString);External 'AtmCrt.Dll';
  Procedure OpenCrt(FormNum :LongInt;MainValue :ShortString);External 'AtmCrt.Dll';
  Procedure DoneAtmCrt;External 'AtmCrt.Dll';

var
  DM_Kupa: TDM_Kupa;
  KupaAlias,KupaDBUserName,KupaDBPassword:String;
  CurKabalaYear          : LongInt;
  SumCheck               : Real;
  KabalaScriptsDir       : String;
  KupaDllIniFileName     : String;
  KupaDllScriptsDir      : String;
  Kabala_type,KodMatbea  : Integer;
  Hadpasa,Atm            : String[1];
  CopysKabala            : String[2];
  TypeOfFile               : Shortstring;
  ShemMadpis,
  SugTashlumItra              : Shortstring;
  HesFromCrt,YearFromCrt,
  LakoGroup,TopLin,TopLinKab  : Integer;
  Atmini   : Tinifile;
implementation

uses F_Hafkada, AtmConst, AtmRutin, F_Kabala;

{$R *.DFM}

Procedure InitHafkadaForms(AUserName,APassword,AAlias,KabalaType:PChar);
Begin
  Try
    KupaAlias:=StrPas(AAlias);
    KupaDBUserName:=StrPas(AUserName);
    KupaDBPassword:=StrPas(APassword);
    Kabala_type:=0; // StrToInt(StrPas(Kabalatype));
    if DM_Kupa=Nil Then
      DM_Kupa:=TDm_Kupa.Create(Nil);
    if Frm_Hafkada=Nil Then
      Frm_Hafkada:=TFrm_Hafkada.Create(Nil);
    Frm_Hafkada.ShowModal;
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
End;

Procedure DoneHafkadaForms;
Begin
  if Frm_Hafkada<>Nil Then
  Begin
    Frm_Hafkada.Release;
    Frm_Hafkada:=Nil;
  End;
  if DM_Kupa<>Nil Then
  Begin
    DM_Kupa.Free;
    DM_Kupa:=Nil;
  End;
End;


Procedure DoCancelHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear:PChar);
Var
  FreeDM:Boolean;
Begin
  Try
    KupaAlias:=StrPas(AAlias);
    KupaDBUserName:=StrPas(AUserName);
    KupaDBPassword:=StrPas(APassword);
    FreeDM:=False;
    if DM_Kupa=Nil Then
    Begin
      DM_Kupa:=TDm_Kupa.Create(Nil);
      FreeDM:=True;
    End;
    DM_Kupa.CancelHafkada(StrToInt(HafkadaNum),StrToInt(YehusYear));
    if FreeDM Then
    Begin
      DM_Kupa.Free;
      DM_Kupa:=Nil;
    End;
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
End;

Procedure DoKabala(AUserName,APassword,AAlias,ScriptsDir,TypeKabala:PChar);
Var
  FreeDM:Boolean;
Begin
  Try
    Screen.Cursor:=crHourGlass;
    Try
      KupaAlias:=StrPas(AAlias);
      KupaDBUserName:=StrPas(AUserName);
      KupaDBPassword:=StrPas(APassword);
      KabalaScriptsDir:=StrPas(ScriptsDir);
      Kabala_type:=0; // StrToint(StrPas(TypeKabala));
      FreeDM:=False;
      if DM_Kupa=Nil Then
      Begin
        DM_Kupa:=TDm_Kupa.Create(Nil);
        FreeDM:=True;
      End;
      if Frm_Kabala=Nil Then
        Frm_Kabala:=TFrm_Kabala.Create(Nil);

      Frm_Kabala.InitKabala;
      InitAtmCrt(KupaAlias,ExtractFilePath(Application.ExeName));
    Finally
      Screen.Cursor:=crDefault;
    End;
    Frm_Kabala.ShowModal;

    If HesFromCrt=0 Then DoneAtmCrt;
    if FreeDM Then
    Begin
      DM_Kupa.Free;
      DM_Kupa:=Nil;
    End;
    if Frm_Kabala<>Nil Then
    Begin
      Frm_Kabala.Free;
      Frm_Kabala:=Nil;
    End;
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
End;


procedure TDM_Kupa.AtmRxQuery_ChecksAfterOpen(DataSet: TDataSet);
begin
  FillKabalaFrmLookupFields;
end;

procedure TDM_Kupa.Database_KupaLogin(Database: TDatabase;
  LoginParams: TStrings);
begin
    LoginParams.Clear;
    LoginParams.Add('USER NAME='+KupaDBUserName);
    LoginParams.Add('PASSWORD='+KupaDBPassword);
end;

procedure TDM_Kupa.DataModuleDestroy(Sender: TObject);
Var
  I :LongInt;
begin
  For I:=0 To ComponentCount-1 Do
    if Components[i] is TDataSet Then
      TDataSet(Components[i]).Close;
end;

Procedure TDM_Kupa.CancelHafkada(HafkadaNum,YehusYear:LongInt);
//����� �����
Var
  IntH :LongInt;
  RMzoman,RTotalOut,RTotalIn :Real;
Begin
 Try
  Screen.Cursor:=crSqlWait;
  Database_Kupa.KeepConnection:=True;
  //����� ��� ��� ����"�
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Count (*) S From Kopa Where MisparHafkada='+IntToStr(HafkadaNum));
    Sql.Add('And YehusYear = '+IntToStr(YehusYear));
    Sql.Add('And KodBitzua<>9');
    Sql.Add('And CodeHaavaraToHan > 0');
    Open;
    IntH:=FieldByName('S').AsInteger;
    Close;
  End;


  Database_Kupa.StartTransaction;
  Try
    //����� �� �������� ������
    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Sum(Schom) T From Kopa Where MisparHafkada='+IntToStr(HafkadaNum));
      Sql.Add('And YehusYear = '+IntToStr(YehusYear));
      Sql.Add('And CodeSugTashlum = '+IntTOStr(stMezoman));
      Sql.Add('And KodBitzua<>9');
      Qry_Temp.Open;
      RMzoman:=Qry_Temp.FieldByName('T').AsFloat;
      Close;

      if RMZoman<>0 Then
      Begin//����� ������
        Sql.Clear;
        Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOut));
        Sql.Add('And YehusYear = '+IntToStr(YehusYear));
        Open;
        RTotalOut:=FieldByName('Schom').AsFloat;
        Close;

        RTotalOut:=RTotalOut-RMzoman;

     //   if RTotalOut<RTotalIn Then
        Begin
          Sql.Clear;
          Sql.Add('Update Kopa Set Schom='+FloatToStr(RTotalOut));
          Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOut));
          Sql.Add('And YehusYear = '+IntToStr(YehusYear));
          ExecSQL;
        End
{        Else
          MessageDlg('�� ����� ������ ���� ��� �������� ����� �� ����� ����� �����',mtError,[mbOk],0);}
      End;
    End;

    //����� ��� ����� 9 �����
    if IntH>0 Then
      Qry_CancelHafkadaFromKupa.MacroByName('UpdateHan').AsString:=',CodeHaavaraToHan=2'
    Else
      Qry_CancelHafkadaFromKupa.MacroByName('UpdateHan').AsString:='';
    Qry_CancelHafkadaFromKupa.ParamByName('PMisHafkada').AsInteger:=HafkadaNum;
    Qry_CancelHafkadaFromKupa.ParamByName('PYehusYear').AsInteger:=YehusYear;
    Qry_CancelHafkadaFromKupa.ExecSQL;


    //����� ���� ����� �����
    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Update Checks Set MisparHafkada=0');
      Sql.Add('Where MisparHafkada = '+IntToStr(HafkadaNum));
      ExecSQL;
    End;
  Except
    Qry_Temp.Database.Rollback;
    Raise;
  End;
  Database_Kupa.Commit;
  ShowMessage('����� ����� '+IntToStr(HafkadaNum)+' ������');
 Finally
  Database_Kupa.KeepConnection:=False;
  Database_Kupa.Connected:=False;
  Screen.Cursor:=crDefault;
 End;
End;

procedure TDM_Kupa.AtmRxQuery_KabalaAfterScroll(DataSet: TDataSet);
begin
  NewKabalaSavedNumber:=-1;
  Frm_Kabala.Edit_ItratLakoach.Text:='';
  if Frm_Kabala.Act_SelectOnlyOpenKabalot.Checked Then
    Frm_Kabala.Act_ShowOpenHeshbonitForLakExecute(Frm_Kabala.Act_ShowOpenHeshbonitForLak);

  AtmRxQuery_Kabala.FieldByName('CodeLakoach').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Kabala.FieldByName('DateKabala').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('CodeSugTashlum').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('CheckNumber').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('DatePiraon').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('SchumCheck').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.Close;
  AtmRxQuery_Checks.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_Checks.Open;

  AtmRxQuery_KabHes.Close;
  AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_KabHes.Open;
  KabHesWasInsert:=False;

  RxQuery_HeshForLak.Close;
end;

Procedure TDM_Kupa.FillKabalaFrmLookupFields;
Var
   SList:TStringList;
   I :LongInt;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select * From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_SugTashlum));
    Sql.Add('Or Sug_Tavla = '+IntToStr(SugTavla_Bankim));
    Open;
    SList:=TStringList.Create;
    SList.AddObject(IntToStr(SugTavla_SugTashlum),AtmRxQuery_ChecksLookSugTashlum);
    SList.AddObject(IntToStr(SugTavla_Bankim),AtmRxQuery_ChecksLookShemBank);
    FillKodTavlaLookupList(Qry_Temp,SList);
    SList.Free;
    Close;
  End;

End;

procedure TDM_Kupa.AtmRxQuery_KabalaCodeLakoachValidate(Sender: TField);
Var
Atzmada  :  Integer;

begin
    if Frm_Kabala.DBComboBox_HeshbonHiuv.Items.Count>0 Then
      Frm_Kabala.DBComboBox_HeshbonHiuv.Field.AsString:=Frm_Kabala.DBComboBox_HeshbonHiuv.Items[0];

    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      If Kabala_type=10 Then
      Begin
        If TypeOfFile='���' Then
        Begin
          Sql.Add('Select Shem_Nehag,Mis_Han From Nehag');
          Sql.Add('Where Kod_Nehag='+AtmRxQuery_KabalaCodeLakoach.AsString);
        End
        Else
        Begin
          Sql.Add('Select Shem_Sapak,Mis_Han From Sapak');
          Sql.Add('Where Kod_Sapak='+AtmRxQuery_KabalaCodeLakoach.AsString);
        End;
      End
      Else
      Begin
        Sql.Add('Select Shem_Lakoach,Mis_Han,Kod_Atzmada From Lakoach');
        Sql.Add('Where Kod_Lakoach='+AtmRxQuery_KabalaCodeLakoach.AsString);
      End;
      Open;
      if Not Eof And Bof Then
      Begin
        If Kabala_type=10 Then
        Begin
          If TypeOfFile='���' Then
          Begin
            AtmRxQuery_Kabala.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_Nehag').asstring;
            AtmRxQuery_Kabala.fieldbyname('HesbonZikuy').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
          End
          Else
          Begin
            AtmRxQuery_Kabala.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_Sapak').asstring;
            AtmRxQuery_Kabala.fieldbyname('HesbonZikuy').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
          End;
        End
        Else
        Begin
          AtmRxQuery_Kabala.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_lakoach').asstring;
          AtmRxQuery_Kabala.fieldbyname('HesbonZikuy').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
          Atzmada:=Qry_Temp.fieldbyname('Kod_Atzmada').AsInteger;
        End;
        Close;
        If TypeOfFile<>'���' Then
        Begin
          ShowOpenHeshbonitForLak(AtmRxQuery_KabalaCodeLakoach.AsInteger,Frm_Kabala.Spb_ShowOpenHeshbonitForLakGroup.Down);
          ItraForLak:=CalcItratLakoach(AtmRxQuery_KabalaCodeLakoach.AsString);
          Frm_Kabala.Edit_ItratLakoach.Text:=FloatToStr(ItraForLak);
        End;
      End
      Else
      Begin
         Close;
         ShowMessage('���� �� ����');
         abort;
      End;
      If (Kabala_type=0) And (Atzmada=2) Then
      Begin
        ShowMessage('���� ����� ������ ');
        Abort;
      End;
      If (Kabala_type=1) And (Atzmada<>2) Then
      Begin
        ShowMessage('���� ����� ������');
        Abort;
      End;

    End;
end;

Procedure TDM_Kupa.ShowOpenHeshbonitForLak(LakCode :LongInt;AllGroup :Boolean);
Var
  LakGroup :longInt;
Begin
  Frm_Kabala.AtmTabSheetBuild_HeshForLak.BuildDbGrid(Frm_Kabala.Panel_HeshForLak);
  Frm_Kabala.AtmTabSheetBuild_HeshForLak.RunQuery('HesbonitNumber');
  Frm_Kabala.AtmTabSheetBuild_HeshForLak.CurRxDbGrid.OnEnter:=Frm_Kabala.RxDBGrid_OpenHeshbonitEnter;
  Frm_Kabala.AtmTabSheetBuild_HeshForLak.CurRxDbGrid.OnExit:=Frm_Kabala.RxDBGrid_OpenHeshbonitExit;
//Showmessage('ShowOpenHeshbonitForLak');
  if AllGroup Then
    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      If Kabala_type=10 Then
      Begin
        Sql.Add('Select Kod_Kvutza From Nehag');
        Sql.Add('Where Kod_Nehag = '+AtmRxQuery_KabalaCodeLakoach.AsString);
      End
      Else
      Begin
        Sql.Add('Select Kod_Kvutza From Lakoach');
        Sql.Add('Where Kod_Lakoach = '+AtmRxQuery_KabalaCodeLakoach.AsString);
      End;
      Open;
      LakGroup:=FieldByName('Kod_Kvutza').AsInteger;
      LakoGroup:=LakGroup;
      Close;
    End
  Else
  Begin
    LakGroup:=-1;
    LakoGroup:=-1;
  End;
  With RxQuery_HeshForLak Do
  Begin
    Screen.Cursor:=crSQLWait;
    Try
      Close;
      if LakGroup > -1 Then
        MacroByName('MWhereLak').AsString:='LakoachGroup = '+IntToStr(LakGroup)
      Else
        MacroByName('MWhereLak').AsString:='CodeLakoach = '+IntToStr(LakCode);
      Open;
    Finally
      Screen.Cursor:=crDefault;
    End;
  End;
End;


procedure TDM_Kupa.AtmRxQuery_ChecksBeforePost(DataSet: TDataSet);
begin
  With AtmRxQuery_Checks Do
  Begin
    if FieldByName('SchumCheck').AsFloat=0 Then //<=0 David 2-11-2000
    Begin
      Cancel;
      Abort;
    End;

     If (
         (DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString='0')
         or
         (
          (DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger>1)
          And
          (DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger<100)
         )
        )
        And
         (DM_Kupa.AtmRxQuery_Checks.FieldByName('CheckNumber').AsString='') Then
     Begin
       ShowMessage('�� ���� ������ �''� ��� ���� �''�.');
//     Frm_Kabala.RxDBGrid_Checks.SetFocus;
       Frm_Kabala.RxDBGrid_Checks.Col:=5;
       Abort;
       Exit;
     End;

    If (AtmRxQuery_Checks.FieldByName('SchumCheck').AsCurrency < 0 )
      And
       (AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger<100)
      And
       (AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger<>1)
    Then
    Begin
        Showmessage('�� ���� ����� �� ����� �����');
        Abort;
        Exit
    End;

    FieldByName('KabalaYear').AsInteger:=CurKabalaYear;
    FieldByName('KabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    If Kabala_type mod 10 =0 Then
       FieldByName('KodMatbea').AsInteger:=0
    Else
       FieldByName('KodMatbea').AsInteger:=2;  //����
  End;

end;

Function TDM_Kupa.GetNextKabalaNumber(Increment :Integer):LongInt;
Var
  CurYear  : Word;
  Res,Code : LongInt;
  StrH     : String;
Begin
  Result:=0;
  CurYear:=CurKabalaYear;
  With Qry_AtmIndexKabala Do
  Begin
    ParamByName('PYehusYear').AsInteger:=CurYear;
    Open;
    if Eof And Bof Then //��� ����
    Begin

    StrH:='1';
    Repeat
      if Not InputQuery('��� ����','������ ����� ���� '+IntTostr(CurYear),StrH) Then
        Exit;     Val(StrH,Res,Code);
    Until Code=0;

      Insert;
      FieldByName('Last_Number').AsInteger:=Res; // 1
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=aiKabala;
      FieldByName('YehusYear').AsInteger:=CurYear;
      Post;
      Result:=1;
    End
    Else
    Begin//�����
      Res:=FieldByName('Last_Number').AsInteger+Increment;
      Edit;
      FieldByName('Last_Number').AsInteger:=Res;
      Post;
      Result:=Res;
    End;
    Qry_AtmIndexKabala.ApplyUpdates;
    Close;
  End;
End;

Function TDM_Kupa.GetLastKabalaNumber:LongInt;
Var
  CurYear :Word;
Begin
  Result:=0;
  With Qry_AtmIndexKabala Do
  Begin
    ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
    Open;
    Result:=FieldByName('Last_Number').AsInteger;
    Close;
  End;
End;


procedure TDM_Kupa.AtmRxQuery_KabalaBeforePost(DataSet: TDataSet);
begin
  if AtmRxQuery_Kabala.State=dsInsert Then
  Begin
    If DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').IsNull Then
    Begin
       Abort;
       Exit;
    End;
    AtmRxQuery_KabalaDateKabala.AsDateTime:=Trunc(AtmRxQuery_KabalaDateKabala.AsDateTime);
    AtmRxQuery_KabalaKabalaNumber.AsInteger:=GetNextKabalaNumber(+1);
    AtmRxQuery_KabalaYehusYear.AsInteger:=CurKabalaYear;
    AtmRxQuery_KabalaKabalaKind.AsInteger:=0;
    NewKabalaSavedNumber:=AtmRxQuery_KabalaKabalaNumber.AsInteger;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesBeforePost(DataSet: TDataSet);
Var
  TotalSagur :Real;
  Hefresh :Real;
  ItratLakoat :Real;
begin

  With AtmRxQuery_KabHes Do
  Begin
    if FieldByName('HesbonitNumber').AsFloat<=0 Then
    Begin
      Cancel;
      Abort;
      Exit;
    End;
  End;

  With AtmRxQuery_KabHes Do
  Begin
    KabHesWasInsert:=(State=dsInsert);
    FieldByName('KabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    FieldByName('KabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  End;
  TotalSagur:=CalcTotalForSgira;
  if AtmRxQuery_KabHes.State=dsEdit Then
  Begin
//    TotalSagur:=TotalSagur+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat;
    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select SchumTashlom From KABHES Where Tnua = '+AtmRxQuery_KabHes.FieldByName('Tnua').AsString);
      Open;
      TotalSagur:=TotalSagur-FieldByName('SchumTashlom').AsFloat;
      Close;
    End;
  End;

  if ItraForLak < 0 Then //�� ����� ���� �����
    ItratLakoat:=Abs(ItraForLak)
  Else
    ItratLakoat:=0;
  Hefresh:=(TotalSagur+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat) -
           (AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat+ItratLakoat);
  if Hefresh > 0 Then
  Begin
    if TotalSagur=AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat+ItraForLak Then
    Begin
      MessageDlg('��� ����� ������',mtInformation,[mbOk],0);
      AtmRxQuery_KabHes.Cancel;
      Abort;
    End
    Else
      if MessageDlg('����� ������ ��� ����� ��������'+
                     #13'��� ����� �����',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
        AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat:=AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat-Hefresh
      Else
      Begin
        AtmRxQuery_KabHes.Cancel;
        Abort;
      End;
  End;
end;

Function TDM_Kupa.HesbonitInKabHes(KabalaNo,KabalaYear,HeshbonitNo,HesbonitYear,HesbonitKind :LongInt):Boolean;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Count(*) C From Kabhes');
    Sql.Add('Where KabalaNumber = '+IntToStr(KabalaNo));
    Sql.Add('And KabalaYear = '+IntToStr(KabalaYear));
    Sql.Add('And HesbonitNumber = '+IntToStr(HeshbonitNo));
    Sql.Add('And HesbonitYear = '+IntToStr(HesbonitYear));
    Sql.Add('And HesbonitKind = '+IntToStr(HesbonitKind));
    Open;
    Result:=FieldByName('C').AsInteger > 0;
    Close;
  End;
End;

procedure TDM_Kupa.AtmRxQuery_ChecksAfterPost(DataSet: TDataSet);
begin
  if TQuery(DataSet).FieldByName('Tnua').AsInteger=0 Then
  Begin
    TQuery(DataSet).Close;
    if TQuery(DataSet).Params.FindParam('PKabalaNumber')<>Nil Then
      TQuery(DataSet).ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    if TQuery(DataSet).Params.FindParam('PKabalaYear')<>Nil Then
      TQuery(DataSet).ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
    TQuery(DataSet).Open;
    TQuery(DataSet).Last;
  End;


  UpdateTotalKabala;
  If (AtmRxQuery_ChecksCodeSugTashlum.AsInteger<>0) Or
     (AtmRxQuery_ChecksCheckNumber.IsNull) Or
     (AtmRxQuery_Checks.RecordCount=0) Then Exit;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Count(CheckNumber) S');
    Sql.Add('From Checks');
    Sql.Add('Where CodeSugTashlum=0');
    Sql.Add('And KabalaYear='+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add('And KabalaNumber='+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And CheckNumber='+AtmRxQuery_ChecksCheckNumber.AsString);
    Open;
    if Not Eof And Bof Then
    Begin
       If FieldByName('S').AsInteger > 1
{
       If ((AtmRxQuery_Checks.State=dsInsert) And
         (FieldByName('S').AsInteger <> 0) )
         Or
         ((AtmRxQuery_Checks.State<>dsInsert) And
         (FieldByName('S').AsInteger > 1) )
}
      Then
      Begin
        Showmessage('���� ��''� ���� ����� �������');
        Frm_Kabala.RxDBGrid_Checks.Col:=5;
        Abort;
        Exit;
      End;
    End; //Not Eof
  End; //With Qry_Temp


end;

Procedure TDM_Kupa.UpdateTotalKabala;
Begin
  With Qry_Temp Do
  Begin
    //�� ������
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(SchumCheck) SC From Checks');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('AND KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Open;

    AtmRxQuery_Kabala.Edit;
    AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat:=FieldByName('SC').AsFloat;
    Close;
    AtmRxQuery_Kabala.Post;

{    //�� ��� ������ ����

    Sql.Add('AND CodeSugTashlum <= '+IntToStr(stHavaraBank));
    Open;
    AtmRxQuery_Kabala.FieldByName('TotalMoneyOnly').AsFloat:=FieldByName('SC').AsFloat;
    AtmRxQuery_Kabala.Post;
    Close;
}
  End;
End;

Function TDM_Kupa.CalcItratLakoach(CodeLak :String) :Real; //���� ����
Var
  RealH :Real;
Begin
  Screen.Cursor:=crSQLWait;
  RealH:=0;
  Try
    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      If Kabala_type=10 Then
        Sql.Add('Select Sum(TotalHesbonitWithMAM) TH, Sum(Total_Zikuy) TZ From MakavNhg')
      Else
        Sql.Add('Select Sum(TotalHesbonitWithMAM) TH, Sum(Total_Zikuy) TZ From Makav');
      if LakoGroup > -1 Then
        Sql.Add('Where LakoachGroup = '+IntToStr(LakoGroup) )
      Else
        Sql.Add('Where CodeLakoach = '+CodeLak);

      Case Kabala_type Of
      0 : Sql.Add('And HesbonitKind in (1,3,7,8,9,12)' );
      1 : Sql.Add('And HesbonitKind in (2,10,14)' );
     10 : Sql.Add('And HesbonitKind in (1,4,13)' );
      End; // Case Kabala_type
      Sql.Add('And Status =0'); //Status<>9
      Open;
//      RealH := Abs(FieldByName('TH').AsFloat)-Abs(FieldByName('TZ').AsFloat);
        RealH :=Round(FieldByName('TH').AsFloat*100)/100-
                Round(FieldByName('TZ').AsFloat*100)/100; // 29-9-2002
      Close;
    End;
  Finally
    Result:=RealH;
    Screen.Cursor:=crDefault;
  End;
End;

Function TDM_Kupa.CalcTotalForSgira:Real;
Begin
  Screen.Cursor:=crSQLWait;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(SchumTashlom) ST From KabHes');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Open;
    Result:=FieldByName('ST').AsFloat;
    Close;
  End;
  Screen.Cursor:=crDefault;
End;

Procedure TDM_Kupa.AtmRxQuery_KabalaAfterInsert(DataSet: TDataSet);
Begin
  ItraForLak:=0;
  Frm_Kabala.Edit_ItratLakoach.Text:='0';
  FindFirstControlInTabOrder(Frm_Kabala.Panel_Kabala).SetFocus;
  NewKabalaSavedNumber:=-1;
end;

Procedure TDM_Kupa.AtmRxQuery_KabHesAfterPost(DataSet: TDataSet);
Var
  StrH :String;
  Sum  :Real;
begin
  //����� �����
  Sum:=0;
  AtmRxQuery_KabHes.First;
  StrH:='';
  While Not AtmRxQuery_KabHes.Eof Do
  Begin
    StrH:=StrH+AtmRxQuery_KabHes.FieldByName('HesbonitNumber').AsString+'  ';
    Sum:=Sum+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat;
    AtmRxQuery_KabHes.Next;
  End;

  AtmRxQuery_Kabala.Edit;
//If AtmRxQuery_Kabala.FieldByName('Pratim').IsNull Then
  If (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Or
     (AtmRxQuery_Kabala.FieldByName('Pratim').IsNull)
  Then
     AtmRxQuery_Kabala.FieldByName('Pratim').AsString:='���� ������� '+StrH;
  AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat:=Sum; //CalcTotalForSgira;
{  if AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat < AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat Then
    AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat := AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat;}
  AtmRxQuery_Kabala.Post;
end;

procedure TDM_Kupa.BuildLineInMakavForCurKabala;
Var
  LakGroup         :LongInt;
  TotalMasBamakor,TotalItra,
  TotalLoLchiuv    :Real;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type=10 Then
    Begin
      Sql.Add('Select Kod_Kvutza From Nehag');
      Sql.Add('Where Kod_Nehag = '+AtmRxQuery_KabalaCodeLakoach.AsString);
    End
    Else
    Begin
      Sql.Add('Select Kod_Kvutza From Lakoach');
      Sql.Add('Where Kod_Lakoach = '+AtmRxQuery_KabalaCodeLakoach.AsString);
    End;
    Open;
    LakGroup:=FieldByName('Kod_Kvutza').AsInteger;
    Close;
  End;

  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(SchumCheck) SC From CHECKS Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add('And CodeSugTashlum = '+IntToStr(stMasBamakor));
    Open;
    TotalMasBamakor:=FieldByName('SC').AsFloat;
    Close;
  // ��� ����� ������ ����
    TotalItra:=0;
    If StrToInt(SugTashlumItra)<>-1 Then
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Sum(SchumCheck) SC From CHECKS Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
      Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
      Sql.Add('And CodeSugTashlum = '+SugTashlumItra);
      Open;
      TotalItra:=FieldByName('SC').AsFloat;
      Close;
    End;

    Sql.Clear;   // ���� ���� �����> 200 �� ������ ����"�
    Sql.Add('Select Sum(SchumCheck) SC From CHECKS Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add('And CodeSugTashlum > 200');
    Open;
    TotalLoLchiuv:=FieldByName('SC').AsFloat;
    Close;
  End;

  AtmRxQuery_KabHes.First;
  AtmRxQuery_Checks.First;

  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type=10 Then
      Sql.Add('Insert into MakavNhg')
    Else
      Sql.Add('Insert into Makav');
//  Sql.Add('(HesbonitNumber,HesbonitKind,YehusYear,CodeLakoach,LakoachGroup,HesbonitDate,TotalHesbonitWithMAM,Status,Total_Zikuy,Date_Zikuy,DateHafakatHeshbonit,HanForLak,HanForMam,String_1,PratimToHan,TotalMasMakor,Last_Update,CodeHaavaraToHan,Mis_Kabala1,OrderNum,DateForFirstPayment)');
    Sql.Add('(');
    Sql.Add('HesbonitNumber,HesbonitKind,YehusYear,CodeLakoach,LakoachGroup,HesbonitDate,TotalHesbonitWithMAM,Status,Total_Zikuy,Date_Zikuy,DateHafakatHeshbonit,');
    Sql.Add('HanForLak,HanForMam,String_1,PratimToHan,TotalMasMakor,Last_Update,CodeHaavaraToHan,Mis_Kabala1,OrderNum,DateForFirstPayment,Bcd_1,Bcd_2)');

    Sql.Add('Values');
    Sql.Add('('+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    If Kabala_type mod 10=0 Then
      Sql.Add(','+IntToStr(hkKabala))
    Else
      Sql.Add(','+IntToStr(hkKabalaD));
    Sql.Add(','+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add(','+AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
    Sql.Add(','+IntToStr(LakGroup));//����� ����
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
//  Sql.Add(',-'+AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsString);
    Sql.Add(','+FloatToStr(AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency*-1));
    if AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat Then
      Sql.Add(',1') //Status
    Else
      Sql.Add(',0'); //Status
    Sql.Add(','+ FloatToStr(AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency*-1));
//  Sql.Add(',-'+ FloatToStr(Abs(AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency)) );
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
//  Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('HesbonZikuy').AsString+'''');
//  Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString+'''');
    Sql.Add(','+QuotedStr(AtmRxQuery_Kabala.FieldByName('HesbonZikuy').AsString));
    Sql.Add(','+QuotedStr(AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString));
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('HesbonMasMakor').AsString+''''); //String_1 in makav
    Sql.Add(','''+Copy(AddQuatesToString(AtmRxQuery_Kabala.FieldByName('Pratim').AsString),1,30)+'''');//PratimToHan
    Sql.Add(','+FloatToStr(TotalMasBamakor));
    Sql.Add(','''+DateToSqlStr(Now,'mm/dd/yyyy')+'''');
    Sql.Add(',0');
    Sql.Add(','''+AtmRxQuery_KabHes.FieldByName('HesbonitNumber').AsString+'''');

    Sql.Add(','''+AtmRxQuery_Checks.FieldByName('CheckNumber').AsString+'''') ;
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','+FloatToStr(TotalLoLchiuv));
    Sql.Add(','+FloatToStr(TotalItra));
    Sql.Add(')');
//  Sql.SaveToFile('c:\BuildMakav1.Sql');
    ExecSQL;
  End;
End;

procedure TDM_Kupa.UpdateMakavForCloseHesbonit;
Var
Ind,mis        : integer;
HStr           : shortstring;

Begin //����� �������� ������ �����
{  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.LoadFromFile(KabalaScriptsDir+'UpdateMakavFromKabHes.Sql');
    ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_KabalaKabalaNumber.AsInteger;
    ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_KabalaYehusYear.AsInteger;
    ExecSQL;
  End;
  }

  AtmRxQuery_KabHes.Close; //�����
  AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_KabHes.Open;
  KabHesWasInsert:=False;

  AtmRxQuery_KabHes.First;
  While Not AtmRxQuery_KabHes.EOF Do
  Begin
    if AtmRxQuery_KabHes.FieldByName('CodeBitzua').AsInteger=0 Then
      With RxQuery_MakavHesbon Do
      Begin
        Close;
        ParamByName('PHesbonitNumber').AsInteger:=AtmRxQuery_KabHes.FieldByName('HesbonitNumber').AsInteger;
        ParamByName('PYehusYear').AsInteger:=AtmRxQuery_KabHes.FieldByName('HesbonitYear').AsInteger;
        ParamByName('PHesbonitKind').AsInteger:=AtmRxQuery_KabHes.FieldByName('HesbonitKind').AsInteger;
        IF Kabala_Type=10 Then  // ����� ���� �����
        Begin
          with RxQuery_MakavHesbon do
          begin
            FOR IND:=0 To (sql.COUNT)-1 Do
              Begin
              HStr:= SQL.Strings[Ind];
              Mis:= pos('Makav',HStr);
              if Mis <> 0 then
              begin
//              move('MakavNhg',HStr[Mis],SizeOf('MakavNhg'));
                SQL.Strings[Ind]:='SELECT * FROM MakavNhg';
              end;
            End;
          end;
       End; // Kabala_Type=10
        Open;
        Edit;
        FieldByName('Total_Zikuy').AsFloat:=FieldByName('Total_Zikuy').AsFloat+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat;
        FieldByName('Date_Zikuy').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        if Abs(FieldByName('Total_Zikuy').AsFloat-FieldByName('TotalHesbonitWithMAM').AsFloat)<0.01 Then
          FieldByName('Status').AsInteger:=1;
        If FieldByName('Mis_Kabala1').IsNull Then
        Begin
          FieldByName('Mis_Kabala1').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala1').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala2').IsNull Then
        Begin
          FieldByName('Mis_Kabala2').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala2').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala3').IsNull Then
        Begin
          FieldByName('Mis_Kabala3').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala3').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala4').IsNull Then
        Begin
          FieldByName('Mis_Kabala4').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala4').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala5').IsNull Then
        Begin
          FieldByName('Mis_Kabala5').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala5').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End;
        AtmRxQuery_Checks.First; // ����� ����� �� ���� ������
        If (Not AtmRxQuery_Checks.Eof) And (Not AtmRxQuery_Checks.FieldByName('DatePiraon').IsNull)
           Then  FieldByName('DateForFirstPayment').AsDateTime:=Trunc(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime);

//        AtmRxQuery_KabHes.RealSQL.SaveToFile('c:\UpdateMakavForCloseHesbonit2.Sql');
        Post;
        If Kabala_Type=10 Then UpdateObject:=UpdateSQL_MakavNhg;
        ApplyUpdates; // David 1-1-1
        Close;

        AtmRxQuery_KabHes.BeforePost:=Nil;
        AtmRxQuery_KabHes.AfterPost:=Nil;
        AtmRxQuery_KabHes.AutoApplyUpdates:=False;
        Try
          AtmRxQuery_KabHes.Edit;
          AtmRxQuery_KabHes.FieldByName('CodeBitzua').AsInteger:=1;
          AtmRxQuery_KabHes.SavePressed:=True;
          AtmRxQuery_KabHes.Post;
          AtmRxQuery_KabHes.ApplyUpdates;
        Finally
          AtmRxQuery_KabHes.BeforePost:=AtmRxQuery_KabHesBeforePost;
          AtmRxQuery_KabHes.AfterPost:=AtmRxQuery_KabHesAfterPost;
          AtmRxQuery_KabHes.AutoApplyUpdates:=True;
        End;
      End; //if
    AtmRxQuery_KabHes.Next;
  End; //While
End;

procedure TDM_Kupa.UpdateKabalaForCloseHesbonit;
Begin
  AtmRxQuery_KabHes.First;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;   //  ����� ����� ����� �-�
    If Kabala_type=10 Then
      Sql.Add('Update MakavNhg')
    Else
      Sql.Add('Update Makav');
    Sql.Add('Set Total_Zikuy='+'-'+
      FloatToStr(Abs(AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency)) );
    if AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat Then
      Sql.Add(',Status=1'); //Status
    Sql.Add(',Mis_Kabala1='+
      AtmRxQuery_KabHes.FieldByName('HesbonitNumber').AsString);
    Sql.Add('Where HesbonitNumber='+
       AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    If Kabala_type mod 10=0 Then
       Sql.Add('And HesbonitKind='+IntToStr(hkKabala))
    Else
       Sql.Add('And HesbonitKind='+IntToStr(hkKabalaD));
    Sql.Add('And YehusYear = '+
       AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    ExecSql;
  End; //

End;

Procedure TDM_Kupa.BuildLinesInKopaForCurChecks;
Var
  LineNo :LongInt;
  TotalMzomanInKabala :Real;
  LineExist :Boolean;
Begin
  AtmRxQuery_Checks.Close;
  AtmRxQuery_Checks.Open;
  AtmRxQuery_Checks.First;
  TotalMzomanInKabala:=0;
  AtmRxQuery_Checks.AutoApplyUpdates:=False;
  AtmRxQuery_Checks.AfterPost:=Nil;
  LineNo := -1;
  While not(AtmRxQuery_Checks.EOF) do
  Begin
//    ShowMessage('CheckNum='+AtmRxQuery_Checks.FieldByName('CheckNumber').AsString);
    If AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger < stHavaraBank Then
    With Qry_Temp Do
    Begin
//      if AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger <> stMezoman Then
      Begin//�� �����
        Query_InsertKupa.Insert;
        Query_InsertKupa.FieldByName('Misparkabala').AsString:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString;
        Query_InsertKupa.FieldByName('TarichKabala').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        Query_InsertKupa.FieldByName('CodeSugTashlum').AsString:=AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString;
        Query_InsertKupa.FieldByName('CodeLakoach').AsString:=AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString;
        Query_InsertKupa.FieldByName('MisparCheck').AsString:=AtmRxQuery_Checks.FieldByName('CheckNumber').AsString;
        Query_InsertKupa.FieldByName('KodBank').AsString:=AtmRxQuery_Checks.FieldByName('KodBank').AsString;
        Query_InsertKupa.FieldByName('Snif').AsString:=AtmRxQuery_Checks.FieldByName('SnifNumber').AsString;
        Query_InsertKupa.FieldByName('TarichPeraon').AsDateTime:=Trunc(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime);
        Query_InsertKupa.FieldByName('Schom').AsString:=AtmRxQuery_Checks.FieldByName('SchumCheck').AsString;
        Query_InsertKupa.FieldByName('MisparChesbon').AsString:=AtmRxQuery_Checks.FieldByName('MisparCheshbon').AsString;
        If Kabala_type mod 10=0 Then
          Query_InsertKupa.FieldByName('KodMatbea').AsInteger:=0
        Else
          Query_InsertKupa.FieldByName('KodMatbea').AsInteger:=2;//����
        Query_InsertKupa.FieldByName('KodBitzua').AsInteger:=0;
        Query_InsertKupa.FieldByName('MisHaavaraToHan').AsInteger:=0;
        Query_InsertKupa.FieldByName('Ipyun').AsInteger:=0;
        Query_InsertKupa.FieldByName('CodeHaavaraToHan').AsInteger:=0;
        Query_InsertKupa.FieldByName('MisparHafkada').AsInteger:=0;
        Query_InsertKupa.FieldByName('YehusYear').AsInteger:=CurKabalaYear;
        Query_InsertKupa.Post;
(*        Close;
        Sql.Clear;
        Sql.Add('Insert into Kopa');
        Sql.Add('(Misparkabala,TarichKabala,CodeSugTashlum,CodeLakoach,MisparCheck,KodBank,Snif,TarichPeraon,Schom,MisparChesbon,KodMatbea,KodBitzua,MisHaavaraToHan,Ipyun,CodeHaavaraToHan,MisparHafkada,YehusYear)');
        Sql.Add('Values');
        Sql.Add('('+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
        Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString);
        Sql.Add(','+AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
        Sql.Add(','''+AtmRxQuery_Checks.FieldByName('CheckNumber').AsString+'''');
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('KodBank').AsString);
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('SnifNumber').AsString);
        Sql.Add(','''+DateToSqlStr(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime,'mm/dd/yyyy')+'''');
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('SchumCheck').AsString);
        Sql.Add(','''+AtmRxQuery_Checks.FieldByName('MisparCheshbon').AsString+'''');
        Sql.Add(',0');
        Sql.Add(',0');
        Sql.Add(',0');//MisHaavaraToHan
        Sql.Add(',0');
        Sql.Add(',0');//CodeHaavaraToHan
        Sql.Add(',0');//MisparHafkada
        Sql.Add(','+IntToStr(CurKabalaYear)+')');//��� ����
//        Sql.SaveToFile('c:\BuildLinesInKopaForCurChecks3.Sql');
        ExecSql;
*)
      End;

      //����� �������
      if AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger = stMezoman Then
        TotalMzomanInKabala := TotalMzomanInKabala + AtmRxQuery_Checks.FieldByName('SchumCheck').AsFloat;

      //����� ���� ����� �����
      if LineNo=-1 Then
      Begin
        Sql.Clear;
        Sql.Add('Select Max(MisparShura) MS From Kopa Where MisparShura > -1');
        Open;
        LineNo:=FieldByName('MS').AsInteger;
        Close;
      End
      Else
        Inc(LineNo);

{      Sql.Clear;
      Sql.Add('Update Checks Set ShuraKopa = '+LineNo);
      Sql.Add('Where Checks.Tnua = '+AtmRxQuery_Checks.FieldByName('Tnua').AsString);
      ExecSQL;}

      AtmRxQuery_Checks.Edit;
      AtmRxQuery_Checks.FieldByName('ShuraKopa').AsInteger:=LineNo;

      AtmRxQuery_Checks.SavePressed:=True;
      AtmRxQuery_Checks.Post;

      AtmRxQuery_Checks.Next;
    End //Whith
    Else
      AtmRxQuery_Checks.Next;
  End;//While
  AtmRxQuery_Checks.Close;
  AtmRxQuery_Checks.AutoApplyUpdates:=True;
  AtmRxQuery_Checks.AfterPost:=AtmRxQuery_ChecksAfterPost;

  // ����� ����� �������
//  ShowMessage('����� ����� �������');
  if TotalMzomanInKabala <> 0 Then
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type mod 10=0 Then  //  �����
    Begin
      Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanIn));
      Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
    End
    Else                   // �������
    Begin
      Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanInDolar));
      Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
    End;
//    ShowMessage('���� ���� ������ ��������');
    Open; //���� ���� ������ ��������
    LineExist:=Not(EOF And BOF);
    if LineExist Then
      TotalMzomanInKabala:=TotalMzomanInKabala+FieldByName('Schom').AsFloat;
//    ShowMessage('Close ���� ���� ������ ��������');
    Close;

    Sql.Clear;
    if LineExist Then
    Begin
      Sql.Add('Update KOPA Set Schom = '+FloatToStr(TotalMzomanInKabala));
      If Kabala_type Mod 10=0 Then  //  �����
      Begin
        Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanIn));
        Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
      End
      Else                   // �������
      Begin
        Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanInDolar));
        Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
      End;
    End
    Else
    Begin
      Sql.Add('Insert Into KOPA (MisparKabala,Schom,MisparCheck,YehusYear)');
      If Kabala_type mod 10=0 Then  //  �����
        Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanIn)     +','+FloatToStr(TotalMzomanInKabala)+','+'''������ �������'''+','+IntToStr(CurKabalaYear)+')')
//      Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanIn)+','+FloatToStr(TotalMzomanInKabala)+',''������ �������'')')
      Else                   // �������
        Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanInDolar)+','+FloatToStr(TotalMzomanInKabala)+','+'''������ ������� �����'''+','+IntToStr(CurKabalaYear)+')');
    End;
//    ShowMessage('ExecSql ����� ������ �������');
//    Sql.SaveToFile('c:\kabala.sql');
    ExecSQL; //����� ������ �������
  End;
//  ShowMessage('Close');
  AtmRxQuery_Checks.Open;
End;

procedure TDM_Kupa.AtmRxQuery_KabHesSchumTashlomValidate(Sender: TField);
begin
  if AtmRxQuery_KabHesSchumTashlom.AsFloat > AtmRxQuery_KabHesSchumHesbonit.AsFloat Then
  Begin
    MessageDlg('���� ������ ���� ����� ��������',mtError,[mbOk],0);
    Abort;
  End;
end;


procedure TDM_Kupa.AtmRxQuery_KabalaBeforeInsert(DataSet: TDataSet);
begin
  AtmRxQuery_Kabala.FieldByName('CodeLakoach').ReadOnly:=False;
  AtmRxQuery_Kabala.FieldByName('DateKabala').ReadOnly:=False;
  AtmRxQuery_Checks.FieldByName('SchumCheck').ReadOnly:=False;
  AtmRxQuery_Checks.FieldByName('CodeSugTashlum').ReadOnly:=False;
  AtmRxQuery_Checks.FieldByName('CheckNumber').ReadOnly:=False;
end;

procedure TDM_Kupa.AtmRxQuery_ChecksBeforeEdit(DataSet: TDataSet);
begin
  if AtmRxQuery_Checks.FieldByName('Tnua').AsInteger = 0 Then
  Begin
    if MessageDlg('���� ����� �� ����� ���� �������'+
               '��� ����� ���',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
    Begin
      AtmRxQuery_Checks.Close;
      AtmRxQuery_Checks.Open;
      Abort;
    End
    Else
      Abort;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesBeforeEdit(DataSet: TDataSet);
begin
  if AtmRxQuery_KabHes.FieldByName('Tnua').AsInteger=0 Then
    if MessageDlg('���� ����� �� ����� ���� �������'+
             '��� ����� ���',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
    Begin
      AtmRxQuery_KabHes.Close;
      AtmRxQuery_KabHes.Open;
      KabHesWasInsert:=False;
      Abort;
    End
    Else
      Abort;
end;

Procedure TDM_Kupa.AtmRxQuery_ChecksAfterInsert(DataSet: TDataSet);
begin
  If AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then Exit;
  Frm_Kabala.RxDBGrid_Checks.Col:=1;
  AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger:=0;//If in TField The Error in SQL Server
  AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime:=
    AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime;
  AtmRxQuery_Checks.FieldByName('CodeSnifHafkada').AsString:='';
end;

procedure TDM_Kupa.AtmRxQuery_KabalaBeforeDelete(DataSet: TDataSet);
begin
  if AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then
  Begin
    ShowMessage('��� ������ ����� ���� �����');
    Abort;
  End
  Else
  if MessageDlg('��� ����� ���� ��',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
  Begin
    AtmRxQuery_Checks.First;
    While Not ((AtmRxQuery_Checks.Eof) And (AtmRxQuery_Checks.BOF)) Do
      AtmRxQuery_Checks.Delete;
    AtmRxQuery_KabHes.First;
    While Not ((AtmRxQuery_KabHes.Eof) And (AtmRxQuery_KabHes.BOF)) Do
      AtmRxQuery_KabHes.Delete;
    if AtmRxQuery_Kabala.State=dsBrowse Then
      AtmRxQuery_Kabala.Edit;
    AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger := 9;
    if AtmRxQuery_KabalaKabalaNumber.AsInteger<>GetLastKabalaNumber Then
      Abort
    Else
      GetNextKabalaNumber(-1);
    NewKabalaSavedNumber:=-1;
  End;

end;

procedure TDM_Kupa.DataModuleCreate(Sender: TObject);
Var
  F :TIniFile;
  Strh :String;
begin
  if KupaDllIniFileName<>'' Then
  Begin
    F:=TIniFile.Create(KupaDllIniFileName);
    Try
      StrH:=F.ReadString(SectionForMain,KeyAliasForMname,'');
      if StrH<>'' Then
        Qry_Mname.DatabaseName:=StrH;
    Finally
      F.Free;
    End;
  End;
  if Trim(KupaAlias)<>'' Then
  Begin
    Database_Kupa.Connected:=False;
    Database_Kupa.AliasName:=KupaAlias;
  End;
  Qry_Mname.Open;
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  TopLin:=Atmini.readInteger('Hafkada','TopLines',10);
  TopLinKab:=Atmini.readInteger('Hafkada','TopLinesKab',Toplin);
  Hadpasa:=Atmini.readString('Hafkada','FormAzor','N');
  ShemMadpis:=Atmini.readString('Hafkada','Hatima','');
  Atm:=Atmini.readString('Hafkada','Atm','N');
  TypeOfFile:=Atmini.readString('Kopa','Sug_File','���');
  SugTashlumItra:=Atmini.readString('Hafkada','SugTashlumItra','-1');
  CopysKabala:=Atmini.readString('Hafkada','CopysOfKabala','2');
  Atmini.WriteString('Hafkada','FormAzor',Hadpasa);
  Atmini.WriteString('Hafkada','Hatima',ShemMadpis);
  Atmini.WriteString('Hafkada','CopysOfKabala',CopysKabala);
  Atmini.WriteString('Hafkada','SugTashlumItra',SugTashlumItra);
  Atmini.WriteInteger('Hafkada','TopLinesKab',TopLinKab);
  Atmini.free;

end;

procedure TDM_Kupa.AtmRxQuery_KabalaDateKabalaValidate(Sender: TField);
Var
  DD,MM,YY :Word;
begin
  DecodeDate(Sender.AsDateTime,yy,mm,dd);
  if YY<>CurKabalaYear Then
    ShowMessage('����� ����� �� ���� �� ��� ����� �����');
end;

Procedure TDM_Kupa.RxQuery_HeshForLakAfterPost(DataSet: TDataSet);
begin
  if AtmRxQuery_KabHes.ParamByName('Tnua').AsInteger=0 Then
  Begin
    AtmRxQuery_KabHes.Close;
    AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
    AtmRxQuery_KabHes.Open;
    KabHesWasInsert:=False;
  End;
end;

procedure TDM_Kupa.Database_KupaAfterConnect(Sender: TObject);
Var
  StrH :String;
  F:TIniFile;
begin
  F:=TIniFile.Create(KupaDllIniFileName);
  StrH:=F.ReadString(SectionForMain,SQLServerType,'Paradox');
  F.Free;

  if CompareText(StrH,'Oracle')=0 Then
    Database_Kupa.Execute(OracleDateFormatSql);

  if CompareText(StrH,'Sybase')=0 Then
  Begin
    Database_Kupa.StartTransaction;
    Database_Kupa.Execute(SybaseDateFormatSql);
    Database_Kupa.Commit;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_KabalaBeforeScroll(DataSet: TDataSet);
begin
  if (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
     (AtmRxQuery_KabalaTotalSumKabla.AsFloat=0) And
     (AtmRxQuery_Kabala.RecordCount<>0) Then
    Case MessageDlg('���� ����� ��� ������ ��� ����� ����',mtConfirmation,[mbYes,mbNo,mbCancel],0) Of
      mrYes:AtmRxQuery_Kabala.Delete;
      mrCancel: Abort;
    End; //Case
end;

procedure TDM_Kupa.AtmRxQuery_ChecksBeforeDelete(DataSet: TDataSet);
begin
  if AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then
  Begin
    ShowMessage('��� ������ ����� ');
    Abort;
  End;
  SumCheck:=AtmRxQuery_Checks.FieldByName('SchumCheck').asfloat;
end;

procedure TDM_Kupa.AtmRxQuery_ChecksAfterDelete(DataSet: TDataSet);
begin
    AtmRxQuery_Kabala.Edit; // ����� ��"� ����
    AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat:=
       AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat-SumCheck;
    AtmRxQuery_Kabala.Post;
//UpdateTotalKabala;
end;

procedure TDM_Kupa.AtmRxQuery_ChecksCodeSugTashlumValidate(Sender: TField);
begin
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_SugTashlum));
    Sql.Add('And Kod_Tavla = ' +AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString);
    Open;
    if Not Eof And Bof Then Exit;
    Showmessage('��� ����� ����');
    Sender.AsString:='0';
    Abort;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_ChecksKodBankValidate(Sender: TField);
begin
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_Bankim));
    Sql.Add('And Kod_Tavla = ' +AtmRxQuery_Checks.FieldByName('KodBank').AsString);
    Open;
    if Not Eof And Bof Then Exit;
    Showmessage('��� ��� ����');
    Sender.AsString:='0';
    Abort;
  End;

end;

End.

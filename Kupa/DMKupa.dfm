object DM_Kupa: TDM_Kupa
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 15
  Top = 47
  Height = 400
  Width = 625
  object Database_Kupa: TDatabase
    AliasName = 'Atm'
    DatabaseName = 'DB_Kupa'
    KeepConnection = False
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    AfterConnect = Database_KupaAfterConnect
    OnLogin = Database_KupaLogin
    Left = 32
    Top = 24
  end
  object Qry_Temp: TQuery
    DatabaseName = 'DB_Kupa'
    UniDirectional = True
    UpdateMode = upWhereChanged
    Left = 100
    Top = 22
  end
  object Qry_CancelHafkadaFromKupa: TRxQuery
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'Update Kopa'
      'Set KodBitzua=9'
      '%UpdateHan'
      'Where MisparHafkada=:PMisHafkada'
      'And YehusYear = :PYehusYear')
    UniDirectional = True
    Macros = <
      item
        DataType = ftString
        Name = 'UpdateHan'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 230
    Top = 34
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PMisHafkada'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end>
  end
  object AtmRxQuery_Kabala: TAtmRxQuery
    CachedUpdates = True
    BeforeInsert = AtmRxQuery_KabalaBeforeInsert
    AfterInsert = AtmRxQuery_KabalaAfterInsert
    BeforePost = AtmRxQuery_KabalaBeforePost
    BeforeDelete = AtmRxQuery_KabalaBeforeDelete
    BeforeScroll = AtmRxQuery_KabalaBeforeScroll
    AfterScroll = AtmRxQuery_KabalaAfterScroll
    DatabaseName = 'DB_Kupa'
    Filter = 'KabalaKind<>1'
    Filtered = True
    RequestLive = True
    SQL.Strings = (
      'Select * From Kabala'
      'Where YehusYear = :PYehusYear'
      'And %MFilter'
      'Order By %OrderFields')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_Kabala
    Macros = <
      item
        DataType = ftString
        Name = 'MFilter'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'OrderFields'
        ParamType = ptInput
        Value = 'YehusYear,KabalaNumber'
      end>
    IndexFieldNames = 'KabalaNumber'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = False
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = False
    Left = 34
    Top = 90
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end>
    object AtmRxQuery_KabalaYehusYear: TIntegerField
      FieldName = 'YehusYear'
      Origin = 'DB_KUPA."Kabala.DB".YehusYear'
    end
    object AtmRxQuery_KabalaKabalaNumber: TIntegerField
      FieldName = 'KabalaNumber'
      Origin = 'DB_KUPA."Kabala.DB".KabalaNumber'
    end
    object AtmRxQuery_KabalaKabalaKind: TIntegerField
      FieldName = 'KabalaKind'
      Origin = 'DB_KUPA."Kabala.DB".KabalaKind'
    end
    object AtmRxQuery_KabalaDateKabala: TDateTimeField
      FieldName = 'DateKabala'
      Origin = 'DB_KUPA."Kabala.DB".DateKabala'
      OnValidate = AtmRxQuery_KabalaDateKabalaValidate
    end
    object AtmRxQuery_KabalaCodeLakoach: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'CodeLakoach'
      Origin = 'DB_KUPA."Kabala.DB".CodeLakoach'
      OnValidate = AtmRxQuery_KabalaCodeLakoachValidate
    end
    object AtmRxQuery_KabalaShemLakoach: TStringField
      FieldName = 'ShemLakoach'
      Origin = 'DB_KUPA."Kabala.DB".ShemLakoach'
      Size = 30
    end
    object AtmRxQuery_KabalaKtovet: TStringField
      FieldName = 'Ktovet'
      Origin = 'DB_KUPA."Kabala.DB".Ktovet'
      Size = 30
    end
    object AtmRxQuery_KabalaHesbonChiuv: TStringField
      FieldName = 'HesbonChiuv'
      Origin = 'DB_KUPA."Kabala.DB".HesbonChiuv'
      Size = 10
    end
    object AtmRxQuery_KabalaHesbonZikuy: TStringField
      FieldName = 'HesbonZikuy'
      Origin = 'DB_KUPA."Kabala.DB".HesbonZikuy'
      Size = 10
    end
    object AtmRxQuery_KabalaHesbonMasMakor: TStringField
      FieldName = 'HesbonMasMakor'
      Origin = 'DB_KUPA."Kabala.DB".HesbonMasMakor'
      Size = 10
    end
    object AtmRxQuery_KabalaKodBItzua: TIntegerField
      FieldName = 'KodBItzua'
      Origin = 'DB_KUPA."Kabala.DB".KodBItzua'
    end
    object AtmRxQuery_KabalaMisparHavara: TIntegerField
      DefaultExpression = '0'
      FieldName = 'MisparHavara'
      Origin = 'DB_KUPA."Kabala.DB".MisparHavara'
    end
    object AtmRxQuery_KabalaTotalSumKabla: TBCDField
      DefaultExpression = '0'
      FieldName = 'TotalSumKabla'
      Origin = 'DB_KUPA."Kabala.DB".TotalSumKabla'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabalaPratim: TStringField
      FieldName = 'Pratim'
      Origin = 'DB_KUPA."Kabala.DB".Pratim'
      Size = 50
    end
    object AtmRxQuery_KabalaHaavaraBankait: TIntegerField
      FieldName = 'HaavaraBankait'
      Origin = 'DB_KUPA."Kabala.DB".HaavaraBankait'
    end
    object AtmRxQuery_KabalaShuraKopa: TIntegerField
      DefaultExpression = '0'
      FieldName = 'ShuraKopa'
      Origin = 'DB_KUPA."Kabala.DB".ShuraKopa'
    end
    object AtmRxQuery_KabalaCodeHaavaraToHan: TIntegerField
      DefaultExpression = '0'
      FieldName = 'CodeHaavaraToHan'
      Origin = 'DB_KUPA."Kabala.DB".CodeHaavaraToHan'
    end
    object AtmRxQuery_KabalaLastUpdate: TDateTimeField
      FieldName = 'LastUpdate'
      Origin = 'DB_KUPA."Kabala.DB".LastUpdate'
    end
    object AtmRxQuery_KabalaMaklidName: TStringField
      FieldName = 'MaklidName'
      Origin = 'DB_KUPA."Kabala.DB".MaklidName'
      Size = 15
    end
    object AtmRxQuery_KabalaTotalZikuy: TBCDField
      DefaultExpression = '0'
      FieldName = 'TotalZikuy'
      Origin = 'DB_KUPA."Kabala.DB".TotalZikuy'
      Precision = 32
      Size = 3
    end
  end
  object DS_Kabala: TDataSource
    DataSet = AtmRxQuery_Kabala
    Left = 32
    Top = 150
  end
  object UpdateSQL_Kabala: TUpdateSQL
    ModifySQL.Strings = (
      'update Kabala'
      'set'
      '  YehusYear = :YehusYear,'
      '  KabalaNumber = :KabalaNumber,'
      '  KabalaKind = :KabalaKind,'
      '  DateKabala = :DateKabala,'
      '  CodeLakoach = :CodeLakoach,'
      '  ShemLakoach = :ShemLakoach,'
      '  Ktovet = :Ktovet,'
      '  HesbonChiuv = :HesbonChiuv,'
      '  HesbonZikuy = :HesbonZikuy,'
      '  HesbonMasMakor = :HesbonMasMakor,'
      '  KodBItzua = :KodBItzua,'
      '  MisparHavara = :MisparHavara,'
      '  TotalSumKabla = :TotalSumKabla,'
      '  Pratim = :Pratim,'
      '  HaavaraBankait = :HaavaraBankait,'
      '  ShuraKopa = :ShuraKopa,'
      '  CodeHaavaraToHan = :CodeHaavaraToHan,'
      '  TotalZikuy = :TotalZikuy,'
      '  LastUpdate = :LastUpdate,'
      '  MaklidName = :MaklidName'
      'where'
      '  YehusYear = :OLD_YehusYear and'
      '  KabalaNumber = :OLD_KabalaNumber')
    InsertSQL.Strings = (
      'insert into Kabala'
      
        '  (YehusYear, KabalaNumber, KabalaKind, DateKabala, CodeLakoach,' +
        ' ShemLakoach, '
      
        '   Ktovet, HesbonChiuv, HesbonZikuy, HesbonMasMakor, KodBItzua, ' +
        'MisparHavara, '
      
        '   TotalSumKabla, Pratim, HaavaraBankait, ShuraKopa, CodeHaavara' +
        'ToHan, '
      '   TotalZikuy, LastUpdate, MaklidName)'
      'values'
      
        '  (:YehusYear, :KabalaNumber, :KabalaKind, :DateKabala, :CodeLak' +
        'oach, :ShemLakoach, '
      
        '   :Ktovet, :HesbonChiuv, :HesbonZikuy, :HesbonMasMakor, :KodBIt' +
        'zua, :MisparHavara, '
      
        '   :TotalSumKabla, :Pratim, :HaavaraBankait, :ShuraKopa, :CodeHa' +
        'avaraToHan, '
      '   :TotalZikuy, :LastUpdate, :MaklidName)')
    DeleteSQL.Strings = (
      'delete from Kabala'
      'where'
      '  YehusYear = :OLD_YehusYear and'
      '  KabalaNumber = :OLD_KabalaNumber')
    Left = 28
    Top = 210
  end
  object AtmRxQuery_Checks: TAtmRxQuery
    CachedUpdates = True
    AfterOpen = AtmRxQuery_ChecksAfterOpen
    AfterInsert = AtmRxQuery_ChecksAfterInsert
    BeforeEdit = AtmRxQuery_ChecksBeforeEdit
    BeforePost = AtmRxQuery_ChecksBeforePost
    AfterPost = AtmRxQuery_ChecksAfterPost
    BeforeDelete = AtmRxQuery_ChecksBeforeDelete
    AfterDelete = AtmRxQuery_ChecksAfterDelete
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From Checks'
      'Where KabalaNumber = :PKabalaNumber'
      '  AND KabalaYear =:PKabalaYear')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_Checks
    Macros = <>
    IndexAutoSort = False
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = False
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    DefaultValues.Strings = (
      'CodeSnifHafkada=0')
    GetOnlyOneRecord = False
    Left = 142
    Top = 94
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PKabalaNumber'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PKabalaYear'
        ParamType = ptUnknown
      end>
    object AtmRxQuery_ChecksTnua: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'Tnua'
      Origin = 'DB_KUPA."Checks.DB".Tnua'
    end
    object AtmRxQuery_ChecksKabalaYear: TIntegerField
      FieldName = 'KabalaYear'
      Origin = 'DB_KUPA."Checks.DB".KabalaYear'
    end
    object AtmRxQuery_ChecksKabalaNumber: TIntegerField
      FieldName = 'KabalaNumber'
      Origin = 'DB_KUPA."Checks.DB".KabalaNumber'
    end
    object AtmRxQuery_ChecksCodeSugTashlum: TIntegerField
      FieldName = 'CodeSugTashlum'
      Origin = 'DB_KUPA."Checks.DB".CodeSugTashlum'
      Required = True
      OnValidate = AtmRxQuery_ChecksCodeSugTashlumValidate
    end
    object AtmRxQuery_ChecksCheckNumber: TStringField
      FieldName = 'CheckNumber'
      Origin = 'DB_KUPA."Checks.DB".CheckNumber'
    end
    object AtmRxQuery_ChecksKodBank: TIntegerField
      DefaultExpression = '0'
      FieldName = 'KodBank'
      Origin = 'DB_KUPA."Checks.DB".KodBank'
      OnValidate = AtmRxQuery_ChecksKodBankValidate
    end
    object AtmRxQuery_ChecksSnifNumber: TIntegerField
      DefaultExpression = '0'
      FieldName = 'SnifNumber'
      Origin = 'DB_KUPA."Checks.DB".SnifNumber'
    end
    object AtmRxQuery_ChecksDatePiraon: TDateTimeField
      FieldName = 'DatePiraon'
      Origin = 'DB_KUPA."Checks.DB".DatePiraon'
      DisplayFormat = 'dd/mm/yyyy'
      EditMask = '!99/99/0000;1;_'
    end
    object AtmRxQuery_ChecksSchumCheck: TBCDField
      DefaultExpression = '0'
      FieldName = 'SchumCheck'
      Origin = 'DB_KUPA."Checks.DB".SchumCheck'
      MaxValue = 99999999
      MinValue = -99999999
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ChecksMisparCheshbon: TStringField
      FieldName = 'MisparCheshbon'
      Origin = 'DB_KUPA."Checks.DB".MisparCheshbon'
      Size = 10
    end
    object AtmRxQuery_ChecksKodMatbea: TIntegerField
      DefaultExpression = '0'
      FieldName = 'KodMatbea'
      Origin = 'DB_KUPA."Checks.DB".KodMatbea'
    end
    object AtmRxQuery_ChecksKamutShtarot: TIntegerField
      DefaultExpression = '0'
      FieldName = 'KamutShtarot'
      Origin = 'DB_KUPA."Checks.DB".KamutShtarot'
    end
    object AtmRxQuery_ChecksDateHamara: TDateTimeField
      FieldName = 'DateHamara'
      Origin = 'DB_KUPA."Checks.DB".DateHamara'
    end
    object AtmRxQuery_ChecksSchumDollar: TBCDField
      DefaultExpression = '0'
      FieldName = 'SchumDollar'
      Origin = 'DB_KUPA."Checks.DB".SchumDollar'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ChecksShuraKopa: TIntegerField
      DefaultExpression = '0'
      FieldName = 'ShuraKopa'
      Origin = 'DB_KUPA."Checks.DB".ShuraKopa'
    end
    object AtmRxQuery_ChecksStatus: TIntegerField
      DefaultExpression = '0'
      FieldName = 'Status'
      Origin = 'DB_KUPA."Checks.DB".Status'
    end
    object AtmRxQuery_ChecksSugStar: TIntegerField
      DefaultExpression = '0'
      FieldName = 'SugStar'
      Origin = 'DB_KUPA."Checks.DB".SugStar'
    end
    object AtmRxQuery_ChecksSchumMatbea: TBCDField
      DefaultExpression = '0'
      FieldName = 'SchumMatbea'
      Origin = 'DB_KUPA."Checks.DB".SchumMatbea'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ChecksMisparHafkada: TIntegerField
      DefaultExpression = '0'
      FieldName = 'MisparHafkada'
      Origin = 'DB_KUPA."Checks.DB".MisparHafkada'
    end
    object AtmRxQuery_ChecksLookSugTashlum: TStringField
      FieldKind = fkLookup
      FieldName = 'LookSugTashlum'
      KeyFields = 'CodeSugTashlum'
      LookupCache = True
      Lookup = True
    end
    object AtmRxQuery_ChecksCodeBankHafkada: TIntegerField
      DefaultExpression = '0'
      FieldName = 'CodeBankHafkada'
      Origin = 'DB_KUPA."Checks.DB".CodeBankHafkada'
    end
    object AtmRxQuery_ChecksMisHeshbonHafkada: TStringField
      FieldName = 'MisHeshbonHafkada'
      Origin = 'DB_KUPA."Checks.DB".MisHeshbonHafkada'
      Size = 10
    end
    object AtmRxQuery_ChecksCodeSnifHafkada: TIntegerField
      FieldName = 'CodeSnifHafkada'
      Origin = 'DB_KUPA."Checks.DB".CodeSnifHafkada'
    end
    object AtmRxQuery_ChecksNumberOfPayment: TIntegerField
      FieldName = 'NumberOfPayment'
      Origin = 'DB_KUPA."Checks.DB".NumberOfPayment'
    end
    object AtmRxQuery_ChecksCreditKind: TIntegerField
      DefaultExpression = '0'
      FieldName = 'CreditKind'
      Origin = 'DB_KUPA."Checks.DB".CreditKind'
    end
    object AtmRxQuery_ChecksValidMonth: TIntegerField
      FieldName = 'ValidMonth'
      Origin = 'DB_KUPA."Checks.DB".ValidMonth'
    end
    object AtmRxQuery_ChecksValidYear: TIntegerField
      FieldName = 'ValidYear'
      Origin = 'DB_KUPA."Checks.DB".ValidYear'
    end
    object AtmRxQuery_ChecksAprovalCode: TIntegerField
      DefaultExpression = '0'
      FieldName = 'AprovalCode'
      Origin = 'DB_KUPA."Checks.DB".AprovalCode'
    end
    object AtmRxQuery_ChecksPhoneNumber: TStringField
      FieldName = 'PhoneNumber'
      Origin = 'DB_KUPA."Checks.DB".PhoneNumber'
      Size = 12
    end
    object AtmRxQuery_ChecksLookShemBank: TStringField
      FieldKind = fkLookup
      FieldName = 'LookShemBank'
      KeyFields = 'KodBank'
      LookupCache = True
      Lookup = True
    end
  end
  object DS_Checks: TDataSource
    DataSet = AtmRxQuery_Checks
    Left = 146
    Top = 142
  end
  object UpdateSQL_Checks: TUpdateSQL
    ModifySQL.Strings = (
      'update Checks'
      'set'
      '  KabalaYear = :KabalaYear,'
      '  KabalaNumber = :KabalaNumber,'
      '  CodeSugTashlum = :CodeSugTashlum,'
      '  CheckNumber = :CheckNumber,'
      '  KodBank = :KodBank,'
      '  SnifNumber = :SnifNumber,'
      '  DatePiraon = :DatePiraon,'
      '  SchumCheck = :SchumCheck,'
      '  MisparCheshbon = :MisparCheshbon,'
      '  KodMatbea = :KodMatbea,'
      '  KamutShtarot = :KamutShtarot,'
      '  DateHamara = :DateHamara,'
      '  SchumDollar = :SchumDollar,'
      '  ShuraKopa = :ShuraKopa,'
      '  Status = :Status,'
      '  SugStar = :SugStar,'
      '  SchumMatbea = :SchumMatbea,'
      '  MisparHafkada = :MisparHafkada,'
      '  CodeBankHafkada = :CodeBankHafkada,'
      '  MisHeshbonHafkada = :MisHeshbonHafkada,'
      '  CodeSnifHafkada = :CodeSnifHafkada,'
      '  NumberOfPayment = :NumberOfPayment,'
      '  CreditKind = :CreditKind,'
      '  ValidMonth = :ValidMonth,'
      '  ValidYear = :ValidYear,'
      '  AprovalCode = :AprovalCode,'
      '  PhoneNumber = :PhoneNumber'
      'where'
      '  Tnua = :OLD_Tnua')
    InsertSQL.Strings = (
      'insert into Checks'
      
        '  (KabalaYear, KabalaNumber, CodeSugTashlum, CheckNumber, KodBan' +
        'k, SnifNumber, '
      
        '   DatePiraon, SchumCheck, MisparCheshbon, KodMatbea, KamutShtar' +
        'ot, DateHamara, '
      
        '   SchumDollar, ShuraKopa, Status, SugStar, SchumMatbea, MisparH' +
        'afkada, '
      
        '   CodeBankHafkada, MisHeshbonHafkada, CodeSnifHafkada, NumberOf' +
        'Payment, '
      '   CreditKind, ValidMonth, ValidYear, AprovalCode, PhoneNumber)'
      'values'
      
        '  (:KabalaYear, :KabalaNumber, :CodeSugTashlum, :CheckNumber, :K' +
        'odBank, '
      
        '   :SnifNumber, :DatePiraon, :SchumCheck, :MisparCheshbon, :KodM' +
        'atbea, '
      
        '   :KamutShtarot, :DateHamara, :SchumDollar, :ShuraKopa, :Status' +
        ', :SugStar, '
      
        '   :SchumMatbea, :MisparHafkada, :CodeBankHafkada, :MisHeshbonHa' +
        'fkada, '
      
        '   :CodeSnifHafkada, :NumberOfPayment, :CreditKind, :ValidMonth,' +
        ' :ValidYear, '
      '   :AprovalCode, :PhoneNumber)')
    DeleteSQL.Strings = (
      'delete from Checks'
      'where'
      '  Tnua = :OLD_Tnua')
    Left = 146
    Top = 198
  end
  object RxQuery_HeshForLak: TRxQuery
    AfterPost = RxQuery_HeshForLakAfterPost
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      
        'SELECT HesbonitNumber, YehusYear, HesbonitKind, CodeLakoach, Lak' +
        'oachGroup, HesbonitDate, TotalHesbonitWithMAM, Total_Zikuy, Tota' +
        'lHesbonitWithMAM-Total_Zikuy Itra'
      'FROM Makav'
      'Where %MWhereLak'
      '   And (TotalHesbonitWithMAM <> Total_Zikuy) '
      '   And Status = 0'
      '   And HesbonitKind In (1,3,7,8,9,12)'
      'Order by HesbonitNumber')
    Macros = <
      item
        DataType = ftString
        Name = 'MWhereLak'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 248
    Top = 100
    object RxQuery_HeshForLakHesbonitNumber: TIntegerField
      FieldName = 'HesbonitNumber'
      Origin = 'DB_KUPA."Makav.DB".HesbonitNumber'
    end
    object RxQuery_HeshForLakCodeLakoach: TIntegerField
      FieldName = 'CodeLakoach'
      Origin = 'DB_KUPA."Makav.DB".CodeLakoach'
    end
    object RxQuery_HeshForLakLakoachGroup: TIntegerField
      FieldName = 'LakoachGroup'
      Origin = 'DB_KUPA."Makav.DB".LakoachGroup'
    end
    object RxQuery_HeshForLakHesbonitDate: TDateTimeField
      FieldName = 'HesbonitDate'
      Origin = 'DB_KUPA."Makav.DB".HesbonitDate'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object RxQuery_HeshForLakTotalHesbonitWithMAM: TBCDField
      FieldName = 'TotalHesbonitWithMAM'
      Origin = 'DB_KUPA."Makav.DB".TotalHesbonitWithMAM'
      Precision = 32
      Size = 3
    end
    object RxQuery_HeshForLakYehusYear: TIntegerField
      FieldName = 'YehusYear'
      Origin = 'DB_KUPA."Makav.DB".YehusYear'
    end
    object RxQuery_HeshForLakHesbonitKind: TIntegerField
      FieldName = 'HesbonitKind'
      Origin = 'DB_KUPA."Makav.DB".HesbonitKind'
    end
    object RxQuery_HeshForLakTotal_Zikuy: TBCDField
      FieldName = 'Total_Zikuy'
      Origin = 'DB_KUPA."Makav.DB".Total_Zikuy'
      Precision = 32
      Size = 3
    end
    object RxQuery_HeshForLakItra: TBCDField
      FieldName = 'Itra'
      Origin = 'DB_KUPA."Makav.DB".TotalHesbonitWithMAM'
      Precision = 32
      Size = 3
    end
  end
  object DS_HeshForLak: TDataSource
    DataSet = RxQuery_HeshForLak
    Left = 250
    Top = 150
  end
  object Qry_AtmIndexKabala: TQuery
    CachedUpdates = True
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From AtmIndex'
      'Where Key_String = '#39'Kabala'#39
      'And YehusYear=:PYehusYear')
    UniDirectional = True
    UpdateObject = UpdateSQL_AtmIndex
    Left = 342
    Top = 25
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end>
  end
  object UpdateSQL_AtmIndex: TUpdateSQL
    ModifySQL.Strings = (
      'update AtmIndex'
      'set'
      '  Key_String = :Key_String,'
      '  YehusYear = :YehusYear,'
      '  Last_Number = :Last_Number,'
      '  LastUpDate = :LastUpDate,'
      '  MaklidName = :MaklidName'
      'where'
      '  Key_String = :OLD_Key_String and'
      '  YehusYear = :OLD_YehusYear')
    InsertSQL.Strings = (
      'insert into AtmIndex'
      '  (Key_String, YehusYear, Last_Number, LastUpDate, MaklidName)'
      'values'
      
        '  (:Key_String, :YehusYear, :Last_Number, :LastUpDate, :MaklidNa' +
        'me)')
    DeleteSQL.Strings = (
      'delete from AtmIndex'
      'where'
      '  Key_String = :OLD_Key_String and'
      '  YehusYear = :OLD_YehusYear')
    Left = 342
    Top = 76
  end
  object AtmRxQuery_KabHes: TAtmRxQuery
    CachedUpdates = True
    BeforeEdit = AtmRxQuery_KabHesBeforeEdit
    BeforePost = AtmRxQuery_KabHesBeforePost
    AfterPost = AtmRxQuery_KabHesAfterPost
    BeforeDelete = AtmRxQuery_KabHesBeforeEdit
    AfterDelete = AtmRxQuery_KabHesAfterPost
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From Kabhes'
      'Where KabalaNumber = :PKabalaNumber'
      'And KabalaYear = :PKabalaYear')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_KabHes
    Macros = <>
    IndexFieldNames = 'KabalaNumber'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = False
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = False
    Left = 340
    Top = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PKabalaNumber'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PKabalaYear'
        ParamType = ptUnknown
      end>
    object AtmRxQuery_KabHesTnua: TIntegerField
      FieldName = 'Tnua'
      Origin = 'DB_KUPA."Kabhes.DB".Tnua'
    end
    object AtmRxQuery_KabHesKabalaNumber: TIntegerField
      FieldName = 'KabalaNumber'
      Origin = 'DB_KUPA."Kabhes.DB".KabalaNumber'
    end
    object AtmRxQuery_KabHesKabalaYear: TIntegerField
      FieldName = 'KabalaYear'
      Origin = 'DB_KUPA."Kabhes.DB".KabalaYear'
    end
    object AtmRxQuery_KabHesSchumHesbonit: TBCDField
      DefaultExpression = '0'
      FieldName = 'SchumHesbonit'
      Origin = 'DB_KUPA."Kabhes.DB".SchumHesbonit'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesDateHesbonit: TDateTimeField
      FieldName = 'DateHesbonit'
      Origin = 'DB_KUPA."Kabhes.DB".DateHesbonit'
    end
    object AtmRxQuery_KabHesSchumTashlom: TBCDField
      DefaultExpression = '0'
      FieldName = 'SchumTashlom'
      Origin = 'DB_KUPA."Kabhes.DB".SchumTashlom'
      OnValidate = AtmRxQuery_KabHesSchumTashlomValidate
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesDateKabala: TDateTimeField
      FieldName = 'DateKabala'
      Origin = 'DB_KUPA."Kabhes.DB".DateKabala'
    end
    object AtmRxQuery_KabHesCodeLakoach: TIntegerField
      DefaultExpression = '0'
      FieldName = 'CodeLakoach'
      Origin = 'DB_KUPA."Kabhes.DB".CodeLakoach'
    end
    object AtmRxQuery_KabHesShemLakoach: TStringField
      FieldName = 'ShemLakoach'
      Origin = 'DB_KUPA."Kabhes.DB".ShemLakoach'
      Size = 30
    end
    object AtmRxQuery_KabHesHesbonitNumber: TIntegerField
      DefaultExpression = '0'
      FieldName = 'HesbonitNumber'
      Origin = 'DB_KUPA."Kabhes.DB".HesbonitNumber'
    end
    object AtmRxQuery_KabHesLakoachGroup: TIntegerField
      DefaultExpression = '0'
      FieldName = 'LakoachGroup'
      Origin = 'DB_KUPA."Kabhes.DB".LakoachGroup'
    end
    object AtmRxQuery_KabHesHesbonitYear: TIntegerField
      DefaultExpression = '0'
      FieldName = 'HesbonitYear'
      Origin = 'DB_KUPA."Kabhes.DB".HesbonitYear'
    end
    object AtmRxQuery_KabHesHesbonitKind: TIntegerField
      DefaultExpression = '1'
      FieldName = 'HesbonitKind'
      Origin = 'DB_KUPA."Kabhes.DB".HesbonitKind'
    end
    object AtmRxQuery_KabHesCodeBitzua: TIntegerField
      FieldName = 'CodeBitzua'
      Origin = 'DB_KUPA."Kabhes.DB".CodeBitzua'
    end
  end
  object DS_KabHes: TDataSource
    DataSet = AtmRxQuery_KabHes
    Left = 338
    Top = 188
  end
  object UpdateSQL_KabHes: TUpdateSQL
    ModifySQL.Strings = (
      'update Kabhes'
      'set'
      '  KabalaNumber = :KabalaNumber,'
      '  KabalaYear = :KabalaYear,'
      '  SchumHesbonit = :SchumHesbonit,'
      '  DateHesbonit = :DateHesbonit,'
      '  SchumTashlom = :SchumTashlom,'
      '  DateKabala = :DateKabala,'
      '  CodeLakoach = :CodeLakoach,'
      '  ShemLakoach = :ShemLakoach,'
      '  HesbonitNumber = :HesbonitNumber,'
      '  HesbonitYear = :HesbonitYear,'
      '  HesbonitKind = :HesbonitKind,'
      '  LakoachGroup = :LakoachGroup,'
      '  CodeBitzua = :CodeBitzua'
      'where'
      '  Tnua = :OLD_Tnua')
    InsertSQL.Strings = (
      'insert into Kabhes'
      
        '  (KabalaNumber, KabalaYear, SchumHesbonit, DateHesbonit, SchumT' +
        'ashlom, '
      
        '   DateKabala, CodeLakoach, ShemLakoach, HesbonitNumber, Hesboni' +
        'tYear, '
      '   HesbonitKind, LakoachGroup, CodeBitzua)'
      'values'
      
        '  (:KabalaNumber, :KabalaYear, :SchumHesbonit, :DateHesbonit, :S' +
        'chumTashlom, '
      
        '   :DateKabala, :CodeLakoach, :ShemLakoach, :HesbonitNumber, :He' +
        'sbonitYear, '
      '   :HesbonitKind, :LakoachGroup, :CodeBitzua)')
    DeleteSQL.Strings = (
      'delete from Kabhes'
      'where'
      '  Tnua = :OLD_Tnua')
    Left = 338
    Top = 234
  end
  object RxQuery_MakavHesbon: TRxQuery
    CachedUpdates = True
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'SELECT * FROM Makav'
      'WHERE   (HesbonitNumber = :PHesbonitNumber)  '
      '   AND  (YehusYear = :PYehusYear)  '
      '   AND  (HesbonitKind = :PHesbonitKind)  ')
    UniDirectional = True
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_Makav
    Macros = <>
    Left = 242
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PHesbonitNumber'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PHesbonitKind'
        ParamType = ptUnknown
      end>
  end
  object UpdateSQL_Makav: TUpdateSQL
    ModifySQL.Strings = (
      'update Makav'
      'set'
      '  CodeLakoach = :CodeLakoach,'
      '  Merakez = :Merakez,'
      '  LakoachGroup = :LakoachGroup,'
      '  HesbonitDate = :HesbonitDate,'
      '  TotalHesbonitWithMAM = :TotalHesbonitWithMAM,'
      '  MAMHesbonit = :MAMHesbonit,'
      '  MAMPercent = :MAMPercent,'
      '  MAMRound = :MAMRound,'
      '  DiscountWork = :DiscountWork,'
      '  DiscountParts = :DiscountParts,'
      '  Achuz_Anacha_Lakoach = :Achuz_Anacha_Lakoach,'
      '  Achuz_Anacha_Klali = :Achuz_Anacha_Klali,'
      '  PratimToHan = :PratimToHan,'
      '  CodeHaavaraToHan = :CodeHaavaraToHan,'
      '  MaklidName = :MaklidName,'
      '  DateHafakatHeshbonit = :DateHafakatHeshbonit,'
      '  CodeMeasher = :CodeMeasher,'
      '  SumPaied = :SumPaied,'
      '  HanForMam = :HanForMam,'
      '  HanForLak = :HanForLak,'
      '  YehusMonth = :YehusMonth,'
      '  PaymentCondition = :PaymentCondition,'
      '  Status = :Status,'
      '  NumberOfPayments = :NumberOfPayments,'
      '  DateForFirstPayment = :DateForFirstPayment,'
      '  OrderNum = :OrderNum,'
      '  TotalSumFromTnua = :TotalSumFromTnua,'
      '  TotalSumPriceKamut = :TotalSumPriceKamut,'
      '  DatePiraon = :DatePiraon,'
      '  KvutzaLeChiyuv = :KvutzaLeChiyuv,'
      '  MisHaavaraToHan = :MisHaavaraToHan,'
      '  Date_Havara = :Date_Havara,'
      '  Total_Zikuy = :Total_Zikuy,'
      '  Date_Zikuy = :Date_Zikuy,'
      '  Mis_Kabala1 = :Mis_Kabala1,'
      '  Mis_Kabala2 = :Mis_Kabala2,'
      '  Mis_Kabala3 = :Mis_Kabala3,'
      '  Mis_Kabala4 = :Mis_Kabala4,'
      '  Mis_Kabala5 = :Mis_Kabala5,'
      '  Date_Kabala1 = :Date_Kabala1,'
      '  Date_Kabala2 = :Date_Kabala2,'
      '  Date_Kabala3 = :Date_Kabala3,'
      '  Date_Kabala4 = :Date_Kabala4,'
      '  Date_Kabala5 = :Date_Kabala5,'
      '  Incom_Type = :Incom_Type,'
      '  Last_Update = :Last_Update,'
      '  CheckNum = :CheckNum'
      'where'
      '  HesbonitKind = :OLD_HesbonitKind and'
      '  YehusYear = :OLD_YehusYear and'
      '  HesbonitNumber = :OLD_HesbonitNumber')
    InsertSQL.Strings = (
      'insert into Makav'
      
        '  (CodeLakoach, Merakez, LakoachGroup, HesbonitDate, TotalHesbon' +
        'itWithMAM, '
      
        '   MAMHesbonit, MAMPercent, MAMRound, DiscountWork, DiscountPart' +
        's, Achuz_Anacha_Lakoach, '
      
        '   Achuz_Anacha_Klali, PratimToHan, CodeHaavaraToHan, MaklidName' +
        ', DateHafakatHeshbonit, '
      
        '   CodeMeasher, SumPaied, HanForMam, HanForLak, YehusMonth, Paym' +
        'entCondition, '
      
        '   Status, NumberOfPayments, DateForFirstPayment, OrderNum, Tota' +
        'lSumFromTnua, '
      
        '   TotalSumPriceKamut, DatePiraon, KvutzaLeChiyuv, MisHaavaraToH' +
        'an, Date_Havara, '
      
        '   Total_Zikuy, Date_Zikuy, Mis_Kabala1, Mis_Kabala2, Mis_Kabala' +
        '3, Mis_Kabala4, '
      
        '   Mis_Kabala5, Date_Kabala1, Date_Kabala2, Date_Kabala3, Date_K' +
        'abala4, '
      '   Date_Kabala5, Incom_Type, Last_Update, CheckNum)'
      'values'
      
        '  (:CodeLakoach, :Merakez, :LakoachGroup, :HesbonitDate, :TotalH' +
        'esbonitWithMAM, '
      
        '   :MAMHesbonit, :MAMPercent, :MAMRound, :DiscountWork, :Discoun' +
        'tParts, '
      
        '   :Achuz_Anacha_Lakoach, :Achuz_Anacha_Klali, :PratimToHan, :Co' +
        'deHaavaraToHan, '
      
        '   :MaklidName, :DateHafakatHeshbonit, :CodeMeasher, :SumPaied, ' +
        ':HanForMam, '
      
        '   :HanForLak, :YehusMonth, :PaymentCondition, :Status, :NumberO' +
        'fPayments, '
      
        '   :DateForFirstPayment, :OrderNum, :TotalSumFromTnua, :TotalSum' +
        'PriceKamut, '
      
        '   :DatePiraon, :KvutzaLeChiyuv, :MisHaavaraToHan, :Date_Havara,' +
        ' :Total_Zikuy, '
      
        '   :Date_Zikuy, :Mis_Kabala1, :Mis_Kabala2, :Mis_Kabala3, :Mis_K' +
        'abala4, '
      
        '   :Mis_Kabala5, :Date_Kabala1, :Date_Kabala2, :Date_Kabala3, :D' +
        'ate_Kabala4, '
      '   :Date_Kabala5, :Incom_Type, :Last_Update, :CheckNum)')
    DeleteSQL.Strings = (
      'delete from Makav'
      'where'
      '  HesbonitKind = :OLD_HesbonitKind and'
      '  YehusYear = :OLD_YehusYear and'
      '  HesbonitNumber = :OLD_HesbonitNumber')
    Left = 352
    Top = 282
  end
  object Qry_Mname: TQuery
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'Select * From MName')
    UpdateMode = upWhereChanged
    Left = 159
    Top = 19
  end
  object Query_InsertKupa: TQuery
    CachedUpdates = True
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'Select * from kopa'
      'Where MisparShura = -1')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_InsertKupa
    Left = 28
    Top = 262
  end
  object UpdateSQL_InsertKupa: TUpdateSQL
    ModifySQL.Strings = (
      'update kopa'
      'set'
      '  Misparkabala = :Misparkabala,'
      '  TarichKabala = :TarichKabala,'
      '  CodeSugTashlum = :CodeSugTashlum,'
      '  MisparCheck = :MisparCheck,'
      '  KodBank = :KodBank,'
      '  Snif = :Snif,'
      '  TarichPeraon = :TarichPeraon,'
      '  Schom = :Schom,'
      '  MisparChesbon = :MisparChesbon,'
      '  KodMatbea = :KodMatbea,'
      '  TarichHafkada = :TarichHafkada,'
      '  Ipyun = :Ipyun,'
      '  ChesbonBankHafkada = :ChesbonBankHafkada,'
      '  KodBankHafkada = :KodBankHafkada,'
      '  SnifHafkada = :SnifHafkada,'
      '  MisparHafkada = :MisparHafkada,'
      '  SugHafkada = :SugHafkada,'
      '  CodeLakoach = :CodeLakoach,'
      '  KodBitzua = :KodBitzua,'
      '  MisHaavaraToHan = :MisHaavaraToHan,'
      '  CodeHaavaraToHan = :CodeHaavaraToHan,'
      '  YehusYear = :YehusYear'
      'where'
      '  MisparShura = :OLD_MisparShura')
    InsertSQL.Strings = (
      'insert into kopa'
      
        '  (Misparkabala, TarichKabala, CodeSugTashlum, MisparCheck, KodB' +
        'ank, Snif, '
      
        '   TarichPeraon, Schom, MisparChesbon, KodMatbea, TarichHafkada,' +
        ' Ipyun, '
      
        '   ChesbonBankHafkada, KodBankHafkada, SnifHafkada, MisparHafkad' +
        'a, SugHafkada, '
      
        '   CodeLakoach, KodBitzua, MisHaavaraToHan, CodeHaavaraToHan, Ye' +
        'husYear)'
      'values'
      
        '  (:Misparkabala, :TarichKabala, :CodeSugTashlum, :MisparCheck, ' +
        ':KodBank, '
      
        '   :Snif, :TarichPeraon, :Schom, :MisparChesbon, :KodMatbea, :Ta' +
        'richHafkada, '
      
        '   :Ipyun, :ChesbonBankHafkada, :KodBankHafkada, :SnifHafkada, :' +
        'MisparHafkada, '
      
        '   :SugHafkada, :CodeLakoach, :KodBitzua, :MisHaavaraToHan, :Cod' +
        'eHaavaraToHan, '
      '   :YehusYear)')
    DeleteSQL.Strings = (
      'delete from kopa'
      'where'
      '  MisparShura = :OLD_MisparShura')
    Left = 118
    Top = 264
  end
  object UpdateSQL_MakavNhg: TUpdateSQL
    ModifySQL.Strings = (
      'update MakavNhg'
      'set'
      '  CodeLakoach = :CodeLakoach,'
      '  Merakez = :Merakez,'
      '  LakoachGroup = :LakoachGroup,'
      '  HesbonitDate = :HesbonitDate,'
      '  TotalHesbonitWithMAM = :TotalHesbonitWithMAM,'
      '  MAMHesbonit = :MAMHesbonit,'
      '  MAMPercent = :MAMPercent,'
      '  MAMRound = :MAMRound,'
      '  DiscountWork = :DiscountWork,'
      '  DiscountParts = :DiscountParts,'
      '  Achuz_Anacha_Lakoach = :Achuz_Anacha_Lakoach,'
      '  Achuz_Anacha_Klali = :Achuz_Anacha_Klali,'
      '  PratimToHan = :PratimToHan,'
      '  CodeHaavaraToHan = :CodeHaavaraToHan,'
      '  MaklidName = :MaklidName,'
      '  DateHafakatHeshbonit = :DateHafakatHeshbonit,'
      '  CodeMeasher = :CodeMeasher,'
      '  SumPaied = :SumPaied,'
      '  HanForMam = :HanForMam,'
      '  HanForLak = :HanForLak,'
      '  YehusMonth = :YehusMonth,'
      '  PaymentCondition = :PaymentCondition,'
      '  Status = :Status,'
      '  NumberOfPayments = :NumberOfPayments,'
      '  DateForFirstPayment = :DateForFirstPayment,'
      '  OrderNum = :OrderNum,'
      '  TotalSumFromTnua = :TotalSumFromTnua,'
      '  TotalSumPriceKamut = :TotalSumPriceKamut,'
      '  DatePiraon = :DatePiraon,'
      '  KvutzaLeChiyuv = :KvutzaLeChiyuv,'
      '  MisHaavaraToHan = :MisHaavaraToHan,'
      '  Date_Havara = :Date_Havara,'
      '  Total_Zikuy = :Total_Zikuy,'
      '  Date_Zikuy = :Date_Zikuy,'
      '  Mis_Kabala1 = :Mis_Kabala1,'
      '  Mis_Kabala2 = :Mis_Kabala2,'
      '  Mis_Kabala3 = :Mis_Kabala3,'
      '  Mis_Kabala4 = :Mis_Kabala4,'
      '  Mis_Kabala5 = :Mis_Kabala5,'
      '  Date_Kabala1 = :Date_Kabala1,'
      '  Date_Kabala2 = :Date_Kabala2,'
      '  Date_Kabala3 = :Date_Kabala3,'
      '  Date_Kabala4 = :Date_Kabala4,'
      '  Date_Kabala5 = :Date_Kabala5,'
      '  Incom_Type = :Incom_Type,'
      '  Last_Update = :Last_Update,'
      '  CheckNum = :CheckNum'
      'where'
      '  HesbonitKind = :OLD_HesbonitKind and'
      '  YehusYear = :OLD_YehusYear and'
      '  HesbonitNumber = :OLD_HesbonitNumber')
    InsertSQL.Strings = (
      'insert into MakavNhg'
      
        '  (CodeLakoach, Merakez, LakoachGroup, HesbonitDate, TotalHesbon' +
        'itWithMAM, '
      
        '   MAMHesbonit, MAMPercent, MAMRound, DiscountWork, DiscountPart' +
        's, Achuz_Anacha_Lakoach, '
      
        '   Achuz_Anacha_Klali, PratimToHan, CodeHaavaraToHan, MaklidName' +
        ', DateHafakatHeshbonit, '
      
        '   CodeMeasher, SumPaied, HanForMam, HanForLak, YehusMonth, Paym' +
        'entCondition, '
      
        '   Status, NumberOfPayments, DateForFirstPayment, OrderNum, Tota' +
        'lSumFromTnua, '
      
        '   TotalSumPriceKamut, DatePiraon, KvutzaLeChiyuv, MisHaavaraToH' +
        'an, Date_Havara, '
      
        '   Total_Zikuy, Date_Zikuy, Mis_Kabala1, Mis_Kabala2, Mis_Kabala' +
        '3, Mis_Kabala4, '
      
        '   Mis_Kabala5, Date_Kabala1, Date_Kabala2, Date_Kabala3, Date_K' +
        'abala4, '
      '   Date_Kabala5, Incom_Type, Last_Update, CheckNum)'
      'values'
      
        '  (:CodeLakoach, :Merakez, :LakoachGroup, :HesbonitDate, :TotalH' +
        'esbonitWithMAM, '
      
        '   :MAMHesbonit, :MAMPercent, :MAMRound, :DiscountWork, :Discoun' +
        'tParts, '
      
        '   :Achuz_Anacha_Lakoach, :Achuz_Anacha_Klali, :PratimToHan, :Co' +
        'deHaavaraToHan, '
      
        '   :MaklidName, :DateHafakatHeshbonit, :CodeMeasher, :SumPaied, ' +
        ':HanForMam, '
      
        '   :HanForLak, :YehusMonth, :PaymentCondition, :Status, :NumberO' +
        'fPayments, '
      
        '   :DateForFirstPayment, :OrderNum, :TotalSumFromTnua, :TotalSum' +
        'PriceKamut, '
      
        '   :DatePiraon, :KvutzaLeChiyuv, :MisHaavaraToHan, :Date_Havara,' +
        ' :Total_Zikuy, '
      
        '   :Date_Zikuy, :Mis_Kabala1, :Mis_Kabala2, :Mis_Kabala3, :Mis_K' +
        'abala4, '
      
        '   :Mis_Kabala5, :Date_Kabala1, :Date_Kabala2, :Date_Kabala3, :D' +
        'ate_Kabala4, '
      '   :Date_Kabala5, :Incom_Type, :Last_Update, :CheckNum)')
    DeleteSQL.Strings = (
      'delete from MakavNhg'
      'where'
      '  HesbonitKind = :OLD_HesbonitKind and'
      '  YehusYear = :OLD_YehusYear and'
      '  HesbonitNumber = :OLD_HesbonitNumber')
    Left = 224
    Top = 274
  end
end

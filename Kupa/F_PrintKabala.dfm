object Frm_PrintKabala: TFrm_PrintKabala
  Left = 0
  Top = 45
  Width = 640
  Height = 397
  HorzScrollBar.Position = 203
  VertScrollBar.Position = 290
  BiDiMode = bdRightToLeft
  Caption = '����� ����'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QuickRep1: TQuickRep
    Left = -33
    Top = -290
    Width = 794
    Height = 1123
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    AfterPrint = QuickRep1AfterPrint
    BeforePrint = QuickRep1BeforePrint
    DataSet = DM_Kupa.AtmRxQuery_Checks
    Font.Charset = HEBREW_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'David'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Values = (
      100
      2970
      100
      2100
      100
      100
      0)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = First
    PrintIfEmpty = False
    SnapToGrid = True
    Units = MM
    Zoom = 100
    object QRBand2: TQRBand
      Left = 38
      Top = 340
      Width = 718
      Height = 153
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      BeforePrint = QRBand2BeforePrint
      Color = clWhite
      Font.Charset = HEBREW_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'David'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        404.8125
        1899.70833333333)
      BandType = rbTitle
      object QRLabel2: TQRLabel
        Left = 606
        Top = 10
        Width = 95
        Height = 31
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          82.0208333333333
          1603.375
          26.4583333333333
          251.354166666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '����� ���'
        Color = clSilver
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object TQRLabel
        Left = 606
        Top = 38
        Width = 95
        Height = 28
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          74.0833333333333
          1603.375
          100.541666666667
          251.354166666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�����'
        Color = clSilver
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel_AddressLak: TQRLabel
        Left = 396
        Top = 42
        Width = 184
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.1458333333333
          1047.75
          111.125
          486.833333333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'QRLabel_AddressLak'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText7: TQRDBText
        Left = 529
        Top = 72
        Width = 51
        Height = 21
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          55.5625
          1399.64583333333
          190.5
          134.9375)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'Pratim'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel9: TQRLabel
        Left = 150
        Top = 5
        Width = 48
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.1458333333333
          396.875
          13.2291666666667
          127)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText10: TQRDBText
        Left = 7
        Top = 5
        Width = 128
        Height = 23
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          60.8541666666667
          18.5208333333333
          13.2291666666667
          338.666666666667)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'DateKabala'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText11: TQRDBText
        Left = 458
        Top = 10
        Width = 122
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.1458333333333
          1211.79166666667
          26.4583333333333
          322.791666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'ShemLakoach'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText5: TQRDBText
        Left = 9
        Top = 136
        Width = 108
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.1458333333333
          23.8125
          359.833333333333
          285.75)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'HesbonMasMakor'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OnPrint = QRLabel8Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel20: TQRLabel
        Left = 606
        Top = 92
        Width = 95
        Height = 26
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          68.7916666666667
          1603.375
          243.416666666667
          251.354166666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� '
        Color = clSilver
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel21: TQRLabel
        Left = 530
        Top = 97
        Width = 50
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1402.29166666667
          256.645833333333
          132.291666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRLabel21Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel10: TQRLabel
        Left = 606
        Top = 66
        Width = 95
        Height = 26
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          68.7916666666667
          1603.375
          174.625
          251.354166666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�����'
        Color = clSilver
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
    end
    object QRBand5: TQRBand
      Left = 38
      Top = 688
      Width = 718
      Height = 107
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        283.104166666667
        1899.70833333333)
      BandType = rbSummary
      object QRDBText8: TQRDBText
        Left = 3
        Top = 6
        Width = 92
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          7.9375
          15.875
          243.416666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'TotalSumKabla'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = [fsBold]
        OnPrint = QRDBText8Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel3: TQRLabel
        Left = 106
        Top = 6
        Width = 45
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          280.458333333333
          15.875
          119.0625)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '��"�'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRShape13: TQRShape
        Left = 98
        Top = -2
        Width = 8
        Height = 31
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          82.0208333333333
          259.291666666667
          -5.29166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape15: TQRShape
        Left = 3
        Top = 28
        Width = 102
        Height = 1
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          2.64583333333333
          7.9375
          74.0833333333333
          269.875)
        Shape = qrsHorLine
      end
      object QRShape17: TQRShape
        Left = -3
        Top = -2
        Width = 8
        Height = 31
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          82.0208333333333
          -7.9375
          -5.29166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape12: TQRShape
        Left = 0
        Top = 0
        Width = 717
        Height = 5
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          13.2291666666667
          0
          0
          1897.0625)
        Shape = qrsHorLine
      end
      object QRLabel19: TQRLabel
        Left = 150
        Top = 58
        Width = 57
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          396.875
          153.458333333333
          150.8125)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel33: TQRLabel
        Left = 8
        Top = 84
        Width = 197
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          21.1666666666667
          222.25
          521.229166666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'QRLabel33'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel6: TQRLabel
        Left = 616
        Top = 50
        Width = 80
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1629.83333333333
          132.291666666667
          211.666666666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�-� �����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText9: TQRDBText
        Left = 509
        Top = 50
        Width = 95
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1346.72916666667
          132.291666666667
          251.354166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'HesbonChiuv'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText26: TQRDBText
        Left = 509
        Top = 76
        Width = 95
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1346.72916666667
          201.083333333333
          251.354166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'HesbonZikuy'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel7: TQRLabel
        Left = 617
        Top = 76
        Width = 79
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1632.47916666667
          201.083333333333
          209.020833333333)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '�-� ������'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel8: TQRLabel
        Left = 616
        Top = 23
        Width = 80
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1629.83333333333
          60.8541666666667
          211.666666666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = ' ��'#39' ����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText2: TQRDBText
        Left = 509
        Top = 23
        Width = 95
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1346.72916666667
          60.8541666666667
          251.354166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'CodeLakoach'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
    end
    object QRBand6: TQRBand
      Left = 38
      Top = 795
      Width = 718
      Height = 34
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        89.9583333333333
        1899.70833333333)
      BandType = rbPageFooter
      object QRLabel4: TQRLabel
        Left = 375
        Top = 23
        Width = 339
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          34.3958333333333
          992.1875
          60.8541666666667
          896.9375)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 
          '���� �"� ����� ���� 2000, ���� ����� ��"� 03-6898000, www.atm.co' +
          '.il'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
    end
    object QRSubDetail1: TQRSubDetail
      Left = 38
      Top = 533
      Width = 718
      Height = 5
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      BeforePrint = QRSubDetail1BeforePrint
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        13.2291666666667
        1899.70833333333)
      Master = QuickRep1
      DataSet = DM_Kupa.AtmRxQuery_KabHes
      FooterBand = GroupFooterBand1
      HeaderBand = GroupHeaderBand1
      PrintBefore = False
      PrintIfEmpty = True
    end
    object GroupHeaderBand1: TQRBand
      Left = 38
      Top = 493
      Width = 718
      Height = 40
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      BeforePrint = GroupHeaderBand1BeforePrint
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        105.833333333333
        1899.70833333333)
      BandType = rbGroupHeader
      object QRShape36: TQRShape
        Left = 1
        Top = 12
        Width = 718
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          2.64583333333333
          31.75
          1899.70833333333)
        Brush.Color = clSilver
        Shape = qrsRectangle
      end
      object QRLabel28: TQRLabel
        Left = 220
        Top = 16
        Width = 99
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          582.083333333333
          42.3333333333333
          261.9375)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� ������'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel29: TQRLabel
        Left = 358
        Top = 18
        Width = 88
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          947.208333333333
          47.625
          232.833333333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� �����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel30: TQRLabel
        Left = 477
        Top = 18
        Width = 104
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1262.0625
          47.625
          275.166666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� �������'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel31: TQRLabel
        Left = 616
        Top = 18
        Width = 92
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1629.83333333333
          47.625
          243.416666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '��'#39' �������'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRShape37: TQRShape
        Left = 1
        Top = 21
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          2.64583333333333
          55.5625
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRShape38: TQRShape
        Left = 344
        Top = 13
        Width = 1
        Height = 27
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          71.4375
          910.166666666667
          34.3958333333333
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRShape39: TQRShape
        Left = 462
        Top = 13
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          1222.375
          34.3958333333333
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRShape40: TQRShape
        Left = 597
        Top = 12
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          1579.5625
          31.75
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRShape41: TQRShape
        Left = 716
        Top = 12
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          1894.41666666667
          31.75
          2.64583333333333)
        Shape = qrsRectangle
      end
    end
    object ChildBand1: TQRChildBand
      Left = 38
      Top = 538
      Width = 718
      Height = 19
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      BeforePrint = ChildBand1BeforePrint
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        50.2708333333333
        1899.70833333333)
      ParentBand = QRSubDetail1
      object QRDBText16: TQRDBText
        Left = 615
        Top = 3
        Width = 86
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1627.1875
          7.9375
          227.541666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_KabHes
        DataField = 'HesbonitNumber'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText17: TQRDBText
        Left = 473
        Top = 3
        Width = 115
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1251.47916666667
          7.9375
          304.270833333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_KabHes
        DataField = 'SchumHesbonit'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText17Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRShape22: TQRShape
        Left = 597
        Top = 0
        Width = 1
        Height = 27
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          71.4375
          1579.5625
          0
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRShape23: TQRShape
        Left = 716
        Top = 0
        Width = 1
        Height = 27
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          71.4375
          1894.41666666667
          0
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRShape24: TQRShape
        Left = 462
        Top = 0
        Width = 1
        Height = 27
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          71.4375
          1222.375
          0
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRDBText25: TQRDBText
        Left = 351
        Top = 3
        Width = 97
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          928.6875
          7.9375
          256.645833333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_KabHes
        DataField = 'SchumTashlom'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText25Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRShape43: TQRShape
        Left = 344
        Top = 0
        Width = 1
        Height = 27
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          71.4375
          910.166666666667
          0
          2.64583333333333)
        Shape = qrsRectangle
      end
      object QRLabel37: TQRLabel
        Left = 218
        Top = 3
        Width = 116
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          576.791666666667
          7.9375
          306.916666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'QRLabel37'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRLabel37Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
    end
    object QRSubDetail2: TQRSubDetail
      Left = 38
      Top = 661
      Width = 718
      Height = 2
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        5.29166666666667
        1899.70833333333)
      Master = QuickRep1
      DataSet = DM_Kupa.AtmRxQuery_Checks
      HeaderBand = GroupHeaderBand2
      PrintBefore = False
      PrintIfEmpty = True
    end
    object ChildBand2: TQRChildBand
      Left = 38
      Top = 663
      Width = 718
      Height = 25
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        66.1458333333333
        1899.70833333333)
      ParentBand = QRSubDetail2
      object QRDBText18: TQRDBText
        Left = 437
        Top = 6
        Width = 182
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1156.22916666667
          15.875
          481.541666666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'CheckNumber'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText19: TQRDBText
        Left = 629
        Top = 6
        Width = 84
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1664.22916666667
          15.875
          222.25)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'LookSugTashlum'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText20: TQRDBText
        Left = 340
        Top = 5
        Width = 97
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          899.583333333333
          13.2291666666667
          256.645833333333)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'DatePiraon'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText21: TQRDBText
        Left = 225
        Top = 6
        Width = 107
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          595.3125
          15.875
          283.104166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'MisparCheshbon'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText22: TQRDBText
        Left = 163
        Top = 6
        Width = 54
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          431.270833333333
          15.875
          142.875)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'KodBank'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText_CheckNumberPrint
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText23: TQRDBText
        Left = 111
        Top = 6
        Width = 46
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          293.6875
          15.875
          121.708333333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'SnifNumber'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText_CheckNumberPrint
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText24: TQRDBText
        Left = 5
        Top = 6
        Width = 92
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          13.2291666666667
          15.875
          243.416666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Checks
        DataField = 'SchumCheck'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText6Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRShape25: TQRShape
        Left = -3
        Top = -2
        Width = 8
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          -7.9375
          -5.29166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape26: TQRShape
        Left = -5
        Top = -2
        Width = 12
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          -13.2291666666667
          -5.29166666666667
          31.75)
        Shape = qrsVertLine
      end
      object QRShape27: TQRShape
        Left = 98
        Top = -2
        Width = 8
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          259.291666666667
          -5.29166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape28: TQRShape
        Left = 154
        Top = -2
        Width = 8
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          407.458333333333
          -5.29166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape29: TQRShape
        Left = 216
        Top = -2
        Width = 8
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          571.5
          -5.29166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape30: TQRShape
        Left = 336
        Top = 0
        Width = 1
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          889
          0
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape31: TQRShape
        Left = 440
        Top = 0
        Width = 1
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          1164.16666666667
          0
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape32: TQRShape
        Left = 626
        Top = 0
        Width = 1
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          1656.29166666667
          0
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape33: TQRShape
        Left = 715
        Top = 0
        Width = 1
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.9583333333333
          1891.77083333333
          0
          2.64583333333333)
        Shape = qrsRectangle
      end
    end
    object GroupHeaderBand2: TQRBand
      Left = 38
      Top = 609
      Width = 718
      Height = 52
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        137.583333333333
        1899.70833333333)
      BandType = rbGroupHeader
      object QRShape34: TQRShape
        Left = 1
        Top = 20
        Width = 716
        Height = 31
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          82.0208333333333
          2.64583333333333
          52.9166666666667
          1894.41666666667)
        Brush.Color = clSilver
        Shape = qrsRectangle
      end
      object QRLabel24: TQRLabel
        Left = 24
        Top = 26
        Width = 72
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          63.5
          68.7916666666667
          190.5)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel25: TQRLabel
        Left = 106
        Top = 25
        Width = 45
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          280.458333333333
          66.1458333333333
          119.0625)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel26: TQRLabel
        Left = 160
        Top = 25
        Width = 57
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          423.333333333333
          66.1458333333333
          150.8125)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '��� ���'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel34: TQRLabel
        Left = 230
        Top = 25
        Width = 100
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          608.541666666667
          66.1458333333333
          264.583333333333)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� �����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel35: TQRLabel
        Left = 363
        Top = 25
        Width = 60
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          960.4375
          66.1458333333333
          158.75)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '������'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel36: TQRLabel
        Left = 472
        Top = 25
        Width = 134
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1248.83333333333
          66.1458333333333
          354.541666666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� ���/�����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRLabel46: TQRLabel
        Left = 630
        Top = 24
        Width = 82
        Height = 24
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          63.5
          1666.875
          63.5
          216.958333333333)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '��� �����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        FontSize = 14
      end
      object QRShape42: TQRShape
        Left = 716
        Top = 20
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          1894.41666666667
          52.9166666666667
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape44: TQRShape
        Left = 626
        Top = 20
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          1656.29166666667
          52.9166666666667
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape45: TQRShape
        Left = 440
        Top = 20
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          1164.16666666667
          52.9166666666667
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape46: TQRShape
        Left = 336
        Top = 20
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          889
          52.9166666666667
          2.64583333333333)
        Shape = qrsVertLine
      end
      object QRShape47: TQRShape
        Left = 216
        Top = 20
        Width = 8
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          571.5
          52.9166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape48: TQRShape
        Left = 154
        Top = 20
        Width = 8
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          407.458333333333
          52.9166666666667
          21.1666666666667)
        Shape = qrsVertLine
      end
      object QRShape49: TQRShape
        Left = 102
        Top = 20
        Width = 1
        Height = 30
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          79.375
          269.875
          52.9166666666667
          2.64583333333333)
        Shape = qrsVertLine
      end
    end
    object GroupFooterBand1: TQRBand
      Left = 38
      Top = 557
      Width = 718
      Height = 52
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      BeforePrint = GroupFooterBand1BeforePrint
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        137.583333333333
        1899.70833333333)
      BandType = rbGroupFooter
      object QRLabel27: TQRLabel
        Left = 344
        Top = 6
        Width = 119
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.9791666666667
          910.166666666667
          15.875
          314.854166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '======================'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRExpr1: TQRExpr
        Left = 343
        Top = 17
        Width = 107
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          907.520833333333
          44.9791666666667
          283.104166666667)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = [fsBold]
        Color = clWhite
        Master = QRSubDetail1
        OnPrint = QRExpr1Print
        ParentFont = False
        ResetAfterPrint = False
        Transparent = False
        WordWrap = True
        Expression = 'SUM(AtmRxQuery_KabHes.SchumTashlom)'
        FontSize = 14
      end
      object QRLabel32: TQRLabel
        Left = 490
        Top = 17
        Width = 46
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1296.45833333333
          44.9791666666667
          121.708333333333)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '��"� '
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel38: TQRLabel
        Left = 219
        Top = 6
        Width = 119
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.9791666666667
          579.4375
          15.875
          314.854166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '======================'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRLabel39: TQRLabel
        Left = 218
        Top = 20
        Width = 118
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          576.791666666667
          52.9166666666667
          312.208333333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'QRLabel39'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = [fsBold]
        OnPrint = QRLabel39Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
    end
    object QrLogo: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 103
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      BeforePrint = QrLogoBeforePrint
      Color = clWhite
      Font.Charset = HEBREW_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'David'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        272.520833333333
        1899.70833333333)
      BandType = rbPageHeader
      object QRImage1: TQRImage
        Left = 10
        Top = 0
        Width = 698
        Height = 78
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          206.375
          26.4583333333333
          0
          1846.79166666667)
        AutoSize = True
        Center = True
      end
    end
    object ChildBand3: TQRChildBand
      Left = 38
      Top = 141
      Width = 718
      Height = 199
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        526.520833333333
        1899.70833333333)
      ParentBand = QrLogo
      object QRLabel1: TQRLabel
        Left = 339
        Top = 156
        Width = 161
        Height = 36
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          95.25
          896.9375
          412.75
          425.979166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� ����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clBlack
        Font.Height = -35
        Font.Name = 'David'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 26
      end
      object QRDBText_MisHafkada: TQRDBText
        Left = 229
        Top = 156
        Width = 100
        Height = 36
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          95.25
          605.895833333333
          412.75
          264.583333333333)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.AtmRxQuery_Kabala
        DataField = 'KabalaNumber'
        Font.Charset = HEBREW_CHARSET
        Font.Color = clBlack
        Font.Height = -35
        Font.Name = 'David'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 26
      end
      object QRLabel_Makor: TQRLabel
        Left = 4
        Top = 155
        Width = 98
        Height = 36
        Frame.Color = clBlack
        Frame.DrawTop = True
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        Size.Values = (
          95.25
          10.5833333333333
          410.104166666667
          259.291666666667)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clBlack
        Font.Height = -35
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 26
      end
      object QRLabel12: TQRLabel
        Left = 535
        Top = 116
        Width = 94
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          1415.52083333333
          306.916666666667
          248.708333333333)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� �����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText13: TQRDBText
        Left = 382
        Top = 116
        Width = 140
        Height = 23
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          60.8541666666667
          1010.70833333333
          306.916666666667
          370.416666666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.Qry_Mname
        DataField = 'Osek_Murshe'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText13Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRLabel5: TQRLabel
        Left = 245
        Top = 116
        Width = 89
        Height = 20
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          52.9166666666667
          648.229166666667
          306.916666666667
          235.479166666667)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = '���� ����'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRDBText14: TQRDBText
        Left = 112
        Top = 116
        Width = 115
        Height = 23
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          60.8541666666667
          296.333333333333
          306.916666666667
          304.270833333333)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Color = clWhite
        DataSet = DM_Kupa.Qry_Mname
        DataField = 'Company_Num'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRDBText14Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object QRMemo_CompanyDetails: TQRLabel
        Left = 251
        Top = 5
        Width = 216
        Height = 89
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          235.479166666667
          664.104166666667
          13.2291666666667
          571.5)
        Alignment = taCenter
        AlignToBand = True
        AutoSize = True
        AutoStretch = True
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'QRMemo_CompanyDetails'
        Color = clWhite
        Font.Charset = HEBREW_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'David'
        Font.Style = []
        OnPrint = QRMemo_CompanyDetailsPrint
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
    end
  end
end

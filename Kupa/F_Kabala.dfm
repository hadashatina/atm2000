object Frm_Kabala: TFrm_Kabala
  Left = 0
  Top = 21
  Width = 640
  Height = 428
  BiDiMode = bdRightToLeft
  Caption = '�����'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 632
    Height = 339
    ActivePage = TS_Main
    Align = alClient
    HotTrack = True
    Style = tsFlatButtons
    TabOrder = 0
    object TS_Main: TTabSheet
      Caption = '�����'
      object Splitter1: TSplitter
        Left = 0
        Top = 197
        Width = 624
        Height = 1
        Cursor = crVSplit
        Align = alTop
      end
      object Panel_Kabala: TPanel
        Left = 0
        Top = 0
        Width = 624
        Height = 78
        Align = alTop
        TabOrder = 0
        OnEnter = Panel_KabalaEnter
        OnExit = Panel_KabalaExit
        object Label1: TLabel
          Left = 577
          Top = 10
          Width = 47
          Height = 13
          Caption = '��'#39' ����'
        end
        object Label2: TLabel
          Left = 435
          Top = 10
          Width = 33
          Height = 13
          Caption = '�����'
        end
        object Label_CodeLakoach: TLabel
          Left = 252
          Top = 10
          Width = 38
          Height = 13
          Caption = '<����>'
          OnDblClick = Label_CodeLakoachDblClick
        end
        object Lbl_zkut: TLabel
          Left = 554
          Top = 31
          Width = 70
          Height = 13
          Caption = '����� ������'
        end
        object Lbl_Chiuv: TLabel
          Left = 401
          Top = 31
          Width = 67
          Height = 13
          Caption = '����� �����'
        end
        object Lbl_masmakor: TLabel
          Left = 213
          Top = 31
          Width = 77
          Height = 13
          Hint = '����� �� �����'
          Caption = '���'#39' �� �����'
        end
        object Lbl_kodbitzua: TLabel
          Left = 82
          Top = 31
          Width = 53
          Height = 13
          Caption = '��� �����'
        end
        object Label4: TLabel
          Left = 591
          Top = 53
          Width = 32
          Height = 13
          Caption = '�����'
        end
        object Spb_ShowOpenHeshbonitForLakGroup: TSpeedButton
          Left = 143
          Top = 51
          Width = 25
          Height = 25
          Hint = '��� �������� ������ ������ ����'
          AllowAllUp = True
          GroupIndex = 1
          Glyph.Data = {
            36020000424D3602000000000000360000002800000010000000100000000100
            10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C000000000000
            0000000000000000000000000000000000001F7C00001F7C1F7C0000FF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7C0000FF7F0000
            0000FF7F00000000FF7F00000000FF7F00001F7C1F7C1F7C1F7C0000FF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7C00000F000F00
            0F000F000F000F000F000F000F000F000F000F001F7C1F7C1F7C1F7C0F00FF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0F001F7C1F7C1F7C1F7C0F00FF7F
            00000000FF7F00000000FF7F00000000FF7F0F001F7C1F7C1F7C1F7C0F00FF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0F001F7C1F7C1F7C1F7C0F000F00
            0F000F000F000F000F000F000F000F000F000F001F7C1F7C1F7C1F7C0F001F7C
            0F000F001F7C0F000F001F7C0F000F001F7C0F001F7C1F7C1F7C1F7C0F000F00
            0F000F000F000F000F000F000F000F000F000F001F7C1F7C1F7C}
        end
        object AtmDbHEdit_KabalaNo: TAtmDbHEdit
          Left = 470
          Top = 6
          Width = 79
          Height = 21
          TabStop = False
          BiDiMode = bdRightToLeft
          DataField = 'KabalaNumber'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 1
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Label1
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = True
        end
        object AtmDBDateEdit1: TAtmDBDateEdit
          Left = 292
          Top = 6
          Width = 108
          Height = 21
          DataField = 'DateKabala'
          DataSource = DM_Kupa.DS_Kabala
          CheckOnExit = True
          DefaultToday = True
          DialogTitle = '����� ����'
          BiDiMode = bdLeftToRight
          ParentBiDiMode = False
          NumGlyphs = 2
          TabOrder = 0
          StartOfWeek = Sun
          Weekends = [Sat]
          LinkLabel = Label2
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          F9String = '26/01/2000'
          IsFixed = False
          FixedColor = clBtnFace
          StatusBarPanelNum = -1
        end
        object KabCodeLakoach: TAtmDbHEdit
          Left = 140
          Top = 5
          Width = 70
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'CodeLakoach'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 2
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Label_CodeLakoach
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SearchComponent = AtmAdvSearch_lakoach
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          BeforeExecuteSearch = KabCodeLakoachBeforeExecuteSearch
          LocateRecordOnChange = False
        end
        object AtmDbHEdit1: TAtmDbHEdit
          Left = 2
          Top = 5
          Width = 136
          Height = 21
          TabStop = False
          BiDiMode = bdRightToLeft
          Color = clInfoBk
          DataField = 'ShemLakoach'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 3
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object ChebZikuy: TAtmDbHEdit
          Left = 470
          Top = 28
          Width = 79
          Height = 21
          TabStop = False
          BiDiMode = bdRightToLeft
          DataField = 'HesbonZikuy'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 4
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_zkut
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object DBComboBox_HeshbonHiuv: TDBComboBox
          Left = 292
          Top = 28
          Width = 108
          Height = 21
          TabStop = False
          DataField = 'HesbonChiuv'
          DataSource = DM_Kupa.DS_Kabala
          ItemHeight = 13
          TabOrder = 5
        end
        object ChenMasMakor: TAtmDbHEdit
          Left = 140
          Top = 28
          Width = 70
          Height = 21
          TabStop = False
          BiDiMode = bdRightToLeft
          DataField = 'HesbonMasMakor'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 6
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_masmakor
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object KodBitzou: TAtmDbHEdit
          Left = 2
          Top = 28
          Width = 78
          Height = 21
          TabStop = False
          AutoSize = False
          BiDiMode = bdRightToLeft
          DataField = 'KodBItzua'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 7
          OnChange = KodBitzouChange
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_kodbitzua
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object KabPratim: TAtmDbHEdit
          Left = 171
          Top = 51
          Width = 419
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Pratim'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 8
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Label4
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SearchComponent = AtmAdvSearch_Remarks
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object BitBtn1: TBitBtn
          Left = 2
          Top = 51
          Width = 140
          Height = 25
          Action = Act_ShowOpenHeshbonitForLak
          Caption = '��� �������� ������'
          TabOrder = 9
          TabStop = False
          Glyph.Data = {
            36020000424D3602000000000000360000002800000010000000100000000100
            10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C00000000000000000000000000001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C0000FF7F1F001F001F00FF7F00001F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7C0000
            00000000000000000000FF7F1F001F001F00FF7F00001F7C1F7C1F7C1F7C0000
            1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C1F7C000000000000
            1F7C1F001F001F000000FF7F1F001F001F00FF7F00001F7C1F7C0000EF010000
            1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C1F7C0000EF010000
            1F7C1F001F001F0000000000000000000000000000001F7C1F7C0000EF010000
            1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000EF010000
            1F7C1F001F001F001F7C00001F7C1F7C1F7C1F7C1F7C0F001F7C0000EF010000
            1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C0F000F000F000000EF010000
            0000000000000000000000001F7C1F7C1F7C1F7C1F7C0F001F7C0000EF01EF01
            EF01EF01EF0100001F7C1F7C1F7C0F001F7C1F7C1F7C0F001F7C000000000000
            00000000000000001F7C1F7C1F7C1F7C0F000F000F001F7C1F7C}
        end
      end
      object RxDBGrid_Checks: TRxDBGrid
        Left = 0
        Top = 78
        Width = 624
        Height = 119
        Align = alTop
        Ctl3D = True
        DataSource = DM_Kupa.DS_Checks
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColEnter = RxDBGrid_ChecksColEnter
        OnEnter = RxDBGrid_ChecksEnter
        OnExit = RxDBGrid_ChecksExit
        OnKeyDown = RxDBGrid_ChecksKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'CodeSugTashlum'
            Title.Caption = '��� �����'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LookSugTashlum'
            ReadOnly = True
            Title.Caption = '����'
            Title.Color = clInfoBk
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SchumCheck'
            Title.Caption = '����'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DatePiraon'
            Title.Caption = '����� ������'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CheckNumber'
            Title.Caption = '����'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MisparCheshbon'
            Title.Caption = '���� �����'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KodBank'
            Title.Caption = '��� ���'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SnifNumber'
            Title.Caption = '����'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LookShemBank'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ReadOnly = True
            Title.Caption = '�� ���'
            Title.Color = clInfoBk
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodeBankHafkada'
            Title.Caption = '��� ��� �����'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodeSnifHafkada'
            Title.Caption = '���� �����'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MisHeshbonHafkada'
            Title.Caption = '��'#39' ����� �����'
            Width = 99
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 198
        Width = 624
        Height = 110
        Align = alClient
        TabOrder = 2
        object Panel3: TPanel
          Left = 199
          Top = 1
          Width = 26
          Height = 85
          Align = alRight
          TabOrder = 1
          object Spb_CloseCurHeshbonit: TSpeedButton
            Left = 2
            Top = 39
            Width = 23
            Height = 22
            Action = Act_SelectOneHeshbonitForClose
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F1F7C1F7C0F00
              1F7C1F7C000000000000000000000000000000000000000000001F7C0F000F00
              1F7C1F7C0000FF7FFF03FF7FFF03FF7FFF03FF7FFF03FF7FFF030F000F000F00
              0F000F000000FF03FF7FFF03FF7FFF03FF7FFF03FF7FFF03FF7F1F7C0F000F00
              1F7C1F7C0000FF7FFF03FF7FFF03FF7FFF03FF7FFF03FF7FFF031F7C1F7C0F00
              1F7C1F7C000000000000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
          end
          object SpeedButton1: TSpeedButton
            Left = 2
            Top = 3
            Width = 23
            Height = 22
            Hint = '���� �� �� ��������� ������� - F1'
            Action = Act_CloseHeshbonitSequence
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C0F000F000F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C0F000F000F000F000F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C0000FF03FF7FFF0300001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C0000FF7FFF03FF7F00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C0000FF03FF7FFF0300001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C0000FF7FFF03FF7F00001F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000
              000000000000FF03FF7FFF03000000000000000000001F7C1F7C1F7C0000FF7F
              FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7F00001F7C1F7C1F7C0000FF7F
              FF7FFF7F0000FF03FF7FFF030000FF7FFF7FFF7F00001F7C1F7C1F7C0000FF7F
              FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7F00001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
          end
        end
        object RxDBGrid_KabHes: TRxDBGrid
          Left = 1
          Top = 1
          Width = 198
          Height = 85
          Align = alClient
          DataSource = DM_Kupa.DS_KabHes
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnEnter = RxDBGrid_KabHesEnter
          OnExit = RxDBGrid_KabHesExit
          OnGetCellParams = RxDBGrid_KabHesGetCellParams
          Columns = <
            item
              Expanded = False
              FieldName = 'HesbonitNumber'
              Title.Caption = '�������'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodeLakoach'
              Title.Caption = '����'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SchumTashlom'
              Title.Caption = '���� ������'
              Width = 71
              Visible = True
            end>
        end
        object Panel_HeshForLak: TPanel
          Left = 225
          Top = 1
          Width = 398
          Height = 85
          Align = alRight
          TabOrder = 0
        end
        object Panel5: TPanel
          Left = 1
          Top = 86
          Width = 622
          Height = 23
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 3
          object Label5: TLabel
            Left = 554
            Top = 4
            Width = 60
            Height = 13
            Caption = '����� ����'
          end
          object Label6: TLabel
            Left = 415
            Top = 4
            Width = 45
            Height = 13
            Caption = '�� ����'
          end
          object Label3: TLabel
            Left = 142
            Top = 4
            Width = 56
            Height = 13
            Caption = '�� �������'
          end
          object Edit_ItratLakoach: TEdit
            Left = 471
            Top = 2
            Width = 82
            Height = 19
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 0
            Text = '0'
            OnChange = Edit_ItratLakoachChange
          end
          object DBEdit_TotalKabala: TDBEdit
            Left = 322
            Top = 2
            Width = 88
            Height = 19
            Ctl3D = False
            DataField = 'TotalSumKabla'
            DataSource = DM_Kupa.DS_Kabala
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 51
            Top = 2
            Width = 88
            Height = 19
            Ctl3D = False
            DataField = 'TotalZikuy'
            DataSource = DM_Kupa.DS_Kabala
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 2
          end
        end
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 339
    Width = 632
    Height = 24
    Align = alBottom
    Caption = 'ToolBar1'
    Flat = True
    Images = ImageList1
    TabOrder = 1
    object DBNavigator1: TDBNavigator
      Left = 0
      Top = 0
      Width = 240
      Height = 22
      DataSource = DM_Kupa.DS_Kabala
      Flat = True
      Hints.Strings = (
        '����� ������'
        '����� �����'
        '����� ����'
        '����� ������'
        '���� �����'
        '��� �����'
        '���� �����'
        '���� �����'
        '��� �������'
        '�����')
      TabOrder = 0
      BeforeAction = DBNavigator1BeforeAction
    end
    object ToolButton3: TToolButton
      Left = 240
      Top = 0
      Action = Act_NewKabala
    end
    object ToolButton1: TToolButton
      Left = 263
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 271
      Top = 0
      Action = Action_OpenSearch
    end
    object ToolButton4: TToolButton
      Left = 294
      Top = 0
      Action = Action_duplicateRecord
    end
    object Btn_Edkun: TBitBtn
      Left = 317
      Top = 0
      Width = 102
      Height = 22
      Action = Act_UpdateFiles
      Caption = '����� ������-F7'
      TabOrder = 1
    end
    object BitBtn2: TBitBtn
      Left = 419
      Top = 0
      Width = 102
      Height = 22
      Action = Act_PrintKabala
      Caption = '���� ����-F8'
      TabOrder = 2
      Glyph.Data = {
        36020000424D3602000000000000360000002800000010000000100000000100
        10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        00000000000000000000000000000000000000001F7C1F7C1F7C1F7C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C00001F7C1F7C000000000000
        00000000000000000000000000000000000000001F7C00001F7C00001F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07F1F7C1F7C0000000000001F7C00001F7C1F7C
        1F7C1F7C1F7C1F7CEF3DEF3DEF3D1F7C1F7C00001F7C00001F7C000000000000
        00000000000000000000000000000000000000001F7C1F7C000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C00001F7C00001F7C00000000
        000000000000000000000000000000001F7C00001F7C000000001F7C1F7C0000
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C00001F7C00001F7C1F7C1F7C
        0000FF7F00000000000000000000FF7F00000000000000001F7C1F7C1F7C1F7C
        0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C0000FF7F00000000000000000000FF7F00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C0000000000000000000000000000000000001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
    end
    object BitBtn3: TBitBtn
      Left = 521
      Top = 0
      Width = 102
      Height = 22
      Hint = '��� ���� �������� ������'#13#10' �"� ���� ������'
      Action = Act_CloseOpenHeshboniot
      Caption = '��� ��������'
      TabOrder = 3
      Glyph.Data = {
        36020000424D3602000000000000360000002800000010000000100000000100
        10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        00000000000000000000000000000000000000001F7C1F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7F1F7C1F7CFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7F003C1F7C1F7CFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7F003C003C003C1F7C1F7CFF7FFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7F003C003CFF7F003C003C1F7CFF7FFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7FFF7FFF7F003CEF3D1F7CFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7FFF7FFF7FFF7F003C1F7C1F7CFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F003C1F7CFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F003CFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C1F7C1F7C1F7CEF3D
        FF7FFF7F000000000000000000000000FF7FFF7F00001F7C1F7C1F7C1F7C1F7C
        EF3DEF3D0000FF7F1F7C1F7C1F7C0000EF3DEF3D1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DEF3DEF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 363
    Width = 632
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object MainMenu1: TMainMenu
    Left = 474
    Top = 65525
    object N1: TMenuItem
      Caption = '����'
      object N8: TMenuItem
        Action = Act_ChangeYehusYear
      end
      object ActSelectOnlyOpenKabalot1: TMenuItem
        Action = Act_SelectOnlyOpenKabalot
      end
      object N9: TMenuItem
        Caption = '��� ����� ������ ����'
        OnClick = N9Click
      end
      object N10: TMenuItem
        Caption = '������ �������'
        OnClick = N10Click
      end
      object N11: TMenuItem
        Caption = '[����� �����  [���/���'
        OnClick = N11Click
      end
      object N2: TMenuItem
        Caption = '�����'
        OnClick = N2Click
      end
    end
    object N3: TMenuItem
      Caption = '������'
      object N4: TMenuItem
        Caption = '���� ��������/�����'
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = '������'
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = '�����'
        OnClick = N6Click
      end
      object N7: TMenuItem
        Caption = '������'
        OnClick = N7Click
      end
      object N21: TMenuItem
        Caption = '������ 2'
        OnClick = N21Click
      end
    end
  end
  object AtmAdvSearch_lakoach: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'kod_lakoach'
      'shem_lakoach'
      'Mis_Han')
    ShowFieldsHeader.Strings = (
      '��� ����'
      '�� ����'
      '���� ����'#39'�')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150'
      '150')
    Sql.Strings = (
      'Select * From Lakoach')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_lakoach'
    KeepList = True
    EditBiDiMode = bdLeftToRight
    Left = 202
  end
  object ImageList1: TImageList
    Left = 166
    Bitmap = {
      494C01010A000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001001000000000000020
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7F000000000000
      00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000
      00000000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0F000F000F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0F000F000F000F000F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F0000FF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FE07FE07FE07FFF7F
      FF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FEF3DEF3DEF3DFF7F
      FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F0000FF7F000000000000000000000000
      00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000
      00000000FF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF03FF7FFF030000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0F000F00FF7FFF7F0000FF7FFF03FF7FFF03FF7F
      FF03FF7FFF03FF7FFF03FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F00000F000000FF03FF7FFF03FF7FFF03
      FF7FFF03FF7FFF03FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      FF7F0000FF7F00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F000F000F000F00FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF03FF7FFF030000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0F000F00FF7FFF7F0000FF7FFF03FF7FFF03FF7F
      FF03FF7FFF03FF7FFF03FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F0000FF7F000000000000000000000000
      00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000000000000000FF7F
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7F00000000
      000000000000FF03FF7FFF0300000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FE07FFF7F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000000000000000
      FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000
      00000000FF7FEF3DEF3DEF3DEF3DEF3DEF3D0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7FFF7F0000
      0000FF7FFF7FFF7FFF7FFF7F000000000000FF7FE07FFF7F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7FFF7F003C
      00000000FF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7F003C003C
      003C00000000FF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000EF3DFF7F003C003CFF7F
      003C003C0000FF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000FF7F00000000
      FF7F00000000FF7F0000000000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7F003CEF3D0000FF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7F003C00000000FF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F000F000F000F000F000F00
      0F000F000F000F000F000F0000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F003C0000FF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F00FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F0000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F003CFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F00FF7F00000000FF7F0000
      0000FF7F00000000FF7F0F0000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F00FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F0000000000000000000000EF3DFF7FFF7F00000000
      0000000000000000FF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F000F000F000F000F000F00
      0F000F000F000F000F000F00000000000000000000000000EF3DEF3D0000FF7F
      0000000000000000EF3DEF3D000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F0000000F000F0000000F00
      0F0000000F000F0000000F00000000000000000000000000000000000000EF3D
      EF3DEF3DEF3D000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F000F000F000F000F000F00
      0F000F000F000F000F000F000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000F00
      0F000F0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000F000F00
      0F000F000F000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0F00000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      E07FE07FE07F0000000000000000000000000000000000000000000000000000
      0F00000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      EF3DEF3DEF3D0000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000F000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF03
      FF7FFF03000000000000000000000000000000000F000F00000000000000FF7F
      FF03FF7FFF03FF7FFF03FF7FFF03FF7FFF0300000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF03FF7F00000000000000000000000000000F000F000F000F000F000000FF03
      FF7FFF03FF7FFF03FF7FFF03FF7FFF03FF7F00000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF03
      FF7FFF03000000000000000000000000000000000F000F00000000000000FF7F
      FF03FF7FFF03FF7FFF03FF7FFF03FF7FFF0300000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F00000000000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000FF7F
      FF03FF7F0000000000000000000000000000000000000F000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000FF7F00000000
      000000000000FF7F00000000000000000000000000000000000000000000FF03
      FF7FFF0300000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FFF7F000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F000000000000000000000000FF7FFF7FFF7F0000FF7F
      FF03FF7F0000FF7FFF7FFF7F0000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FFF7F0000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7F000000000000000000000000FF7F0000
      0000000000000000FF7F000000000000000000000000FF7FFF7FFF7F0000FF03
      FF7FFF030000FF7FFF7FFF7F0000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DE07F000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FFF7FFF7F0000FF7F
      FF03FF7F0000FF7FFF7FFF7F0000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000F0000000F00
      00000F0000000F0000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000F0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F000000000000EF3D00000000000000000000000000000000
      0000FF7F1F001F001F00FF7F0000000000000000000000000F000F000F000000
      000000000000000000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F00000000000000000000
      FF7FFF7F0000FF7F000000000000EF3D00000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000F0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0000FF7F1F001F001F00FF7F0000000000000000000000000000000000000000
      000000000000000000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F00000000000000000000
      0000FF7F00000000000000000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3DEF3DEF3D000000000000000000001F001F001F00
      0000FF7F1F001F001F00FF7F000000000000000000000000FF7FE07FFF7FE07F
      0000000000000000FF7FE07FFF7FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7F000000000000EF3D00000000EF0100000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000FF7F00000000000000000000
      00000000FF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F000000000000EF3D00000000EF01000000001F001F001F00
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000EF0100000000000000000000
      0000000000000000000000000000000000000000FF7FEF3DEF3DEF3DFF7F0000
      00000000FF7FEF3DEF3DEF3DFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EF01000000001F001F001F00
      00000000000000000000000000000F0000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000000EF0100000000000000000000
      0000000000000000000000000F000F000F000000FF7FEF3DEF3DEF3DFF7F0000
      00000000FF7FEF3DEF3DEF3DFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000000EF0100000000000000000000
      00000000000000000000000000000F0000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000000EF01EF01EF01EF01EF010000
      0000000000000F000000000000000F0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000F000F000F0000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00EF3DFF7F000000000000FF7FEF3DEF3D
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FEF3DEF3DEF3DFF7FFF7FFF7FFF7FFF7F
      0000000000000000FF7F000000000000FF7FFF7FFF7FFF7F0000FF7FFF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7F000000000000FF7FFF7FFF7FFFFFFFFF00000000FFFBE007FF7FFF7F
      FFFBC003FF7FFF7FFFE0C303FF7FFF7FFFFBC183EF3DFF7F000BC0C3FF7FFF7F
      000FC04300000000000FC02300000000000FC033000000000007C01300000000
      8007C003000000008007C003000000008007C003000000008007E1C700000000
      A497FC3F000000008007FFFF00000000FFFFFFFFFFFFFFFFFEFFFFFF800FC007
      FC7FFF00800FBFEBF83FFF00800F0005FEFFFF00800F7E31FEFFFF00800F7E35
      F83FD800800F0006F83F9800800F7FEAF83F0000800F8014F83F9800800FC00A
      F83FD800800FE0018003FF008000E0078003FF008000F0078003FF008000F003
      8003FF00F000F803FFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FE03
      F557FFFF0000FE03F7FFFFFF0014FE03E3F7E00F0000FE03F7FFF3EF0000C003
      FFF7F9FF0000DE03C0C0FCFF000010030000FE7F00001E030000FCFF00141003
      0101F9FF00001F7F0101F3EF0000117D0101E00F00001F780101FFFF0008007D
      0101FFFF001C01DD0101FFFF000001E300000000000000000000000000000000
      000000000000}
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 126
    Top = 1
    object Action_duplicateRecord: TAction
      Caption = '���� �����'
      Hint = '���� �����*'
      ImageIndex = 0
      ShortCut = 122
      OnExecute = Action_duplicateRecordExecute
      OnUpdate = Action_duplicateRecordUpdate
    end
    object Action_SumTotals: TAction
      Caption = '�������'
      Hint = '�������'
      ImageIndex = 1
    end
    object Action_OpenSearch: TAction
      Caption = '���� �����'
      Hint = '���� �����'
      ImageIndex = 2
      ShortCut = 113
      OnExecute = Action_OpenSearchExecute
      OnUpdate = Action_OpenSearchUpdate
    end
    object Action_SelectNextControl: TAction
      Caption = '���� ���'
      Hint = '���� ���'
      ShortCut = 117
    end
    object Action_SelectPrevControl: TAction
      Caption = '���� �����'
      Hint = '���� �����'
      ShortCut = 8309
    end
    object Act_ShowOpenHeshbonitForLak: TAction
      Caption = '��� �������� ������'
      Hint = '��� �������� ������ �����'
      ImageIndex = 3
      OnExecute = Act_ShowOpenHeshbonitForLakExecute
    end
    object Act_SelectOneHeshbonitForClose: TAction
      Hint = '���� ������� ������'
      ImageIndex = 5
      OnExecute = Act_SelectOneHeshbonitForCloseExecute
    end
    object Act_CloseHeshbonitSequence: TAction
      Hint = '���� �� �� ��������� �������'
      ImageIndex = 4
      OnExecute = Act_CloseHeshbonitSequenceExecute
    end
    object Act_PrintKabala: TAction
      Caption = '���� ����-F8'
      ImageIndex = 7
      ShortCut = 119
      OnExecute = Act_PrintKabalaExecute
    end
    object Act_NewKabala: TAction
      Caption = '���� ����'
      Hint = '���� ����'
      ImageIndex = 6
      ShortCut = 123
      OnExecute = Act_NewKabalaExecute
    end
    object Act_ChangeYehusYear: TAction
      Caption = '����� ��� ����� �����'
      OnExecute = Act_ChangeYehusYearExecute
    end
    object Act_UpdateFiles: TAction
      Caption = '����� ������-F7'
      Hint = '����� ������-F7'
      ShortCut = 118
      OnExecute = Act_UpdateFilesExecute
    end
    object Act_CloseOpenHeshboniot: TAction
      Caption = '��� ��������'
      Hint = '��� ���� �������� ������ �"� ���� ������'
      ImageIndex = 9
      OnExecute = Act_CloseOpenHeshboniotExecute
    end
    object Act_SelectOnlyOpenKabalot: TAction
      Caption = '���� �������� ������'
      OnExecute = Act_SelectOnlyOpenKabalotExecute
    end
  end
  object AtmAdvSearch_KodBank: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla'
      'Teur_Tavla')
    ShowFieldsHeader.Strings = (
      '��� ���'
      '�� ���')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select * From KodTavla'
      'Where Sug_Tavla=101')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_KodBank'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_KodBankAfterExecute
    Left = 216
    Top = 131
  end
  object AtmAdvSearch_SugTashlum: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla'
      'Teur_Tavla')
    ShowFieldsHeader.Strings = (
      '��� �����'
      '���� �����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select * From KodTavla'
      'Where Sug_Tavla=107')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_SugTashlum'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_SugTashlumAfterExecute
    Left = 538
    Top = 125
  end
  object AtmAdvSearch_BankForHafkada: TAtmAdvSearch
    Caption = '����� ����� ������'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla2'
      'Teur_Tavla'
      'Teur_Tavla2')
    ShowFieldsHeader.Strings = (
      '���� �����'
      '�����'
      '��'#39' ���"�')
    ShowFieldsDisplayWidth.Strings = (
      '70'
      '150'
      '70')
    Sql.Strings = (
      'Select * from kodtvla2'
      'where Sug_Tavla=101')
    ReturnFieldIndex = -1
    SeparateChar = ';'
    IniSection = 'AtmAdvSearch_BankForHafkada'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_BankForHafkadaAfterExecute
    Left = 68
    Top = 132
  end
  object Scale1: TScale
    Active = True
    X_ScreenWidth = 640
    Y_ScreenHeight = 480
    Left = 342
    Top = 145
  end
  object AtmTabSheetBuild1: TAtmTabSheetBuild
    SetKeepGrid = False
    WebMode = False
    ScrFileName = 'TSBKabala1.Scr'
    SqlFileName = 'TSBKabala1.Sql'
    PageControl = PageControl1
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = DS_TSB
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForCutColor = 'CutColor'
    ParamsKeyForSelectedColor = 'SelectedColor'
    ParamsKeyForExportColor = 'ExportColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = RxQuery_TSB
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    EditBiDiMode = bdLeftToRight
    BeforeExecuteQuery = AtmTabSheetBuild1BeforeExecuteQuery
    OnGridDblClick = AtmTabSheetBuild1GridDblClick
    Left = 326
    Top = 65523
  end
  object DS_TSB: TDataSource
    DataSet = RxQuery_TSB
    Left = 365
    Top = 65527
  end
  object RxQuery_TSB: TRxQuery
    DatabaseName = 'DB_Kupa'
    Macros = <>
    Left = 404
    Top = 65524
  end
  object RxDBFilter_TSB: TRxDBFilter
    DataSource = DS_TSB
    Left = 432
    Top = 65524
  end
  object AtmTabSheetBuild_HeshForLak: TAtmTabSheetBuild
    SetKeepGrid = False
    WebMode = False
    ScrFileName = 'TSBKabala0.Scr'
    SqlFileName = 'TSBKabala0.Sql'
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = DM_Kupa.DS_HeshForLak
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForCutColor = 'CutColor'
    ParamsKeyForSelectedColor = 'SelectedColor'
    ParamsKeyForExportColor = 'ExportColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = DM_Kupa.RxQuery_HeshForLak
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = False
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    EditBiDiMode = bdLeftToRight
    BeforeExecuteQuery = AtmTabSheetBuild1BeforeExecuteQuery
    OnGridKeyDown = RxDBGrid_OpenHeshbonitKeyDown
    Left = 330
    Top = 244
  end
  object RxDBFilter1: TRxDBFilter
    DataSource = DS_TSB
    Left = 466
    Top = 242
  end
  object AtmAdvSearch_Nehag: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'kod_Nehag'
      'shem_Nehag'
      'Mis_Han'
      'Mis_Oved')
    ShowFieldsHeader.Strings = (
      '��� ���'
      '�� ���'
      '���� ����'#39'�'
      '��'#39' �����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150'
      '100'
      '100')
    Sql.Strings = (
      'Select *'
      'from nehag')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Nehag'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 93
  end
  object AtmAdvSearch_Remarks: TAtmAdvSearch
    Caption = '����� '
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Teur_Tavla'
      'Kod_Tavla')
    ShowFieldsHeader.Strings = (
      '����  '
      '��� ����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select * From KodTavla'
      'Where Sug_Tavla=21')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearchRemarks'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 420
    Top = 140
  end
  object AtmAdvSearch_Sapak: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'kod_Sapak'
      'shem_Sapak'
      'Mis_Han')
    ShowFieldsHeader.Strings = (
      '��� ���'
      '�� ���'
      '���� ����'#39'�')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150'
      '100'
      '')
    Sql.Strings = (
      'Select kod_Sapak,shem_Sapak,Mis_Han'
      'from Sapak')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Sapak'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 57
    Top = 65533
  end
end

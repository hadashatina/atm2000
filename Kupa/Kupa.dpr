library Kupa;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  DMKupa in 'DMKupa.pas' {DM_Kupa: TDataModule},
  F_Hafkada in 'F_Hafkada.pas' {Frm_Hafkada},
  AtmRutin in 'K:\Timchur\Common\AtmRutin.pas',
  MslDllInterface in 'K:\Timchur\Common\MslDllInterface.pas',
  AtmConst in 'K:\Timchur\Common\AtmConst.pas',
//AtmRutin in 'c:\NTATM\PROGRAMERS\ShareUnit\AtmRutin.pas',
//MslDllInterface in 'c:\NTATM\PROGRAMERS\ShareUnit\MslDllInterface.pas',
//AtmConst in 'c:\NTATM\PROGRAMERS\ShareUnit\AtmConst.pas',
  RptTofesHafkada in 'RptTofesHafkada.pas' {Rpt_TofesHafkada},
  F_Kabala in 'F_Kabala.pas' {Frm_Kabala},
  F_PrintKabala in 'F_PrintKabala.pas' {Frm_PrintKabala};

{$R *.RES}

Procedure Hafkada(AUserName,APassword,AAlias,Kabalatype:PChar);
Begin
  InitHafkadaForms(AUserName,APassword,AAlias,KabalaType);
  DoneHafkadaForms;
//Kabala_type:=2 ����
//0 = ���
End;

Procedure PrintCopyHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear,PrintToPrinter,TopLines,NumOfCopys :PChar);
Begin
  InitPrintHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear,PrintToPrinter,TopLines,NumOfCopys);
End;

Procedure CancelHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear:PChar);
Begin
  DoCancelHafkada(AUserName,APassword,AAlias,HafkadaNum,YehusYear);
End;

Procedure InitKupaDll(IniFileName,ScriptsDir :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);//(c:\Program files\atm\atm2000\AtmCrt.ini) �� ���� ������
  KupaDllScriptsDir:=StrPas(ScriptsDir);
End;

Procedure InitKupaDllFromAtmCrt(IniFileName,ScriptsDir,HesNum,YearNum :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);
  KupaDllScriptsDir:=StrPas(ScriptsDir);
  HesFromCrt:=StrToint(StrPas(HesNum));
  YearFromCrt:=StrToint(StrPas(YearNum));
End;

Exports
  InitKupaDll,
  InitKupaDllFromAtmCrt,
  Hafkada,
  PrintCopyHafkada{In RptTofesHafkada},
  CancelHafkada,
  DoKabala;
begin
  KupaDllIniFileName:='';
  KupaDllScriptsDir:='';
  HesFromCrt:=0;
  YearFromCrt:=0;
end.

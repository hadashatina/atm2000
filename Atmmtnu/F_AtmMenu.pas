unit F_AtmMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, SpeedBar, ExtCtrls, ImgList, Placemnt, jpeg,ShellApi, Menus,
  ExeMenu,Registry, Scale250,Inifiles,DbTables, RXShell, Just1_32,FileCtrl,
  ComCtrls, AppEvent;

Type
    TNoParamProc = Procedure;
    T2IntParamProc = Procedure (P1:LongInt;P2:LongInt);

type
  TFrm_AtmMainMenu = class(TForm)
    ActionList_Menu: TActionList;
    SpeedBar1: TSpeedBar;
    Action_ShowMapBmp: TAction;
    SpeedbarSection2: TSpeedbarSection;
    Spb_Crts: TSpeedItem;
    Btn_Maslul: TSpeedItem;
    Spb_Mehiron: TSpeedItem;
    Spb_WinTuda: TSpeedItem;
    Spb_Reports: TSpeedItem;
    Spb_Heshbonit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    Action_CloseApp: TAction;
    Action_WinTuda: TAction;
    PopupMenu_Crt: TPopupMenu;
    Spb_Exit: TSpeedItem;
    Action_OpenSadran: TAction;
    PopupMenu_Run: TPopupMenu;
    ExeMenu1: TExeMenu;
    NEdit_ExecMenu: TMenuItem;
    N6: TMenuItem;
    SpeedbarSection3: TSpeedbarSection;
    Spb_Sadran: TSpeedItem;
    Action_Meholel: TAction;
    Action_OpenMaslulon: TAction;
    SpeedItem3: TSpeedItem;
    Action_OpenNzr: TAction;
    Spb_Nzr: TSpeedItem;
    Action_OpenAtmTools: TAction;
    PopupMenu_Defs: TPopupMenu;
    NDefinePosNzr: TMenuItem;
    NDefinePosMaslulon: TMenuItem;
    NDefinePosSadran: TMenuItem;
    NCustomizeSpeedBar: TMenuItem;
    OpenDialog1: TOpenDialog;
    NDefinePosAtmTool: TMenuItem;
    Scale1: TScale;
    PopupMenu_HasotKvuot: TPopupMenu;
    N2: TMenuItem;
    N11: TMenuItem;
    PopupMenu_Iterface: TPopupMenu;
    SItem_Interface: TSpeedItem;
    Action_ImportManbasStudents: TAction;
    Action_ImportMmhaStudents: TAction;
    Action_CopyLastYearMusa: TAction;
    Action_ImportSchoolHourManbas: TAction;
    Action_CopyMaslul: TAction;
    Action_ImportMaslulExtFile: TAction;
    N12: TMenuItem;
    N20001: TMenuItem;
    N13: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    Action_MoveToHan: TAction;
    N17: TMenuItem;
    PopupMenu_Tools: TPopupMenu;
    Action_ManageSecurity: TAction;
    Action_AtmToolBackup: TAction;
    Action_AtmToolRestore: TAction;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    ActionManageSecurity1: TMenuItem;
    Action_MoveToHistory: TAction;
    PopupMenu_Reports: TPopupMenu;
    ActionMeholel1: TMenuItem;
    N_SaveDohSa: TMenuItem;
    N_SaveDohNetzer: TMenuItem;
    N24: TMenuItem;
    N5: TMenuItem;
    N25: TMenuItem;
    Action_ShibAutoShem: TAction;
    Action_ShibAutoKamut: TAction;
    N26: TMenuItem;
    N27: TMenuItem;
    Action_OpenHeshbon: TAction;
    RxTrayIcon1: TRxTrayIcon;
    PopupMenu_TrayICon: TPopupMenu;
    N22: TMenuItem;
    N23: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    Action_AddUsers: TAction;
    N32: TMenuItem;
    Action_DelUsers: TAction;
    N33: TMenuItem;
    StatusBar1: TStatusBar;
    JustOne321: TJustOne32;
    N34: TMenuItem;
    AppEvents1: TAppEvents;
    NChangePassword: TMenuItem;
    procedure Action_ShowMapBmpExecute(Sender: TObject);
    procedure Action_CloseAppExecute(Sender: TObject);
    procedure Action_WinTudaExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Action_OpenSadranExecute(Sender: TObject);
    procedure NEdit_ExecMenuClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Action_MeholelExecute(Sender: TObject);
    procedure Action_OpenMaslulonExecute(Sender: TObject);
    procedure Action_OpenNzrExecute(Sender: TObject);
    procedure Action_OpenAtmToolsExecute(Sender: TObject);
    procedure NCustomizeSpeedBarClick(Sender: TObject);
    procedure NDefinePosNzrClick(Sender: TObject);
    procedure NDefinePosMaslulonClick(Sender: TObject);
    procedure NDefinePosSadranClick(Sender: TObject);
    procedure NDefinePosAtmToolClick(Sender: TObject);
    procedure Action_NotInstalledExecute(Sender: TObject);
    procedure Action_AtmToolBackupExecute(Sender: TObject);
    procedure Action_AtmToolRestoreExecute(Sender: TObject);
    procedure Spb_ReportsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Action_ShibAutoShemExecute(Sender: TObject);
    procedure Action_OpenHeshbonExecute(Sender: TObject);
    procedure Spb_WinTudaMouseEnter(Sender: TObject);
    procedure Spb_WinTudaMouseLeave(Sender: TObject);
    procedure RxTrayIcon1DblClick(Sender: TObject);
    procedure ActionList_MenuExecute(Action: TBasicAction;
      var Handled: Boolean);
    procedure Action_ManageSecurityExecute(Sender: TObject);
    procedure Action_AddUsersExecute(Sender: TObject);
    procedure Action_DelUsersExecute(Sender: TObject);
    procedure AppEvents1Minimize(Sender: TObject);
    procedure NChangePasswordClick(Sender: TObject);

  private
    { Private declarations }
    CanClose :Boolean;
    CrtWasInit :Boolean;
    Procedure CreateInstallDir(Route,Value,AppFileName:String);
    Procedure ReadConfigurationFile;
    Procedure ShowCrt(FNum:LongInt;MVal:ShortString);
    procedure CrtClick(Sender: TObject);
    Procedure BuildAtmCrtPopupMenu(MasterMenu :TPopupMenu;IniFileName,IniSection :String;TheEvent :TNotifyEvent);
  public
    { Public declarations }
    ExecDir :String;
    CurBmpFileName :String;
    CurIniFileName :String;
    CurAliasName :String;

    procedure DrawGrids;
    Procedure ReadAtmAppLocation;
    procedure MenuOfReportCliCk (Sender: TObject);{Rani}
  end;


  Procedure OpenCrt(FormNum :LongInt;MainValue :ShortString);External 'AtmCrt.Dll';
  Procedure InitAtmCrt(AliasName,DirForIni:ShortString);External 'AtmCrt.Dll';
  Procedure DoneAtmCrt;External 'AtmCrt.Dll';

{  Procedure OpenNzr;External 'Nzr2Han.Dll';
  Procedure ShowNzr;External 'Nzr2Han.Dll';
  Procedure CloseNzr;External 'Nzr2Han.Dll';
}

  Procedure InitWinTuda(AliasName,DirForIni:ShortString);External 'WinTuda.Dll';
  Procedure OpenWinTuda(HazmanaNum:LongInt);External 'WinTuda.Dll';
  Procedure DoneWinTuda;External 'WinTuda.Dll';

  {Meholel.Dll}
  procedure OpenMeholel(SecureFromDll:Integer;UserNameFromDll,ReportDirectoryFromdll:Pchar);External 'Meholel.Dll';
  procedure CloseMeholel;External 'Meholel.Dll';
  procedure ShowMain;External 'Meholel.Dll';
  procedure PrintDoh (FileName:PChar);External 'Meholel.Dll';

  {Selective}
  Function GetCurrentCompanyIni(IniFileName:ShortString):ShortString;External 'Selectiv.Dll';
  Procedure DeleteCompanyRegValues;External 'Selectiv.Dll';

  {Permit}
  Procedure InitPermit(IniFileName:ShortString);External 'Permit.Dll';
  Function AtmLogin(Var LoginName :ShortString) :Boolean;External 'Permit.Dll';
  Procedure AtmLogOut;External 'Permit.Dll';
  Procedure DonePermit;External 'Permit.Dll';
  Procedure ManagePermitions;External 'Permit.Dll';
  Procedure AddUser;External 'Permit.Dll';
  Procedure ChangePassword;External 'Permit.Dll';
  Procedure DeleteUser;External 'Permit.Dll';
  Function CheckFormAccess(UserName,FormName :PChar) :Boolean;External 'Permit.Dll';

var

  Frm_AtmMainMenu: TFrm_AtmMainMenu;

implementation
Uses AtmConst, AtmRutin, FSplash;


{$R *.DFM}
Var
   CurUserName :String;
procedure TFrm_AtmMainMenu.Action_ShowMapBmpExecute(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open', PChar(ExecDir+'\Menu.Bmp'), Nil, 'c:\', 0);
end;

procedure TFrm_AtmMainMenu.Action_CloseAppExecute(Sender: TObject);
begin
     CanClose:=True;
     Close;
end;

procedure TFrm_AtmMainMenu.Action_WinTudaExecute(Sender: TObject);
begin
     OpenWinTuda(0);
end;

procedure TFrm_AtmMainMenu.FormCreate(Sender: TObject);
Var
   StrH :String;
   R :TRegistry;
   TheUserName:ShortString;
begin
//     LoadMslDll;
  Try
    Screen.Cursor:=crHourGlass;

    CrtWasInit:=False;
    Application.BiDiMode:=bdRightToLeft;
    Application.BiDiKeyboard:=HebBidiKeyboard;
    CanClose:=False;
    SplashScreen:=TSplashScreen.Create(Self);
    Try
       SplashScreen.Show;
       Application.ProcessMessages;
       RxTrayIcon1.Hide;
{       StrH:=GetTempDir;
       if StrH[Length(StrH)]='\' Then
          StrH:=Copy(StrH,1,Length(StrH)-1);
       Session.PrivateDir:=StrH;}
    Except
          //Ignore
    End;
    CurIniFileName:=ExtractFilePath(Application.ExeName)+Atm2000IniFileName;

    SplashScreen.Hide;
    StrH:=GetCurrentCompanyIni(CurIniFileName);
    if StrH='-1' Then
    Begin
       CanClose:=True;
       Exit;
    End;
    SplashScreen.Show;
    if StrH<>'' Then
       CurIniFileName:=ExtractFilePath(Application.ExeName)+StrH;
   ReadConfigurationFile;
   Application.ProcessMessages;
   ExecDir:=ExtractFileDir(ParamStr(0));
   CurBmpFileName:='Menu.Bmp';
   DrawGrids;
   Application.ProcessMessages;
//     InitAtmCrt('');
   ReadAtmAppLocation;
   Application.ProcessMessages;
   InitPermit(CurIniFileName);
   if Not AtmLogin(TheUserName) Then
   Begin
        CanClose:=True;
        Exit;
//      Application.Terminate;
   End;
   CurUserName:=TheUserName;
   R:=TRegistry.Create;
   Try
      StrH:='';
      R.RootKey:=RegRootForAtm;
      R.OpenKey(RegKeyForAtm2000,True);
      if R.ValueExists(RegValueForUsersDir) Then
         StrH:=R.ReadString(RegValueForUsersDir);
      if StrH='' Then
         StrH:=ExecDir+'\Users';
      ForceDirectories(StrH+'\'+CurUserName);
      ExeMenu1.IniFileName:=StrH+'\'+CurUserName+'\'+ExeMenu1.IniFileName;
      ExeMenu1.FillMenu;
   Finally
          R.Free;
   End;
   InitWinTuda(CurAliasName,ExtractFilePath(Application.ExeName));
  Finally
         SplashScreen.Free;
         SplashScreen:=Nil;
         Screen.Cursor:=crDefault;
         RxTrayIcon1.Show;
         if CanClose Then
            Application.Terminate;
  End;
end;

procedure TFrm_AtmMainMenu.Action_OpenSadranExecute(Sender: TObject);
Var
   S :Pchar;
begin
     S:=StrAlloc(Length(Action_OpenSadran.Hint)+1);
     StrPCopy(S,Action_OpenSadran.Hint);
     WinExec(S,SW_RESTORE);
     StrDispose(S);
end;

procedure TFrm_AtmMainMenu.NEdit_ExecMenuClick(Sender: TObject);
begin
     exeMenu1.EditExeList;
end;

procedure TFrm_AtmMainMenu.DrawGrids;
Begin
//     Image1.Canvas.MoveTo
End;
procedure TFrm_AtmMainMenu.FormDestroy(Sender: TObject);
begin
    AtmLogOut;
    DonePermit;
    DeleteCompanyRegValues;
    CloseMeholel;
    DoneWinTuda;
    if CrtWasInit Then
      DoneAtmCrt;
    RxTrayIcon1.Hide;
end;

procedure TFrm_AtmMainMenu.Action_MeholelExecute(Sender: TObject);
begin
     OpenMeholel (9999,'�����','');
     showMain;
end;

Procedure TFrm_AtmMainMenu.ReadAtmAppLocation;
Var
   Reg:TRegistry;
Begin
     Reg:=TRegistry.Create;
     Try
         Reg.RootKey:=RegRootForAtm;
         if Reg.KeyExists(RegKeyForMaslulon) Then
         Begin
              Reg.OpenKey(RegKeyForMaslulon,False);
              if Reg.ValueExists(RegValueForInstallDir) Then
              Begin
                 Action_OpenMaslulon.Enabled:=True;
                 Action_OpenMaslulon.Hint:=Reg.ReadString(RegValueForInstallDir)+'\Prj_Msl.exe';
              End;
              Action_OpenMaslulon.Enabled:=(Action_OpenMaslulon.Hint<>'') And (FileExists(Action_OpenMaslulon.Hint));
              ActionList_Menu.UpdateAction(Action_OpenMaslulon);
         End;

         if Reg.KeyExists(RegKeyNzr98) Then
         Begin
              Reg.OpenKey(RegKeyNzr98,False);
              if Reg.ValueExists(RegValueForInstallDir) Then
                   Action_OPenNzr.Hint:=Reg.ReadString(RegValueForInstallDir)+'\Netzer.Exe';
              Action_OpenNzr.Enabled:=(Action_OPenNzr.Hint<>'') And (FileExists(Action_OPenNzr.Hint));
         End;

         if Reg.KeyExists(RegKeyForSadran) Then
         Begin
              Reg.OpenKey(RegKeyForSadran,False);
              if Reg.ValueExists(RegValueForInstallDir) Then
              Begin
                 if Reg.ValueExists(RegKeyForAppFileName) Then
                    Action_OpenSadran.Hint:=Reg.ReadString(RegValueForInstallDir)+'\'+Reg.ReadString(RegKeyForAppFileName)
                 Else
                     Action_OpenSadran.Hint:=Reg.ReadString(RegValueForInstallDir)+'\AtmPar.exe 3';
                 Action_OpenSadran.Enabled:=True;
              End;
              Action_OpenSadran.Enabled:=(Action_OpenSadran.Hint<>'');
         End;

         if Reg.KeyExists(RegKeyForATMTool) Then
         Begin
              Reg.OpenKey(RegKeyForAtmTool,False);
              if Reg.ValueExists(RegValueForInstallDir) Then
              Begin
                 if Reg.ValueExists(RegKeyForAppFileName) Then
                 Begin
                    Action_OpenAtmTools.Hint:=Reg.ReadString(RegValueForInstallDir)+'\'+Reg.ReadString(RegKeyForAppFileName)+'1 1';
                    Action_AtmToolBackup.Hint:=Reg.ReadString(RegValueForInstallDir)+'\'+Reg.ReadString(RegKeyForAppFileName)+'1 4';
                    Action_AtmToolRestore.Hint:=Reg.ReadString(RegValueForInstallDir)+'\'+Reg.ReadString(RegKeyForAppFileName)+'1 5';
                 End
                 Else
                 Begin
                     Action_OpenAtmTools.Hint:=Reg.ReadString(RegValueForInstallDir)+'\MainTool.Exe 1 1';
                     Action_AtmToolBackup.Hint:=Reg.ReadString(RegValueForInstallDir)+'\MainTool.Exe 1 4';
                     Action_AtmToolRestore.Hint:=Reg.ReadString(RegValueForInstallDir)+'\MainTool.Exe 1 5';
                 End;
                 Action_OpenAtmTools.Enabled:=True;
                 Action_AtmToolBackup.Enabled:=True;
                 Action_AtmToolRestore.Enabled:=True;
              End;
              Action_OpenAtmTools.Enabled:=(Action_OpenAtmTools.Hint<>'');
         End;

     Finally
            Reg.Free;
     End;
End;
procedure TFrm_AtmMainMenu.Action_OpenMaslulonExecute(Sender: TObject);
begin
     WinExec(PChar(Action_OpenMaslulon.Hint),SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.Action_OpenNzrExecute(Sender: TObject);
begin
     WinExec(PChar(Action_OpenNzr.Hint),SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.Action_OpenAtmToolsExecute(Sender: TObject);
begin
     WinExec(PChar(Action_OpenAtmTools.Hint),SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.NCustomizeSpeedBarClick(Sender: TObject);
begin
     SpeedBar1.Customize(0);
     ReadAtmAppLocation;
end;

Procedure TFrm_AtmMainMenu.CreateInstallDir(Route,Value,AppFileName:String);
Var
   Reg:TRegistry;
Begin
     Reg:=TRegistry.Create;
     Try
         Reg.RootKey:=RegRootForAtm;
         Reg.OpenKey(Route,True);
         if Not Reg.ValueExists(RegValueForInstallDir) Then
              Reg.WriteString(RegValueForInstallDir,Value)
         Else
             if MessageDlg(Value+' ��� ������ ���� � '+Reg.ReadString(RegValueForInstallDir)+' ���� ��� ',
                mtConfirmation,[mbYes,mbNo],0)=mrYes Then
                Reg.WriteString(RegValueForInstallDir,Value);
         if AppFileName<>'' Then
             //if Not Reg.ValueExists(RegKeyForAppFileName) Then
                  Reg.WriteString(RegKeyForAppFileName,AppFileName);
     Finally
         Reg.Free;
     End;
End;

procedure TFrm_AtmMainMenu.NDefinePosNzrClick(Sender: TObject);
begin
     OpenDialog1.Filter:='��� Netzer.Exe|Netzer.Exe';
     if OpenDialog1.Execute Then
     Begin
          if CompareText(ExtractFileName(OpenDialog1.FileName),'Netzer.Exe')=0 Then
             CreateInstallDir(RegKeyNzr98,ExtractFileDir(OpenDialog1.FileName),'Netzer.Exe');
          ReadAtmAppLocation;
     End;
end;

procedure TFrm_AtmMainMenu.NDefinePosMaslulonClick(Sender: TObject);
begin
     OpenDialog1.Filter:='������� Prj_Msl.Exe|Prj_Msl.Exe';
     if OpenDialog1.Execute Then
     Begin
          if CompareText(ExtractFileName(OpenDialog1.FileName),'Prj_Msl.Exe')=0 Then
             CreateInstallDir(RegKeyForMaslulon,ExtractFileDir(OpenDialog1.FileName),'Prj_Msl.Exe');
          ReadAtmAppLocation;
     End;
end;

procedure TFrm_AtmMainMenu.NDefinePosSadranClick(Sender: TObject);
begin
     OpenDialog1.Filter:='���� *.Exe|*.Exe';
     if OpenDialog1.Execute Then
     Begin
          CreateInstallDir(RegKeyForSadran,ExtractFileDir(OpenDialog1.FileName),ExtractFileName(OpenDialog1.FileName));
          ReadAtmAppLocation;
     End;
end;

procedure TFrm_AtmMainMenu.NDefinePosAtmToolClick(Sender: TObject);
begin
     OpenDialog1.Filter:='������� ���� MainTool.Exe|MainTool.Exe';
     if OpenDialog1.Execute Then
     Begin
          CreateInstallDir(RegKeyForAtmTool,ExtractFileDir(OpenDialog1.FileName),ExtractFileName(OpenDialog1.FileName));
          ReadAtmAppLocation;
     End;
end;

procedure TFrm_AtmMainMenu.Action_NotInstalledExecute(
  Sender: TObject);
begin
     ShowMessage('�� �����');
end;
procedure TFrm_AtmMainMenu.Action_AtmToolBackupExecute(Sender: TObject);
begin
     WinExec(PChar(Action_AtmToolBackup.Hint),SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.Action_AtmToolRestoreExecute(Sender: TObject);
begin
     WinExec(PChar(Action_AtmToolRestore.Hint),SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.MenuOfReportCliCk (Sender: TObject);{Rani}
Var
   FileName : Pchar;
begin
       OpenMeholel(9999,'','');
       StrPcopy (FileName,(Sender As TMenuItem).Hint);
       PrintDoh (FileName);
end;

procedure GetReportMenu (ProgramIniName : String;MenuItem1:TMenuItem);{Rani}
        procedure InsetrMenuItem (Source,TmpItem :TMenuItem;Caption,Hint : String; Event : TNotifyEvent);
        begin
            TmpItem := TMenuItem.Create (Frm_AtmMainMenu);
            TmpItem.Caption := Caption;
            TmpItem.Hint := Hint;
            TmpItem.OnClick :=  Event;
            Source.Add (TmpItem);
        end;
(***************)
    procedure  Build_NetzerMenu (Menu:TMenuItem);
    var
       i : integer;
       TmpMenu : TMenuItem;
    begin
        i := 0;
        While i < Menu.Count do
            Menu.Delete (i);
        InsetrMenuItem (Menu,TmpMenu,'����� �� �������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������ �������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������ ���','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'�������� �����','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'�����','',Nil);
              InsetrMenuItem (Menu[8],TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[8],TmpMenu,'�����','',Nil);
              InsetrMenuItem (Menu[8],TmpMenu,'�����','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[9],TmpMenu,'�������','',Nil);
              InsetrMenuItem (Menu[9],TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[9],TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[9],TmpMenu,'�����','',Nil);
              InsetrMenuItem (Menu[9],TmpMenu,'�����','',Nil);
              InsetrMenuItem (Menu[9],TmpMenu,'�����','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'�������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'��������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
    end;
(***************)
    procedure  Build_AtmMenu (Menu:TMenuItem);
    var
       i : integer;
       TmpMenu : TMenuItem;
    begin
        i := 0;
        While i < Menu.Count do
            Menu.Delete (i);
        InsetrMenuItem (Menu,TmpMenu,'����� �� �������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������ �������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������ �������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'����� ���','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'������','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'����� ����','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'�����','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'����� ������','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'�����','',Nil);
              InsetrMenuItem (Menu[3],TmpMenu,'�����','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'��������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
        InsetrMenuItem (Menu,TmpMenu,'������','',Nil);
    end;
(***************)
    Function FindIndexMenuByName (Menu : TMenuItem; Name : String):Integer;
    var
       i : integer;
       Found :Boolean;
    begin
            Found := False;
            i := 0;
            While (i < Menu.Count)And (Found = False) do
               if Name = Menu.Items[i].Caption then
                  Found := True
               else
                  inc (i);
            if Found then
               Result := i
            else
                Result := -1;
    end;
(***************)

var
    TmpItem : TMenuItem;
    SRec    : TSearchRec;
    Res,I,Group,Subject : Integer;
    TmpIni : TIniFile;
    ReportDir : String;
    Netzer : Boolean;
begin {GetReportMenu}
    Netzer := UpperCase (ProgramIniName) = UpperCase ('c:\windows\ReNetzer.ini');
    if Netzer Then
       Build_NetzerMenu (MenuItem1)
    else
       Build_AtmMenu (MenuItem1);
    {exit;}
    i := 0;
    TmpIni := TIniFile.Create (ProgramIniName);
    ReportDir := TmpIni.ReadString('Main','ReportDir','c:\');
    TmpIni.Free;
    TmpIni := nil;
    Try
       Res:=FindFirst(ReportDir+'*.Rpt',faAnyFile,SRec);
       while Res = 0 do
       begin
             TmpIni := TIniFile.Create (ReportDir+Srec.Name);
              If (TmpIni.ReadInteger('Main','Secure',-1) <= {UserSecure}9999) then
              begin
                if Netzer Then
                   Group   := FindIndexMenuByName (Frm_AtmMainMenu.N_SaveDohNetzer,TmpIni.ReadString('Main','Group',''))
                else
                   Group   := FindIndexMenuByName (Frm_AtmMainMenu.N_SaveDohSa,TmpIni.ReadString('Main','Group',''));
                if Group <> -1 Then
                begin
                   if Netzer Then
                      Subject := FindIndexMenuByName (Frm_AtmMainMenu.N_SaveDohNetzer.Items[Group],TmpIni.ReadString('Main','Subject',''))
                   else
                      Subject := FindIndexMenuByName (Frm_AtmMainMenu.N_SaveDohSa.Items[Group],TmpIni.ReadString('Main','Subject',''))
                end
                else
                    Subject := -1;
                if subject <> -1 then
                begin
                   if netzer Then
                      InsetrMenuItem (Frm_AtmMainMenu.N_SaveDohNetzer.items[Group][Subject],TmpItem,TmpIni.ReadString('Main','Title','�� ����� �����'),Srec.Name,Frm_AtmMainMenu.MenuOfReportCliCk)
                   else
                      InsetrMenuItem (Frm_AtmMainMenu.N_SaveDohSa.items[Group][Subject],TmpItem,TmpIni.ReadString('Main','Title','�� ����� �����'),Srec.Name,Frm_AtmMainMenu.MenuOfReportCliCk)
                end
                else
                    if Group <> -1 then
                    begin
                       if Netzer Then
                           InsetrMenuItem (Frm_AtmMainMenu.N_SaveDohNetzer.Items[Group],TmpItem,TmpIni.ReadString('Main','Title','�� ����� �����'),Srec.Name,Frm_AtmMainMenu.MenuOfReportCliCk)
                       else
                           InsetrMenuItem (Frm_AtmMainMenu.N_SaveDohSa.Items[Group],TmpItem,TmpIni.ReadString('Main','Title','�� ����� �����'),Srec.Name,Frm_AtmMainMenu.MenuOfReportCliCk)
                    end
                    else
                      if Netzer Then
                          InsetrMenuItem (Frm_AtmMainMenu.N_SaveDohNetzer.Items[0],TmpItem,TmpIni.ReadString('Main','Title','�� ����� �����'),Srec.Name,Frm_AtmMainMenu.MenuOfReportCliCk)
                      else
                          InsetrMenuItem (Frm_AtmMainMenu.N_SaveDohSa.Items[0],TmpItem,TmpIni.ReadString('Main','Title','�� ����� �����'),Srec.Name,Frm_AtmMainMenu.MenuOfReportCliCk)
              end;
          TmpIni.Free;
          Res:=FindNext(SRec);
        end;{While Rec = 0}
        FindClose(SRec);
    except on e:Exception do
         ShowMessage (E.Message);
    end;{Try}
end;


procedure TFrm_AtmMainMenu.Spb_ReportsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Try
    Screen.Cursor:=crHourGlass;
    GetReportMenu(GetWinDir+'\ReNetzer.ini',Frm_AtmMainMenu.N_SaveDohNetzer);
    GetReportMenu(GetWinDir+'\ReAtmPar.ini',Frm_AtmMainMenu.N_SaveDohSa);
  Finally
    Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_AtmMainMenu.Action_ShibAutoShemExecute(Sender: TObject);
begin
    WinExec('ShibShem.exe',SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.Action_OpenHeshbonExecute(Sender: TObject);
begin
     WinExec('WinHshbn.exe',SW_RESTORE);
end;

procedure TFrm_AtmMainMenu.Spb_WinTudaMouseEnter(Sender: TObject);
begin
     TSpeedItem(Sender).Font.Color:=clRed;
end;

procedure TFrm_AtmMainMenu.Spb_WinTudaMouseLeave(Sender: TObject);
begin
     TSpeedItem(Sender).Font.Color:=clBlack;
end;

procedure TFrm_AtmMainMenu.RxTrayIcon1DblClick(Sender: TObject);
begin
     ShowWindow(Application.Handle,SW_SHOW);
     ShowWindow(Self.Handle,SW_SHOW);
     if WindowState=WSMinimized Then
        WindowState:=WSNormal;
     Show;
     Application.BringToFront;
end;

procedure TFrm_AtmMainMenu.ActionList_MenuExecute(Action: TBasicAction;
  var Handled: Boolean);
Var
   I,J :LongInt;
begin
    For I:=0 To SpeedBar1.SectionCount-1 Do
        For J:=0 To SpeedBar1.ItemsCount(i)-1 Do
         SpeedBar1.Items(i,J).Font.Color:=clBlack;
end;

Procedure TFrm_AtmMainMenu.ReadConfigurationFile;
Var
   F:TIniFile;
Begin
    F:=TIniFile.Create(CurIniFileName);
    Try
       Caption:=Caption+'-����� '+F.ReadString(SectionForMain,KeyForCompanyName,'������ ���� �����');
       CurAliasName:=F.ReadString(SectionForMain,KeyForAliasName,'Atm');
       StatusBar1.SimpleText:=CurAliasName+' �� ���� ������� ';
    Finally
           F.Free;
    End;
    BuildAtmCrtPopupMenu(PopupMenu_Crt,CurIniFileName,'CrtMain',CrtClick);
End;

Procedure TFrm_AtmMainMenu.BuildAtmCrtPopupMenu(MasterMenu :TPopupMenu;IniFileName,IniSection :String;TheEvent :TNotifyEvent);
Var
  T :TIniFile;
  S :TstringList;
  TempMenu:TMenuItem;
  I:longInt;
Begin
  T:=TIniFile.Create(IniFileName);
  S:=TStringList.Create;
  Try
    T.ReadSectionValues(IniSection,S);
    For I:=0 To S.Count-1 Do
    Begin
        TempMenu:=TMenuItem.Create(MasterMenu);
        TempMenu.Caption:=S.Names[i];
        TempMenu.Tag:=StrToInt(S.Values[S.Names[i]]);
        TempMenu.OnClick:=TheEvent;
        MasterMenu.Items.Add(TempMenu);
    End;

  Finally
      T.Free;
      S.Free;
  End;
End;


procedure TFrm_AtmMainMenu.CrtClick(Sender: TObject);
Begin
    OpenCrt(TComponent(Sender).Tag,'');
End;

procedure TFrm_AtmMainMenu.Action_ManageSecurityExecute(Sender: TObject);
begin
     if CheckFormAccess(PChar(CurUserName),'Frm_Permision') Then
        ManagePermitions
     Else
         MessageDlg('��� �� ������ ����� ���� ��',mtError,[mbOk],0);
end;

procedure TFrm_AtmMainMenu.Action_AddUsersExecute(Sender: TObject);
begin
     if CheckFormAccess(PChar(CurUserName),'Frm_Permision') Then
        AddUser
     Else
         MessageDlg('��� �� ������ ����� ���� ��',mtError,[mbOk],0);
end;

procedure TFrm_AtmMainMenu.Action_DelUsersExecute(Sender: TObject);
begin
     if CheckFormAccess(PChar(CurUserName),'Frm_Permision') Then
        DeleteUser
     Else
         MessageDlg('��� �� ������ ����� ���� ��',mtError,[mbOk],0);
end;

procedure TFrm_AtmMainMenu.AppEvents1Minimize(Sender: TObject);
begin
     ShowWindow(Application.Handle,SW_HIDE);
     ShowWindow(Self.Handle,SW_HIDE);
end;

Procedure TFrm_AtmMainMenu.ShowCrt(FNum:LongInt;MVal:ShortString);
Begin
    if Not CrtWasInit Then
    Begin
      InitAtmCrt(CurAliasName,ExtractFilePath(Application.ExeName));
      CrtWasInit:=True;
    End;
    OpenCrt(FNum,MVal);
End;
procedure TFrm_AtmMainMenu.NChangePasswordClick(Sender: TObject);
begin
  ChangePassword;
end;

end.

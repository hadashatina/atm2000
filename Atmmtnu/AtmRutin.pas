{1800525354}
{ 16/03/99 11:20:04 > [Aviran on AVIRAN] check in: (1.0)  / None }
unit AtmRutin;

interface

uses
   AtmComp,Mapi, DBTables, classes, SysUtils,Controls,DB,AtmLookupCombo,DbSrtGrd,Graphics,
   AtmDbDateEdit,Dialogs,AtmListDlg,quickrpt,Printers ,QrPrnTr,Consts,Menus,
   Windows,ComCtrls,DBCtrls,DBGrids,ShellApi,Forms,TypInfo,Grids,StdCtrls,Messages,
   MslDllInterface,IniFiles,AdvSearch,AtmAdvTable,Math,ExtCtrls,AtmRxQuery,RxQuery
   ,ComObj,AtmConst, Registry, WinINet, Gauges, AdoDB;

const
    Win2000FlipFields: array[0..8] of String = ('FAX','TEL','PNIMI','CARN','RISH','PHON', 'OSEK_MORSHE', 'KOD_REHEV','ASMACHTA');// (RISHUI, PHJ);

type TIntObj = class
       Kod : longint;
       constructor Create(AKod: longint);
     end;

Type
    TConnectInfo = Class(TSTringList)
    Private
       FFileName:String;


       FIniF:TIniFile;
    Public
       FPckSize:Integer;
       FServer:String;
       FDB:String;
       FUSer:String;
       FPass:String;
       FAlias:String;
       FLock:Byte;
       FIso:Byte;
       FCursorT:Byte;
       FCursorP:Byte;
       FRecCache:Integer;
       FConnectNormal:Boolean;
       constructor Create(FileName:String);
       destructor Destroy;
       Procedure LoadConnect(Var Server, DBName, User, Pass, Alias:String; Var Lock, Iso, CursorT, CursorP:Byte; Var PackSize, RecCache:Integer; Var ConnectNormal:Boolean);overload;
       Procedure SaveConnect(Server, DBName, User, Pass, Alias:String; Lock, Iso, CursorT, CursorP:Byte; PackSize, RecCache:Integer; ConnectNormal:Boolean);
       Procedure LoadConnect(ByAlias:String=''); overload;
       Function ReturnADO:String;
    End;

Type
    TMhirClass = Class
              Key :LongInt;
              FPrice1,
              FPrice2,
              FPrice3,
              FPrice4 :Real;
              SugBhira,
              GroupNum :LongInt;
              Constructor Create(AKey:LongInt;APrice1,APrice2,APrice3,APrice4:Real;ASugBhira,AGroupNum:LongInt);
    End;
    TMhirList = Class(TList)
       Function FindItem(Key :LongInt):Pointer;
       Destructor Destroy;
    End;
Type
   CharSet= Set of Char;
Type
    THebHintWindow = Class(THintWindow)
    Protected
             Procedure Paint;Override;
    End;

Type //Number to text
  YHIDType = Record
             HebString :String[6];
             WavFileName :String[150];
  End;

Type
    TDllProcNoParam=Procedure;
    TDllProc1PCharParam=Procedure(Param1 :PChar);
    TDllProc2PCharParam=Procedure(Param1,Param2 :PChar);
    TDllProc3PCharParam=Procedure(Param1,Param2,Param3 :PChar);
    TDllProc4PCharParam=Procedure(Param1,Param2,Param3,Param4 :PChar);
    TDllProc5PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5 :PChar);
    TDllProc6PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5,Param6 :PChar);
    TDllProc7PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5,Param6,Param7 :PChar);
    TDllProc8PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8 :PChar);
    TDllProc9PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9 :PChar);
    TDllProc10PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10 :PChar);
    TDllProc11PCharParam=Procedure(Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10,Param11 :PChar);
    TDllProc1IntParam=Procedure(Param1:LongInt);
    TDllProc2IntParam=Procedure(Param1,Param2:LongInt);
    TDllProc6IntParam=Procedure(Param1,Param2, P3, P4, P5, P6:LongInt);
    TDllProcOpenMeholl=Procedure(Param1:LongInt;Param2,Param3:PChar);
    TDllProc1Int2StrParam=Procedure(Param1:LongInt; AStr1, AStr2:ShortString);
    TDllFunc1StrParam=Function(AStr :ShortString):ShortString;
    TDllFunc2StrParamBoolResult=Function(AStr1, AStr2 :ShortString):Boolean;
    TDllFunc2PCharParamBool=Function(Param1,Param2 :PChar):Boolean;
    TDllFuncStringNoParam=Function :ShortString;
    TDllOpenMslPointForm = Function (TheNodeList :TNodeList;AutoCalcMaslul :Boolean) :Boolean;
    TDllScanImage=Procedure (var Image1:TImage);
    ELibNotFound = Class(Exception);
    ELowSysRes = Class(Exception);
function GetInetFile
(const fileURL, FileName: String; ProgBar:TGauge; Var DoAbort:Boolean): boolean;


// (���) EXE �� DLL ������� ����� �� ���� ������ �� ����
Function GetFileVersion(ProgName:String; Major, Minor, Release, Build:pWord):Boolean;
//-------------------- ������� ����� ������
function LeftToRight(InStr:string):string;
function RightCut(St1: String; LenOf: Integer): String;
function FillString(Fill: Char; Count: Byte): String;
{function LeftToRight(InStr:shortstring):shortstring;}
//������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                    FieldNum :LongInt): String;
// ���� ���� ������� ������ �"� �� �����
Function CountFieldsInSepString(TheString:String;TheSeparator :Char):LongInt;
//--------------- ������� ������ ����� ����� ���� ������
Function DelChars(TheString :ShortString;CharsToDel :CharSet):ShortString;
//������� ������ ������ ����� ��� �� �����
Function ConvertToRishuy(InStr :String) :String;
//---------------- �������� ������ ���� ���� ����� �����
procedure CreateNewTable(Sender: TComponent;
     DbNameIn,TblNameIn,DBNameOut,TblNameOut:string;
     TblTypeOut : TTableType );
Function ConvertDSStateToText(TheDS :TDataSet):String;

Function GetDataSetFromTControl(TheControl :TControl):TDataSet;
Function GetDataSetFromTComponent(AComponent :TComponent):TDataSet;
Function GetDataSourceFromTControl(TheControl :TControl):TDataSource;
Function GetDataSourceFromTComponent(AComponent :TComponent):TDataSource;
Function GetDataDataFieldNameFromTControl(TheControl :TControl):ShortString;
Function GetLinkLabelTextFromTControl(TheControl :TControl):ShortString;
Procedure DuplicateOneRecord(TheTable :TDataSet;PostAfterDup,DupFieldsWithDefault:BOOLEAN;CancelEvents :Boolean = FALSE; FirstFieldNo: Integer=0);
Procedure CopyOneRecord(SourceDataSet,DestDataSet:TDataSet;PostAfterDup,DupFieldsWithDefault :Boolean); //����� ����� ����� �����
function GetProductVersion(FileName :String): String;
Function FindFirstControlInTabOrder(ParentControl :TWinControl):TWinControl;
function FindGlobalNextControl(CurControl,CurParent: TWinControl;
  GoForward, CheckTabStop, CheckParent: Boolean): TWinControl;

// CrtRutin
   Procedure SetAtmDbEditF4State(TheForm :TWinControl);//����� ����� �������
   Procedure KeyboardManageForTableAction(Sender :TObject;Var Key :Word;Shift: TShiftState;TheTable :TDataset);
   Procedure SaveF4ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
   Procedure ReadF4ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
   Procedure SaveF10ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
   Procedure ReadF10ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
   procedure RefreshAllSearchWindows(Sender: TForm);



//----------------------------------
Function MonthDiff(Date1, Date2:TDateTime):Integer;
Function AutoDateForField(const Text:String):String;
function IsLeapYear(AYear: Integer): Boolean;//��� ������
function DaysPerMonth(AYear, AMonth: Integer): Integer;//���� �����
function DaysInPeriod(Date1, Date2: TDateTime): Longint; //���� ������
Function TimeToMinutes(ATime :TDateTime):LongInt; //���� ��� �����
function ValidDate(ADate: TDateTime): Boolean;
function ValidStrDate(ADate: String): Boolean;

//LOOKUP ���� ����� �����
Procedure FillKodTavlaLookupList(TheDataset:TDataset;FieldsList :TStringList);
Procedure FillKodTavlaComboList(TheDataset:TDataset;FieldsList :TStringList);
Procedure FillKodTavla2ComboList(TheDataset:TDataset;FieldsList :TStringList;KeyFieldName,ValueFieldName :String);
Function ValidateTableLookupFields(TheDataSet:TDataSet):Boolean;
Procedure ReplaceTableNameForSql(AComponent :TComponent;Prefix :String);
Procedure KeyboardManageForTableActionOnForm(Sender :TObject;Key :Word;Shift: TShiftState;
                                       TheTable :TDBDataSet);
Procedure MarkSaveOnTable(TheTable :TDataset);//����� ����� �� ����� �����
Procedure PrintDbGrid(Koteret :String;TheGrid :TDbGrid);

Function GetWinDir  :String;
Function GetTempDir :String;
Function GetSysDir :String;
function GetProgDir: TFileName;
function UserName :String;
function ComputerName :String;
function DoEncryption(Src:string; Key: string; DecryptKey: integer; Encrypt: Boolean):string;
Function CrackPassword (Password :String):Boolean;
Function IntToLenStr(TheNumber,TheLength:LongInt; LeadingChar:Char):String;
Function ExtractFileExtention(FileName :String):String; //����� �� ��� ����� �� ������ �� ���� �����
Function CopyAFile(AFrom,ATo :String):Boolean;//����� �����
Procedure CopyDir(FromDir,ToDir :String;FormHandle :THandle);
Function GetParamStr(ParamNum :LongInt): String;//ParamStr ����� ��
   { ************** }
function WinToDos(WinString : String) : String;
function WinToDos7Bit(WinString : String) : String;
function DosToWin(WinString : String) : String;
function AddLeadeSpace(St1:String;Len: Integer): String;

Procedure RunDllProcPCharParam(DllName,DllProc:String;Prm :Array of String;FreeHandle :Boolean;Var DllHandle :THandle);
Procedure RunDllNoParam(DllName,DllProc :String;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);
Procedure RunDll1IntParam(DllName,DllProc :String;Param1 :LongInt;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);
Procedure RunDll2IntParam(DllName,DllProc :String;Param1,Param2 :LongInt;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);
Procedure RunDllOpenMeholel(DllName,DllProc :String;Param1:LongInt;Param2,Param3:String;FreeHandle :Boolean;Var DllHandle :THandle);
Procedure RunDll1PCharParam(DllName,DllProc,Param1 :String);
Procedure RunDll3PCharParam(DllName,DllProc,Param1,Param2,Param3 :String;FreeHandle :Boolean;Var DllHandle :THandle);
Procedure RunDll1Int2StrParam(DllName,DllProc :String;Param1 :LongInt; Param2, Param3:String; FreeHandle :Boolean;Var DllHandle :THandle);
Procedure RunDll6IntParam(DllName,DllProc :String;Param1,Param2, P3, P4, P5, P6 :LongInt;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);


Function  RunDllFunc2StringParamBoolResult(DllName,DllProc,Param1,Param2 :String):Boolean;
Function  RunDllFunc1StringParam(DllName,DllProc,Param1 :String):String;
Function  RunDllFunc2PCharParamBool(DllName,DllProc :String;Param1,Param2:String;FreeHandle :Boolean;Var DllHandle :THandle):Boolean;
Function  RunDllStringFuncNoParam(DllName,DllProc :String):ShortString;
Function  RunDllOpenMslPointForm(DllName,DllProc :String;TheNodeList :TNodeList;AutoCalcMaslul :Boolean) :Boolean;
Procedure RunDllScanImage(DllName,DllProc :String;var Image1:TImage;FreeHandle :Boolean;Var DllHandle :THandle);
Procedure FreeDllHandle(Var DllHandle :THandle);


/////////////////////////////////////////////////
Procedure GetMehironPrice(MehironKey,CodeHiuv,CodeSherut,DefaultSugLink:LongInt;
                          TnuaDataSet:TDataSet;TempQry :TQuery;
                          Var Price1,Price2,Price3,Price4:Real;
                          Var YMida:Integer;
                          IncludeNumber: Integer=1;
                          StopCalcInRange: Boolean=False);

Procedure BuildCrtMenu(MasterMenu :TMenuItem;IniFileName,IniSection :String;TheEvent :TNotifyEvent);
Function  DateToSqlStr(TheDate :TDateTime;FormatStr :String):String;
Procedure SetDatasetState(DataSet:TDataSet;TheState :Byte);
Function  ReverseWords(InString:String) :String;
Procedure BuildSqlScriptForInsert(TheStrings :TStringList;TheDataset :TDataset;TheTableName :String);
//Procedure BuildSqlScriptForModify(TheStrings :TStringList;TheDataset :TDataset;TheTableName :String);
Function AddQuatesToString(TheString :String ):String;
Procedure PutLookupFieldsAsDefaultFromIni(IniFileName,IniSection:String;MatchDataset,DestDataset:TDataset;TempRxQry :TRxQuery);

{Rani}
function DataSet2ExcelFile(TmpTable: TDataSet; FileName,SheetName: string ;FieldsName:TStringList = nil;RecordFont : Tfont = nil ; HeaderFont : Tfont=nil ): boolean;
function SendMail (ToMail , Subject : String ; Body : TStringList):Integer;
Function  MaskToNum (MaskStr:String):String;
Function  ReplaceString (SourceString,TextToFind,ReplaceWith : String;NumberOfReplace:Integer):String;{������ ����� ������� �����}
Function NumberToHebWords(TarNumber:real;WithShekel:boolean =True;Male:Boolean =True;CoinType:Integer = 0):string;
//Function NumberToHebWords(TarNumber:real;WithShekel:boolean =True;Male:Boolean =True):string;
function InWords(TheNumber:LongInt;Male:boolean = True):string;

function Mis_RishuiToInteger(Mis_Rishui: String): Integer;
function IntegerToMis_Rishui(Mis_Rishui: Integer): String;

function IsWindows2000: Boolean;
function ReverseString(sStr: String): String;
procedure SetWin2000Display(AComponent: TComponent);

Procedure ScanImage(var Image1 :TImage);

procedure DBGridWin2000ColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);


function CheckFlipField(sFieldName: String): Boolean;
{����� �� ����� ������� �������� ����� ������ �� �����}
procedure GetLoginFromSqlSever (AliasName , DatabaseName , UserName ,PassWord, SreverType: String; ListOfHost : TstringList);

Function FindDistance(TempQry:TQuery; Yaad, Motza:String; ReplaceToSpace:Boolean=False):Integer;
Function GetDBName(Alias:String):String;
Function GetServer(Alias:String):String;


Function F2S(F:Real; DigNum:Integer=2; DigNum2:Integer=8):String;
Function F2SC(F:Real; DigNum:Integer=2):String; // �� ���� �� ����� �����
Function S2F(S:String):Real;
Function F2I(F:Real):Integer;
Function I2F(I:Integer):Real;
Function I2S(I:Integer):String;
Function S2I(S:String):Integer;
Function D2S(D:TDateTime):String;
Function S2D(S:String):TDateTime;
Function T2S(T:TDateTime):String;
Function S2T(S:String):TDateTime;
Function DateFieldNull(Field:TField):Boolean;
Function Date2Iso(D:TDateTime):String;
Procedure ChangeDateFloatFormat(Tbl:TDataSet);

Procedure ShowProg1(ProgVal:String);
Procedure ShowProg2(ProgVal:String);
Procedure ShowProgCrt(CrtNum:Integer; ProgValOrMax:String=''; ProgTextOrMode:String=''; Pause:Boolean=False);

Function CalcDateTashlum(FromDate :TDateTime;PayCond :ShortString) : TDateTime;

Function GetTableList(Qry:TQuery; TblList:TStringList):Boolean;
Function GetFields(Qry:TQuery; FldList:TStringList; Tbl:String):Boolean;
Function TableExists(Qry:TObject; TblName:String):Boolean;
Function FieldExists(Qry:TQuery; StrLst:TStringList; TblName:String; FieldName:String; FieldParams:String=''; AutoCreate:Boolean=False):Boolean;

Function DoQryStr(Qry:TQuery; QryStr:String; UseExecSql:Boolean=False):Integer;
Function DoSelect(Qry:TQuery; Fields, Tbl, WhereStr:String; Params:String=''):Integer;

function FindIndObj(Kod:String; StrLst:TStringList):longint;
Procedure FillListBox(LB:TListBox; ValueToFind:String; StrLst:TStringList);


Procedure ReadADOInfo(FileName:String; Alias:String; Var CntInfo:TConnectInfo);
Procedure ApplyADO2Connect(CntInfo:TconnectInfo; Var Connection:TADOConnection);
Procedure ApplyADO2Qry(CntInfo:TconnectInfo; Var Query:TADOQuery);

implementation

Uses AtmLookCombo;

//-----------------------------------------------------------------------
//                          ������� ����� ������
{
function LeftToRight(InStr:shortstring):shortstring;
 var ix1:integer;
     HelpStr:string;
begin
  HelpStr:='';
  for ix1:= Length(InStr) downto 1 do
     HelpStr:=HelpStr+InStr[ix1];
  LeftToRight:=HelpStr;
end;
}
//-----------------------------------------------------------------------

Procedure ReadADOInfo(FileName:String; Alias:String; Var CntInfo:TConnectInfo);
Var
 St, St1, St2, St3, St4, St5, St6:String;
 NotIP, Bool1:Boolean;
 I, Cnt, Int1, Int2, Int3, Int4:Byte;
 IntArr:Array[1..4] of Integer;
 Int5:Integer;
Begin
{  If CB_ConnectWinTuda.Checked Then ST:=DirWintudaEXE+CompIni
  Else St:=DE_Scripts.Text+'\'+'Op'+ DBAlias+'.ini';
}
  If CntInfo=Nil Then
     CntInfo:=TConnectInfo.Create(FileName);

{  St1:=ServerName;
  St2:=ServerDB;
  St3:=DBUserName;
  St4:=DBPass;
  St5:=DBAlias;
  Int5:=1;
  Bool1:=True;        }
  Try
    CntInfo.LoadConnect(Alias);
  Finally
{    CntInfo1.Free;}
{    CntInfo1:=Nil;}
  End;
End;

Procedure ApplyADO2Connect(CntInfo:TconnectInfo; Var Connection:TADOConnection);
Begin
  Connection.Connected:=False;
  Connection.IsolationLevel:=TIsolationLevel(CntInfo.FIso);
  If CntInfo.FConnectNormal Then
     Connection.ConnectionString:=
        'Provider=SQLOLEDB.1;Persist Security Info=False;User ID='+
        CntInfo.FUser +
        ';Password="'+
        CntInfo.FPass+
        '";Packet Size='+
        I2S(CntInfo.FPckSize)+
        ';Initial Catalog='+
        CntInfo.FDB +
        ';Data Source='+
        CntInfo.FServer+';'
  Else
     Connection.ConnectionString:=
          'Provider=SQLOLEDB.1;'+
          'Integrated Security=SSPI;'+
          'User ID='+CntInfo.FUser+';'+
          'Initial Catalog='+CntInfo.FDB+';'+
          'Data Source='+CntInfo.FServer+';'+
          'Packet Size='+I2S(CntInfo.FPckSize)+';';
End;

Procedure ApplyADO2Qry(CntInfo:TconnectInfo; Var Query:TADOQuery);
Begin
  Query.Active:=False;
  Query.CacheSize:=CntInfo.FRecCache;
  Query.CursorLocation := TCursorLocation(CntInfo.FCursorP) ;
  Query.CursorType :=TCursorType(CntInfo.FCursorT) ;
  Query.LockType :=TADOLockType(CntInfo.FLock);
End;

  Procedure FillListBox(LB:TListBox; ValueToFind:String; StrLst:TStringList);
  Var I:Integer;
  Begin
    LB.Items.Clear;
    For I:=0 To StrLst.Count-1 Do
      If (Trim(ValueToFind)='') or (Pos(ValueToFind, StrLst[I])>0) Then
      Begin
        LB.Items.AddObject(StrLst[I], StrLst.Objects[I]);
      End;
  End;

function FindIndObj(Kod:String; StrLst:TStringList):longint;
var ix1 : longint;
Var
   MyObj : TIntObj;
begin
  Result:=-1;
  if StrLst.Count <> 0 then
  for ix1:=0 to StrLst.Count -1 do
  begin
    MyObj:=(StrLst.Objects[ix1] as TIntObj);
    if StrToInt(Kod) = MyObj.Kod then
    begin
      Result:=ix1;
      break;
    end;
  end;
end;

Constructor TIntObj.Create(AKod:longint);
begin
   inherited Create;
   Kod:=AKod;
end;

constructor TConnectInfo.Create(FileName:String);
Begin
  Inherited Create;
  FFileName:=FileName;
  FRecCache:=1;
  FUser:='sa';
  FPass:='';
  FConnectNormal:=True;
  FRecCache:=1;
  FPckSize:=4096;

  Try
    If Trim(FileName)<>'' Then
      FIniF:=TIniFile.Create(FileName);
  Except
    FileName:='';
  End;
End;
destructor TConnectInfo.Destroy;
Begin
  FIniF.Free;
  Inherited Destroy;
End;
Function TConnectInfo.ReturnADO:String;
Var
  St:String;
Begin
  If FConnectNormal Then
  Begin
    ST:=
'Provider=sqloledb;'+
'Data Source='+FServer+';'+
'Initial Catalog='+FDB+';'+
'User Id='+FUser+';'+
'Password='+FPass+';';
  End
  Else
  Begin
   St:=
'Provider=SQLOLEDB.1;'+
'Integrated Security=SSPI;'+
'Catalog='+FDB+';'+
'Data Source='+FServer;
  End;
  Result:=St;
End;
Procedure TConnectInfo.LoadConnect(ByAlias:String='');
Begin
  If FFileName='' Then Exit;
  If ByAlias<>'' Then
  Begin
    FServer:=GetServer(ByAlias);
    FDB:=GetDBName(ByAlias);
  End;
  If Pos('�����',FServer)>1 Then
    FServer:=FIniF.ReadString('Connect','Server',FServer);
  If Pos('�����',FDB)>1 Then
    FDB:=FIniF.ReadString('Connect','DBNAme',FDB);
  FUser:=FIniF.ReadString('Connect','User',FUser);
  FPass:=FIniF.ReadString('Connect','Pass',FPass);
  FAlias:=FIniF.ReadString('Connect','Alias',FAlias);
  FLock:=FIniF.ReadInteger('Connect','Lock',FLock);
  FIso:=FIniF.ReadInteger('Connect','Iso',FIso);
  FCursorT:=FIniF.ReadInteger('Connect','CursorT',FCursorT);
  FCursorP:=FIniF.ReadInteger('Connect','CursorP',FCursorP);
  FRecCache:=FIniF.ReadInteger('Connect','RecCache',FRecCache);
  FConnectNormal:=FIniF.ReadBool('Connect','ConnectNormal',FConnectNormal);
  FPckSize:=FIniF.ReadInteger('Connect','PackSize',FPckSize);
End;
Procedure TConnectInfo.LoadConnect(Var Server, DBName, User, Pass, Alias:String; Var Lock, Iso, CursorT, CursorP:Byte; Var PackSize, RecCache:Integer; Var ConnectNormal:Boolean);
Begin
  If FFileName='' Then Exit;
  Server:=FIniF.ReadString('Connect','Server',Server);
  DBNAme:=FIniF.ReadString('Connect','DBNAme',DBNAme);
  User:=FIniF.ReadString('Connect','User',User);
  Pass:=FIniF.ReadString('Connect','Pass',Pass);
  Alias:=FIniF.ReadString('Connect','Alias',Alias);
  Lock:=FIniF.ReadInteger('Connect','Lock',Lock);
  Iso:=FIniF.ReadInteger('Connect','Iso',Iso);
  CursorT:=FIniF.ReadInteger('Connect','CursorT',CursorT);
  CursorP:=FIniF.ReadInteger('Connect','CursorP',CursorP);
  RecCache:=FIniF.ReadInteger('Connect','RecCache',RecCache);
  PackSize:=FIniF.ReadInteger('Connect','PackSize',PackSize);
  ConnectNormal:=FIniF.ReadBool('Connect','ConnectNormal',ConnectNormal);
End;
Procedure TConnectInfo.SaveConnect(Server, DBName, User, Pass, Alias:String; Lock, Iso, CursorT, CursorP:Byte; PackSize, RecCache:Integer; ConnectNormal:Boolean);
Begin
  FIniF.WriteString('Connect','Server',Server);
  FIniF.WriteString('Connect','DBName',DBName);
  FIniF.WriteString('Connect','User',User);
  FIniF.WriteString('Connect','Pass',Pass);
  FIniF.WriteString('Connect','Alias',Alias);
  FIniF.WriteInteger('Connect','Lock',Lock);
  FIniF.WriteInteger('Connect','Iso',Iso);
  FIniF.WriteInteger('Connect','CursorT',CursorT);
  FIniF.WriteInteger('Connect','CursorP',CursorP);
  FIniF.WriteInteger('Connect','RecCache',RecCache);
  FIniF.WriteBool('Connect','ConnectNormal',ConnectNormal);
  FIniF.WriteInteger('Connect','PackSize',PackSize);
End;

Constructor TMhirClass.Create(AKey:LongInt;APrice1,APrice2,APrice3,APrice4:Real;ASugBhira,AGroupNum:LongInt);
Begin
     Inherited Create;
     Key:=AKey;
     FPrice1:=APrice1;
     FPrice2:=APrice2;
     FPrice3:=APrice3;
     FPrice4:=APrice4;
     SugBhira:=ASugBhira;
     GroupNum:=AGroupNum;
End;

Function TMhirList.FindItem(Key:LongInt):Pointer;
Var
   I :LongInt;
Begin
     Result:=Nil;
     For I:=0 TO Count-1 Do
     Begin
          if TMhirClass(Items[I]).Key=Key Then
          Begin
              Result:=Items[I];
              Break;
          End;
     End;
End;

Destructor TMhirList.Destroy;
Begin
     While Count>0 Do
     Begin
           TMhirClass(Items[0]).Free;
           Delete(0);
     End;
     Clear;
     Inherited Destroy;
End;


Function AutoDateForField(const Text:String):String;
Var C, I:Integer;
   Month, Year, Day:String;

  Function IsNum(Ch:Char):Boolean;
  Begin
    If ((Ch>='0') and (Ch<='9')) or (Ch='/') Then Result:=True
    Else Result:=False;
  End;
begin
  I:=0;
  Month:='';
  Day:='';
  Year:='';
  Repeat
     Inc(I);
     Case I Of
       1..2:If IsNum(Text[I]) Then day:=day+Text[I];
       4..5:If IsNum(Text[I]) Then month:=Month+Text[I];
       7..10:If IsNum(Text[I]) Then Year:=Year+Text[I];
     End;
  Until (I=Length(Text)) or( Not IsNum(Text[I]));

  IF Not IsNum(Text[I]) Then
  Begin
     If Year='' Then
       Year:=FormatDateTime('yyyy',Now);

     If Month='' Then
       Month:=FormatDateTime('mm',Now);
     If Day='' Then
       Day:=FormatDateTime('dd',Now);
     If Length(Day)<>2 Then Day:='0'+Day[1];
     If Length(Month)<>2 Then Month:='0'+Month[1];
     If Length(Year)<>4 Then
     Begin
       If StrToInt(Trim(Year))>=0 Then Year:=IntToStr(2000+StrToInt(Trim(Year)))
       Else Year:=IntToStr(1900+StrToInt(Trim(Year)))
     End;
  End;
  Result:=(Day+'/'+Month+'/'+Year);
End;

function LeftToRight(InStr:string):string;
 var
   ix1,i:integer;
   HelpStr,Tmp,Tmp1:string;
begin
  HelpStr:='';
  Tmp := '';
  Tmp1:='';
  for ix1:= Length(InStr) downto 1 do
     HelpStr:=HelpStr+InStr[ix1];
  for ix1:= 1 to Length(HelpStr) Do
  begin
      if ((HelpStr[ix1] <'0') Or (HelpStr[ix1] >'9'))And (HelpStr[ix1]<>'.')And (HelpStr[ix1]<>',') Then
      begin
         For i:= Length (tmp1)downto 1  do
            Tmp:=Tmp+Tmp1[i];
         Tmp1:='';
         Tmp:=Tmp+HelpStr[ix1];
      end
      else
        Tmp1 := Tmp1+HelpStr[ix1]
  end;
    For i:= Length (tmp1)downto 1  do
       Tmp:=Tmp+Tmp1[i];
   LeftToRight:=tmp;
end;
  { ***************** }

Function RightCut(St1: String; LenOf: Integer): String;
  var
    I: Integer;
    LenOfStr: Integer;
    St2: String;
  begin
    St2:='';
    LenOfStr:=Length(St1);
    St2:=Copy(St1,LenOfStr-LenOf+1,LenOf);
    RightCut:=St2;
  end;
  
function FillString(Fill: Char; Count: Byte): String;
var
  StrH: String;
begin
  StrH := '';
  FillChar(StrH, Count, fill);
  SetLength(StrH,Count);
  Result := StrH;
end;

//         ������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                         FieldNum:LongInt): String;
Var
   StrH :String;
   Ix1,FieldCount,FieldPos :LongInt;

Begin
    if TheString='' Then
    Begin
        Result:='';
        Exit;
    End;
    if (FieldNum=1) Then
    Begin
        if TheString[1]=Separator Then
             Result:=''
        Else
        Begin
             StrH:=Copy(TheString,1,Pos(Separator,TheString)-1);
             if StrH='' Then StrH:=TheString;
             {EndOfString:=Copy(TheString,Pos(Separator,TheString)+1,Length(TheString)-Length(StrH)+1);}
             Result:=StrH;
        End;
    End
    Else {FieldNum>1}
    Begin
         FieldCount:=1;
         For Ix1:=1 To Length(TheString) Do
            if TheString[Ix1]=Separator Then
            Begin
                 FieldPos:=Ix1+1;
                 FieldCount:=FieldCount+1;
                 if FieldCount=FieldNum Then Break;
            End;
         if (FieldCount=FieldNum) And (FieldPos<=Length(TheString)) Then
         Begin
             StrH:=Copy(TheString,FieldPos,Length(TheString)-FieldPos+1);
             if Pos(Separator,StrH)>0 Then
                StrH:=Copy(StrH,1,Pos(Separator,StrH)-1);
             Result:=StrH;
         End
         Else
             Result:='';
    End;
End;
//-----------------------------------------------------------------------
//               ������� ������ ����� ����� ���� ������
Function DelChars(TheString :ShortString;CharsToDel :CharSet):ShortString;
Var
   I    :Integer;
   StrH :ShortString;
Begin
     StrH:='';
     For I:=1 To Length(TheString) Do
        if Not (TheString[I] in CharsToDel) Then
           StrH:=StrH+TheString[I];
     DelChars:=StrH;
End; {DelChars}
//-----------------------------------------------------------------------

//������� ������ ������ ����� ��� �� �����
Function ConvertToRishuy(InStr :String) :String;
Var
   StrH :String;
Begin
     StrH:=DelChars(InStr,['-']);
     Case Length(StrH) of
          5:Result:=Copy(StrH,1,2)+'-'+Copy(StrH,3,3);
          6:Result:=Copy(StrH,1,3)+'-'+Copy(StrH,4,3);
          7:Result:=Copy(StrH,1,2)+'-'+Copy(StrH,3,3)+'-'+Copy(StrH,6,2);
     Else
         Result:=InStr;
End;

End;

//                �������� ������ ���� ���� ����� �����
procedure CreateNewTable(Sender: TComponent;
  DbNameIn,TblNameIn,DBNameOut,TblNameOut:string;
  TblTypeOut : TTableType );
var MyTable,NewTable:TTable;
begin
  MyTable:=TTable.Create(Sender);
  NewTable:=TTable.Create(Sender);
  with MyTable do
  begin
    TableName:=TblNameIn;
    DataBaseName:=DbNameIn;
    IndexDefs.Update;
    FieldDefs.Update;
    NewTable.TableName:=TblNameOut;
    NewTable.TableType:=TblTypeOut;
    NewTable.DataBaseName:=DBNameOut;
    NewTable.IndexDefs.Assign(IndexDefs);
    NewTable.FieldDefs.Assign(FieldDefs);
    NewTable.CreateTable;
  end;
  MyTable.Free;
  NewTable.Free;
end;

Function ConvertDSStateToText(TheDS :TDataSet):String;
Begin
// ������ �� ����� ������� ���. ��� 05/09/2003
     If TheDS=Nil Then Exit;
     Case TheDs.State Of
          dsBrowse    : Result:='����';
          dsEdit      : Result:='�����';
          dsInsert    : Result:='���';
          dsInactive  : Result:='�� ����';
     Else
         Result:='';
     End;
End;

function DataSet2ExcelFile(TmpTable: TDataSet; FileName,SheetName: string;FieldsName:TStringList = nil;
         RecordFont : Tfont = nil ; HeaderFont : Tfont=nil ): boolean;
const
  xlWBATWorksheet = -4167;
var
  Row, Col:     integer;
  GridPrevFile: string;
  XLApp, Sheet: OLEVariant;
  NoVisible : Integer;{���� ��� ���� ����� ����� ������� �����}
begin

  if assigned (FieldsName) Then
  begin
     Col := 0;
     For Col := 0 To TmpTable.FieldCount -1 do
     begin
           if FieldsName.Values [TmpTable.Fields[col].FieldName] = '' Then
             TmpTable.Fields[Col].Visible := False;
     end;
  end;
  Result := false;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := True;
    XLApp.Workbooks.Add(xlWBatWorkSheet);
    Sheet      := XLApp.Workbooks[1].WorkSheets[1];
    try
      if Copy (SheetName,1,30) <> '' Then
         Sheet.Name := Copy (SheetName,1,30);
    except
    end;
    TmpTable.First;
    Row := 1;
    while Not TmpTable.Eof do
    begin
      NoVisible := 0;
      for col := 0 to TmpTable.FieldCount - 1 do
      begin
         if TmpTable.Fields[col].Visible Then
         begin
           Sheet.Cells[row + 1,(col+1)-NoVisible].HorizontalAlignment := $FFFFEFC8;{Right}
           if TmpTable.Fields[col].DataType = FtDateTime Then
           begin
              if Not TmpTable.Fields[col].IsNull Then
                 if TdateTimeField(TmpTable.Fields[col]).DisplayFormat <> '' Then
                    Sheet.Cells[row + 1,(col+1)-NoVisible] := FormatDateTime (TdateTimeField(TmpTable.Fields[col]).DisplayFormat,TmpTable.Fields[col].AsDateTime)
                 else
                    Sheet.Cells[row + 1,(col+1)-NoVisible] := FormatDateTime ('MM/DD/YYYY',TmpTable.Fields[col].AsDateTime);
           end
           else
              if TmpTable.Fields[col] is TnumericField Then
                 Sheet.Cells[row + 1,(col+1)-NoVisible] := TmpTable.Fields[col].DisplayText
              else
              begin
                 Sheet.Cells[row + 1,(col+1)-NoVisible].NumberFormat := '@';
                 Sheet.Cells[row + 1,(col+1)-NoVisible] := TmpTable.Fields[col].DisplayText;
              end;
           if RecordFont <> nil Then
           begin
              Sheet.Cells[row + 1,(col+1)-NoVisible].Font.Name := RecordFont.Name;
              Sheet.Cells[row + 1,(col+1)-NoVisible].Font.Bold := fsBold in RecordFont.Style;
              Sheet.Cells[row + 1,(col+1)-NoVisible].Font.Underline := fsUnderline in RecordFont.Style;
              Sheet.Cells[row + 1,(col+1)-NoVisible].Font.Italic := fsItalic in RecordFont.Style;
              Sheet.Cells[row + 1,(col+1)-NoVisible].Font.Color := RecordFont.Color;
           end;
         end
         else
            inc(NoVisible);
      end;{For}
      inc (Row);
      TmpTable.next;
    end;
    NoVisible := 0;
      for col := 0 to TmpTable.FieldCount - 1 do
         if TmpTable.Fields[col].Visible Then
         begin
            if FieldsName <> nil Then
               Sheet.Cells[1,(col+1)-NoVisible] := FieldsName.Values [TmpTable.Fields[col].FieldName]
            else
               Sheet.Cells[1,(col+1)-NoVisible] := TmpTable.Fields[col].FieldName;
            //Sheet.Cells[1,col+1].EntireColumn.AutoFit;
            Sheet.Cells.Item[(col+1)-NoVisible].EntireColumn.AutoFit;
            if HeaderFont <> nil Then
            begin
               Sheet.Cells[1,(col+1)-NoVisible].Font.Name := HeaderFont.Name;
               Sheet.Cells[1,(col+1)-NoVisible].Font.Bold := fsBold in HeaderFont.Style;
               Sheet.Cells[1,(col+1)-NoVisible].Font.Underline := fsUnderline in HeaderFont.Style;
               Sheet.Cells[1,(col+1)-NoVisible].Font.Italic := fsItalic in HeaderFont.Style;
               Sheet.Cells[1,(col+1)-NoVisible].Font.Color := HeaderFont.Color;
            end
            else
               Sheet.Cells[1,(col+1)-NoVisible].Font.Bold := true;
            Sheet.Cells[1,(col+1)-NoVisible].Interior.Color :=clSilver;
         end
         else
            inc(NoVisible);
    try
      Result := True;
    except on e:exception do
        ShowMessage (e.message);
    end;
  finally
(*    if not VarIsEmpty(XLApp) then
    begin
      XLApp.DisplayAlerts := False;
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
    end;*)
  end;
end;

Function GetDataSetFromTControl(TheControl :TControl):TDataSet;
Begin
  Try
    if (TheControl is TDBEdit) Then
        Result:=((TheControl As TDBEdit).DataSource.DataSet)
    Else
    if (TheControl is TDBLookupComboBox) Then
        Result:=((TheControl As TDBLookupComboBox).DataSource.DataSet)
    Else
    if (TheControl is TDbSrtGrd) Then
        Result:=((TheControl As TDbSrtGrd).DataSource.DataSet)
    Else
    if (TheControl is TDbGrid) Then
        Result:=((TheControl As TDbGrid).DataSource.DataSet)
    Else
    if (TheControl is TAtmDbDateEdit) Then
        Result:=((TheControl As TAtmDbDateEdit).DataSource.DataSet)
    Else
      Result:=Nil;
  Except
      Result:=Nil;
  End;

End;

Function GetDataSetFromTComponent(AComponent :TComponent):TDataSet;
var
  propInfo:  PPropInfo;
begin
  Result    := nil;
  propInfo  := GetPropInfo(AComponent.ClassInfo, 'DataSet');   //PropInfo  := GetProperty(AComponent, 'DataSource');

  if PropInfo <> nil then
    Result := TDataSet(GetOrdProp(AComponent, PropInfo));
End;

Function GetDataSourceFromTControl(TheControl :TControl):TDataSource;
Begin
  if (TheControl is TDBEdit) Then
      Result:=((TheControl As TDBEdit).DataSource)
  Else
  if (TheControl is TDBLookupComboBox) Then
      Result:=((TheControl As TDBLookupComboBox).DataSource)
  Else
  if (TheControl is TDbSrtGrd) Then
      Result:=((TheControl As TDbSrtGrd).DataSource)
  Else
  if (TheControl is TDbGrid) Then
      Result:=((TheControl As TDbGrid).DataSource)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).DataSource)
  Else
      Result:=Nil;
End;

Function GetDataSourceFromTComponent(AComponent :TComponent):TDataSource;
var
  propInfo:  PPropInfo;
begin
  Result    := nil;
  If AComponent<>Nil Then
    propInfo  := GetPropInfo(AComponent.ClassInfo, 'DataSource');   //PropInfo  := GetProperty(AComponent, 'DataSource');

  if (AComponent<>Nil) and (PropInfo <> nil) then
    Result := TDataSource(GetOrdProp(AComponent, PropInfo));
End;


Function GetDataDataFieldNameFromTControl(TheControl :TControl):ShortString;
Begin
  if (TheControl is TAtmDBHEdit) Then
      Result:=((TheControl As TAtmDBHEdit).DataField)
  Else
  if (TheControl is TAtmDBLookupCombo) Then
      Result:=((TheControl As TAtmDBLookupCombo).DataField)
  Else
  if (TheControl is TDbSrtGrd) Then
      Result:=((TheControl As TDbSrtGrd).SelectedField.FieldName)
  Else
  if (TheControl is TDbGrid) Then
      Result:=((TheControl As TDbGrid).SelectedField.FieldName)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).DataField)
  Else
      Result:='';
End;


Function GetLinkLabelTextFromTControl(TheControl :TControl):ShortString;
Begin
  if (TheControl is TAtmDBHEdit) Then
      Result:=((TheControl As TAtmDBHEdit).LinkLabel.Caption)
  Else
  if (TheControl is TAtmDBLookupCombo) Then
      Result:=((TheControl As TAtmDBLookupCombo).LinkLabel.Caption)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).LinkLabel.Caption)
  Else
      Result:='';
End;


//Procedure DuplicateOneRecord(TheTable :TDataSet;PostAfterDup,DupFieldsWithDefault,CancelEvents :Boolean; FirstFieldNo: Integer);
Procedure DuplicateOneRecord(TheTable :TDataSet;PostAfterDup,DupFieldsWithDefault:BOOLEAN;CancelEvents :Boolean = FALSE; FirstFieldNo: Integer=0);
{Var
   TempTable :TTable;
   I :LOngInt;
Begin
     if TheTable.State<>dsBrowse Then
     Begin
          ShowMessage('������ ������� �� ����� - ��� ������ ������');
          Exit;
     End;

     TempTable:=TTable.Create(TheTable.Owner);
     TempTable.DatabaseName:=TheTable.DatabaseName;
     TempTable.TableName:=TheTable.TableName;
     TempTable.MasterSource:=TheTable.MasterSource;
     TempTable.MasterFields:=TheTable.MasterFields;
     TempTable.IndexName:=TheTable.IndexName;
     TempTable.Open;
     TempTable.GotoCurrent(TheTable);
     TheTable.Insert;
     For I:=0 To TempTable.FieldDefs.Count-1 Do
     Begin
          if (TempTable.Fields.Fields[I].FieldKind=fkData) And
              (TempTable.Fields.Fields[I].DataType<>ftAutoInc) And
              (CompareText(TempTable.Fields.Fields[I].FieldName,'Tnua')<>0) Then
          Begin
            Try
               TheTable.FieldByName(TempTable.Fields.Fields[I].FieldName).Value:=
                    TempTable.FieldByName(TempTable.Fields.Fields[I].FieldName).Value;
            Except On Exception Do; End;
          End;
     End;
     TempTable.Close;
     TempTable.Free;
}
var
  CopyData: Variant;
  Counter: Integer;
  EV :TFieldNotifyEvent;
begin
  with TheTable do
  begin
    { Store the data of the Current record into a Variant Array }
    CopyData := VarArrayCreate([0, FieldCount-1], varVariant);
    for Counter := FirstFieldNo to FieldCount - 1 do
      CopyData[Counter] := Fields[Counter].Value;

    { Insert a new Record }
    Append;

    { Insert the copied Data into the new record only if there
      is no default data specified, and if the field isn't a calcluated
      or AutoInc field }

    for Counter := FirstFieldNo to FieldCount - 1 do
    begin
      if ((Fields[Counter].IsNull) Or DupFieldsWithDefault) and                    { Field is Empty }
         (not Fields[Counter].Calculated) and            { Not a Calc Field }
         (not (Fields[Counter].DataType=ftAutoInc)) then { No AutoInc Field }
      Begin
        if CancelEvents Then
        Begin
          EV:=Fields[Counter].OnValidate;
          Fields[Counter].OnValidate:=NIL;
        End;
          Fields[Counter].Value := CopyData[Counter];
        if CancelEvents Then
          Fields[Counter].OnValidate:=EV;
      End;
    end;
  end;
  if PostAfterDup Then
     TheTable.Post;
End;

Procedure CopyOneRecord(SourceDataSet,DestDataSet:TDataSet;PostAfterDup,DupFieldsWithDefault :Boolean);
var
  CopyData: Variant;
  Counter: Integer;
begin
  Begin
    { Insert a new Record }
    DestDataSet.Insert;

    { Insert the copied Data into the new record only if there
      is no default data specified, and if the field isn't a calcluated
      or AutoInc field }
    for Counter := 0 to SourceDataSet.FieldCount - 1 do
    begin
      Try
          if DestDataSet.FindField(SourceDataSet.Fields[Counter].FieldName)<>Nil Then

          if ((DestDataSet.FieldByName(SourceDataSet.Fields[Counter].FieldName).IsNull) Or DupFieldsWithDefault) and  { Field is Empty }
             ((not DestDataSet.FieldByName(SourceDataSet.Fields[Counter].FieldName).Calculated) and            { Not a Calc Field }
             (Not DestDataSet.FieldByName(SourceDataSet.Fields[Counter].FieldName).Lookup) And                 {Not Lookup}
             (DestDataSet.FieldByName(SourceDataSet.Fields[Counter].FieldName).DataType<>ftAutoInc)) then { No AutoInc Field }
            DestDataSet.FieldByName(SourceDataSet.Fields[Counter].FieldName).Value := SourceDataSet.Fields[Counter].Value;
      Except
      End;
    end;
  end;
  if PostAfterDup Then
     DestDataSet.Post;
End;


function GetProductVersion(FileName :String): String;
var
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
  V1,V2,V :DWord;
  StrH : String;
begin
  begin
    if FileName='' Then FileName:=ParamStr(0);
    InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
    if InfoSize <> 0 then
    begin
      GetMem(VerBuf, InfoSize);
      try
        if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
          if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
          Begin
                V1 := FI.dwProductVersionMS;
                V2 := Fi.dwProductVersionLS;

                V:=V1 Div $FFFF;
                StrH:=IntToStr(V);

                V:=0;
                Move(V1,V,2);
                StrH:=StrH+'.'+IntToStr(V);


                V:=V2 Div $FFFF;
                StrH:=StrH+'.'+IntToStr(V);
                V:=0;
                Move(V2,V,2);
                StrH:=StrH+' (Build '+IntToStr(V)+')';
          End;
      finally
        FreeMem(VerBuf);
      end;
    end;
  end;
  Result := StrH;
end;
//----------------------------------------------------------------------
function IsLeapYear(AYear: Integer): Boolean;
begin
  Result := (AYear mod 4 = 0) and ((AYear mod 100 <> 0) or (AYear mod 400 = 0));
end;
Function MonthDiff(Date1, Date2:TDateTime):Integer;
Var Y1,M1,Y2,M2, D:Word;
    ITmp:Integer;
    Tmp:TDateTime;
Begin
  // DATE 2 Shoudld be > DATE 1

  If Date1>Date2 Then
  Begin
    Tmp:=Date1;
    Date1:=Date2;
    Date2:=Tmp;
  End;

  DecodeDate(Date1,Y1,M1,D);
  DecodeDate(Date2,Y2,M2,D);


  ITmp:=M2;
  ITmp:=ITmp-M1+1;
  ITmp:=ITmp+(Y2-Y1)*12;
  Result:=ITmp;


End;
//-----------------------------------------------------------------------
function DaysPerMonth(AYear, AMonth: Integer): Integer;
const
  DaysInMonth: array[1..12] of Integer =
    (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := DaysInMonth[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then Inc(Result); { leap-year Feb is special }
end;
//-----------------------------------------------------------------------
function IsValidDate(Y, M, D: Word): Boolean;
begin
  Result := (Y >= 1) and (Y <= 9999) and (M >= 1) and (M <= 12) and
    (D >= 1) and (D <= DaysPerMonth(Y, M));
end;

function ValidDate(ADate: TDateTime): Boolean;
var
  Year, Month, Day: Word;
begin
  try
    DecodeDate(ADate, Year, Month, Day);
    Result := IsValidDate(Year, Month, Day);
  except
    Result := False;
  end;
end;
function ValidStrDate(ADate: String): Boolean;
begin
  try
    StrToDate(ADate);
    Result := True;
  except
    Result := False;
  end;
end;


function DaysInPeriod(Date1, Date2: TDateTime): Longint;
begin
  if ValidDate(Date1) and ValidDate(Date2) then
    Result := Abs(Trunc(Date2) - Trunc(Date1)) + 1
  else Result := 0;
end;

Function TimeToMinutes(ATime :TDateTime):LongInt;
Var
  H,M,S,MS :Word;
Begin
  DecodeTime(ATime,H,M,S,MS);
  Result:=H*60+M;
End;

Function FindFirstControlInTabOrder(ParentControl :TWinControl):TWinControl;
Var
   T :TWinControl;
   Li :TList;
begin
  Try
     T:=Nil;
     Li:=TList.Create;
     ParentControl.GetTaborderList(Li);
     if Li.Count>0 Then
     Repeat
         T:=TWinControl(Li.Items[0]);
         Li.Free;
         Li:=TList.Create;
         T.GetTaborderList(Li);
     Until Li.Count<=0;
  Finally
         Li.Free;
         Result:=T;
  End;
End;

Procedure FillKodTavlaLookupList(TheDataset:TDataset;FieldsList :TStringList);
Var
   TheField :TField;
   StrH     :String;
   Ind      :LongInt;
   DSWasOpen :Boolean;
Begin
     DSWasOpen:=TheDataSet.Active;
     if Not DSWasOpen Then
        TheDataSet.Active:=True;
     For Ind:=0 To FieldsList.Count-1 Do
     Begin
          TheField:=TField(FieldsList.Objects[Ind]);
          TheField.LookupList.Clear;
     End;
     TheDataSet.First;
     While Not TheDataSet.Eof Do
     Begin
          StrH:=TheDataSet.FieldByName('Sug_Tavla').AsString;
          For Ind:=0 To FieldsList.Count-1 Do
          if CompareText(FieldsList[Ind],StrH)=0 Then
          Begin
//               ShowMessage(StrH);
               TheField:=TField(FieldsList.Objects[Ind]);
               TheField.LookupList.Add(TheDataSet.FieldByName('Kod_Tavla').Value,
                                       TheDataSet.FieldByName('Teur_Tavla').Value);
          End;
          TheDataSet.Next;
     End;
     if Not DSWasOpen Then
        TheDataSet.Active:=False;
End;

Procedure FillKodTavlaComboList(TheDataset:TDataset;FieldsList :TStringList);
Var
   TheCombo :TAtmDBComboBox;
   StrH     :String;
   Ind      :LongInt;
Begin
     For Ind:=0 To FieldsList.Count-1 Do
     Begin
          TheCombo:=TAtmDBComboBox(FieldsList.Objects[Ind]);
          TheCombo.Clear;
          TheCombo.FreeListObjects;
     End;

     TheDataSet.First;
     While Not TheDataSet.Eof Do
     Begin
          StrH:=TheDataSet.FieldByName('Sug_Tavla').AsString;
          For Ind:=0 To FieldsList.Count-1 Do
          if CompareText(FieldsList[Ind],StrH)=0 Then
          Begin
//               ShowMessage('a '+StrH);
               TheCombo:=TAtmDBComboBox(FieldsList.Objects[Ind]);
               TheCombo.AddToList(TheDataSet.FieldByName('Teur_Tavla').AsString,
                                  TheDataSet.FieldByName('Kod_Tavla').AsInteger);
          End;
          TheDataSet.Next;
     End;
End;


Procedure FillKodTavla2ComboList(TheDataset:TDataset;FieldsList :TStringList;KeyFieldName,ValueFieldName :String);
Var
   TheCombo :TAtmDBComboBox;
   StrH     :String;
   Ind      :LongInt;
Begin
     For Ind:=0 To FieldsList.Count-1 Do
     Begin
          TheCombo:=TAtmDBComboBox(FieldsList.Objects[Ind]);
          TheCombo.Clear;
          TheCombo.FreeListObjects;
     End;
     TheDataSet.First;
     While Not TheDataSet.Eof Do
     Begin
          StrH:=TheDataSet.FieldByName('Sug_Tavla').AsString;
          For Ind:=0 To FieldsList.Count-1 Do
          if CompareText(FieldsList[Ind],StrH)=0 Then
          Begin
               TheCombo:=TAtmDBComboBox(FieldsList.Objects[Ind]);
               TheCombo.AddToList(TheDataSet.FieldByName(ValueFieldName).AsString,
                                  TheDataSet.FieldByName(KeyFieldName).AsInteger);
          End;
          TheDataSet.Next;
     End;
End;

function GetProgDir: TFileName;
function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
var
  Reg: TRegistry;
  RegDataType: TRegDataType;
  DataSize, Len: integer;
  s: string;
label cantread;
begin
  Reg := nil;
  try
    Reg := TRegistry.Create(KEY_QUERY_VALUE);
    Reg.RootKey := RootKey;
    if Reg.OpenKeyReadOnly(Key) then begin
      try
        RegDataType := Reg.GetDataType(Value);
        if (RegDataType = rdString) or
           (RegDataType = rdExpandString) then
          Result := Reg.ReadString(Value)
        else if RegDataType = rdInteger then
          Result := Reg.ReadInteger(Value)
        else if RegDataType = rdBinary then begin
          DataSize := Reg.GetDataSize(Value);
          if DataSize = -1 then goto cantread;
          SetLength(s, DataSize);
          Len := Reg.ReadBinaryData(Value, PChar(s)^, DataSize);
          if Len <> DataSize then goto cantread;
          Result := s;
        end else
cantread:
          raise Exception.Create(SysErrorMessage(ERROR_CANTREAD));
      except
        s := ''; // Deallocates memory if allocated
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
      raise Exception.Create(SysErrorMessage(GetLastError));
  except
    Reg.Free;
    raise;
  end;
  Reg.Free;
end;
begin
  Result := GetRegistryData(HKEY_LOCAL_MACHINE,
    '\Software\Microsoft\Windows\CurrentVersion',
    'ProgramFilesDir');  // or 'ProgramFilesPath'
end;

Function GetWinDir :String;
Var
   WinDir :PChar;
Begin
  GetMem (winDir,255);
  Try
     GetWindowsDirectory(WinDir,255);
     Result:=StrPas(WinDir);
     If Result[Length(Result)]<>'\' Then
        Result:=Result+'\';
  Finally
         FreeMem(WinDir);
  End;
End;

function UserName :String;
var
    Size : Cardinal;
    TmpPchar :Pchar;
begin
    try
     Size := 256;
     Getmem (TmpPchar,size);
     GetUserName (TmpPchar,Size);
     UserName := TmpPchar;
    finally
     FreeMem (TmpPchar,Size);
    end;{Try}
end;

function ComputerName :String;
var
    Size : Cardinal;
    TmpPchar :Pchar;
begin
    try
     Size := 256;
     Getmem (TmpPchar,size);
     GetComputerName (TmpPchar,Size);
     ComputerName := TmpPchar;
    finally
     FreeMem (TmpPchar,Size);
    end;{Try}
end;

Function GetTempDir :String;
Var
   WinDir :PChar;
Begin
  GetMem (winDir,255);
  Try
     GetTempPath(255,WinDir);
     Result:=StrPas(WinDir);
  Finally
         FreeMem(WinDir);
  End;
End;

Function GetSysDir :String;
Var
   WinDir :PChar;
Begin
  GetMem (winDir,255);
  Try
     GetSystemDirectory(WinDir,255);
     Result:=StrPas(WinDir);
  Finally
         FreeMem(WinDir);
  End;
End;

function FindGlobalNextControl(CurControl,CurParent: TWinControl;
  GoForward, CheckTabStop, CheckParent: Boolean): TWinControl;
var
  I, StartIndex: Integer;
  List: TList;
begin
  Result := nil;
  List := TList.Create;
  try
    if CurParent<>Nil Then
       While CurParent.Parent<>Nil Do
          CurParent:=CurParent.Parent;
    if CurParent=Nil Then
       Exit;

    CurParent.GetTabOrderList(List);
    if List.Count > 0 then
    begin
      StartIndex := List.IndexOf(CurControl);
      if StartIndex = -1 then
        if GoForward then StartIndex := List.Count - 1 else StartIndex := 0;
      I := StartIndex;
      repeat
        if GoForward then
        begin
          Inc(I);
          if I = List.Count then I := 0;
        end else
        begin
          if I = 0 then I := List.Count;
          Dec(I);
        end;
        CurControl := List[I];
        if CurControl.CanFocus and
          (not CheckTabStop or CurControl.TabStop) and
          (not CheckParent or (CurControl.Parent = CurControl{Self})) then
          Result := CurControl;
      until (Result <> nil) or (I = StartIndex);
    end;
  finally
    List.Destroy;
  end;
end;

function DoEncryption(Src:string; Key: string; DecryptKey: integer;
                      Encrypt: Boolean) // True - ����, False - ������ ������
                                          :string;
var
   KeyLen      :Integer;
   KeyPos      :Integer;
   offset      :Integer;
   dest        :string;
   SrcPos      :Integer;
   SrcAsc      :Integer;
   TmpSrcAsc   :Integer;

begin
  Try
     Result:='';
     if Src = '' Then
        Exit;
     KeyLen:=Length(Key);
     if KeyLen = 0 then Key:='Aviran';
     KeyPos:=0;
     SrcPos:=0;
     SrcAsc:=0;
     if Encrypt then begin
          offset:=DecryptKey;
          dest:=format('%1.2x',[offset]);
          for SrcPos := 1 to Length(Src) do
          begin
               SrcAsc:=(Ord(Src[SrcPos]) + offset) MOD 255;
               if KeyPos < KeyLen then KeyPos:= KeyPos + 1 else KeyPos:=1;
               SrcAsc:= SrcAsc xor Ord(Key[KeyPos]);
               dest:=dest + format('%1.2x',[SrcAsc]);
               offset:=SrcAsc;
          end;
     end
     else
     begin
          offset:=StrToInt('$'+ copy(src,1,2));
          SrcPos:=3;
          repeat
                SrcAsc:=StrToInt('$'+ copy(src,SrcPos,2));
                if KeyPos < KeyLen Then KeyPos := KeyPos + 1 else KeyPos := 1;
                TmpSrcAsc := SrcAsc xor Ord(Key[KeyPos]);
                if TmpSrcAsc <= offset then
                     TmpSrcAsc := 255 + TmpSrcAsc - offset
                else
                     TmpSrcAsc := TmpSrcAsc - offset;
                dest := dest + chr(TmpSrcAsc);
                offset:=srcAsc;
                SrcPos:=SrcPos + 2;
          until SrcPos >= Length(Src);
     end;
     Result:=Dest;
  Except 
//         ShowMessage('����� ������');
  End;
end;

Function ValidateTableLookupFields(TheDataSet:TDataSet):Boolean;
Var
   I,II :LongInt;
   StrH :String;
Begin
   Result:=True;
   For I:=0 To TheDataSet.Fields.Count-1 Do
      if (TheDataSet.Fields[I].FieldKind=fkLookup) And (TheDataSet.Fields[I].KeyFields<>'')
          And (Pos(';',TheDataSet.Fields[I].KeyFields)=0) Then
      Begin
          if TheDataSet.Fields[I].AsString='' Then
            if (TheDataSet.FieldByName(TheDataSet.Fields[I].KeyFields).AsString<>'0') And (TheDataSet.FieldByName(TheDataSet.Fields[I].KeyFields).AsString<>'')Then
            Begin
                 ShowMessage(TheDataSet.FieldByName(TheDataSet.Fields[I].KeyFields).DisplayName+' ��� ���� ');
                 Result:=False;
                 Exit;
            End;
      End;
End;


Function IntToLenStr(TheNumber,TheLength:LongInt; LeadingChar:Char):String;
Var
   StrH,St1 :String;
Begin
    StrH:=IntToStr(TheNumber);
    FillChar(St1,TheLength-Length(StrH),Ord(LeadingChar));
    SetLength(St1,TheLength-Length(StrH));
    Result:=St1+StrH;
End;


Function CountFieldsInSepString(TheString:String;TheSeparator :Char):LongInt;
// ���� ���� ������� ������ �"� �� �����
Var
   I :LongInt;
   NumOfSep :LongInt;
Begin
     if TheString='' Then
     Begin
          Result:=0;
          Exit;
     End;
     NumOfSep:=0;
     For I:=1 To Length(TheString) Do
     Begin
         if TheString[i]=TheSeparator Then
            Inc(NumOfSep);
     End;
     Result:=NumOfSep+1;
End;

//********************************
Function ExtractFileExtention(FileName :String):String;
Var
   StrH :String;
   I :LongInt;
Begin
     I:=Length(FileName);
     StrH:='';
     While (I>0) And (FileName[I]<>'.') Do
     Begin
          StrH:=FileName[i]+StrH;
          Dec(I);
     End;
     Result:=StrH;
End;

Function CopyAFile(AFrom,ATo :String):Boolean;
Var
   SHFileOpStruct :TSHFileOpStruct;
Begin
     AFrom:=AFrom+#0+#0;
     ATo:=ATo+#0+#0;
     Result:=FALSE;
     if Not FileExists(AFrom) Then
     Begin
          MessageDlg('File does not exist '+AFrom,mtError,[mbOk],0);
          Exit;
     End;

     FillChar(SHFileOpStruct,SizeOf(TSHFileOpStruct),#0);
     SHFileOpStruct.Wnd:=Application.Handle;
     SHFileOpStruct.wFunc:=FO_Copy;
     SHFileOpStruct.pFrom:=PChar(AFrom);
     SHFileOpStruct.pTo:=PChar(ATo);
     SHFileOpStruct.HNameMappings:=Nil;
     Result:=(SHFileOperation(SHFileOpStruct)=0);
End;

Procedure CopyDir(FromDir,ToDir :String;FormHandle :THandle);
Var
  OpStruc: TSHFileOpStruct;
  frombuf, tobuf: Array [0..60] of Char;
Begin
  FillChar( frombuf, Sizeof(frombuf), 0 );
  FillChar( tobuf, Sizeof(tobuf), 0 );
  StrPCopy( frombuf, FromDir+'\*.*' );
  StrPCopy( tobuf, ToDir );
  With OpStruc Do Begin
    Wnd:= FormHandle;
    wFunc:= FO_COPY;
    pFrom:= @frombuf;
    pTo:=@tobuf;
    fFlags:= FOF_NOCONFIRMATION or FOF_RENAMEONCOLLISION;
    fAnyOperationsAborted:= False;
    hNameMappings:= Nil;
    lpszProgressTitle:= Nil;
  end; { With }
  ShFileOperation( OpStruc );
End;

Procedure ReplaceTableNameForSql(AComponent :TComponent;Prefix :String);
Var
   I :LongInt;
Begin
     With AComponent Do
          For I:=0 To ComponentCount-1 Do
              if Components[i] is TTable Then
                 TTable(Components[i]).TableName:=Prefix+TTable(Components[i]).TableName;
End;


Procedure KeyboardManageForTableActionOnForm(Sender :TObject;Key :Word;Shift: TShiftState;
                                       TheTable :TDBDataSet);
Var
   MyControl :TWinControl;
Begin
  case Key of
     VK_Next  :TheTable.Next;
     VK_Prior :TheTable.Prior;
     VK_F3    :if  TheTable.State in [dsEdit ,dsInsert] then
                      TheTable.Post;
     VK_Home  :if  Shift = [ssCtrl] then
                   TheTable.First;
     VK_End   :if  Shift = [ssCtrl] then
                   TheTable.Last;
     VK_F11   :TheTable.Append;
     VK_Return,VK_DOWN ://���� ����� �� ���
                        Begin
                            if (Sender is TWinControl) Then
                            Begin
                                  MyControl:=(Sender As TWinControl);
                                  if MyControl is TForm Then
                                     MyControl:=TForm(MyControl).ActiveControl;
                                  if (MyControl Is TDBLookupComboBox) then exit;
                                  if (MyControl is TCustomGrid) Or (MyControl Is TCustomMemo) Or
                                     ((MyControl Is TCustomComboBox) And (Key<>VK_RETURN)) Then
                                     Exit;
                            End;

                             if (TWinControl(Sender).Perform(CM_WANTSPECIALKEY, VK_Tab, 0) =0) And
                               (TWinControl(Sender).Perform(WM_GETDLGCODE, 0, 0) and DLGC_WANTTAB = 0) and
                               (GetParentForm(TWinControl(Sender)).Perform(CM_DIALOGKEY,VK_TAB, VK_TAB) <> 0) then
                                    Exit;
                        End;
     VK_UP    :Begin
                    if (Sender is TWinControl) Then
                    Begin
                          MyControl:=(Sender As TWinControl);
                          if MyControl is TForm Then
                             MyControl:=TForm(MyControl).ActiveControl;
                          if (MyControl Is TDBLookupComboBox) then exit;
                          if (MyControl is TCustomGrid) Or (MyControl Is TCustomMemo) Or
                             ((MyControl Is TCustomComboBox) And (Key<>VK_RETURN)) Then
                             Exit
                    End;
                    if Not (Sender is TWinControl) Then Exit;
                    MyControl:=(Sender As TWinControl);
                    if MyControl is TForm Then
                       MyControl:=TForm(MyControl).ActiveControl;
                    MyControl:=FindGlobalNextControl(MyControl,MyControl.Parent,
                               False,True,False);
                    if MyControl<>Nil Then
                       MyControl.SetFocus;
               End;

  End;//Case
End;

Procedure MarkSaveOnTable(TheTable :TDataset);
Begin
    if TheTable is TAtmTable Then
      TAtmTable(TheTable).SavePressed:=True
    Else
      if TheTable is TAtmRxQuery Then
        TatmRxQuery(TheTable).SavePressed:=True;
End;

function TranslLetterDosToWin( Ch:char):char;
begin
   case ord(Ch) of
     40,41,46: TranslLetterDosToWin:=chr(32);
     127..156: TranslLetterDosToWin:=chr(ord(ch)+96);
     else TranslLetterDosToWin:=ch;
   end;
end;
    { *********** }
function TranslLetterWinToDos( Ch:char):char;
begin
   case ord(Ch) of
     224..252: TranslLetterWinToDos:=chr(ord(ch)-96);
     else TranslLetterWinToDos:=ch;
   end;
end;
    { *********** }
function TranslLetterWinToDos7Bit( Ch:char):char;
begin
   case ord(Ch) of
     224..252: TranslLetterWinToDos7Bit:=chr(ord(ch)-96 - 32);
     else TranslLetterWinToDos7Bit:=ch;
   end;
end;
    { *********** }
function WinToDos(WinString : String) : String;
var
  CountI : Integer;
begin
  for CountI:=1 to Length(WinString) do
    WinString[CountI]:=TranslLetterWinToDos(WinString[CountI]);
  Result := WinString;
end;
  { *************** }
function WinToDos7Bit(WinString : String) : String;
var
  CountI : Integer;
begin
  for CountI:=1 to Length(WinString) do
    WinString[CountI]:=TranslLetterWinToDos7Bit(WinString[CountI]);
  Result := WinString;
end;
  { *************** }
function DosToWin(WinString : String) : String;
var
  CountI : Integer;
begin
  for CountI:=1 to Length(WinString) do
    WinString[CountI]:=TranslLetterDosToWin(WinString[CountI]);
  Result := WinString;
end;
  { ***************** }
function AddLeadeSpace(St1:String;Len: Integer): String;
  var
    I: Integer;
    LenOfStr: Integer;
    St2: String;
begin
    St2:='';
    LenOfStr:=Length(St1);
    if len<LenOfStr then
    begin
      For I:=LenOfStr-Len+1 to LenOfStr do
        St2:=St2+St1[I];
    end
    else
    begin
      For I:=1 to Len-LenOfStr do
        St2:=St2+' ';
      St2:=St2+St1;
    end;
    AddLeadeSpace:=St2;
end;

procedure THebHintWindow.Paint;
var
  R: TRect;
begin
  R := ClientRect;
  Inc(R.Left, 1);
  Canvas.Font.Color := clInfoText;
  DrawText(Canvas.Handle, PChar(Caption), -1, R, DT_RTLREADING or DT_NOPREFIX or
    DT_WORDBREAK or DT_RIGHT);
end;


Procedure FreeDllHandle(Var DllHandle :THandle);
Begin
     if DllHandle<>0 Then
     Begin
        FreeLibrary(DllHandle);
        DllHandle:=0;
     End;
End;


Procedure RunDllProcPCharParam(DllName,DllProc:String;Prm :Array of String;FreeHandle :Boolean;Var DllHandle :THandle);
Var
   DllProcPrm0:TDllProcNoParam;//=Procedure;
   DllProcPrm1:TDllProc1PCharParam;//=Procedure(Param1 :PChar);
   DllProcPrm2:TDllProc2PCharParam;//=Procedure(Param1,Param2 :PChar);
   DllProcPrm3:TDllProc3PCharParam;//=Procedure(Param1,Param2,Param3 :PChar);
   DllProcPrm4:TDllProc4PCharParam;
   DllProcPrm5:TDllProc5PCharParam;
   DllProcPrm6:TDllProc6PCharParam;
   DllProcPrm7:TDllProc7PCharParam;
   DllProcPrm8:TDllProc8PCharParam;
   DllProcPrm9:TDllProc9PCharParam;
   DllProcPrm10: TDllProc10PCharParam;
   DllProcPrm11: TDllProc11PCharParam;
   Buf, DN,DP:PChar;
   I:DWord;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     StrPCopy(DN,DllName);
     StrPCopy(DP,DllProc);
     Try
//         ShowMessage('Handle:'+I2S(DllHandle)+#13+''''+String(DN)+'''');
         if DllHandle=0 Then
            DllHandle:=LoadLibrary(DN);
{             GetMem(Buf,255);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM ,
                  Nil,
                  GetLastError,
                  0,
                  Buf,
                  254,
                  Nil);
    ShowMessage('Handle:'+I2S(DllHandle)+#13+'LastError:'+I2S(GetLastError)+#13+'System Message:'+String(Buf));
}
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
         Case High(Prm)+1 of
              1 :Begin
                    @DllProcPrm1:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm1 <>  nil  then
                       DllProcPrm1(PChar(Prm[0]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
              2: Begin
                    @DllProcPrm2:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm2 <>  nil  then
                       DllProcPrm2(PChar(Prm[0]),PChar(Prm[1]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found'+#13+IntToStr(GetLastError));
                 End;
              3 :Begin
                    @DllProcPrm3:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm3 <>  nil  then
                       DllProcPrm3(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found'+#13+IntToStr(GetLastError));
                 End;
              4 :Begin
                    @DllProcPrm4:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm4 <>  nil  then
                       DllProcPrm4(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found'+#13+IntToStr(GetLastError));
                 End;
              5 :Begin
                    @DllProcPrm5:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm5 <>  nil  then
                       DllProcPrm5(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found'+#13+IntToStr(GetLastError));
                 End;
              6:Begin
                    @DllProcPrm6:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm6 <>  nil  then
                       DllProcPrm6(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]),PChar(Prm[5]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
              7:Begin
                    @DllProcPrm7:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm7 <>  nil  then
                       DllProcPrm7(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]),PChar(Prm[5]),PChar(Prm[6]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
              8:Begin
                    @DllProcPrm8:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm8 <>  nil  then
                       DllProcPrm8(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]),PChar(Prm[5]),PChar(Prm[6]),PChar(Prm[7]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
              9:Begin
                    @DllProcPrm9:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm9 <>  nil  then
                       DllProcPrm9(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]),PChar(Prm[5]),PChar(Prm[6]),PChar(Prm[7]),PChar(Prm[8]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
              10:Begin
                    @DllProcPrm10:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm10 <>  nil  then
                       DllProcPrm10(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]),PChar(Prm[5]),PChar(Prm[6]),PChar(Prm[7]),PChar(Prm[8]),PChar(Prm[9]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
              11:Begin
                    @DllProcPrm11:=GetProcAddress(DllHandle,DP);
                    if @DllProcPrm11 <>  nil  then
                       DllProcPrm11(PChar(Prm[0]),PChar(Prm[1]),PChar(Prm[2]),PChar(Prm[3]),PChar(Prm[4]),PChar(Prm[5]),PChar(Prm[6]),PChar(Prm[7]),PChar(Prm[8]),PChar(Prm[9]),PChar(Prm[10]))
                    Else
                      Raise ELibNotFound.Create(DllName+' '+DllProc+' Not Found');
                 End;
                 

              Else
                  Raise ELibNotFound.Create('���� ������� �� �����');
         End; //Case
          if FreeHandle Then
          Begin
               FreeLibrary(DllHandle);
               DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            DN:=Nil;
            StrDispose(DP);
            DP:=Nil;
     End;
End;

Procedure RunDllNoParam(DllName,DllProc :String;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);
Var
   ProcNoParam:TDllProcNoParam;
   ExeParams:String;
   DN,DP:PChar;
Begin
  If (Pos('WINHSHBN.DLL',UpperCase(DllName))>0) And (Not DirectLoad) Then
  Begin
     ExeParams:='WINHSHBN.DLL '+DllProc+' 0';
     ShellExecute(Application.Handle,PCHAR('open'), PCHar('WTCallDll.exe'),PCHAR(ExeParams),Nil,SW_SHOW	);
//     WinExec(PChar('WTCallDll.exe '+ExeParams+''), Sw_Hide);
  End
  Else
  Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     StrPCopy(DN,DllName);
     StrPCopy(DP,DllProc);
     Try
         if DllHandle=0 Then
            DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @ProcNoParam:=GetProcAddress(DllHandle,DP);
          if @ProcNoParam <>  nil  then
             ProcNoParam;
          if FreeHandle Then
          Begin
              FreeLibrary(DllHandle);
              DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
  End; // ELSE
End;

Procedure RunDll1IntParam(DllName,DllProc :String;Param1 :LongInt;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);
Var
   DllProcPrm :TDllProc1IntParam;
   ExeParams:String;
   DN,DP:PChar;
Begin
  If (Pos('WINHSHBN.DLL',UpperCase(DllName))>0) And (Not DirectLoad) Then
  Begin
     ExeParams:='WINHSHBN.DLL '+DllProc+' 1 '+I2S(Param1);
     ShellExecute(Application.Handle,PCHAR('open'), PCHar('WTCallDll.exe'),PCHAR(ExeParams),Nil,SW_SHOW	);
  End
  Else
  Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         if DllHandle = 0 Then
           DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(Param1);
          if FreeHandle Then
          Begin
            FreeLibrary(DllHandle);
            DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
  End;
End;


Procedure RunDll2IntParam(DllName,DllProc :String;Param1,Param2 :LongInt;FreeHandle :Boolean;Var DllHandle :THandle; DirectLoad:Boolean=True);
Var
   DllProcPrm :TDllProc2IntParam;
   ExeParams:String;
   DN,DP:PChar;
Begin
  If (Pos('WINHSHBN.DLL',UpperCase(DllName))>0) And (Not DirectLoad) Then
  Begin
     ExeParams:='WINHSHBN.DLL '+DllProc+' 2 '+I2S(Param1)+' '+I2S(Param2);
     ShowMessage(ExeParams);
     ShellExecute(Application.Handle,PCHAR('open'), PCHar('WTCallDll.exe'),PCHAR(ExeParams),Nil,SW_SHOW	);
  End
  Else
  Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         if DllHandle = 0 Then
          DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(Param1,Param2);
          if FreeHandle Then
          Begin
            FreeLibrary(DllHandle);
            DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
  End;
End;

Procedure RunDll6IntParam(DllName,DllProc :String;Param1,Param2, P3, P4, P5, P6 :LongInt;FreeHandle :Boolean;Var DllHandle :THandle;DirectLoad:Boolean=True);
Var
   DllProcPrm :TDllProc6IntParam;
   ExeParams:String;
   DN,DP:PChar;
Begin
  If (Pos('WINHSHBN.DLL',UpperCase(DllName))>0) And (Not DirectLoad) Then
  Begin
     ExeParams:='WINHSHBN.DLL '+DllProc+' 6 '+I2S(Param1)+' '+I2S(Param2)+' '+I2S(P3)+' '+I2S(P4)+' '+I2S(P5)+' '+I2S(P6);
     ShellExecute(Application.Handle,PCHAR('open'), PCHar('WTCallDll.exe'),PCHAR(ExeParams),Nil,SW_SHOW	);
  End
  Else
  Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         if DllHandle = 0 Then
          DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(Param1,Param2, P3, P4, P5, P6);
          if FreeHandle Then
          Begin
            FreeLibrary(DllHandle);
            DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
  End;
End;

Procedure RunDll1Int2StrParam(DllName,DllProc :String;Param1 :LongInt; Param2, Param3:String; FreeHandle :Boolean;Var DllHandle :THandle);
Var
   DllProcPrm :TDllProc1Int2StrParam;
   DN,DP:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         if DllHandle = 0 Then
          DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(Param1,Param2, Param3);
          if FreeHandle Then
          Begin
            FreeLibrary(DllHandle);
            DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;

Procedure RunDllOpenMeholel(DllName,DllProc :String;Param1:LongInt;Param2,Param3:String;FreeHandle :Boolean;Var DllHandle :THandle);
Var
   DllProcPrm :TDllProcOpenMeholl;
   DN,DP:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     StrPCopy(DN,DllName);
     StrPCopy(DP,DllProc);
     Try
         if DllHandle = 0 then
            DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(Param1,PChar(Param2),PChar(Param3));
          if FreeHandle Then
          Begin
               FreeLibrary(DllHandle);
               DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;

Function RunDllFunc2PCharParamBool(DllName,DllProc :String;Param1,Param2:String;FreeHandle :Boolean;Var DllHandle :THandle):Boolean;
Var
   DllFuncPrm :TDllFunc2PCharParamBool;
   DN,DP,P1,P2:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     StrPCopy(DN,DllName);
     StrPCopy(DP,DllProc);
     Try
         if DllHandle = 0 then
            DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllFuncPrm:=GetProcAddress(DllHandle,DP);
          if @DllFuncPrm <>  nil  then
             Result:=DllFuncPrm(PChar(Param1),PChar(Param2));
          if FreeHandle Then
          Begin
               FreeLibrary(DllHandle);
               DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;
Function RunDllFunc2StringParamBoolResult(DllName,DllProc,Param1,Param2 :String):Boolean;
Var
   DllHandle :THandle;
   DllProcPrm :TDllFunc2StrParamBoolResult;
   DN,DP:PChar;
   P1, P2 :ShortString;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         P1:=Param1;
         P2:=Param2;
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             Result:=DllProcPrm(P1, P2);
          FreeLibrary(DllHandle);
          DllHandle:=0;
     Finally
          StrDispose(DN);
          StrDispose(DP);
     End;
End;

Function RunDllFunc1StringParam(DllName,DllProc,Param1 :String):String;
Var
   DllHandle :THandle;
   DllProcPrm :TDllFunc1StrParam;
   DN,DP:PChar;
   P1 :ShortString;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         P1:=Param1;
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             Result:=DllProcPrm(P1);
          FreeLibrary(DllHandle);
          DllHandle:=0;
     Finally
          StrDispose(DN);
          StrDispose(DP);
     End;
End;

Procedure RunDll1PCharParam(DllName,DllProc,Param1 :String);
Var
   DllHandle :THandle;
   DllProcPrm :TDllProc1PCharParam;
   DN,DP,P1:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     P1:=StrAlloc(Length(Param1)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         StrPCopy(P1,Param1);
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(P1);
          FreeLibrary(DllHandle);
          DllHandle:=0;
     Finally
            StrDispose(DN);
            StrDispose(DP);
            StrDispose(P1);
     End;
End;

Procedure RunDll3PCharParam(DllName,DllProc,Param1,Param2,Param3 :String;FreeHandle :Boolean;Var DllHandle :THandle);
Var
   DllProcPrm :TDllProc3PCharParam;
   DN,DP,P1,P2,P3:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     P1:=StrAlloc(Length(Param1)+1);
     P2:=StrAlloc(Length(Param2)+1);
     P3:=StrAlloc(Length(Param3)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         StrPCopy(P1,Param1);
         StrPCopy(P2,Param2);
         StrPCopy(P3,Param3);
         if DllHandle=0 Then
            DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             DllProcPrm(P1,P2,P3);
          if FreeHandle Then
          Begin
               FreeLibrary(DllHandle);
               DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
            StrDispose(P1);
            StrDispose(P2);
            StrDispose(P3);
     End;
End;

Function RunDllStringFuncNoParam(DllName,DllProc :String):ShortString;
Var
   DllHandle :THandle;
   ProcNoParam:TDllFuncStringNoParam;
   DN,DP:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @ProcNoParam:=GetProcAddress(DllHandle,DP);
          if @ProcNoParam <>  nil  then
             Result:=ProcNoParam;
          FreeLibrary(DllHandle);
          DllHandle:=0;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;

Function RunDllOpenMslPointForm(DllName,DllProc :String;TheNodeList :TNodeList;AutoCalcMaslul :Boolean) :Boolean;
Var
   DllHandle :THandle;
   DllProcPrm :TDllOpenMslPointForm; //= Function (TheNodeList :TNodeList) :Boolean;
   DN,DP:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @DllProcPrm:=GetProcAddress(DllHandle,DP);
          if @DllProcPrm <>  nil  then
             Result:=DllProcPrm(TheNodeList,AutoCalcMaslul);
          FreeLibrary(DllHandle);
          DllHandle:=0;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;

Procedure RunDllScanImage(DllName,DllProc :String;var Image1:TImage;FreeHandle :Boolean;Var DllHandle :THandle);
{Var
   ProcParam:TDllScanImage;
   DN,DP:PChar;}
Begin
     ScanImage(Image1);
{     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     StrPCopy(DN,DllName);
     StrPCopy(DP,DllProc);
     Try
         if DllHandle=0 Then
            DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @ProcParam:=GetProcAddress(DllHandle,DP);
          if @ProcParam <>  nil  then
             ProcParam(Image1);
          if FreeHandle Then
          Begin
              FreeLibrary(DllHandle);
              DllHandle:=0;
          End;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;}
End;



Function GetParamStr(ParamNum :LongInt): String;
Var
   I :longInt;
   StrH :String;
Begin
    if ParamNum=0 Then
    Begin
         Result:=Application.ExeName;
         Exit;
    End;
    For I:=0 To ParamCount Do
        StrH:=StrH+ParamStr(i)+' ';
    I:=Length(Application.ExeName);
    StrH:=Copy(StrH,I+1,Length(StrH));
    StrH:=Trim(StrH);
    Result:=GetFieldFromSeparateString(StrH,' ',ParamNum);
End;

Function CrackPassword (Password :String):Boolean;
//                         ����� �� ������
Var
  Present: TDateTime;
  Year, Month, Day, IntH, Int1 : Word;
  StrH  :String;
Begin
  {����� ����� ������ * ���� �����}
  {Password:=Copy(Password,1,2);}
  Present:= Now;
  DecodeDate(Present, Year, Month, Day);
  Int1:=Day;
  IntH:=Int1 Mod 10;
  IntH:=IntH+(Int1 Div 10);
  Int1:=Month;
  IntH:=IntH+(Int1 Mod 10);
  IntH:=IntH+(Int1 Div 10);
  StrH:=IntToStr (Year);
  IntH:=IntH+StrToInt (Copy(StrH,1,1));
  IntH:=IntH+StrToInt (Copy(StrH,2,1));
  IntH:=IntH+StrToInt (Copy(StrH,3,1));
  IntH:=IntH+StrToInt (Copy(StrH,4,1));

  IntH:=IntH*DayOfWeek(Present);
  Try
     CrackPassword := IntH = StrToInt (Password);
  Except On Exception Do
         CrackPassword :=False;
  End;

end;

Procedure PrintDbGrid(Koteret :String;TheGrid :TDbGrid);
Const
     FontName ='Miriam Fixed';
var
   i,j,WidthOfFields,PageWidth,PageHeight : Integer;
   TmpStr : String;
   PrevOrient :TPrinterOrientation;
   ScaleFact :Real;
  procedure Preperhedear;
  var
     i,j :Integer;
  begin
       PageWidth := Printer.PageWidth;
       PageHeight := 10;
       Printer.Canvas.Font.Name := FontName;
       Printer.Canvas.Font.Size:=16;
       TmpStr:=Koteret;
       Printer.Canvas.TextOut((PageWidth Div 2)-(Printer.Canvas.TextWidth(TmpStr) Div 2),PageHeight,TmpStr);
       PageHeight := PageHeight+Printer.Canvas.TextHeight ('�')*2 ;
       Printer.Canvas.Font.Name := FontName;
       Printer.Canvas.Font.Size:=10;
       For i := 0 To TheGrid.Columns.Count-1   Do
       begin
            WidthOfFields := Trunc(TheGrid.Columns[i].Width * ScaleFact) div (Printer.Canvas.TextWidth('�')) -1;
            TmpStr := Copy (TheGrid.Columns.Items[i].Title.Caption,1,WidthOfFields);
            for j := Length (tmpStr) to WidthOfFields Do{����� ������}
               tmpStr := ' '+tmpStr;
            PageWidth := PageWidth-((WidthOfFields+1)*Printer.Canvas.TextWidth('�'));
            Printer.Canvas.TextOut (PageWidth,PageHeight,TmpStr);
       end;
       PageHeight := PageHeight+2*Printer.Canvas.TextHeight ('�');
   end;
begin{Printing Grid}

    PrevOrient:=Printer.Orientation;
    Printer.Orientation:=poLandScape;
    printer.BeginDoc;
    Printer.Title:=Koteret;
    WidthOfFields:=0;
    For i := 0 To TheGrid.Columns.Count-1   Do
        WidthOfFields := WidthOfFields+TheGrid.Columns[i].Width;

    ScaleFact:=(Printer.Canvas.ClipRect.Right/WidthOfFields{TheGrid.Canvas.ClipRect.Right});
    TheGrid.DataSource.Dataset.First;
    try
       Preperhedear;
       While Not TheGrid.DataSource.Dataset.EOF Do
       begin
          PageWidth := Printer.PageWidth;
          Printer.Canvas.Font.Name := FontName;
          Printer.Canvas.Font.Size:=10;
          For i := 0 To TheGrid.Columns.Count-1   Do
          begin
            WidthOfFields := Trunc(TheGrid.Columns[i].Width * ScaleFact) div (Printer.Canvas.TextWidth('�')) -1;
            TmpStr:=Trim(TheGrid.DataSource.Dataset.FieldByName(TheGrid.Columns[I].FieldName).AsString);
            if (TheGrid.DataSource.Dataset.FieldByName(TheGrid.Columns[I].FieldName) Is TNumericField) And (Length(TmpStr)>WidthOfFields) Then
               TmpStr:='**********';
            TmpStr := Copy (TmpStr,1,WidthOfFields);
            for j := Length (tmpStr) to WidthOfFields Do{����� ������}
               tmpStr := ' '+tmpStr;
            PageWidth := PageWidth-((WidthOfFields+1)*Printer.Canvas.TextWidth('�'));
            Printer.Canvas.TextOut (PageWidth,PageHeight,TmpStr);
          end;
          TheGrid.DataSource.Dataset.Next;
          PageWidth := Printer.PageWidth;
          PageHeight := PageHeight+Printer.Canvas.TextHeight('�');
          if PageHeight >= Printer.PageHeight then
          begin
             Printer.NewPage;
             PageHeight := 0;
             Preperhedear;
          end;
       end;
    finally
        Printer.EndDoc;
        Printer.Orientation:=PrevOrient;
  //     CloseFile (Pprn);
    end;
End;


Procedure SetAtmDbEditF4State(TheForm :TWinControl);
Var
   I :LongInt;
Begin
     if TheForm Is TForm Then
     Begin
         For I:=0 To TheForm.ComponentCount-1 Do
         Begin
             if TheForm.Components[I] is TAtmDBHEdit Then
                (TheForm.Components[I] As TAtmDBHEdit).SetF1StatusColor;

             if TheForm.Components[I] is TAtmDBHEdit Then
                (TheForm.Components[I] As TAtmDBHEdit).SetF4StatusColor
             Else
               if TheForm.Components[I] is TAtmDBDateEdit Then
                  (TheForm.Components[I] As TAtmDBDateEdit).SetF4StatusColor
         End;
     End
     Else
     Begin
         For I:=0 To TheForm.ControlCount-1 Do
         Begin
             if TheForm.Controls[I] is TAtmDBHEdit Then
                (TheForm.Controls[I] As TAtmDBHEdit).SetF1StatusColor;

             if TheForm.Controls[I] is TAtmDBHEdit Then
                (TheForm.Controls[I] As TAtmDBHEdit).SetF4StatusColor
             Else
               if TheForm.Controls[I] is TAtmDBDateEdit Then
                  (TheForm.Controls[I] As TAtmDBDateEdit).SetF4StatusColor
         End;
     End;
End;

Procedure SaveF4ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
Var
   TheIni :TIniFile;
   I :LongInt;
Begin
     if IniFileName='' Then
        Exit;

     If not (FileExists(IniFileName)) Then Exit;
        
     SaveF10ValuesToIni(TheForm,IniFileName,'',StringsToAdd);

     if Section='' Then
        Section:='F4Values';

     TheIni:=TIniFile.Create(IniFileName);
     Try
         TheIni.EraseSection(Section);
         For I:=0 To TheForm.ComponentCount-1 Do
         Begin
              if TheForm.Components[I] is TAtmDBHEdit Then
                 if TAtmDBHEdit(TheForm.Components[I]).IsFixed Then
                    TheIni.WriteString(Section,TAtmDBHEdit(TheForm.Components[I]).Name,
                                     TAtmDBHEdit(TheForm.Components[I]).FixedString);
              if TheForm.Components[I] is TAtmDBDateEdit Then
                 if TAtmDBDateEdit(TheForm.Components[I]).IsFixed Then
                    TheIni.WriteString(Section,TAtmDBDateEdit(TheForm.Components[I]).Name,
                                     TAtmDBDateEdit(TheForm.Components[I]).FixedString);
         End;
         // Add other strings that came from other objects
         if StringsToAdd <> nil then
            for i := 0 to StringsToAdd.Count-1 do
                TheIni.WriteString(Section, StringsToAdd.Names[i],
                                   StringsToAdd.Values[StringsToAdd.Names[i]]);


     Finally
            TheIni.Free;
     End;
End;

Procedure SaveF10ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
Var
   TheIni :TIniFile;
   I :LongInt;
Begin
     if IniFileName='' Then
        Exit;
     if Section='' Then
        Section:='F10Values';
     TheIni:=TIniFile.Create(IniFileName);
     Try
         TheIni.EraseSection(Section);
         For I:=0 To TheForm.ComponentCount-1 Do
         Begin
              if TheForm.Components[I] is TAtmDBHEdit Then
                 if TAtmDBHEdit(TheForm.Components[I]).IsSkipTab Then
                    TheIni.WriteString(Section,TAtmDBHEdit(TheForm.Components[I]).Name,
                                     '1');
              if TheForm.Components[I] is TAtmHComboBox Then
                 if TAtmHComboBox(TheForm.Components[I]).IsSkipTab Then
                    TheIni.WriteString(Section,TAtmHComboBox(TheForm.Components[I]).Name,
                                     '1');
              if TheForm.Components[I] is TAtmDbHMemo Then
                 if TAtmDbHMemo(TheForm.Components[I]).IsSkipTab Then
                    TheIni.WriteString(Section,TAtmDbHMemo(TheForm.Components[I]).Name,
                                     '1');
              if TheForm.Components[I] is TAtmDBDateEdit Then
                 if TAtmDBDateEdit(TheForm.Components[I]).IsSkipTab Then
                    TheIni.WriteString(Section,TAtmDBDateEdit(TheForm.Components[I]).Name,
                                     '1');

         End;
         // Add other strings that came from other objects
         if StringsToAdd <> nil then
            for i := 0 to StringsToAdd.Count-1 do
                TheIni.WriteString(Section, StringsToAdd.Names[i],
                                   StringsToAdd.Values[StringsToAdd.Names[i]]);


     Finally
            TheIni.Free;
     End;
End;

Procedure ReadF4ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
Var
   TheIni :TIniFile;
   TheStrings :TStringList;
   I :LongInt;
Begin
     if IniFileName='' Then
        Exit;

     ReadF10ValuesToIni(TheForm,IniFileName,'',StringsToAdd);

     if Section='' Then
        Section:='F4Values';
     TheIni:=TIniFile.Create(IniFileName);
     TheStrings:=TStringList.Create;
     Try
         TheIni.ReadSectionValues(Section,TheStrings);
         For I:=0 To TheStrings.Count-1 Do
         Begin
              if TheForm.FindComponent(TheStrings.Names[I])<>Nil Then
              begin
                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmDBHEdit Then
                 Begin
                      TAtmDBHEdit(TheForm.FindComponent(TheStrings.Names[I])).FixedString:=
                         TheStrings.Values[TheStrings.Names[I]];
                         TAtmDBHEdit(TheForm.FindComponent(TheStrings.Names[I])).IsFixed:=True;
                      Continue;
                 End;
                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmDBDateEdit Then
                 Begin
                      TAtmDBDateEdit(TheForm.FindComponent(TheStrings.Names[I])).FixedString:=
                         TheStrings.Values[TheStrings.Names[I]];
                         TAtmDBDateEdit(TheForm.FindComponent(TheStrings.Names[I])).IsFixed:=True;
                      Continue;
                 End;
                 if StringsToAdd <> nil then
                    StringsToAdd.Add(TheStrings[i]);
              End;
         End;

     Finally
            TheIni.Free;
            TheStrings.Free;
     End;
End;

Procedure ReadF10ValuesToIni(TheForm:TForm;IniFileName,Section :String; StringsToAdd: TStrings = Nil);
Var
   TheIni :TIniFile;
   TheStrings :TStringList;
   I :LongInt;
Begin
     if IniFileName='' Then
        Exit;
     if Section='' Then
        Section:='F10Values';
     TheIni:=TIniFile.Create(IniFileName);
     TheStrings:=TStringList.Create;
     Try
         TheIni.ReadSectionValues(Section,TheStrings);
         For I:=0 To TheStrings.Count-1 Do
         Begin
              if TheForm.FindComponent(TheStrings.Names[I])<>Nil Then
              begin
                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmDBHEdit Then
                 Begin
                      TAtmDBHEdit(TheForm.FindComponent(TheStrings.Names[I])).IsSkipTab:=True;
                      TAtmDBHEdit(TheForm.FindComponent(TheStrings.Names[I])).SetF1StatusColor;
                      Continue;
                 End;
                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmDBDateEdit Then
                 Begin
                      TAtmDBDateEdit(TheForm.FindComponent(TheStrings.Names[I])).IsSkipTab:=True;
                      TAtmDBDateEdit(TheForm.FindComponent(TheStrings.Names[I])).SetF1StatusColor;
                      Continue;
                 End;
                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmHComboBox Then
                 Begin
                      TAtmHComboBox(TheForm.FindComponent(TheStrings.Names[I])).IsSkipTab:=True;
                      TAtmHComboBox(TheForm.FindComponent(TheStrings.Names[I])).SetF1StatusColor;
                      Continue;
                 End;
                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmDbHMemo Then
                 Begin
                      TAtmDbHMemo(TheForm.FindComponent(TheStrings.Names[I])).IsSkipTab:=True;
                      TAtmDbHMemo(TheForm.FindComponent(TheStrings.Names[I])).SetF1StatusColor;
                      Continue;
                 End;
{                 if TheForm.FindComponent(TheStrings.Names[I]) is TAtmDBDateEdit Then
                 Begin
                      TAtmDBDateEdit(TheForm.FindComponent(TheStrings.Names[I])).FixedString:=
                         TheStrings.Values[TheStrings.Names[I]];
                         TAtmDBDateEdit(TheForm.FindComponent(TheStrings.Names[I])).IsFixed:=True;
                      Continue;
                 End;}
                 if StringsToAdd <> nil then
                    StringsToAdd.Add(TheStrings[i]);
              End;
         End;

     Finally
            TheIni.Free;
            TheStrings.Free;
     End;
End;

procedure RefreshAllSearchWindows(Sender: TForm);
Var
   I :LongInt;
begin
     For I:=0 To Sender.ComponentCount-1 Do
         if Sender.Components[I] is TAtmAdvSearch Then
            if (Sender.Components[I] As TAtmAdvSearch).KeepList Then
               (Sender.Components[I] As TAtmAdvSearch).ClearList;
end;

Procedure KeyboardManageForTableAction(Sender :TObject;Var Key :Word;Shift: TShiftState;
                                       TheTable :TDataSet);
Var
   MyControl :TWinControl;
   EnterCase :Boolean;
Begin
  EnterCase:=True;
  case Key of
     VK_Next  :if (TheTable is TAtmRxQuery) Then
                      TAtmRxQuery(TheTable).Next
               Else
                 TheTable.Next;
     VK_Prior :if (TheTable is TAtmRxQuery) Then
                      TAtmRxQuery(TheTable).Prior
               Else
               TheTable.Prior;
     VK_F3    :if  TheTable.State in [dsEdit ,dsInsert] then
               Begin
                    if (TheTable is TAtmTable) Then
                      TAtmTable(TheTable).SavePressed:=True;
                    if (TheTable is TAtmRxQuery) Then
                      TAtmRxQuery(TheTable).SavePressed:=True;
                    TheTable.Post;
               End;
     VK_Home  :if  Shift = [ssCtrl] then
                 if (TheTable is TAtmRxQuery) Then
                      TAtmRxQuery(TheTable).First
                 Else
                   TheTable.First;
     VK_End   :if  Shift = [ssCtrl] then
                 if (TheTable is TAtmRxQuery) Then
                      TAtmRxQuery(TheTable).Last
                 Else
                   TheTable.Last;
     VK_F11   :Begin
                    if (TheTable is TAtmTable) Then
                      TAtmTable(TheTable).SavePressed:=True;
                    if (TheTable is TAtmRxQuery) Then
                      TAtmRxQuery(TheTable).SavePressed:=True;
                    TheTable.Insert;
               End;
     VK_Escape : if Shift = [ssShift] Then
                    TheTable.Cancel;
     VK_DOWN ://���� ����� �� ���
                        Begin
{                     38: begin                   // Up
                            SendMessage( GetParentForm(Self).Handle, WM_NEXTDLGCTL, VK_SHIFT, 0);
                            Key := 0;
                          end;
                      40: begin                   // Down
                            SendMessage( GetParentForm(Self).Handle, WM_NEXTDLGCTL, 0, 0);
                            Key := 0;
                          end;
}

                            if (Sender is TWinControl) Then
                            Begin
                                  MyControl:=(Sender As TWinControl);
                                  if MyControl is TForm Then
                                     MyControl:=TForm(MyControl).ActiveControl;
                                  if (MyControl Is TDBLookupComboBox) then exit;
                                  if (MyControl is TCustomGrid) Or (MyControl Is TCustomMemo) Or
                                     ((MyControl Is TCustomComboBox) And (Key<>VK_RETURN)) Then
                                     Exit;
                            End;

{                            if (TWinControl(Sender).Perform(CM_WANTSPECIALKEY, VK_Tab, 0) =0) And
                               (TWinControl(Sender).Perform(WM_GETDLGCODE, 0, 0) and DLGC_WANTTAB = 0) and
                               (GetParentForm(TWinControl(Sender)).Perform(CM_DIALOGKEY,VK_TAB, VK_TAB) <> 0) then
                                    Exit;
}
                            //SendMessage( GetParentForm(TWinControl(Sender)).Handle, WM_NEXTDLGCTL, 0, 0);
                            PostMessage( GetParentForm(TWinControl(Sender)).Handle, WM_NEXTDLGCTL, 0, 0);
                        End;
     VK_UP    :Begin
                    if (Sender is TWinControl) Then
                    Begin
                          MyControl:=(Sender As TWinControl);
                          if MyControl is TForm Then
                             MyControl:=TForm(MyControl).ActiveControl;
                          if (MyControl Is TDBLookupComboBox) then exit;
                          if (MyControl is TCustomGrid) Or (MyControl Is TCustomMemo) Or
                             ((MyControl Is TCustomComboBox) And (Key<>VK_RETURN)) Then
                             Exit
                    End;
                    if Not (Sender is TWinControl) Then Exit;
                    MyControl:=(Sender As TWinControl);
                    if MyControl is TForm Then
                       MyControl:=TForm(MyControl).ActiveControl;
                    MyControl:=FindGlobalNextControl(MyControl,MyControl.Parent,
                               False,True,False);
                    if MyControl<>Nil Then
                       MyControl.SetFocus;
               End;
  Else
      EnterCase:=False;
  End;//Case
  if EnterCase Then
     Key:=0;
End;

Procedure GetMehironPrice(MehironKey,CodeHiuv,CodeSherut,DefaultSugLink:LongInt;
                          TnuaDataSet:TDataSet;TempQry :TQuery;
                          Var Price1,Price2,Price3,Price4:Real;
                          Var YMida:Integer;
                          IncludeNumber: Integer=1;
                          StopCalcInRange: Boolean=False);
Var
   TempKamut : Real ;
   TempPrice1,TempPrice2,TempPrice3,TempPrice4:Real;
   SugLink :LongInt;
   PriceTList :TMhirList;
   I :LongInt;
   Mhir,MaxMehir,MinMehir :TMhirClass;
   DoMaxMinMehir:boolean;
   RangeKamut:Real;
   CalculationDone: Boolean;

   MhrLog:TStringList;
   OK:Boolean;
   TempDir:String;
   Function I2s(I:Integer):String; Begin Result:=InttoStr(I); End;

Begin
//  ShowMessage('Start GetMehironPrice ');
  PriceTList:=TMhirList.Create;
  MhrLog:=TStringList.Create;
  MhrLog.Add('<< Parameters >>');
  MhrLog.Add('Mehiron Key, SugHiuv, CodeSherut, DefaultSugLink');
  MhrLog.Add(I2S(MehironKey)+', '+ I2S(CodeHiuv)+', '+I2S(CodeSherut)+', '+I2S(DefaultSugLink));
  Try
     MaxMehir:=Nil;
     MinMehir:=Nil;
     Price1:=0;
     Price2:=0;
     Price3:=0;
     Price4:=0;
     MhrLog.Add('<< START >>');
     With TempQry Do
     Begin
       Active:=False;
       Sql.Clear;
       Sql.Add('Select * From MHIRCHLD');
       Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
       Active:=True;
//       ShowMessage('Open Table MHIRCHLD where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
       YMida:=1;
       First;
       While Not Eof Do
       Begin
//          If CodeSherut=FieldByName('SUG_SHERUT_IN_TNUA').AsInteger Then YMida:=FieldByName('YEHIDAT_MIDA').AsInteger;
          CalculationDone := False;
          OK := False;
          MhrLog.Add('<<-------------------->>');
          MhrLog.Add('<< MehirChild DETAILS >>');
          MhrLog.Add('Sug Sherut In tnua, Yehidat Mida, Price1, Range field, Kamut field');
          MhrLog.Add(FieldByName('SUG_SHERUT_IN_TNUA').AsString+', '+
          FieldByName('YEHIDAT_MIDA').AsString+', '+FieldByName('PRICE1').AsString+
          FieldByName('RANGE_FIELD_NAME').AsString+', '+FieldByName('SUG_KAMUT_FIELD_NAME').AsString);


          MhrLog.Add('<< START >>');
          MhrLog.Add('Mehiron: Checking whether to do a range/multiply calculation.');

          if  (
                (FieldByName('RANGE_FIELD_NAME').AsString<>'')       // �� ��� ����
                And
                (TnuaDataSet.FindField(FieldByName('RANGE_FIELD_NAME').AsString)<>Nil)  // ����� ���� ���� ������
              ) Then
          Begin
            OK:=True;
            MhrLog.Add(' -> True. Range field is defined in mehiron ('+FieldByName('RANGE_FIELD_NAME').AsString+'), and exists in shib.');
          end;
          If
              (
                (FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'')        // �� ��� ������
                And
                (TnuaDataSet.FindField(FieldByName('SUG_KAMUT_FIELD_NAME').AsString)<>Nil)  // ���� ������ ���� ������
              ) Then
          Begin
            OK:=True;
            MhrLog.Add(' -> True. Field for kamut is defined in mehiron ('+FieldByName('SUG_KAMUT_FIELD_NAME').AsString+', and exists in shib.');
          end;
          If
              (
                (FieldByName('RANGE_FIELD_NAME').AsString='')      // ��� ����
                And                                                 // ���
                (FieldByName('SUG_KAMUT_FIELD_NAME').AsString='')   // ��� ��� ������
              ) Then
          Begin
            OK:=True;
            MhrLog.Add(' -> True. No range field AND no field for Kamut.');
          end;
          if
              (FieldByName('SUG_KAMUT_FIELD_NAME').AsString = 'Quntity1/1000')  { ������ ����}
              Or
              (FieldByName('SUG_KAMUT_FIELD_NAME').AsString = 'Nefh/1000')      {������ ���}
          Then Ok:=True;

          If Ok Then
          Begin

            MhrLog.Add('Setting TEMP_Kamut to default value. Checking if current Code sherut in Mehir Child');
            MhrLog.Add('is our "main Code Sherut" - the one entered in Shib.');
            if (CodeSherut<>FieldByName('SUG_SHERUT_IN_TNUA').AsInteger) Then
            Begin
              TempKamut:=0;
              MhrLog.Add(' -> False. TempKamut = 0');
            End
            Else
            Begin
              TempKamut:=1;
              MhrLog.Add(' -> True. TempKamut = 1');
            End;

            SugLink:=DefaultSugLink;
            if (Not FieldByName('SUG_HISHUV').IsNull) {or
               (FieldByName('SUG_HISHUV').AsInteger <> slFix) } Then
               SugLink:=FieldByName('SUG_HISHUV').AsInteger;

            MhrLog.Add('<< GET KAMUT >>');
            if FieldByName('SUG_KAMUT_FIELD_NAME').AsString = 'ShibCarKind' Then {��� ��� �����}
              TempKamut:= TnuaDataSet.FieldByName('Quntity1').AsFloat
            else
            if FieldByName('SUG_KAMUT_FIELD_NAME').AsString = 'Quntity1/1000' Then {������}
              TempKamut:= TnuaDataSet.FieldByName('Quntity1').AsFloat  * 0.001
            else
            if FieldByName('SUG_KAMUT_FIELD_NAME').AsString = 'Nefh/1000' Then {������ ���}
                  TempKamut:=TnuaDataSet.FieldByName('Nefh').AsFloat * 0.001
            else
            begin
              MhrLog.Add('Shib: Trying to fing the field "'+FieldByName('SUG_KAMUT_FIELD_NAME').AsString+'" to use as Kamut');
              if FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'' Then
                 if TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsString<>'' Then
                 Begin
                    TempKamut:=TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsFloat;
                    MhrLog.Add(' -> Found. TempKamut value is '+FloatToStr(TempKamut)+'.');
                 End
                 Else
                    MhrLog.Add(' -> Not found.');
            end;
            Try
                //����� ��� �����

                MhrLog.Add('<< GET RANGE VALUE >>');
                MhrLog.Add('Setting the range value to check as TempKamut - '+FloatToStr(TempKamut));
                RangeKamut:=TempKamut;
                MhrLog.Add('Shib: Trying to find the field "'+FieldByName('RANGE_FIELD_NAME').AsString+'" to use as Range');
                if FieldByName('RANGE_FIELD_NAME').AsString<>'' Then
                   if TnuaDataSet.FieldByName(FieldByName('RANGE_FIELD_NAME').AsString).AsString<>'' Then
                   Begin
                      RangeKamut:=TnuaDataSet.FieldByName(FieldByName('RANGE_FIELD_NAME').AsString).AsFloat;
                      MhrLog.Add(' -> Found. Range value is '+FloattoStr(RangeKamut)+'.');

                   End
                   Else
                      MhrLog.Add(' -> Not found.');

                MhrLog.Add('<< VERIFYING RANGE >>');
                if FieldByName('RANGE_FIELD_NAME').AsString<>'' Then
                begin

                  MhrLog.Add('Checking if from-value exists in mehiron.');
                  if Not(FieldByName('FROM_VALUE').IsNull) and (FieldByName('FROM_VALUE').AsString<>'') Then
                  Begin
                    MhrLog.Add(' -> Found. Checking if current range value is > '+FieldByName('FROM_VALUE').AsString+' (which is from-value)');
                    if (FieldByName('FROM_VALUE').AsFloat)>RangeKamut{TempKamut} Then
                    Begin
                      MhrLog.Add('   -> Value is not in range. >> exit.');
                      Raise ERangeError.Create('');
                    End
                    Else
                      MhrLog.Add('   -> Value in range. >> continue.');
                  End
                  Else MhrLog.Add(' -> Not found.');

                  MhrLog.Add('Checking if to-value exists in mehiron.');
                  if Not(FieldByName('TO_VALUE').IsNull) and (FieldByName('TO_VALUE').AsString<>'') Then
                  Begin
                    MhrLog.Add(' -> Found. Checking if current range value is < '+FieldByName('TO_VALUE').AsString+' (which is to-value)');
                    if ((FieldByName('TO_VALUE').AsFloat)<RangeKamut{TempKamut}) And (SugLink<>slRelative) Then
                    Begin
                      MhrLog.Add('   -> Value is not in range >> exit.');
                      Raise ERangeError.Create('');
                    End
                    Else
                      MhrLog.Add('   -> Value in range. >> continue.');
                  End
                  Else MhrLog.Add(' -> Not found.');
                end;

            // ����� ����� �����
                MhrLog.Add('<< YEHIDAT MIDA >>');
                MhrLog.Add('Checking if current Code Sherut is like in MehironChild line.');
                If CodeSherut=FieldByName('SUG_SHERUT_IN_TNUA').AsInteger Then
                Begin
                  YMida:=FieldByName('YEHIDAT_MIDA').AsInteger;
                  MhrLog.Add(' -> Yes. Copy current Yehidat Mida from Mehiron to Shib.');
                End
                Else
                  MhrLog.Add(' -> No. Do NOT copy Yehidat Mida from Mehiron.');

                CalculationDone := True;

                MhrLog.Add('<< SUG LINK >>');
                MhrLog.Add('Checking if Code Sherut is not like in Mehiron, and Link is Relative.');
                if (CodeSherut<>FieldByName('SUG_SHERUT_IN_TNUA').AsInteger) And (SugLink=slRelative) Then
                Begin
                     MhrLog.Add(' -> True. >> Checking if '+FloatToStr(TempKamut)+' < '+FieldByName('FROM_VALUE').AsString+' (from-value).');
                     if (FieldByName('FROM_VALUE').AsFloat<=TempKamut) Then
                     Begin
                         MhrLog.Add('   -> True. >> Checking if '+FloatToStr(TempKamut)+' > '+FieldByName('TO_VALUE').AsString+' (to-value).');
                         if (FieldByName('TO_VALUE').AsFloat<=TempKamut) Then
                         Begin
                            MhrLog.Add('     -> True. >> TempKamut=TO_VALUE - FROM_VALUE + INCLUDE_NUMBER ');
                            MhrLog.Add('                   ='+FieldByName('TO_VALUE').AsString+' - '+FieldByName('FROM_VALUE').AsString+' + '+I2S(IncludeNumber));
                            TempKamut:=FieldByName('TO_VALUE').AsFloat-FieldByName('FROM_VALUE').AsFloat+IncludeNumber; //- Frac(FieldByName('FROM_VALUE').AsFloat) {mod-1}
                            MhrLog.Add('           TempKamut='+FloatToStr(TempKamut));
                         End
                         Else
                         Begin
//                             if Frac(FieldByName('FROM_VALUE').AsFloat) = 0 then
                            MhrLog.Add('     -> False. >> TempKamut=TempKmut - FROM_VALUE + INCLUDE_NUMBER ');
                            MhrLog.Add('                   ='+FloatToStr(TempKamut)+' - '+FieldByName('FROM_VALUE').AsString+' + '+I2S(IncludeNumber));
                            TempKamut:=TempKamut-FieldByName('FROM_VALUE').AsFloat+IncludeNumber;
                            MhrLog.Add('           TempKamut='+FloatToStr(TempKamut));
                         End
//                             else
//                                 TempKamut:=TempKamut-FieldByName('FROM_VALUE').AsFloat;
                     End
                     Else
                       MhrLog.Add(' -> False. TempKamut is not changed.');

                End;
                MhrLog.Add(' -> False. Code Sherut = Mehiron , or, Link is not RELATIVE.');

                MhrLog.Add('Checking if Link type is Fixed, or (Yehidat Mida not global and Kamut Field shoud be Quntity1)');
                if (SugLink = slFix)
                   or ((YMida<>0) and (UpperCase(FieldByName('SUG_KAMUT_FIELD_NAME').AsString)='QUNTITY1'))
                   Then // ���� ���� ��� �����
                   Begin
                     MhrLog.Add(' -> True. Multiply the price by 1 (fixed price).');
                     TempKamut:=1;
                   End
                   Else
                     MhrLog.Add(' -> False. Multiply the price by '+ FloatToStr(TempKamut)+'.');


                Begin//����� ���� �����
                     TempPrice1:=FieldByName('Price1').AsFloat*TempKamut;
                     TempPrice2:=FieldByName('Price2').AsFloat*TempKamut;
                     TempPrice3:=FieldByName('Price3').AsFloat*TempKamut;
                     TempPrice4:=FieldByName('Price4').AsFloat*TempKamut;

                     MhrLog.Add(' TempPrice1 = '+FieldByName('Price1').AsString+' (p1) X '+FloatToStr(TempKamut) + ' (tempkamut) = '+FloatToStr(TempPrice1));
                     MhrLog.Add(' TempPrice1 = '+FieldByName('Price2').AsString+' (p2) X '+FloatToStr(TempKamut) + ' (tempkamut) = '+FloatToStr(TempPrice2));
                     MhrLog.Add(' TempPrice1 = '+FieldByName('Price3').AsString+' (p3) X '+FloatToStr(TempKamut) + ' (tempkamut) = '+FloatToStr(TempPrice3));
                     MhrLog.Add(' TempPrice4 = '+FieldByName('Price4').AsString+' (p4) X '+FloatToStr(TempKamut) + ' (tempkamut) = '+FloatToStr(TempPrice4));
{                     ShowMessage('Prices: Price1: ' + FloatToStr( TempPrice1 ) +
                                 ' Price2: ' + FloatToStr( TempPrice2 ) +
                                 ' Price3: ' + FloatToStr( TempPrice3 ) +
                                 ' Price4: ' + FloatToStr( TempPrice4 ));
}
                End;

                Case SugLink of
                  slSum,slRelative,slFix:  //����� ����
                    Begin
                        // ����� ��� ������ �� �� ����� �� ��� �� ���
                       if (Not FieldByName('SUG_BHIRA').IsNull) And
                          (FieldByName('SUG_BHIRA').AsInteger<>sbNone) Then
                       Begin
                            if PriceTList.FindItem(FieldByName('SUG_SHERUT_IN_TNUA').AsInteger)=Nil Then
                                  PriceTList.Add(TMhirClass.Create(FieldByName('SUG_SHERUT_IN_TNUA').AsInteger,
                                                 TempPrice1,
                                                 TempPrice2,
                                                 TempPrice3,
                                                 TempPrice4,
                                                 FieldByName('SUG_BHIRA').AsInteger,
                                                 FieldByName('CALC_GROUP').AsInteger))
                            Else
                                With TMhirClass(PriceTList.FindItem(FieldByName('SUG_SHERUT_IN_TNUA').AsInteger)) Do
                                Begin
                                     FPrice1:=FPrice1+TempPrice1;
                                     FPrice2:=FPrice2+TempPrice2;
                                     FPrice3:=FPrice3+TempPrice3;
                                     FPrice4:=FPrice4+TempPrice4;
                                End;
                       End

                       Else //SUG_BHIRA=sbNone
                       Begin
//                            ShowMessage('Sug_Bhira = sbNone ');
                            Price1:=Price1+TempPrice1;
                            Price2:=Price2+TempPrice2;
                            Price3:=Price3+TempPrice3;
                            Price4:=Price4+TempPrice4;
{                            ShowMessage('Prices: Price1: ' + FloatToStr( Price1 ) +
                                 ' Price2: ' + FloatToStr( Price2 ) +
                                 ' Price3: ' + FloatToStr( Price3 ) +
                                 ' Price4: ' + FloatToStr( Price4 ));
}


                       End;
                    End;

                  slMax: //�������
                    Begin
                       Price1:=Max(Price1,TempPrice1);
                       Price2:=Max(Price2,TempPrice2);
                       Price3:=Max(Price3,TempPrice3);
                       Price4:=Max(Price4,TempPrice4);
                    End;
                  slMaxTotal:
                       MaxMehir:=TMhirClass.Create(FieldByName('SUG_SHERUT_IN_TNUA').AsInteger,
                                                 TempPrice1,
                                                 TempPrice2,
                                                 TempPrice3,
                                                 TempPrice4,
                                                 FieldByName('SUG_BHIRA').AsInteger,
                                                 FieldByName('CALC_GROUP').AsInteger);
                  slMinTotal:
                       MinMehir:=TMhirClass.Create(FieldByName('SUG_SHERUT_IN_TNUA').AsInteger,
                                                 TempPrice1,
                                                 TempPrice2,
                                                 TempPrice3,
                                                 TempPrice4,
                                                 FieldByName('SUG_BHIRA').AsInteger,
                                                 FieldByName('CALC_GROUP').AsInteger);
                End; //Case

            Except on
                   ERangeError Do;
            End;

          End; //  if ((FieldByName('RANGE_FIELD_NAME').AsString<>'') And (TnuaDataSet.FindField(FieldByName('RANGE_FIELD_NAME').AsString)<>Nil)) Or...

          if StopCalcInRange then
             if (SugLink = slFix) and
                CalculationDone Then //   ���� ���� ��� ����� ����� �����
                Break;

          Next;
       End;// While Not Eof
     End;//With Qry_Generic

     //����� ���� ������ ������ ����� ��� ��� �����
     Mhir:=Nil;
     if PriceTList.Count>0 Then
        Mhir:=TMhirClass(PriceTList.Items[0]);
        
     For I:=0 To PriceTList.Count-1 Do
     Begin
          Case Mhir.SugBhira of
               sbMax: if Mhir.FPrice1<TMhirClass(PriceTList.Items[i]).FPrice1 Then
                         Mhir:=TMhirClass(PriceTList.Items[i]);
               sbMin: if Mhir.FPrice1>TMhirClass(PriceTList.Items[i]).FPrice1 Then
                         Mhir:=TMhirClass(PriceTList.Items[i]);
          End;
     End;
     if Mhir<>Nil Then
     Begin
          // ����� ���� ������� ��������
          case DefaultSugLink of 
               slMax: begin
                        Price1:=Max(Price1,Mhir.FPrice1);
                        Price2:=Max(Price2,Mhir.FPrice2);
                        Price3:=Max(Price3,Mhir.FPrice3);
                        Price4:=Max(Price4,Mhir.FPrice4);
                      end;
               else
               begin
                    Price1:=Price1+Mhir.FPrice1;
                    Price2:=Price2+Mhir.FPrice2;
                    Price3:=Price3+Mhir.FPrice3;
                    Price4:=Price4+Mhir.FPrice4;
               end;
          end;
     End;

     //����� ���� ���' ����' ����

     DoMaxMinMehir:=True;
     if (MaxMehir<>Nil) And (MinMehir<>Nil) Then
        if MaxMehir.FPrice1<MinMehir.FPrice1 Then
        Begin
            MessageDlg('���� ������� ��� ����� ������� ������� ��',mtError,[mbOk],0);
            DoMaxMinMehir:=False;
        End;
     if DoMaxMinMehir Then
     Begin
         if MaxMehir<>Nil Then
         Begin
            if MaxMehir.FPrice1<Price1 Then
            Begin
              Price1:=MaxMehir.FPrice1;
              Price2:=MaxMehir.FPrice2;
              Price3:=MaxMehir.FPrice3;
              Price4:=MaxMehir.FPrice4;
            End;
         End;

         if MinMehir<>Nil Then
            if MinMehir.FPrice1>Price1 Then
            Begin
            Price1:=MinMehir.FPrice1;
            Price2:=MinMehir.FPrice2;
            Price3:=MinMehir.FPrice3;
            Price4:=MinMehir.FPrice4;
            End;
     End;
  Finally
       TempDir:=GetTempDir;
       If TempDir[Length(TempDir)]<>'\' Then TempDir:=TempDir+'\';
       TempDir := TempDir+ ChangeFileExt(ExtractFileName(Application.exeName), '');
       If TempDir[Length(TempDir)]<>'\' Then TempDir:=TempDir+'\';

     MhrLog.SaveToFile(TempDir+'Mehir.txt');
     MhrLog.Free;
     TempQry.Close;
     PriceTList.Free;
     if MaxMehir<>Nil Then
        MaxMehir.Free;
     if MinMehir<>Nil Then
        MinMehir.Free;
  End;
//  ShowMessage('End Of GetMehironPrice with Price1: '+ FloatToStr( Price1 ) + ' Price2: ' + FloatToStr( Price2) + ' Price3: ' + FloatToStr( Price3) + ' Price4: ' + FloatToStr( Price4));
End;

Procedure BuildCrtMenu(MasterMenu :TMenuItem;IniFileName,IniSection :String;TheEvent :TNotifyEvent);
Var
  T :TIniFile;
  S :TstringList;
  TempMenu:TMenuItem;
  I:longInt;
Begin
  T:=TIniFile.Create(IniFileName);
  S:=TStringList.Create;
  Try
    T.ReadSectionValues(IniSection,S);
    For I:=0 To S.Count-1 Do
    Begin
        TempMenu:=TMenuItem.Create(MasterMenu);
        TempMenu.Caption:=S.Names[i];
        TempMenu.Tag:=StrToInt(S.Values[S.Names[i]]);
        TempMenu.OnClick:=TheEvent;
        MasterMenu.Add(TempMenu);
    End;

  Finally
      T.Free;
      S.Free;
  End;
End;

Function DateToSqlStr(TheDate :TDateTime;FormatStr :String):String;
Var
  OldDateFormat :String;
Begin
    OldDateFormat :=ShortDateFormat;
    ShortDateFormat:=FormatStr;
    Try
      Result:=DateToStr(TheDate);
    Finally
      ShortDateFormat:=OldDateFormat;
    End;
End;

Procedure SetDatasetState(DataSet:TDataSet;TheState :Byte);
Begin
 Try
    Case TheState of
      tsInsert : DataSet.Insert;
      tsFirst  : DataSet.First;
      tsLast   : DataSet.Last;
    End;
 Except
 End;
End;

Function ReverseWords(InString:String) :String;
Var
   Buffer :String[255];
   I      :Integer;
Begin
   I:=Pos(' ',InString);
   Buffer:='';
   While (I>0)  Do
   Begin
        Buffer:=Copy(InString,1,I)+Buffer;
        InString:=Copy(InString,I+1,Length(InString));
        I:=Pos(' ',InString);
        ShowMessage(IntToStr(I)+'  '+InString);
   End; {While}
   ReverseWords:=InString+' '+Buffer;
End;{ ReverseWords}

Procedure BuildSqlScriptForInsert(TheStrings :TStringList;TheDataset :TDataset;TheTableName :String);
Var
  I :longInt;
  StrValue,StrName :String;
Begin
  ShortDateFormat:='DD/MM/YYYY';
  Try
    TheStrings.Clear;
    TheStrings.Add('Insert Into '+UpperCase(TheTableName));
    StrName:='';
    StrValue:='';
    For I:=0 To TheDataset.Fields.Count-1 Do
      if (Not TheDataset.Fields[i].IsNull) And (TheDataset.Fields[i].FieldKind=fkData) Then
      Begin
        if StrName<>'' Then
          StrName:=StrName+',';
        StrName:=StrName+TheDataset.Fields[i].FieldName;

        if StrValue<>'' Then
          StrValue:=StrValue+',';
        if TheDataset.Fields[i] Is TNumericField Then
          StrValue:=StrValue+TheDataset.Fields[i].AsString
        Else
        if TheDataset.Fields[i] Is TDateTimeField Then
          StrValue:=StrValue+''''+FormatDateTime('yyyymmdd hh:nn:ss',TheDataset.Fields[i].AsDateTime)+''''
        Else
          StrValue:=StrValue+''''+AddQuatesToString(TheDataset.Fields[i].AsString)+'''';
      End;
    StrName:='('+StrName+')';
    StrValue:='('+StrValue+')';
    TheStrings.Add(StrName);
    TheStrings.Add('Values');
    TheStrings.Add(StrValue);
  Finally
    ShortDateFormat:='DD/MM/YYYY';
  End;
End;

Function AddQuatesToString(TheString :String ):String;
Var
  I :longInt;
  StrH :String;
Begin
  StrH:='';
  For I := 1 To Length(TheString) Do
  Begin
    if TheString[i]='''' Then
    Begin
      StrH:=StrH+TheString[i];
      StrH:=StrH+'''';
    End
    Else
      if TheString[i]='"' Then
      Begin
        StrH:=StrH+'''''';//+TheString[i];
      End
      Else
        StrH:=StrH+TheString[i];
  End;
  Result:=StrH;
End;

Procedure PutLookupFieldsAsDefaultFromIni(IniFileName,IniSection:String;MatchDataset,DestDataset:TDataset;TempRxQry :TRxQuery);
//����� ����� ������� ��� ������ ����� ������
Var
  F :TIniFile;
  TheLines:TStringList;
  I,J :longInt;
Begin
  F:=TIniFile.Create(IniFileName);
  TheLines:=TStringList.Create;
  Try
    F.ReadSectionValues(IniSection,TheLines);
    For I:=0 To TheLines.Count-1 Do
    Begin
      TempRxQry.Close;
      TempRxQry.Sql.Clear;
      TempRxQry.Sql.Add(TheLines.Values[TheLines.Names[I]]);
      For J:=0 To TempRxQry.Macros.Count-1 Do
        TempRxQry.Macros[j].AsString:=MatchDataSet.FieldByName(TempRxQry.Macros[j].Name).AsString;
      TempRxQry.Open;
      if Not ((TempRxQry.Eof) And (TempRxQry.BOF)) Then
        DestDataSet.FieldByName(TheLines.Names[I]).AsString:=TempRxQry.FieldByName(TheLines.Names[I]).AsString;
      TempRxQry.Close;
    End;
  Finally
    F.Free;
    TheLines.Free;
  End;
End;

{Rani Functions}
function SendMail (ToMail , Subject : String ; Body : TStringList):Integer;
{����� 0 ������� �� -1 ������}
const
  RECIP_MAX       = MaxInt div SizeOf(TMapiRecipDesc);
  ATTACH_MAX      = MaxInt div SizeOf(TMapiFileDesc);
type
  { array of structures for TO, CC, and BCC }
  TRecipAccessArray     = array [0 .. (RECIP_MAX - 1)] of TMapiRecipDesc;
  TlpRecipArray         = ^TRecipAccessArray;
  { array of structures for attachments }
  TAttachAccessArray    = array [0 .. (ATTACH_MAX - 1)] of TMapiFileDesc;
  TlpAttachArray        = ^TAttachAccessArray;
var
    lppMapiMessage: MapiMessage; // main message info pointer
    hSession: ULONG;
    lppMapiFileDesc: MapiFileDesc; // attachment info pointer
    flFlags: ULONG;
    lparrayRecips: TlpRecipArray;
    lppMapiRecipDesc: TMapiRecipDesc; // recipient info pointer
    lparrayAttachments: TlpAttachArray;
    err: ULONG;
    AnyStr: PChar;
    nAttachments,
    nRecipients: Cardinal;
begin
   try
          nAttachments := 1;
          nRecipients := 0;
          if ToMail <> '' then
             Inc(nRecipients);

          { assign 0 to all structure options }
          FillChar(lppMapiRecipDesc, SizeOf(TMapiRecipDesc), 0);
          lparrayRecips  := TlpRecipArray(StrAlloc(nRecipients*SizeOf(TMapiRecipDesc)));
          FillChar(lparrayRecips^, StrBufSize(PChar(lparrayRecips)), 0);
          lparrayAttachments := TlpAttachArray(StrAlloc(nAttachments*SizeOf(TMapiFileDesc)));
          FillChar(lparrayAttachments^, StrBufSize(PChar(lparrayAttachments)), 0);
          { info about recipient }
          lparrayRecips^[0].ulReserved := 0;
          lparrayRecips^[0].ulRecipClass := MAPI_TO;
          AnyStr := StrAlloc(length(ToMail)+1);
          StrPCopy(AnyStr,ToMail);
          lparrayRecips^[0].lpszName := AnyStr;
          lparrayRecips^[0].lpszAddress := nil;
          lparrayRecips^[0].ulEIDSize := 0;
          lparrayRecips^[0].lpEntryID := nil;
          AnyStr := nil;
          StrDispose(AnyStr);


          { main structure that is used to send the message }
          lppMapiMessage.ulReserved := ULONG(0);
          lppMapiMessage.lpszSubject := PChar(Subject);
          if Assigned (Body) Then
             lppMapiMessage.lpszNoteText := PChar(Body.Text);
          lppMapiMessage.lpszMessageType := nil;
          lppMapiMessage.lpszDateReceived := nil;
          lppMapiMessage.lpszConversationID := nil;
          lppMapiMessage.flFlags := ULONG(0);
          lppMapiMessage.lpOriginator := nil;
          lppMapiMessage.nRecipCount := nRecipients;
          lppMapiMessage.lpRecips := @lparrayRecips^;
          lppMapiMessage.nFileCount := {nAttachments}0;
          lppMapiMessage.lpFiles := nil;{@lparrayAttachments^};
          err := MAPISendMail(0,0,lppMapiMessage,0,0);
          if err <> SUCCESS_SUCCESS then
          begin
             ShowMessage('Error');
             result := -1;
          end;
          result := 0;

   except on e:exception do
   begin
      showMessage (e.message);
      Result := -1;
   end;
   end;{Try}
end;

Function ReplaceString (SourceString,TextToFind,ReplaceWith : String;NumberOfReplace:Integer):String;{������ ����� ������� �����}
var
   PosIndex,Num : Integer;
   Part :String;
begin
      Part := '';
      Num := 0;{���� ������}
      while (pos (UpperCase (TextToFind),UpperCase(SourceString)) <> 0) And
             ((NumberOfReplace=0)Or (NumberOfReplace>num)) do
      begin
	           PosIndex := pos (UpperCase(TextToFind),UpperCase(SourceString));
           delete (SourceString,PosIndex,Length (TextToFind));
           insert (ReplaceWith,SourceString,PosIndex);
           Part := Part + Copy (SourceString,1,Pos (ReplaceWith,SourceString)+Length(ReplaceWith));
           Delete (SourceString,1,Length (Copy (SourceString,1,Pos (ReplaceWith,SourceString)+Length(ReplaceWith))));
           inc (Num);
      end;{While}
      ReplaceString := Part + SourceString;
end;

Function MaskToNum (MaskStr:String):String;
var{����� ���� ������� ������� �� ������� ���}
    tmpStr:String;
    i,Code : Integer;
    TmpReal : Real;
begin
try
     tmpStr:='';
     for i:= 1 to length (MaskStr) do
     begin
        if ((MaskStr[i] >= '0') And (MaskStr[i] <= '9'))Or
            (MaskStr[i]='.')or (MaskStr[i]='-') Then
           tmpStr:=tmpStr+MaskStr[i];
     end;
     Val (TmpStr,TmpReal,Code);
     If Code <> 0  Then
         Result := '0'
     else
         Result :=tmpStr;
 except
     Result :='0';
 end;{Try}
end;
{End Rani Functions}

{oren functions}
function InWords(TheNumber:LongInt;Male:boolean = True):string;
const
  NumberOneToTenMale:Array[1..9] of String =( '���', '�����', '�����','�����' , '�����', '���', '����', '�����', '����');
  NumberOneToTenFemale  :Array[1..9] of String =('���','�����', '����','����' , '���', '��', '���', '�����', '���');
  Asarot:array[1..9] of String =('���','�����','������','������','������','�����','�����','������','�����');
  Sp:string = ' ';
  Mataim:string = '������';
  Meah:string = '���';
  Ten:string = '����';
var
  StrToReturn:String;
begin
//showmessage(IntToStr(TheNumber  10) );
  case TheNumber of
  0     : Result:='���';
  1..9  : Begin
          if male then
             Result:=NumberOneToTenMale[TheNumber]
          else Result:=NumberOneToTenFemale[TheNumber];
          end;
  10      : Result:=Asarot[1];
  11..19  : if Male then
              Result:=NumberOneToTenmale[TheNumber mod 10] +sp +Asarot[1]
            else Result:=NumberOneToTenFemale[TheNumber mod 10] +sp +Ten;
  20      : Result:=Asarot[TheNumber div 10];
  21..99  : begin
              if (TheNumber mod 10) > 0 then
              begin
                   if Male then
                     StrToReturn:= '�' + NumberOneToTenMale[TheNumber mod 10]
                   else StrToReturn:= '�' + NumberOneToTenFemale[TheNumber mod 10];
              end;
              Result:=Asarot[TheNumber div 10] +sp +StrToReturn ;
            end;
  100     : Result:=Meah;
  101..119: Result:=Meah +sp +'�' +InWords(TheNumber mod 100,Male);
  120..199: Result:=Meah +sp +InWords(TheNumber mod 100,Male);
  200     : Result:=Mataim;
  201..219: Result:=Mataim +sp +'�' +InWords(TheNumber mod 100,Male);
  220..299: Result:=Mataim +sp +InWords(TheNumber mod 100,Male);
  300..999: begin
            if (TheNumber mod 100) > 0 then
            begin
               if ((TheNumber mod 100) < 20) or ( (TheNumber mod 10) = 0) then
                  StrToReturn:='�';
               StrToReturn:=sp +StrToReturn +InWords(TheNumber mod 100,Male);
            end;
            Result:= InWords(TheNumber div 100,False) + sp + '����' + StrToReturn ;
            end;
  end;
end;


{Oren continue}
Function NumberToHebWords(TarNumber:real;WithShekel:boolean =True;Male:Boolean =True;CoinType:Integer = 0):string;
const
  Elef:String   = '���';
  Alpaim:String   = '������';
  Alafim:String   = '�����';
  Agorot:string = ' ������';
  Milion:string = '����� ';
  Sp:string     = ' ';
  CoinName:String = '�"�';
var
  TargetNumber:LongInt;
  AndAgorot:Boolean;
  OneShekel:Boolean;
  ResultStr:String;
  StrAgorot:String;
  HelpStr:string;
begin
  if CoinType = 2 Then {����}
  begin
      Agorot := '���';
      CoinName := '����';
  end
  Else
  begin
      Agorot := ' ������';
      CoinName := '�"�';
  end;

  TarNumber:=abs(TarNumber);
  if TarNumber > 1 then
     AndAgorot:=True
  else AndAgorot:=false;

  if Frac(TarNumber) > 0 then
  begin
    StrAgorot:=InWords( Round( Frac(TarNumber)*100 )) +Agorot;
  end;
  TargetNumber:=Trunc(TarNumber);
  if TargetNumber > 999 then
     OneShekel:=False
  else OneShekel:=True;

  if TargetNumber > 999999 then
  begin
    case (TargetNumber div 1000000) of
    1    :ResultStr:=Milion ;
    2    :begin
          if Male then
            ResultStr:='��� ' +Milion
          else ResultStr:='��� ' +Milion ;
          end;
    else ResultStr:=InWords(TargetNumber div 1000000,Male) +sp +Milion;
    end;
    TargetNumber:=(TargetNumber mod 1000000);
    if ( TargetNumber < 999) and ( TargetNumber > 0 ) then ResultStr:= ResultStr +'�';
  end;

  if TargetNumber > 999 then
  begin
    case (TargetNumber div 1000) of
      1       :ResultStr:=ResultStr +Elef;
      2       :ResultStr:=ResultStr +Alpaim;
      3,4,6..9:begin
                 HelpStr:=InWords(TargetNumber div 1000);
                 ResultStr:=ResultStr +copy(HelpStr,1, Length(HelpStr)-1)+'�'+sp+ Alafim
               end;
      5       :ResultStr:=ResultStr +'����'+sp+ Alafim;
      10      :ResultStr:=ResultStr +'����'+sp+ Alafim;
      else     ResultStr:=ResultStr +InWords(TargetNumber div 1000,Male) +sp+ Elef;
    end;
    TargetNumber:=TargetNumber mod 1000;
    ResultStr:=ResultStr +sp;
    if (TargetNumber < 20) or (TargetNumber mod 10 = 0 )then
       ResultStr:=ResultStr +'�' ;
  end;

  case TargetNumber of
  0   :;
  1   : begin
         if WithShekel and OneShekel then
            ResultStr:=ResultStr + '��� ���'
         else
           if Male then
                ResultStr:=ResultStr + '���'
           else ResultStr:=ResultStr + '���';{male}
        end;
  2   : begin
         if WithShekel then
            ResultStr:=ResultStr +'���'
         else
           if Male then
             ResultStr:=ResultStr +'�����'
           else ResultStr:=ResultStr + '�����';{male}
        end;
  else ResultStr:=ResultStr +InWords(TargetNumber,Male);{i took of sp}
  end; {case}

  if WithShekel then
  begin
    if (TargetNumber <> 1) or not OneShekel then ResultStr:=ResultStr +sp+ CoinName;
    if StrAgorot <> '' then
    begin
        if AndAgorot = True then ResultStr:=ResultStr +sp +'�';
        ResultStr:=ResultStr +StrAgorot
    end;
  end;{WithShekel}
  Result:=ResultStr;
end;
{end of oren functions}


function Mis_RishuiToInteger(Mis_Rishui: String): Integer;
begin
     Result := 0;
     while Pos('-', Mis_Rishui) > 0 do
           Delete(Mis_Rishui, Pos('-', Mis_Rishui),1);
     Result := StrToInt(Mis_Rishui);
end;


function IntegerToMis_Rishui(Mis_Rishui: Integer): String;
begin
     Result := IntToStr(Mis_Rishui);
     case Length(Result) of
          6: Insert('-', Result, 4);
          7: begin
                  Insert('-', Result, 6);
                  Insert('-', Result, 3);
             end;
     end;
end;


function IsWindows2000: Boolean;
var
   osv: TOSVERSIONINFO;
begin
     osv.dwOSVersionInfoSize := SizeOf(TOSVERSIONINFO);
     GetVersionEx(osv);
     Result := (osv.dwPlatformId = VER_PLATFORM_WIN32_NT) and
               (osv.dwMajorVersion = 5) and
               (osv.dwMinorVersion = 0); 
end;

procedure SetWin2000Display(AComponent: TComponent);
var
   i: Integer;
   sDataField: String;
begin
     if not IsWindows2000 then
        Exit;
     
     with AComponent do
     begin
          for i := 0 to ComponentCount-1 do
          begin
               if GetPropValue(Components[i], 'DataField') = NULL then
                  Continue; // The component is not DBAware
                  
               sDataField := GetPropValue(Components[i], 'DataField');
               
{               if (Pos('RISH',UpperCase(sDataField)) = 0) and
                  (Pos('PHONE',UpperCase(sDataField)) = 0) then
                  }
               if not CheckFlipField(sDataField) then
                  Continue; // The field is not a phone or Rishui field
                  
               if GetPropValue(Components[i], 'Hebrew') <> NULL then
                    SetPropValue(Components[i], 'Hebrew', False);
               
               if GetPropValue(Components[i], 'BiDiMode') <> NULL then
                    SetPropValue(Components[i], 'BiDiMode', bdLeftToRight);
          end;
     end;
end;

function ReverseString(sStr: String): String;
var
   i: Integer;
begin
     Result := '';
     for i := Length(sStr) downto 1 do
         Result := Result + sStr[i];
end;


Procedure ScanImage(var Image1 :TImage);
Var
   testdib: hbitmap;
Begin
//   TWAIN_RegisterCallback(CallbackFxn);
{}{}{Rani}
(*
   if TWAIN_SelectImageSource(0)=0 Then
      Exit;

   if TWAIN_AcquireNative(0, 0)=0 Then
      Exit;

   TestDib := TWAIN_GetDib(0);

{   Image1.Width := 1000;
   Image1.Height := 1000;
   Image1.Picture.Bitmap.Handle := TWAIN_GetDib(0);
   }

   CopyDibIntoImage(TestDib, Image1);

   TWAIN_FreeNative(TestDib);

   TestDib := 0;*)
End;


function CheckFlipField(sFieldName: String): Boolean;
var
   i: Integer;
begin
      Result := False;
      for i := 0 to High(Win2000FlipFields) do
          if Pos(UpperCase(Win2000FlipFields[i]), UpperCase(sFieldName)) > 0 then
          begin
               Result := True;
               Break;
          end;
end;


procedure DBGridWin2000ColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   nCurTextAlign: Cardinal;
{   R: TRect;
   s: String;
   i: Integer;}
begin
     if not IsWindows2000 then
        Exit;

     if not (Sender is TDBGrid) then
        Exit;

     if (Sender as TDBGrid).SelectedField = nil then
        Exit;

     if not CheckFlipField(UpperCase(Column.Field.FieldName)) then // The field is not a phone or Rishui field
        Exit;

     with (Sender as TDBGrid).Canvas do
     begin
          FillRect (Rect);
          nCurTextAlign := TextFlags;
          TextFlags := 0;
          TextRect(Rect, Rect.Left+Column.Width, Rect.Top,Column.Field.AsString);//(Sender as TDBGrid).SelectedField.AsString);
          TextFlags := nCurTextAlign;
     end;
end;

Function GetFileVersion(ProgName:String; Major, Minor, Release, Build:pWord):Boolean;
Var
  Info: PVSFixedFileInfo;
  InfoSize:UINT;
  nHWND, BuffSize:DWord;
  Buffer:Pointer;
Begin
  BuffSize:= GetFileVersionInfoSize(PChar(ProgName),nHWND);
  Result:=False;
  If BuffSize<>0 Then
  Begin
    GetMem(Buffer, BuffSize);
    Try
      If GetFileVersionInfo(PChar(ProgName),nHWND, BuffSize, Buffer) Then
        If VerQueryValue(Buffer,'\',Pointer(Info), InfoSize) Then
        Begin
          If Assigned(Major) Then Major^:=HiWord(Info^.dwFileVersionMS);
          If Assigned(Minor) Then Minor^:=LoWord(Info^.dwFileVersionMS);
          If Assigned(Release) Then Release^:=HiWord(Info^.dwFileVersionLS);
          If Assigned(Build) Then Build^:=LoWord(Info^.dwFileVersionLS);
          Result:=True;
        End;
    Finally
      FreeMem(Buffer, BuffSize);
    End; // Finally
  End;  // BuffSize<>0
End;

procedure GetLoginFromSqlSever (AliasName , DatabaseName , UserName ,PassWord, SreverType: String; ListOfHost : TstringList);
var
   TmpQuery : TQuery;
   TmpDataBase : Tdatabase;
begin
   if Not Assigned (ListOfHost) then
      ShowMessage ('����� �� �����')
   else
   begin
      TmpDataBase := Tdatabase.Create (nil);
      TmpDataBase.AliasName :=  AliasName;
      TmpDataBase.DatabaseName := 'TmpData';
      TmpDataBase.Params.Add ('USER NAME='+UserName);
      TmpDataBase.Params.Add ('PASSWORD='+Password);
      TmpDataBase.LoginPrompt := False;
      TmpDataBase.Connected := True;

      TmpQuery  := TQuery.Create(nil);
      TmpQuery.DatabaseName := TmpDataBase.DatabaseName;
      TmpQuery.Sql.Clear;
      if SreverType = 'SQLSERVER' Then
      begin
        TmpQuery.Sql.Add('use master select Dat.Name,Pro.net_address, Pro.LogiName,Pro.nt_username,Pro.nt_domain,Pro.HostName,Pro.program_name from sysprocesses Pro , sysDatabases Dat where Dat.dbid = Pro.dbid');
        TmpQuery.Open;
        while not TmpQuery.Eof do
        begin
            if(Not TmpQuery.FieldByName ('net_address').IsNull) And
              (CompareText (DatabaseName,TmpQuery.FieldByName ('Name').AsString) = 0 )And
              (ListOfHost.IndexOf (TmpQuery.FieldByName ('net_address').AsString) = -1) Then
               ListOfHost.Add (TmpQuery.FieldByName ('net_address').AsString);
            TmpQuery.Next;
        end;
      end
      else
      begin
        TmpQuery.Sql.Add('select sid, serial#, username, osuser, Machine from v$session where type=''USER'' And SchemaName = (select SchemaName from v$session where audsid = userenv(''sessionid''))');
        TmpQuery.Open;
        while not TmpQuery.Eof do
        begin
            if(Not TmpQuery.FieldByName ('Machine').IsNull) And
              (ListOfHost.IndexOf (TmpQuery.FieldByName ('Machine').AsString) = -1) Then
               ListOfHost.Add (TmpQuery.FieldByName ('Machine').AsString);
            TmpQuery.Next;
        end;
      end;
      ListOfHost.Sort;
      TmpDataBase.Connected := False;
      TmpDataBase.Free;
      TmpDataBase := nil;
      TmpQuery.Free;
      TmpQuery := nil;
   end;
end;

Function FindDistance(TempQry:TQuery; Yaad, Motza:String; ReplaceToSpace:Boolean=False):Integer;
Var
  Tick:LongInt;
  TempSt:String;
  TmpInt, CodeY, CodeM:Integer;
begin
  (** ����� ����� ������ - Distance **)
  Result:=0;
  If Motza=Yaad Then Exit;
  If (Motza='') or (Yaad='') Then Exit;

  If ReplaceToSpace Then
  Begin
     Motza:=StringReplace(Motza, '-',' ',[rfReplaceAll]);
     Yaad:=StringReplace(Yaad, '-',' ',[rfReplaceAll]);
  End;

  Motza:=StringReplace(Motza, '''','''''',[rfReplaceAll]);
  Yaad:=StringReplace(Yaad, '''','''''',[rfReplaceAll]);

  Tick:=GetTickCount;
  With TempQry Do
  Begin
     Close;
     With Sql Do
     Begin
       Clear;
       Add(' (Select CodeCity From City Where NameCity='''+Motza+''')');
     End;
     Open;
     CodeM:=FieldByName('CodeCity').AsInteger;

     Close;
     With Sql Do
     Begin
       Clear;
       Add(' (Select CodeCity From City Where NameCity='''+Yaad+''')');
     End;
     Open;
     CodeY:=FieldByName('CodeCity').AsInteger;

     If (CodeY=0) or (CodeM=0) Then
     Begin
     {
        Motza:=StringReplace(Motza, '-',' ',[rfReplaceAll]);
        With Sql Do
        Begin
          Clear;
          Add(' (Select CodeCity From City Where NameCity='''+Motza+''')');
        End;
        Open;
        CodeM:=FieldByName('CodeCity').AsInteger;
        If CodeM<>0 Then
          If CodeY=0 Then
          Begin

          end;   }


        ShowMessage('�� ����� ����.');
        Exit;
     end;

     If CodeM>CodeY Then
     Begin
       TmpInt:=CodeM;
       CodeM:=CodeY;
       CodeY:=TmpInt;
     End;

     Close;
     With Sql Do
     Begin
       Clear;
       Add('Select DistKM_Str from Distance Where');
       Add(' CodeFrom='+IntToStr(CodeM)+' And CodeTo='+IntToStr(CodeY));
     End;
     Open;

     If RecordCount=0 Then
     Begin
        ShowMessage('�� ���� ����.');
        Exit;
     end;

     TempSt:=DoEncryption(
                     FieldByName('DistKM_Str').AsString,
                     PasswordEncryptKey,
                     PasswordDecryptKey,
                     False);

     Val(TempSt,TmpInt,CodeM);
     If CodeM=0 Then Result:=TmpInt;
     Close;
     Sql.Clear;
  End;
End;

Function GetDBName(Alias:String):String;
Var
  I:Integer;
  StrLst:TStringList;
  St:String;
  TrySystemDsn:Boolean;
  Reg:TRegistry;
begin
  Result:='';
  StrLst:=TStringList.Create;
  Session.GetAliasParams(Alias,StrLst);
  I:=-1;
  Repeat
    inc(i);
  Until (i=StrLst.Count) or (Pos('DATABASE NAME=',UpperCase(StrLst[I]))>0);
  If I<>StrLst.Count Then
  Begin
     St:=StrLst[I];
   // �� ���
     If Length(St)=Length('DATABASE NAME=') Then
     Begin
       StrLst.Clear;
       Session.GetAliasParams(Alias,StrLst);
       I:=-1;
       Repeat
          inc(i);
       Until (i=StrLst.Count) or (Pos('ODBC DSN=',UpperCase(StrLst[I]))>0);

       If I<>StrLst.Count Then
       Begin
          St:=StrLst[I];
          If Length(St)=Length('ODBC DSN=') Then
          Begin
            Result:=('Alias: ODBC DSN found, but incorrect.'+#13+'��� �����: 001.');
            Exit;
          End;
          St:=Copy(St,Length('ODBC DSN=')+1, Length(St)-Length('ODBC DSN='));

          Try
            Reg := TRegistry.Create;
            Reg.RootKey := HKEY_CURRENT_USER;
            TrySystemDsn:=False;
            If Not Reg.OpenKey('\Software\ODBC\ODBC.INI\'+St,  False)
              Then TrySystemDsn:=True;

            If (Not TrySystemDsn) and (Not Reg.ValueExists('Database'))
              Then TrySystemDsn:=True;

            If TrySystemDsn Then
            Begin
              Reg.Free;
              Reg := TRegistry.Create;
              Reg.RootKey := HKEY_LOCAL_MACHINE;
              If Not Reg.OpenKey('\Software\ODBC\ODBC.INI\'+St,  False) Then
              Begin
                Result:=('ODBC: Registry entry for "'+st+'" not found.'+#13+'��� �����: 004.');
                Exit;
              End;
              If Not Reg.ValueExists('Database') Then
              Begin
                Result:=('ODBC: Registry key "Database" for "'+st+'" not found.'+#13+'��� �����: 005.');
                Exit;
              End;
            End;

            Result:=Reg.ReadString('Database');

          Finally
            Reg.Free;
          End;
       End
       Else
         Result:=('Alias: ODBC DSN not found.'+#13+'��� �����: 002.');
       Exit;
     End;

     St:=Copy(St,Length('DATABASE NAME=')+1, Length(St)-Length('DATABASE NAME='));
     Result:=St;
  End
  Else
    Result:=('Alias: DATABASE NAME not found.'+#13+'��� �����: 003.');
End;

Function GetServer(Alias:String):String;
Var
  I:Integer;
  StrLst:TStringList;
  St:String;
  Reg:TRegistry;
  TrySystemDsn:Boolean;
begin
  Result:='';
  StrLst:=TStringList.Create;
  Session.GetAliasParams(Alias,StrLst);
  I:=-1;
  Repeat
     inc(i);
  Until (i=StrLst.Count) or (Pos('ODBC DSN=',UpperCase(StrLst[I]))>0);
  If I<>StrLst.Count Then
  Begin
     St:=StrLst[I];
     If Length(St)=Length('ODBC DSN=') Then
     Begin
       Result:=('Alias: ODBC DSN found, but incorrect.'+#13+'��� �����: 201.');
       Exit;
     End;
     St:=Copy(St,Length('ODBC DSN=')+1, Length(St)-Length('ODBC DSN='));
     Try
       Reg := TRegistry.Create;
       Reg.RootKey := HKEY_CURRENT_USER;

       TrySystemDsn:=False;
       If Not Reg.OpenKey('\Software\ODBC\ODBC.INI\'+St,  False)
          Then TrySystemDsn:=True;

       If (Not TrySystemDsn) and (Not Reg.ValueExists('Database'))
          Then TrySystemDsn:=True;

       If TrySystemDsn Then
       Begin
         Reg.Free;
         Reg := TRegistry.Create;
         Reg.RootKey := HKEY_LOCAL_MACHINE;
         If Not Reg.OpenKey('\Software\ODBC\ODBC.INI\'+St,  False) Then
         Begin
           Result:=('ODBC: Registry entry for "'+st+'" not found.'+#13+'��� �����: 204.');
           Exit;
         End;
         If Not Reg.ValueExists('Server') Then
         Begin
           Result:=('ODBC: Registry key "Server" for "'+st+'" not found.'+#13+'��� �����: 205.');
           Exit;
         End;
       End;

       Result:=Reg.ReadString('Server');
     Finally
       Reg.Free;
     End;
  End
  Else
    Result:=('Alias: ODBC DSN not found.'+#13+'��� �����: 202.');
End;
Function F2SC(F:Real; DigNum:Integer=2):String;
Begin
  Result:=Format('%8.'+I2S(DigNum)+'n',[F]);
End;
Function F2S(F:Real; DigNum:Integer=2; DigNum2:Integer=8):String;
Begin
  Result:=Format('%'+I2S(DigNum2)+'.'+I2S(DigNum)+'f',[F]);
End;

Function S2F(S:String):Real;
Begin
  If S<>'' Then Result:=StrToFloat(Trim(S))
  Else Result:=0;
End;
Function F2I(F:Real):Integer;
Begin
  Result:=S2I(F2S(F, 0));
End;
Function I2F(I:Integer):Real;
Begin
  Result:=S2F(I2S(I));
End;

Function I2S(I:Integer):String;
Begin
  Result:=IntToStr(I);
End;
Function S2I(S:String):Integer;
Begin
  If S<>'' Then Result:=StrToInt(Trim(S))
  Else Result:=0;
End;
Function D2S(D:TDateTime):String;
Begin
  Result:=FormatDateTime('dd/mm/yyyy',D);
End;
Function S2D(S:String):TDateTime;
Begin
  If S<>'' Then Result:=StrToDate(S)
  Else Result:=0;
End;
Function T2S(T:TDateTime):String;
Begin
  Result:=FormatDateTime('hh:mm',T);
End;
Function S2T(S:String):TDateTime;
Begin
  If S<>'' Then Result:=StrToTime(S)
  Else Result:=0;
End;

Function DateFieldNull(Field:TField):Boolean;
Begin
  Result:=False;
  If Field=Nil Then
  Begin
    Result:=True;
    Exit;
  End;
  If (Field.IsNull) or (Field.AsString='') or
     (Pos('/',trim(Field.AsString))=1) or (Field.AsDateTime=0)
  Then Result:=True;
End;
Procedure ChangeDateFloatFormat(Tbl:TDataSet);
Var C:Integer;
Begin
// ����� ����� ����� �� �����
 With Tbl Do
   For C:=0 To FieldCount-1 Do
      Begin
// ���� ��� �� ���� ������
         If Fields[C] is TBCDField Then
         With TBCDField(Fields[C]) Do
         Begin
            DefaultExpression:='0';
            DisplayFormat:='0.00';
         End;
// ���� ��� �� �����-���: �� ���� ����� ��� ����
         If Fields[C] is TDateTimeField Then
         With TDateTimeField(Fields[C]) Do
            If (Pos('TIME',UpperCase(Fields[C].FieldName))<>0) And
               (Pos('SHIB',UpperCase(Fields[C].FieldName))<>0) Then
            Begin
               EditMask:='!90:00;1;_';
               DisplayFormat:='HH:MM';
            End;
   End;  // For Loop on FieldsCount
End;

Function Date2Iso(D:TDateTime):String;
Begin
  Result:=FormatDateTime('yyyymmdd',D);
End;

Procedure ShowProg1(ProgVal:String);
Var
  I:Integer;
Begin
  Try
    I:=StrToInt(ProgVal);
    SendMessage(FindWindow('TF_AtmProg','�������..'),CnstWM_MsgAddProg ,I,0);

  Except
  End;

End;
Procedure ShowProg2(ProgVal:String);
Var
  I:Integer;
Begin
  Try
    I:=StrToInt(ProgVal);
    SendMessage(FindWindow('TF_AtmProg','�����..'),CnstWM_MsgAddProg ,I,0);
  Except
  End;

End;
Procedure ShowProgCrt(CrtNum:Integer; ProgValOrMax:String=''; ProgTextOrMode:String=''; Pause:Boolean=False);
// Init: ShowProgCrt(fnFormNum - ���� ����, MaxProg - ��� ������� �� �������/�����,
//                                                                                 '1');
// Progress: ShowProgCrt(0, CurProg - ������� ������, ProgText - ���� ���� ������ ��������);
                            // CurProg='' - ���� �������� ������� �-1
// Done: ShowProgCrt(fnFormNum - ���� ����,
//                                          '', '0');

Var
  I:Integer;
  H:THandle;
  CaptionPChar:PChar;
Begin
  // ���� �������
  If (ProgTextOrMode<>'1') and (ProgTextOrMode<>'0') Then
  Try
    CaptionPChar:=PChar(ProgTextorMode);
    If ProgValOrMax='' Then
       I:=999
    Else
    If ProgValOrMax='-1' Then
       I:=-1
    Else
       I:=StrToInt(ProgValOrMax);
    SendMessage(FindWindow('TF_AtmProg','���� 2003 - �������'),CnstWM_MsgAddProg ,Integer(CaptionPChar), I);
    If Pause Then
    ShowMessage('Press <ENTER> to continue.');
   { Sleep(100); }
  Except
  End;

  // �����
  If ProgTextOrMode='1' Then
  Try
    H:=FindWindow('TF_AtmProg','WintudaProg');
    I:=StrToInt(ProgValOrMax)+1;
    SendMessage(H,CnstWM_MsgInit, CnstCrtStart,CrtNum);
    SendMessage(H,CnstWM_MsgInit, CnstCrtSetMax,I);
//    SendMessage(H,CnstWM_MsgInit, CnstCrtSetCaption,Integer(CaptionPChar));
//    SendMessage(H,nstWM_MsgShow ,CrtNum,I);
  Except
  End;

  // ������
  If ProgTextOrMode='0' Then
  Try
    SendMessage(FindWindow('TF_AtmProg','���� 2003 - �������'),CnstWM_MsgInit, CnstCrtEnd ,CrtNum);
  Except
  End;

End;

Function CalcDateTashlum(FromDate :TDateTime;PayCond :ShortString) : TDateTime;
Var
   Day,Month,Year :Word;
   Res            :TDateTime;
   Str1           :ShortString;
   I              :LongInt;
   FoundNum       :Boolean;
   AddValue       :LongInt;
Begin
  DecodeDate(FromDate, Year, Month, Day);
//------------------- ����� �����
  if Pos('����',PayCond)> 0 Then
     begin
       Day := 1;
       if Month < 12 then Inc(Month)
          else begin
               Inc(Year);
               Month := 1;
          end;
       FromDate := EncodeDate(Year, Month, Day);
       FromDate := FromDate -1;
     end;
//------------------- ����� �����
  I:=1;
  FoundNum:=True;
  Str1:='';
  While (FoundNum) And (I<=Length(PayCond)) Do
    Begin
      if (PayCond[I]>='0') And (PayCond[I]<='9') Then
         Str1:=Str1+PayCond[I]
      Else
         if Str1<>''Then
            FoundNum:=False;
      Inc(I);
    End;
//-------------------------
  if Str1 > '' then
     Result:= FromDate + StrToInt(Str1)
  else
     Result:= FromDate;
End;

Function GetTableList(Qry:TQuery; TblList:TStringList):Boolean;
Begin
Try
   With Qry Do
   Begin
     Close;
     With Qry.Sql Do
     Begin
        Clear;
        Add('select Cast(O.Name as Char(25)) as TblName from SysObjects O');
        Add('where ObjectProperty(O.ID, ''IsUserTable'') = 1');
     End;
     Open;
     First;
     If BOF And EOF Then Begin
       Result:=False;
       Exit;
     End;
     TblList.Clear;
     While Not (EOF) Do
     Begin
       TblList.Add(FieldByName('TblName').AsString);
     End;
     Result:=True;
   End;
Finally
  Qry.Close;
End;
End;


Function GetFields(Qry:TQuery; FldList:TStringList; Tbl:String):Boolean;
Begin
Try
   With Qry Do
   Begin
     Close;
     With Qry.Sql Do
     Begin
        Clear;
        Add('select Cast(L.Name as Char(32)) As FldName from SysObjects O, SysColumns L');
        Add('where ObjectProperty(O.ID, ''IsUserTable'') = 1');
        Add('and O.ID = L.ID and Upper(O.Name) like IsNull('''+UpperCase(Tbl)+''', O.Name)');
     End;
     Open;
     First;
     If BOF And EOF Then Begin
       Result:=False;
       Exit;
     End;
     FldList.Clear;
     While Not (EOF) Do
     Begin
       FldList.Add(FieldByName('FldName').AsString);
     End;
     Result:=True;
   End;
Finally
  Qry.Close;
End;
End;

Function TableExists(Qry:TObject; TblName:String):Boolean;
Var
  QryAdo:TADOQuery;
  QryBde:TQuery;
Begin
If Qry is TADOQuery Then
Try
   With TADOQuery(Qry) Do
   Begin
     Close;
     With TADOQuery(Qry).Sql Do
     Begin
        Clear;
        Add('select Count(*) as RecCnt from SysObjects O');
        Add('where ObjectProperty(O.ID, ''IsUserTable'') = 1');
        Add('      and Upper(O.Name) like IsNull('''+UpperCase(TblName)+''', O.Name)');
     End;

     Open;
     If BOF And EOF Then Begin
       Result:=False;
       Exit;
     End;
     If FieldByName('RecCnt').AsInteger>0 Then Result:=True
     Else Result:=False;
   End;
Finally
  TADOQuery(Qry).Close;
End
Else
Try
   With TQuery(Qry) Do
   Begin
     Close;
     With TQuery(Qry).Sql Do
     Begin
        Clear;
        Add('select Count(*) as RecCnt from SysObjects O');
        Add('where ObjectProperty(O.ID, ''IsUserTable'') = 1');
        Add('      and Upper(O.Name) like IsNull('''+UpperCase(TblName)+''', O.Name)');
     End;
     Open;
     If BOF And EOF Then Begin
       Result:=False;
       Exit;
     End;
     If FieldByName('RecCnt').AsInteger>0 Then Result:=True
     Else Result:=False;
   End;
Finally
  TQuery(Qry).Close;
End;

End;

Function FieldExists(Qry:TQuery; StrLst:TStringList; TblName:String; FieldName:String; FieldParams:String=''; AutoCreate:Boolean=False):Boolean;
Begin
Try
   If AutoCreate Then
   With Qry Do
   Begin
     Close;
     With Qry.Sql Do
     Begin
        Clear;
        Add('If Not Exists(');
        Add('select * from SysObjects O, SysColumns L');
        Add('where ObjectProperty(O.ID, ''IsUserTable'') = 1');
        Add('      and O.ID = L.ID');
        Add('      and Upper(L.Name) like '''+UpperCase(FieldName)+'''');
        Add('      and Upper(O.Name) like IsNull('''+UpperCase(TblNAme)+''', O.Name)');
        Add(')');
        Add('ALTER TABLE dbo.'+TblName+' ADD '+FieldName+' '+FieldParams);
      End;
      ExecSql;
   End
   Else
   With StrLst Do
   With Qry Do
   Begin
     Close;
     With Qry.Sql Do
     Begin
        Clear;
        Add('select Count(*) as RecCnt from SysObjects O, SysColumns L');
        Add('where ObjectProperty(O.ID, ''IsUserTable'') = 1');
        Add('      and O.ID = L.ID');
        Add('      and Upper(L.Name) like '''+UpperCase(FieldName)+'''');
        Add('      and Upper(O.Name) like IsNull('''+UpperCase(TblNAme)+''', O.Name)');
     End;
     Open;
     If BOF And EOF Then Begin
       Result:=False;
       Exit;
     End;
     If FieldByName('RecCnt').AsInteger>0 Then Result:=True
     Else Result:=False;
   End
Finally
  Qry.Close;
End;
End;

Function DoQryStr(Qry:TQuery; QryStr:String; UseExecSql:Boolean=False):Integer;
// run a query (=QryStr) + return how many records were found + leave the query open
Begin
  With Qry do
  Begin
     If Active Then Close;
     Sql.Clear;
     Sql.add(QryStr);
     If UseExecSql Then
     Begin
       ExecSql;
       Exit;
     End;
     Open;
     First;
     If ((EOF) and (BOF)) or (RecordCount=0) Then Result:=0
     Else Result:=RecordCount;
  End;
End;

Function DoSelect(Qry:TQuery; Fields, Tbl, WhereStr:String; Params:String=''):Integer;
// run a query and return "Fields" from "Table"  + return how many records were found + leave the query open
Begin
  If WhereStr='' Then Result:=DoQryStr(Qry,'select '+Fields+ ' from '+ Tbl + ' '+Params, False)
  Else Result:=DoQryStr(Qry,'select '+Fields+ ' from '+ Tbl+ ' Where '+WhereStr + ' '+Params, False);
End;

{procedure DBGridWin2000ColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   nCurTextAlign: Cardinal;
begin
     if not (Sender is TDBGrid) then
        Exit;

     if not IsWindows2000 then
        Exit;

     if (Sender as TDBGrid).SelectedField = nil then
        Exit;

     if (Pos('RISH',UpperCase(Column.FieldName)) = 0) and
        (Pos('PHONE',UpperCase(Column.FieldName)) = 0) then
        Exit; // The field is not a phone or Rishui field

     nCurTextAlign := GetTextAlign((Sender as TDBGrid).Canvas.Handle);
     SetTextAlign((Sender as TDBGrid).Canvas.Handle, nCurTextAlign - TA_RTLREADING);
     (Sender as TDBGrid).Canvas.TextRect(Rect, 1,1, (Sender as TDBGrid).SelectedField.AsString);
     SetTextAlign((Sender as TDBGrid).Canvas.Handle, nCurTextAlign);
end;
 }

  { *************** }
(*Uses TypInfo
procedure GetListOfProperties(Instance : TComponent);
var
  P : TComponent;
  X,Y: Integer;
  PropInfo: PPropInfo;
  MyPropList : PPropList;
  MyTPropList : TPropList;
begin
  MyPropList:=@MyTPropList;
  MainForm.MyMemo.Lines.Clear;
  for X := 0 to Instance.ComponentCount - 1 do
  begin
    P := Instance.Components[X];
    GetPropList(P.ClassInfo, [tkUnknown, tkInteger, tkChar, tkEnumeration, tkFloat,
    tkString, tkSet, tkClass, tkMethod, tkWChar, tkLString, tkWString,
    tkVariant, tkArray, tkRecord, tkInterface, tkInt64, tkDynArray],MyPropList);
    for Y:=0 to GetTypeData(P.ClassInfo)^.PropCount -1 do
      MainForm.MyMemo.Lines.Add(P.Name+'.'+MyPropList[Y].Name);
  end;

end;
*)


{
//Set Event or method
procedure TForm1.Button1Click(Sender: TObject);
Var
   I :LongInt;
   propInfo   :  PPropInfo;
   PPointer   : PTypeData;
   Method      : TMethod;

begin
     For I:=0 To ComponentCount-1 Do
     Begin
          PropInfo:=GetPropInfo(Components[i].ClassInfo,'OnMouseMove');
          PPointer := GetTypeData(PropInfo^.PropType^);      // Get the Type Data of the property (Parameters)
          Method.Code:=MethodAddress('GenMouseMove');
          Method.Data:=Self;
          if PropInfo<>Nil Then
             SetMethodProp(Components[i], PropInfo,Method);  // Link the Method with the Object
     End;
end;

}

(*Procedure GetCPUID;
Var
   DD :DWord;

Begin
Asm
   MOV EAX,01h;
   db 0fh;    //CPUID
   db 0a2h;   //CPUID
   MOV EAX,03h;
   db 0fh;
   db 0a2h;
   MOV DD,EAX;

   //Now DD Has The CPUID
end;
End;
*)

function GetInetFile
(const fileURL, FileName: String; ProgBar:TGauge; Var DoAbort:Boolean): boolean;
const BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array[1..BufferSize] of Byte;
  BufferLen: DWORD;
  f: File;
  sAppName: string;
begin
 Result:=False;
 sAppName := ExtractFileName(Application.ExeName);
 hSession := InternetOpen(PChar(sAppName),
                INTERNET_OPEN_TYPE_PRECONFIG,
               nil, nil, 0);
 try
  hURL := InternetOpenURL(hSession,
            PChar(fileURL),
            nil,0,0,0);
  try
   AssignFile(f, FileName);
   Rewrite(f,1);
   repeat
    InternetReadFile(hURL, @Buffer,
                     SizeOf(Buffer), BufferLen);
    BlockWrite(f, Buffer, BufferLen);
    ProgBar.Progress:=ProgBar.Progress+BufferSize;
    Application.ProcessMessages;
   until (BufferLen = 0) or (DoAbort);
   CloseFile(f);
   If DoAbort Then Result:=False Else Result:=True;
  finally
   InternetCloseHandle(hURL)
  end
 finally
  InternetCloseHandle(hSession)
 end
end;


Begin
    DateSeparator := '/';
    ShortDateFormat := 'DD/MM/YYYY';
    SysLocale.PriLangID:=LANG_HEBREW;
    HintWindowClass:=THebHintWindow;
end.

unit FrmAtmMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, RXCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    RxLabel1: TRxLabel;
    procedure RxLabel1MouseEnter(Sender: TObject);
    procedure RxLabel1MouseLeave(Sender: TObject);
    procedure RxLabel1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RxLabel1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.RxLabel1MouseEnter(Sender: TObject);
begin
     TRxLabel(Sender).ShadowSize:=2;
     TRxLabel(Sender).Font.Color:=clRed;
end;

procedure TForm1.RxLabel1MouseLeave(Sender: TObject);
begin
     TRxLabel(Sender).ShadowSize:=0;
     TRxLabel(Sender).Font.Color:=clBlack;
end;

procedure TForm1.RxLabel1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
     TRxLabel(Sender).ShadowSize:=0;
     TRxLabel(Sender).Font.Color:=clBlack;
end;

procedure TForm1.RxLabel1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
     TRxLabel(Sender).ShadowSize:=2;
     TRxLabel(Sender).Font.Color:=clRed;
end;

end.

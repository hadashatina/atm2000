unit DMKupa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, RxQuery, AtmRxQuery,IniFiles;

type
  TDM_Kupa = class(TDataModule)
    Database_Kupa: TDatabase;
    Qry_Temp: TQuery;
    AtmRxQuery_Kabala: TAtmRxQuery;
    DS_Kabala: TDataSource;
    UpdateSQL_Kabala: TUpdateSQL;
    AtmRxQuery_Checks: TAtmRxQuery;
    DS_Checks: TDataSource;
    UpdateSQL_Checks: TUpdateSQL;
    AtmRxQuery_ChecksTnua: TIntegerField;
    AtmRxQuery_ChecksKabalaYear: TIntegerField;
    AtmRxQuery_ChecksKabalaNumber: TIntegerField;
    AtmRxQuery_ChecksCodeSugTashlum: TIntegerField;
    AtmRxQuery_ChecksKodBank: TIntegerField;
    AtmRxQuery_ChecksSnifNumber: TIntegerField;
    AtmRxQuery_ChecksDatePiraon: TDateTimeField;
    AtmRxQuery_ChecksSchumCheck: TBCDField;
    AtmRxQuery_ChecksMisparCheshbon: TStringField;
    AtmRxQuery_ChecksKodMatbea: TIntegerField;
    AtmRxQuery_ChecksKamutShtarot: TIntegerField;
    AtmRxQuery_ChecksDateHamara: TDateTimeField;
    AtmRxQuery_ChecksSchumDollar: TBCDField;
    AtmRxQuery_ChecksShuraKopa: TIntegerField;
    AtmRxQuery_ChecksStatus: TIntegerField;
    AtmRxQuery_ChecksSugStar: TIntegerField;
    AtmRxQuery_ChecksSchumMatbea: TBCDField;
    AtmRxQuery_ChecksMisparHafkada: TIntegerField;
    AtmRxQuery_ChecksLookShemBank: TStringField;
    AtmRxQuery_KabalaYehusYear: TIntegerField;
    AtmRxQuery_KabalaKabalaNumber: TIntegerField;
    AtmRxQuery_KabalaKabalaKind: TIntegerField;
    AtmRxQuery_KabalaDateKabala: TDateTimeField;
    AtmRxQuery_KabalaCodeLakoach: TIntegerField;
    AtmRxQuery_KabalaShemLakoach: TStringField;
    AtmRxQuery_KabalaKtovet: TStringField;
    AtmRxQuery_KabalaHesbonChiuv: TStringField;
    AtmRxQuery_KabalaHesbonZikuy: TStringField;
    AtmRxQuery_KabalaHesbonMasMakor: TStringField;
    AtmRxQuery_KabalaKodBItzua: TIntegerField;
    AtmRxQuery_KabalaMisparHavara: TIntegerField;
    AtmRxQuery_KabalaTotalSumKabla: TBCDField;
    AtmRxQuery_KabalaPratim: TStringField;
    AtmRxQuery_KabalaHaavaraBankait: TIntegerField;
    AtmRxQuery_KabalaShuraKopa: TIntegerField;
    AtmRxQuery_KabalaCodeHaavaraToHan: TIntegerField;
    AtmRxQuery_KabalaLastUpdate: TDateTimeField;
    AtmRxQuery_KabalaMaklidName: TStringField;
    Qry_AtmIndexKabala: TQuery;
    UpdateSQL_AtmIndex: TUpdateSQL;
    DS_KabHes: TDataSource;
    UpdateSQL_KabHes: TUpdateSQL;
    RxQuery_MakavHesbon: TRxQuery;
    UpdateSQL_Makav: TUpdateSQL;
    AtmRxQuery_ChecksCodeBankHafkada: TIntegerField;
    AtmRxQuery_ChecksMisHeshbonHafkada: TStringField;
    AtmRxQuery_ChecksCodeSnifHafkada: TIntegerField;
    Qry_Mname: TQuery;
    AtmRxQuery_ChecksCheckNumber: TStringField;
    AtmRxQuery_ChecksNumberOfPayment: TIntegerField;
    AtmRxQuery_ChecksCreditKind: TIntegerField;
    AtmRxQuery_ChecksValidMonth: TIntegerField;
    AtmRxQuery_ChecksValidYear: TIntegerField;
    AtmRxQuery_ChecksAprovalCode: TIntegerField;
    AtmRxQuery_ChecksPhoneNumber: TStringField;
    AtmRxQuery_ChecksLookSugTashlum: TStringField;
    Query_InsertKupa: TQuery;
    UpdateSQL_InsertKupa: TUpdateSQL;
    UpdateSQL_MakavNhg: TUpdateSQL;
    AtmRxQuery_KabalaTotalVat: TBCDField;
    AtmRxQuery_KabalaTotalPlusVat: TBCDField;
    AtmRxQuery_KabalaMamPercent: TBCDField;
    AtmRxQuery_KabalaTotalZikuy: TBCDField;
    AtmRxQuery_KabHes: TAtmRxQuery;
    AtmRxQuery_KabHesTnua: TIntegerField;
    AtmRxQuery_KabHesKabalaNumber: TIntegerField;
    AtmRxQuery_KabHesKabalaYear: TIntegerField;
    AtmRxQuery_KabHesSchumHesbonit: TBCDField;
    AtmRxQuery_KabHesDateHesbonit: TDateTimeField;
    AtmRxQuery_KabHesKamutHesbonit: TBCDField;
    AtmRxQuery_KabHesPratimHesbonit: TStringField;
    AtmRxQuery_KabHesTotalSchum: TBCDField;
    AtmRxQuery_KabHesDateKabala: TDateTimeField;
    AtmRxQuery_KabHesCodeLakoach: TIntegerField;
    AtmRxQuery_KabHesShemLakoach: TStringField;
    AtmRxQuery_KabHesLakoachGroup: TIntegerField;
    AtmRxQuery_KabHesCodebitzua: TIntegerField;
    AtmRxQuery_KabHescodemaslul: TIntegerField;
    AtmRxQuery_KabHesFromNum: TStringField;
    AtmRxQuery_KabHesToNum: TStringField;
    AtmRxQuery_KabHesLakNehag: TIntegerField;
    AtmRxQuery_KabHesMaamPercent: TBCDField;
    AtmRxQuery_KabHesTotalMaam: TBCDField;
    AtmRxQuery_KabHesShipnum: TIntegerField;
    procedure Database_KupaLogin(Database: TDatabase;
      LoginParams: TStrings);
    procedure DataModuleDestroy(Sender: TObject);
    procedure AtmRxQuery_KabalaAfterScroll(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterOpen(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaCodeLakoachValidate(Sender: TField);
    procedure AtmRxQuery_ChecksBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterPost(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesAfterPost(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaBeforeInsert(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksBeforeEdit(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesBeforeEdit(DataSet: TDataSet);
    procedure AtmRxQuery_ChecksAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabalaBeforeDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure AtmRxQuery_KabalaDateKabalaValidate(Sender: TField);
    procedure Database_KupaAfterConnect(Sender: TObject);
    procedure AtmRxQuery_KabalaBeforeScroll(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesSchumHesbonitValidate(Sender: TField);
    procedure AtmRxQuery_KabHesBeforeDelete(DataSet: TDataSet);
    procedure AtmRxQuery_KabHescodemaslulValidate(Sender: TField);
    procedure AtmRxQuery_KabHesToNumChange(Sender: TField);
    procedure AtmRxQuery_KabHesKamutHesbonitChange(Sender: TField);
    procedure AtmRxQuery_ChecksCodeSugTashlumValidate(Sender: TField);
    procedure AtmRxQuery_ChecksKodBankValidate(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    NewKabalaSavedNumber:LongInt;
    ItraForLak :Real;
    KabHesWasInsert :Boolean;
    KaBhesWasDelete :Boolean;
    Procedure FillKabalaFrmLookupFields;
    Function  GetNextKabalaNumber(Increment :Integer):LongInt;
    Function  GetLastKabalaNumber:LongInt;
    Procedure UpdateTotalKabala;
    Function  CalcTotalForSgira:Real;
    Function  CalcMaamForSgira:Real;    
    Procedure CalcTotalHesbonit;
    procedure BuildLineInMakavForCurKabala;
    Procedure UpdateMakavForCloseHesbonit;
    Procedure BuildLinesInKopaForCurChecks;
    Procedure UpdateMlayPritim;
    Function  HesbonitInKabHes(KabalaNo,KabalaYear,ParitNo :LongInt):Boolean;
  end;

//Procedure InitHafkadaForms(AUserName,APassword,AAlias,KabalaType:PChar);
//Procedure DoneHafkadaForms;
  Procedure DoKabala(AUserName,APassword,AAlias,ScriptsDir,TypeKabala:PChar);

  Procedure InitAtmCrt(AliasName,DirForIni:ShortString);External 'AtmCrt.Dll';
  Procedure OpenCrt(FormNum :LongInt;MainValue :ShortString);External 'AtmCrt.Dll';
  Procedure DoneAtmCrt;External 'AtmCrt.Dll';

var
  DM_Kupa: TDM_Kupa;
  KupaAlias,KupaDBUserName,KupaDBPassword:String;
  CurKabalaYear         :LongInt;
  KabalaScriptsDir      :String;
  KupaDllIniFileName    :String;
  KupaDllScriptsDir     :String;
  Kabala_type,KodMatbea,
  Snif,LastNum,ShibAzmn,
  ShibAzmnTo,TopLines,Super,
  HesFromMlay,YearFromMlay,
  HesFromCrt,YearFromCrt: Integer;
  Hadpasa               : String[1];
  CopysKabala           : String[2];
  ShemMadpis            : Shortstring;
  Maam,
  TotalSagur            : Real;
  Atzmada,MaamPrit,SumEgul,
  KamutMin,KamutMlay,SumMakor,
  Schum                 : Real;
  YesMlay,YesPrit       : Boolean;
  Atmini                : Tinifile;
implementation

uses  AtmConst, AtmRutin, F_Kabala;

{$R *.DFM}


Procedure DoKabala(AUserName,APassword,AAlias,ScriptsDir,TypeKabala:PChar);
Var
  FreeDM:Boolean;
Begin
  Try
    Screen.Cursor:=crHourGlass;
    Try
      KupaAlias:=StrPas(AAlias);
      KupaDBUserName:=StrPas(AUserName);
      KupaDBPassword:=StrPas(APassword);
      KabalaScriptsDir:=StrPas(ScriptsDir);
      Kabala_type:=StrToint(StrPas(TypeKabala));
      FreeDM:=False;
      if DM_Kupa=Nil Then
      Begin
        DM_Kupa:=TDm_Kupa.Create(Nil);
        FreeDM:=True;
      End;
      if Frm_Kabala=Nil Then
        Frm_Kabala:=TFrm_Kabala.Create(Nil);

      Frm_Kabala.InitKabala;
      InitAtmCrt(KupaAlias,ExtractFilePath(Application.ExeName));
    Finally
      Screen.Cursor:=crDefault;
    End;
    Frm_Kabala.ShowModal;

    If HesFromCrt=0 Then DoneAtmCrt;
    if FreeDM Then
    Begin
      DM_Kupa.Free;
      DM_Kupa:=Nil;
    End;

    if Frm_Kabala<>Nil Then
    Begin
      Frm_Kabala.Free;
      Frm_Kabala:=Nil;
    End;
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
End;


procedure TDM_Kupa.AtmRxQuery_ChecksAfterOpen(DataSet: TDataSet);
begin
  FillKabalaFrmLookupFields;
end;

procedure TDM_Kupa.Database_KupaLogin(Database: TDatabase;
  LoginParams: TStrings);
begin
    LoginParams.Clear;
    LoginParams.Add('USER NAME='+KupaDBUserName);
    LoginParams.Add('PASSWORD='+KupaDBPassword);
end;

procedure TDM_Kupa.DataModuleDestroy(Sender: TObject);
Var
  I :LongInt;
begin
  For I:=0 To ComponentCount-1 Do
    if Components[i] is TDataSet Then
      TDataSet(Components[i]).Close;
end;


procedure TDM_Kupa.AtmRxQuery_KabalaAfterScroll(DataSet: TDataSet);
begin
  NewKabalaSavedNumber:=-1;
  AtmRxQuery_Kabala.FieldByName('CodeLakoach').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Kabala.FieldByName('MamPercent').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Kabala.FieldByName('TotalVat').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('SchumCheck').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('CodeSugTashlum').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('CheckNumber').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_Checks.FieldByName('DatePiraon').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('KamutHesbonit').ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;
  Frm_Kabala.RxDBGrid_KabHes.ReadOnly:=AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0;

  AtmRxQuery_Checks.Close;
  AtmRxQuery_Checks.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_Checks.Open;

  AtmRxQuery_KabHes.Close;
  AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_KabHes.Open;
  KabHesWasInsert:=False;

  Frm_Kabala.TotalBeforeMaam.Text:=FloattoStr(
      AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat-
      AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat);

end;

Procedure TDM_Kupa.FillKabalaFrmLookupFields;
Var
   SList:TStringList;
   I :LongInt;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select * From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_SugTashlum));
    Sql.Add('Or Sug_Tavla = '+IntToStr(SugTavla_Bankim));
    Open;
    SList:=TStringList.Create;
    SList.AddObject(IntToStr(SugTavla_SugTashlum),AtmRxQuery_ChecksLookSugTashlum);
    SList.AddObject(IntToStr(SugTavla_Bankim),AtmRxQuery_ChecksLookShemBank);
    FillKodTavlaLookupList(Qry_Temp,SList);
    SList.Free;
    Close;
  End;

End;

procedure TDM_Kupa.AtmRxQuery_KabalaCodeLakoachValidate(Sender: TField);

begin
    if Frm_Kabala.DBComboBox_HeshbonHiuv.Items.Count>0 Then
      Frm_Kabala.DBComboBox_HeshbonHiuv.Field.AsString:=Frm_Kabala.DBComboBox_HeshbonHiuv.Items[0];

    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      If Kabala_type=10 Then
      Begin
        Sql.Add('Select Shem_Nehag,Mis_Han From Nehag');
        Sql.Add('Where Kod_Nehag='+AtmRxQuery_KabalaCodeLakoach.AsString);
      End
      Else
      Begin
        Sql.Add('Select Shem_Lakoach,Mis_Han,Kod_Atzmada From Lakoach');
        Sql.Add('Where Kod_Lakoach='+AtmRxQuery_KabalaCodeLakoach.AsString);
      End;
      Open;
      if Not Eof And Bof Then
      Begin
        If Kabala_type=10 Then
        Begin
          AtmRxQuery_Kabala.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_Nehag').asstring;
//        AtmRxQuery_Kabala.fieldbyname('HesbonZikuy').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
        End
        Else
        Begin
          AtmRxQuery_Kabala.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_lakoach').asstring;
//        AtmRxQuery_Kabala.fieldbyname('HesbonZikuy').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
          Atzmada:=Qry_Temp.fieldbyname('Kod_Atzmada').AsInteger;
        End;
        Close;
      End
      Else
      Begin
         Close;
         ShowMessage('���� �� ����');
         abort;
      End;
      If (Kabala_type=0) And (Atzmada=2) Then
      Begin
        ShowMessage('���� ����� ������ ');
        Abort;
      End;
      If (Kabala_type=1) And (Atzmada<>2) Then
      Begin
        ShowMessage('���� ����� ������');
        Abort;
      End;
     If (Kabala_type=1) Or (Atzmada=2) Then
        AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=0
     Else
        AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=Maam;
    End;
End;

Procedure TDM_Kupa.AtmRxQuery_ChecksBeforePost(DataSet: TDataSet);
Begin
  With AtmRxQuery_Checks Do
  Begin
    If FieldByName('SchumCheck').AsFloat=0 Then //<=0 David 2-11-2000
    Begin
      Cancel;
      Abort;
    End;
    FieldByName('KabalaYear').AsInteger:=CurKabalaYear;
    FieldByName('KabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    If Kabala_type mod 10 =0 Then
       FieldByName('KodMatbea').AsInteger:=0
    Else
       FieldByName('KodMatbea').AsInteger:=2;  //����
  End;
end;

Function TDM_Kupa.GetNextKabalaNumber(Increment :Integer):LongInt;
Var
  CurYear   : Word;
  Res,Code  : LongInt;
  StrH,StrK : String;
Begin
  Result:=0;
  CurYear:=CurKabalaYear;
  With Qry_AtmIndexKabala Do
  Begin
//  If Snif<>-1 Then StrK:=aiHeshbonitMasKabala+IntToStr(Snif) // 'HsbMasKabala'
    If Super<>-1 Then StrK:=aiHeshbonitMasKabala+IntToStr(Super)
    ELse StrK:=aiHeshbonitMasKabala;

    ParamByName('PKey_String').AsString:=StrK;
    ParamByName('PYehusYear').AsInteger:=CurYear;
    Open;
    if Eof And Bof Then //��� ����
    Begin

    StrH:='1';
    Repeat
      if Not InputQuery('��� ����','������ ����� �� ��/���� '+IntTostr(CurYear),StrH) Then
        Exit;     Val(StrH,Res,Code);
    Until Code=0;

      Insert;
      FieldByName('Last_Number').AsInteger:=Res; // 1
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=StrK; //aiHeshbonitMasKabala;
      FieldByName('YehusYear').AsInteger:=CurYear;
      Post;
      Result:=1;
    End
    Else
    Begin//�����
      Res:=FieldByName('Last_Number').AsInteger+Increment;
      Edit;
      FieldByName('Last_Number').AsInteger:=Res;
      Post;
      Result:=Res;
    End;
    Qry_AtmIndexKabala.ApplyUpdates;
    Close;
  End;
End;

Function TDM_Kupa.GetLastKabalaNumber:LongInt;
Var
  CurYear :Word;
Begin
  Result:=0;
  With Qry_AtmIndexKabala Do
  Begin
    ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
    Open;
    Result:=FieldByName('Last_Number').AsInteger;
    Close;
  End;
End;

Procedure TDM_Kupa.AtmRxQuery_KabalaBeforePost(DataSet: TDataSet);
begin
    if (AtmRxQuery_Kabala.State=dsInsert)
//    And (not AtmRxQuery_KabalaCodeLakoach.IsNull)
    Then
  Begin
    AtmRxQuery_KabalaDateKabala.AsDateTime:=Trunc(AtmRxQuery_KabalaDateKabala.AsDateTime);
    AtmRxQuery_KabalaKabalaNumber.AsInteger:=GetNextKabalaNumber(+1);
    AtmRxQuery_KabalaYehusYear.AsInteger:=CurKabalaYear;
    AtmRxQuery_KabalaMaklidName.Asstring:=IntToStr(Snif);    
    NewKabalaSavedNumber:=AtmRxQuery_KabalaKabalaNumber.AsInteger;
  End;
end;

Procedure TDM_Kupa.AtmRxQuery_KabHesBeforePost(DataSet: TDataSet);
Var
  TotalSagur :Real;
  Hefresh :Real;
  ItratLakoat :Real;
  Kamut       :Real;

Begin

  With AtmRxQuery_KabHes Do
  Begin
    if AtmRxQuery_KabHes.FieldByName('Codemaslul').IsNull Then
    Begin
      Cancel;
      Abort;
    End;
    If Not AtmRxQuery_Kabala.FieldByName('KabalaNumber').IsNull Then
    Begin
      KabHesWasInsert:=(State=dsInsert);
      FieldByName('KabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
      FieldByName('KabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
      FieldByName('shemlakoach').asstring:=
        AtmRxQuery_Kabala.fieldbyname('shemlakoach').asstring;
      FieldByName('CodeLakoach').AsInteger:=
        AtmRxQuery_Kabala.fieldbyname('CodeLakoach').AsInteger;
      FieldByName('DateKabala').AsDateTime:=
         AtmRxQuery_KabalaDateKabala.AsDateTime;

      If MaamPrit=0 Then
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('MaamPercent').AsFloat:=Maam
      Else
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('MaamPercent').AsFloat:=0;
      Kamut:=
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat*
        (1+DM_Kupa.AtmRxQuery_KabHes.FieldByName('MaamPercent').AsFloat/100);
//    Kamut:=Round((Kamut+0.005)*100)/100 -
      Kamut:=Int((Kamut+0.005)*100)/100 -
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat;
      If SumMakor<>0 Then
         Kamut:=SumMakor*AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency
        -DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat;
{
Showmessage(FloatTostr(Kamut)+' <kamut Sum>'+
DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsString+'<>'+
FloatTostr((1+DM_Kupa.AtmRxQuery_KabHes.FieldByName('MaamPercent').AsFloat/100)));
      Kamut:=
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat*
        (1+DM_Kupa.AtmRxQuery_KabHes.FieldByName('MaamPercent').AsFloat/100)-
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat;

Showmessage(FloatTostr(Kamut));
}     If HesFromMlay=0 Then // ����� ������ �� ����
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalMaam').AsFloat:=Kamut;
      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('LakoachGroup').Asinteger:=Snif;
    End;
  End;

  if AtmRxQuery_KabHes.State=dsEdit Then
  Begin
//    TotalSagur:=TotalSagur+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_ChecksAfterPost(DataSet: TDataSet);
begin
  if TQuery(DataSet).FieldByName('Tnua').AsInteger=0 Then
  Begin
    TQuery(DataSet).Close;
    if TQuery(DataSet).Params.FindParam('PKabalaNumber')<>Nil Then
      TQuery(DataSet).ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    if TQuery(DataSet).Params.FindParam('PKabalaYear')<>Nil Then
      TQuery(DataSet).ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
    TQuery(DataSet).Open;
    TQuery(DataSet).Last;
  End;
  UpdateTotalKabala;
end;

Procedure TDM_Kupa.UpdateTotalKabala;
Begin
  With Qry_Temp Do
  Begin
    //�� ������
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(SchumCheck) SC From Checks');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('AND KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Open;

    AtmRxQuery_Kabala.Edit;
    AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat:=FieldByName('SC').AsFloat;
    Close;
    AtmRxQuery_Kabala.Post;

{    //�� ��� ������ ����

    Sql.Add('AND CodeSugTashlum <= '+IntToStr(stHavaraBank));
    Open;
    AtmRxQuery_Kabala.FieldByName('TotalMoneyOnly').AsFloat:=FieldByName('SC').AsFloat;
    AtmRxQuery_Kabala.Post;
    Close;
}
  End;
End;


Function TDM_Kupa.CalcTotalForSgira:Real;
Begin
  Screen.Cursor:=crSQLWait;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(TotalSchum) ST From KabHesMs');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Open;
    Result:=FieldByName('ST').AsFloat;
    Close;
  End;
  Screen.Cursor:=crDefault;
End;

Function TDM_Kupa.CalcMaamForSgira:Real;
Begin
  Screen.Cursor:=crSQLWait;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(TotalMaam) ST From KabHesMs');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Open;
    Result:=FieldByName('ST').AsFloat;
    Close;
  End;
  Screen.Cursor:=crDefault;
End;

procedure TDM_Kupa.AtmRxQuery_KabalaAfterInsert(DataSet: TDataSet);
begin
  ItraForLak:=0;
  FindFirstControlInTabOrder(Frm_Kabala.Panel_Kabala).SetFocus;
  NewKabalaSavedNumber:=-1;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesAfterPost(DataSet: TDataSet);
Var
  StrH :String;
  Sum  :Real;

begin
  if (AtmRxQuery_KabHes.FieldByName('Tnua').AsInteger=0) And
     (Not KaBhesWasDelete)
  Then
  Begin
    AtmRxQuery_KabHes.Close;
    AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
    AtmRxQuery_KabHes.Open;
    KabHesWasInsert:=False;
    AtmRxQuery_KabHes.Last;
  End;

  DM_Kupa.KaBhesWasDelete:=False;
  AtmRxQuery_Kabala.Edit;
  CalcTotalHesbonit;
  AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat:=
       AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat;
//     TotalSagur; //CalcTotalForSgira;
    //����� �����
  AtmRxQuery_Kabala.Post;
end;

Procedure TDM_Kupa.CalcTotalHesbonit;

Begin
  TotalSagur:=CalcTotalForSgira;
  If (Kabala_type=1) Or (Atzmada=2) Then
  Begin
     AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat:=0;
     AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=0;
  End
  Else
  Begin
     AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=Maam;
//   If (AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat <> 0) Or
//      (AtmRxQuery_Kabala.FieldByName('TotalVat').IsNull)
//   Then
//      AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat:=(Round((TotalSagur*
//         Maam/100)*100)/100);
     AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat:=CalcMaamForSgira;
  End;
  AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat:=TotalSagur+
      AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat;
  Frm_Kabala.TotalBeforeMaam.Text:=FloattoStr(
      AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat-
      AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat);
End;

procedure TDM_Kupa.BuildLineInMakavForCurKabala;
Var
  LakGroup         :LongInt;
  TotalMasBamakor,
  TotalLoLchiuv    : Real;
  Month,Year,Day   : Word;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type=10 Then
    Begin
      Sql.Add('Select Kod_Kvutza From Nehag');
      Sql.Add('Where Kod_Nehag = '+AtmRxQuery_KabalaCodeLakoach.AsString);
    End
    Else
    Begin
      Sql.Add('Select Kod_Kvutza From Lakoach');
      Sql.Add('Where Kod_Lakoach = '+AtmRxQuery_KabalaCodeLakoach.AsString);
    End;
    Open;
    LakGroup:=FieldByName('Kod_Kvutza').AsInteger;
    Close;
  End;

  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(SchumCheck) SC From CHECKS Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add('And CodeSugTashlum = '+IntToStr(stMasBamakor));
    Open;
    TotalMasBamakor:=FieldByName('SC').AsFloat;
    Close;

    Sql.Clear;   // ���� ���� �����> 200 �� ������ ����"�
    Sql.Add('Select Sum(SchumCheck) SC From CHECKS Where KabalaNumber = '+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add('And CodeSugTashlum > 200');
    Open;
    TotalLoLchiuv:=FieldByName('SC').AsFloat;
    Close;
  End;

  AtmRxQuery_KabHes.First;
  AtmRxQuery_Checks.First;

  DecodeDate(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,
             Year,Month,Day);

  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type=10 Then
      Sql.Add('Insert into MakavNhg')
    Else
      Sql.Add('Insert into Makav');
//  Sql.Add('(HesbonitNumber,HesbonitKind,YehusYear,CodeLakoach,LakoachGroup,HesbonitDate,TotalHesbonitWithMAM,Status,Total_Zikuy,Date_Zikuy,DateHafakatHeshbonit,HanForLak,HanForMam,String_1,PratimToHan,TotalMasMakor,Last_Update,CodeHaavaraToHan,Mis_Kabala1,OrderNum,DateForFirstPayment)');
    Sql.Add('(');
    Sql.Add('HesbonitNumber,HesbonitKind,YehusYear,CodeLakoach,LakoachGroup,HesbonitDate,TotalHesbonitWithMAM,Status,Total_Zikuy,Date_Zikuy,DateHafakatHeshbonit,');
    Sql.Add('HanForLak,HanForMam,String_1,PratimToHan,TotalMasMakor,Last_Update,CodeHaavaraToHan,Mis_Kabala1,OrderNum,DateForFirstPayment,Bcd_1,Mamhesbonit,Mampercent,');
    Sql.Add('TotalSumFromTnua,TotalSumPriceKamut,YehusMonth,Kod_Sapak)');

    Sql.Add('Values');
    Sql.Add('('+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
    If Kabala_type mod 10=0 Then
      Sql.Add(','+IntToStr(hkKabalaMas))
    Else
      Sql.Add(','+IntToStr(hkKabalaMasD));
    Sql.Add(','+AtmRxQuery_Kabala.FieldByName('YehusYear').AsString);
    Sql.Add(','+AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
    Sql.Add(','+IntToStr(LakGroup));//����� ����
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','+AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsString);
//  if AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat Then
    Sql.Add(',1'); //Status
    Sql.Add(','+AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsString);
//  Sql.Add(',-'+ FloatToStr(Abs(AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency)) );
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('HesbonZikuy').AsString+'''');
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString+'''');
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('HesbonMasMakor').AsString+''''); //String_1 in makav
//  Sql.Add(','''+Copy(AddQuatesToString(AtmRxQuery_Kabala.FieldByName('Pratim').AsString),1,30)+'''');//PratimToHan
    Sql.Add(','''+Copy(AddQuatesToString(AtmRxQuery_Kabala.FieldByName('Pratim').AsString),1,30)+'''');//PratimHesbonit
    Sql.Add(','+FloatToStr(TotalMasBamakor));
    Sql.Add(','''+DateToSqlStr(Now,'mm/dd/yyyy')+'''');
    Sql.Add(',0');

//  Sql.Add(','''+AtmRxQuery_KabHes.FieldByName('HesbonitNumber').AsString+'''');
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString+'''');
    Sql.Add(','''+AtmRxQuery_Checks.FieldByName('CheckNumber').AsString+'''') ;
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','+FloatToStr(TotalLoLchiuv));
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('TotalVat').AsString+'''');
    Sql.Add(','''+AtmRxQuery_Kabala.FieldByName('MamPercent').AsString+'''');
    Sql.Add(','''+Frm_Kabala.TotalBeforeMaam.Text+'''');
    Sql.Add(','''+Frm_Kabala.TotalBeforeMaam.Text+'''');
    Sql.Add(','+FloatToStr(Month));
    If Snif<>-1 Then
       Sql.Add(','+IntToStr(Snif))
    Else
       Sql.Add(',0');
    Sql.Add(')');
//  Sql.SaveToFile('c:\BuildMakav1.Sql');
    ExecSQL;
  End;
End;

Procedure TDM_Kupa.UpdateMlayPritim;
Begin
   AtmRxQuery_KabHes.First;
   While Not AtmRxQuery_KabHes.EOF Do
   Begin
     With Qry_Temp Do
     Begin
       If (AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency<>0)
          And (Not AtmRxQuery_KabHes.FieldByName('Tonum').IsNull) Then
       Begin
         Close;
         Sql.Clear;
         Sql.Add('Update Maslul Set Day1=Day1 -'+AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsString);
         Sql.Add(',Day6='+AtmRxQuery_KabHes.FieldByName('Tonum').AsString);
         Sql.Add(',Date_end_Maslul='''+DateToSqlStr(AtmRxQuery_KabHes.FieldByName('DateHesbonit').AsDateTime,'mm/dd/yyyy')+'''');
         Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodemaslul.AsString);
         Sql.Add('And Sug_Rehev1=1');
         Sql.Add('And code_pizul=' + IntToStr(Snif) );
         Execsql;
       End;
     End; // Qry_Temp
     AtmRxQuery_KabHes.Next;
   End; // While Not AtmRxQuery_KabHes.EOF

End; // UpdateMlayPritim;

procedure TDM_Kupa.UpdateMakavForCloseHesbonit;
Var
Ind,mis        : integer;
HStr           : shortstring;

Begin //����� �������� ������ �����
Exit;
{  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.LoadFromFile(KabalaScriptsDir+'UpdateMakavFromKabHes.Sql');
    ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_KabalaKabalaNumber.AsInteger;
    ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_KabalaYehusYear.AsInteger;
    ExecSQL;
  End;
  }

  AtmRxQuery_KabHes.Close; //�����
  AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_KabHes.Open;
  KabHesWasInsert:=False;

  AtmRxQuery_KabHes.First;
  While Not AtmRxQuery_KabHes.EOF Do
  Begin
    if AtmRxQuery_KabHes.FieldByName('CodeBitzua').AsInteger=0 Then
      With RxQuery_MakavHesbon Do
      Begin
        Close;
        ParamByName('PHesbonitNumber').AsInteger:=AtmRxQuery_KabHes.FieldByName('HesbonitNumber').AsInteger;
        ParamByName('PYehusYear').AsInteger:=AtmRxQuery_KabHes.FieldByName('HesbonitYear').AsInteger;
        ParamByName('PHesbonitKind').AsInteger:=AtmRxQuery_KabHes.FieldByName('HesbonitKind').AsInteger;
        IF Kabala_Type=10 Then  // ����� ���� �����
        Begin
          with RxQuery_MakavHesbon do
          begin
            FOR IND:=0 To (sql.COUNT)-1 Do
              Begin
              HStr:= SQL.Strings[Ind];
              Mis:= pos('Makav',HStr);
              if Mis <> 0 then
              begin
//              move('MakavNhg',HStr[Mis],SizeOf('MakavNhg'));
                SQL.Strings[Ind]:='SELECT * FROM MakavNhg';
              end;
            End;
          end;
       End; // Kabala_Type=10
        Open;
        Edit;
        FieldByName('Total_Zikuy').AsFloat:=FieldByName('Total_Zikuy').AsFloat+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat;
        FieldByName('Date_Zikuy').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        if Abs(FieldByName('Total_Zikuy').AsFloat-FieldByName('TotalHesbonitWithMAM').AsFloat)<0.01 Then
          FieldByName('Status').AsInteger:=1;
        If FieldByName('Mis_Kabala1').IsNull Then
        Begin
          FieldByName('Mis_Kabala1').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala1').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala2').IsNull Then
        Begin
          FieldByName('Mis_Kabala2').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala2').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala3').IsNull Then
        Begin
          FieldByName('Mis_Kabala3').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala3').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala4').IsNull Then
        Begin
          FieldByName('Mis_Kabala4').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala4').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End
        Else
        If FieldByName('Mis_Kabala5').IsNull Then
        Begin
          FieldByName('Mis_Kabala5').AsInteger:=AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
          FieldByName('Date_kabala5').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        End;
        AtmRxQuery_Checks.First; // ����� ����� �� ���� ������
        If (Not AtmRxQuery_Checks.Eof) And (Not AtmRxQuery_Checks.FieldByName('DatePiraon').IsNull)
           Then  FieldByName('DateForFirstPayment').AsDateTime:=Trunc(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime);

//        AtmRxQuery_KabHes.RealSQL.SaveToFile('c:\UpdateMakavForCloseHesbonit2.Sql');
        Post;
        If Kabala_Type=10 Then UpdateObject:=UpdateSQL_MakavNhg;
        ApplyUpdates; // David 1-1-1
        Close;

        AtmRxQuery_KabHes.BeforePost:=Nil;
        AtmRxQuery_KabHes.AfterPost:=Nil;
        AtmRxQuery_KabHes.AutoApplyUpdates:=False;
        Try
          AtmRxQuery_KabHes.Edit;
          AtmRxQuery_KabHes.FieldByName('CodeBitzua').AsInteger:=1;
          AtmRxQuery_KabHes.SavePressed:=True;
          AtmRxQuery_KabHes.Post;
          AtmRxQuery_KabHes.ApplyUpdates;
        Finally
          AtmRxQuery_KabHes.BeforePost:=AtmRxQuery_KabHesBeforePost;
          AtmRxQuery_KabHes.AfterPost:=AtmRxQuery_KabHesAfterPost;
          AtmRxQuery_KabHes.AutoApplyUpdates:=True;
        End;
      End; //if
    AtmRxQuery_KabHes.Next;
  End; //While
End;

Procedure TDM_Kupa.BuildLinesInKopaForCurChecks;
Var
  LineNo :LongInt;
  TotalMzomanInKabala :Real;
  LineExist : Boolean;
  Ipyun     : Integer;
Begin
  AtmRxQuery_Checks.Close;
  AtmRxQuery_Checks.Open;
  AtmRxQuery_Checks.First;
  TotalMzomanInKabala:=0;
  AtmRxQuery_Checks.AutoApplyUpdates:=False;
  AtmRxQuery_Checks.AfterPost:=Nil;
  LineNo := -1;
  while not(AtmRxQuery_Checks.EOF) do
  Begin
//    ShowMessage('CheckNum='+AtmRxQuery_Checks.FieldByName('CheckNumber').AsString);
    If AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger < stHavaraBank Then
    With Qry_Temp Do
    Begin
//      if AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger <> stMezoman Then
      Begin//�� �����
        Query_InsertKupa.Insert;
        Query_InsertKupa.FieldByName('Misparkabala').AsString:=AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString;
        Query_InsertKupa.FieldByName('TarichKabala').AsDateTime:=Trunc(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime);
        Query_InsertKupa.FieldByName('CodeSugTashlum').AsString:=AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString;
        Query_InsertKupa.FieldByName('CodeLakoach').AsString:=AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString;
        Query_InsertKupa.FieldByName('MisparCheck').AsString:=AtmRxQuery_Checks.FieldByName('CheckNumber').AsString;
        Query_InsertKupa.FieldByName('KodBank').AsString:=AtmRxQuery_Checks.FieldByName('KodBank').AsString;
        Query_InsertKupa.FieldByName('Snif').AsString:=AtmRxQuery_Checks.FieldByName('SnifNumber').AsString;
        Query_InsertKupa.FieldByName('TarichPeraon').AsDateTime:=Trunc(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime);
        Query_InsertKupa.FieldByName('Schom').AsString:=AtmRxQuery_Checks.FieldByName('SchumCheck').AsString;
        Query_InsertKupa.FieldByName('MisparChesbon').AsString:=AtmRxQuery_Checks.FieldByName('MisparCheshbon').AsString;
        If Kabala_type mod 10=0 Then
          Query_InsertKupa.FieldByName('KodMatbea').AsInteger:=0
        Else
          Query_InsertKupa.FieldByName('KodMatbea').AsInteger:=2;//����
        Query_InsertKupa.FieldByName('KodBitzua').AsInteger:=0;
        Query_InsertKupa.FieldByName('MisHaavaraToHan').AsInteger:=0;
        If Snif<>-1 Then Ipyun:=Snif ELse Ipyun:=0;
        Query_InsertKupa.FieldByName('Ipyun').AsInteger:=Ipyun; // 0;
        Query_InsertKupa.FieldByName('CodeHaavaraToHan').AsInteger:=0;
        Query_InsertKupa.FieldByName('MisparHafkada').AsInteger:=0;
        Query_InsertKupa.FieldByName('YehusYear').AsInteger:=CurKabalaYear;
        Query_InsertKupa.Post;
(*        Close;
        Sql.Clear;
        Sql.Add('Insert into Kopa');
        Sql.Add('(Misparkabala,TarichKabala,CodeSugTashlum,CodeLakoach,MisparCheck,KodBank,Snif,TarichPeraon,Schom,MisparChesbon,KodMatbea,KodBitzua,MisHaavaraToHan,Ipyun,CodeHaavaraToHan,MisparHafkada,YehusYear)');
        Sql.Add('Values');
        Sql.Add('('+AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
        Sql.Add(','''+DateToSqlStr(AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString);
        Sql.Add(','+AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
        Sql.Add(','''+AtmRxQuery_Checks.FieldByName('CheckNumber').AsString+'''');
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('KodBank').AsString);
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('SnifNumber').AsString);
        Sql.Add(','''+DateToSqlStr(AtmRxQuery_Checks.FieldByName('DatePiraon').AsDateTime,'mm/dd/yyyy')+'''');
        Sql.Add(','+AtmRxQuery_Checks.FieldByName('SchumCheck').AsString);
        Sql.Add(','''+AtmRxQuery_Checks.FieldByName('MisparCheshbon').AsString+'''');
        Sql.Add(',0');
        Sql.Add(',0');
        Sql.Add(',0');//MisHaavaraToHan
        Sql.Add(',0');
        Sql.Add(',0');//CodeHaavaraToHan
        Sql.Add(',0');//MisparHafkada
        Sql.Add(','+IntToStr(CurKabalaYear)+')');//��� ����
//        Sql.SaveToFile('c:\BuildLinesInKopaForCurChecks3.Sql');
        ExecSql;
*)
      End;

      //����� �������
      if AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger = stMezoman Then
        TotalMzomanInKabala := TotalMzomanInKabala + AtmRxQuery_Checks.FieldByName('SchumCheck').AsFloat;

      //����� ���� ����� �����
      if LineNo=-1 Then
      Begin
        Sql.Clear;
        Sql.Add('Select Max(MisparShura) MS From Kopa Where MisparShura > -1');
        Open;
        LineNo:=FieldByName('MS').AsInteger;
        Close;
      End
      Else
        Inc(LineNo);

{      Sql.Clear;
      Sql.Add('Update Checks Set ShuraKopa = '+LineNo);
      Sql.Add('Where Checks.Tnua = '+AtmRxQuery_Checks.FieldByName('Tnua').AsString);
      ExecSQL;}

      AtmRxQuery_Checks.Edit;
      AtmRxQuery_Checks.FieldByName('ShuraKopa').AsInteger:=LineNo;

      AtmRxQuery_Checks.SavePressed:=True;
      AtmRxQuery_Checks.Post;

      AtmRxQuery_Checks.Next;
    End //Whith
    Else
      AtmRxQuery_Checks.Next;
  End;//While
  AtmRxQuery_Checks.Close;
  AtmRxQuery_Checks.AutoApplyUpdates:=True;
  AtmRxQuery_Checks.AfterPost:=AtmRxQuery_ChecksAfterPost;

  // ����� ����� �������
//  ShowMessage('����� ����� �������');
  if TotalMzomanInKabala > 0 Then
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type mod 10=0 Then  //  �����
    Begin
      Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanIn));
      Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
    End
    Else                   // �������
    Begin
      Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanInDolar));
      Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
    End;
//    ShowMessage('���� ���� ������ ��������');
    Open; //���� ���� ������ ��������
    LineExist:=Not(EOF And BOF);
    if LineExist Then
      TotalMzomanInKabala:=TotalMzomanInKabala+FieldByName('Schom').AsFloat;
//    ShowMessage('Close ���� ���� ������ ��������');
    Close;

    Sql.Clear;
    if LineExist Then
    Begin
      Sql.Add('Update KOPA Set Schom = '+FloatToStr(TotalMzomanInKabala));
      If Kabala_type Mod 10=0 Then  //  �����
      Begin
        Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanIn));
        Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
      End
      Else                   // �������
      Begin
        Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanInDolar));
        Sql.Add('And YehusYear= '+IntToStr(CurKabalaYear)) ;
      End;
    End
    Else
    Begin
      Sql.Add('Insert Into KOPA (MisparKabala,Schom,MisparCheck,YehusYear)');
      If Kabala_type mod 10=0 Then  //  �����
        Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanIn)     +','+FloatToStr(TotalMzomanInKabala)+','+'''������ �������'''+','+IntToStr(CurKabalaYear)+')')
//      Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanIn)+','+FloatToStr(TotalMzomanInKabala)+',''������ �������'')')
      Else                   // �������
        Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanInDolar)+','+FloatToStr(TotalMzomanInKabala)+','+'''������ ������� �����'''+','+IntToStr(CurKabalaYear)+')');
    End;
//    ShowMessage('ExecSql ����� ������ �������');
//    Sql.SaveToFile('c:\kabala.sql');
    ExecSQL; //����� ������ �������
  End;
//  ShowMessage('Close');
  AtmRxQuery_Checks.Open;
End;


procedure TDM_Kupa.AtmRxQuery_KabalaBeforeInsert(DataSet: TDataSet);
begin
  AtmRxQuery_Kabala.FieldByName('CodeLakoach').ReadOnly:=False;
  AtmRxQuery_Kabala.FieldByName('MamPercent').ReadOnly:=False;
  AtmRxQuery_Kabala.FieldByName('TotalVat').ReadOnly:=False;
//AtmRxQuery_Kabala.Edit;
//AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=Maam;
  AtmRxQuery_Checks.FieldByName('SchumCheck').ReadOnly:=False;
  AtmRxQuery_Checks.FieldByName('CodeSugTashlum').ReadOnly:=False;
  AtmRxQuery_Checks.FieldByName('CheckNumber').ReadOnly:=False;
  AtmRxQuery_Checks.FieldByName('DatePiraon').ReadOnly:=False;
  AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ReadOnly:=False;
  AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
  AtmRxQuery_KabHes.FieldByName('KamutHesbonit').ReadOnly:=False;

end;

procedure TDM_Kupa.AtmRxQuery_ChecksBeforeEdit(DataSet: TDataSet);
begin
  if AtmRxQuery_Checks.FieldByName('Tnua').AsInteger = 0 Then
  Begin
    if MessageDlg('���� ����� �� ����� ���� �������'+
               '��� ����� ���',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
    Begin
      AtmRxQuery_Checks.Close;
      AtmRxQuery_Checks.Open;
      Abort;
    End
    Else
      Abort;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesBeforeEdit(DataSet: TDataSet);
begin
  if AtmRxQuery_KabHes.FieldByName('Tnua').AsInteger=0 Then
    if MessageDlg('���� ����� �� ����� ���� �������'+
             '��� ����� ���',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
    Begin
      AtmRxQuery_KabHes.Close;
      AtmRxQuery_KabHes.Open;
      KabHesWasInsert:=False;
      Abort;
    End
    Else
      Abort;
end;

procedure TDM_Kupa.AtmRxQuery_ChecksAfterInsert(DataSet: TDataSet);
begin
  If AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then Exit;
  Frm_Kabala.RxDBGrid_Checks.Col:=1;
  AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger:=0;//If in TField The Error in SQL Server
end;

procedure TDM_Kupa.AtmRxQuery_KabalaBeforeDelete(DataSet: TDataSet);
begin
  if AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then
  Begin
    ShowMessage('��� ������ ����� �� ��/���� �����');
    Abort;
  End
  Else
  if MessageDlg('��� ����� �� ��/���� ��',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
  Begin
    AtmRxQuery_Checks.First;
    While Not ((AtmRxQuery_Checks.Eof) And (AtmRxQuery_Checks.BOF)) Do
      AtmRxQuery_Checks.Delete;
    AtmRxQuery_KabHes.First;
    While Not ((AtmRxQuery_KabHes.Eof) And (AtmRxQuery_KabHes.BOF)) Do
      AtmRxQuery_KabHes.Delete;
    if AtmRxQuery_Kabala.State=dsBrowse Then
      AtmRxQuery_Kabala.Edit;
    AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger := 9;
    if AtmRxQuery_KabalaKabalaNumber.AsInteger<>GetLastKabalaNumber Then
      Abort
    Else
      GetNextKabalaNumber(-1);
    NewKabalaSavedNumber:=-1;
  End;

end;

Procedure TDM_Kupa.DataModuleCreate(Sender: TObject);
Var
  F :TIniFile;
  Strh :String;
begin
  if KupaDllIniFileName<>'' Then
  Begin
    F:=TIniFile.Create(KupaDllIniFileName);
    Try
      Maam:=F.ReadFloat('Main','Mam',0);
      Super:=F.ReadInteger('Main','Super',-1);      
      Snif:=F.ReadInteger('Main','Snif',-1);
      Snif:=-1; // David
      If Snif=-1 Then YesMlay:=False Else YesMlay:=True;
      StrH:=F.ReadString(SectionForMain,KeyAliasForMname,'');
      if StrH<>'' Then
        Qry_Mname.DatabaseName:=StrH;
    Finally
      F.Free;
    End;
  End;
  if Trim(KupaAlias)<>'' Then
  Begin
    Database_Kupa.Connected:=False;
    Database_Kupa.AliasName:=KupaAlias;
  End;
  Qry_Mname.Open;
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  ShemMadpis:=Atmini.readString('Hafkada','Hatima','');
  CopysKabala:=Atmini.readString('Hafkada','CopysOfKabala','2');
  TopLines:=Atmini.readInteger('Hafkada','TopLines',10);
  Atmini.WriteString('Hafkada','CopysOfKabala',CopysKabala);
  Atmini.free;

end;

procedure TDM_Kupa.AtmRxQuery_KabalaDateKabalaValidate(Sender: TField);
Var
  DD,MM,YY :Word;
begin
  DecodeDate(Sender.AsDateTime,yy,mm,dd);
  if YY<>CurKabalaYear Then
    ShowMessage('����� �� ��/���� �� ���� �� ��� ������');
end;

procedure TDM_Kupa.Database_KupaAfterConnect(Sender: TObject);
Var
  StrH :String;
  F:TIniFile;
begin
  F:=TIniFile.Create(KupaDllIniFileName);
  StrH:=F.ReadString(SectionForMain,SQLServerType,'Paradox');
  F.Free;

  if CompareText(StrH,'Oracle')=0 Then
    Database_Kupa.Execute(OracleDateFormatSql);

  if CompareText(StrH,'Sybase')=0 Then
  Begin
    Database_Kupa.StartTransaction;
    Database_Kupa.Execute(SybaseDateFormatSql);
    Database_Kupa.Commit;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_KabalaBeforeScroll(DataSet: TDataSet);
begin
  If (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
     (AtmRxQuery_KabalaTotalSumKabla.AsFloat=0) And
     (AtmRxQuery_Kabala.RecordCount<>0) Then
    Case MessageDlg('�� ��/���� ����� ��� ������ ��� ����� ����',mtConfirmation,[mbYes,mbNo],0) Of
      mrYes:AtmRxQuery_Kabala.Delete;
      mrNo: Abort;
    End; //Case

  If (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
     (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) And
     (AtmRxQuery_KabalaTotalSumKabla.AsFloat<>0) Then
  Begin
    Showmessage(' �-� ��/���� �� ������ ');
    Abort;
  End;


end;

procedure TDM_Kupa.AtmRxQuery_KabHesAfterInsert(DataSet: TDataSet);
begin
    Frm_Kabala.RxDBGrid_KabHes.Col:=1;
    AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
    AtmRxQuery_KabHes.FieldByName('DateHesbonit').AsDateTime:=
      AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime;
    AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=True;      
end;

procedure TDM_Kupa.AtmRxQuery_KabHesSchumHesbonitValidate(Sender: TField);
Var
Kamut,KamutLeMlay   :  Real;
Begin
     If YesMlay Then
       If AtmRxQuery_KabHesSchumHesbonit.AsCurrency < 0 Then
       Begin
         Showmessage('!! ���� ����� ����');
         Abort;
       End;
     If SumMakor=0 then
        SumEgul:=AtmRxQuery_KabHesSchumHesbonit.AsCurrency;
//   IF SumEgul<>AtmRxQuery_KabHesSchumHesbonit.AsCurrency Then
//      SumEgul:=AtmRxQuery_KabHesSchumHesbonit.AsCurrency;

     Frm_Kabala.TotalSchum;
{
     If Sender=AtmRxQuery_KabHes.FieldByName('KamutHesbonit') Then
     Begin
       If (AtmRxQuery_Kabhes.FieldByName('ToNum').IsNull) And
         (Not DM_Kupa.AtmRxQuery_Kabhes.FieldByName('FromNum').IsNull)
       Then
         AtmRxQuery_Kabhes.FieldByName('ToNum').AsString:=IntToStr(
         AtmRxQuery_Kabhes.FieldByName('FromNum').Asinteger+
         AtmRxQuery_Kabhes.FieldByName('KamutHesbonit').asinteger-1);

         If AtmRxQuery_KabalaKabalaKind.AsInteger =1 Then
           Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency*-1
         Else
           Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency;
         KamutLeMlay:=KamutMlay+Kamut;
         If (KamutMin<>0) And (KamutMin>KamutLeMlay) Then
           ShowMessage('!! ����� ����� ���� ����� ������� ');
     End; //KamutHesbonit
}
end;


procedure TDM_Kupa.AtmRxQuery_KabHesBeforeDelete(DataSet: TDataSet);
begin
    KaBhesWasDelete:=True;
end;


procedure TDM_Kupa.AtmRxQuery_KabHescodemaslulValidate(Sender: TField);
Var
Mispar    :   Integer;
begin
    With Qry_Temp Do
    Begin
      AtmRxQuery_KabHes.FieldByName('FromNum').ReadOnly:=False;
      AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=False;
      AtmRxQuery_KabHes.FieldByName('Codemaslul').ReadOnly:=False;
      AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
      If YesMlay Then
      Begin
        if HesbonitInKabHes(AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger,
               AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger,
               AtmRxQuery_KabHes.FieldByName('Codemaslul').AsInteger)
        Then
        Begin
          Showmessage('����� ��"� ���� !!');
          Abort;
          Exit;
        End;
      End;

      SumEgul:=0;
      SumMakor:=0;
      MaamPrit:=0;
      KamutMin:=0;
      KamutMlay:=9999999;
      LastNum:=9999999;
      YesPrit:=False;

      Close;
      Sql.Clear;
      Sql.Add('Select Name,Sug_rehev1,PricePerMusa,DAY5,DAY6,Sug_Maslul,Day1,Day3 From Maslul');
      Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodeMaslul.AsString);
      If YesMlay Then
        Sql.Add('And code_pizul=' + IntToStr(Snif) );
      Open;
      if Not Eof And Bof Then
      Begin
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:=
          Qry_Temp.fieldbyname('Name').asstring;
//      If Qry_Temp.fieldbyname('Sug_rehev1').AsInteger=1 Then
        If YesMlay Then
        Begin
          YesPrit:=True;
          LastNum:=Qry_Temp.fieldbyname('DAY5').AsInteger;
          Mispar:=Qry_Temp.fieldbyname('DAY6').AsInteger;

          If Qry_Temp.fieldbyname('Sug_Maslul').AsInteger=1 Then
             MaamPrit:=1;
          KamutMin:=Qry_Temp.fieldbyname('DAY3').AsInteger;
          KamutMlay:=Qry_Temp.fieldbyname('DAY1').AsInteger;
          Schum:=Qry_Temp.fieldbyname('PricePerMusa').AsCurrency;
          SumMakor:=Schum;
          If MaamPrit=0 Then
            Begin
              SumEgul:=Schum/(1+Maam/100);
              Schum:=Round(SumEgul*100)/100;
//            Schum:=Round(Schum/(1+Maam/100) *100)/100 ;
            End;
          DM_Kupa.AtmRxQuery_Kabhes.FieldByName('SchumHesbonit').AsCurrency:=
             Schum;
          DM_Kupa.AtmRxQuery_Kabhes.FieldByName('FromNum').Asstring:=
             IntToStr(Mispar+1);
          AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=True;
          AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=True;
          AtmRxQuery_KabHes.FieldByName('FromNum').ReadOnly:=True;
        End
        Else
          DM_Kupa.AtmRxQuery_Kabhes.FieldByName('SchumHesbonit').AsCurrency:=0;
      End
      Else
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('SchumHesbonit').AsCurrency:=0;
    End;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesToNumChange(Sender: TField);
begin
   If (AtmRxQuery_Kabhes.FieldByName('ToNum').AsString <>'') And
      (AtmRxQuery_KabHes.FieldByName('FromNum').Asstring <>'')
   Then
   AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat:=
      AtmRxQuery_Kabhes.FieldByName('ToNum').AsFloat-
      AtmRxQuery_KabHes.FieldByName('FromNum').AsFloat+1;

end;

procedure TDM_Kupa.AtmRxQuery_KabHesKamutHesbonitChange(Sender: TField);
Var
Kamut,KamutLeMlay   :  Real;
begin
     Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency;
     If (Not YesMlay) Or (Not YesPrit) Then // ����� ��� ����� ����
     Begin                                  // �� ���� �� ���� �����
        Frm_Kabala.TotalSchum;
        Exit;
     End;
     if (Kamut<1) Or (Kamut<>Int(Kamut)) Then
     Begin
       Showmessage('���� �� �����');
       Abort;
     End;

     IF Kamut > KamutMlay Then
     Begin
       Showmessage('��� ����� �� ����� ������');
       Abort;
     End;

     AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=False;
     Try
       AtmRxQuery_Kabhes.FieldByName('ToNum').AsString:=IntToStr(
       AtmRxQuery_Kabhes.FieldByName('FromNum').Asinteger+
       AtmRxQuery_Kabhes.FieldByName('KamutHesbonit').asinteger-1);
       AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=True;
     Except On Exception Do;
     End;
     If AtmRxQuery_Kabhes.FieldByName('ToNum').AsInteger > LastNum Then
     Begin
         Showmessage(IntToStr(LastNum)+' << �� ���� ���� ���� �');
         Abort;
     End;
      KamutLeMlay:=KamutMlay+Kamut;
     If (KamutMin<>0) And (KamutMin>KamutLeMlay)
     Then
         ShowMessage('!!������ ����� ����� ���� ����� ������� ');

     Frm_Kabala.TotalSchum;
end;

Function TDM_Kupa.HesbonitInKabHes(KabalaNo,KabalaYear,ParitNo :LongInt):Boolean;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Count(*) C From KabhesMs');
    Sql.Add('Where KabalaNumber = '+IntToStr(KabalaNo));
    Sql.Add('And KabalaYear = '+IntToStr(KabalaYear));
    Sql.Add('And CodeMaslul = '+IntToStr(ParitNo));
    Open;
    Result:=FieldByName('C').AsInteger > 0;
    Close;
  End;
End;

procedure TDM_Kupa.AtmRxQuery_ChecksCodeSugTashlumValidate(Sender: TField);
begin
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_SugTashlum));
    Sql.Add('And Kod_Tavla = ' +AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString);
    Open;
    if Not Eof And Bof Then Exit;
    Showmessage('��� ����� ����');
    Sender.AsString:='0';
    Abort;
  End;

end;

procedure TDM_Kupa.AtmRxQuery_ChecksKodBankValidate(Sender: TField);
begin
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_Bankim));
    Sql.Add('And Kod_Tavla = ' +AtmRxQuery_Checks.FieldByName('KodBank').AsString);
    Open;
    if Not Eof And Bof Then Exit;
    Showmessage('��� ��� ����');
    Sender.AsString:='0';
    Abort;
  End;

end;

end.

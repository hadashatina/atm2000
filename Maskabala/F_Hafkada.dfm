object Frm_Hafkada: TFrm_Hafkada
  Left = 27
  Top = 30
  Width = 613
  Height = 402
  BiDiMode = bdRightToLeft
  Caption = '������'
  Color = clBtnFace
  Font.Charset = HEBREW_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RxDBGrid_Checks: TRxDBGrid
    Left = 0
    Top = 51
    Width = 605
    Height = 158
    Align = alClient
    DataSource = DS_ChecksForHafkada
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = HEBREW_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = RxDBGrid_ChecksCellClick
    MultiSelect = True
    OnGetCellParams = RxDBGrid_ChecksGetCellParams
    Columns = <
      item
        Expanded = False
        FieldName = 'KabalaNumber'
        Title.Caption = '��'#39' ����'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LookSugTashlum'
        Title.Caption = '��� �����'
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DatePiraon'
        Title.Caption = '����� �����'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SchumCheck'
        Title.Caption = '����'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CheckNumber'
        Title.Caption = '����'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KodBank'
        Title.Caption = '��� ���'
        Width = 49
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SnifNumber'
        Title.Caption = '����'
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MisparCheshbon'
        Title.Caption = '��'#39' �����'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LookShemBank'
        Title.Caption = '�� ���'
        Width = 98
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 605
    Height = 51
    Align = alTop
    TabOrder = 1
    object Label2: TLabel
      Left = 377
      Top = 10
      Width = 33
      Height = 13
      Caption = '�����'
    end
    object Label6: TLabel
      Left = 209
      Top = 37
      Width = 187
      Height = 13
      Caption = '��� �� �������� ������� ������'
      Color = clInfoBk
      ParentColor = False
    end
    object DateEdit_ChecksDate: TDateEdit
      Left = 252
      Top = 6
      Width = 121
      Height = 21
      CheckOnExit = True
      DefaultToday = True
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      NumGlyphs = 2
      StartOfWeek = Sun
      Weekends = [Sat]
      YearDigits = dyFour
      TabOrder = 0
      OnAcceptDate = DateEdit_ChecksDateAcceptDate
      OnExit = DateEdit_ChecksDateExit
    end
    object Btn_StartChecksSql: TBitBtn
      Left = 156
      Top = 4
      Width = 95
      Height = 25
      Caption = '��� ������'
      TabOrder = 1
      OnClick = Btn_StartChecksSqlClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADAADA000000000000DDAD7FFFFFFFFFF0AADA7FF000000FF0DDAD7FFFFFFFF
        FF0AA007FF000000FF0DDB00FFFFFFFFFF0AA7B000000000FF0DDA7FB0000000
        FF0A0000FB00FFFFFF0D7FBBBBB00FF7000AA7FB00FFFFF7F0ADDA7FB00FFFF7
        0ADAAD7FBB007777ADADDAD7FBB00ADADADAADAD7FBB00ADADAD}
    end
    object RG_HafkadaKind: TRadioGroup
      Left = 432
      Top = 1
      Width = 172
      Height = 49
      Align = alRight
      Caption = '��� �����'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        '�����'
        '������')
      TabOrder = 2
      OnClick = RG_HafkadaKindClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 209
    Width = 605
    Height = 30
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 334
      Top = 8
      Width = 122
      Height = 13
      Caption = '����� ������� ������'
    end
    object Label5: TLabel
      Left = 521
      Top = 8
      Width = 77
      Height = 13
      Caption = '������� �����'
    end
    object Lbl_TotalCashInKopa: TLabel
      Left = 512
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label8: TLabel
      Left = 86
      Top = 8
      Width = 136
      Height = 13
      Caption = '��"� ���� ����� ������'
    end
    object Lbl_TotalChecksHafkada: TLabel
      Left = 74
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Color = clInfoBk
      ParentColor = False
    end
    object CurrencyEdit_Cash: TCurrencyEdit
      Left = 258
      Top = 6
      Width = 72
      Height = 21
      AutoSize = False
      CheckOnExit = True
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      TabOrder = 0
      OnChange = CurrencyEdit_CashChange
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 356
    Width = 605
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Panel3: TPanel
    Left = 0
    Top = 322
    Width = 605
    Height = 34
    Align = alBottom
    TabOrder = 4
    object Btn_DoHafkadot: TBitBtn
      Left = 152
      Top = 5
      Width = 75
      Height = 25
      Caption = '����'
      Enabled = False
      TabOrder = 0
      OnClick = Btn_DoHafkadotClick
      Glyph.Data = {
        F2010000424DF201000000000000760000002800000024000000130000000100
        0400000000007C01000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333334433333
        3333333333388F3333333333000033334224333333333333338338F333333333
        0000333422224333333333333833338F33333333000033422222243333333333
        83333338F3333333000034222A22224333333338F33F33338F33333300003222
        A2A2224333333338F383F3338F33333300003A2A222A222433333338F8333F33
        38F33333000034A22222A22243333338833333F3338F333300004222A2222A22
        2433338F338F333F3338F3330000222A3A2224A22243338F3838F338F3338F33
        0000A2A333A2224A2224338F83338F338F3338F300003A33333A2224A2224338
        333338F338F3338F000033333333A2224A2243333333338F338F338F00003333
        33333A2224A2233333333338F338F83300003333333333A2224A333333333333
        8F338F33000033333333333A222433333333333338F338F30000333333333333
        A224333333333333338F38F300003333333333333A223333333333333338F8F3
        000033333333333333A3333333333333333383330000}
      NumGlyphs = 2
    end
    object Btn_SelectAll: TBitBtn
      Left = 230
      Top = 5
      Width = 75
      Height = 25
      Caption = '��� ���'
      TabOrder = 1
      OnClick = Btn_SelectAllClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Btn_Close: TBitBtn
      Left = 308
      Top = 5
      Width = 75
      Height = 25
      Cancel = True
      Caption = '����'
      TabOrder = 2
      OnClick = Btn_CloseClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object Btn_ChageYehusYear: TBitBtn
      Left = 470
      Top = 5
      Width = 75
      Height = 25
      Hint = '����� ��� ����� �������'
      Caption = '��� �����'
      TabOrder = 3
      OnClick = Btn_ChageYehusYearClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000CE0E0000C40E00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00222222222222
        22222222200000000000222220F7F7F7F7F0000000700000007007F7F0F7F7F7
        F7F00F00007F711F7F7007F7F0F7F117F7F00F81107F7F117F700711F0F7F7F1
        17F00F81107F11111F700711F0F7F7F7F7F00F8110444444444007F7F0444444
        4440044440000000000004444444440222220000000000022222}
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 239
    Width = 605
    Height = 42
    Align = alBottom
    Caption = '���� ��� ������'
    TabOrder = 5
    object Label3: TLabel
      Left = 505
      Top = 16
      Width = 41
      Height = 13
      Caption = '��� ���'
    end
    object Label4: TLabel
      Left = 332
      Top = 16
      Width = 24
      Height = 13
      Caption = '����'
    end
    object Lbl_HShemBank: TLabel
      Left = 437
      Top = 16
      Width = 19
      Height = 13
      Caption = '���'
      Color = clInfoBk
      ParentColor = False
    end
    object Lbl_HShemSnif: TLabel
      Left = 251
      Top = 16
      Width = 32
      Height = 13
      Caption = 'Label5'
      Color = clInfoBk
      ParentColor = False
    end
    object Label7: TLabel
      Left = 122
      Top = 18
      Width = 52
      Height = 13
      Caption = '��'#39' �����'
    end
    object Spb_SelectHBank: TSpeedButton
      Left = 6
      Top = 12
      Width = 23
      Height = 22
      Hint = '��� ����� ������'
      Glyph.Data = {
        36020000424D3602000000000000360000002800000010000000100000000100
        10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
        00000000000000000000000000000000000000000000000000000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1F7C00001F7CEF3D00000000FF7F0000
        0000000000000000FF7FFF7F0000FF7F000000000000EF3D00000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000FF7F0000
        00000000000000000000FF7F00000000000000000000000000000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000FF7F0000
        000000000000FF7FFF7FFF7F0000FF7F000000000000EF3D00000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1F7C00001F7CEF3D00000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F0000000000000000
        00000000000000000000000000000000000000000000000000000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F0000000000001F7C0000000000000000FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F000000001F7C1F7C1F7C00000000000000000000
        0000000000000000000000000000000000000000000000000000}
      OnClick = Spb_SelectHBankClick
    end
    object Edit_HKodBank: TEdit
      Left = 458
      Top = 14
      Width = 44
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object Edit_HKodSnif: TEdit
      Left = 286
      Top = 14
      Width = 44
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 1
    end
    object Edit_HHeshbonNo: TEdit
      Left = 34
      Top = 14
      Width = 86
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 2
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 281
    Width = 605
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object Label9: TLabel
      Left = 233
      Top = 14
      Width = 82
      Height = 13
      Caption = '������ �������'
    end
    object Label10: TLabel
      Left = 80
      Top = 16
      Width = 59
      Height = 13
      Caption = '��'#39' ������'
    end
    object CB_PrintHafkada: TdxfCheckBox
      Left = 494
      Top = 2
      Width = 88
      Height = 18
      Checked = True
      Alignment = taLeftJustify
      GroupIndex = 0
      Caption = '���� �����'
      TabOrder = 0
    end
    object RG_PrintTo: TRadioGroup
      Left = 339
      Top = -1
      Width = 145
      Height = 40
      Caption = '���� �����'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        '�����'
        '���')
      TabOrder = 1
    end
    object SpinEdit1: TSpinEdit
      Left = 154
      Top = 10
      Width = 57
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 0
    end
    object SpinEdit2: TSpinEdit
      Left = 13
      Top = 10
      Width = 57
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 0
    end
  end
  object Qry_ChecksForHafkada: TRxQuery
    BeforeOpen = Qry_ChecksForHafkadaBeforeOpen
    AfterOpen = Qry_ChecksForHafkadaAfterOpen
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'SELECT Checks.*,Kabala.DateKabala,Kabala.CodeLakoach'
      'FROM Checks,Kabala'
      'WHERE   (Checks.KabalaNumber = Kabala.KabalaNumber)  '
      '  AND  Kabala.KodBItzua BETWEEN 1 AND 2  '
      '  And Checks.MisparHafkada = 0'
      '  And Checks.CodeSugTashlum <> 1'
      '  And Checks.CodeSugTashlum < 100'
      '  And  %DateCondition'
      '  And YehusYear = :PYehusYear'
      '  And KabalaYear =:PYehusYear'
      '  And Checks.KodMatbea=:PMatbea'
      '  ORDER BY Checks.DatePiraon')
    UpdateMode = upWhereChanged
    Macros = <
      item
        DataType = ftString
        Name = 'DateCondition'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 72
    Top = 74
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PMatbea'
        ParamType = ptUnknown
      end>
    object Qry_ChecksForHafkadaTnua: TIntegerField
      FieldName = 'Tnua'
      Origin = 'DB_KUPA."Checks.DB".Tnua'
    end
    object Qry_ChecksForHafkadaKabalaYear: TIntegerField
      FieldName = 'KabalaYear'
      Origin = 'DB_KUPA."Checks.DB".KabalaYear'
    end
    object Qry_ChecksForHafkadaKabalaNumber: TIntegerField
      FieldName = 'KabalaNumber'
      Origin = 'DB_KUPA."Checks.DB".KabalaNumber'
    end
    object Qry_ChecksForHafkadaCodeSugTashlum: TIntegerField
      FieldName = 'CodeSugTashlum'
      Origin = 'DB_KUPA."Checks.DB".CodeSugTashlum'
    end
    object Qry_ChecksForHafkadaCheckNumber: TStringField
      FieldName = 'CheckNumber'
      Origin = 'DB_KUPA."Checks.DB".CheckNumber'
    end
    object Qry_ChecksForHafkadaKodBank: TIntegerField
      FieldName = 'KodBank'
      Origin = 'DB_KUPA."Checks.DB".KodBank'
    end
    object Qry_ChecksForHafkadaSnifNumber: TIntegerField
      FieldName = 'SnifNumber'
      Origin = 'DB_KUPA."Checks.DB".SnifNumber'
    end
    object Qry_ChecksForHafkadaDatePiraon: TDateTimeField
      FieldName = 'DatePiraon'
      Origin = 'DB_KUPA."Checks.DB".DatePiraon'
    end
    object Qry_ChecksForHafkadaSchumCheck: TBCDField
      FieldName = 'SchumCheck'
      Origin = 'DB_KUPA."Checks.DB".SchumCheck'
      Precision = 32
      Size = 3
    end
    object Qry_ChecksForHafkadaMisparCheshbon: TStringField
      FieldName = 'MisparCheshbon'
      Origin = 'DB_KUPA."Checks.DB".MisparCheshbon'
      Size = 10
    end
    object Qry_ChecksForHafkadaKodMatbea: TIntegerField
      FieldName = 'KodMatbea'
      Origin = 'DB_KUPA."Checks.DB".KodMatbea'
    end
    object Qry_ChecksForHafkadaKamutShtarot: TIntegerField
      FieldName = 'KamutShtarot'
      Origin = 'DB_KUPA."Checks.DB".KamutShtarot'
    end
    object Qry_ChecksForHafkadaDateHamara: TDateTimeField
      FieldName = 'DateHamara'
      Origin = 'DB_KUPA."Checks.DB".DateHamara'
    end
    object Qry_ChecksForHafkadaSchumDollar: TBCDField
      FieldName = 'SchumDollar'
      Origin = 'DB_KUPA."Checks.DB".SchumDollar'
      Precision = 32
      Size = 3
    end
    object Qry_ChecksForHafkadaShuraKopa: TIntegerField
      FieldName = 'ShuraKopa'
      Origin = 'DB_KUPA."Checks.DB".ShuraKopa'
    end
    object Qry_ChecksForHafkadaStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'DB_KUPA."Checks.DB".Status'
    end
    object Qry_ChecksForHafkadaSugStar: TIntegerField
      FieldName = 'SugStar'
      Origin = 'DB_KUPA."Checks.DB".SugStar'
    end
    object Qry_ChecksForHafkadaSchumMatbea: TBCDField
      FieldName = 'SchumMatbea'
      Origin = 'DB_KUPA."Checks.DB".SchumMatbea'
      Precision = 32
      Size = 3
    end
    object Qry_ChecksForHafkadaMisparHafkada: TIntegerField
      FieldName = 'MisparHafkada'
      Origin = 'DB_KUPA."Checks.DB".MisparHafkada'
    end
    object Qry_ChecksForHafkadaCodeBankHafkada: TIntegerField
      FieldName = 'CodeBankHafkada'
      Origin = 'DB_KUPA."Checks.DB".CodeBankHafkada'
    end
    object Qry_ChecksForHafkadaMisHeshbonHafkada: TStringField
      FieldName = 'MisHeshbonHafkada'
      Origin = 'DB_KUPA."Checks.DB".MisHeshbonHafkada'
      Size = 10
    end
    object Qry_ChecksForHafkadaCodeSnifHafkada: TIntegerField
      FieldName = 'CodeSnifHafkada'
      Origin = 'DB_KUPA."Checks.DB".CodeSnifHafkada'
    end
    object Qry_ChecksForHafkadaNumberOfPayment: TIntegerField
      FieldName = 'NumberOfPayment'
      Origin = 'DB_KUPA."Checks.DB".NumberOfPayment'
    end
    object Qry_ChecksForHafkadaCreditKind: TIntegerField
      FieldName = 'CreditKind'
      Origin = 'DB_KUPA."Checks.DB".CreditKind'
    end
    object Qry_ChecksForHafkadaValidMonth: TIntegerField
      FieldName = 'ValidMonth'
      Origin = 'DB_KUPA."Checks.DB".ValidMonth'
    end
    object Qry_ChecksForHafkadaValidYear: TIntegerField
      FieldName = 'ValidYear'
      Origin = 'DB_KUPA."Checks.DB".ValidYear'
    end
    object Qry_ChecksForHafkadaAprovalCode: TIntegerField
      FieldName = 'AprovalCode'
      Origin = 'DB_KUPA."Checks.DB".AprovalCode'
    end
    object Qry_ChecksForHafkadaPhoneNumber: TStringField
      FieldName = 'PhoneNumber'
      Origin = 'DB_KUPA."Checks.DB".PhoneNumber'
      Size = 12
    end
    object Qry_ChecksForHafkadaDateKabala: TDateTimeField
      FieldName = 'DateKabala'
      Origin = 'DB_KUPA."Kabala.DB".DateKabala'
    end
    object Qry_ChecksForHafkadaCodeLakoach: TIntegerField
      FieldName = 'CodeLakoach'
      Origin = 'DB_KUPA."Kabala.DB".CodeLakoach'
    end
    object Qry_ChecksForHafkadaLookShemBank: TStringField
      FieldKind = fkLookup
      FieldName = 'LookShemBank'
      KeyFields = 'KodBank'
      LookupCache = True
      Lookup = True
    end
    object Qry_ChecksForHafkadaLookSugTashlum: TStringField
      FieldKind = fkLookup
      FieldName = 'LookSugTashlum'
      KeyFields = 'CodeSugTashlum'
      LookupCache = True
      Lookup = True
    end
  end
  object DS_ChecksForHafkada: TDataSource
    DataSet = Qry_ChecksForHafkada
    Left = 73
    Top = 105
  end
  object AtmAdvSearch_BankForHafkada: TAtmAdvSearch
    Caption = '����� ����� ������'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla2'
      'Teur_Tavla'
      'Teur_Tavla2')
    ShowFieldsHeader.Strings = (
      '���� �����'
      '�����'
      '��'#39' ���"�')
    ShowFieldsDisplayWidth.Strings = (
      '70'
      '150'
      '70')
    Sql.Strings = (
      'Select * from kodtvla2'
      'where Sug_Tavla=101')
    ReturnFieldIndex = -1
    SeparateChar = ';'
    KeepList = False
    Left = 204
    Top = 242
  end
  object Qry_AtmIndex: TQuery
    CachedUpdates = True
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From AtmIndex'
      'Where Key_String = '#39'Hafkada'#39
      'And YehusYear = :PYehusYear')
    UniDirectional = True
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_AtmIndex
    Left = 118
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end>
  end
  object UpdateSQL_AtmIndex: TUpdateSQL
    ModifySQL.Strings = (
      'update AtmIndex'
      'set'
      '  Key_String = :Key_String,'
      '  YehusYear = :YehusYear,'
      '  Last_Number = :Last_Number,'
      '  LastUpDate = :LastUpDate,'
      '  MaklidName = :MaklidName'
      'where'
      '  Key_String = :OLD_Key_String and'
      '  YehusYear = :OLD_YehusYear')
    InsertSQL.Strings = (
      'insert into AtmIndex'
      '  (Key_String, YehusYear, Last_Number, LastUpDate, MaklidName)'
      'values'
      
        '  (:Key_String, :YehusYear, :Last_Number, :LastUpDate, :MaklidNa' +
        'me)')
    DeleteSQL.Strings = (
      'delete from AtmIndex'
      'where'
      '  Key_String = :OLD_Key_String and'
      '  YehusYear = :OLD_YehusYear')
    Left = 118
    Top = 106
  end
end

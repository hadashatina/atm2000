unit F_Kabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, ToolWin, StdCtrls, Mask, DBCtrls, AtmComp, AdvSearch,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, Menus, Grids, DBGrids, ActnList, DB,
  ImgList, Buttons, DBTables, Scale250, DBFilter, RxQuery,
  AtmTabSheetBuild,inifiles;

type
  TFrm_Kabala = class(TForm)
    PageControl1: TPageControl;
    ToolBar1: TToolBar;
    TS_Main: TTabSheet;
    Panel_Kabala: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    AtmDbHEdit_KabalaNo: TAtmDbHEdit;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    AtmDBDateEdit1: TAtmDBDateEdit;
    Label2: TLabel;
    Label_CodeLakoach: TLabel;
    KabCodeLakoach: TAtmDbHEdit;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmAdvSearch_lakoach: TAtmAdvSearch;
    Lbl_zkut: TLabel;
    ChebZikuy: TAtmDbHEdit;
    Lbl_Chiuv: TLabel;
    DBComboBox_HeshbonHiuv: TDBComboBox;
    Lbl_masmakor: TLabel;
    ChenMasMakor: TAtmDbHEdit;
    Lbl_kodbitzua: TLabel;
    KodBitzou: TAtmDbHEdit;
    DBNavigator1: TDBNavigator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ActionList1: TActionList;
    Action_duplicateRecord: TAction;
    Action_SumTotals: TAction;
    Action_OpenSearch: TAction;
    Action_SelectNextControl: TAction;
    Action_SelectPrevControl: TAction;
    AtmAdvSearch_KodBank: TAtmAdvSearch;
    AtmAdvSearch_SugTashlum: TAtmAdvSearch;
    N2: TMenuItem;
    Panel2: TPanel;
    RxDBGrid_KabHes: TRxDBGrid;
    Act_SelectOneHeshbonitForClose: TAction;
    Act_CloseHeshbonitSequence: TAction;
    Btn_Edkun: TBitBtn;
    Panel5: TPanel;
    Label6: TLabel;
    DBEdit_TotalKabala: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    AtmAdvSearch_BankForHafkada: TAtmAdvSearch;
    Act_PrintKabala: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Act_NewKabala: TAction;
    ToolButton3: TToolButton;
    N7: TMenuItem;
    N21: TMenuItem;
    Act_ChangeYehusYear: TAction;
    N8: TMenuItem;
    BitBtn2: TBitBtn;
    Scale1: TScale;
    Act_UpdateFiles: TAction;
    ToolButton4: TToolButton;
    Act_CloseOpenHeshboniot: TAction;
    Act_SelectOnlyOpenKabalot: TAction;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    Panel1: TPanel;
    RxDBGrid_Checks: TRxDBGrid;
    Label4: TLabel;
    Label5: TLabel;
    DBEdt_maam: TDBEdit;
    DBedt_totalPlusVat: TDBEdit;
    DBEdt_MamPercent: TDBEdit;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    Label7: TLabel;
    TotalBeforeMaam: TEdit;
    AtmAdvSearchenZikuy: TAtmAdvSearch;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery_Tavla: TRxQuery;
    RxDBFilter1: TRxDBFilter;
    Ds_tavla1: TDataSource;
    N10: TMenuItem;
    Pratim: TAtmDbHEdit;
    Lbl_Pratim: TLabel;
    procedure Action_OpenSearchExecute(Sender: TObject);
    procedure AtmAdvSearch_KodBankAfterExecute(Sender: TObject);
    procedure Action_OpenSearchUpdate(Sender: TObject);
    procedure N2Click(Sender: TObject);
//  procedure Act_ShowOpenHeshbonitForLakExecute(Sender: TObject);
    procedure Panel_KabalaExit(Sender: TObject);
    procedure Panel_KabalaEnter(Sender: TObject);
    procedure RxDBGrid_ChecksEnter(Sender: TObject);
    procedure AtmAdvSearch_SugTashlumAfterExecute(Sender: TObject);
//  procedure Act_SelectOneHeshbonitForCloseExecute(Sender: TObject);
//  procedure Act_CloseHeshbonitSequenceExecute(Sender: TObject);
    procedure RxDBGrid_KabHesEnter(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AtmAdvSearch_BankForHafkadaAfterExecute(Sender: TObject);
    procedure RxDBGrid_ChecksExit(Sender: TObject);
    procedure RxDBGrid_KabHesExit(Sender: TObject);
    procedure Act_PrintKabalaExecute(Sender: TObject);
    procedure DBNavigator1BeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure KodBitzouChange(Sender: TObject);
    procedure Label_CodeLakoachDblClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure Act_NewKabalaExecute(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure Act_ChangeYehusYearExecute(Sender: TObject);
//  procedure RxDBGrid_OpenHeshbonitExit(Sender: TObject);
    procedure Act_UpdateFilesExecute(Sender: TObject);
    procedure Action_duplicateRecordUpdate(Sender: TObject);
    procedure Action_duplicateRecordExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RxDBGrid_KabHesGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure RxDBGrid_ChecksKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure KabCodeLakoachBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_MaslulAfterExecute(Sender: TObject);
    procedure RxDBGrid_ChecksColEnter(Sender: TObject);
    procedure ChebZikuyBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure ChebZikuyEnter(Sender: TObject);
    procedure ChebZikuyExit(Sender: TObject);
    procedure RxDBGrid_KabHesColEnter(Sender: TObject);
    procedure RxDBGrid_KabHesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdt_maamExit(Sender: TObject);
    procedure DBEdt_maamKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdt_maamEnter(Sender: TObject);
    procedure DBEdt_MamPercentEnter(Sender: TObject);
    procedure DBEdt_MamPercentKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdt_MamPercentExit(Sender: TObject);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure RxDBGrid_KabHesDblClick(Sender: TObject);
  private
    { Private declarations }
    Procedure FillComboHeshbonHiuv;
  public
    { Public declarations }
    Procedure InitKabala;
    Procedure UpdateAllFiles(KodBitzua:LongInt);
    Procedure DoUpdateInFiles;
    Procedure PrintCurKabala;
    Procedure TotalSchum;
    Procedure EgulMaam;
    Procedure BuildHesFromAzmn;
    Procedure BuildHesFromMlay;
  end;

var
  Frm_Kabala: TFrm_Kabala;
//  Hadpasa   : String[1];

implementation

uses Dmkupa, AtmConst, AtmRutin, F_PrintKabala;

{$R *.DFM}

Procedure TFrm_Kabala.FillComboHeshbonHiuv;
Begin
  DBComboBox_HeshbonHiuv.Items.Clear;
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_HeshbonLehiuv));
    Open;
    First;
    While Not Eof Do
    Begin
      DBComboBox_HeshbonHiuv.Items.Add(FieldByName('Teur_Tavla').AsString);
      Next;
    End;
    Close;
  End;
End;

procedure TFrm_Kabala.Action_OpenSearchExecute(Sender: TObject);
Var
   Bool :Boolean;
Begin
     if ActiveControl is TAtmDbHEdit Then
       (ActiveControl As TAtmDbHEdit).ExecSearch
     Else
     Begin
       if ActiveControl=RxDBGrid_Checks Then
       Begin
        if RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank') Then
          AtmAdvSearch_KodBank.Execute;
        if RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum') Then
          AtmAdvSearch_SugTashlum.Execute;
        if (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeBankHafkada')) Or
           (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSnifHafkada')) Or
           (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('MisHeshbonHafkada')) Then
                AtmAdvSearch_BankForHafkada.Execute;
       End
     Else
       if ActiveControl=RxDBGrid_Kabhes Then
          if RxDBGrid_Kabhes.SelectedField=DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul') Then
            AtmAdvSearch_Maslul.Execute;
     End;
end;

procedure TFrm_Kabala.AtmAdvSearch_KodBankAfterExecute(Sender: TObject);
begin
  if DM_Kupa.AtmRxQuery_Checks.State=dsBrowse Then
    DM_Kupa.AtmRxQuery_Checks.Edit;
  DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank').AsString:=AtmAdvSearch_KodBank.ReturnString;
end;

procedure TFrm_Kabala.Action_OpenSearchUpdate(Sender: TObject);
Begin
  Try
     if ActiveControl is TAtmDbHEdit Then
        (Sender As TAction).Enabled:=(ActiveControl As TAtmDbHEdit).EnableSearch
     Else
      if (ActiveControl=RxDBGrid_Checks) Or
         (ActiveControl=RxDBGrid_Kabhes) Then
      Begin
        (Sender As TAction).Enabled:=(RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('KodBank')) Or
                                     (RxDBGrid_Checks.SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum')) Or
                                     (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeBankHafkada')) Or
                                     (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSnifHafkada')) Or
                                     (RxDbGrid_Checks.SelectedField = DM_Kupa.AtmRxQuery_Checks.FieldByName('MisHeshbonHafkada')) Or
                                     (RxDbGrid_Kabhes.SelectedField = DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit')) ;
      End
      Else
         (Sender As TAction).Enabled:=False;
  Except On Exception Do;
  End;
end;

Procedure TFrm_Kabala.InitKabala;
Var
  yy,mm,dd :Word;
  HStr     :shortstring;
Begin
  DecodeDate(Now,YY,MM,DD);
  CurKabalaYear:=YY;
  DM_Kupa.AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  If Snif<>-1 Then
  Begin
    DM_Kupa.AtmRxQuery_Kabala.Filtered:=True;
    DM_Kupa.AtmRxQuery_Kabala.Filter:='MaklidName='+IntToStr(snif);
  End;
  DM_Kupa.AtmRxQuery_Kabala.Open;
  DM_Kupa.AtmRxQuery_Kabala.Last;

  If Kabala_type MOD 10=0 Then HStr:='������ ' Else HStr:='����� ';
  Caption:='������� ��/���� '+HStr+IntToStr(CurKabalaYear);
  If Kabala_type=10 Then
    Label_CodeLakoach.Caption:='<���>'
  Else
    Label_CodeLakoach.Caption:='<����>';
End;

procedure TFrm_Kabala.N2Click(Sender: TObject);
begin
  Close;
end;
{
procedure TFrm_Kabala.Act_ShowOpenHeshbonitForLakExecute(Sender: TObject);
begin
  DM_Kupa.ShowOpenHeshbonitForLak(DM_Kupa.AtmRxQuery_KabalaCodeLakoach.AsInteger,Spb_ShowOpenHeshbonitForLakGroup.Down);
end;
}

procedure TFrm_Kabala.Panel_KabalaExit(Sender: TObject);
Var
  NewKabala :Boolean;
begin
  NewKabala:=DM_Kupa.AtmRxQuery_Kabala.State=dsInsert;
  if DM_Kupa.AtmRxQuery_Kabala.State in [dsInsert,dsEdit] Then
    DM_Kupa.AtmRxQuery_Kabala.Post;
  if NewKabala Then
    DM_Kupa.AtmRxQuery_Checks.Append;
end;

procedure TFrm_Kabala.Panel_KabalaEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_Kabala;

end;

procedure TFrm_Kabala.RxDBGrid_ChecksEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_Checks;
  RxDBGrid_Checks.Col:=1;
  StatusBar1.SimpleText:='F6 - ����� ����� ���';
end;

procedure TFrm_Kabala.AtmAdvSearch_SugTashlumAfterExecute(Sender: TObject);
begin
  if AtmAdvSearch_SugTashlum.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Checks.State=dsBrowse Then
      DM_Kupa.AtmRxQuery_Checks.Edit;
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsString:=AtmAdvSearch_SugTashlum.ReturnString;
  End;
end;


Procedure TFrm_Kabala.RxDBGrid_KabHesEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_KabHes;
  RxDBGrid_Kabhes.Col:=1;
  StatusBar1.SimpleText:='F2 ����� ������� F5 ����� ���"�' ;
  DM_Kupa.KaBhesWasDelete:=False;

  if DM_Kupa.KabHesWasInsert Then
  Begin
    if DM_Kupa.AtmRxQuery_KabHes.State in [dsEdit,dsInsert] Then
      DM_Kupa.AtmRxQuery_KabHes.Post;
    DM_Kupa.AtmRxQuery_KabHes.Close;
    DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
    DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
    DM_Kupa.AtmRxQuery_KabHes.Open;
    DM_Kupa.KabHesWasInsert:=False;
  End;

end;

procedure TFrm_Kabala.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyboardManageForTableAction(Self,Key,Shift,DBNavigator1.DataSource.DataSet);
  if Key=VK_F6 Then
    SelectNext(ActiveControl,Not (ssShift in Shift),True);
end;

Procedure TFrm_Kabala.UpdateAllFiles(KodBitzua:LongInt);
Begin
  if DM_Kupa.AtmRxQuery_Checks.State in [dsEdit,dsInsert] Then
    DM_Kupa.AtmRxQuery_Checks.Post;
  DM_Kupa.Query_InsertKupa.Open;
  DM_Kupa.Database_Kupa.StartTransaction;
  Try
    DoUpdateInFiles;
  Except On E:Exception Do
    Begin
      ShowMessage(e.Message);
      DM_Kupa.Database_Kupa.Rollback;
      DM_Kupa.Query_InsertKupa.Close;
      Raise;
    End;
  End;
  DM_Kupa.Query_InsertKupa.ApplyUpdates;
  DM_Kupa.Database_Kupa.Commit;
  DM_Kupa.Query_InsertKupa.Close;
  //����� ��� ����� 2
  DM_Kupa.AtmRxQuery_Kabala.Edit;
  DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger:=KodBitzua;
  DM_Kupa.AtmRxQuery_Kabala.SavePressed:=True;
  DM_Kupa.AtmRxQuery_Kabala.Post;
  DM_Kupa.AtmRxQuery_Checks.Database.ApplyUpdates([DM_Kupa.AtmRxQuery_Checks]);
  If HesFromMlay<>0 Then
  Begin
    With DM_Kupa.Qry_Temp Do //SHIPMENT ����� ������ ���� ��� �-�
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Update Shipment');
      Sql.Add('Set HesbonZikuy='+
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsString);
      Sql.Add(',KodBitzua=2');
      Sql.Add('Where KabalaNumber='+IntToStr(HesFromMlay) );
      Sql.Add('And   YehusYear=' + IntToStr(YearFromMlay) );
      ExecSQL;
      HesFromMlay:=0;
    End; //With DM_Kupa.Qry_Temp
  End;
End;

Procedure TFrm_Kabala.DoUpdateInFiles;
Begin
  if DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger > 0 Then
    Exit;
  Screen.Cursor:=crSQLWait;
  Try
//    ShowMessage('BuildLineInMakavForCurKabala');
    DM_Kupa.BuildLineInMakavForCurKabala;
//    ShowMessage('UpdateMakavForCloseHesbonit');
//  DM_Kupa.UpdateMakavForCloseHesbonit;
//    ShowMessage('BuildLinesInKopaForCurChecks');
    DM_Kupa.BuildLinesInKopaForCurChecks;
//    ShowMessage('End');
    If YesMlay Then DM_Kupa.UpdateMlayPritim;
  Finally
    Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_Kabala.AtmAdvSearch_BankForHafkadaAfterExecute(
  Sender: TObject);
Var
  StrH :String;
begin
  if AtmAdvSearch_BankForHafkada.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Checks.State = dsBrowse Then
      DM_Kupa.AtmRxQuery_Checks.Edit;

    StrH:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,2);//Teur_Tavla
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeBankHafkada').AsString:=GetFieldFromSeparateString(StrH,',',1);
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSnifHafkada').AsString:=GetFieldFromSeparateString(StrH,',',2);
    DM_Kupa.AtmRxQuery_Checks.FieldByName('MisHeshbonHafkada').AsString:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,1);//Teur_Tavla

    if DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger=stHavaraBank Then
    Begin
      StrH:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,3); //���� ���"� �� ��� ������
      if (Trim(StrH)<>'') And (CompareText(DM_Kupa.AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString,StrH)<>0) Then
        if MessageDlg('����� ����� ���� ����� ���"� �� ���� ������. ��� ����',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
        Begin
          if DM_Kupa.AtmRxQuery_Kabala.State = dsBrowse Then
            DM_Kupa.AtmRxQuery_Kabala.Edit;
          DM_Kupa.AtmRxQuery_Kabala.FieldByName('HesbonChiuv').AsString:=StrH;
        End;
    End;

  End;
end;

procedure TFrm_Kabala.RxDBGrid_ChecksExit(Sender: TObject);
begin
  if TCustomDbGrid(Sender).DataSource.DataSet.State in [dsInsert,dsEdit] Then
    TCustomDbGrid(Sender).DataSource.DataSet.Post;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesExit(Sender: TObject);
begin
  if TCustomDbGrid(Sender).DataSource.DataSet.State in [dsInsert,dsEdit] Then
    TCustomDbGrid(Sender).DataSource.DataSet.Post;

end;

Procedure TFrm_Kabala.PrintCurKabala;
Var
  StrH :String;
  NumOfCopy,Code :LongInt;
Begin
(*
  StrH :=InputBox('����� ����','���� ������','2');
  Val(StrH,NumOfCopy,Code);
  if Code<>0 Then Exit;
  Frm_kabprt:=Tfrm_kabprt.Create(Nil);
  Try
//    If Frm_copy.showModal = mrOk Then
    Begin
     With DM_Kupa Do
     Begin
{      Frm_kabprt.QRLabel_CompayName.Caption:=ComName;
      Frm_kabprt.QrLbl_CompNum.Caption:=ComNum;
      Frm_kabprt.Qrlbl_OsekMorshe.Caption := OsekMorske;
      Frm_kabprt.QR_title.Caption:=Detail;}
      If (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Or
         (AtmRxQuery_Kabala.FieldByName('KodBItzua').IsNull) Or
         (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=2)
      Then
      Begin
        Frm_kabprt.QRLabel4.Caption:='����';
        Print_Kabala;
        Frm_kabprt.QRLabel4.Caption:='����';
      End
      Else
         If Tbl_KabalaKodBItzua.AsInteger=9
      Then
         Frm_kabprt.QRLabel4.Caption:='�����'
      Else
         Frm_kabprt.QRLabel4.Caption:='����';
    // ����� ������
       For Ix1:=1 To NumOfCopy Do
          Frm_kabprt.QuickRep1.Preview; {Print}{preview;}
       If (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Or
          (AtmRxQuery_Kabala.FieldByName('KodBItzua').IsNull) Then
             UpdateAllFiles(cbkPrintSource);
     End; {With Frm_DbKab}
    End;      //mrOk
  Finally
    Frm_kabprt.Free;
    Frm_kabprt:=Nil;
  End;

*)
End;

procedure TFrm_Kabala.Act_PrintKabalaExecute(Sender: TObject);
Var
  StrH           : String;
  NumOfCopy,Code : LongInt;
  Hefresh        : Real;
Begin
  If (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency=0)
    Or
     (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency=0)
  Then
  Begin
     Showmessage('��� ���� ����� �� ���� �����');
     Abort;
     Exit;
  End;

  If DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency <>
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency Then
  Begin
     Hefresh:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency-
              DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency;

     Showmessage('��� ����� ��� ����� ����� ����� ����� '
                 +#13+'����� >>>>>> '+FloatToStr(Hefresh)
                 +#13+'�� ����� �����');
     Abort;
     Exit;
  End;

  Repeat
    StrH:=CopysKabala ; //'1';
    if Not InputQuery('����� �� ��/����','���� ������',StrH) Then
      Exit;    Val(StrH,NumOfCopy,Code);
  Until Code=0;
  DM_Kupa.AtmRxQuery_KabalaAfterScroll(Nil); //refresh all qreries
  Frm_PrintKabala:=TFrm_PrintKabala.Create(Nil);
  Try
    Case DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger Of
      0:
        Begin
          StatusBar1.SimpleText:='����� �����...';
          Frm_PrintKabala.QRLabel_Makor.Caption:='����';
          UpdateAllFiles(cbkPrintSource);
          NumOfCopy:=NumOfCopy+1;
        End;
      1 : Frm_PrintKabala.QRLabel_Makor.Caption:='����';
      2 :    //����� ��� ����� 2 �-1
        Begin
          Frm_PrintKabala.QRLabel_Makor.Caption:='����';
          DM_Kupa.AtmRxQuery_Kabala.Edit;
          DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger:=1;
          DM_Kupa.AtmRxQuery_Kabala.SavePressed:=True;
          DM_Kupa.AtmRxQuery_Kabala.Post;
        End;
      9 : Frm_PrintKabala.QRLabel_Makor.Caption:='�����';
    End;

    For Code:=1 To NumOfCopy Do
    Begin
      DM_Kupa.AtmRxQuery_Checks.First;
      Kodmatbea:=DM_Kupa.AtmRxQuery_Checks.FieldByName('KodMatbea').AsInteger;
      Frm_PrintKabala.QuickRep1.Preview; //Print; //  David
      Frm_PrintKabala.QRLabel_Makor.Caption:='����';
    End;
    DM_Kupa.AtmRxQuery_Kabala.Append;
  Finally
    Frm_PrintKabala.Release; //  Free;
    Frm_PrintKabala:=Nil;
    StatusBar1.SimpleText:='';
  End;
end;

procedure TFrm_Kabala.DBNavigator1BeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
    if (DM_Kupa.NewKabalaSavedNumber=DM_Kupa.AtmRxQuery_KabalaKabalaNumber.AsInteger) And
       (DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) And
       (DM_Kupa.AtmRxQuery_KabalaTotalPlusVat.AsFloat<>0) And
       (Button<>nbDelete)
    Then
    Begin
       Showmessage(' �-� �� ���� �� ������ ');
       Abort;
    End;

  Case Button Of
    nbRefresh:Begin
                if DBNavigator1.DataSource.DataSet is TQuery Then
                Begin
                  TQuery(DBNavigator1.DataSource.DataSet).Close;
                  if TQuery(DBNavigator1.DataSource.DataSet).Params.FindParam('PKabalaNumber')<>Nil Then
                    TQuery(DBNavigator1.DataSource.DataSet).ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
                  if TQuery(DBNavigator1.DataSource.DataSet).Params.FindParam('PKabalaYear')<>Nil Then
                    TQuery(DBNavigator1.DataSource.DataSet).ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
                  TQuery(DBNavigator1.DataSource.DataSet).Open;
                  Abort;
                End;
              End;
  End //Case;
end;

procedure TFrm_Kabala.KodBitzouChange(Sender: TObject);
begin
  Act_UpdateFiles.Enabled:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0;
  Act_CloseOpenHeshboniot.Enabled:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger in [1,2];
end;

procedure TFrm_Kabala.Label_CodeLakoachDblClick(Sender: TObject);
begin
  If Kabala_type=10 Then
    OpenCrt(fnNehag,KabCodeLakoach.Text)
  Else
    OpenCrt(fnLakoach,KabCodeLakoach.Text);
end;

procedure TFrm_Kabala.N4Click(Sender: TObject);
begin
  OpenCrt(fnMakav,'');
end;

procedure TFrm_Kabala.N5Click(Sender: TObject);
begin
  OpenCrt(fnLakoach,'');
end;

procedure TFrm_Kabala.N6Click(Sender: TObject);
begin
  OpenCrt(fnNehag,'');
end;

procedure TFrm_Kabala.Act_NewKabalaExecute(Sender: TObject);
begin
  DM_Kupa.AtmRxQuery_Kabala.Append;
end;

procedure TFrm_Kabala.N7Click(Sender: TObject);
begin
  OpenCrt(fnTavla,'');
end;

procedure TFrm_Kabala.N21Click(Sender: TObject);
begin
  OpenCrt(fnTavla2,'');
end;

procedure TFrm_Kabala.Act_ChangeYehusYearExecute(Sender: TObject);
Var
  StrH,HStr :String;
  Code      :longInt;
begin
  Repeat
    StrH:=IntToStr(CurKabalaYear);
    if Not InputQuery('����� ��� �����','��� �����',StrH) Then
      Exit;
    Val(StrH,CurKabalaYear,Code);
  Until Code=0;

  DM_Kupa.AtmRxQuery_Kabala.Close;
  DM_Kupa.AtmRxQuery_Checks.Close;
  DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Checks.ParamByName('PKabalaYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_Kabala.Open;
  If Kabala_type MOD 10=0 Then HStr:='������ ' Else HStr:='����� ';
  Caption:='����� '+HStr+IntToStr(CurKabalaYear);
  DM_Kupa.GetLastKabalaNumber;
end;
{
procedure TFrm_Kabala.RxDBGrid_OpenHeshbonitExit(Sender: TObject);
begin
  StatusBar1.SimpleText:='';
end;
}
procedure TFrm_Kabala.Act_UpdateFilesExecute(Sender: TObject);
Var
Hefresh  :  Real ;
begin
  If (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency=0)
    Or
     (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency=0)
  Then
  Begin
     Showmessage('��� ���� ����� �� ���� �����');
     Abort;
     Exit;
  End;

  If DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency <>
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency Then
  Begin
     Hefresh:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency-
              DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency;
     Showmessage('��� ����� ��� ����� ����� ����� ����� '
                 +#13+'����� >>>>>> '+FloatToStr(Hefresh)
                 +#13+' �� ����� �����');
     Abort;
     Exit;
  End;
  DM_Kupa.AtmRxQuery_KabalaAfterScroll(Nil); //refresh all qreries
  UpdateAllFiles(cbkUpdateOnly);
  DM_Kupa.AtmRxQuery_Kabala.Append;
end;

procedure TFrm_Kabala.Action_duplicateRecordUpdate(Sender: TObject);
begin
  Action_duplicateRecord.Enabled:=ActiveControl=RxDBGrid_Checks;
end;

procedure TFrm_Kabala.Action_duplicateRecordExecute(Sender: TObject);
begin
  if (Action_duplicateRecord.Enabled) And (DM_Kupa.AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0) Then
    DuplicateOneRecord(DM_Kupa.AtmRxQuery_Checks,False,True);
end;

Procedure TFrm_Kabala.FormCreate(Sender: TObject);
Var
//Atmini   : Tinifile;
  CurYear  : Word;
  Res,Code : LongInt;
  StrH,StrK: String;
  yy,mm,dd : Word;

begin
  AtmTabSheetBuild1.ScrFileName:=KupaDllScriptsDir+AtmTabSheetBuild1.ScrFileName;
  AtmTabSheetBuild1.SqlFileName:=KupaDllScriptsDir+AtmTabSheetBuild1.SqlFileName;
  AtmTabSheetBuild1.BuildTabsForGrids;
  FillComboHeshbonHiuv;
  Strh:=ExtractFilePath(Application.ExeName)+'Frm_GlbMasKab.Ini';
  AtmAdvSearch_lakoach.IniFileName:=Strh;
  AtmAdvSearch_Nehag.IniFileName:=Strh;
  AtmAdvSearch_Maslul.IniFileName:=Strh;
  AtmAdvSearchenZikuy.IniFileName:=Strh;
  AtmAdvSearch_KodBank.IniFileName:=Strh;
  AtmAdvSearch_SugTashlum.IniFileName:=Strh;
  AtmAdvSearch_BankForHafkada.IniFileName:=Strh;
Try
  except
  on E:exception do
      showmessage(E.Message);
  end;

  DecodeDate(Now,YY,MM,DD);
  CurKabalaYear:=YY;
//DM_Kupa.GetNextKabalaNumber(+0);
//Result:=0;
  CurYear:=CurKabalaYear;
  With DM_Kupa.Qry_AtmIndexKabala Do
  Begin
//  If Snif<>-1 Then StrK:=aiHeshbonitMasKabala+IntToStr(Snif) // 'HsbMasKabala'
    If Super<>-1  Then StrK:=aiHeshbonitMasKabala+IntToStr(Super)
    ELse StrK:=aiHeshbonitMasKabala;

    ParamByName('PKey_String').AsString:=StrK;
    ParamByName('PYehusYear').AsInteger:=CurYear;
    Open;
    if Eof And Bof Then //��� ����
    Begin
    StrH:='16000';
    Repeat
      if Not InputQuery('��� ���� ','������ ����� �� ��/���� ',StrH) Then
        Exit;     Val(StrH,Res,Code);
    Until Code=0;
      Insert;
      FieldByName('Last_Number').AsInteger:=Res-1; // 1
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=StrK;
      FieldByName('YehusYear').AsInteger:=CurYear;
      Post;
    End
  End;
end;


Procedure TFrm_Kabala.FormShow(Sender: TObject);
Var
  NewKabala: Boolean;
  LastKab  : Integer;
begin
  DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DBNavigator1.DataSource:=DM_Kupa.DS_Kabala;
  If HesFromCrt<>0 Then
  Begin
    DM_Kupa.AtmRxQuery_Kabala.ParamByName('PYehusYear').AsInteger:=YearFromCrt;
    CurKabalaYear:=YearFromCrt;
    DM_Kupa.AtmRxQuery_Kabala.Locate('KabalaNumber;YehusYear',VarArrayOf([HesFromCrt,YearFromCrt]),[]);
    Exit;
  End;
  DM_Kupa.AtmRxQuery_Kabala.Last;

  If HesFromMlay>0 Then
  Begin
    BuildHesFromMlay;
    RxDBGrid_KabHes.ReadOnly:=True;
    DBEdt_maam.ReadOnly:=True;
    Frm_Kabala.Activecontrol:=RxDBGrid_Checks;
    DBNavigator1.DataSource:=DM_Kupa.DS_Checks;
    RxDBGrid_Checks.Col:=1;
    DM_Kupa.AtmRxQuery_Checks.Edit;
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger:=0;
    Exit;
  End;

  If ShibAzmn>0 Then
  Begin
    BuildHesFromAzmn;
    Frm_Kabala.Activecontrol:=RxDBGrid_Checks;
    DBNavigator1.DataSource:=DM_Kupa.DS_Checks;
    RxDBGrid_Checks.Col:=1;
    DM_Kupa.AtmRxQuery_Checks.Edit;
    DM_Kupa.AtmRxQuery_Checks.FieldByName('CodeSugTashlum').AsInteger:=0;
  End
  Else
  Begin
    NewKabala:=True;
    if (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat=0)
       And (DM_Kupa.AtmRxQuery_Kabala.RecordCount<>0)  Then
    NewKabala:=MessageDlg('� �-�/�� ���� ������� ����� ��� ���� ��� ������ ������ ������ ����',mtInformation,[mbYes,mbNo],0)=mrNo;
    Try
    if NewKabala Then
    Begin
      DM_Kupa.AtmRxQuery_Kabala.Append;
    End;
    except
      on E:exception do
        showmessage(E.Message);
      end;
  End;
    DBNavigator1.DataSource:=DM_Kupa.DS_Kabala;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
//  if RxDBGrid_KabHes.DataSource.DataSet.FieldByName('CodeBitzua').AsInteger=1 Then
//    Background:=clRed;
end;


procedure TFrm_Kabala.RxDBGrid_ChecksKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
Var
  IntH :LongInt;
begin
  if (Key=VK_F5) And
   (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_Checks.FieldByName('SchumCheck'))
  Then
  Begin
     DM_Kupa.AtmRxQuery_Checks.Edit;
     DM_Kupa.AtmRxQuery_Checks.FieldByName('SchumCheck').AsCurrency:=
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency -
    (DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsCurrency-
     DM_Kupa.AtmRxQuery_Checks.FieldByName('SchumCheck').AsCurrency);
  End;
  if Key=VK_F9 Then
  Begin
    if TDbGrid(Sender).SelectedField.FieldKind=fkData Then
    With DM_Kupa.Qry_Temp DO
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Max(TNUA) SN From CHECKS');
      Open;
      IntH:=FieldByName('SN').AsInteger;
      Close;
      if IntH<>0 Then
      Begin
        Sql.Clear;
        Sql.Add('Select '+TDbGrid(Sender).SelectedField.FieldName);
        Sql.Add('From CHECKS Where TNUA='+IntToStr(IntH));
        Open;
        if Not Eof Or BOF Then
        Begin
          TDbGrid(Sender).SelectedField.AsString:=FieldByName(TDbGrid(Sender).SelectedField.FieldName).AsString;
          TDbGrid(Sender).SelectedIndex:=TDbGrid(Sender).SelectedIndex+1;
        End;
        Close;
      End;
    End;
  End;

end;

procedure TFrm_Kabala.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  With DM_Kupa Do
  Begin
    if (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
       (AtmRxQuery_KabalaTotalSumKabla.AsFloat=0) Then
      Case MessageDlg('�� ��/���� ����� ��� ������ ��� ����� ����',mtConfirmation,[mbYes,mbNo,mbCancel],0) Of
        mrYes:AtmRxQuery_Kabala.Delete;
        mrCancel:Action:=caNone;
      End; //Case

    if (NewKabalaSavedNumber=AtmRxQuery_KabalaKabalaNumber.AsInteger) And
       (AtmRxQuery_Kabala.FieldByName('KodBItzua').AsInteger=0)
    Then
    Begin
       Showmessage(' �-� �� ���� �� ������ ');
       Abort;
    End;
  End; //With DM_Kupa
end;

procedure TFrm_Kabala.KabCodeLakoachBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
   If Kabala_type=10 Then
     KabCodeLakoach.SearchComponent:=AtmAdvSearch_Nehag
   Else
     KabCodeLakoach.SearchComponent:=AtmAdvSearch_lakoach;
end;

Procedure TFrm_Kabala.TotalSchum;
begin
//  DM_Kupa.AtmRxQuery_Kabhes.Edit;
    If Not DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Schumhesbonit').IsNull
    Then
      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat:=
      Round(
       DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat *
       SumEgul*100)/100
     Else
       DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsString:=null;

//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Schumhesbonit').AsFloat;
//  DM_Kupa.AtmRxQuery_Kabhes.Post;
    DM_Kupa.AtmRxQuery_Kabala.Edit;
    DM_Kupa.CalcTotalHesbonit;
    DM_Kupa.AtmRxQuery_Kabala.Post;
end;

Procedure TFrm_Kabala.AtmAdvSearch_MaslulAfterExecute(Sender: TObject);
begin
  if AtmAdvSearch_Maslul.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Kabhes.State=dsBrowse Then
      DM_Kupa.AtmRxQuery_Kabhes.Edit;
//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:=AtmAdvSearch_Maslul.ReturnString;
    DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul').AsString:=AtmAdvSearch_Maslul.ReturnString;
  End;

end;

procedure TFrm_Kabala.RxDBGrid_ChecksColEnter(Sender: TObject);
begin
      If TDbGrid(Sender).SelectedField.FieldName='SchumCheck'  Then
      StatusBar1.SimpleText:='F5 - ����� ���� ��������'
  Else
      StatusBar1.SimpleText:='';

end;

procedure TFrm_Kabala.ChebZikuyBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ChebZikuy.SearchComponent:=AtmAdvSearchenZikuy;
end;

procedure TFrm_Kabala.ChebZikuyEnter(Sender: TObject);
begin
    StatusBar1.SimpleText:='���/� �-� ������ ������ F2 ����� �����'
end;

procedure TFrm_Kabala.ChebZikuyExit(Sender: TObject);
begin
    StatusBar1.SimpleText:='';
end;

procedure TFrm_Kabala.RxDBGrid_KabHesColEnter(Sender: TObject);
begin
    StatusBar1.SimpleText:='';
    If TDbGrid(Sender).SelectedField.FieldName='SchumHesbonit' Then
       StatusBar1.SimpleText:='F5 ����� ���� ��"�';
    If TDbGrid(Sender).SelectedField.FieldName='CodeMaslul' Then
       StatusBar1.SimpleText:='F2 ����� ������� F5 ����� ���"�'
end;

procedure TFrm_Kabala.RxDBGrid_KabHesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
Var
  IntH :LongInt;

begin
  if (Key=VK_F5) And
   (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit'))
  Then
  Begin
    Try
      DM_Kupa.AtmRxQuery_Kabhes.Post;
    Except
    End; //Try
    DM_Kupa.AtmRxQuery_Kabhes.Edit;
    SumEgul:=
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').AsCurrency/
     (1+DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency/100);
    SumMakor:=DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').AsCurrency;

    DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').AsCurrency:=
    Round(SumEgul*100)/100;

{     Round(
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').AsCurrency/
     (1+DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency/100)
      *100)/100 ;
}
  End;
  if Key=VK_F9 Then
  Begin
    if TDbGrid(Sender).SelectedField.FieldKind=fkData Then
    With DM_Kupa.Qry_Temp DO
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Max(TNUA) SN From KABHESMS');
      Open;
      IntH:=FieldByName('SN').AsInteger;
      Close;
      if IntH<>0 Then
      Begin
        Sql.Clear;
        Sql.Add('Select '+TDbGrid(Sender).SelectedField.FieldName);
        Sql.Add('From KABHESMS Where TNUA='+IntToStr(IntH));
        Open;
        if Not Eof Or BOF Then
        Begin
          TDbGrid(Sender).SelectedField.AsString:=FieldByName(TDbGrid(Sender).SelectedField.FieldName).AsString;
          TDbGrid(Sender).SelectedIndex:=TDbGrid(Sender).SelectedIndex+1;
        End;
        Close;
      End; // if IntH<>0
    End;  //With DM_Kupa.Qry_Temp
  End;   //Vk-F9
  if (Key=VK_F5) And
   (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_KabHes.FieldByName('CodeMaslul'))
  Then
    DBEdt_maam.SetFocus;

end;

procedure TFrm_Kabala.DBEdt_maamExit(Sender: TObject);
begin
  StatusBar1.SimpleText:='';
  DM_Kupa.AtmRxQuery_Kabala.Edit;
  DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat:=
      StrToFloat(Frm_Kabala.TotalBeforeMaam.Text)+
      DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat;
  DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency:=
      DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat;

end;

procedure TFrm_Kabala.DBEdt_maamKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F5 Then
    EgulMaam;
end;

procedure TFrm_Kabala.DBEdt_maamEnter(Sender: TObject);
begin
     StatusBar1.SimpleText:='F5 ����� ��"� ��������';
end;

procedure TFrm_Kabala.DBEdt_MamPercentEnter(Sender: TObject);
begin
     StatusBar1.SimpleText:='F6 ����� ���"� F5 ������ ���"�';

end;

procedure TFrm_Kabala.DBEdt_MamPercentKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_F6 Then
  Begin
     DM_Kupa.AtmRxQuery_Kabala.Edit;
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat:=0;
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsFloat:=0;
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat:=
           StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsFloat:=
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat;
  End; // Vk_F6

  If Key=VK_F5 Then
  Begin
     DM_Kupa.AtmRxQuery_Kabala.Edit;
     If (Kabala_type=1) Or (Atzmada=2) Then
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=0
     Else
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=Maam;
     EgulMaam;
  End; //Vk_F5
end;

Procedure TFrm_Kabala.EgulMaam;
Begin
     DM_Kupa.AtmRxQuery_Kabala.Edit;
     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat:=
     Round(
       StrToFloat(Frm_Kabala.TotalBeforeMaam.Text)*
       (1+DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsFloat/100))-
//     DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat)-
       StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
       DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat:=
           DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat+
           StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
       DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency:=
         DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat;
end;

procedure TFrm_Kabala.DBEdt_MamPercentExit(Sender: TObject);
begin
     StatusBar1.SimpleText:='';
end;


procedure TFrm_Kabala.AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
begin
     If RxQuery_Tavla.Params.FindParam('PyehusYear') <> Nil then
        RxQuery_Tavla.ParamByName('PyehusYear').AsInteger :=CurKabalaYear;
end;

procedure TFrm_Kabala.N9Click(Sender: TObject);
begin
  OpenCrt(fnPritim,'');
end;

procedure TFrm_Kabala.RxDBGrid_KabHesDblClick(Sender: TObject);
Begin
     if Sender Is TDBGrid Then
     Begin
       if TDBGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul')
         Then OpenCrt(fnPritim,DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul').Asstring);
     End;

end;
Procedure TFrm_Kabala.BuildHesFromAzmn;
Begin
  DM_Kupa.AtmRxQuery_Kabala.Append;
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Lakno1,Maslulcode1,Maslul1,Quntity1,Price1,PriceQuntity1,L.Shem_lakoach,ShibAzmnDate,shibazmnno');
    Sql.Add('From Atmshib,Lakoach L');
    Sql.Add('Where shibazmnno>=' + IntToStr(ShibAzmn) );
    Sql.Add('And   shibazmnno<=' + IntToStr(ShibAzmnTo) );
    Sql.Add('And Lakno1=L.Kod_lakoach');
    Sql.Add('And LakCodeBizua Is Null');
    Sql.Add('Order By shibazmnno');
Sql.savetofile('c:\l.sql');
    Open;
    First;
    DM_Kupa.AtmRxQuery_KabalaCodeLakoach.OnValidate:=Nil;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').Asinteger:=
        DM_Kupa.Qry_Temp.FieldByName('Lakno1').AsInteger;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('ShemLakoach').Asstring:=
        DM_Kupa.Qry_Temp.FieldByName('Shem_Lakoach').Asstring;
     If Kabala_type=1
     Then
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=0
     Else
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=Maam;
    DM_Kupa.AtmRxQuery_Kabala.Post;
    DM_Kupa.AtmRxQuery_KabalaCodeLakoach.OnValidate:=
        DM_Kupa.AtmRxQuery_KabalaCodeLakoachValidate;

    DM_Kupa.AtmRxQuery_KabHescodemaslul.OnValidate:=Nil;
    DM_Kupa.AtmRxQuery_KabHesKamutHesbonit.OnChange:=Nil;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnChange:=Nil;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnValidate:=Nil;
    DM_Kupa.AtmRxQuery_KabHes.AfterPost:=Nil;

    While Not Eof Do
    Begin
      DM_Kupa.AtmRxQuery_KabHes.Append;
      DM_Kupa.AtmRxQuery_KabHesFromNum.Asstring:=
           FieldByName('shibazmnno').Asstring;
      DM_Kupa.AtmRxQuery_KabHesToNum.Asstring:=
           FieldByName('shibazmnno').Asstring;
      DM_Kupa.AtmRxQuery_KabHescodemaslul.ASinteger:=
           FieldByName('Maslulcode1').ASinteger;
      DM_Kupa.AtmRxQuery_KabHesPratimHesbonit.AsString:=
           FieldByName('Maslul1').AsString;
      DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.AsFloat:=
           FieldByName('Price1').Asfloat;
      DM_Kupa.AtmRxQuery_KabHesKamutHesbonit.AsFloat:=
           FieldByName('Quntity1').Asfloat;
      DM_Kupa.AtmRxQuery_KabHesTotalSchum.Asfloat:=
           FieldByName('PriceQuntity1').Asfloat;
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
      DM_Kupa.AtmRxQuery_KabHesDateHesbonit.AsDateTime:=
           FieldByName('ShibAzmnDate').AsDateTime;
      DM_Kupa.AtmRxQuery_KabHes.Post;
      Next;
    End;
    DM_Kupa.AtmRxQuery_Kabala.Edit;
    DM_Kupa.CalcTotalHesbonit;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency:=
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat;
    DM_Kupa.AtmRxQuery_Kabala.Post;

    DM_Kupa.AtmRxQuery_KabHescodemaslul.OnValidate:=
       DM_Kupa.AtmRxQuery_KabHescodemaslulValidate;
    DM_Kupa.AtmRxQuery_KabHesKamutHesbonit.OnChange:=
       Dm_Kupa.AtmRxQuery_KabHesKamutHesbonitChange;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnChange:=
       DM_Kupa.AtmRxQuery_KabHesSchumHesbonitValidate;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnValidate:=
       DM_Kupa.AtmRxQuery_KabHesSchumHesbonitValidate;
    DM_Kupa.AtmRxQuery_KabHes.AfterPost:=
       DM_Kupa.AtmRxQuery_KabHesAfterPost;
  End;
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Update Atmshib');
    Sql.Add('Set LakCodeBizua=1');
    Sql.Add(',LakHesbonit='+DM_Kupa.AtmRxQuery_KabalaKabalaNumber.AsString);
    Sql.Add(',LakHesbonitDate=+'''+DateToSqlStr(DM_Kupa.AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add('Where shibazmnno>=' + IntToStr(ShibAzmn) );
    Sql.Add('And   shibazmnno<=' + IntToStr(ShibAzmnTo) );
    Sql.Add('And LakCodeBizua Is Null');    
    ExecSQL;
  End;
End;

Procedure TFrm_Kabala.BuildHesFromMlay;
Var
Hstr   : shortstring;
Begin
  DM_Kupa.AtmRxQuery_Kabala.Append;
  With DM_Kupa.Qry_Temp Do //SHIPMENT ����� ���� ����/�� ������� ����
  Begin
    Close;
    Sql.Clear;
//  Sql.Add('Select Lakno1,Maslulcode1,Maslul1,Quntity1,Price1,PriceQuntity1,L.Shem_lakoach,ShibAzmnDate,shibazmnno');
    Sql.Add('Select CodeLakoach,ShemLakoach,TotalPlusVat,TotalVat,MamPercent,HesbonChiuv');
    Sql.Add('From Shipment');
    Sql.Add('Where KabalaNumber='+IntToStr(HesFromMlay) );
    Sql.Add('And   YehusYear=' + IntToStr(YearFromMlay) );
//Sql.savetofile('c:\l.sql');
    Open;
    First;
    If DM_Kupa.Qry_Temp.FieldByName('HesbonChiuv').AsString='���'  Then
    Begin
       Kabala_type:=10;
       Label_CodeLakoach.Caption:='<���>';
    End
    Else
    Begin
       Kabala_type:=0;
       Label_CodeLakoach.Caption:='<����>';
    End;

    DM_Kupa.AtmRxQuery_KabalaCodeLakoach.OnValidate:=Nil;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').Asinteger:=
        DM_Kupa.Qry_Temp.FieldByName('CodeLakoach').AsInteger;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('ShemLakoach').Asstring:=
        DM_Kupa.Qry_Temp.FieldByName('ShemLakoach').Asstring;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('MamPercent').AsCurrency:=
        DM_Kupa.Qry_Temp.FieldByName('MamPercent').AsCurrency;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency:=
        DM_Kupa.Qry_Temp.FieldByName('TotalPlusVat').AsCurrency;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat:=
        DM_Kupa.Qry_Temp.FieldByName('TotalPlusVat').AsCurrency;
    DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat:=
        DM_Kupa.Qry_Temp.FieldByName('TotalVat').AsCurrency;
    DM_Kupa.AtmRxQuery_Kabala.Post;
    TotalBeforeMaam.Text:=FloattoStr(
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat-
        DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalVat').AsFloat);

    DM_Kupa.AtmRxQuery_KabalaCodeLakoach.OnValidate:=
        DM_Kupa.AtmRxQuery_KabalaCodeLakoachValidate;

    DM_Kupa.AtmRxQuery_KabHescodemaslul.OnValidate:=Nil;
    DM_Kupa.AtmRxQuery_KabHesKamutHesbonit.OnChange:=Nil;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnChange:=Nil;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnValidate:=Nil;
    DM_Kupa.AtmRxQuery_KabHes.AfterPost:=Nil;

    Close;
    Sql.Clear;
    Sql.Add('Select SchumHesbonit,DateHesbonit,KamutHesbonit,PratimHesbonit,TotalSchum,TotalMaam,CodeMaslul,FromNum,Tonum');
    Sql.Add('From Shiptnua');
    Sql.Add('Where KabalaNumber='+IntToStr(HesFromMlay) );
    Sql.Add('And   KabalaYear=' + IntToStr(YearFromMlay) );
    Open;
    First;

    While Not Eof Do
    Begin
      DM_Kupa.AtmRxQuery_KabHes.Append;
      DM_Kupa.AtmRxQuery_KabHesFromNum.Asstring:=
           FieldByName('FromNum').Asstring;
      DM_Kupa.AtmRxQuery_KabHesToNum.Asstring:=
           FieldByName('ToNum').Asstring;
      DM_Kupa.AtmRxQuery_KabHescodemaslul.ASinteger:=
           FieldByName('CodeMaslul').ASinteger;
      DM_Kupa.AtmRxQuery_KabHesPratimHesbonit.AsString:=
           FieldByName('PratimHesbonit').AsString;
      DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.AsFloat:=
           FieldByName('SchumHesbonit').Asfloat;
      DM_Kupa.AtmRxQuery_KabHesKamutHesbonit.AsFloat:=
           FieldByName('KamutHesbonit').Asfloat;
      DM_Kupa.AtmRxQuery_KabHesTotalSchum.Asfloat:=
           FieldByName('TotalSchum').Asfloat;
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
      DM_Kupa.AtmRxQuery_KabHesDateHesbonit.AsDateTime:=
          DM_Kupa.AtmRxQuery_Kabala.FieldByName('DateKabala').AsDateTime;
//         FieldByName('DateHesbonit').AsDateTime;
      DM_Kupa.AtmRxQuery_KabHesShipnum.AsString:=IntToStr(HesFromMlay);
      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalMaam').AsFloat:=
           FieldByName('TotalMaam').AsFloat;
      DM_Kupa.AtmRxQuery_KabHes.Post;
      Next;
      DM_Kupa.AtmRxQuery_KabHes.ApplyUpdates;
      DM_Kupa.AtmRxQuery_KabHes.Close;
      DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('KabalaNumber').AsInteger;
      DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_Kabala.FieldByName('YehusYear').AsInteger;
      DM_Kupa.AtmRxQuery_KabHes.Open;
    End;
//  DM_Kupa.AtmRxQuery_Kabala.Edit;
//  DM_Kupa.CalcTotalHesbonit;
//  DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalZikuy').AsCurrency:=
//  DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalPlusVat').AsFloat;
//  DM_Kupa.AtmRxQuery_Kabala.Post;

    DM_Kupa.AtmRxQuery_KabHescodemaslul.OnValidate:=
       DM_Kupa.AtmRxQuery_KabHescodemaslulValidate;
    DM_Kupa.AtmRxQuery_KabHesKamutHesbonit.OnChange:=
       Dm_Kupa.AtmRxQuery_KabHesKamutHesbonitChange;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnChange:=
       DM_Kupa.AtmRxQuery_KabHesSchumHesbonitValidate;
    DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.OnValidate:=
       DM_Kupa.AtmRxQuery_KabHesSchumHesbonitValidate;
    DM_Kupa.AtmRxQuery_KabHes.AfterPost:=
       DM_Kupa.AtmRxQuery_KabHesAfterPost;

  End;

End;

end.


object Frm_Kabala: TFrm_Kabala
  Left = 1
  Top = 117
  Width = 640
  Height = 428
  BiDiMode = bdRightToLeft
  Caption = '������� ��/����'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 632
    Height = 339
    ActivePage = TS_Main
    Align = alClient
    HotTrack = True
    Style = tsFlatButtons
    TabOrder = 0
    object TS_Main: TTabSheet
      Caption = '�-� ��/����'
      object Panel_Kabala: TPanel
        Left = 0
        Top = 0
        Width = 624
        Height = 57
        Align = alTop
        TabOrder = 0
        OnEnter = Panel_KabalaEnter
        OnExit = Panel_KabalaExit
        object Label1: TLabel
          Left = 559
          Top = 10
          Width = 62
          Height = 13
          Caption = '�� ��/����'
        end
        object Label2: TLabel
          Left = 435
          Top = 10
          Width = 33
          Height = 13
          Caption = '�����'
        end
        object Label_CodeLakoach: TLabel
          Left = 252
          Top = 10
          Width = 38
          Height = 13
          Caption = '<����>'
          OnDblClick = Label_CodeLakoachDblClick
        end
        object Lbl_zkut: TLabel
          Left = 554
          Top = 31
          Width = 70
          Height = 13
          Caption = '����� ������'
        end
        object Lbl_Chiuv: TLabel
          Left = 401
          Top = 31
          Width = 67
          Height = 13
          Caption = '����� �����'
        end
        object Lbl_masmakor: TLabel
          Left = 213
          Top = 31
          Width = 77
          Height = 13
          Hint = '����� �� �����'
          Caption = '���'#39' �� �����'
        end
        object Lbl_kodbitzua: TLabel
          Left = 82
          Top = 31
          Width = 53
          Height = 13
          Caption = '��� �����'
        end
        object AtmDbHEdit_KabalaNo: TAtmDbHEdit
          Left = 470
          Top = 6
          Width = 79
          Height = 21
          TabStop = False
          BiDiMode = bdRightToLeft
          DataField = 'KabalaNumber'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 1
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Label1
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = True
        end
        object AtmDBDateEdit1: TAtmDBDateEdit
          Left = 292
          Top = 6
          Width = 108
          Height = 21
          TabStop = False
          DataField = 'DateKabala'
          DataSource = DM_Kupa.DS_Kabala
          CheckOnExit = True
          DefaultToday = True
          DialogTitle = '����� ����'
          BiDiMode = bdLeftToRight
          ParentBiDiMode = False
          NumGlyphs = 2
          TabOrder = 2
          StartOfWeek = Sun
          Weekends = [Sat]
          LinkLabel = Label2
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          F9String = '26/01/2000'
          IsFixed = False
          FixedColor = clBtnFace
          StatusBarPanelNum = -1
        end
        object KabCodeLakoach: TAtmDbHEdit
          Left = 140
          Top = 5
          Width = 70
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'CodeLakoach'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 0
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Label_CodeLakoach
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SearchComponent = AtmAdvSearch_lakoach
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          BeforeExecuteSearch = KabCodeLakoachBeforeExecuteSearch
          LocateRecordOnChange = False
        end
        object AtmDbHEdit1: TAtmDbHEdit
          Left = 2
          Top = 5
          Width = 136
          Height = 21
          TabStop = False
          BiDiMode = bdRightToLeft
          Color = clInfoBk
          DataField = 'ShemLakoach'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 6
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object ChebZikuy: TAtmDbHEdit
          Left = 470
          Top = 28
          Width = 79
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'HesbonZikuy'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 3
          OnEnter = ChebZikuyEnter
          OnExit = ChebZikuyExit
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_zkut
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SearchComponent = AtmAdvSearchenZikuy
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          BeforeExecuteSearch = ChebZikuyBeforeExecuteSearch
          LocateRecordOnChange = False
        end
        object DBComboBox_HeshbonHiuv: TDBComboBox
          Left = 292
          Top = 28
          Width = 108
          Height = 21
          DataField = 'HesbonChiuv'
          DataSource = DM_Kupa.DS_Kabala
          ItemHeight = 13
          TabOrder = 4
        end
        object ChenMasMakor: TAtmDbHEdit
          Left = 140
          Top = 28
          Width = 70
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'HesbonMasMakor'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 5
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_masmakor
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object KodBitzou: TAtmDbHEdit
          Left = 2
          Top = 28
          Width = 78
          Height = 21
          TabStop = False
          AutoSize = False
          BiDiMode = bdRightToLeft
          DataField = 'KodBItzua'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 7
          OnChange = KodBitzouChange
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_kodbitzua
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 57
        Width = 624
        Height = 251
        Align = alClient
        TabOrder = 1
        TabStop = True
        object Label4: TLabel
          Left = 135
          Top = 97
          Width = 49
          Height = 13
          Caption = '��"�     %'
        end
        object Label5: TLabel
          Left = 101
          Top = 115
          Width = 84
          Height = 13
          Caption = '��"� ���� ��"�'
        end
        object Label7: TLabel
          Left = 293
          Top = 97
          Width = 82
          Height = 13
          Caption = '��"� ���� ��"�'
        end
        object Lbl_Pratim: TLabel
          Left = 585
          Top = 115
          Width = 32
          Height = 13
          Caption = '�����'
        end
        object RxDBGrid_KabHes: TRxDBGrid
          Left = 1
          Top = 1
          Width = 622
          Height = 88
          Align = alTop
          DataSource = DM_Kupa.DS_KabHes
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColEnter = RxDBGrid_KabHesColEnter
          OnDblClick = RxDBGrid_KabHesDblClick
          OnEnter = RxDBGrid_KabHesEnter
          OnExit = RxDBGrid_KabHesExit
          OnKeyDown = RxDBGrid_KabHesKeyDown
          OnGetCellParams = RxDBGrid_KabHesGetCellParams
          Columns = <
            item
              Expanded = False
              FieldName = 'codemaslul'
              Title.Caption = '��� ����'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PratimHesbonit'
              Title.Alignment = taCenter
              Title.Caption = '�����'
              Width = 243
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FromNum'
              Title.Caption = '�����'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ToNum'
              Title.Caption = '�� ����'
              Width = 67
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'KamutHesbonit'
              Title.Alignment = taCenter
              Title.Caption = '����'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DateHesbonit'
              Title.Alignment = taCenter
              Title.Caption = '�����'
              Width = 81
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SchumHesbonit'
              Title.Alignment = taCenter
              Title.Caption = '����'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalSchum'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = '��"�'
              Width = 80
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 1
          Top = 227
          Width = 622
          Height = 23
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 0
          object Label6: TLabel
            Left = 553
            Top = 4
            Width = 45
            Height = 13
            Caption = '�� ����'
          end
          object Label3: TLabel
            Left = 145
            Top = 4
            Width = 53
            Height = 13
            Caption = '�� ������'
          end
          object DBEdit_TotalKabala: TDBEdit
            Left = 455
            Top = 2
            Width = 88
            Height = 19
            TabStop = False
            Ctl3D = False
            DataField = 'TotalSumKabla'
            DataSource = DM_Kupa.DS_Kabala
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 51
            Top = 2
            Width = 88
            Height = 19
            TabStop = False
            Ctl3D = False
            DataField = 'TotalZikuy'
            DataSource = DM_Kupa.DS_Kabala
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 1
          end
        end
        object Panel1: TPanel
          Left = 1
          Top = 135
          Width = 622
          Height = 92
          Align = alBottom
          Caption = 'Panel1'
          TabOrder = 2
          object RxDBGrid_Checks: TRxDBGrid
            Left = 1
            Top = 1
            Width = 620
            Height = 90
            Align = alClient
            Ctl3D = True
            DataSource = DM_Kupa.DS_Checks
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnColEnter = RxDBGrid_ChecksColEnter
            OnEnter = RxDBGrid_ChecksEnter
            OnExit = RxDBGrid_ChecksExit
            OnKeyDown = RxDBGrid_ChecksKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'CodeSugTashlum'
                Title.Caption = '��� �����'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LookSugTashlum'
                ReadOnly = True
                Title.Caption = '����'
                Title.Color = clInfoBk
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SchumCheck'
                Title.Caption = '����'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DatePiraon'
                Title.Caption = '����� ������'
                Width = 81
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CheckNumber'
                Title.Caption = '����'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MisparCheshbon'
                Title.Caption = '���� �����'
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'KodBank'
                Title.Caption = '��� ���'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SnifNumber'
                Title.Caption = '����'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LookShemBank'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ReadOnly = True
                Title.Caption = '�� ���'
                Title.Color = clInfoBk
                Width = 91
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodeBankHafkada'
                Title.Caption = '��� ��� �����'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodeSnifHafkada'
                Title.Caption = '���� �����'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MisHeshbonHafkada'
                Title.Caption = '��'#39' ����� �����'
                Width = 99
                Visible = True
              end>
          end
        end
        object DBEdt_maam: TDBEdit
          Left = 4
          Top = 91
          Width = 88
          Height = 21
          DataField = 'TotalVat'
          DataSource = DM_Kupa.DS_Kabala
          TabOrder = 3
          OnEnter = DBEdt_maamEnter
          OnExit = DBEdt_maamExit
          OnKeyDown = DBEdt_maamKeyDown
        end
        object DBedt_totalPlusVat: TDBEdit
          Left = 4
          Top = 114
          Width = 88
          Height = 21
          TabStop = False
          DataField = 'TotalPlusVat'
          DataSource = DM_Kupa.DS_Kabala
          ReadOnly = True
          TabOrder = 4
        end
        object DBEdt_MamPercent: TDBEdit
          Left = 103
          Top = 93
          Width = 31
          Height = 21
          TabStop = False
          DataField = 'MamPercent'
          DataSource = DM_Kupa.DS_Kabala
          ReadOnly = True
          TabOrder = 5
          OnEnter = DBEdt_MamPercentEnter
          OnExit = DBEdt_MamPercentExit
          OnKeyDown = DBEdt_MamPercentKeyDown
        end
        object TotalBeforeMaam: TEdit
          Left = 193
          Top = 93
          Width = 89
          Height = 21
          TabStop = False
          ReadOnly = True
          TabOrder = 6
        end
        object Pratim: TAtmDbHEdit
          Left = 294
          Top = 113
          Width = 273
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Pratim'
          DataSource = DM_Kupa.DS_Kabala
          ParentBiDiMode = False
          TabOrder = 7
          UseF2ToRunSearch = False
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_Pratim
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 339
    Width = 632
    Height = 24
    Align = alBottom
    Caption = 'ToolBar1'
    Flat = True
    Images = ImageList1
    TabOrder = 1
    object DBNavigator1: TDBNavigator
      Left = 0
      Top = 0
      Width = 240
      Height = 22
      Flat = True
      ConfirmDelete = False
      TabOrder = 0
      BeforeAction = DBNavigator1BeforeAction
    end
    object ToolButton3: TToolButton
      Left = 240
      Top = 0
      Hint = '�-� ��/���� ����'
      Action = Act_NewKabala
      Caption = '�-� ��/���� ����'
    end
    object ToolButton1: TToolButton
      Left = 263
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 271
      Top = 0
      Action = Action_OpenSearch
    end
    object ToolButton4: TToolButton
      Left = 294
      Top = 0
      Action = Action_duplicateRecord
    end
    object Btn_Edkun: TBitBtn
      Left = 317
      Top = 0
      Width = 102
      Height = 22
      Action = Act_UpdateFiles
      Caption = '����� ������-F7'
      TabOrder = 1
    end
    object BitBtn2: TBitBtn
      Left = 419
      Top = 0
      Width = 134
      Height = 22
      Action = Act_PrintKabala
      Caption = '���� �� ��/����-F8'
      TabOrder = 2
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C
        00001F7C1F7C0000000000000000000000000000000000000000000000000000
        1F7C00001F7C00001F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F1F7C1F7C0000
        000000001F7C00001F7C1F7C1F7C1F7C1F7C1F7CEF3DEF3DEF3D1F7C1F7C0000
        1F7C00001F7C0000000000000000000000000000000000000000000000000000
        1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C
        00001F7C00001F7C00000000000000000000000000000000000000001F7C0000
        1F7C000000001F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001F7C
        00001F7C00001F7C1F7C1F7C0000FF7F00000000000000000000FF7F00000000
        000000001F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7F00000000000000000000FF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 363
    Width = 632
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object MainMenu1: TMainMenu
    Left = 448
    Top = 65525
    object N1: TMenuItem
      Caption = '����'
      object N8: TMenuItem
        Action = Act_ChangeYehusYear
        Caption = '����� ��� ����� '
      end
      object N2: TMenuItem
        Caption = '�����'
        OnClick = N2Click
      end
    end
    object N3: TMenuItem
      Caption = '������'
      object N4: TMenuItem
        Caption = '���� ��������/�����'
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = '������'
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = '�����'
        OnClick = N6Click
      end
      object N10: TMenuItem
        Caption = '������'
        OnClick = N9Click
      end
      object N7: TMenuItem
        Caption = '������'
        OnClick = N7Click
      end
      object N21: TMenuItem
        Caption = '������ 2'
        OnClick = N21Click
      end
    end
  end
  object AtmAdvSearch_lakoach: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'kod_lakoach'
      'shem_lakoach'
      'Mis_Han')
    ShowFieldsHeader.Strings = (
      '��� ����'
      '�� ����'
      '���� ����'#39'�')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150'
      '150')
    Sql.Strings = (
      'Select * From Lakoach')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_lakoach'
    KeepList = True
    EditBiDiMode = bdLeftToRight
    Left = 386
    Top = 65522
  end
  object ImageList1: TImageList
    Left = 306
    Top = 65522
    Bitmap = {
      494C01010A000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001001000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000F000F000F00000000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000000EF01EF01EF01EF01EF010000
      0000000000000F000000000000000F0000000000FF7FEF3DEF3DEF3DFF7F0000
      00000000FF7FEF3DEF3DEF3DFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000000EF0100000000000000000000
      00000000000000000000000000000F0000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000000EF0100000000000000000000
      0000000000000000000000000F000F000F000000FF7FEF3DEF3DEF3DFF7F0000
      00000000FF7FEF3DEF3DEF3DFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EF01000000001F001F001F00
      00000000000000000000000000000F0000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000EF0100000000000000000000
      0000000000000000000000000000000000000000FF7F00000000000000000000
      00000000FF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F000000000000EF3D00000000EF01000000001F001F001F00
      000000000000000000000000000000000000000000000000FF7FE07FFF7FE07F
      0000000000000000FF7FE07FFF7FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7F000000000000EF3D00000000EF0100000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3DEF3DEF3D000000000000000000001F001F001F00
      0000FF7F1F001F001F00FF7F0000000000000000000000000000000000000000
      000000000000000000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F00000000000000000000
      0000FF7F00000000000000000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000F0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0000FF7F1F001F001F00FF7F0000000000000000000000000F000F000F000000
      000000000000000000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F00000000000000000000
      FF7FFF7F0000FF7F000000000000EF3D00000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000F0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F000000000000EF3D00000000000000000000000000000000
      0000FF7F1F001F001F00FF7F00000000000000000000000000000F0000000F00
      00000F0000000F0000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7F0000FF7F
      FF03FF7F0000FF7FFF7FFF7F0000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7F0000FF03
      FF7FFF030000FF7FFF7FFF7F0000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DE07F000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FFF7FFF7F0000FF7F
      FF03FF7F0000FF7FFF7FFF7F0000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FFF7F0000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7F000000000000000000000000FF7F0000
      0000000000000000FF7F0000000000000000000000000000000000000000FF03
      FF7FFF0300000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FFF7F000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000FF7F
      FF03FF7F0000000000000000000000000000000000000F000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000FF7F00000000
      000000000000FF7F00000000000000000000000000000000000000000000FF03
      FF7FFF03000000000000000000000000000000000F000F00000000000000FF7F
      FF03FF7FFF03FF7FFF03FF7FFF03FF7FFF0300000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F00000000000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000FF7F
      FF03FF7F00000000000000000000000000000F000F000F000F000F000000FF03
      FF7FFF03FF7FFF03FF7FFF03FF7FFF03FF7F00000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF03
      FF7FFF03000000000000000000000000000000000F000F00000000000000FF7F
      FF03FF7FFF03FF7FFF03FF7FFF03FF7FFF0300000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000F000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0F00000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      EF3DEF3DEF3D0000000000000000000000000000000000000000000000000000
      0F00000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      E07FE07FE07F000000000000000000000000000000000000000000000F000F00
      0F000F000F000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000F00
      0F000F0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F000F000F000F000F000F00
      0F000F000F000F000F000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F0000000F000F0000000F00
      0F0000000F000F0000000F00000000000000000000000000000000000000EF3D
      EF3DEF3DEF3D0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F000F000F000F000F000F00
      0F000F000F000F000F000F00000000000000000000000000EF3DEF3D0000FF7F
      0000000000000000EF3DEF3D0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F00FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F0000000000000000000000EF3DFF7FFF7F00000000
      0000000000000000FF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F00FF7F00000000FF7F0000
      0000FF7F00000000FF7F0F0000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F00FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F0000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F003CFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000F000F000F000F000F000F00
      0F000F000F000F000F000F0000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F003C0000FF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7F003C00000000FF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000FF7F00000000
      FF7F00000000FF7F0000000000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7F003CEF3D0000FF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000EF3DFF7F003C003CFF7F
      003C003C0000FF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7F003C003C
      003C00000000FF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7FFF7F003C
      00000000FF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7FFF7F0000
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000E07FFF7F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000FF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000
      00000000FF7FEF3DEF3DEF3DEF3DEF3DEF3D0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F0000FF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000000000000000
      FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0F00FF7F0000000000000000000000000000
      00000000000000000000FF7F000000000000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000000000000000FF7F
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000000000000000FF7F00000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0F000F00FF7FFF7F0000FF7FFF03FF7FFF03FF7F
      FF03FF7FFF03FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F0000000000000000FF7FFF7FFF7F0000
      00000000000000000000FF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF03FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F000F000F000F000000000000000000FF0300000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      FF7F0000FF7F00000000FF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000FF03000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0F000F00FF7FFF7F0000FF7FFF03FF7FFF03FF7F
      FF03FF7FFF03FF7FFF03FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F000000000000FF7FFF7FFF7FFF7FFF7F
      FF7F00000000FF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0F00FF7FFF7F000000000000000000000000
      00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000
      00000000FF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7F0000FF7FFF7F0000FF7FFF7FFF7FEF3DEF3DEF3DFF7F
      FF7F0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FE07FE07FE07FFF7F
      FF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0F000F000F000F000F00FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000
      00000000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0F000F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000
      00000000000000000000FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7F0F00FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00EF3DFF7F000000000000FF7FEF3DEF3D
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FEF3DEF3DEF3DFF7FFF7FFF7FFF7FFF7F
      0000000000000000FF7F000000000000FF7FFF7FFF7FFF7F0000FF7FFF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7F000000000000FF7FFF7FFF7FFFFFFFFF00000000FFFBE007FF7FFF7F
      FFFBC003FF7FFF7FFFE0C303FF7FFF7FFFFBC183EF3DFF7F000BC0C3FF7FFF7F
      000FC04300000000000FC02300000000000FC033000000000007C01300000000
      8007C003000000008007C003000000008007C003000000008007E1C700000000
      A497FC3F000000008007FFFF00000000FFFFFFFFFFFFFFFFFEFFFFFF800FC007
      FC7FFF00800FBFEBF83FFF00800F0005FEFFFF00800F7E31FEFFFF00800F7E35
      F83FD800800F0006F83F9800800F7FEAF83F0000800F8014F83F9800800FC00A
      F83FD800800FE0018003FF008000E0078003FF008000F0078003FF008000F003
      8003FF00F000F803FFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FE03
      F557FFFF0000FE03F7FFFFFF0014FE03E3F7E00F0000FE03F7FFF3EF0000C003
      FFF7F9FF0000DE03C0C0FCFF000010030000FE7F00001E030000FCFF00141003
      0101F9FF00001F7F0101F3EF0000117D0101E00F00001F780101FFFF0008007D
      0101FFFF001C01DD0101FFFF000001E300000000000000000000000000000000
      000000000000}
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 242
    Top = 65522
    object Action_duplicateRecord: TAction
      Caption = '���� �����'
      Hint = '���� �����*'
      ImageIndex = 0
      ShortCut = 122
      OnExecute = Action_duplicateRecordExecute
      OnUpdate = Action_duplicateRecordUpdate
    end
    object Action_SumTotals: TAction
      Caption = '�������'
      Hint = '�������'
      ImageIndex = 1
    end
    object Action_OpenSearch: TAction
      Caption = '���� �����'
      Hint = '���� �����'
      ImageIndex = 2
      ShortCut = 113
      OnExecute = Action_OpenSearchExecute
      OnUpdate = Action_OpenSearchUpdate
    end
    object Action_SelectNextControl: TAction
      Caption = '���� ���'
      Hint = '���� ���'
      ShortCut = 117
    end
    object Action_SelectPrevControl: TAction
      Caption = '���� �����'
      Hint = '���� �����'
      ShortCut = 8309
    end
    object Act_ShowOpenHeshbonitForLak: TAction
      Caption = '��� �������� ������'
      Hint = '��� �������� ������ �����'
      ImageIndex = 3
    end
    object Act_SelectOneHeshbonitForClose: TAction
      Hint = '���� ������� ������'
      ImageIndex = 5
    end
    object Act_CloseHeshbonitSequence: TAction
      Hint = '���� �� �� ��������� �������'
      ImageIndex = 4
    end
    object Act_PrintKabala: TAction
      Caption = '���� ����-F8'
      ImageIndex = 7
      ShortCut = 119
      OnExecute = Act_PrintKabalaExecute
    end
    object Act_NewKabala: TAction
      Caption = '���� ����'
      Hint = '���� ����'
      ImageIndex = 6
      ShortCut = 123
      OnExecute = Act_NewKabalaExecute
    end
    object Act_ChangeYehusYear: TAction
      Caption = '����� ��� ����� �����'
      OnExecute = Act_ChangeYehusYearExecute
    end
    object Act_UpdateFiles: TAction
      Caption = '����� ������-F7'
      Hint = '����� ������-F7'
      ShortCut = 118
      OnExecute = Act_UpdateFilesExecute
    end
    object Act_CloseOpenHeshboniot: TAction
      Caption = '��� ��������'
      Hint = '��� ���� �������� ������ �"� ���� ������'
      ImageIndex = 9
    end
    object Act_SelectOnlyOpenKabalot: TAction
      Caption = '���� �������� ������'
    end
  end
  object AtmAdvSearch_KodBank: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla'
      'Teur_Tavla')
    ShowFieldsHeader.Strings = (
      '��� ���'
      '�� ���')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select * From KodTavla'
      'Where Sug_Tavla=101')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_KodBank'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_KodBankAfterExecute
    Left = 244
    Top = 116
  end
  object AtmAdvSearch_SugTashlum: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla'
      'Teur_Tavla')
    ShowFieldsHeader.Strings = (
      '��� ���'
      '�� ���')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select * From KodTavla'
      'Where Sug_Tavla=107')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_SugTashlum'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_SugTashlumAfterExecute
    Left = 398
    Top = 111
  end
  object AtmAdvSearch_BankForHafkada: TAtmAdvSearch
    Caption = '����� ����� ������'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Kod_Tavla2'
      'Teur_Tavla'
      'Teur_Tavla2')
    ShowFieldsHeader.Strings = (
      '���� �����'
      '�����'
      '��'#39' ���"�')
    ShowFieldsDisplayWidth.Strings = (
      '70'
      '150'
      '70')
    Sql.Strings = (
      'Select * from kodtvla2'
      'where Sug_Tavla=101')
    ReturnFieldIndex = -1
    SeparateChar = ';'
    IniSection = 'AtmAdvSearch_BankForHafkada'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_BankForHafkadaAfterExecute
    Left = 68
    Top = 116
  end
  object Scale1: TScale
    Active = True
    X_ScreenWidth = 640
    Y_ScreenHeight = 480
    Left = 313
    Top = 115
  end
  object AtmAdvSearch_Nehag: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'kod_Nehag'
      'shem_Nehag'
      'Mis_Han'
      'Mis_Oved')
    ShowFieldsHeader.Strings = (
      '��� ���'
      '�� ���'
      '���� ����'#39'�'
      '��'#39' �����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150'
      '100'
      '100')
    Sql.Strings = (
      'Select *'
      'from nehag')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Nehag'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 509
    Top = 65529
  end
  object AtmAdvSearch_Maslul: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Code_maslul'
      'name')
    ShowFieldsHeader.Strings = (
      '��� ����'
      '����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select Code_maslul,Name'
      'From Maslul'
      'Where sug_rehev1=1')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Maslul'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    AfterExecute = AtmAdvSearch_MaslulAfterExecute
    Left = 523
    Top = 127
  end
  object AtmAdvSearchenZikuy: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_Kupa.Qry_Temp
    ShowFields.Strings = (
      'Teur_Tavla'
      'Kod_Tavla')
    ShowFieldsHeader.Strings = (
      '�� '
      '��� ����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150')
    Sql.Strings = (
      'Select * From KodTavla'
      'Where Sug_Tavla=18')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearchenZikuy'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 164
    Top = 113
  end
  object AtmTabSheetBuild1: TAtmTabSheetBuild
    SetKeepGrid = False
    WebMode = False
    ScrFileName = 'Tsb_KAbhes1.scr'
    SqlFileName = 'Tsb_KAbhes1.sql'
    PageControl = PageControl1
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = Ds_tavla1
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForCutColor = 'CutColor'
    ParamsKeyForSelectedColor = 'SelectedColor'
    ParamsKeyForExportColor = 'ExportColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = RxQuery_Tavla
    BDEQuery = RxQuery_Tavla
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    ParamsKeyForMultiSelect = 'MultiSelect'
    ParamsKeyForRowSelect = 'RowSelect'
    EditBiDiMode = bdLeftToRight
    BeforeExecuteQuery = AtmTabSheetBuild1BeforeExecuteQuery
    Left = 441
    Top = 105
  end
  object RxQuery_Tavla: TRxQuery
    DatabaseName = 'DB_Kupa1'
    Macros = <>
    Left = 497
    Top = 105
  end
  object RxDBFilter1: TRxDBFilter
    Left = 545
    Top = 105
  end
  object Ds_tavla1: TDataSource
    DataSet = RxQuery_Tavla
    Left = 585
    Top = 105
  end
end

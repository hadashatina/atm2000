unit F_Hafkada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Db, DBTables, RxQuery, AtmRxQuery, Mask, ToolEdit,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, Spin, CurrEdit, ComCtrls, AdvSearch,
  dxfCheckBox,inifiles;

type
  TFrm_Hafkada = class(TForm)
    RxDBGrid_Checks: TRxDBGrid;
    Panel1: TPanel;
    Qry_ChecksForHafkada: TRxQuery;
    DS_ChecksForHafkada: TDataSource;
    Btn_StartChecksSql: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    CurrencyEdit_Cash: TCurrencyEdit;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Btn_DoHafkadot: TBitBtn;
    RG_HafkadaKind: TRadioGroup;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Edit_HKodBank: TEdit;
    Label4: TLabel;
    Edit_HKodSnif: TEdit;
    Lbl_HShemBank: TLabel;
    Lbl_HShemSnif: TLabel;
    Label7: TLabel;
    Edit_HHeshbonNo: TEdit;
    AtmAdvSearch_BankForHafkada: TAtmAdvSearch;
    Spb_SelectHBank: TSpeedButton;
    Btn_SelectAll: TBitBtn;
    Panel4: TPanel;
    CB_PrintHafkada: TdxfCheckBox;
    RG_PrintTo: TRadioGroup;
    Qry_AtmIndex: TQuery;
    UpdateSQL_AtmIndex: TUpdateSQL;
    Btn_Close: TBitBtn;
    Qry_ChecksForHafkadaTnua: TIntegerField;
    Qry_ChecksForHafkadaKabalaYear: TIntegerField;
    Qry_ChecksForHafkadaKabalaNumber: TIntegerField;
    Qry_ChecksForHafkadaCodeSugTashlum: TIntegerField;
    Qry_ChecksForHafkadaCheckNumber: TStringField;
    Qry_ChecksForHafkadaKodBank: TIntegerField;
    Qry_ChecksForHafkadaSnifNumber: TIntegerField;
    Qry_ChecksForHafkadaDatePiraon: TDateTimeField;
    Qry_ChecksForHafkadaSchumCheck: TBCDField;
    Qry_ChecksForHafkadaMisparCheshbon: TStringField;
    Qry_ChecksForHafkadaKodMatbea: TIntegerField;
    Qry_ChecksForHafkadaKamutShtarot: TIntegerField;
    Qry_ChecksForHafkadaDateHamara: TDateTimeField;
    Qry_ChecksForHafkadaSchumDollar: TBCDField;
    Qry_ChecksForHafkadaShuraKopa: TIntegerField;
    Qry_ChecksForHafkadaStatus: TIntegerField;
    Qry_ChecksForHafkadaSugStar: TIntegerField;
    Qry_ChecksForHafkadaSchumMatbea: TBCDField;
    Qry_ChecksForHafkadaMisparHafkada: TIntegerField;
    Qry_ChecksForHafkadaCodeBankHafkada: TIntegerField;
    Qry_ChecksForHafkadaMisHeshbonHafkada: TStringField;
    Qry_ChecksForHafkadaCodeSnifHafkada: TIntegerField;
    Qry_ChecksForHafkadaNumberOfPayment: TIntegerField;
    Qry_ChecksForHafkadaCreditKind: TIntegerField;
    Qry_ChecksForHafkadaValidMonth: TIntegerField;
    Qry_ChecksForHafkadaValidYear: TIntegerField;
    Qry_ChecksForHafkadaAprovalCode: TIntegerField;
    Qry_ChecksForHafkadaPhoneNumber: TStringField;
    Qry_ChecksForHafkadaDateKabala: TDateTimeField;
    Qry_ChecksForHafkadaCodeLakoach: TIntegerField;
    Qry_ChecksForHafkadaLookSugTashlum: TStringField;
    Qry_ChecksForHafkadaLookShemBank: TStringField;
    Label5: TLabel;
    Lbl_TotalCashInKopa: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Lbl_TotalChecksHafkada: TLabel;
    Btn_ChageYehusYear: TBitBtn;
    DateEdit_ChecksDate: TDateEdit;
    SpinEdit1: TSpinEdit;
    Label9: TLabel;
    Label10: TLabel;
    SpinEdit2: TSpinEdit;
    procedure Btn_StartChecksSqlClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Btn_DoHafkadotClick(Sender: TObject);
    procedure Spb_SelectHBankClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Btn_SelectAllClick(Sender: TObject);
    procedure Qry_ChecksForHafkadaAfterOpen(DataSet: TDataSet);
    procedure CurrencyEdit_CashChange(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure RxDBGrid_ChecksGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure Qry_ChecksForHafkadaBeforeOpen(DataSet: TDataSet);
    procedure RxDBGrid_ChecksCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure Btn_ChageYehusYearClick(Sender: TObject);
    procedure RG_HafkadaKindClick(Sender: TObject);
    procedure DateEdit_ChecksDateAcceptDate(Sender: TObject;
      var ADate: TDateTime; var Action: Boolean);
    procedure DateEdit_ChecksDateExit(Sender: TObject);
  private
    { Private declarations }
    HafkadaYehusYear :Word;
    Procedure DoOneHafkadaLine(HafkadaNum:LongInt);
    Procedure DoHafkadaMzoman(HafkadaNum:LongInt);
    Function  GetNextHafkadaNumber:LongInt;
    Procedure BuildChecksLookupList;
    Function  CalcTotalChecks :Real;
  public
    { Public declarations }
  end;

var
  Frm_Hafkada: TFrm_Hafkada;
  Atmini     : Tinifile;
implementation

uses AtmRutin, DMKupa,RptTofesHafkada, AtmConst;

{$R *.DFM}

procedure TFrm_Hafkada.Btn_StartChecksSqlClick(Sender: TObject);
Var
  TotalMzomanInKopa:Real;
begin
//  ShowMessage('Qry_ChecksForHafkada');
  With Qry_ChecksForHafkada Do
  Begin
    Close;
    Case RG_HafkadaKind.ItemIndex Of
      0: MacroByName('DateCondition').AsString:='Checks.DatePiraon <='''+DateToSqlStr(DateEdit_ChecksDate.Date,'mm/dd/yyyy')+'''';
      1: MacroByName('DateCondition').AsString:='Checks.DatePiraon >'''+DateToSqlStr(DateEdit_ChecksDate.Date,'mm/dd/yyyy')+'''';
    End;//Case;
    ParamByName('PYehusYear').AsInteger:=HafkadaYehusYear;
    ParamByName('Pmatbea').AsInteger:=Kabala_type;
    Open;
  End;

//  ShowMessage('Select Schom From Kopa1');
  if RG_HafkadaKind.ItemIndex=0 Then
  //�������
  With DM_Kupa.Qry_Temp DO
  Begin
    Close;
    Sql.Clear;
//    Sql.Add('Select TotalKnisot-TotalHafkadot Cash From Itrot Where SugMatbea=0');

    If Kabala_type mod 10=0 Then
      Begin
        Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanIn));
        Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
      End
    Else
      Begin
        Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanInDolar));
        Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
      End;
    Open;
    TotalMzomanInKopa:=FieldByName('Schom').AsFloat;
    Close;

//    ShowMessage('Select Schom From Kopa2');
    Sql.Clear;
    If Kabala_type mod 10=0 Then
    Begin
      Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOut));
      Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
    End
    Else
    Begin
      Sql.Add('Select Schom From Kopa Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOutDolar));
      Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
    End;
    Open;

    if (Eof And BOF) Then
    Begin
      Close;
      Sql.Clear;

      Sql.Add('Insert Into KOPA (MisparKabala,Schom,MisparCheck,YehusYear)');
      If Kabala_type mod 10=0 Then
        Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanOut)+',0,'+'''������ �������'''+','+IntToStr(HafkadaYehusYear)+')')
//      Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanOut)+',0,''������ �������'')');
      Else
//      Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanOutDolar)+',0,''������ ������� �����'')');
        Sql.Add('Values ('+IntToStr(KopaKabalaNumForMezomanOutDolar)+',0,'+'''������ ������� �����'''+','+IntToStr(HafkadaYehusYear)+')');
      Try
// SQl.SaveToFile('c:\11.Sql');
//        ShowMessage('Insert Into KOPA');
        ExecSQL;
      Except On E:Exception Do
        ShowMessage('�� ���� ������ ���� ����� ������ ������� '+E.Message);
      End;
    End
    Else
      TotalMzomanInKopa:=TotalMzomanInKopa+FieldByName('Schom').AsFloat;
    Close;

    Try
//      if Not Eof And Bof Then
        CurrencyEdit_Cash.Value:=TotalMzomanInKopa;
        CurrencyEdit_Cash.MaxValue:=TotalMzomanInKopa;
        Lbl_TotalCashInKopa.Caption:=FloatToStr(TotalMzomanInKopa);
    Finally
      Close;
    End;
  End //With
  Else
  Begin
    CurrencyEdit_Cash.MaxValue:=0;
    CurrencyEdit_Cash.Value:=0;
    Lbl_TotalCashInKopa.Caption:='�� ����';
  End;
  CurrencyEdit_Cash.ReadOnly:=(RG_HafkadaKind.ItemIndex=1) Or (CurrencyEdit_Cash.Value=0);
end;

procedure TFrm_Hafkada.FormDestroy(Sender: TObject);
Var
  I :LongInt;
begin

  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+
  'Atmcfg.ini');
  Atmini.WriteInteger('Hafkada','TopLines',Spinedit1.Value);
  Atmini.WriteInteger('Hafkada','NumOfCopys',Spinedit2.Value);  
  Atmini.free;

  For I:=0 To ComponentCount-1 Do
    if Components[i] is TDataSet Then
      TDataSet(Components[i]).Close;
end;

procedure TFrm_Hafkada.Btn_DoHafkadotClick(Sender: TObject);
Var
  CurHafkadaNum:LongInt;

begin
  Screen.Cursor:=crSqlWait;
  Try
    Qry_ChecksForHafkada.Database.StartTransaction;
    Try
      CurHafkadaNum:=GetNextHafkadaNumber;

      While RxDBGrid_Checks.SelectedRows.Count>0 Do
      Begin
        Qry_ChecksForHafkada.GotoBookmark(TBookmark(RxDBGrid_Checks.SelectedRows[0]));
        Application.ProcessMessages;
        DoOneHafkadaLine(CurHafkadaNum);
        RxDBGrid_Checks.ToggleRowSelection;
      End;
      if CurrencyEdit_Cash.Value>0 Then
        DoHafkadaMzoman(CurHafkadaNum);
    Except
      Qry_ChecksForHafkada.Database.RollBack;
      Raise;
    End;
    Qry_ChecksForHafkada.Database.Commit;
    Qry_ChecksForHafkada.Close;
//    Qry_ChecksForHafkada.Open;
    Btn_StartChecksSqlClick(Nil); //Refresh Query
    if CB_PrintHafkada.Checked Then
      PrintHafkada(CurHafkadaNum,HafkadaYehusYear,SpinEdit1.Value,SpinEdit2.Value,(RG_PrintTo.ItemIndex=0),Lbl_HShemBank.Caption,DateEdit_ChecksDate.Text,Edit_HKodSnif.Text,Edit_HHeshbonNo.Text);
//    PrintHafkada(CurHafkadaNum,HafkadaYehusYear,(RG_PrintTo.ItemIndex=0),Lbl_HShemBank.Caption,DateEdit_ChecksDate.Text,Edit_HKodSnif.Text,Edit_HHeshbonNo.Text);

    Lbl_TotalChecksHafkada.Caption:=FloatToStr(CalcTotalChecks);
  Finally
    Screen.Cursor:=crDefault;
  End;
end;

Procedure TFrm_Hafkada.DoOneHafkadaLine(HafkadaNum:LongInt);
Begin
  //����� ���� ����� ���
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Update Checks Set MisparHafkada='+IntToStr(HafkadaNum));
    Sql.Add('Where Tnua='+Qry_ChecksForHafkada.FieldByName('Tnua').AsString);
    ExecSql;
  End;

  //���� � ���� ����
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Insert Into Kopa');
    Sql.Add('(Misparkabala,TarichKabala,CodeSugTashlum,MisparCheck,KodBank,Snif,TarichPeraon,Schom,MisparChesbon,KodMatbea,TarichHafkada,ChesbonBankHafkada,KodBankHafkada,SnifHafkada,MisparHafkada,SugHafkada,CodeLakoach,KodBitzua,YehusYear,CodeHaavaraToHan)');
    Sql.Add('Values');
    Sql.Add('('+Qry_ChecksForHafkada.FieldByName('KabalaNumber').AsString);
    Sql.Add(','''+DateToSqlStr(DateEdit_ChecksDate.Date,'mm/dd/yyyy')+'''');
//    Sql.Add(','''+DateToSqlStr(Qry_ChecksForHafkada.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','+Qry_ChecksForHafkada.FieldByName('CodeSugTashlum').AsString);
    Sql.Add(','''+Qry_ChecksForHafkada.FieldByName('CheckNumber').AsString+'''');
    Sql.Add(','+Qry_ChecksForHafkada.FieldByName('KodBank').AsString);
    Sql.Add(','+Qry_ChecksForHafkada.FieldByName('SnifNumber').AsString);
    Sql.Add(','''+DateToSqlStr(Qry_ChecksForHafkada.FieldByName('DatePiraon').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(',-'+Qry_ChecksForHafkada.FieldByName('SchumCheck').AsString);
    Sql.Add(','''+Qry_ChecksForHafkada.FieldByName('MisparCheshbon').AsString+'''');
    Sql.Add(','+Qry_ChecksForHafkada.FieldByName('KodMatbea').AsString);
    Sql.Add(','''+DateToSqlStr(DateEdit_ChecksDate.Date,'mm/dd/yyyy')+'''');//����� �����
    Sql.Add(','''+Edit_HHeshbonNo.Text+'''');//�-� ��� ������
    Sql.Add(','+Edit_HKodBank.Text);//��� ��� ������
    Sql.Add(','+Edit_HKodSnif.Text);//���� ������
    Sql.Add(','+IntToStr(HafkadaNum));//��, �����
    Sql.Add(','+IntToStr(RG_HafkadaKind.ItemIndex));//��� �����
    Sql.Add(','+Qry_ChecksForHafkada.FieldByName('CodeLakoach').AsString);
    Sql.Add(',1');//��� �����
    Sql.Add(','+IntToStr(HafkadaYehusYear)); //��� ����
    Sql.Add(',0)');//��� ����� ����"�
//    SQl.SaveToFile('c:\1.Sql');
    ExecSql;
  End;
End;

procedure TFrm_Hafkada.Spb_SelectHBankClick(Sender: TObject);
Var
  StrH:String;
begin
  if AtmAdvSearch_BankForHafkada.Execute Then
  Begin
    StrH:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,2);//Teur_Tavla
    Edit_HKodBank.Text:=GetFieldFromSeparateString(StrH,',',1);
    Edit_HKodSnif.Text:=GetFieldFromSeparateString(StrH,',',2);
    Lbl_HShemSnif.Caption:=GetFieldFromSeparateString(StrH,',',3);
    Edit_HHeshbonNo.Text:=GetFieldFromSeparateString(AtmAdvSearch_BankForHafkada.ReturnString,AtmAdvSearch_BankForHafkada.SeparateChar,1);//Teur_Tavla

    With DM_Kupa.Qry_Temp DO
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Teur_Tavla From KodTavla');
      Sql.Add('Where Sug_Tavla='+IntToStr(SugTavla_Bankim));
      Sql.Add('And Kod_Tavla='+Edit_HKodBank.Text);
      Open;
      if Not (Eof And BOF) Then
        Lbl_HShemBank.Caption:=FieldByName('Teur_Tavla').AsString;
      Close;
    End;
  End;
end;

Procedure TFrm_Hafkada.DoHafkadaMzoman(HafkadaNum:LongInt);
Var
  CurHafkadot:Real;
Begin
  //���� � ���� ����
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Insert Into Kopa');
    Sql.Add('(Misparkabala,CodeSugTashlum,Schom,KodMatbea,TarichKabala,TarichHafkada,ChesbonBankHafkada,KodBankHafkada,SnifHafkada,MisparHafkada,SugHafkada,KodBitzua,CodeLakoach,YehusYear,CodeHaavaraToHan)');
    Sql.Add('Values');
    Sql.Add('(0');
    Sql.Add(',1'); //�����
    Sql.Add(',-'+FloatToStr(CurrencyEdit_Cash.Value));
    If Kabala_type mod 10=0 Then
       Sql.Add(',0')  //��� ����
    Else
       Sql.Add(',2');  //��� ���� �����
    Sql.Add(','''+DateToSqlStr(DateEdit_ChecksDate.Date,'mm/dd/yyyy')+'''');//����� ����
    Sql.Add(','''+DateToSqlStr(DateEdit_ChecksDate.Date,'mm/dd/yyyy')+'''');//����� �����
    Sql.Add(','''+Edit_HHeshbonNo.Text+'''');//�-� ��� ������
    Sql.Add(','+Edit_HKodBank.Text);//��� ��� ������
    Sql.Add(','+Edit_HKodSnif.Text);//���� ������
    Sql.Add(','+IntToStr(HafkadaNum));//��, �����
    Sql.Add(','+IntToStr(RG_HafkadaKind.ItemIndex));//��� �����
    Sql.Add(',1');//��� �����
    Sql.Add(',0'); //��� ����
    Sql.Add(','+IntToStr(HafkadaYehusYear)); //��� ����
    Sql.Add(',0)');//��� ����� ����"�
    ExecSql;
  End;



  With DM_Kupa.Qry_Temp Do //����� ���� �����
  Begin
    Sql.Clear;//����� ��"� ������ �����
    If Kabala_type Mod 10=0 Then
    Begin
      Sql.Add('Select Schom From KOPA Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOut));
      Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
    End
    Else
    Begin
      Sql.Add('Select Schom From KOPA Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOutDolar));
      Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
    End;
    Open;
    CurHafkadot:=Abs(FieldByName('Schom').AsFloat);
    Close;

    Sql.Clear;//����� ���� �����
    Sql.Add('Update KOPA Set Schom = -'+FloatToStr(CurHafkadot+CurrencyEdit_Cash.Value));
    If Kabala_type Mod 10=0 Then
    Begin
      Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOut));
      Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear));
    End
    Else
      Sql.Add('Where MisparKabala = '+IntToStr(KopaKabalaNumForMezomanOutDolar));
      Sql.Add('And YehusYear= '+IntToStr(HafkadaYehusYear)) ;
    ExecSql;
  End;
End;

procedure TFrm_Hafkada.FormShow(Sender: TObject);
begin
  Spb_SelectHBankClick(Nil);
end;

procedure TFrm_Hafkada.Btn_SelectAllClick(Sender: TObject);
begin
  RxDBGrid_Checks.SelectAll;
end;

procedure TFrm_Hafkada.Qry_ChecksForHafkadaAfterOpen(DataSet: TDataSet);
begin
  Btn_DoHafkadot.Enabled:=(Not (Qry_ChecksForHafkada.Eof And Qry_ChecksForHafkada.Bof)) Or
                          (CurrencyEdit_Cash.Value>0);
end;

procedure TFrm_Hafkada.CurrencyEdit_CashChange(Sender: TObject);
begin
  Btn_DoHafkadot.Enabled:=(Not (Qry_ChecksForHafkada.Eof And Qry_ChecksForHafkada.Bof)) Or
                          (CurrencyEdit_Cash.Value>0);
end;

Function TFrm_Hafkada.GetNextHafkadaNumber:LongInt;
Var
  Res :LongInt;
Begin
  Result:=0;

  With Qry_AtmIndex Do
  Begin
    ParamByName('PYehusYear').AsInteger:=HafkadaYehusYear;
    Open;
    if Eof And Bof Then //��� ����
    Begin
      Insert;
      FieldByName('Last_Number').AsInteger:=1;
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=aiHafkada;
      FieldByName('YehusYear').AsInteger:=HafkadaYehusYear;
      Post;
      Result:=1;
    End
    Else
    Begin//�����
      Res:=FieldByName('Last_Number').AsInteger+1;
      Edit;
      FieldByName('Last_Number').AsInteger:=Res;
      Post;
      Result:=Res;
    End;
    Qry_AtmIndex.ApplyUpdates;
    Close;
  End;
  StatusBar1.SimpleText:='����� ���� '+IntToStr(Result);
End;
procedure TFrm_Hafkada.Btn_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrm_Hafkada.RxDBGrid_ChecksGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if RxDBGrid_Checks.SelectedRows.CurrentRowSelected Then
    BackGround:=clRed;
end;

Procedure TFrm_Hafkada.BuildChecksLookupList;
Var
   SList:TStringList;
   I :LongInt;
Begin
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select * From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_SugTashlum));
    Sql.Add('Or Sug_Tavla = '+IntToStr(SugTavla_Bankim));
    Open;
    SList:=TStringList.Create;
    SList.AddObject(IntToStr(SugTavla_SugTashlum),Qry_ChecksForHafkadaLookSugTashlum);
    SList.AddObject(IntToStr(SugTavla_Bankim),Qry_ChecksForHafkadaLookShemBank);
    FillKodTavlaLookupList(DM_Kupa.Qry_Temp,SList);
    SList.Free;
    Close;
  End;

End;

procedure TFrm_Hafkada.Qry_ChecksForHafkadaBeforeOpen(DataSet: TDataSet);
begin
  BuildChecksLookupList;
end;

Function  TFrm_Hafkada.CalcTotalChecks :Real;
Var
  I :longInt;
  R :Real;
  BM :TBookmark;
Begin
  R:=0;
  BM:=Qry_ChecksForHafkada.GetBookmark;
  Qry_ChecksForHafkada.DisableControls;
  For I:=0 To RxDBGrid_Checks.SelectedRows.Count -1 Do
  Begin
    Qry_ChecksForHafkada.GotoBookmark(TBookmark(RxDBGrid_Checks.SelectedRows[I]));
    R:=R+Qry_ChecksForHafkada.FieldByName('SchumCheck').AsFloat;
  End;
  Qry_ChecksForHafkada.Gotobookmark(BM);
  Qry_ChecksForHafkada.EnableControls;
  Result:=R;
End;

procedure TFrm_Hafkada.RxDBGrid_ChecksCellClick(Column: TColumn);
begin
  Lbl_TotalChecksHafkada.Caption:=FloatToStr(CalcTotalChecks);
end;

procedure TFrm_Hafkada.FormCreate(Sender: TObject);
Var
  MM,DD                : Word;
  Hstr                 : Shortstring ;
  TopLines,NumOfCopys  : Integer;

begin

  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  TopLines:=Atmini.readInteger('Hafkada','TopLines',10);
  NumOfCopys:=Atmini.readInteger('Hafkada','NumOfCopys',1);
  Spinedit1.Value:=TopLines;
  Spinedit2.Value:=NumOfCopys;
  Atmini.free;

  DecodeDate(DateEdit_ChecksDate.Date,HafkadaYehusYear,MM,DD);
  If Kabala_type Mod 10=0 Then
    Begin
    CurrencyEdit_Cash.DisplayFormat:='� ,0.00;-� ,0.00';
    HStr:='������ ';
    End
  Else
    Begin
    CurrencyEdit_Cash.DisplayFormat:='$ ,0.00;-$ ,0.00';
    HStr:='����� ';
    End;
  Caption:='������ '+Hstr+'��� '+IntToStr(HafkadaYehusYear);
end;

procedure TFrm_Hafkada.Btn_ChageYehusYearClick(Sender: TObject);
Var
  StrH :String;
  IntH :longInt;
begin
  StrH:=IntToStr(HafkadaYehusYear);
  if InputQuery('��� �����','��� ��� �����',StrH) Then
  Begin
    Try
      IntH:=StrToInt(StrH);
      if (IntH>2100) Or (IntH<1900) Then
        StrH:='Error';
      HafkadaYehusYear:=StrToInt(StrH);
      Caption:='������ ���� '+StrH;
    Except
      MessageDlg('��� ����� �����',mtError,[mbOk],0);
    End;
  End;
end;

procedure TFrm_Hafkada.RG_HafkadaKindClick(Sender: TObject);
begin
  if Qry_ChecksForHafkada.Active Then
    Btn_StartChecksSqlClick(Btn_StartChecksSql);
end;

procedure TFrm_Hafkada.DateEdit_ChecksDateAcceptDate(Sender: TObject;
  var ADate: TDateTime; var Action: Boolean);
begin
  if Qry_ChecksForHafkada.Active Then
    Btn_StartChecksSqlClick(Btn_StartChecksSql);
end;

procedure TFrm_Hafkada.DateEdit_ChecksDateExit(Sender: TObject);
begin
  if Qry_ChecksForHafkada.Active Then
    Btn_StartChecksSqlClick(Btn_StartChecksSql);
end;

end.

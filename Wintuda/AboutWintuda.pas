unit AboutWinTuda;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, GoToWeb,Scale250,  RXCtrls,AtmRutin,AtmConst, GIFImage;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    OKButton: TBitBtn;
    HebLabel9: TLabel;
    Panel2: TPanel;
    HebLabel1: TLabel;
    Lbl_Ver: TLabel;
    HebLabel3: TLabel;
    HebLabel4: TLabel;
    HebLabel5: TLabel;
    HebLabel10: TLabel;
    HebLabel6: TLabel;
    HebLabel7: TLabel;
    GoToWeb1: TGoToWeb;
    Scale1: TScale;
    SecretPanel1: TSecretPanel;
    Lbl_CrtVer: TLabel;
    GIFImage1: TGIFImage;
    procedure HebLabel7Click(Sender: TObject);
    procedure HebLabel7MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Panel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure HebLabel6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

{$R *.DFM}

procedure TAboutBox.HebLabel7Click(Sender: TObject);
begin
     {WinExec('Start www.atm.co.il/index.html',0);}
     GotoWeb1.Execute;
end;

procedure TAboutBox.HebLabel7MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
     Screen.Cursor:=CrHandPoint;
end;

procedure TAboutBox.Panel1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
     Screen.Cursor:=crDefault;
end;

procedure TAboutBox.FormCreate(Sender: TObject);
Var
   Ix1    :Integer;
begin
     Lbl_Ver.Caption:=Lbl_Ver.Caption+Version_WinTuda;
     Lbl_CrtVer.Caption:=Lbl_CrtVer.Caption+Version_AtmCrt;
     For Ix1:=0 To ComponentCount-1 Do
     Begin
         if Components[Ix1] is TLabel Then
         Begin
             (Components[Ix1] As TLabel).Autosize:=False;
             (Components[Ix1] As TLabel).Autosize:=True;
         End;
     End;
end;

procedure TAboutBox.HebLabel6Click(Sender: TObject);
begin
     GotoWeb1.SendEmail;
end;

end.


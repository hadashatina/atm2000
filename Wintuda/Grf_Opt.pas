unit Grf_Opt;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, Spin, Grids, DBGrids, HebForm;

type
  TGrf_options = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Pnl_Gnrl: TPanel;
    PCn_Options: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Cbx_MajorScale: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    SpE_Mafor: TSpinEdit;
    DBGrid1: TDBGrid;
    Label5: TLabel;
    SpE_Length: TSpinEdit;
    HebForm1: THebForm;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Grf_options: TGrf_options;

implementation
  uses Grf_Data,UniDataModule1;
{$R *.DFM}

procedure TGrf_options.FormCreate(Sender: TObject);
begin
  Cbx_MajorScale.text:='���';
  SpE_Mafor.Value:=2;
  SpE_Length.Value:=100;
end;
end.

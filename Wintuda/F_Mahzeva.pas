unit F_Mahzeva;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, ToolWin, ComCtrls, Mask, ExtCtrls, DBTables,
  AtmAdvTable, AtmComp, AdvSearch, AtmLookCombo, RxQuery, AtmRxQuery;

type
  TFrm_Mahzeva = class(TForm)
    Tbl_MahzevaCodeMahzeva: TIntegerField;
    Tbl_MahzevaGroupMahzeva: TIntegerField;
    Tbl_MahzevaNameMahzeva: TStringField;
    Tbl_MahzevaAddress: TStringField;
    Tbl_MahzevaCity: TStringField;
    Tbl_MahzevaZipCode: TIntegerField;
    Tbl_MahzevaPhone1: TStringField;
    Tbl_MahzevaPhone2: TStringField;
    Tbl_MahzevaFax: TStringField;
    Tbl_MahzevaDiscountPercent: TBCDField;
    Tbl_MahzevaMasBamakor: TBCDField;
    Tbl_MahzevaTnaeyTashlum: TIntegerField;
    Tbl_MahzevaMisHan: TStringField;
    Tbl_MahzevaCodeMehiron: TIntegerField;
    Tbl_MahzevaLastUpdate: TDateTimeField;
    Tbl_MahzevaMaklidName: TStringField;
    DS_Mahzeva: TDataSource;
    Tbl_Mahzeva: TAtmRxQuery;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    Panel_Main: TPanel;
    Lbl_CodeMahzeva: TLabel;
    Lbl_GroupMahzeva: TLabel;
    Lbl_NameMahzeva: TLabel;
    Lbl_Address: TLabel;
    Lbl_City: TLabel;
    Lbl_ZipCode: TLabel;
    Lbl_Phone: TLabel;
    Lbl_Phone2: TLabel;
    Lbl_Fax: TLabel;
    Lbl_DiscountPercent: TLabel;
    Lbl_MasBamakor: TLabel;
    Lbl_TnaeyTashlum: TLabel;
    Lbl_MisHan: TLabel;
    Lbl_CodeMehiron: TLabel;
    Lbl_LastUpdate: TLabel;
    Lbl_MaklidName: TLabel;
    EditCodeMahzeva: TAtmDbHEdit;
    EditGroupMahzeva: TAtmDbHEdit;
    EditNameMahzeva: TAtmDbHEdit;
    EditAddress: TAtmDbHEdit;
    EditCity: TAtmDbHEdit;
    EditZipCode: TAtmDbHEdit;
    EditPhone: TAtmDbHEdit;
    EditPhone2: TAtmDbHEdit;
    EditFax: TAtmDbHEdit;
    EditDiscountPercent: TAtmDbHEdit;
    EditMasBamakor: TAtmDbHEdit;
    EditMisHan: TAtmDbHEdit;
    EditCodeMehiron: TAtmDbHEdit;
    EditLastUpdate: TAtmDbHEdit;
    EditMaklidName: TAtmDbHEdit;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    AtmAdvSearch_Mahzeva: TAtmAdvSearch;
    Tbl_MahzevaString_1: TStringField;
    Tbl_MahzevaString_2: TStringField;
    Tbl_MahzevaString_3: TStringField;
    Tbl_MahzevaString_4: TStringField;
    Tbl_MahzevaString_5: TStringField;
    Tbl_MahzevaInteger_1: TIntegerField;
    Tbl_MahzevaInteger_2: TIntegerField;
    Tbl_MahzevaInteger_3: TIntegerField;
    Tbl_MahzevaInteger_4: TIntegerField;
    Tbl_MahzevaInteger_5: TIntegerField;
    Tbl_MahzevaDate_1: TDateTimeField;
    Tbl_MahzevaDate_2: TDateTimeField;
    Tbl_MahzevaDate_3: TDateTimeField;
    Tbl_MahzevaDate_4: TDateTimeField;
    Tbl_MahzevaDate_5: TDateTimeField;
    Tbl_MahzevaBcd_1: TBCDField;
    Tbl_MahzevaBcd_2: TBCDField;
    Tbl_MahzevaBcd_3: TBCDField;
    Tbl_MahzevaBcd_4: TBCDField;
    Tbl_MahzevaBcd_5: TBCDField;
    Tbl_MahzevaX: TIntegerField;
    Tbl_MahzevaY: TIntegerField;
    Tbl_MahzevaNodeNumB: TIntegerField;
    Tbl_MahzevaNodeSugSystem: TIntegerField;
    Tbl_MahzevaNodeKodCityName: TIntegerField;
    AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox;
    UpdateSQL_Mahzeva: TUpdateSQL;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Tbl_MahzevaAfterInsert(DataSet: TDataSet);
    procedure Tbl_MahzevaAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure EditCodeMahzevaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DS_MahzevaStateChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    Procedure ShowForm(FormParams:ShortString);
    Procedure BuildLookupList;
  end;

var
  Frm_Mahzeva: TFrm_Mahzeva;

implementation

uses DMAtmCrt, AtmRutin, AtmConst, Crt_Glbl;

{$R *.DFM}
Const
     PnlNum_DSState=0;

procedure TFrm_Mahzeva.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     BuildLookupList;
     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataset) And (Components[i].Tag=1)Then
             (Components[I] As TDataset).Open
          Else
              if (Components[I] is TAtmAdvSearch) Then
                  TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_Mahzeva.Tbl_MahzevaAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Panel_Main).SetFocus;
end;

procedure TFrm_Mahzeva.Tbl_MahzevaAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_Mahzeva.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_Mahzeva:=Nil;
     Action:=caFree;
end;

procedure TFrm_Mahzeva.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataSet).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

Procedure TFrm_Mahzeva.ShowForm(FormParams:ShortString);
Begin
     if FormParams<>'' Then
          Tbl_Mahzeva.FindKey([FormParams])
     Else
       SetDatasetState(Tbl_Mahzeva,CrtFirstTableState);
     Show;
End;

procedure TFrm_Mahzeva.EditCodeMahzevaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Mahzeva);
end;

Procedure TFrm_Mahzeva.BuildLookupList;
var
   SList :TStringList;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_TnaeyTashlum),AtmDbComboBox_TnaeyTashlum);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;

procedure TFrm_Mahzeva.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet);
     if Button=nbRefresh Then
     Begin
        (Sender As TDBNavigator).DataSource.DataSet.Close;
        (Sender As TDBNavigator).DataSource.DataSet.Open;
        BuildLookupList;
        Abort;
     End;
end;

procedure TFrm_Mahzeva.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Mahzeva);
end;

procedure TFrm_Mahzeva.DS_MahzevaStateChange(Sender: TObject);
begin
  StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

end.

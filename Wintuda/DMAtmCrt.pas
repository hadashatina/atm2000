{ 16/03/99 11:20:05 > [Aviran on AVIRAN] check in: (1.0)  / None }
unit DMAtmCrt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, ActnList, ImgList, Db, AdvSearch,AtmComp, AppEvent, AtmAdvTable,
  AtmRutin,Crt_Glbl,AtmConst,DynamicControls, RxQuery, AtmRxQuery,ComCtrls,DBGrids;
 
type
  TDM_AtmCrt = class(TDataModule)
    Database_AtmCrt: TDatabase;
    ImageList_CrtButtons: TImageList;
    Crt_GlobalActionList: TActionList;
    Action_OpenSearch: TAction;
    AtmAdvSearch_KodTavla: TAtmAdvSearch;
    Query_KodTavla: TQuery;
    AppEvents1: TAppEvents;
    Qry_Temp: TQuery;
    Action_OpenDlgSearchPlace: TAction;
    Action_OpenSearchForm: TAction;
    Action_DupRecord: TAction;
    Act_ChooseDynamicControlFile: TAction;
    Act_EditExtraFields: TAction;
    ImageList_Boolean: TImageList;
    Tbl_KodTavla: TAtmRxQuery;
    procedure Action_OpenSearchExecute(Sender: TObject);
    procedure Action_OpenSearchUpdate(Sender: TObject);
    procedure AppEvents1ActiveControlChange(Sender: TObject);
    procedure DM_AtmCrtCreate(Sender: TObject);
    procedure DM_AtmCrtDestroy(Sender: TObject);
    procedure Tbl_KodTavlaTableChange;
    procedure Action_OpenDlgSearchPlaceExecute(Sender: TObject);
    procedure Action_OpenSearchFormExecute(Sender: TObject);
    procedure Action_DupRecordExecute(Sender: TObject);
    procedure Act_ChooseDynamicControlFileExecute(Sender: TObject);
    procedure Database_AtmCrtLogin(Database: TDatabase;
      LoginParams: TStrings);
    procedure Act_EditExtraFieldsExecute(Sender: TObject);
    procedure Database_AtmCrtAfterConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    DbPassword,DBUserName :ShortString;
  end;

var
  DM_AtmCrt: TDM_AtmCrt;

implementation

uses F_Musa;

{$R *.DFM}

procedure TDM_AtmCrt.Action_OpenSearchExecute(Sender: TObject);
Var
   Bool :Boolean;
Begin
  if Screen.FormCount<=0 Then
    Exit;
  if Screen.Forms[0].ActiveControl is TAtmDbHEdit Then
   (Screen.Forms[0].ActiveControl As TAtmDbHEdit).ExecSearch
  Else
    if Screen.Forms[0].ActiveControl is TDbGrid Then
      if TDbGrid(Screen.Forms[0].ActiveControl).Columns[TDbGrid(Screen.Forms[0].ActiveControl).SelectedIndex].ButtonStyle=cbsEllipsis Then
        if Assigned(TDbGrid(Screen.Forms[0].ActiveControl).OnEditButtonClick) Then
          TDbGrid(Screen.Forms[0].ActiveControl).OnEditButtonClick(Screen.Forms[0].ActiveControl);

end;

procedure TDM_AtmCrt.Action_OpenSearchUpdate(Sender: TObject);
begin
  Try
     if Screen.FormCount<=0 Then
        Exit;
     if Screen.Forms[0].ActiveControl is TAtmDbHEdit Then
        (Sender As TAction).Enabled:=(Screen.Forms[0].ActiveControl As TAtmDbHEdit).EnableSearch
     Else
         (Sender As TAction).Enabled:=False;
  Except On Exception Do;
  End;
end;

procedure TDM_AtmCrt.AppEvents1ActiveControlChange(Sender: TObject);
begin
     Action_OpenSearchUpdate(Action_OpenSearch);
end;

procedure TDM_AtmCrt.DM_AtmCrtCreate(Sender: TObject);
Var
   I :longInt;
begin
{  Try
     StrH:=GetTempDir;
     if StrH[Length(StrH)]='\' Then
        StrH:=Copy(StrH,1,Length(StrH)-1);
     Session.PrivateDir:=StrH;
  Except
        //Ignore
  End;
}
   Application.BiDiMode:=bdRightToLeft;
   Application.BiDiKeyboard:=HebBidiKeyboard;
   Database_AtmCrt.Close;
   DbPassword:=RunDllStringFuncNoParam('Permit.dll','GetCurDbPassword');
   DBUserName:=RunDllStringFuncNoParam('Permit.dll','GetCurDbUserName');
 //  ShowMessage(DbPassword+'  pass');
     // ShowMessage(DBUserName+'  us');
//   Database_AtmCrt.LoginPrompt:=DBUserName='';
   Database_AtmCrt.LoginPrompt:=True;
    if CurCrtAliasName<>'' Then
       Database_AtmCrt.AliasName:=CurCrtAliasName
    Else
        CurCrtAliasName:=Database_AtmCrt.AliasName;

     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataSet)  And (Components[i].Tag=1)Then
             (Components[I] As TDataSet).Open;
     End;
end;

procedure TDM_AtmCrt.DM_AtmCrtDestroy(Sender: TObject);
Var
   I:LongInt;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
     Database_AtmCrt.Connected:=False;
end;

procedure TDM_AtmCrt.Tbl_KodTavlaTableChange;
begin
     Try
        Tbl_KodTavla.Refresh;
     Except On Exception Do;
     End;
end;

procedure TDM_AtmCrt.Action_OpenDlgSearchPlaceExecute(Sender: TObject);
begin
//     ShowMessage(Sender.ClassName);
end;

procedure TDM_AtmCrt.Action_OpenSearchFormExecute(Sender: TObject);
begin
     OpenAtmCrt(fnSearchForm,'');
end;

procedure TDM_AtmCrt.Action_DupRecordExecute(Sender: TObject);
Var
   DS:TDataSource;
begin
  Try
     DS:=GetDatasourceFromTComponent(Screen.Forms[0].ActiveControl);
     if DS<>Nil Then
     Begin
        DuplicateOneRecord(DS.DataSet,False,True);
        MessageDlg('������ ������, �� ���� �� �������� �������',mtInformation,[mbOk],0);
     End;
  Except on e:Exception Do
         ShowMessage('�� ���� ������ ����� ��'+
                     #13+E.Message);
  End;
end;

procedure TDM_AtmCrt.Act_ChooseDynamicControlFileExecute(Sender: TObject);
Var
   OpenDialog:TOpenDialog;
   TempDynamicControls :TDynamicControls;
   I :LongInt;
begin
     if Screen.ActiveForm=Nil Then Exit;
     TempDynamicControls:=Nil;
     For I:=0 To Screen.ActiveForm.ComponentCount-1 Do
         if Screen.ActiveForm.Components[i] is TDynamicControls Then
         Begin
              TempDynamicControls:=TDynamicControls(Screen.ActiveForm.Components[i]);
              Break;
         End;
     if TempDynamicControls=Nil Then Exit;

     OpenDialog:=TOpenDialog.Create(Self);
     Try
         OpenDialog.InitialDir:=ExtractFileDir(TempDynamicControls.IniFileName);
         OpenDialog.FileName:=ExtractFileName(TempDynamicControls.IniFileName);
         if OpenDialog.Execute Then
         Begin
             TempDynamicControls.IniFileName:=OpenDialog.FileName;
             ShowMessage('������� ����� ����� ������ ���� ���� ��');
         End
         Else
         Begin
             if MessageDlg('��� ���� �� ������ ����',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
                TempDynamicControls.IniFileName:='';
         End;
     Finally
            OpenDialog.Free;
     End;
end;

procedure TDM_AtmCrt.Database_AtmCrtLogin(Database: TDatabase;
  LoginParams: TStrings);
begin
  LoginParams.Clear;
  LoginParams.Add('USER NAME='+DBUserName);
  LoginParams.Add('PASSWORD='+DBPassword);
end;

procedure TDM_AtmCrt.Act_EditExtraFieldsExecute(Sender: TObject);
Var
   OpenDialog:TSaveDialog;
   TempDynamicControls :TDynamicControls;
   I :LongInt;
begin
     if Screen.ActiveForm=Nil Then Exit;
     TempDynamicControls:=Nil;
     For I:=0 To Screen.ActiveForm.ComponentCount-1 Do
         if Screen.ActiveForm.Components[i] is TDynamicControls Then
         Begin
              TempDynamicControls:=TDynamicControls(Screen.ActiveForm.Components[i]);
              Break;
         End;
     if TempDynamicControls=Nil Then Exit;

     Begin
       OpenDialog:=TSaveDialog.Create(Self);
       Try
           OpenDialog.Title:='��� �� ���� ������ ������ ����� ������';
           OpenDialog.InitialDir:=ExtractFileDir(TempDynamicControls.IniFileName);
           OpenDialog.FileName:=ExtractFileName(TempDynamicControls.IniFileName);
           if OpenDialog.Execute Then
           Begin
               TempDynamicControls.IniFileName:=OpenDialog.FileName;
           End
           Else
            TempDynamicControls.IniFileName:='';
       Finally
              OpenDialog.Free;
       End;
     End;
     if TempDynamicControls.IniFileName<>'' Then
     Begin
        if TempDynamicControls.ExtraFieldsParent is TTabSheet Then
          TTabSheet(TempDynamicControls.ExtraFieldsParent).PageControl.ActivePage:=TTabSheet(TempDynamicControls.ExtraFieldsParent)
        Else
          TempDynamicControls.ExtraFieldsParent.BringToFront;
        TempDynamicControls.EditExtraFields;
     End;
end;

procedure TDM_AtmCrt.Database_AtmCrtAfterConnect(Sender: TObject);
begin
  if CompareText(CrtSQLServerType,'Oracle')=0 Then
    Database_AtmCrt.Execute(OracleDateFormatSql);

  if CompareText(CrtSQLServerType,'Sybase')=0 Then
  Begin
//    Database_AtmCrt.StartTransaction;
    Database_AtmCrt.Execute(SybaseDateFormatSql);
//    Database_AtmCrt.Commit;
  End;
end;

end.

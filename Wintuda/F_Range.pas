unit F_Range;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Grids, DBGrids, AtmListDlg, RxQuery, AtmRxQuery, RXDBCtrl;

type
  TFrm_Range = class(TForm)
    DBGrid_Range: TRxDBGrid;
    Tbl_Range: TAtmRxQuery;
    DS_Range: TDataSource;
    Tbl_RangeUserName: TStringField;
    Tbl_RangeRangeType: TStringField;
    Tbl_RangeHebrewRangeName: TStringField;
    Tbl_RangeFromValue: TIntegerField;
    Tbl_RangeToValue: TIntegerField;
    AtmListDlg_RangeType: TAtmListDlg;
    UpdateSQL_Range: TUpdateSQL;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid_RangeEditButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid_RangeTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_Range: TFrm_Range;

implementation

uses AtmRutin;

{$R *.DFM}

procedure TFrm_Range.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Open;
     End;
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_Range.DBGrid_RangeEditButtonClick(Sender: TObject);
begin
     if AtmListDlg_RangeType.Execute Then
     Begin
         if Tbl_Range.State=dsBrowse Then
            Tbl_Range.Edit;
         Tbl_RangeRangeType.AsString:=GetFieldFromSeparateString(AtmListDlg_RangeType.ReturnString,'_',1);
         Tbl_RangeHebrewRangeName.AsString:=GetFieldFromSeparateString(AtmListDlg_RangeType.ReturnString,'_',2);
     End;
end;

procedure TFrm_Range.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
end;

procedure TFrm_Range.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Range:=Nil;
     Action:=caFree;
end;

Procedure TFrm_Range.ShowForm(FormParams:ShortString);
Begin
     Show;
End;
procedure TFrm_Range.DBGrid_RangeTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  Tbl_Range.IndexFieldNames:='RangeType,'+Field.FieldName;
end;

end.

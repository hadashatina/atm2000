unit F_Site;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, Mask, ComCtrls, DBTables, ExtCtrls, AtmComp,
  AtmAdvTable,AtmRutin;

type
  TFrm_Site = class(TForm)
    DBNavigator: TDBNavigator;
    Panel1: TPanel;
    DS_Site: TDataSource;
    Panel2: TPanel;
    Tbl_Site: TAtmTable;
    PageControl1: TPageControl;
    TS_Site: TTabSheet;
    ScrollBox: TScrollBox;
    Lbl_Code: TLabel;
    Lbl_Group: TLabel;
    Lbl_Name: TLabel;
    Lbl_X: TLabel;
    Lbl_Y: TLabel;
    Lbl_Pratim: TLabel;
    Lbl_City: TLabel;
    Lbl_Street: TLabel;
    Lbl_HouseNum: TLabel;
    Lbl_ZipCode: TLabel;
    Lbl_Icon: TLabel;
    Lbl_Contact: TLabel;
    Lbl_Phone: TLabel;
    Lbl_Fax: TLabel;
    Lbl_KneMida: TLabel;
    Lbl_Kamut1: TLabel;
    Lbl_Kamut2: TLabel;
    Lbl_Kamut3: TLabel;
    Lbl_Kamut4: TLabel;
    Lbl_Kamut5: TLabel;
    Lbl_Kamut6: TLabel;
    Lbl_Kamut7: TLabel;
    EditCODE_SITE: TAtmDbHEdit;
    EditGROUP: TAtmDbHEdit;
    EditNAME_SITE: TAtmDbHEdit;
    EditX: TAtmDbHEdit;
    EditY: TAtmDbHEdit;
    EditPRATIM: TAtmDbHEdit;
    EditCITY: TAtmDbHEdit;
    EditSTREET: TAtmDbHEdit;
    EditHOUSE_NUM: TAtmDbHEdit;
    EditZIP_CODE: TAtmDbHEdit;
    EditICON: TAtmDbHEdit;
    EditCONTACT_MAN: TAtmDbHEdit;
    EditPHONE: TAtmDbHEdit;
    EditFAX: TAtmDbHEdit;
    EditKNE_MIDA: TAtmDbHEdit;
    EditKAMUT_: TAtmDbHEdit;
    EditKAMUT_2: TAtmDbHEdit;
    EditKAMUT_3: TAtmDbHEdit;
    EditKAMUT_4: TAtmDbHEdit;
    EditKAMUT_5: TAtmDbHEdit;
    EditKAMUT_6: TAtmDbHEdit;
    EditKAMUT_7: TAtmDbHEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_SiteAfterPost(DataSet: TDataSet);
    procedure Tbl_SiteAfterInsert(DataSet: TDataSet);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Frm_Site: TFrm_Site;


implementation
Uses Crt_Glbl;
{$R *.DFM}

procedure TFrm_Site.FormCreate(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
end;

procedure TFrm_Site.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
end;

procedure TFrm_Site.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
end;

procedure TFrm_Site.Tbl_SiteAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_Site.Tbl_SiteAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

end.
unit F_MaslulMoatza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, AtmAdvTable, ExtCtrls, DBCtrls, StdCtrls, Mask, AtmComp,
  ComCtrls, ToolEdit, RXDBCtrl, AtmDBDateEdit, Grids, DBGrids, RXSplit,
  AdvSearch, ToolWin,Math, ActnList;

type
  TFrm_MsllMoatsa = class(TForm)
    Panel2: TPanel;
    PageControl_Maslul: TPageControl;
    TS_Maslul: TTabSheet;
    TS_Picture: TTabSheet;
    DS_Maslul: TDataSource;
    Tbl_Maslul: TAtmTable;
    Tbl_MaslulCODE_MASLUL: TIntegerField;
    Tbl_MaslulNAME: TStringField;
    Tbl_MaslulMOTZA: TStringField;
    Tbl_MaslulYAAD: TStringField;
    Tbl_MaslulPRATIM: TStringField;
    Tbl_MaslulSUG_MASLUL: TIntegerField;
    Tbl_MaslulTIME_MASLUL: TIntegerField;
    Tbl_MaslulKAMUT: TBCDField;
    Tbl_MaslulHOUR_START: TDateTimeField;
    Tbl_MaslulHOUR_END: TDateTimeField;
    Tbl_MaslulIFYUN: TIntegerField;
    Tbl_MaslulDAY1: TIntegerField;
    Tbl_MaslulDAY2: TIntegerField;
    Tbl_MaslulDAY3: TIntegerField;
    Tbl_MaslulDAY4: TIntegerField;
    Tbl_MaslulDAY5: TIntegerField;
    Tbl_MaslulDAY6: TIntegerField;
    Tbl_MaslulDAY7: TIntegerField;
    Tbl_MaslulPRICE_GLOBAL: TBCDField;
    Tbl_MaslulPRICE_KAMUT: TBCDField;
    Tbl_MaslulPRICE_KM: TBCDField;
    Tbl_MaslulINTEGER_1: TIntegerField;
    Tbl_MaslulINTEGER_2: TIntegerField;
    Tbl_MaslulINTEGER_3: TIntegerField;
    Tbl_MaslulINTEGER_4: TIntegerField;
    Tbl_MaslulINTEGER_5: TIntegerField;
    Tbl_MaslulINTEGER_6: TIntegerField;
    Tbl_MaslulINTEGER_7: TIntegerField;
    Tbl_MaslulINTEGER_8: TIntegerField;
    Tbl_MaslulINTEGER_9: TIntegerField;
    Tbl_MaslulINTEGER_10: TIntegerField;
    Tbl_MaslulSTRING_1: TStringField;
    Tbl_MaslulSTRING_2: TStringField;
    Tbl_MaslulSTRING_3: TStringField;
    Tbl_MaslulSTRING_4: TStringField;
    Tbl_MaslulSTRING_5: TStringField;
    Tbl_MaslulSTRING_6: TStringField;
    Tbl_MaslulSTRING_7: TStringField;
    Tbl_MaslulSTRING_8: TStringField;
    Tbl_MaslulSTRING_9: TStringField;
    Tbl_MaslulSTRING_10: TStringField;
    Tbl_MaslulBCD_1: TBCDField;
    Tbl_MaslulBCD_2: TBCDField;
    Tbl_MaslulBCD_3: TBCDField;
    Tbl_MaslulBCD_4: TBCDField;
    Tbl_MaslulBCD_5: TBCDField;
    Tbl_MaslulBCD_6: TBCDField;
    Tbl_MaslulBCD_7: TBCDField;
    Tbl_MaslulBCD_8: TBCDField;
    Tbl_MaslulBCD_9: TBCDField;
    Tbl_MaslulBCD_10: TBCDField;
    Tbl_MaslulGROUP_MASLUL: TIntegerField;
    Tbl_MaslulDISTANCE_MASLUL: TBCDField;
    Tbl_MaslulMAP_FILENAME: TStringField;
    Tbl_MaslulDATE_START_MASLUL: TDateTimeField;
    Tbl_MaslulDATE_END_MASLUL: TDateTimeField;
    Tbl_MaslulSUG_PIKUACH: TIntegerField;
    Tbl_MaslulHIUV_MISRAD_HINUCH: TIntegerField;
    Tbl_MaslulSUG_HINUCH: TIntegerField;
    Tbl_MaslulTEUR_MUGBAL: TStringField;
    Tbl_MaslulCODE_LUACH: TIntegerField;
    Tbl_MaslulSCHOOL_YEAR: TIntegerField;
    Tbl_MaslulCODE_MEHIRON: TIntegerField;
    Tbl_MaslulTOTAL_ADULT: TIntegerField;
    Tbl_MaslulTOTAL_YOUNG: TIntegerField;
    Tbl_MaslulTOTAL_TEACHER: TIntegerField;
    Tbl_MaslulTOTAL_MUSAIM: TIntegerField;
    Tbl_MaslulSUG_REHEV1: TIntegerField;
    Tbl_MaslulSUG_REHEV2: TIntegerField;
    Tbl_MaslulSUG_REHEV3: TIntegerField;
    Tbl_MaslulKAMUT_REHEV1: TIntegerField;
    Tbl_MaslulKAMUT_REHEV2: TIntegerField;
    Tbl_MaslulKAMUT_REHEV3: TIntegerField;
    Tbl_MaslulCODE_CLIENT: TIntegerField;
    Panel3: TPanel;
    Lbl_CodeMaslul: TLabel;
    Lbl_Motza: TLabel;
    Lbl_Yaad: TLabel;
    Lbl_Distance: TLabel;
    Lbl_time: TLabel;
    Lbl_HourStart: TLabel;
    Lbl_HourEnd: TLabel;
    Label7: TLabel;
    Lbl_StartDate: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EditCODE_MASLUL: TAtmDbHEdit;
    EditMOTZA: TAtmDbHEdit;
    EditYAAD: TAtmDbHEdit;
    EditDISTANCE_MASLUL: TAtmDbHEdit;
    EditTIME_MASLUL: TAtmDbHEdit;
    EditHOUR_START: TAtmDbHEdit;
    EditHOUR_END: TAtmDbHEdit;
    GroupBox1: TGroupBox;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBRG_SugMaslul: TDBRadioGroup;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmDBDateEdit1: TAtmDBDateEdit;
    AtmDBDateEdit2: TAtmDBDateEdit;
    AtmDbHEdit2: TAtmDbHEdit;
    AtmDbHEdit3: TAtmDbHEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    AtmDbHEdit4: TAtmDbHEdit;
    AtmDbHEdit5: TAtmDbHEdit;
    AtmDbHEdit6: TAtmDbHEdit;
    Label15: TLabel;
    Panel4: TPanel;
    DBGrid_MsllStat: TDBGrid;
    Tbl_MaslulStation: TAtmTable;
    DS_MaslulStation: TDataSource;
    RxSplitter1: TRxSplitter;
    Tbl_StatMusa: TAtmTable;
    DS_StatMusa: TDataSource;
    Label16: TLabel;
    DBEdit1: TAtmDbHEdit;
    Label17: TLabel;
    DBEdit2: TAtmDbHEdit;
    Label18: TLabel;
    DBEdit3: TAtmDbHEdit;
    TabSheet1: TTabSheet;
    Tbl_MaslulSUG_REHEV_ACTUAL1: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL2: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL3: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL1: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL2: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL3: TIntegerField;
    Tbl_MaslulCODE_PIZUL: TIntegerField;
    Tbl_MaslulCODE_TKUFA: TIntegerField;
    Tbl_MaslulCODE_KAV: TIntegerField;
    Label34: TLabel;
    DBEdit19: TAtmDbHEdit;
    Label35: TLabel;
    DBEdit20: TAtmDbHEdit;
    Tbl_StatMusaCODE_MASLUL: TIntegerField;
    Tbl_StatMusaCODE_STATION: TIntegerField;
    Tbl_StatMusaCODE_MUSA: TIntegerField;
    Tbl_StatMusaKAMUT: TIntegerField;
    Tbl_Musa: TAtmTable;
    Tbl_StatMusaLookMusaLastName: TStringField;
    Tbl_StatMusaLookMusaFirstName: TStringField;
    Tbl_KodTavla: TAtmTable;
    DBText1: TDBText;
    AtmAdvSearch_KodTavla: TAtmAdvSearch;
    Query_KodTavla: TQuery;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    Tbl_MaslulStationCODE_MASLUL: TIntegerField;
    Tbl_MaslulStationSEMEL_YESHUV: TIntegerField;
    Tbl_MaslulStationCODE_STATION: TIntegerField;
    Tbl_MaslulStationPOSITION_IN_MASLUL: TIntegerField;
    Tbl_MaslulStationTOTAL_ADULT: TIntegerField;
    Tbl_MaslulStationTOTAL_YOUNG: TIntegerField;
    Tbl_MaslulStationTOTAL_TEACHER: TIntegerField;
    Tbl_MaslulStationTOTAL_MUSAIM: TIntegerField;
    Tbl_MaslulStationTIME_ARIVAL_TO_STATION: TDateTimeField;
    Tbl_MaslulStationTIME_ARIVAL_TO_TARGET: TDateTimeField;
    Tbl_MaslulStationDOWN_UP: TIntegerField;
    Tbl_MaslulStationDISTANCE_FROM_PREV_STAT: TIntegerField;
    Tbl_MaslulStationDISTANCE_FROM_FIRST_PNT: TIntegerField;
    Tbl_StatMusaLookMusaTZ: TIntegerField;
    Tbl_StatMusaLookMusaClass: TIntegerField;
    Panel6: TPanel;
    Tbl_MaslulLookSugHinuch: TStringField;
    Tbl_MaslulLookSugRehev1: TStringField;
    Tbl_MaslulLookSugRehev2: TStringField;
    Tbl_MaslulLookSugRehev3: TStringField;
    Tbl_MaslulLookSugRehevA1: TStringField;
    Tbl_MaslulLookSugRehevA2: TStringField;
    Tbl_MaslulLookSugRehevA3: TStringField;
    GroupBox2: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBEdit10: TAtmDbHEdit;
    DBEdit11: TAtmDbHEdit;
    DBEdit12: TAtmDbHEdit;
    DBEdit13: TAtmDbHEdit;
    DBEdit14: TAtmDbHEdit;
    DBEdit15: TAtmDbHEdit;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBEdit4: TAtmDbHEdit;
    DBEdit5: TAtmDbHEdit;
    DBEdit6: TAtmDbHEdit;
    DBEdit7: TAtmDbHEdit;
    DBEdit8: TAtmDbHEdit;
    DBEdit9: TAtmDbHEdit;
    GroupBox4: TGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit16: TAtmDbHEdit;
    DBEdit17: TAtmDbHEdit;
    DBEdit18: TAtmDbHEdit;
    Panel7: TPanel;
    Lbl_MapFileName: TLabel;
    DBEdit21: TAtmDbHEdit;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ToolButton1: TToolButton;
    TabSheet2: TTabSheet;
    DBGrid_StatMusa: TDBGrid;
    ToolButton2: TToolButton;
    TBtn_SumMaxMusaim: TToolButton;
    Qry_Temp: TQuery;
    Tbl_MaslulStationLookSugDownUp: TStringField;
    ActionList_Maslul: TActionList;
    Action_CalcMaxMusa: TAction;
    Tbl_Station: TAtmTable;
    Tbl_MaslulStationLookStationName: TStringField;
    ActionOpenSearch: TAction;
    AtmAdvSearch_Station: TAtmAdvSearch;
    TBtn_NewMaslul: TToolButton;
    Action_NewMaslul: TAction;
    AtmAdvSearch_Musa: TAtmAdvSearch;
    Tbl_Lak: TTable;
    Tbl_MaslulLookClientName: TStringField;
    DBText8: TDBText;
    Panel1: TPanel;
    DBText9: TDBText;
    Label36: TLabel;
    TS_Shib: TTabSheet;
    Bevel1: TBevel;
    procedure Tbl_MaslulAfterInsert(DataSet: TDataSet);
    procedure Tbl_MaslulAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure AtmDbHEdit4BeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Tbl_MaslulCODE_KAVValidate(Sender: TField);
    procedure PageControl_MaslulChange(Sender: TObject);
    procedure Tbl_MaslulStationBeforePost(DataSet: TDataSet);
    procedure Tbl_StatMusaBeforePost(DataSet: TDataSet);
    procedure Tbl_MaslulHOUR_STARTChange(Sender: TField);
    procedure FormShow(Sender: TObject);
    procedure ActionList_MaslulUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure DBGrid_MsllStatEnter(Sender: TObject);
    procedure Tbl_MaslulStationAfterPost(DataSet: TDataSet);
    procedure ActionOpenSearchExecute(Sender: TObject);
    procedure ActionOpenSearchUpdate(Sender: TObject);
    procedure Action_NewMaslulExecute(Sender: TObject);
    procedure Tbl_MaslulStationAfterInsert(DataSet: TDataSet);
    procedure Action_CalcMaxMusaExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ChangeDSForNavigator(TheNavi :TDBNavigator);
  end;

var
  Frm_MsllMoatsa: TFrm_MsllMoatsa;

implementation
Uses DMAtmCrt,Crt_Glbl,AtmRutin,AtmConst;
{$R *.DFM}

procedure TFrm_MsllMoatsa.Tbl_MaslulAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     Tbl_MaslulStation.Insert;
end;

procedure TFrm_MsllMoatsa.Tbl_MaslulAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_MsllMoatsa.FormCreate(Sender: TObject);
var
   i:longint;
   SList:TStringList;
begin
     For I:=ComponentCount-1 DownTo 0 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;
     ReadF4ValuesToIni(Self,DirectoryForIni+Name+'.Ini','FixedFields');

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_SugHinuch),Tbl_MaslulLookSugHinuch);
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_MaslulLookSugRehev1);
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_MaslulLookSugRehev2);
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_MaslulLookSugRehev3);
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_MaslulLookSugRehevA1);
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_MaslulLookSugRehevA2);
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_MaslulLookSugRehevA3);
     FillKodTavlaLookupList(Tbl_KodTavla,SList);
     SList.Free;

     Tbl_MaslulStationLookSugDownUp.LookupList.Add(0,'����');
     Tbl_MaslulStationLookSugDownUp.LookupList.Add(1,'����');

end;

procedure TFrm_MsllMoatsa.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,DirectoryForIni+Name+'.ini','FixedFields');
end;

procedure TFrm_MsllMoatsa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Maslul);
end;

procedure TFrm_MsllMoatsa.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Not Visible Then Exit;
     ChangeDSForNavigator(TDBNavigator(Sender));
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
end;

procedure TFrm_MsllMoatsa.AtmDbHEdit4BeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugHinuch;
end;

Procedure TFrm_MsllMoatsa.ChangeDSForNavigator(TheNavi :TDBNavigator);
Var
   TheDS :TDataSource;
begin
     if Not Visible Then Exit;
     TheDs:=GetDataSourceFromTControl(ActiveControl);
     if (TheDs<>Nil) And (TheNavi.DataSource<>TheDs) Then
        TheNavi.DataSource:=TheDs;
End;

procedure TFrm_MsllMoatsa.Tbl_MaslulCODE_KAVValidate(Sender: TField);
begin
     if (Tbl_Maslul.FieldByName('CODE_KAV').AsString<>'') And
        (Tbl_Maslul.FieldByName('CODE_TKUFA').AsString<>'') And
        (Tbl_Maslul.FieldByName('CODE_PIZUL').AsString<>'') Then
              Tbl_Maslul.FieldByName('CODE_MASLUL').AsInteger:=
                 (Tbl_Maslul.FieldByName('CODE_KAV').AsInteger)*100+
                 (Tbl_Maslul.FieldByName('CODE_PIZUL').AsInteger)*10+
                 (Tbl_Maslul.FieldByName('CODE_PIZUL').AsInteger);

end;

procedure TFrm_MsllMoatsa.PageControl_MaslulChange(Sender: TObject);
Var
   StrH :String;
begin
     if PageControl_Maslul.ActivePage=TS_Picture Then
     Begin
           StrH:=Tbl_Maslul.FieldByName('MAP_FILENAME').AsString;
           if StrH='' Then
              Image1.Picture:=Nil
           Else
               Image1.Picture.LoadFromFile(StrH);
     End;
     ChangeDSForNavigator(DBNavigator);
end;

procedure TFrm_MsllMoatsa.Tbl_MaslulStationBeforePost(DataSet: TDataSet);
begin
     Tbl_MaslulStation.FieldByName('CODE_MASLUL').AsInteger:=
          Tbl_Maslul.FieldByName('CODE_MASLUL').AsInteger;
end;

procedure TFrm_MsllMoatsa.Tbl_StatMusaBeforePost(DataSet: TDataSet);
begin
      Tbl_StatMusa.FieldByName('CODE_MASLUL').AsInteger:=
          Tbl_MaslulStation.FieldByName('CODE_MASLUL').AsInteger;
      Tbl_StatMusa.FieldByName('CODE_STATION').AsInteger:=
          Tbl_MaslulStation.FieldByName('CODE_STATION').AsInteger;
end;

procedure TFrm_MsllMoatsa.Tbl_MaslulHOUR_STARTChange(Sender: TField);
Var
   CHour,CMinute,CSec,CMSec :Word;
   IntH :LongInt;
begin
     if (Tbl_Maslul.FieldByName('HOUR_START').AsString<>'') And
        (Tbl_Maslul.FieldByName('HOUR_End').AsString<>'') Then
     Begin
          DecodeTime(Tbl_Maslul.FieldByName('HOUR_End').AsDateTime,CHour,Cminute,CSec,CMSec);
          IntH:=CHour*60+Cminute;
          DecodeTime(Tbl_Maslul.FieldByName('HOUR_START').AsDateTime,CHour,Cminute,CSec,CMSec);
          IntH:=IntH-(CHour*60+Cminute);
          Tbl_Maslul.FieldByName('TIME_MASLUL').AsInteger:=IntH;
     End;

end;

procedure TFrm_MsllMoatsa.FormShow(Sender: TObject);
Var
   MyControl :TWinControl;
begin
     MyControl:=FindFirstControlInTabOrder(Self);
     if MyControl.CanFocus Then
        MyControl.SetFocus
     Else
         SelectNext(MyControl,True,True);
     PageControl_Maslul.ActivePage:=PageControl_Maslul.Pages[0];
end;

procedure TFrm_MsllMoatsa.ActionList_MaslulUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
     ChangeDSForNavigator(DBNavigator);
end;

procedure TFrm_MsllMoatsa.DBGrid_MsllStatEnter(Sender: TObject);
begin
     ChangeDSForNavigator(DBNavigator);
end;

procedure TFrm_MsllMoatsa.Tbl_MaslulStationAfterPost(DataSet: TDataSet);
begin
     Tbl_MaslulStation.First;
     if Tbl_MaslulStation.FieldByName('LookStationName').AsString<>
        Tbl_Maslul.FieldByName('MOTZA').AsString Then
     Begin
         if Tbl_Maslul.State=dsBrowse Then
            Tbl_Maslul.Edit;
         Tbl_Maslul.FieldByName('MOTZA').AsString:=Tbl_MaslulStation.FieldByName('LookStationName').AsString;
     End;
     Tbl_MaslulStation.Last;
     if Tbl_MaslulStation.FieldByName('LookStationName').AsString<>
        Tbl_Maslul.FieldByName('YAAD').AsString Then
     Begin
         if Tbl_Maslul.State=dsBrowse Then
            Tbl_Maslul.Edit;
         Tbl_Maslul.FieldByName('YAAD').AsString:=Tbl_MaslulStation.FieldByName('LookStationName').AsString;
     End;
     if Tbl_Maslul.State<>dsBrowse Then
        Tbl_Maslul.Post;
end;

procedure TFrm_MsllMoatsa.ActionOpenSearchExecute(Sender: TObject);
Var
   Bool :Boolean;
Begin
     if ActiveControl is TAtmDbHEdit Then
         if (ActiveControl As TAtmDbHEdit).SearchComponent<>Nil Then
         Begin
              if Assigned((ActiveControl As TAtmDbHEdit).BeforeExecuteSearch) Then
                 (ActiveControl As TAtmDbHEdit).BeforeExecuteSearch(ActiveControl,Bool);
              if (ActiveControl As TAtmDbHEdit).SearchComponent.Execute Then
              Begin
                 if (ActiveControl As TAtmDbHEdit).DataSource.DataSet.State=dsBrowse Then
                    (ActiveControl As TAtmDbHEdit).DataSource.DataSet.Edit;
                 (ActiveControl As TAtmDbHEdit).Text:=(ActiveControl As TAtmDbHEdit).SearchComponent.ReturnString;
              End;
         End;
     if ActiveControl = DBGrid_MsllStat Then
           if AtmAdvSearch_Station.Execute Then
           Begin
               if (ActiveControl As TDbGrid).DataSource.DataSet.State=dsBrowse Then
                  (ActiveControl As TDbGrid).DataSource.DataSet.Edit;
               Tbl_MaslulStation.FieldByName('SEMEL_YESHUV').AsString:=
                  GetFieldFromSeparateString(AtmAdvSearch_Station.ReturnString,AtmAdvSearch_Station.SeparateChar,1);
               Tbl_MaslulStation.FieldByName('CODE_STATION').AsString:=
                  GetFieldFromSeparateString(AtmAdvSearch_Station.ReturnString,AtmAdvSearch_Station.SeparateChar,3);
           End;
     if ActiveControl = DBGrid_StatMusa Then
           if AtmAdvSearch_Musa.Execute Then
           Begin
               if (ActiveControl As TDbGrid).DataSource.DataSet.State=dsBrowse Then
                  (ActiveControl As TDbGrid).DataSource.DataSet.Edit;
               Tbl_StatMusa.FieldByName('CODE_MUSA').AsString:=AtmAdvSearch_Musa.ReturnString;
           End;
end;

procedure TFrm_MsllMoatsa.ActionOpenSearchUpdate(Sender: TObject);
begin
     if ActiveControl is TAtmDbHEdit Then
        (Sender As TAction).Enabled:=(ActiveControl As TAtmDbHEdit).SearchComponent<>Nil
     Else
     if ActiveControl is TDbGrid Then
        (Sender As TAction).Enabled:=True{CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'MaslulCode1')=0}
     Else
         (Sender As TAction).Enabled:=False;
end;

procedure TFrm_MsllMoatsa.Action_NewMaslulExecute(Sender: TObject);
Var
   MyControl :TWinControl;
begin
     Tbl_Maslul.Insert;
     PageControl_Maslul.ActivePage:=TS_Maslul;
     MyControl:=FindFirstControlInTabOrder(Self);
     if MyControl.CanFocus Then
        MyControl.SetFocus
     Else
         SelectNext(MyControl,True,True);
     ChangeDSForNavigator(DBNavigator);
end;

procedure TFrm_MsllMoatsa.Tbl_MaslulStationAfterInsert(DataSet: TDataSet);
begin
     Tbl_StatMusa.Insert;
end;

procedure TFrm_MsllMoatsa.Action_CalcMaxMusaExecute(Sender: TObject);
Var
   TYoung,TAdult,TTeacher,IntH :longInt;
   MYoung,MAdult,MTeacher :LongInt;
   Mult :Integer;
begin
     TYoung:=0;
     TAdult:=0;
     TTeacher:=0;
     MAdult:=0;
     MYoung:=0;
     MTeacher:=0;
     Tbl_MaslulStation.First;
     While Not Tbl_MaslulStation.Eof Do
     Begin
          if Tbl_MaslulStation.FieldByName('DOWN_UP').Value>0 Then
             Mult:=1
          Else
              Mult:=-1;
          IntH:=Tbl_MaslulStation.FieldByName('TOTAL_ADULT').AsInteger;
          IntH:=IntH*Mult;
          TAdult:=TAdult+IntH;
          IntH:=Tbl_MaslulStation.FieldByName('TOTAL_YOUNG').AsInteger;
          IntH:=IntH*Mult;
          TYoung:=TYoung+IntH;
          IntH:=Tbl_MaslulStation.FieldByName('TOTAL_TEACHER').AsInteger;
          IntH:=IntH*Mult;
          TTeacher:=TTeacher+IntH;

          if TAdult+TYoung+TTeacher > MAdult+MYoung+MTeacher Then
          Begin
               MAdult:=TAdult;
               MYoung:=TYoung;
               MTeacher:=TTeacher;
          End;

          Tbl_MaslulStation.Next;
     End;

     if Tbl_Maslul.State=dsBrowse Then
        Tbl_Maslul.Edit;
     Tbl_Maslul.FieldByName('TOTAL_ADULT').AsInteger:=MAdult;
     Tbl_Maslul.FieldByName('TOTAL_YOUNG').AsInteger:=MYoung;
     Tbl_Maslul.FieldByName('TOTAL_TEACHER').AsInteger:=MTeacher;
end;

end.

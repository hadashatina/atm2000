unit F_TzfifutHodshit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, Grids, DBGrids, Db;

procedure ShowTzfifuyot(TzType: Integer);

type
  TFrm_TzfifutHodshit = class(TForm)
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DataSource1: TDataSource;
  private
    { Private declarations }
  public
        
    { Public declarations }
  end;

var
  Frm_TzfifutHodshit: TFrm_TzfifutHodshit;

implementation

uses
    DMWinTuda;
{$R *.DFM}

procedure ShowTzfifuyot(TzType: Integer);
begin
     Application.CreateForm(TFrm_TzfifutHodshit, Frm_TzfifutHodshit);
     case TzType of
          1: DM_WinTuda.RxQuery_Temp.SQL.Text := 'SELECT  TzfifutBsisit, CTemperature, Tzfifut FROM TZFIFUYOT ORDER BY TzfifutBsisit, CTemperature';
          2: DM_WinTuda.RxQuery_Temp.SQL.Text := 'SELECT * FROM TzififutHodshit ORDER BY nYear, nMonth';
     end;
     
     DM_WinTuda.RxQuery_Temp.RequestLive := True;
     with DM_WinTuda.RxQuery_Temp do
     begin
          Open;
          case TzType of
               1: begin
                       Fields.Fields[0].DisplayLabel := '������';
                       Fields.Fields[1].DisplayLabel := '��������';
                       Fields.Fields[2].DisplayLabel := '����';
                  end;
               2: begin
                       Fields.Fields[0].DisplayLabel := '���';
                       Fields.Fields[1].DisplayLabel := '����';
                       Fields.Fields[2].DisplayLabel := '������';
                  end;
          end;
     end;
     Frm_TzfifutHodshit.DataSource1.DataSet := DM_WinTuda.RxQuery_Temp;
     Frm_TzfifutHodshit.ShowModal;
     Frm_TzfifutHodshit.Free;
     if (DM_WinTuda.RxQuery_Temp.State = dsEdit) or
        (DM_WinTuda.RxQuery_Temp.State = dsInsert) then
        DM_WinTuda.RxQuery_Temp.Post;        
     DM_WinTuda.RxQuery_Temp.Close;
     DM_WinTuda.RxQuery_Temp.RequestLive := False;
end;

end.

unit MainTovala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, ImgList, Menus, ExtCtrls, SpeedBar, Placemnt, jpeg, ExeMenu;

type
  TFrm_MainWinTuda = class(TForm)
    MainMenu1: TMainMenu;
    ActionList_Tovala: TActionList;
    ImageList_Buttons: TImageList;
    Act_WinTuda: TAction;
    N1: TMenuItem;
    SpeedBar1: TSpeedBar;
    Act_CrtOpenLako: TAction;
    NCrtMenu: TMenuItem;
    NCrtOpenLako: TMenuItem;
    Act_CrtOpenNehag: TAction;
    NCrtOpenNehag: TMenuItem;
    Act_CrtOpenRehev: TAction;
    NCrtOpenRehev: TMenuItem;
    Act_CrtOpenMaslul: TAction;
    NCrtOpenMaslul: TMenuItem;
    FormStorage_TovalaMain: TFormStorage;
    SpeedbarSection1: TSpeedbarSection;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    Act_CrtOpenMehiron: TAction;
    NCrtOpenMehiron: TMenuItem;
    SpeedItem6: TSpeedItem;
    N3: TMenuItem;
    NExit: TMenuItem;
    Panel1: TPanel;
    Image1: TImage;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    NMakavHeshbon: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    NEditSpeedBar: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    Act_OpenTavla: TAction;
    NOpenTavla: TMenuItem;
    N16: TMenuItem;
    NDefParams: TMenuItem;
    Act_HsbOpenMakav: TAction;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem7: TSpeedItem;
    Act_HsbPerutLak: TAction;
    N8: TMenuItem;
    Act_HsbCopyHeshbonit: TAction;
    Act_HsbHeshbonit: TAction;
    Act_HsbZikuyNehag: TAction;
    N19: TMenuItem;
    N20: TMenuItem;
    Act_HsbEditMname: TAction;
    N21: TMenuItem;
    Act_Kupa_Kabala: TAction;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    SpeedItem13: TSpeedItem;
    Act_AboutBox: TAction;
    N13: TMenuItem;
    N22: TMenuItem;
    Act_MeholelOpen: TAction;
    N23: TMenuItem;
    Act_MeholelReport: TAction;
    N18: TMenuItem;
    SpeedbarSection4: TSpeedbarSection;
    Spb_MeholelMain: TSpeedItem;
    Spb_MeholelReport: TSpeedItem;
    Act_Util_CopyMehiron: TAction;
    N24: TMenuItem;
    N25: TMenuItem;
    Act_DefRange: TAction;
    N26: TMenuItem;
    Act_CrtOpenMahzeva: TAction;
    N27: TMenuItem;
    SpeedItem14: TSpeedItem;
    Act_HsbPerutHiuvNehag: TAction;
    N28: TMenuItem;
    Act_HsbHeshbonitNehag: TAction;
    N29: TMenuItem;
    Act_HsbCopyHeshbonitNehag: TAction;
    N30: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    Act_CancelHeshbonLak: TAction;
    Act_CancelHeshbonNehag: TAction;
    N34: TMenuItem;
    N35: TMenuItem;
    Act_HsbOpenMakavNehag: TAction;
    N36: TMenuItem;
    SpeedItem15: TSpeedItem;
    Act_Interface_SonolSap: TAction;
    N33: TMenuItem;
    Act_Interface_SonolPrem: TAction;
    N37: TMenuItem;
    Act_MeholelCloseMeholel: TAction;
    N2: TMenuItem;
    N38: TMenuItem;
    Act_HsbOpenMakavPreforma: TAction;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    N43: TMenuItem;
    N44: TMenuItem;
    Act_HsbSikumLak: TAction;
    Act_HsbSikumCopyLak: TAction;
    Act_HsbSikumPerutLak: TAction;
    N45: TMenuItem;
    N46: TMenuItem;
    N47: TMenuItem;
    N48: TMenuItem;
    Act_HsbPreformaPerutMerukaz: TAction;
    N42: TMenuItem;
    Act_HsbPreformaMerukaz: TAction;
    N49: TMenuItem;
    N50: TMenuItem;
    Act_Kupa_Hafkada: TAction;
    Act_Kupa_PrintCopyHafkadot: TAction;
    N51: TMenuItem;
    Act_Crt_OpenTavla2: TAction;
    N210: TMenuItem;
    Act_Kupa_CancelHafkada: TAction;
    N52: TMenuItem;
    SpeedbarSection5: TSpeedbarSection;
    SpeedItem12: TSpeedItem;
    N53: TMenuItem;
    SpeedItem16: TSpeedItem;
    Act_Util_RaisePriceInTnua: TAction;
    N54: TMenuItem;
    Act_HsbPerutDolarRegular: TAction;
    Act_HsbDolar: TAction;
    Act_HsbCopyDolar: TAction;
    N55: TMenuItem;
    N56: TMenuItem;
    N57: TMenuItem;
    N58: TMenuItem;
    N59: TMenuItem;
    Act_Interface_Han: TAction;
    Act_HsbZikuyNehagMake: TAction;
    Act_HsbZikuyNehagCopyHeshbonit: TAction;
    N60: TMenuItem;
    N61: TMenuItem;
    N62: TMenuItem;
    Act_HsbZikuyLakPerut: TAction;
    Act_HsbZikuyLakMake: TAction;
    Act_HsbZikuyLakCopy: TAction;
    Act_HsbZikuyLakCancel: TAction;
    N63: TMenuItem;
    N64: TMenuItem;
    N65: TMenuItem;
    N66: TMenuItem;
    N67: TMenuItem;
    N68: TMenuItem;
    Act_WinTudaAtmLako: TAction;
    N70: TMenuItem;
    Act_MoDoh_RetzefTeuda: TAction;
    N71: TMenuItem;
    N72: TMenuItem;
    N73: TMenuItem;
    Act_HsbRikuz_Lak: TAction;
    N74: TMenuItem;
    Act_HsbRikuz_Nehag: TAction;
    N75: TMenuItem;
    N76: TMenuItem;
    N69: TMenuItem;
    Act_HsbZikuyLakDolar_Perut: TAction;
    Act_HsbZikuyLakDolar_Make: TAction;
    Act_HsbZikuyLakDolar_Copy: TAction;
    Act_HsbZikuyLakDolar_Cancel: TAction;
    N77: TMenuItem;
    N78: TMenuItem;
    N79: TMenuItem;
    N80: TMenuItem;
    N81: TMenuItem;
    Act_Interface_HanKabala: TAction;
    N82: TMenuItem;
    N83: TMenuItem;
    Act_HsbPerforma_Perut: TAction;
    Act_HsbPerforma_Make: TAction;
    Act_HsbPerforma_Copy: TAction;
    act_HsbPerforma_Cancel: TAction;
    N84: TMenuItem;
    N85: TMenuItem;
    N86: TMenuItem;
    N87: TMenuItem;
    N88: TMenuItem;
    Act_HsbMerukazPerut: TAction;
    Act_HsbMerukazMake: TAction;
    Act_HsbMerukazCopy: TAction;
    Act_HsbMerukazCancel: TAction;
    N89: TMenuItem;
    N90: TMenuItem;
    N91: TMenuItem;
    N92: TMenuItem;
    N93: TMenuItem;
    N94: TMenuItem;
    N95: TMenuItem;
    Act_HsbDolarCancel: TAction;
    N96: TMenuItem;
    N97: TMenuItem;
    Act_Util_DeleteMehiron: TAction;
    N98: TMenuItem;
    Act_Meholel_ReportKupa: TAction;
    N99: TMenuItem;
    Act_CrtOpenShaar: TAction;
    N100: TMenuItem;
    SpeedItem17: TSpeedItem;
    N101: TMenuItem;
    Act_Interface_HanHafada: TAction;
    N102: TMenuItem;
    Act_HsbZikuyNehagCancel: TAction;
    N103: TMenuItem;
    N104: TMenuItem;
    Act_HsbZikuyXReportNehag: TAction;
    X1: TMenuItem;
    ExeMenu1: TExeMenu;
    Act_EditExecuteMenu: TAction;
    NexecuteMenu: TMenuItem;
    N105: TMenuItem;
    N106: TMenuItem;
    procedure Act_WinTudaExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Act_CrtOpenLakoExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Act_CrtOpenNehagExecute(Sender: TObject);
    procedure Act_CrtOpenRehevExecute(Sender: TObject);
    procedure Act_CrtOpenMaslulExecute(Sender: TObject);
    procedure NExitClick(Sender: TObject);
    procedure Act_CrtOpenMehironExecute(Sender: TObject);
    procedure NEditSpeedBarClick(Sender: TObject);
    procedure Act_OpenTavlaExecute(Sender: TObject);
    procedure Act_HsbOpenMakavExecute(Sender: TObject);
    procedure Act_HsbPerutLakExecute(Sender: TObject);
    procedure Act_HsbCopyHeshbonitExecute(Sender: TObject);
    procedure Act_HsbHeshbonitExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagExecute(Sender: TObject);
    procedure Act_HsbEditMnameExecute(Sender: TObject);
    procedure Act_Kupa_KabalaExecute(Sender: TObject);
    procedure Act_AboutBoxExecute(Sender: TObject);
    procedure Act_MeholelOpenExecute(Sender: TObject);
    procedure NDefParamsClick(Sender: TObject);
    procedure Act_MeholelReportExecute(Sender: TObject);
    procedure Act_Util_CopyMehironExecute(Sender: TObject);
    procedure Act_DefRangeExecute(Sender: TObject);
    procedure Act_CrtOpenMahzevaExecute(Sender: TObject);
    procedure Act_HsbPerutHiuvNehagExecute(Sender: TObject);
    procedure Act_HsbHeshbonitNehagExecute(Sender: TObject);
    procedure Act_HsbCopyHeshbonitNehagExecute(Sender: TObject);
    procedure Act_CancelHeshbonLakExecute(Sender: TObject);
    procedure Act_CancelHeshbonNehagExecute(Sender: TObject);
    procedure Act_HsbOpenMakavNehagExecute(Sender: TObject);
    procedure Act_Interface_SonolSapExecute(Sender: TObject);
    procedure Act_Interface_SonolPremExecute(Sender: TObject);
    procedure Act_MeholelCloseMeholelExecute(Sender: TObject);
    procedure Act_HsbOpenMakavPreformaExecute(Sender: TObject);
    procedure Act_HsbSikumLakExecute(Sender: TObject);
    procedure Act_HsbSikumCopyLakExecute(Sender: TObject);
    procedure Act_HsbSikumPerutLakExecute(Sender: TObject);
    procedure Act_HsbPreformaPerutMerukazExecute(Sender: TObject);
    procedure Act_HsbPreformaMerukazExecute(Sender: TObject);
    procedure Act_Kupa_HafkadaExecute(Sender: TObject);
    procedure Act_Kupa_PrintCopyHafkadotExecute(Sender: TObject);
    procedure Act_Crt_OpenTavla2Execute(Sender: TObject);
    procedure Act_Kupa_CancelHafkadaExecute(Sender: TObject);
    procedure Act_Util_RaisePriceInTnuaExecute(Sender: TObject);
    procedure Act_HsbPerutDolarRegularExecute(Sender: TObject);
    procedure Act_HsbDolarExecute(Sender: TObject);
    procedure Act_HsbCopyDolarExecute(Sender: TObject);
    procedure Act_Interface_HanExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagMakeExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagCopyHeshbonitExecute(Sender: TObject);
    procedure Act_HsbZikuyLakPerutExecute(Sender: TObject);
    procedure Act_HsbZikuyLakMakeExecute(Sender: TObject);
    procedure Act_HsbZikuyLakCopyExecute(Sender: TObject);
    procedure Act_HsbZikuyLakCancelExecute(Sender: TObject);
    procedure Act_WinTudaAtmLakoExecute(Sender: TObject);
    procedure Act_MoDoh_RetzefTeudaExecute(Sender: TObject);
    procedure Act_HsbRikuz_LakExecute(Sender: TObject);
    procedure Act_HsbRikuz_NehagExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_PerutExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_MakeExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_CopyExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_CancelExecute(Sender: TObject);
    procedure Act_Interface_HanKabalaExecute(Sender: TObject);
    procedure Act_HsbPerforma_PerutExecute(Sender: TObject);
    procedure Act_HsbPerforma_MakeExecute(Sender: TObject);
    procedure Act_HsbPerforma_CopyExecute(Sender: TObject);
    procedure act_HsbPerforma_CancelExecute(Sender: TObject);
    procedure Act_HsbMerukazPerutExecute(Sender: TObject);
    procedure Act_HsbMerukazMakeExecute(Sender: TObject);
    procedure Act_HsbMerukazCopyExecute(Sender: TObject);
    procedure Act_HsbMerukazCancelExecute(Sender: TObject);
    procedure Act_HsbDolarCancelExecute(Sender: TObject);
    procedure Act_Util_DeleteMehironExecute(Sender: TObject);
    procedure Act_Meholel_ReportKupaExecute(Sender: TObject);
    procedure Act_CrtOpenShaarExecute(Sender: TObject);
    procedure Act_Interface_HanHafadaExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagCancelExecute(Sender: TObject);
    procedure Act_HsbZikuyXReportNehagExecute(Sender: TObject);
    procedure Act_EditExecuteMenuExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CrtClick(Sender: TObject);
  end;

// Dll Heshbon
{Procedure Edit_MName;External'WinHshbn.Dll';
Procedure Perot_To_Lak;External'WinHshbn.Dll';
Procedure CopyHshbonit;External'WinHshbn.Dll';
Procedure MakeHsbonit;External'WinHshbn.Dll';
Procedure Zikoy_To_Nehag;External'WinHshbn.Dll';
}


var
  Frm_MainWinTuda: TFrm_MainWinTuda;

implementation
{$R *.DFM}
Uses
    F_GlblWinTuda, Frm_WinTuda,AtmConst,AboutWinTuda,AtmRutin,
  F_WinTudaParam, F_WinTudaAtmLak;

procedure TFrm_MainWinTuda.Act_WinTudaExecute(Sender: TObject);
begin
  Act_WinTuda.Enabled:=False;
  Try
    ShowWinTuda(0);
  Finally
    Act_WinTuda.Enabled:=True;
  End;
end;

procedure TFrm_MainWinTuda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_MainWinTuda:=Nil;
     Action:=caFree;
     if FrmWinTuda<>Nil Then
     Begin
          FrmWinTuda.Free;
          FrmWinTuda:=Nil;
          //FrmWinTuda ��� ������� �������� �"� (DM,Glbl)
     End;
end;

procedure TFrm_MainWinTuda.Act_CrtOpenLakoExecute(Sender: TObject);
begin
     OpenCrtDll(fnLakoach,'');
end;

procedure TFrm_MainWinTuda.FormCreate(Sender: TObject);
begin
  InitAtmCrt(CurAliasName,DirectoryForIniFiles);
  BuildCrtMenu(NCrtMenu,WinTudaCurIniFileName,'Crt',CrtClick);
  Act_WinTudaAtmLako.Visible:=WinTudaSugSystem=ssMaz;
  ExeMenu1.IniFileName:=DirectoryForCurUser+ExeMenu1.IniFileName;
  ExeMenu1.FillMenu;
end;

procedure TFrm_MainWinTuda.FormDestroy(Sender: TObject);
begin
     DoneAtmCrt;
end;

procedure TFrm_MainWinTuda.Act_CrtOpenNehagExecute(Sender: TObject);
begin
     OpenCrtDll(fnNehag,'');
end;

procedure TFrm_MainWinTuda.Act_CrtOpenRehevExecute(Sender: TObject);
begin
     OpenCrtDll(fnRehev,'');
end;

procedure TFrm_MainWinTuda.Act_CrtOpenMaslulExecute(Sender: TObject);
begin
     OpenCrtDll(fnMaslul,'');
end;

procedure TFrm_MainWinTuda.NExitClick(Sender: TObject);
begin
     Close;
end;

procedure TFrm_MainWinTuda.Act_CrtOpenMehironExecute(Sender: TObject);
begin
     OpenCrtDll(fnMehiron,'');
end;

procedure TFrm_MainWinTuda.NEditSpeedBarClick(Sender: TObject);
begin
     SpeedBar1.Customize(0);
end;

procedure TFrm_MainWinTuda.Act_OpenTavlaExecute(Sender: TObject);
begin
     OpenCrtDll(fnTavla,'');
end;

procedure TFrm_MainWinTuda.Act_HsbOpenMakavExecute(Sender: TObject);
begin
     OpenCrtDll(fnMakav,'');
end;

procedure TFrm_MainWinTuda.Act_HsbPerutLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','Perot_To_Lak',True,H);
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(StandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyHeshbonitExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
//     RunDllNoParam('WinHshbn.Dll','CopyHshbonit',True,H);
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(StandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbHeshbonitExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','MakeHshbonit',True,H);
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(StandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','Zikuy_To_Nehag',True,H);
//     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbEditMnameExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','Edit_MName',True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_KabalaExecute(Sender: TObject);
begin
{     StrH:=DirectoryForExeFiles+'kabalot.exe';
     WinExec(PChar(StrH),SW_Restore);
}
//  RunDllProcPCharParam('Kabalot.Dll','ShowKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,WinTudaKabalaHandle);
   RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
   RunDllProcPCharParam('Kupa.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\'],True,WinTudaKabalaHandle);
end;

procedure TFrm_MainWinTuda.Act_AboutBoxExecute(Sender: TObject);
begin
     With TAboutBox.Create(Nil) Do
     Begin
          ShowModal;
          Free;
     End;

end;

procedure TFrm_MainWinTuda.Act_MeholelOpenExecute(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,WinTudaCurUserName,CurAliasName,False,WinTudaMeholelHandle);
     RunDllNoParam('Meholel.Dll','ShowMain',False,WinTudaMeholelHandle);
end;

procedure TFrm_MainWinTuda.NDefParamsClick(Sender: TObject);
begin
     With TFrm_WinTudaParams.Create(Self) Do
          ShowModal;
     // Free is done in the form onclose
end;

procedure TFrm_MainWinTuda.Act_MeholelReportExecute(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,WinTudaCurUserName,CurAliasName,False,WinTudaMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[''],False,WinTudaMeholelHandle);
end;

procedure TFrm_MainWinTuda.Act_Util_CopyMehironExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[WinTudaDBUserName,WinTudaDbPassword,CurAliasName,WinTudaSqlServerType],False,H);
     RunDllProcPCharParam('MhrnUtil.Dll','UpdateMehiron',['','',''],False,H);
     RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MainWinTuda.Act_DefRangeExecute(Sender: TObject);
begin
    OpenCrtDll(fnRange,'');
end;

procedure TFrm_MainWinTuda.Act_CrtOpenMahzevaExecute(Sender: TObject);
begin
    OpenCrtDll(fnMahzeva,'');
end;

procedure TFrm_MainWinTuda.Act_HsbPerutHiuvNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(HiyuvToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbHeshbonitNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(HiyuvToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyHeshbonitNehagExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(HiyuvToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_CancelHeshbonLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(StandardToLak)),True,H);
end;

procedure TFrm_MainWinTuda.Act_CancelHeshbonNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(HiyuvToNehag)),True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbOpenMakavNehagExecute(Sender: TObject);
begin
    OpenCrtDll(fnMakavNehag,'');
end;

procedure TFrm_MainWinTuda.Act_Interface_SonolSapExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('GetMessDll','Call_Tenuot_Sonol',[CurAliasName,WinTudaDBUserName,WinTudaDBPassword,WinTudaPrivateIniFileName,DirectoryForScripts+WinTudaSqlServerType+'\'],True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_SonolPremExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('GetMessDll','Call_Prem_Nehag_Sonol',[CurAliasName,WinTudaDBUserName,WinTudaDBPassword,WinTudaCurIniFileName],True,H);
end;

procedure TFrm_MainWinTuda.CrtClick(Sender: TObject);
Begin
    OpenCrt(TComponent(Sender).Tag,'');
End;
procedure TFrm_MainWinTuda.Act_MeholelCloseMeholelExecute(Sender: TObject);
begin
  if WinTudaMeholelHandle > 0 Then
    RunDllNoParam('Meholel.Dll','CloseMeholel',True,WinTudaMeholelHandle);
end;

procedure TFrm_MainWinTuda.Act_HsbOpenMakavPreformaExecute(
  Sender: TObject);
begin
  OpenCrtDll(fnMakavPreforma,'');
end;

procedure TFrm_MainWinTuda.Act_HsbSikumLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Sicum)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbSikumCopyLakExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Sicum)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbSikumPerutLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Sicum)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPreformaPerutMerukazExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','Perut_PrephormaMerukezet',Ord(TSugHesbonit(PrephormaMerukezet)),True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPreformaMerukazExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
//  RunDllNoParam('WinHshbn.Dll','DoHsbonitPrephormaMerukezet',True,H);
  RunDll1IntParam('WinHshbn.Dll','DoHsbonitPrephormaMerukezet',Ord(TSugHesbonit(PrephormaMerukezet)),True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_HafkadaExecute(Sender: TObject);
begin
  RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
  RunDllProcPCharParam('Kupa.Dll','Hafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,WinTudaKupaHandle);
end;

procedure TFrm_MainWinTuda.Act_Kupa_PrintCopyHafkadotExecute(
  Sender: TObject);
Var
  HafNum,YYear:String;
  IntH :LongInt;
begin
  HafNum:=InputBox('����� ���� �����','���� �����','');
  Try
    IntH:=-1;
    IntH:=StrToInt(HafNum);
  Except
  End;
    if IntH>-1 Then
    Begin
      YYear:=InputBox('����� ���� �����','��� �����',IntToStr(WinTudaYehusYear));
      Try
        IntH:=-1;
        IntH:=StrToInt(YYear);
      Except
      End;
      if IntH>-1 Then
      Begin
        RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
        RunDllProcPCharParam('Kupa.Dll','PrintCopyHafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,HafNum,YYear,'False'],False,WinTudaKupaHandle);
      End;
    End;
end;

procedure TFrm_MainWinTuda.Act_Crt_OpenTavla2Execute(Sender: TObject);
begin
  OpenCrtDll(fnTavla2,'');
end;

procedure TFrm_MainWinTuda.Act_Kupa_CancelHafkadaExecute(Sender: TObject);
Var
  HafNum,YYear:String;
  IntH :LongInt;
begin
  HafNum:=InputBox('����� �����','���� �����','');
  Try
    IntH:=-1;
    IntH:=StrToInt(HafNum);
  Except
  End;
  if IntH>-1 Then
  Begin
    if IntH>-1 Then
    Begin
      YYear:=InputBox('����� ���� �����','��� �����',IntToStr(WinTudaYehusYear));
      Try
        IntH:=-1;
        IntH:=StrToInt(YYear);
      Except
      End;
      if IntH>-1 Then
      Begin
        RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
        RunDllProcPCharParam('Kupa.Dll','CancelHafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,HafNum,YYear],False,WinTudaKupaHandle);
      End;
    End;
  End;
end;

procedure TFrm_MainWinTuda.Act_Util_RaisePriceInTnuaExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[WinTudaDBUserName,WinTudaDbPassword,CurAliasName,WinTudaSqlServerType],False,H);
     RunDllProcPCharParam('MhrnUtil.Dll','DoUPdatePriceInTnua',[DirectoryForScripts+WinTudaSqlServerType+'\'],False,H);
     RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerutDolarRegularExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbDolarExecute(Sender: TObject);
Var
   H :THandle;
begin
   H:=0;
   RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyDolarExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_HanExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Han.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Han.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Han.Dll','DoneAtmHan',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','Make_Zikuy_To_Nehag',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagCopyHeshbonitExecute(
  Sender: TObject);
Var
   H :THandle;
begin
   H:=0;
   RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakPerutExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyShekel)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(ZikuyShekel)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakCopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyShekel)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyShekel)),True,H);
end;

procedure TFrm_MainWinTuda.Act_WinTudaAtmLakoExecute(Sender: TObject);
begin
  if Frm_AtmShibLak=Nil Then
    Frm_AtmShibLak:=TFrm_AtmShibLak.Create(Nil);
  Frm_AtmShibLak.Show;
end;

procedure TFrm_MainWinTuda.Act_MoDoh_RetzefTeudaExecute(Sender: TObject);
begin
  RunDllFunc1StringParam('MoDoh.Dll','ShowRetzef','ShibTuda');
end;

procedure TFrm_MainWinTuda.Act_HsbRikuz_LakExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','RikuzForLakNehag',0,-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbRikuz_NehagExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','RikuzForLakNehag',1,1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_PerutExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyDolar)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_MakeExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(ZikuyDolar)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_CopyExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyDolar)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_CancelExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyDolar)),True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_HanKabalaExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Kab.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Kab.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Kab.Dll','DoneAtmHan',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerforma_PerutExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Prephorma)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerforma_MakeExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Prephorma)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerforma_CopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Prephorma)),-1,True,H);
end;

procedure TFrm_MainWinTuda.act_HsbPerforma_CancelExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(Prephorma)),True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazPerutExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazCopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbDolarCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_Util_DeleteMehironExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[WinTudaDBUserName,WinTudaDbPassword,CurAliasName,WinTudaSqlServerType],False,H);
  RunDllNoParam('MhrnUtil.Dll','DelMehiron',False,H);
  RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MainWinTuda.Act_Meholel_ReportKupaExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDllProcPCharParam('Kopa_Report.Dll','ReportKopa_Init',[CurAliasName,WinTudaDBUserName,WinTudaDbPassword,WinTudaPrivateIniFileName,DirectoryForScripts+WinTudaSqlServerType+'\'],False,H);
  RunDllNoParam('Kopa_Report.Dll','ReportKopa_Done',True,H);
end;

procedure TFrm_MainWinTuda.Act_CrtOpenShaarExecute(Sender: TObject);
begin
  OpenCrtDll(fnShaar,'');
end;

procedure TFrm_MainWinTuda.Act_Interface_HanHafadaExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Haf.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Haf.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Haf.Dll','DoneAtmHan',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyToNehag)),True,H);
End;
procedure TFrm_MainWinTuda.Act_HsbZikuyXReportNehagExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','Perut_PrephormaMerukezet',Ord(TSugHesbonit(PrephormaMerukezetToNehag)),True,H);
end;

procedure TFrm_MainWinTuda.Act_EditExecuteMenuExecute(Sender: TObject);
begin
  exeMenu1.EditExeList;
end;

end.

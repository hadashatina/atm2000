unit F_Station;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, Mask, ComCtrls, DBTables, ExtCtrls,
  AtmAdvTable, AtmComp, ToolWin, AdvSearch, Grids, DBGrids, AtmLookCombo,
  MslDllInterface, Scale250, RxQuery, AtmRxQuery;

type
  TFrm_Stations = class(TForm)
    Tbl_StationCODE_RECORD: TAutoIncField;
    Tbl_StationSEMEL_YESHUV: TIntegerField;
    Tbl_StationSHEM_YESHUV: TStringField;
    Tbl_StationCODE_STAITION: TIntegerField;
    Tbl_StationNAME_STATION: TStringField;
    Tbl_StationX: TIntegerField;
    Tbl_StationY: TIntegerField;
    DataSource1: TDataSource;
    Panel2: TPanel;
    Tbl_Station: TAtmRxQuery;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    Tbl_StationSUG_STATION: TIntegerField;
    Tbl_StationRASHUT_NUM: TIntegerField;
    Tbl_StationString_1: TStringField;
    Tbl_StationString_2: TStringField;
    Tbl_StationString_3: TStringField;
    Tbl_StationString_4: TStringField;
    Tbl_StationString_5: TStringField;
    Tbl_StationString_6: TStringField;
    Tbl_StationString_7: TStringField;
    Tbl_StationString_8: TStringField;
    Tbl_StationString_9: TStringField;
    Tbl_StationString_10: TStringField;
    Tbl_StationInteger_1: TIntegerField;
    Tbl_StationInteger_2: TIntegerField;
    Tbl_StationInteger_3: TIntegerField;
    Tbl_StationInteger_4: TIntegerField;
    Tbl_StationInteger_5: TIntegerField;
    Tbl_StationInteger_6: TIntegerField;
    Tbl_StationInteger_7: TIntegerField;
    Tbl_StationInteger_8: TIntegerField;
    Tbl_StationInteger_9: TIntegerField;
    Tbl_StationInteger_10: TIntegerField;
    Tbl_StationDate_1: TDateTimeField;
    Tbl_StationDate_2: TDateTimeField;
    Tbl_StationDate_3: TDateTimeField;
    Tbl_StationDate_5: TDateTimeField;
    Tbl_StationDate_6: TDateTimeField;
    Tbl_StationDate_7: TDateTimeField;
    Tbl_StationDate_8: TDateTimeField;
    Tbl_StationDate_9: TDateTimeField;
    Tbl_StationDate_10: TDateTimeField;
    Tbl_StationBcd_1: TBCDField;
    Tbl_StationBcd_2: TBCDField;
    Tbl_StationBcd_3: TBCDField;
    Tbl_StationBcd_4: TBCDField;
    Tbl_StationBcd_5: TBCDField;
    Tbl_StationBcd_6: TBCDField;
    Tbl_StationBcd_7: TBCDField;
    Tbl_StationBcd_8: TBCDField;
    Tbl_StationBcd_9: TBCDField;
    Tbl_StationBcd_10: TBCDField;
    AtmAdvSearch_SemelYeshuv: TAtmAdvSearch;
    Tbl_StationNodeNumB: TIntegerField;
    Tbl_StationPopulation: TIntegerField;
    Tbl_StationNodeKodCityName: TIntegerField;
    Tbl_StationNodeSugSystem: TIntegerField;
    Tbl_StationName_engl: TStringField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    StatusBar1: TStatusBar;
    Tbl_StationLookRashutNum: TStringField;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EditCODE_RECORD: TAtmDbHEdit;
    EditSEMEL_YESHUV: TAtmDbHEdit;
    EditSHEM_YESHUV: TAtmDbHEdit;
    EditCODE_STAITION: TAtmDbHEdit;
    EditNAME_STATION: TAtmDbHEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    EditX: TAtmDbHEdit;
    EditY: TAtmDbHEdit;
    AtmDbHEdit_SemelRashut: TAtmDbHEdit;
    AtmDbComboBox_RashutNum: TAtmDbComboBox;
    AtmDbComboBox_SugStation: TAtmDbComboBox;
    TBtn_MslChoosePoint: TToolButton;
    Scale1: TScale;
    TBrn_Report: TToolButton;
    UpdateSQL_Station: TUpdateSQL;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_StationAfterInsert(DataSet: TDataSet);
    procedure Tbl_StationAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtmAdvSearch_SemelYeshuvAfterExecute(Sender: TObject);
    procedure Tbl_StationSEMEL_YESHUVValidate(Sender: TField);
    procedure TBtn_MslChoosePointClick(Sender: TObject);
    procedure DataSource1StateChange(Sender: TObject);
    procedure Tbl_StationAfterScroll(DataSet: TDataSet);
    procedure TBrn_ReportClick(Sender: TObject);
  private
    { private declarations }
    Procedure PutNodeDataInRecord(TheNode:TNode);
  public
    { public declarations }
    Procedure BuildLookupList;
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_Stations: TFrm_Stations;

implementation
Uses Crt_Glbl,AtmRutin,DMAtmCrt,AtmConst;
{$R *.DFM}

procedure TFrm_Stations.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     TBtn_MslChoosePoint.Enabled:=MslDllLoaded;
     BuildLookupList;
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataset).Open;
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_Stations.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataset).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_Stations.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Station);
end;

procedure TFrm_Stations.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
  if Button=nbPost Then
    ((Sender As TDBNavigator).DataSource.DataSet As TAtmRxQuery).SavePressed:=True;
  if Button=nbRefresh Then
  Begin
    ((Sender As TDBNavigator).DataSource.DataSet As TAtmRxQuery).Close;
    ((Sender As TDBNavigator).DataSource.DataSet As TAtmRxQuery).Open;
    Abort;
  End
end;

procedure TFrm_Stations.Tbl_StationAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     //FindFirstControlInTabOrder(Self).SetFocus;
     EditSEMEL_YESHUV.SetFocus;
end;

procedure TFrm_Stations.Tbl_StationAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     if Tbl_StationCODE_STAITION.AsString='' Then
     Begin
          Tbl_Station.Edit;
          Tbl_StationCODE_STAITION.AsString:=Tbl_StationCODE_RECORD.AsString;
          Tbl_Station.Post;
     End;
end;


procedure TFrm_Stations.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_Stations:=Nil;
     Action:=caFree;
end;

procedure TFrm_Stations.AtmAdvSearch_SemelYeshuvAfterExecute(
  Sender: TObject);
begin
     if Not AtmAdvSearch_SemelYeshuv.Success Then
        Exit;
     if Tbl_Station.State=dsBrowse Then
        Tbl_Station.Edit;
     if (Tbl_StationSEMEL_YESHUV.IsNull) Or (ActiveControl=EditSEMEL_YESHUV) Then
        Tbl_StationSEMEL_YESHUV.AsString:=GetFieldFromSeparateString(AtmAdvSearch_SemelYeshuv.ReturnString,',',2);
     if (Tbl_StationSHEM_YESHUV.IsNull) Or (ActiveControl=EditSHEM_YESHUV) Then
        Tbl_StationSHEM_YESHUV.AsString:=GetFieldFromSeparateString(AtmAdvSearch_SemelYeshuv.ReturnString,',',1);
     if (Tbl_StationRASHUT_NUM.IsNull) Or (ActiveControl=AtmDbHEdit_SemelRashut) Then
        Tbl_StationRASHUT_NUM.AsString:=GetFieldFromSeparateString(AtmAdvSearch_SemelYeshuv.ReturnString,',',3);

end;

procedure TFrm_Stations.Tbl_StationSEMEL_YESHUVValidate(Sender: TField);
begin
     if Tbl_StationSEMEL_YESHUV.AsString='' Then
        Exit;
     if Tbl_StationCODE_STAITION.IsNull Then
     With DM_AtmCrt.Qry_Temp Do
     Begin //���� ���� ��� �����
          Active:=False;
          Sql.Clear;
          Sql.Add('Select Max(CODE_STAITION) MCS');
          Sql.Add('From STATIONS');
          Sql.Add('Where SEMEL_YESHUV='+Tbl_StationSEMEL_YESHUV.AsString);
          Active:=True;
          Tbl_StationCODE_STAITION.AsInteger:=DM_AtmCrt.Qry_Temp.FieldByName('MCS').AsInteger+1;
          Active:=False;
     End;

     With DM_AtmCrt.Qry_Temp Do
     Begin
          Active:=False;
          Sql.Clear;
          Sql.Add('Select *');
          Sql.Add('From STATIONS');
          Sql.Add('Where SEMEL_YESHUV='+Tbl_StationSEMEL_YESHUV.AsString);
          Active:=True;

          Tbl_StationSHEM_YESHUV.AsString:=FieldByName('SHEM_YESHUV').AsString;
          if Tbl_StationX.IsNull Then
          Begin
             Tbl_StationX.AsString:=FieldByName('X').AsString;
             Tbl_StationY.AsString:=FieldByName('Y').AsString;
          End;
          if Tbl_StationRASHUT_NUM.IsNull Then
             Tbl_StationRASHUT_NUM.AsString:=FieldByName('RASHUT_NUM').AsString;

          Active:=False;
    End;
end;

Procedure TFrm_Stations.BuildLookupList;
Var
   SList:TStringList;
Begin
     DM_AtmCrt.Qry_Temp.Active:=False;
     DM_AtmCrt.Qry_Temp.Sql.Clear;
     DM_AtmCrt.Qry_Temp.Sql.Add('Select * From KodTvla2');
     DM_AtmCrt.Qry_Temp.Active:=True;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(ST2_Rashut),AtmDbComboBox_RashutNum);
     FillKodTavlaComboList(DM_AtmCrt.Qry_Temp,SList);

     SList.Clear;
     SList.AddObject(IntToStr(ST2_Rashut),Tbl_StationLookRashutNum);
     FillKodTavlaLookupList(DM_AtmCrt.Qry_Temp,SList);

     SList.Clear;
     SList.AddObject(IntToStr(SugTavla_SugStation),AtmDbComboBox_SugStation);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);

     SList.Free;
End;
procedure TFrm_Stations.TBtn_MslChoosePointClick(Sender: TObject);
Var
   TheNode :TNode;
begin
     if Not MslDllLoaded Then
        Exit;
     StatusBar1.SimpleText:='���� �������...';
     Application.ProcessMessages;
     if (Tbl_StationX.AsInteger>0) And (Tbl_StationY.AsInteger>0) Then
        MslGotoXY(Tbl_StationX.AsInteger,Tbl_StationY.AsInteger);
     Application.ProcessMessages;
     if MslChoosePoint(TheNode) Then
     Begin
          if Tbl_Station.State=dsBrowse Then
              Tbl_Station.Edit;
          StatusBar1.SimpleText:=TheNode.NameOfPoint;
          PutNodeDataInRecord(TheNode);
     End;
     StatusBar1.SimpleText:='';
end;

Procedure TFrm_Stations.PutNodeDataInRecord(TheNode:TNode);
Begin
     Tbl_StationNodeNumB.AsInteger:=TheNode.Numb;
     Tbl_StationNodeKodCityName.AsInteger:=TheNode.KodCityName;
     Tbl_StationNodeSugSystem.AsInteger:=TheNode.SugSystem;
     Tbl_StationX.AsInteger:=TheNode.Crd.X;
     Tbl_StationY.AsInteger:=TheNode.Crd.Y;
End;

procedure TFrm_Stations.DataSource1StateChange(Sender: TObject);
begin
     StatusBar1.SimpleText:='';
end;

procedure TFrm_Stations.Tbl_StationAfterScroll(DataSet: TDataSet);
begin
     Statusbar1.SimpleText:='';
end;

Procedure TFrm_Stations.ShowForm(FormParams:ShortString);
Begin
     if FormParams<>'' Then
          Tbl_Station.FindKey([FormParams]);
     Show;
End;

procedure TFrm_Stations.TBrn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msStation],False,CrtMeholelHandle);
end;

end.

object Frm_newlak: TFrm_newlak
  Left = 5
  Top = 73
  Width = 635
  Height = 379
  Caption = '����-���'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 627
    Height = 57
    Align = alTop
    TabOrder = 4
    object Label3: TLabel
      Left = 534
      Top = 32
      Width = 55
      Height = 13
      Caption = '��� �����'
    end
    object Label2: TLabel
      Left = 384
      Top = 8
      Width = 45
      Height = 13
      Caption = '�� ����'
    end
    object Label1: TLabel
      Left = 541
      Top = 8
      Width = 48
      Height = 13
      Caption = '��� ����'
    end
  end
  object Kod_lak: TAtmDbHEdit
    Left = 456
    Top = 4
    Width = 65
    Height = 21
    BiDiMode = bdRightToLeft
    DataField = 'Kod_Lakoach'
    DataSource = Ds_lakoach
    ParentBiDiMode = False
    TabOrder = 0
    OnExit = Kod_lakExit
    UseF2ToRunSearch = True
    IsFixed = False
    IsMust = True
    DefaultKind = dkString
    KeyToRunSearch = ksF2
    LinkLabel = Label1
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    FixedColor = clBtnFace
    NormalColor = clWindow
    SearchComponent = AtmAdvSearch_lak
    SelectNextAfterSearch = True
    StatusBarPanelNum = -1
    Hebrew = False
    LocateRecordOnChange = True
  end
  object Shem_lak: TAtmDbHEdit
    Left = 180
    Top = 5
    Width = 142
    Height = 21
    BiDiMode = bdRightToLeft
    DataField = 'Shem_Lakoach'
    DataSource = Ds_lakoach
    ParentBiDiMode = False
    TabOrder = 1
    UseF2ToRunSearch = False
    IsFixed = False
    IsMust = False
    DefaultKind = dkString
    KeyToRunSearch = ksF2
    LinkLabel = Label2
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    FixedColor = clBtnFace
    NormalColor = clWindow
    SelectNextAfterSearch = True
    StatusBarPanelNum = -1
    Hebrew = False
    LocateRecordOnChange = False
  end
  object AtmDbHEdit1: TAtmDbHEdit
    Left = 456
    Top = 30
    Width = 65
    Height = 21
    BiDiMode = bdRightToLeft
    DataField = 'Kod_Kvutza'
    DataSource = Ds_lakoach
    ParentBiDiMode = False
    TabOrder = 2
    UseF2ToRunSearch = True
    IsFixed = False
    IsMust = False
    DefaultKind = dkString
    KeyToRunSearch = ksF2
    LinkLabel = Label3
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    FixedColor = clBtnFace
    NormalColor = clWindow
    SearchComponent = DM_AtmCrt.AtmAdvSearch_KodTavla
    SelectNextAfterSearch = True
    StatusBarPanelNum = -1
    Hebrew = False
    BeforeExecuteSearch = AtmDbHEdit1BeforeExecuteSearch
    LocateRecordOnChange = False
  end
  object AtmDbComboBoxGroup: TAtmDbComboBox
    Left = 293
    Top = 31
    Width = 145
    Height = 21
    BiDiMode = bdRightToLeft
    ItemHeight = 13
    ParentBiDiMode = False
    Sorted = True
    TabOrder = 3
    DataSource = Ds_lakoach
    DataField = 'Kod_Kvutza'
    SugTavlaIndex = 0
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 57
    Width = 627
    Height = 250
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 5
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = '����� ����'
      object Label4: TLabel
        Left = 573
        Top = 9
        Width = 33
        Height = 13
        Caption = '�����'
      end
      object AtmDbHEdit2: TAtmDbHEdit
        Left = 435
        Top = 5
        Width = 121
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Ktovet_1'
        DataSource = Ds_lakoach
        ParentBiDiMode = False
        TabOrder = 0
        UseF2ToRunSearch = False
        IsFixed = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label4
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = '���� �����'
      ImageIndex = 1
      object Label5: TLabel
        Left = 550
        Top = 8
        Width = 52
        Height = 13
        Caption = '��'#39' �����'
      end
      object AtmDbHEdit3: TAtmDbHEdit
        Left = 400
        Top = 4
        Width = 121
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Mis_Taktziv'
        DataSource = Ds_lakoach
        ParentBiDiMode = False
        TabOrder = 0
        UseF2ToRunSearch = False
        IsFixed = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label5
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 333
    Width = 627
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 307
    Width = 627
    Height = 26
    Align = alBottom
    ButtonWidth = 59
    Caption = 'ToolBar1'
    Flat = True
    Images = DM_AtmCrt.ImageList_CrtButtons
    List = True
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 7
    object AtmDbNavigator1: TAtmDbNavigator
      Left = 0
      Top = 0
      Width = 233
      Height = 22
      VisibleBtns.nbBkSet = False
      VisibleBtns.nbBkGoto = False
      VisibleBtns.nbBkClear = False
      VisibleBtns.nbPriorSet = False
      VisibleBtns.nbNextSet = False
      DataSource = Ds_lakoach
      Align = alLeft
      Flat = True
      TabOrder = 0
    end
    object ToolButton1: TToolButton
      Left = 233
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object TBtn_OpenSearch: TToolButton
      Left = 241
      Top = 0
      Action = DM_AtmCrt.Action_OpenSearch
      AutoSize = True
    end
  end
  object DynamicControls1: TDynamicControls
    IniFileName = 'Frm_newlak'
    RegistryKey = 'Software\Atm\Atm2000\Forms\FrmLakoach'
    SaveIniFileNameToRegistry = True
    ExtraFieldsParent = Owner
    ExtraFiledsFileName = 'Lakoach.Xtr'
    Left = 320
    Top = 56
  end
  object Tbl_lakoach: TAtmRxQuery
    Tag = 1
    CachedUpdates = True
    AfterInsert = Tbl_lakoachAfterInsert
    AfterPost = Tbl_lakoachAfterPost
    AfterScroll = Tbl_lakoachAfterScroll
    OnNewRecord = Tbl_lakoachNewRecord
    AutoRefresh = True
    DatabaseName = 'DB_AtmCrt'
    ParamCheck = False
    SQL.Strings = (
      'Select * '
      'From Lakoach'
      'Where %MWhereIndex'
      'Order By %OrderFields')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL1
    Macros = <
      item
        DataType = ftString
        Name = 'MWhereIndex'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'OrderFields'
        ParamType = ptInput
        Value = 'KOD_LAKOACH'
      end>
    IndexFieldNames = 'KOD_LAKOACH'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = True
    AutoValidateLookupFields = True
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = True
    Left = 248
    Top = 96
    object Tbl_lakoachKod_Lakoach: TIntegerField
      FieldName = 'Kod_Lakoach'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Lakoach'
    end
    object Tbl_lakoachKod_Kvutza: TIntegerField
      FieldName = 'Kod_Kvutza'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Kvutza'
    end
    object Tbl_lakoachKod_Miyun: TIntegerField
      FieldName = 'Kod_Miyun'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Miyun'
    end
    object Tbl_lakoachMis_Han: TStringField
      FieldName = 'Mis_Han'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mis_Han'
      Size = 15
    end
    object Tbl_lakoachShem_Lakoach: TStringField
      FieldName = 'Shem_Lakoach'
      Origin = 'DB_ATMCRT."Lakoach.DB".Shem_Lakoach'
      Size = 30
    end
    object Tbl_lakoachKtovet_1: TStringField
      FieldName = 'Ktovet_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Ktovet_1'
      Size = 30
    end
    object Tbl_lakoachMis_Bait1: TStringField
      FieldName = 'Mis_Bait1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mis_Bait1'
      Size = 7
    end
    object Tbl_lakoachIr_1: TStringField
      FieldName = 'Ir_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Ir_1'
      Size = 15
    end
    object Tbl_lakoachMikud_1: TIntegerField
      FieldName = 'Mikud_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mikud_1'
    end
    object Tbl_lakoachKtovet_2: TStringField
      FieldName = 'Ktovet_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Ktovet_2'
      Size = 30
    end
    object Tbl_lakoachMis_Bait2: TStringField
      FieldName = 'Mis_Bait2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mis_Bait2'
      Size = 7
    end
    object Tbl_lakoachIr_2: TStringField
      FieldName = 'Ir_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Ir_2'
      Size = 15
    end
    object Tbl_lakoachMikud_2: TIntegerField
      FieldName = 'Mikud_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mikud_2'
    end
    object Tbl_lakoachTel_1: TStringField
      FieldName = 'Tel_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Tel_1'
      Size = 13
    end
    object Tbl_lakoachTel_2: TStringField
      FieldName = 'Tel_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Tel_2'
      Size = 13
    end
    object Tbl_lakoachFax: TStringField
      FieldName = 'Fax'
      Origin = 'DB_ATMCRT."Lakoach.DB".Fax'
      Size = 13
    end
    object Tbl_lakoachPle_Phon: TStringField
      FieldName = 'Ple_Phon'
      Origin = 'DB_ATMCRT."Lakoach.DB".Ple_Phon'
      Size = 13
    end
    object Tbl_lakoachIsh_Kesher: TStringField
      FieldName = 'Ish_Kesher'
      Origin = 'DB_ATMCRT."Lakoach.DB".Ish_Kesher'
    end
    object Tbl_lakoachOsek_Morshe_ID: TStringField
      FieldName = 'Osek_Morshe_ID'
      Origin = 'DB_ATMCRT."Lakoach.DB".Osek_Morshe_ID'
      Size = 12
    end
    object Tbl_lakoachTaarich_Osek_Morshe: TDateTimeField
      FieldName = 'Taarich_Osek_Morshe'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Osek_Morshe'
    end
    object Tbl_lakoachKod_Tnai_Tashlum: TIntegerField
      FieldName = 'Kod_Tnai_Tashlum'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Tnai_Tashlum'
    end
    object Tbl_lakoachKod_Eara: TIntegerField
      FieldName = 'Kod_Eara'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Eara'
    end
    object Tbl_lakoachMas_Makor: TBCDField
      FieldName = 'Mas_Makor'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mas_Makor'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachTaarich_Mas: TDateTimeField
      FieldName = 'Taarich_Mas'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Mas'
    end
    object Tbl_lakoachAchuz_Anacha_Amala: TBCDField
      FieldName = 'Achuz_Anacha_Amala'
      Origin = 'DB_ATMCRT."Lakoach.DB".Achuz_Anacha_Amala'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachSug_Anacha_Amala: TIntegerField
      FieldName = 'Sug_Anacha_Amala'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Anacha_Amala'
    end
    object Tbl_lakoachAchuz_Anacha_Amala_2: TBCDField
      FieldName = 'Achuz_Anacha_Amala_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Achuz_Anacha_Amala_2'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachKod_Kesher: TIntegerField
      FieldName = 'Kod_Kesher'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Kesher'
    end
    object Tbl_lakoachTaarich_Atchala: TDateTimeField
      FieldName = 'Taarich_Atchala'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Atchala'
    end
    object Tbl_lakoachTaarich_Siyum: TDateTimeField
      FieldName = 'Taarich_Siyum'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Siyum'
    end
    object Tbl_lakoachEarot: TMemoField
      FieldName = 'Earot'
      Origin = 'DB_ATMCRT."Lakoach.DB".Earot'
      BlobType = ftMemo
      Size = 30
    end
    object Tbl_lakoachKod_Banehag: TIntegerField
      FieldName = 'Kod_Banehag'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Banehag'
    end
    object Tbl_lakoachKod_Basapak: TIntegerField
      FieldName = 'Kod_Basapak'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Basapak'
    end
    object Tbl_lakoachKod_Atzmada: TIntegerField
      FieldName = 'Kod_Atzmada'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_Atzmada'
    end
    object Tbl_lakoachKod_mechiron: TIntegerField
      FieldName = 'Kod_mechiron'
      Origin = 'DB_ATMCRT."Lakoach.DB".Kod_mechiron'
    end
    object Tbl_lakoachSug_Rikuz: TStringField
      FieldName = 'Sug_Rikuz'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Rikuz'
      Size = 40
    end
    object Tbl_lakoachSug_Koteret: TIntegerField
      FieldName = 'Sug_Koteret'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Koteret'
    end
    object Tbl_lakoachSug_Miyun: TIntegerField
      FieldName = 'Sug_Miyun'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Miyun'
    end
    object Tbl_lakoachMis_Taktziv: TStringField
      FieldName = 'Mis_Taktziv'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mis_Taktziv'
      Size = 25
    end
    object Tbl_lakoachChoze: TStringField
      FieldName = 'Choze'
      Origin = 'DB_ATMCRT."Lakoach.DB".Choze'
      Size = 25
    end
    object Tbl_lakoachSug_Bitachon: TIntegerField
      FieldName = 'Sug_Bitachon'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Bitachon'
    end
    object Tbl_lakoachSchum_Bitachon: TBCDField
      FieldName = 'Schum_Bitachon'
      Origin = 'DB_ATMCRT."Lakoach.DB".Schum_Bitachon'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachTaarich_Siyun_Bitachon: TDateTimeField
      FieldName = 'Taarich_Siyun_Bitachon'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Siyun_Bitachon'
    end
    object Tbl_lakoachTosefet_Achuz: TBCDField
      FieldName = 'Tosefet_Achuz'
      Origin = 'DB_ATMCRT."Lakoach.DB".Tosefet_Achuz'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachTosefet_Melel: TStringField
      FieldName = 'Tosefet_Melel'
      Origin = 'DB_ATMCRT."Lakoach.DB".Tosefet_Melel'
      Size = 30
    end
    object Tbl_lakoachSug_Madad_1: TIntegerField
      FieldName = 'Sug_Madad_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Madad_1'
    end
    object Tbl_lakoachTaarich_Madad_1: TDateTimeField
      FieldName = 'Taarich_Madad_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Madad_1'
    end
    object Tbl_lakoachMadad_Basis_1: TBCDField
      FieldName = 'Madad_Basis_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Madad_Basis_1'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMutzmad_Achuz_1: TBCDField
      FieldName = 'Mutzmad_Achuz_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mutzmad_Achuz_1'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachSug_Madad_2: TIntegerField
      FieldName = 'Sug_Madad_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Madad_2'
    end
    object Tbl_lakoachTaarich_Madad_2: TDateTimeField
      FieldName = 'Taarich_Madad_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_Madad_2'
    end
    object Tbl_lakoachMadad_Basis_2: TBCDField
      FieldName = 'Madad_Basis_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Madad_Basis_2'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMutzmad_Achuz_2: TBCDField
      FieldName = 'Mutzmad_Achuz_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mutzmad_Achuz_2'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachSug_Madad_3: TIntegerField
      FieldName = 'Sug_Madad_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Sug_Madad_3'
    end
    object Tbl_lakoachTaarich_madad_3: TDateTimeField
      FieldName = 'Taarich_madad_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Taarich_madad_3'
    end
    object Tbl_lakoachMadad_Basis_3: TBCDField
      FieldName = 'Madad_Basis_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Madad_Basis_3'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMutzmad_Achuz_3: TBCDField
      FieldName = 'Mutzmad_Achuz_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Mutzmad_Achuz_3'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachYatza_Teudat_Mishloach: TBCDField
      FieldName = 'Yatza_Teudat_Mishloach'
      Origin = 'DB_ATMCRT."Lakoach.DB".Yatza_Teudat_Mishloach'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachYatza_Cheshbon_Lo_Shula: TBCDField
      FieldName = 'Yatza_Cheshbon_Lo_Shula'
      Origin = 'DB_ATMCRT."Lakoach.DB".Yatza_Cheshbon_Lo_Shula'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachChekim_Dachui_Lo_Nifra: TBCDField
      FieldName = 'Chekim_Dachui_Lo_Nifra'
      Origin = 'DB_ATMCRT."Lakoach.DB".Chekim_Dachui_Lo_Nifra'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachX: TIntegerField
      FieldName = 'X'
      Origin = 'DB_ATMCRT."Lakoach.DB".X'
    end
    object Tbl_lakoachY: TIntegerField
      FieldName = 'Y'
      Origin = 'DB_ATMCRT."Lakoach.DB".Y'
    end
    object Tbl_lakoachNodeNumB: TIntegerField
      FieldName = 'NodeNumB'
      Origin = 'DB_ATMCRT."Lakoach.DB".NodeNumB'
    end
    object Tbl_lakoachNodeSugSystem: TIntegerField
      FieldName = 'NodeSugSystem'
      Origin = 'DB_ATMCRT."Lakoach.DB".NodeSugSystem'
    end
    object Tbl_lakoachNodeKodCityName: TIntegerField
      FieldName = 'NodeKodCityName'
      Origin = 'DB_ATMCRT."Lakoach.DB".NodeKodCityName'
    end
    object Tbl_lakoachString_1: TStringField
      FieldName = 'String_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_1'
      Size = 30
    end
    object Tbl_lakoachString_2: TStringField
      FieldName = 'String_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_2'
      Size = 30
    end
    object Tbl_lakoachString_3: TStringField
      FieldName = 'String_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_3'
      Size = 30
    end
    object Tbl_lakoachString_4: TStringField
      FieldName = 'String_4'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_4'
      Size = 30
    end
    object Tbl_lakoachString_5: TStringField
      FieldName = 'String_5'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_5'
      Size = 30
    end
    object Tbl_lakoachString_6: TStringField
      FieldName = 'String_6'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_6'
      Size = 30
    end
    object Tbl_lakoachString_7: TStringField
      FieldName = 'String_7'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_7'
      Size = 30
    end
    object Tbl_lakoachString_8: TStringField
      FieldName = 'String_8'
      Origin = 'DB_ATMCRT."Lakoach.DB".String_8'
      Size = 30
    end
    object Tbl_lakoachInteger_1: TIntegerField
      FieldName = 'Integer_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Integer_1'
    end
    object Tbl_lakoachInteger_2: TIntegerField
      FieldName = 'Integer_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Integer_2'
    end
    object Tbl_lakoachInteger_3: TIntegerField
      FieldName = 'Integer_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Integer_3'
    end
    object Tbl_lakoachInteger_4: TIntegerField
      FieldName = 'Integer_4'
      Origin = 'DB_ATMCRT."Lakoach.DB".Integer_4'
    end
    object Tbl_lakoachInteger_5: TIntegerField
      FieldName = 'Integer_5'
      Origin = 'DB_ATMCRT."Lakoach.DB".Integer_5'
    end
    object Tbl_lakoachDate_1: TDateTimeField
      FieldName = 'Date_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Date_1'
    end
    object Tbl_lakoachDate_2: TDateTimeField
      FieldName = 'Date_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Date_2'
    end
    object Tbl_lakoachDate_3: TDateTimeField
      FieldName = 'Date_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Date_3'
    end
    object Tbl_lakoachDate_4: TDateTimeField
      FieldName = 'Date_4'
      Origin = 'DB_ATMCRT."Lakoach.DB".Date_4'
    end
    object Tbl_lakoachDate_5: TDateTimeField
      FieldName = 'Date_5'
      Origin = 'DB_ATMCRT."Lakoach.DB".Date_5'
    end
    object Tbl_lakoachMoney_1: TBCDField
      FieldName = 'Money_1'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_1'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMoney_2: TBCDField
      FieldName = 'Money_2'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_2'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMoney_3: TBCDField
      FieldName = 'Money_3'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_3'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMoney_4: TBCDField
      FieldName = 'Money_4'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_4'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMoney_5: TBCDField
      FieldName = 'Money_5'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_5'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMoney_6: TBCDField
      FieldName = 'Money_6'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_6'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachMoney_7: TBCDField
      FieldName = 'Money_7'
      Origin = 'DB_ATMCRT."Lakoach.DB".Money_7'
      Precision = 32
      Size = 3
    end
    object Tbl_lakoachInt_Kod_Kvutza: TIntegerField
      FieldName = 'Int_Kod_Kvutza'
      Origin = 'DB_ATMCRT."Lakoach.DB".Int_Kod_Kvutza'
    end
    object Tbl_lakoachInt_Kod_Miyun: TIntegerField
      FieldName = 'Int_Kod_Miyun'
      Origin = 'DB_ATMCRT."Lakoach.DB".Int_Kod_Miyun'
    end
    object Tbl_lakoachInt_Kod_Tnai_Tashlum: TIntegerField
      FieldName = 'Int_Kod_Tnai_Tashlum'
      Origin = 'DB_ATMCRT."Lakoach.DB".Int_Kod_Tnai_Tashlum'
    end
    object Tbl_lakoachLookGroup: TStringField
      FieldKind = fkLookup
      FieldName = 'LookGroup'
      LookupDataSet = Qry_grouplak
      LookupKeyFields = 'Kod_Tavla'
      LookupResultField = 'Teur_tavla'
      KeyFields = 'Kod_Kvutza'
      LookupCache = True
      Lookup = True
    end
  end
  object Ds_lakoach: TDataSource
    DataSet = Tbl_lakoach
    OnStateChange = Ds_lakoachStateChange
    OnDataChange = Ds_lakoachDataChange
    Left = 249
    Top = 135
  end
  object AtmAdvSearch_lak: TAtmAdvSearch
    Caption = '����� ����'
    KeyIndex = 1
    SourceDataSet = DM_AtmCrt.Qry_Temp
    ShowFields.Strings = (
      'Kod_Lakoach'
      'Shem_Lakoach'
      'Kod_Kvutza')
    ShowFieldsHeader.Strings = (
      '��� ����'
      '�� ����'
      '�����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '200'
      '100')
    Sql.Strings = (
      'Select Kod_Lakoach,Shem_Lakoach,Kod_Kvutza'
      'From LAKOACH')
    ReturnFieldIndex = 0
    SeparateChar = ','
    KeepList = False
    Left = 364
    Top = 305
  end
  object Scale1: TScale
    Active = False
    X_ScreenWidth = 640
    Y_ScreenHeight = 480
    Left = 584
    Top = 317
  end
  object UpdateSQL1: TUpdateSQL
    ModifySQL.Strings = (
      'update Lakoach'
      'set'
      '  Kod_Lakoach = :Kod_Lakoach,'
      '  Kod_Kvutza = :Kod_Kvutza,'
      '  Kod_Miyun = :Kod_Miyun,'
      '  Mis_Han = :Mis_Han,'
      '  Shem_Lakoach = :Shem_Lakoach,'
      '  Ktovet_1 = :Ktovet_1,'
      '  Mis_Bait1 = :Mis_Bait1,'
      '  Ir_1 = :Ir_1,'
      '  Mikud_1 = :Mikud_1,'
      '  Ktovet_2 = :Ktovet_2,'
      '  Mis_Bait2 = :Mis_Bait2,'
      '  Ir_2 = :Ir_2,'
      '  Mikud_2 = :Mikud_2,'
      '  Tel_1 = :Tel_1,'
      '  Tel_2 = :Tel_2,'
      '  Fax = :Fax,'
      '  Ple_Phon = :Ple_Phon,'
      '  Ish_Kesher = :Ish_Kesher,'
      '  Osek_Morshe_ID = :Osek_Morshe_ID,'
      '  Taarich_Osek_Morshe = :Taarich_Osek_Morshe,'
      '  Kod_Tnai_Tashlum = :Kod_Tnai_Tashlum,'
      '  Kod_Eara = :Kod_Eara,'
      '  Mas_Makor = :Mas_Makor,'
      '  Taarich_Mas = :Taarich_Mas,'
      '  Achuz_Anacha_Amala = :Achuz_Anacha_Amala,'
      '  Sug_Anacha_Amala = :Sug_Anacha_Amala,'
      '  Achuz_Anacha_Amala_2 = :Achuz_Anacha_Amala_2,'
      '  Kod_Kesher = :Kod_Kesher,'
      '  Taarich_Atchala = :Taarich_Atchala,'
      '  Taarich_Siyum = :Taarich_Siyum,'
      '  Earot = :Earot,'
      '  Kod_Banehag = :Kod_Banehag,'
      '  Kod_Basapak = :Kod_Basapak,'
      '  Kod_Atzmada = :Kod_Atzmada,'
      '  Kod_mechiron = :Kod_mechiron,'
      '  Sug_Rikuz = :Sug_Rikuz,'
      '  Sug_Koteret = :Sug_Koteret,'
      '  Sug_Miyun = :Sug_Miyun,'
      '  Mis_Taktziv = :Mis_Taktziv,'
      '  Choze = :Choze,'
      '  Sug_Bitachon = :Sug_Bitachon,'
      '  Schum_Bitachon = :Schum_Bitachon,'
      '  Taarich_Siyun_Bitachon = :Taarich_Siyun_Bitachon,'
      '  Tosefet_Achuz = :Tosefet_Achuz,'
      '  Tosefet_Melel = :Tosefet_Melel,'
      '  Sug_Madad_1 = :Sug_Madad_1,'
      '  Taarich_Madad_1 = :Taarich_Madad_1,'
      '  Madad_Basis_1 = :Madad_Basis_1,'
      '  Mutzmad_Achuz_1 = :Mutzmad_Achuz_1,'
      '  Sug_Madad_2 = :Sug_Madad_2,'
      '  Taarich_Madad_2 = :Taarich_Madad_2,'
      '  Madad_Basis_2 = :Madad_Basis_2,'
      '  Mutzmad_Achuz_2 = :Mutzmad_Achuz_2,'
      '  Sug_Madad_3 = :Sug_Madad_3,'
      '  Taarich_madad_3 = :Taarich_madad_3,'
      '  Madad_Basis_3 = :Madad_Basis_3,'
      '  Mutzmad_Achuz_3 = :Mutzmad_Achuz_3,'
      '  Yatza_Teudat_Mishloach = :Yatza_Teudat_Mishloach,'
      '  Yatza_Cheshbon_Lo_Shula = :Yatza_Cheshbon_Lo_Shula,'
      '  Chekim_Dachui_Lo_Nifra = :Chekim_Dachui_Lo_Nifra,'
      '  X = :X,'
      '  Y = :Y,'
      '  NodeNumB = :NodeNumB,'
      '  NodeSugSystem = :NodeSugSystem,'
      '  NodeKodCityName = :NodeKodCityName,'
      '  String_1 = :String_1,'
      '  String_2 = :String_2,'
      '  String_3 = :String_3,'
      '  String_4 = :String_4,'
      '  String_5 = :String_5,'
      '  String_6 = :String_6,'
      '  String_7 = :String_7,'
      '  String_8 = :String_8,'
      '  Integer_1 = :Integer_1,'
      '  Integer_2 = :Integer_2,'
      '  Integer_3 = :Integer_3,'
      '  Integer_4 = :Integer_4,'
      '  Integer_5 = :Integer_5,'
      '  Date_1 = :Date_1,'
      '  Date_2 = :Date_2,'
      '  Date_3 = :Date_3,'
      '  Date_4 = :Date_4,'
      '  Date_5 = :Date_5,'
      '  Money_1 = :Money_1,'
      '  Money_2 = :Money_2,'
      '  Money_3 = :Money_3,'
      '  Money_4 = :Money_4,'
      '  Money_5 = :Money_5,'
      '  Money_6 = :Money_6,'
      '  Money_7 = :Money_7,'
      '  Int_Kod_Kvutza = :Int_Kod_Kvutza,'
      '  Int_Kod_Miyun = :Int_Kod_Miyun,'
      '  Int_Kod_Tnai_Tashlum = :Int_Kod_Tnai_Tashlum'
      'where'
      '  Kod_Lakoach = :OLD_Kod_Lakoach')
    InsertSQL.Strings = (
      'insert into Lakoach'
      
        '  (Kod_Lakoach, Kod_Kvutza, Kod_Miyun, Mis_Han, Shem_Lakoach, Kt' +
        'ovet_1, '
      
        '   Mis_Bait1, Ir_1, Mikud_1, Ktovet_2, Mis_Bait2, Ir_2, Mikud_2,' +
        ' Tel_1, '
      
        '   Tel_2, Fax, Ple_Phon, Ish_Kesher, Osek_Morshe_ID, Taarich_Ose' +
        'k_Morshe, '
      
        '   Kod_Tnai_Tashlum, Kod_Eara, Mas_Makor, Taarich_Mas, Achuz_Ana' +
        'cha_Amala, '
      
        '   Sug_Anacha_Amala, Achuz_Anacha_Amala_2, Kod_Kesher, Taarich_A' +
        'tchala, '
      
        '   Taarich_Siyum, Earot, Kod_Banehag, Kod_Basapak, Kod_Atzmada, ' +
        'Kod_mechiron, '
      
        '   Sug_Rikuz, Sug_Koteret, Sug_Miyun, Mis_Taktziv, Choze, Sug_Bi' +
        'tachon, '
      
        '   Schum_Bitachon, Taarich_Siyun_Bitachon, Tosefet_Achuz, Tosefe' +
        't_Melel, '
      
        '   Sug_Madad_1, Taarich_Madad_1, Madad_Basis_1, Mutzmad_Achuz_1,' +
        ' '
      'Sug_Madad_2, '
      
        '   Taarich_Madad_2, Madad_Basis_2, Mutzmad_Achuz_2, Sug_Madad_3,' +
        ' '
      'Taarich_madad_3, '
      '   Madad_Basis_3, Mutzmad_Achuz_3, Yatza_Teudat_Mishloach, '
      'Yatza_Cheshbon_Lo_Shula, '
      
        '   Chekim_Dachui_Lo_Nifra, X, Y, NodeNumB, NodeSugSystem, NodeKo' +
        'dCityName, '
      
        '   String_1, String_2, String_3, String_4, String_5, String_6, S' +
        'tring_7, '
      
        '   String_8, Integer_1, Integer_2, Integer_3, Integer_4, Integer' +
        '_5, Date_1, '
      
        '   Date_2, Date_3, Date_4, Date_5, Money_1, Money_2, Money_3, Mo' +
        'ney_4, '
      '   Money_5, Money_6, Money_7, Int_Kod_Kvutza, Int_Kod_Miyun, '
      'Int_Kod_Tnai_Tashlum)'
      'values'
      
        '  (:Kod_Lakoach, :Kod_Kvutza, :Kod_Miyun, :Mis_Han, :Shem_Lakoac' +
        'h, :Ktovet_1, '
      
        '   :Mis_Bait1, :Ir_1, :Mikud_1, :Ktovet_2, :Mis_Bait2, :Ir_2, :M' +
        'ikud_2, '
      
        '   :Tel_1, :Tel_2, :Fax, :Ple_Phon, :Ish_Kesher, :Osek_Morshe_ID' +
        ', '
      ':Taarich_Osek_Morshe, '
      '   :Kod_Tnai_Tashlum, :Kod_Eara, :Mas_Makor, :Taarich_Mas, '
      ':Achuz_Anacha_Amala, '
      
        '   :Sug_Anacha_Amala, :Achuz_Anacha_Amala_2, :Kod_Kesher, :Taari' +
        'ch_Atchala, '
      
        '   :Taarich_Siyum, :Earot, :Kod_Banehag, :Kod_Basapak, :Kod_Atzm' +
        'ada, '
      ':Kod_mechiron, '
      
        '   :Sug_Rikuz, :Sug_Koteret, :Sug_Miyun, :Mis_Taktziv, :Choze, :' +
        'Sug_Bitachon, '
      
        '   :Schum_Bitachon, :Taarich_Siyun_Bitachon, :Tosefet_Achuz, :To' +
        'sefet_Melel, '
      
        '   :Sug_Madad_1, :Taarich_Madad_1, :Madad_Basis_1, :Mutzmad_Achu' +
        'z_1, '
      ':Sug_Madad_2, '
      
        '   :Taarich_Madad_2, :Madad_Basis_2, :Mutzmad_Achuz_2, :Sug_Mada' +
        'd_3, '
      ':Taarich_madad_3, '
      '   :Madad_Basis_3, :Mutzmad_Achuz_3, :Yatza_Teudat_Mishloach, '
      ':Yatza_Cheshbon_Lo_Shula, '
      '   :Chekim_Dachui_Lo_Nifra, :X, :Y, :NodeNumB, :NodeSugSystem, '
      ':NodeKodCityName, '
      
        '   :String_1, :String_2, :String_3, :String_4, :String_5, :Strin' +
        'g_6, :String_7, '
      
        '   :String_8, :Integer_1, :Integer_2, :Integer_3, :Integer_4, :I' +
        'nteger_5, '
      
        '   :Date_1, :Date_2, :Date_3, :Date_4, :Date_5, :Money_1, :Money' +
        '_2, :Money_3, '
      
        '   :Money_4, :Money_5, :Money_6, :Money_7, :Int_Kod_Kvutza, :Int' +
        '_Kod_Miyun, '
      '   :Int_Kod_Tnai_Tashlum)')
    DeleteSQL.Strings = (
      'delete from Lakoach'
      'where'
      '  Kod_Lakoach = :OLD_Kod_Lakoach')
    Left = 288
    Top = 96
  end
  object Qry_grouplak: TQuery
    DatabaseName = 'DB_AtmCrt'
    SQL.Strings = (
      'Select * From kodTavla'
      'Where Sug_Tavla=17')
    Left = 264
    Top = 56
  end
  object AtmTabSheetBuild1: TAtmTabSheetBuild
    ScrFileName = 'TSBLaknew1.Scr'
    SqlFileName = 'TSBLaknew1.Sql'
    PageControl = PageControl1
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    DataSource = Ds_Tabsheetbuild
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = RxQuery_Tabsheetbuild
    RxDBFilter = RxDBFilter_TabsheetBuild
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    BeforeExecuteQuery = AtmTabSheetBuild1BeforeExecuteQuery
    Left = 444
    Top = 57
  end
  object RxQuery_Tabsheetbuild: TAtmRxQuery
    DatabaseName = 'DB_AtmCrt'
    SQL.Strings = (
      'Select * From '
      'Where %MWhereIndex'
      'Order By %OrderFields')
    Macros = <
      item
        DataType = ftString
        Name = 'MWhereIndex'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'OrderFields'
        ParamType = ptInput
        Value = '0=0'
      end>
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = False
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = False
    GetOnlyOneRecord = False
    Left = 476
    Top = 57
  end
  object RxDBFilter_TabsheetBuild: TRxDBFilter
    Left = 508
    Top = 57
  end
  object Ds_Tabsheetbuild: TDataSource
    DataSet = RxQuery_Tabsheetbuild
    Left = 544
    Top = 56
  end
end

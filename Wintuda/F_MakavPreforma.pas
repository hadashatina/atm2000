unit F_MakavPreforma;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, ToolWin, ComCtrls, DBTables, Mask, ExtCtrls,
  AtmAdvTable, AtmComp, Scale250, AtmLookCombo, AdvSearch, Grids, DBGrids,
  RXDBCtrl, ToolEdit, DBFilter, RxQuery, AtmTabSheetBuild, AtmDBDateEdit,
  Dialogs;

type
  TFrm_MakavPreforma = class(TForm)
    Tbl_MakavHesbonitNumber: TIntegerField;
    Tbl_MakavCodeLakoach: TIntegerField;
    Tbl_MakavLakoachGroup: TIntegerField;
    Tbl_MakavHesbonitDate: TDateTimeField;
    Tbl_MakavHesbonitKind: TIntegerField;
    Tbl_MakavMerakez: TStringField;
    Tbl_MakavTotalHesbonitWithMAM: TBCDField;
    Tbl_MakavMAMHesbonit: TBCDField;
    Tbl_MakavMAMPercent: TBCDField;
    Tbl_MakavMAMRound: TBCDField;
    Tbl_MakavDiscountWork: TBCDField;
    Tbl_MakavDiscountParts: TBCDField;
    Tbl_MakavAchuz_Anacha_Lakoach: TBCDField;
    Tbl_MakavAchuz_Anacha_Klali: TBCDField;
    Tbl_MakavPratimToHan: TStringField;
    Tbl_MakavCodeHaavaraToHan: TIntegerField;
    Tbl_MakavMaklidName: TStringField;
    Tbl_MakavDateHafakatHeshbonit: TDateTimeField;
    Tbl_MakavCodeMeasher: TIntegerField;
    Tbl_MakavSumPaied: TBCDField;
    Tbl_MakavHanForMam: TStringField;
    Tbl_MakavYehusMonth: TIntegerField;
    Tbl_MakavYehusYear: TIntegerField;
    Tbl_MakavPaymentCondition: TIntegerField;
    Tbl_MakavStatus: TIntegerField;
    Tbl_MakavNumberOfPayments: TIntegerField;
    Tbl_MakavDateForFirstPayment: TDateTimeField;
    Tbl_MakavOrderNum: TIntegerField;
    Tbl_MakavTotalSumFromTnua: TBCDField;
    Tbl_MakavTotalSumPriceKamut: TBCDField;
    Tbl_MakavDatePiraon: TDateTimeField;
    Tbl_MakavKvutzaLeChiyuv: TIntegerField;
    Tbl_MakavMisHaavaraToHan: TIntegerField;
    Tbl_MakavDate_Havara: TDateTimeField;
    Tbl_MakavTotal_Zikuy: TBCDField;
    Tbl_MakavDate_Zikuy: TDateTimeField;
    Tbl_MakavMis_Kabala1: TIntegerField;
    Tbl_MakavMis_Kabala2: TIntegerField;
    Tbl_MakavMis_Kabala3: TIntegerField;
    Tbl_MakavMis_Kabala4: TIntegerField;
    Tbl_MakavMis_Kabala5: TIntegerField;
    Tbl_MakavDate_Kabala1: TDateTimeField;
    Tbl_MakavDate_Kabala2: TDateTimeField;
    Tbl_MakavDate_Kabala3: TDateTimeField;
    Tbl_MakavDate_Kabala4: TDateTimeField;
    Tbl_MakavDate_Kabala5: TDateTimeField;
    Tbl_MakavLast_Update: TDateTimeField;
    Tbl_MakavCheckNum: TMemoField;
    DS_Makav: TDataSource;
    Tbl_Makav: TAtmTable;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    TS_Zikuy: TTabSheet;
    Tbl_MakavHanForLak: TStringField;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label49: TLabel;
    Label6: TLabel;
    EditDate_Zikuy: TAtmDbHEdit;
    EditMis_Kabala: TAtmDbHEdit;
    EditMis_Kabala2: TAtmDbHEdit;
    EditMis_Kabala3: TAtmDbHEdit;
    EditMis_Kabala4: TAtmDbHEdit;
    EditMis_Kabala5: TAtmDbHEdit;
    EditDate_Kabala: TAtmDbHEdit;
    EditDate_Kabala2: TAtmDbHEdit;
    EditDate_Kabala3: TAtmDbHEdit;
    EditDate_Kabala4: TAtmDbHEdit;
    EditDate_Kabala5: TAtmDbHEdit;
    AtmDbHMemo1: TAtmDbHMemo;
    AtmDbHEdit1: TAtmDbHEdit;
    Panel2: TPanel;
    Scale1: TScale;
    Tbl_Lako: TAtmTable;
    Tbl_LakoKod_Lakoach: TIntegerField;
    Tbl_LakoKod_Kvutza: TIntegerField;
    Tbl_LakoShem_Lakoach: TStringField;
    Tbl_MakavLookLakoachName: TStringField;
    GroupBox2: TGroupBox;
    EditMisHaavaraToHan: TAtmDbHEdit;
    EditHanForMam: TAtmDbHEdit;
    EditCodeHaavaraToHan: TAtmDbHEdit;
    Label16: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    EditHanForLak: TAtmDbHEdit;
    EditDate_Havara: TAtmDbHEdit;
    Label15: TLabel;
    EditPratimToHan: TAtmDbHEdit;
    AtmAdvSearch_Lako: TAtmAdvSearch;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    TS_GridMakav: TTabSheet;
    RxDBGrid_Makav: TRxDBGrid;
    Query_MakavLako: TQuery;
    DS_MakavLako: TDataSource;
    Panel3: TPanel;
    Label11: TLabel;
    Label_NameLak: TLabel;
    ComboEdit_CodeLak: TComboEdit;
    Tbl_MakavIncom_Type: TIntegerField;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    QRY_Grigs: TRxQuery;
    RxDBFilter1: TRxDBFilter;
    DS_Grids: TDataSource;
    ToolButton3: TToolButton;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    EditHesbonitNumber: TAtmDbHEdit;
    Label4: TLabel;
    Label18: TLabel;
    Lbl_CodeLakoach: TLabel;
    EditCodeLakoach: TAtmDbHEdit;
    DBText1: TDBText;
    Label7: TLabel;
    EditTotalHesbonitWithMAM: TAtmDbHEdit;
    Label3: TLabel;
    EditLakoachGroup: TAtmDbHEdit;
    Label28: TLabel;
    EditDateForFirstPayment: TAtmDbHEdit;
    Label32: TLabel;
    EditDatePiraon: TAtmDbHEdit;
    Label25: TLabel;
    AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox;
    GroupBox4: TGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label27: TLabel;
    EditTotalSumFromTnua: TAtmDbHEdit;
    EditTotalSumPriceKamut: TAtmDbHEdit;
    EditMAMHesbonit: TAtmDbHEdit;
    EditMAMPercent: TAtmDbHEdit;
    EditMAMRound: TAtmDbHEdit;
    EditAchuz_Anacha_Lakoach: TAtmDbHEdit;
    EditAchuz_Anacha_Klali: TAtmDbHEdit;
    EditNumberOfPayments: TAtmDbHEdit;
    Tbl_MakavYitra: TCurrencyField;
    AtmDbHEdit2: TAtmDbHEdit;
    Label2: TLabel;
    Label5: TLabel;
    EditHesbonitKind: TAtmDbHEdit;
    Label48: TLabel;
    Label17: TLabel;
    EditLast_Update: TAtmDbHEdit;
    EditMaklidName: TAtmDbHEdit;
    Label23: TLabel;
    Label24: TLabel;
    EditYehusMonth: TAtmDbHEdit;
    EditYehusYear: TAtmDbHEdit;
    Label29: TLabel;
    EditOrderNum: TAtmDbHEdit;
    Label12: TLabel;
    DBEdit_IncomType: TAtmDbHEdit;
    DBRadioGroup1: TDBRadioGroup;
    Label36: TLabel;
    EditTotal_Zikuy: TAtmDbHEdit;
    AtmDBDateEdit_HeshbonitDate: TAtmDBDateEdit;
    AtmDBDateEdit_DateHafaka: TAtmDBDateEdit;
    Tbl_LakoKod_Tnai_Tashlum: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MakavAfterInsert(DataSet: TDataSet);
    procedure Tbl_MakavAfterPost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Change(Sender: TObject);
    procedure RxDBGrid_MakavTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure RxDBGrid_MakavGetBtnParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; var SortMarker: TSortMarker;
      IsDown: Boolean);
    procedure RxDBGrid_MakavGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure EditCodeLakoachBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure ComboEdit_CodeLakButtonClick(Sender: TObject);
    procedure RxDBGrid_MakavDblClick(Sender: TObject);
    procedure Lbl_CodeLakoachDblClick(Sender: TObject);
    procedure Tbl_MakavCalcFields(DataSet: TDataSet);
    procedure Tbl_MakavCodeLakoachValidate(Sender: TField);
  private
    { private declarations }
    CurSortFieldName :ShortString;
  public
    { public declarations }
    Procedure BuildLookupList;
    procedure RunQryMakav(OrderByFieldName :String);
  end;

var
  Frm_MakavPreforma: TFrm_MakavPreforma;

implementation
Uses DMAtmCrt,AtmConst,AtmRutin,Crt_Glbl;
{$R *.DFM}

procedure TFrm_MakavPreforma.FormCreate(Sender: TObject);
Var
   I:LongInt;
Begin
  Try
     Screen.Cursor:=crHourGlass;
     BuildLookupList;
     PageControl1.ActivePage:=TS_Main;
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');

{     AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
     AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;}
     AtmTabSheetBuild1.ScriptsDir:=CrtDirForScripts;
     AtmTabSheetBuild1.FilterDir:=CrtDirForCurUser;
     AtmTabSheetBuild1.BuildTabsForGrids;
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_MakavPreforma.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_MakavPreforma.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_MakavPreforma:=Nil;
     Action:=caFree;
end;

procedure TFrm_MakavPreforma.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Not Visible Then Exit;
//     ChangeDSForNavigator(TDBNavigator(Sender));
     if Button=nbPost Then
         DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
end;

procedure TFrm_MakavPreforma.Tbl_MakavAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_MakavPreforma.Tbl_MakavAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_MakavPreforma.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(ActiveControl,Key,Shift,Tbl_Makav);
end;

Procedure TFrm_MakavPreforma.BuildLookupList;
Var
   SList :TStringList;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_TnaeyTashlum),AtmDbComboBox_TnaeyTashlum);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;

procedure TFrm_MakavPreforma.PageControl1Change(Sender: TObject);
begin
     if Query_MakavLako.Active Then
        Query_MakavLako.Active:=False;
     if PageControl1.ActivePage=TS_GridMakav Then
     Begin
          ComboEdit_CodeLak.Text:=Tbl_Makav.FieldByName('CodeLakoach').AsString;
          Label_NameLak.Caption:=Tbl_MakavLookLakoachName.AsString;
          RunQryMakav('HesbonitNumber');
          DBNavigator.DataSource:=DS_MakavLako;
     End
     Else
         DBNavigator.DataSource:=DS_Makav;

end;

procedure TFrm_MakavPreforma.RunQryMakav(OrderByFieldName :String);
Begin
  Try
     Screen.Cursor:=crHourGlass;
      CurSortFieldName:=OrderByFieldName;
      With Query_MakavLako Do
      Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From '+Tbl_Makav.TableName);
           Sql.Add('Where CodeLakoach='+ComboEdit_CodeLak.Text);
           Sql.Add('Order By '+OrderByFieldName);
           Active:=True;
      End;
  Finally
         Screen.Cursor:=crDefault;
  End;
End;
procedure TFrm_MakavPreforma.RxDBGrid_MakavTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
     RunQryMakav(Field.FieldName);
end;

procedure TFrm_MakavPreforma.RxDBGrid_MakavGetBtnParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor;
  var SortMarker: TSortMarker; IsDown: Boolean);
begin
     if CompareText(CurSortFieldName,Field.FieldName)=0 Then
          SortMarker := smDown;
end;

procedure TFrm_MakavPreforma.RxDBGrid_MakavGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     if CompareText(Field.FullName,CurSortFieldName)=0 Then
        Background:=clInfoBk;
end;

procedure TFrm_MakavPreforma.EditCodeLakoachBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     EditCodeLakoach.SearchComponent.ReturnFieldIndex:=0;
end;

procedure TFrm_MakavPreforma.ComboEdit_CodeLakButtonClick(Sender: TObject);
begin
     AtmAdvSearch_Lako.ReturnFieldIndex:=-1;
     if AtmAdvSearch_Lako.Execute Then
     Begin
          ComboEdit_CodeLak.Text:=GetFieldFromSeparateString(AtmAdvSearch_Lako.ReturnString,AtmAdvSearch_Lako.SeparateChar,1);
          Label_NameLak.Caption:=GetFieldFromSeparateString(AtmAdvSearch_Lako.ReturnString,AtmAdvSearch_Lako.SeparateChar,3);
          RunQryMakav(CurSortFieldName);
     End;
end;

procedure TFrm_MakavPreforma.RxDBGrid_MakavDblClick(Sender: TObject);
begin
     Tbl_Makav.FindKey([Query_MakavLako.FieldByName('HesbonitNumber').Value]);
     PageControl1.ActivePage:=TS_Main;
end;

procedure TFrm_MakavPreforma.Lbl_CodeLakoachDblClick(Sender: TObject);
begin
     OpenAtmCrt(fnLakoach,EditCodeLakoach.Text);
end;

procedure TFrm_MakavPreforma.Tbl_MakavCalcFields(DataSet: TDataSet);
begin
     Tbl_MakavYitra.AsFloat:=Tbl_MakavTotalHesbonitWithMAM.AsFloat-Tbl_MakavTotal_Zikuy.AsFloat;
end;

procedure TFrm_MakavPreforma.Tbl_MakavCodeLakoachValidate(Sender: TField);
begin
     if Tbl_Lako.FindKey([Tbl_MakavCodeLakoach.Value]) Then
     Begin
        if Tbl_Makav.State=dsInsert Then
          Tbl_MakavPaymentCondition.AsInteger:=Tbl_LakoKod_Tnai_Tashlum.AsInteger;
     End
     ELse
     Begin
          MessageDlg('���� �� ����',mtError,[mbOk],0);
          Abort;
     End;
end;

end.


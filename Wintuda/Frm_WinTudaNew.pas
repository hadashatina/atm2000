unit Frm_WinTudaNew;

interface
uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, Mask, ExtCtrls, AtmComp, ComCtrls,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, Grids, DBGrids, RXSplit, Placemnt,
  DBTables, AtmAdvTable, Buttons, ToolWin, ActnList, ImgList, Menus,
  AdvSearch, KeyBoard, AppEvent, Scale250, SpeedBar, AtmLookCombo,IniFiles,
  AtmTabSheetBuild, DynamicControls, RxQuery, DBFilter,AtmRxQuery,TypInfo,
  dbnav797, ExeMenu, DBLogFiler, DBActns, F_TzfifutHodshit;


type
  TFormWinTudaNew = class(TForm)
    PageControl_Main: TPageControl;
    TS_Tnua: TTabSheet;
    Splitter1: TSplitter;
    Panel_Top: TPanel;
    ToolBar_Tnua: TToolBar;
    DBNavigator: TAtmDbNavigator;
    TBtn_NewHeshbon: TToolButton;
    TBtn_NewTnua: TToolButton;
    Btn_PostNew: TBitBtn;
    ToolButton1: TToolButton;
    SpeedBar1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedbarSection1: TSpeedbarSection;
    SpeedbarSection3: TSpeedbarSection;
    SpeedbarSection4: TSpeedbarSection;
    SpeedbarSection5: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem12: TSpeedItem;
    SpeedItem13: TSpeedItem;
    SpeedItem14: TSpeedItem;
    SpeedItem15: TSpeedItem;
    SpeedItem16: TSpeedItem;
    SpeedItem20: TSpeedItem;
    SpeedItem18: TSpeedItem;
    SpeedItem17: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem19: TSpeedItem;
    SpeedItem21: TSpeedItem;
    SpeedItem22: TSpeedItem;
    SpeedItem23: TSpeedItem;
    PageControl_Tnua: TPageControl;
    TS_Tnua1: TTabSheet;
    Panel_TsTnuaMain: TPanel;
    Panel_GridTnua: TPanel;
    DBGrid_Tnua: TRxDBGrid;
    Panel_ShibKind: TPanel;
    TS_Perut2: TTabSheet;
    DBGrid_Preut2: TRxDBGrid;
    TS_StatusFields: TTabSheet;
    Pnl_TabSheetExtraFields: TPanel;
    Label17: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    DBRG_ShibKind: TDBRadioGroup;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label31: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    Label27: TLabel;
    AtmDbHEdit_KBLak: TAtmDbHEdit;
    AtmDbHEdit_KBDriver: TAtmDbHEdit;
    AtmDbHEdit_Heshbonit: TAtmDbHEdit;
    AtmDbHEdit7: TAtmDbHEdit;
    AtmDbHEdit8: TAtmDbHEdit;
    AtmDbHEdit5: TAtmDbHEdit;
    AtmDbHEdit2: TAtmDbHEdit;
    AtmDbHEdit3: TAtmDbHEdit;
    DBRadioGroup1: TDBRadioGroup;
    AppEvents1: TAppEvents;
    Scale1: TScale;
    DynamicControls1: TDynamicControls;
    MainMenu1: TMainMenu;
    NFile: TMenuItem;
    N24: TMenuItem;
    N9: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N47: TMenuItem;
    N57: TMenuItem;
    N52: TMenuItem;
    N55: TMenuItem;
    N27: TMenuItem;
    N29: TMenuItem;
    N7: TMenuItem;
    N56: TMenuItem;
    N3: TMenuItem;
    NClose: TMenuItem;
    NEdit: TMenuItem;
    NOpenSearchWindow: TMenuItem;
    NNewHazmana: TMenuItem;
    N2: TMenuItem;
    N10: TMenuItem;
    MenuItemCancelMovement: TMenuItem;
    N58: TMenuItem;
    NStartKeyboard: TMenuItem;
    N20: TMenuItem;
    NEditRxSpeedBar: TMenuItem;
    N19: TMenuItem;
    N43: TMenuItem;
    N13: TMenuItem;
    N40: TMenuItem;
    N39: TMenuItem;
    N59: TMenuItem;
    N60: TMenuItem;
    N61: TMenuItem;
    NCrt: TMenuItem;
    N4: TMenuItem;
    ActionCrtOpenNehag1: TMenuItem;
    N5: TMenuItem;
    ActionCrtOpenMehiron1: TMenuItem;
    N12: TMenuItem;
    N28: TMenuItem;
    N11: TMenuItem;
    N66: TMenuItem;
    N63: TMenuItem;
    N64: TMenuItem;
    N65: TMenuItem;
    N1: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N21: TMenuItem;
    N32: TMenuItem;
    N42: TMenuItem;
    N122: TMenuItem;
    N46: TMenuItem;
    N16: TMenuItem;
    N62: TMenuItem;
    N23: TMenuItem;
    NReports: TMenuItem;
    N22: TMenuItem;
    N30: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    NRefreshAllSearchWindows: TMenuItem;
    N38: TMenuItem;
    N44: TMenuItem;
    N8: TMenuItem;
    N48: TMenuItem;
    N49: TMenuItem;
    N50: TMenuItem;
    N51: TMenuItem;
    N67: TMenuItem;
    N53: TMenuItem;
    N54: TMenuItem;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    TSB2: TAtmTabSheetBuild;
    AtmKeyBoard1: TAtmKeyBoard;
    ImageList_Buttons: TImageList;
    ActionList1: TActionList;
    Action_LastPriceListAction: TAction;
    Action_OpenSearch: TAction;
    Action_NewHazmana: TAction;
    Action_CalcTotalTeudaAzmn: TAction;
    Action_Price4: TAction;
    Action_Price3: TAction;
    Action_Price1: TAction;
    Action_Price2: TAction;
    Action_Price1PlusPrice2: TAction;
    Action_SubtractMam: TAction;
    Action_CrtOpenLak: TAction;
    Action_CrtOpenNehag: TAction;
    Action_CrtOpenRehev: TAction;
    Action_ChooseMehiron: TAction;
    Action_CrtOpenMehiron: TAction;
    Action_DupRecord: TAction;
    Action_CrtOpenKodTavla: TAction;
    Action_CrtOpenMaslul: TAction;
    Action_SelectExtraFile: TAction;
    Action_AllPrices: TAction;
    Action_SearchCodesFromMehiron: TAction;
    Action_ChangeParams: TAction;
    Action_NewTnua: TAction;
    Action_ChoosePointsFromMsl: TAction;
    Action_MeholelReport: TAction;
    Action_MeholelMain: TAction;
    Action_ChooseYehus: TAction;
    Action_ShibKindHiuvNehag: TAction;
    Action_ShibKindKikuyLak: TAction;
    Action_ShibKindNormal: TAction;
    Action_PriceKindNoMam: TAction;
    Action_CrtOpenMahzeva: TAction;
    Action_CopyTnuot: TAction;
    Act_Refresh_Lako: TAction;
    Act_Refresh_Nehag: TAction;
    Act_Refresh_Rehev: TAction;
    Act_Refresh_Maslul: TAction;
    Act_Refresh_KodTavla: TAction;
    Act_Maintaince_CloseMeholel: TAction;
    Act_EditScreenControls: TAction;
    Act_EditExtraFields: TAction;
    Action_ChangeTab: TAction;
    Act_Maintaince_UPdatePricesInTnuaFromMehiron: TAction;
    Action_Price2SubHanForLakFromPrice1: TAction;
    Act_HsbZikuyLakPerut: TAction;
    Act_HsbZikuyLakMake: TAction;
    Act_HsbZikuyLakCopy: TAction;
    Act_HsbZikuyLakCancel: TAction;
    Act_HsbPriceProposal: TAction;
    Act_HsbPrintHazmana: TAction;
    Action_AtmAzmnHatz: TAction;
    Action_CopyHatzaotToHazmanot: TAction;
    Action_ShibKindZikuyHiyuvLak: TAction;
    ActionCancelMovement: TAction;
    ActionBringMovementBack: TAction;
    NextAzmn: TDataSetNext;
    DataSetPrior1: TDataSetPrior;
    Action_ChangePricePercent: TAction;
    Action_OpenTzfifutHodshit: TAction;
    Action_Tzfifut: TAction;
    Action_SearchCodesFromMehironConect: TAction;
    Act_Hesb_Mas_Kabla: TAction;
    FormStorage_WinTeuda: TFormStorage;
    PopupMenu_Mehiron: TPopupMenu;
    N17: TMenuItem;
    ActionPrice11: TMenuItem;
    ActionPrice21: TMenuItem;
    N31: TMenuItem;
    N41: TMenuItem;
    N121: TMenuItem;
    N45: TMenuItem;
    N18: TMenuItem;
    N6: TMenuItem;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Lbl_Hazmana: TLabel;
    Lbl_AzmnSugHazmana: TLabel;
    Lbl_FromAzmnDate: TLabel;
    Lbl_AzmnFromTime: TLabel;
    Lbl_ToAzmnDate: TLabel;
    Lbl_AzmnToTime: TLabel;
    Lbl_ShemLakoach: TLabel;
    Lbl_AzmnAsmachta: TLabel;
    DBText_LakName: TDBText;
    Lbl_InvName: TLabel;
    AtmDbHEdit_Hazmana: TAtmDbHEdit;
    AtmDBDateEdit_AzmnFromDate: TAtmDBDateEdit;
    AtmDBDateEdit_AzmnToDate: TAtmDBDateEdit;
    AtmDbHEdit_Lak: TAtmDbHEdit;
    AtmDbHEdit_AzmnAsmachta: TAtmDbHEdit;
    MaskEdit_FromTime: TMaskEdit;
    MaskEdit_ToTime: TMaskEdit;
    AtmDbComboBox_azmnType: TAtmDbComboBox;
    AtmDbHEdit_InvLakName: TAtmDbHEdit;
    Panel_Days: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBCheckBox_Day1: TDBCheckBox;
    DBCheckBox_Day2: TDBCheckBox;
    DBCheckBox_Day3: TDBCheckBox;
    DBCheckBox_Day4: TDBCheckBox;
    DBCheckBox_Day5: TDBCheckBox;
    DBCheckBox_Day6: TDBCheckBox;
    DBCheckBox_Day7: TDBCheckBox;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    Pnl_Mid: TPanel;
    Splitter2: TSplitter;
    Label4: TLabel;
    Label7: TLabel;
    Label24: TLabel;
    Label_Tz: TLabel;
    Lbl_BeforeSupply: TLabel;
    Lbl_AfterSupply: TLabel;
    Lbl_DeliveryHour: TLabel;
    Panel_Extra: TPanel;
    AtmDbHEdit1: TAtmDbHEdit;
    DBRadioGroup_SugTemperature: TDBRadioGroup;
    DBRadioGroup_SugKamut: TDBRadioGroup;
    AtmDbHEditBeforeSupply: TAtmDbHEdit;
    AtmDbHEditAfterSupply: TAtmDbHEdit;
    AtmDbHEditDeliveryHour: TAtmDbHEdit;
    Panel3: TPanel;
    Lbl_TnuaDate: TLabel;
    Lbl_Rehev: TLabel;
    Label9: TLabel;
    Lbl_SugHovala: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    DBText6: TDBText;
    Lbl_Teuda: TLabel;
    Label5: TLabel;
    Lbl_Nehag1: TStaticText;
    AtmDBDateEdit_TnuaDate: TAtmDBDateEdit;
    AtmDbHEdit_Rehev: TAtmDbHEdit;
    AtmDbHEdit_Nehag1: TAtmDbHEdit;
    AtmDbHEdit_ShemNehag: TAtmDbHEdit;
    AtmDbHEdit_HovalaKind: TAtmDbHEdit;
    AtmDbHEdit_AutoInc: TAtmDbHEdit;
    AtmDbHEdit_Lakoach: TAtmDbHEdit;
    AtmDbComboBox_SugHovala: TAtmDbComboBox;
    AtmDbHEdit_Teuda: TAtmDbHEdit;
    AtmDbHEdit4: TAtmDbHEdit;
    AtmDbHMemo_Remark: TAtmDbHMemo;
    Lbl_Remark: TLabel;
    ScrollBox1: TScrollBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Lbl_PriceA: TLabel;
    Lbl_PriceB: TLabel;
    Lbl_PriceC: TLabel;
    Lbl_PriceD: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    AtmDbHEdit_YeMida1: TAtmDbHEdit;
    AtmDbHEdit_Kamut1: TAtmDbHEdit;
    AtmDbHEdit_Price1: TAtmDbHEdit;
    AtmDbHEdit_Total1: TAtmDbHEdit;
    AtmDbHEdit_YeMida2: TAtmDbHEdit;
    AtmDbHEdit_Kamut2: TAtmDbHEdit;
    AtmDbHEdit_Price2: TAtmDbHEdit;
    AtmDbHEdit_Total2: TAtmDbHEdit;
    AtmDbHEdit_YeMida3: TAtmDbHEdit;
    AtmDbHEdit_Kamut3: TAtmDbHEdit;
    AtmDbHEdit_Price3: TAtmDbHEdit;
    AtmDbHEdit_Total3: TAtmDbHEdit;
    AtmDbHEdit_YeMida4: TAtmDbHEdit;
    AtmDbHEdit_Kamut4: TAtmDbHEdit;
    AtmDbHEdit_Price4: TAtmDbHEdit;
    AtmDbHEdit_Total4: TAtmDbHEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    Procedure WriteYehusStatusBar;
    Procedure BuildLookupList;
    Procedure ArrangeWintudaForGas;



  public
    { Public declarations }
  end;

var
  FormWinTudaNew: TFormWinTudaNew;

implementation

uses DMWinTuda,F_GlblWinTuda,AtmRutin,AtmConst, FrmSearch,F_WinTudaParam,
  MslDllInterface, F_ChooseYehus, F_CopyTnua, F_UpdPriceFromMehiron;

{$R *.DFM}

Procedure TFormWinTudaNew.WriteYehusStatusBar;
Begin
     StatusBar1.Panels[PnlNum_Yehus].Text:=IntToStr(WinTudaYehusMonth)+'/'+IntToStr(WinTudaYehusYear);
     Caption := '������ ���� - '+StatusBar1.Panels[PnlNum_Yehus].Text+' - '+StatusBar1.Panels[PnlNum_DSState].Text;          
End;


Procedure TFormWinTudaNew.BuildLookupList;
Var
   SList:TStringList;
Begin
  SList:=TStringList.Create;
  Try
    SList.AddObject(IntToStr(SugTavla_SugHovala),AtmDbComboBox_SugHovala);
    FillKodTavlaComboList(DM_WinTuda.Tbl_KodTavla,SList);
  Finally
        SList.Free;
  End;
  
End;

Procedure TFormWinTudaNew.ArrangeWintudaForGas;
var
   MishkalWidth, Kamutwidth : Integer ;
   MishkalTabIndex, KamutTabIndex : Integer ;
   MishkalLocation, KamutLocation :TPoint ;
   Tmpstr, sStr : String ;
begin
     Label4.Visible := True;
     AtmDbHEdit1.Visible := True;
     DBRadioGroup_SugKamut.Visible := True;
     Label24.Visible := True;
     DBRadioGroup_SugTemperature.Visible := True;
     Tmpstr :=  Label5.Caption ;
     Label5.Caption :=  Label21.Caption ;
     Label21.Caption := Tmpstr ;
     Label7.Visible := True;
     AtmDbHEdit4.Visible := True;
     Label5.Visible := True;

     with DM_WinTuda do
     begin
        Tbl_AzmnLookAzmnType.LookupList.Add(0,'�����');
        Tbl_AzmnLookAzmnType.LookupList.Add(1,'������ ������');
        Tbl_AzmnLookAzmnType.LookupList.Add(2,'������');
        Tbl_AzmnLookAzmnType.LookupList.Add(3,'�� ���� �����');
     end;
     
     AtmDbComboBox_azmnType.AddToList('�����',0);
     AtmDbComboBox_azmnType.AddToList('������ ������',1);
     AtmDbComboBox_azmnType.AddToList('������',2);//�� ���� ����
     AtmDbComboBox_azmnType.AddToList('�� ���� �����',3);


     // A change asked by ron - not applyed yet
     MishkalLocation.x := AtmDbHEdit4.Left ;
     MishkalLocation.y := AtmDbHEdit4.Top ;
     KamutLocation.x := AtmDbHEdit_Kamut1.Left ;
     KamutLocation.y := AtmDbHEdit_Kamut1.Top ;
     MishkalWidth := AtmDbHEdit4.ClientWidth ;
     Kamutwidth := AtmDbHEdit_Kamut1.ClientWidth ;
     MishkalTabIndex := AtmDbHEdit4.TabOrder ;
     KamutTabIndex := AtmDbHEdit_Kamut1.TabOrder ;
     

     AtmDbHEdit_Kamut1.Parent := Pnl_Mid ;
     AtmDbHEdit4.Parent := ScrollBox1 ;

     AtmDbHEdit_Kamut1.Left := MishkalLocation.x ;
     AtmDbHEdit_Kamut1.top := MishkalLocation.y ;

     AtmDbHEdit4.Left := KamutLocation.x ;
     AtmDbHEdit4.Top := KamutLocation.y ;


     AtmDbHEdit_Kamut1.ClientWidth := MishkalWidth ;
     AtmDbHEdit4.ClientWidth := Kamutwidth ;

     AtmDbHEdit_Kamut1.TabOrder := MishkalTabIndex ;
     AtmDbHEdit4.TabOrder := KamutTabIndex ;

     

     Lbl_BeforeSupply.Visible := true ;
     Lbl_AfterSupply.Visible := True ;
     AtmDbHEditBeforeSupply.Visible := true ;
     AtmDbHEditAfterSupply.Visible := True ;
     Lbl_DeliveryHour.Visible := True ;
     AtmDbHEditDeliveryHour.Visible := True ;
     
     
     
     
{
     // If Gas then The detail Grid contain diffrent data - the Data comes from Maslul table
     DBGrid_Tnua.DataSource := nil ;
     DBGrid_Tnua.Columns.Clear ;
     DBGrid_Tnua.DataSource := DM_WinTuda.DS_Maslul ;
     //DBGrid_Tnua.Columns.RebuildColumns ;
}

end;



procedure TFormWinTudaNew.FormCreate(Sender: TObject);
Var
   Day :Word;
   StringsToAddToF4IniFile: TStringList;
begin

  Screen.Cursor:=crHourGlass;
  Try
     DataSetPrior1.ShortCut := Shortcut(VK_Prior, [ssCtrl]);
     NextAzmn.ShortCut := Shortcut(VK_Next, [ssCtrl]);     
     Application.Title:='���� 2000';
     Application.BiDiMode:=bdRightToLeft;
     Application.BiDiKeyboard:=HebBidiKeyboard;
     if Frm_GlblWinTuda=Nil Then
        Frm_GlblWinTuda:=TFrm_GlblWinTuda.Create(Self);
     if DM_WinTuda=Nil Then
        DM_WinTuda:=TDM_WinTuda.Create(Self);

     WinTudaShibKind:=skNormal;
     WinTudaPriceKind:=pkShekel;

     DecodeDate(Now,WinTudaYehusYear,WinTudaYehusMonth,Day);
     WriteYehusStatusBar;

  {         AtmTabSheetBuild1.SqlFileName:=DirectoryForScripts+AtmTabSheetBuild1.SqlFileName;
     AtmTabSheetBuild1.ScrFileName:=DirectoryForScripts+AtmTabSheetBuild1.ScrFileName;}
     AtmTabSheetBuild1.ScriptsDir:=DirectoryForScripts;
     AtmTabSheetBuild1.FilterDir:=DirectoryForCurUser;
     AtmTabSheetBuild1.BuildTabsForGrids;

     StringsToAddToF4IniFile := TStringList.Create;         
     ReadF4ValuesToIni(Self,DirectoryForCurUser+Name+'.Ini','FixedFields', StringsToAddToF4IniFile);
     //MaskEditFromTimeFixedString := StringsToAddToF4IniFile.Values[MaskEdit_FromTime.Name];
     //MaskEditToTimeFixedString := StringsToAddToF4IniFile.Values[MaskEdit_ToTime.Name];     
     StringsToAddToF4IniFile.Free;
     
     ActiveControl:=FindFirstControlInTabOrder(Self);

     DynamicControls1.ExtraFiledsFileName:=DirectoryForScripts+DynamicControls1.ExtraFiledsFileName;
     DynamicControls1.Execute;
     BuildLookupList;

    AtmDbComboBox_azmnType.Clear;
    AtmDbComboBox_azmnType.FreeListObjects;
    Case WinTudaSugSystem Of
      ssMaz    : Begin
                   AtmDbComboBox_azmnType.AddToList('�����',0);
                   AtmDbComboBox_azmnType.AddToList('���� ����',1);
                   AtmDbComboBox_azmnType.AddToList('�����',2);//�� ���� ����
                   AtmDbComboBox_azmnType.AddToList('�� ���� �����',3);
                   AtmDbComboBox_azmnType.AddToList('�����',4);
                   {AtmDbComboBox_azmnType.AddToList('����"�',5);}
                   AtmDbComboBox_azmnType.AddToList('���� ����',atHiuvNehag);
                   AtmDbComboBox_azmnType.AddToList('����� ����',atZikuyLak);
                   AtmDbComboBox_azmnType.AddToList('������� �� ������',atMasMizdamen);

                   Action_CopyHatzaotToHazmanot.Visible:=True;
                   Action_AtmAzmnHatz.Visible:=True;
                 End;
      ssTovala : Begin
                   AtmDbComboBox_azmnType.AddToList('�����',0);
                   AtmDbComboBox_azmnType.AddToList('������ ������',1);
                   AtmDbComboBox_azmnType.AddToList('������',2);//�� ���� ����
                   AtmDbComboBox_azmnType.AddToList('�� ���� �����',3);
                   AtmDbComboBox_azmnType.AddToList('�����',4);
                   AtmDbComboBox_azmnType.AddToList('����"�',5);
                   AtmDbComboBox_azmnType.AddToList('���� ���',atHiuvNehag);
                   AtmDbComboBox_azmnType.AddToList('����� ����',atZikuyLak);
                   AtmDbComboBox_azmnType.AddToList('������� �� ������',atMasMizdamen);
                 End;
      ssGazCompany: begin
                    ArrangeWintudaForGas;
                    // Removed by Yaron 
                    // FormStorage_WinTeuda.IniSection :='winTudaForGas';
                    end;
    End; //Case

     Scale1.DoScaleNow;
  Finally
        Screen.Cursor:=crDefault;
  End;

end;

procedure TFormWinTudaNew.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     FormWinTudaNew :=Nil;
     Action:=caFree;

end;

procedure TFormWinTudaNew.ToolButton2Click(Sender: TObject);
begin
     Panel_Top.Visible := Not Panel_Top.Visible ;
     if Panel_Top.Visible then
        ToolButton2.ImageIndex := 21
     else
        ToolButton2.ImageIndex := 20 ;

end;

end.

unit f_Nehag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, DBCtrls, Buttons, StdCtrls, Grids, DBGrids, DBSrtGrd, ExtCtrls,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, AtmComp,Mask, Db, DBTables, AtmAdvTable,
  ToolWin,AtmLookCombo, Scale250, AtmTabSheetBuild, RxQuery, DBFilter,Spin,
  MslDllInterface,AdvSearch, DynamicControls, Menus, ExtDlgs, AtmRxQuery,Yearplan;

type
  TFrm_Nehag = class(TForm)
    Ds_Nehag: TDataSource;
    Panel3: TPanel;
    HebLabel1: TLabel;
    Lbl_ShemNehag: TLabel;
    HebLabel3: TLabel;
    HebLabel4: TLabel;
    HebLabel5: TLabel;
    HebDBText1: TDBText;
    Kod: TAtmDbHEdit;
    AtmDbHEdit2: TAtmDbHEdit;
    Kod_kvutza: TAtmDbHEdit;
    Kod_Miyun: TAtmDbHEdit;
    AtmDbHEdit21: TAtmDbHEdit;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    HebLabel14: TLabel;
    HebLabel16: TLabel;
    HebLabel23: TLabel;
    HebLabel24: TLabel;
    HebLabel27: TLabel;
    HebLabel25: TLabel;
    HebLabel26: TLabel;
    HebLabel44: TLabel;
    HebLabel45: TLabel;
    HebLabel46: TLabel;
    HebLabel47: TLabel;
    HebLabel10: TLabel;
    HebLabel28: TLabel;
    HebLabel29: TLabel;
    HebLabel35: TLabel;
    HebLabel36: TLabel;
    HebGroupBox1: TGroupBox;
    HebLabel6: TLabel;
    HebLabel7: TLabel;
    HebLabel8: TLabel;
    HebLabel9: TLabel;
    AtmDbHEdit4: TAtmDbHEdit;
    AtmDbHEdit5: TAtmDbHEdit;
    AtmDbHEdit6: TAtmDbHEdit;
    AtmDbHEdit3: TAtmDbHEdit;
    HebGroupBox2: TGroupBox;
    HebLabel11: TLabel;
    HebLabel12: TLabel;
    HebLabel13: TLabel;
    AtmDbHEdit7: TAtmDbHEdit;
    AtmDbHEdit8: TAtmDbHEdit;
    AtmDbHEdit9: TAtmDbHEdit;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmDbHEdit10: TAtmDbHEdit;
    AtmDbHMemo1: TAtmDbHMemo;
    AtmDbHEdit36: TAtmDbHEdit;
    AtmDbHEdit37: TAtmDbHEdit;
    Shana_Hotzaat_Rishayon: TAtmDbHEdit;
    Kod_Agaph: TAtmDbHEdit;
    Kod_Machlaka: TAtmDbHEdit;
    AtmDbHEdit26: TAtmDbHEdit;
    AtmDbHEdit44: TAtmDbHEdit;
    HebDBRadioGroup1: TDBRadioGroup;
    AtmDBDateEdit1: TAtmDBDateEdit;
    AtmDBDateEdit2: TAtmDBDateEdit;
    Edt_Taarich_Leda: TAtmDBDateEdit;
    AtmDBDateEdit4: TAtmDBDateEdit;
    Mis_Rishui: TAtmDbHEdit;
    TS_Main2: TTabSheet;
    HebLabel37: TLabel;
    HebLabel38: TLabel;
    HebLabel39: TLabel;
    HebLabel33: TLabel;
    HebLabel40: TLabel;
    HebLabel42: TLabel;
    HebLabel43: TLabel;
    HebLabel21: TLabel;
    HebLabel22: TLabel;
    HebLabel15: TLabel;
    HebLabel18: TLabel;
    HebLabel19: TLabel;
    HebLabel20: TLabel;
    HebLabel30: TLabel;
    HebLabel31: TLabel;
    AtmDbHEdit28: TAtmDbHEdit;
    AtmDbHEdit29: TAtmDbHEdit;
    AtmDbHEdit33: TAtmDbHEdit;
    AtmDbHEdit34: TAtmDbHEdit;
    AtmDbHEdit35: TAtmDbHEdit;
    AtmDbHEdit24: TAtmDbHEdit;
    AtmDbHEdit31: TAtmDbHEdit;
    AtmDbHEdit11: TAtmDbHEdit;
    AtmDbHEdit12: TAtmDbHEdit;
    AtmDbHEdit13: TAtmDbHEdit;
    AtmDbHEdit23: TAtmDbHEdit;
    Kod_Kesher_Av: TAtmDbHEdit;
    AtmDbHEdit14: TAtmDbHEdit;
    TS_Meda: TTabSheet;
    Bevel3: TBevel;
    Image_Meda: TImage;
    DBSrtGrd3: TDBSrtGrd;
    Btn_PicMeda: TBitBtn;
    DBEdit3: TDBEdit;
    TS_Grid: TTabSheet;
    Grid_Nehag: TDBSrtGrd;
    TS_ExtraFileds: TTabSheet;
    Ts_Picture: TTabSheet;
    Image_Driver: TImage;
    Image_Rishayon: TImage;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Spb_ScanNehagPic: TSpeedButton;
    Spb_ScanDriverLisence: TSpeedButton;
    DBEdit1: TDBEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBEdit2: TDBEdit;
    AtmDbComboBox_SugRehev: TAtmDbComboBox;
    AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox;
    Scale1: TScale;
    AtmDbComboBox_KodAgaf: TAtmDbComboBox;
    AtmDbComboBox_Kod_Machlaka: TAtmDbComboBox;
    AtmDbComboBox_Kod_Miyun: TAtmDbComboBox;
    DS_TabSheetBuild: TDataSource;
    RxDBFilter_TabSheetBuild: TRxDBFilter;
    RxQuery_TabSheetBuild: TRxQuery;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    Tbl_Meda: TAtmTable;
    DS_Meda: TDataSource;
    Tbl_MedaTnua: TAutoIncField;
    Tbl_MedaKod_Nehag: TIntegerField;
    Tbl_MedaKod: TIntegerField;
    Tbl_MedaTeur: TStringField;
    Tbl_MedaMaslul_Bmp: TStringField;
    Tbl_MedaLookTavla: TStringField;
    Spb_OpenMslDlgSearchPlace: TSpeedButton;
    AtmAdvSearch_Rehev: TAtmAdvSearch;
    StatusBar1: TStatusBar;
    DynamicControls1: TDynamicControls;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    AtmDBDateEdit3: TAtmDBDateEdit;
    OpenPictureDialog1: TOpenPictureDialog;
    UpdateSQL_Nehag: TUpdateSQL;
    Tbl_Nehag: TAtmRxQuery;
    ToolBar1: TToolBar;
    DBNavigator1: TDBNavigator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    TBtn_Report: TToolButton;
    Tbl_NehagKod_Nehag: TIntegerField;
    Tbl_NehagKod_Kvutza: TIntegerField;
    Tbl_NehagKod_Miyun: TIntegerField;
    Tbl_NehagMis_han: TStringField;
    Tbl_NehagShem_Nehag: TStringField;
    Tbl_NehagKtovet_1: TStringField;
    Tbl_NehagIr_1: TStringField;
    Tbl_NehagMikud_1: TIntegerField;
    Tbl_NehagKtovet_2: TStringField;
    Tbl_NehagIr_2: TStringField;
    Tbl_NehagMikud_2: TIntegerField;
    Tbl_NehagTel_1: TStringField;
    Tbl_NehagTel_2: TStringField;
    Tbl_NehagFax: TStringField;
    Tbl_NehagPle_Phon: TStringField;
    Tbl_NehagIsh_Kesher: TStringField;
    Tbl_NehagOsek_morshe_ID: TStringField;
    Tbl_NehagTaarich_Osek_morshe: TDateTimeField;
    Tbl_NehagKod_Tnai_tashlum: TIntegerField;
    Tbl_NehagKod_Eara: TIntegerField;
    Tbl_NehagMas_Makor: TBCDField;
    Tbl_NehagTaarich_mas: TDateTimeField;
    Tbl_NehagAchuz_Anacha_Amala: TBCDField;
    Tbl_NehagSug_Anacha_Amala: TIntegerField;
    Tbl_NehagKod_Kesher: TIntegerField;
    Tbl_NehagTaarich_Atchala: TDateTimeField;
    Tbl_NehagTaarich_Siyum: TDateTimeField;
    Tbl_NehagTaarich_Leda: TDateTimeField;
    Tbl_NehagEarot: TMemoField;
    Tbl_NehagMis_Rishui: TStringField;
    Tbl_NehagSug_Rehev: TIntegerField;
    Tbl_NehagMis_Rishayon: TStringField;
    Tbl_NehagTaarich_Tokef_Rishayon: TDateTimeField;
    Tbl_NehagTaarich_Chidush_Rishayon: TDateTimeField;
    Tbl_NehagShana_Hotzaat_Rishayon: TIntegerField;
    Tbl_NehagDarga_Rishayon: TStringField;
    Tbl_NehagMis_Oved: TIntegerField;
    Tbl_NehagKod_Agafh: TIntegerField;
    Tbl_NehagKod_Machlaka: TIntegerField;
    Tbl_NehagKod_Mechiron: TIntegerField;
    Tbl_NehagSug_Koteret: TIntegerField;
    Tbl_NehagTotal_Hovalot: TBCDField;
    Tbl_NehagTotal_Mas_Makor: TBCDField;
    Tbl_NehagKm_Muktzav: TBCDField;
    Tbl_NehagKod_Lakuach: TIntegerField;
    Tbl_NehagKod_Sapak: TIntegerField;
    Tbl_NehagMaslul_Bmp: TStringField;
    Tbl_NehagRishayon_Bmp: TStringField;
    Tbl_NehagString_1: TStringField;
    Tbl_NehagString_2: TStringField;
    Tbl_NehagString_3: TStringField;
    Tbl_NehagString_4: TStringField;
    Tbl_NehagString_5: TStringField;
    Tbl_NehagString_6: TStringField;
    Tbl_NehagString_7: TStringField;
    Tbl_NehagString_8: TStringField;
    Tbl_NehagInteger_1: TIntegerField;
    Tbl_NehagInteger_2: TIntegerField;
    Tbl_NehagInteger_3: TIntegerField;
    Tbl_NehagInteger_4: TIntegerField;
    Tbl_NehagInteger_5: TIntegerField;
    Tbl_NehagDate_1: TDateTimeField;
    Tbl_NehagDate_2: TDateTimeField;
    Tbl_NehagDate_3: TDateTimeField;
    Tbl_NehagDate_4: TDateTimeField;
    Tbl_NehagDate_5: TDateTimeField;
    Tbl_NehagMoney_1: TBCDField;
    Tbl_NehagMoney_2: TBCDField;
    Tbl_NehagMoney_3: TBCDField;
    Tbl_NehagMoney_4: TBCDField;
    Tbl_NehagMoney_5: TBCDField;
    Tbl_NehagMoney_6: TBCDField;
    Tbl_NehagMoney_7: TBCDField;
    Tbl_NehagNehag_Pail: TSmallintField;
    Tbl_NehagKod_Kesher_Av: TIntegerField;
    Tbl_NehagInt_Kod_Kvutza: TIntegerField;
    Tbl_NehagInt_Kod_Miyun: TIntegerField;
    Tbl_NehagInt_Kod_Tnai_Tashlum: TIntegerField;
    Tbl_NehagInt_Sug_rehev: TIntegerField;
    Tbl_NehagInt_Kod_Agafh: TIntegerField;
    Tbl_NehagInt_Kod_Machlaka: TIntegerField;
    Tbl_NehagE_Mail: TStringField;
    Tbl_NehagLookAgaf: TStringField;
    Tbl_NehagLookMiun: TStringField;
    Tbl_NehagLookKvutza: TStringField;
    Tbl_NehagLookMachlaka: TStringField;
    N2: TMenuItem;
    TS_CALNehag: TTabSheet;
    Panel1: TPanel;
    YearPlanner1: TYearPlanner;
    SpinEdit_Year: TSpinEdit;
    AtmRxQuery_Calendar: TAtmRxQuery;
    PopupMenu_SugDay: TPopupMenu;
    Shape_Color7: TShape;
    Shape_Color8: TShape;
    Shape_Color5: TShape;
    Shape_Color6: TShape;
    Shape_Color3: TShape;
    Shape_Color4: TShape;
    Shape_Color1: TShape;
    Shape_Color2: TShape;
    Label_Color1: TLabel;
    Label_Color2: TLabel;
    Label_Color3: TLabel;
    Label_Color4: TLabel;
    Label_Color5: TLabel;
    Label_Color6: TLabel;
    Label_Color7: TLabel;
    Label_Color8: TLabel;
    DBCheckBox_WithMam: TDBCheckBox;
    TBtn_ShowMap: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure Kod_AgaphBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_MachlakaBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Sug_RehevBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_MiyunBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_kvutzaBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_Tnai_TashlumBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Tbl_NehagAfterInsert(DataSet: TDataSet);
    procedure Tbl_NehagAfterPost(DataSet: TDataSet);
    procedure DBNavigator1BeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Tbl_NehagNewRecord(DataSet: TDataSet);
    procedure DBSrtGrd3EditButtonClick(Sender: TObject);
    procedure Tbl_MedaBeforePost(DataSet: TDataSet);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure Tbl_NehagAfterScroll(DataSet: TDataSet);
    procedure Spb_OpenMslDlgSearchPlaceClick(Sender: TObject);
    procedure Tbl_NehagMis_RishuiValidate(Sender: TField);
    procedure Ds_NehagStateChange(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure Tbl_NehagTaarich_AtchalaValidate(Sender: TField);
    procedure Tbl_NehagTaarich_SiyumValidate(Sender: TField);
    procedure Btn_PicMedaClick(Sender: TObject);
    procedure Tbl_MedaAfterScroll(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Spb_ScanNehagPicClick(Sender: TObject);
    procedure Spb_ScanDriverLisenceClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Tbl_NehagMis_RishuiSetText(Sender: TField;
      const Text: String);
    procedure SpinEdit_YearChange(Sender: TObject);
    procedure Tbl_NehagBeforePost(DataSet: TDataSet);
    procedure TBtn_ShowMapClick(Sender: TObject);
  private
    { Private declarations }
    Procedure LoadBitmaps;
    Procedure BuildSugDayMenu;
  public
     Procedure ShowForm(FormParams:ShortString);
     Procedure LoadCalendarAndUpdateView;
     Procedure DaySelected(Sender :TObject);
     Function  SugDayToColor(SugDay :LongInt):TColor;
  Protected
    Procedure BuildLookupList;
  end;

var
  Frm_Nehag: TFrm_Nehag;

implementation
Uses DmAtmCrt,AtmRutin,AtmConst,Crt_Glbl;
{$R *.DFM}
Const
     PnlNum_DSState=0;

procedure TFrm_Nehag.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     PageControl1.ActivePage:=PageControl1.Pages[0];
     DynamicControls1.ExtraFiledsFileName:=CrtDirForScripts+DynamicControls1.ExtraFiledsFileName;
     DynamicControls1.Execute;
     BuildLookupList;
     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataSet) And (Components[i].Tag=1) Then
             (Components[I] As TDataSet).Open
          Else
          if Components[i] is TAtmAdvSearch Then
            TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;

     Scale1.DoScaleNow;

     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
     PageControl1.ActivePage:=TS_Main;

{     AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
     AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;}
     AtmTabSheetBuild1.ScriptsDir:=CrtDirForScripts;
     AtmTabSheetBuild1.FilterDir:=CrtDirForCurUser;

     AtmTabSheetBuild1.BuildTabsForGrids;

     BuildSugDayMenu;
  Finally
         Screen.Cursor:=crDefault;
  End;

end;

procedure TFrm_Nehag.Kod_AgaphBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Agaf;
end;

procedure TFrm_Nehag.Kod_MachlakaBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Machlaka;
end;

procedure TFrm_Nehag.Sug_RehevBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugRehev;
end;

procedure TFrm_Nehag.Kod_MiyunBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_MiunNehag;
end;

procedure TFrm_Nehag.Kod_kvutzaBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_GroupNehag;
end;

procedure TFrm_Nehag.Kod_Tnai_TashlumBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_TnaeyTashlum;
end;

procedure TFrm_Nehag.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_Nehag.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Nehag);
end;

procedure TFrm_Nehag.Tbl_NehagAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_Nehag.Tbl_NehagAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_Nehag.DBNavigator1BeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        ((Sender As TDBNavigator).DataSource.DataSet As TAtmRxQuery).SavePressed:=True;
     if Button=nbRefresh Then
     Begin
        (Sender As TDBNavigator).DataSource.DataSet.Close;
        (Sender As TDBNavigator).DataSource.DataSet.Open;
        BuildLookupList;
        Abort;
     End;
end;

procedure TFrm_Nehag.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Nehag:=Nil;
     Action:=caFree;
end;

Procedure TFrm_Nehag.ShowForm(FormParams:ShortString);
Begin
     Show;
     if FormParams<>'' Then
        Tbl_Nehag.FindKey([FormParams])
     Else
        //Tbl_Nehag.Insert;
        SetDatasetState(Tbl_Nehag,CrtFirstTableState);
End;
procedure TFrm_Nehag.Tbl_NehagNewRecord(DataSet: TDataSet);
Var
   MaxNehag :LongInt;

begin
     With DM_AtmCrt Do
     Begin
          Qry_Temp.Active:=False;
          With Qry_Temp.Sql Do
          Begin
               Clear;
               Add('Select Max(Kod_Nehag) MN From Nehag');
          End;
          Qry_Temp.Active:=True;
          MaxNehag:=Qry_Temp.FieldByName('MN').AsInteger+1;
          Qry_Temp.Active:=False;
          Tbl_Nehag.FieldByName('Kod_Nehag').AsInteger:=MaxNehag;
     End;
     Tbl_Nehag.FieldByName('Kod_Agafh').AsInteger:=0;
end;

Procedure TFrm_Nehag.BuildLookupList;
Var
   SList:TStringList;
   I :longInt;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_Machlaka),Tbl_Nehag.FieldByName('LookMachlaka'));
     SList.AddObject(IntToStr(SugTavla_Agaf),Tbl_Nehag.FieldByName('LookAgaf'));
     SList.AddObject(IntToStr(SugTavla_MiunNehag),Tbl_Nehag.FieldByName('LookMiun'));
     SList.AddObject(IntToStr(SugTavla_GroupNehag),Tbl_Nehag.FieldByName('LookKvutza'));
     SList.AddObject(IntToStr(SugTavla_MedaNehag),Tbl_MedaLookTavla);
     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_SugRehev),AtmDbComboBox_SugRehev);
     SList.AddObject(IntToStr(SugTavla_TnaeyTashlum),AtmDbComboBox_TnaeyTashlum);
     SList.AddObject(IntToStr(SugTavla_Agaf),AtmDbComboBox_KodAgaf);
     SList.AddObject(IntToStr(SugTavla_Machlaka),AtmDbComboBox_Kod_Machlaka);
     SList.AddObject(IntToStr(SugTavla_MiunNehag),AtmDbComboBox_Kod_Miyun);
     For I:=0 To ComponentCount-1 Do
     Begin
        if Components[i] is TAtmDbComboBox Then
          if TAtmDbComboBox(Components[i]).SugTavlaIndex>0 Then
            SList.AddObject(IntToStr(TAtmDbComboBox(Components[i]).SugTavlaIndex),Components[i]);
     End;

     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;
procedure TFrm_Nehag.DBSrtGrd3EditButtonClick(Sender: TObject);
begin
     (DM_AtmCrt.AtmAdvSearch_KodTavla.SourceDataSet As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_MedaNehag;
     if DM_AtmCrt.AtmAdvSearch_KodTavla.Execute Then
     Begin
        if Tbl_Meda.State=dsBrowse Then
           Tbl_Meda.Edit;
        Tbl_MedaKod.AsString:=DM_AtmCrt.AtmAdvSearch_KodTavla.ReturnString;
     End;
end;

procedure TFrm_Nehag.Tbl_MedaBeforePost(DataSet: TDataSet);
begin
     Tbl_MedaKod_Nehag.AsInteger:=Tbl_Nehag.FieldByName('Kod_Nehag').AsInteger;
end;

procedure TFrm_Nehag.AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
begin
     if TQuery(AtmTabSheetBuild1.Query).Params.FindParam('PKod_Nehag')<>Nil Then
        TQuery(AtmTabSheetBuild1.Query).ParamByName('PKod_Nehag').AsInteger:=Tbl_Nehag.FieldByName('Kod_Nehag').AsInteger;
end;

procedure TFrm_Nehag.Tbl_NehagAfterScroll(DataSet: TDataSet);
begin
     LoadBitmaps;
     if AtmTabSheetBuild1.Query.Active Then
        AtmTabSheetBuild1.RefreshQry(Nil);
     if PageControl1.ActivePage=TS_CALNehag Then
        LoadCalendarAndUpdateView;
end;

procedure TFrm_Nehag.Spb_OpenMslDlgSearchPlaceClick(Sender: TObject);
Var
   TheNode :TNode;
begin
     if Not MslDllLoaded Then
        Exit;
     if MslFindNodeFromDlgSearchPlace(TheNode,'') Then
     Begin
          if Tbl_Nehag.State=dsBrowse Then
             Tbl_Nehag.Edit;
          Tbl_Nehag.FieldByName('Ir_1').AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,1);
          Tbl_Nehag.FieldByName('Ktovet_1').AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,2)+MslSeparator+GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,3);

//          PutNodeDataInRecord(TheNode);
     End;
end;

procedure TFrm_Nehag.Tbl_NehagMis_RishuiValidate(Sender: TField);
begin
     if Sender.AsString<>'' Then
     With DM_AtmCrt,Qry_Temp Do
     Begin
          Active:=False;
          Sql.Clear;
          Sql.Add('Select Count(*) RecFound From Rehev Where Mis_Rishui = '''+Sender.AsString+'''');
          Active:=True;
          Try
              if FieldByName('RecFound').AsInteger<1 Then
              Begin
                   MessageDlg('��� �� ����',mtError,[mbOk],0);
                   Abort;
              End;
          Finally
                 Active:=False;
          End;
     End;//With
end;

procedure TFrm_Nehag.Ds_NehagStateChange(Sender: TObject);
begin
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

procedure TFrm_Nehag.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msNehag],False,CrtMeholelHandle);

end;

procedure TFrm_Nehag.Tbl_NehagTaarich_AtchalaValidate(Sender: TField);
begin
     if ((Tbl_Nehag.FieldByName('Taarich_Atchala').AsDateTime) > (Tbl_Nehag.FieldByName('Taarich_Siyum').AsDateTime)) And (Not Tbl_Nehag.FieldByName('Taarich_Siyum').IsNull) Then
        Tbl_Nehag.FieldByName('Taarich_Siyum').AsDateTime:=Tbl_Nehag.FieldByName('Taarich_Atchala').AsDateTime;
end;

procedure TFrm_Nehag.Tbl_NehagTaarich_SiyumValidate(Sender: TField);
begin
     if (Tbl_Nehag.FieldByName('Taarich_Atchala').AsDateTime > Tbl_Nehag.FieldByName('Taarich_Siyum').AsDateTime) And (Not Tbl_Nehag.FieldByName('Taarich_Atchala').IsNull)Then
        Tbl_Nehag.FieldByName('Taarich_Atchala').AsDateTime:=Tbl_Nehag.FieldByName('Taarich_Siyum').AsDateTime;

end;

procedure TFrm_Nehag.Btn_PicMedaClick(Sender: TObject);
begin
    if OpenPictureDialog1.Execute  then
      begin
        Tbl_Meda.Edit;
        Tbl_MedaMaslul_Bmp.Text := OpenPictureDialog1.FileName;
      end;
    LoadBitmaps;
end;

Procedure TFrm_Nehag.LoadBitmaps;
Begin
  if PageControl1.ActivePage=Ts_Picture Then
  Begin
      try
      if (Tbl_Nehag.FieldByName('Rishayon_Bmp').isnull) or
         (Tbl_Nehag.FieldByName('Rishayon_Bmp').Text = '') then
         begin
           Image_Rishayon.Picture := nil;
         end
      else
         begin
           Image_Rishayon.Picture.LoadFromFile(Tbl_Nehag.FieldByName('Rishayon_Bmp').AsString);
         end;
      except
         Image_Rishayon.Picture := nil;
      End;

      Try
      if (Tbl_Nehag.FieldByName('Maslul_Bmp').isnull) or
         (Tbl_Nehag.FieldByName('Maslul_Bmp').Text = '') then
         begin
           Image_Driver.Picture := nil;
         end
      else
         begin
           Image_Driver.Picture.LoadFromFile(Tbl_Nehag.FieldByName('Maslul_Bmp').AsString);
         end;
      except
         Image_Driver.Picture := nil;
      End;
      Exit;
  End;

  if PageControl1.ActivePage=TS_Meda Then
  Begin
      try
      if (Tbl_MedaMaslul_Bmp.isnull) or
         (Tbl_MedaMaslul_Bmp.Text = '') then
         begin
           Image_Meda.Picture := nil;
         end
      else
         begin
           Image_Meda.Picture.LoadFromFile(Tbl_MedaMaslul_Bmp.Text);
         end;
      except
         Image_Meda.Picture := nil;
      end;
      Exit;
  End;
End;

procedure TFrm_Nehag.Tbl_MedaAfterScroll(DataSet: TDataSet);
begin
     LoadBitmaps;
end;

procedure TFrm_Nehag.BitBtn1Click(Sender: TObject);
begin
    if OpenPictureDialog1.Execute  then
      begin
        Tbl_Nehag.Edit;
        Tbl_Nehag.FieldByName('Maslul_Bmp').Text := OpenPictureDialog1.FileName;
      end;
    LoadBitmaps;
end;

procedure TFrm_Nehag.BitBtn2Click(Sender: TObject);
begin
    if OpenPictureDialog1.Execute  then
      begin
        Tbl_Nehag.Edit;
        Tbl_Nehag.FieldByName('Rishayon_Bmp').Text := OpenPictureDialog1.FileName;
      end;
    LoadBitmaps;

end;

procedure TFrm_Nehag.Spb_ScanNehagPicClick(Sender: TObject);
Var
   StrH :String;
   H :THandle;
begin
  if Tbl_Nehag.State=dsBrowse Then Tbl_Nehag.Edit;
  if Image_Driver.Picture<>Nil Then Image_Driver.Picture:=Nil;
  H:=0;
  RunDllScanImage('AtmScan.Dll','ScanImage',Image_Driver,True,H);
  if Image_Driver.Picture<>Nil Then
  Begin
       StrH:=DirectoryForIni+'P'+Tbl_Nehag.FieldByName('Kod_Nehag').AsString+'.Bmp';
       Image_Driver.Picture.SaveToFile(StrH);
       Tbl_Nehag.FieldByName('Maslul_Bmp').AsSTring:=StrH;
  End;

end;

procedure TFrm_Nehag.Spb_ScanDriverLisenceClick(Sender: TObject);
Var
   StrH :String;
   H :THandle;
begin
  if Tbl_Nehag.State=dsBrowse Then Tbl_Nehag.Edit;
  if Image_Rishayon.Picture<>Nil Then Image_Rishayon.Picture:=Nil;
  H:=0;
  RunDllScanImage('AtmScan.Dll','ScanImage',Image_Rishayon,True,H);
  if Image_Rishayon.Picture<>Nil Then
  Begin
       StrH:=DirectoryForIni+'L'+Tbl_Nehag.FieldByName('Kod_Nehag').AsString+'.Bmp';
       Image_Rishayon.Picture.SaveToFile(StrH);
       Tbl_Nehag.FieldByName('Rishayon_Bmp').AsString:=StrH;
  End;

end;

procedure TFrm_Nehag.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePage=Ts_Grid Then
    Grid_Nehag.DataSource:=Ds_Nehag
  Else
    Grid_Nehag.DataSource:=Nil;

  if PageControl1.ActivePage=TS_CALNehag Then
  Begin
    YearPlanner1.Year:=SpinEdit_Year.Value;
    LoadCalendarAndUpdateView;
  End;
end;

procedure TFrm_Nehag.Tbl_NehagMis_RishuiSetText(Sender: TField;
  const Text: String);
begin
  Sender.AsString:=ConvertToRishuy(Text);
end;

Procedure TFrm_Nehag.LoadCalendarAndUpdateView;
Var
  D,M,Y :Word;
  I,J :LongInt;
Begin
    For I:=1 To 12 Do
      For J:=1 To 31 Do
      Begin
        YearPlanner1.CellData[I,J].CellHint:='';
        YearPlanner1.CellData[I,J].CustomColor:=False;
        YearPlanner1.CellData[I,J].CellColor:=YearPlanner1.DayColor;
        YearPlanner1.CellData[I,J].CellTag:=-1;
      End;

    With AtmRxQuery_Calendar Do //���� ��� ����
    Begin
      Active:=False;
      ParamByName('PYear').AsInteger:=SpinEdit_Year.Value;
      ParamByName('PCodeCalendar').AsInteger:=Tbl_Nehag.FieldByName('Kod_Nehag').AsInteger;
      Screen.Cursor:=crSqlWait;
      Active:=True;
      Try
        First;
        While Not Eof Do
        Begin
          DecodeDate(FieldByName('DAY_DATE').AsDateTime,y,m,d);
          YearPlanner1.CellData[m,d].CellTag:=FieldByName('CODE_SUG_DAY').AsInteger;
          YearPlanner1.CellData[m,d].CellColor:=SugDayToColor(FieldByName('CODE_SUG_DAY').AsInteger);
          YearPlanner1.CellData[m,d].CustomColor:=True;
          For I:=0 To PopupMenu_SugDay.Items.Count-1 Do
            if TMenuItem(PopupMenu_SugDay.Items[i]).Tag=FieldByName('CODE_SUG_DAY').AsInteger Then
            Begin
              YearPlanner1.CellData[m,d].CellHint:=TMenuItem(PopupMenu_SugDay.Items[i]).Caption;
              Break;
            End;
          Next;
        End;
      Finally
        Active:=False;
        Screen.Cursor:=crDefault;
      End;
    End;
    YearPlanner1.Invalidate;
End;

Procedure TFrm_Nehag.BuildSugDayMenu;
Var
  TM :TMenuItem;
  StrH :String;
Begin
    With DM_AtmCrt.Qry_Temp Do //����� ���� �����
    Begin
      TM:=TMenuItem.Create(PopupMenu_SugDay);
      TM.Caption:='��� ����� ����';
      TM.Tag:=-1;
      TM.OnClick:=DaySelected;
      PopupMenu_SugDay.Items.Add(TM);
      Active:=False;
      Sql.Clear;
      Sql.Add('Select * From KODTAVLA');
      Sql.Add('Where Sug_Tavla='+IntToStr(SugTavla_SugDay));
      Try
        Active:=True;
        First;
        While Not Eof Do
        Begin
          TM:=TMenuItem.Create(PopupMenu_SugDay);
          TM.Caption:=FieldByName('Teur_Tavla').AsString;
          TM.Tag:=FieldByName('Kod_Tavla').AsInteger;
          TM.OnClick:=DaySelected;
          PopupMenu_SugDay.Items.Add(TM);

          StrH:='Label_Color'+FieldByName('Kod_Tavla').AsString;
          if Self.FindComponent(Trim(StrH))<>Nil Then
          Begin
            TWinControl(Self.FindComponent('Shape_Color'+FieldByName('Kod_Tavla').AsString)).Visible:=True;
            TWinControl(Self.FindComponent('Label_Color'+FieldByName('Kod_Tavla').AsString)).Visible:=True;
            TLabel(Self.FindComponent('Label_Color'+FieldByName('Kod_Tavla').AsString)).Caption:=FieldByName('Teur_Tavla').AsString;
          End;

          Next;
        End;
      Finally
        Active:=False;
      End;
    End;
End;

Procedure TFrm_Nehag.DaySelected(Sender :TObject);
Var
  m,d :Word;
  CurDate:TDateTime;
  CurDateString :String;
Begin
  d:=YearPlanner1.CurrentDate.Day;
  m:=YearPlanner1.CurrentDate.Month;
  if YearPlanner1.CellData[M,D].CellTag=TMenuItem(Sender).Tag Then
    Exit;
  CurDate:=EncodeDate(SpinEdit_Year.Value,M,D);
  CurDateString:=DateToSqlStr(CurDate,'mm/dd/yyyy');

  if YearPlanner1.CellData[M,D].CellTag>-1 Then //��� ��� ����
  Begin
    if TMenuItem(Sender).Tag=-1 Then //����� ���� ���� - ����� ������
    With DM_AtmCrt.Qry_Temp Do
    Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Delete From CALNEHAG');
      Sql.Add('Where SCHOOL_YEAR='+SpinEdit_Year.Text);
      Sql.Add('And CODE_CALENDER='+Tbl_Nehag.FieldByName('Kod_Nehag').AsString);
      Sql.Add('And DAY_DATE='''+CurDateString+'''');
      ExecSQL;
    End
    Else
    Begin //����� ���
      With DM_AtmCrt.Qry_Temp Do
      Begin
        Active:=False;
        Sql.Clear;
        Sql.Add('Update CALNEHAG');
        Sql.Add('Set DESCRIPTION='''+TMenuItem(Sender).Caption+''',');
        Sql.Add('DAY_OF_WEEK='''+ShortDayNames[DayOfWeek(EncodeDate(SpinEdit_Year.Value,M,D))]+''',');
        Sql.Add('CODE_SUG_DAY='+IntToStr(TMenuItem(Sender).Tag));
        Sql.Add('Where SCHOOL_YEAR='+SpinEdit_Year.Text);
        Sql.Add('And CODE_CALENDER='+Tbl_Nehag.FieldByName('Kod_Nehag').AsString);
        Sql.Add('And DAY_DATE='''+CurDateString+'''');
        ExecSQL;
      End;
    End;
  End
  Else //��� ��� ���� �� ������ ���
  Begin
    With DM_AtmCrt.Qry_Temp Do
    Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Insert into CALNEHAG');
      Sql.Add('(SCHOOL_YEAR,CODE_CALENDER,DAY_DATE,DESCRIPTION,DAY_OF_WEEK,CODE_SUG_DAY)');
      Sql.Add('Values');
      Sql.Add('('+SpinEdit_Year.Text+',');
      Sql.Add(Tbl_Nehag.FieldByName('Kod_Nehag').AsString+',');
      Sql.Add(''''+CurDateString+''',');
      Sql.Add(''''+TMenuItem(Sender).Caption+''',');
      Sql.Add(''''+ShortDayNames[DayOfWeek(CurDate)]+''',');
      Sql.Add(IntToStr(TMenuItem(Sender).Tag)+')');
      ExecSQL;
    End;
  End;

  YearPlanner1.CellData[m,d].CellTag:=TMenuItem(Sender).Tag;
  YearPlanner1.CellData[m,d].CellColor:=SugDayToColor(TMenuItem(Sender).Tag);
  YearPlanner1.CellData[m,d].CustomColor:=TMenuItem(Sender).Tag<>-1;
  YearPlanner1.CellData[m,d].CellHint:=TMenuItem(Sender).Caption;
  YearPlanner1.Invalidate;

End;
procedure TFrm_Nehag.SpinEdit_YearChange(Sender: TObject);
begin
  YearPlanner1.Year:=SpinEdit_Year.Value;
  LoadCalendarAndUpdateView;
end;

Function TFrm_Nehag.SugDayToColor(SugDay :LongInt):TColor;
Begin
  Case SugDay Of
    1:Result:=clRed; //�����
    2:Result:=clFuchsia;//�����
    3:Result:=clYellow;//�������
    4:Result:=clTeal;//��� ���
    5:Result:=clAqua;//�����
    6:Result:=clLime;//����� ������
    7:Result:=clPurple//����
  Else
    Result:=clOlive;
  End;//Case

End;

procedure TFrm_Nehag.Tbl_NehagBeforePost(DataSet: TDataSet);
begin
   if Trim(Tbl_Nehag.FieldByName('Kod_Nehag').AsString)='' Then
   Begin
     MessageDlg('��� ��� ���� ���� ���',mtError,[mbOk],0);
     Abort;
   End;

end;

procedure TFrm_Nehag.TBtn_ShowMapClick(Sender: TObject);
Var
  X,Y :LongInt;
  TmpNode:TNode;
begin
  With Tbl_Nehag Do
    MslFindNodeFromAddress(FieldByName('Ir_1').AsString,GetFieldFromSeparateString(FieldByName('Ktovet_1').AsString,MslSeparator,1),GetFieldFromSeparateString(FieldByName('Ktovet_1').AsString,MslSeparator,2),TmpNode);
  X:=TmpNode.Crd.X;
  Y:=TmpNode.Crd.Y;
  MslGotoXY(X,Y);
end;

end.

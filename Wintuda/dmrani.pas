{ 16/03/99 11:23:05 > [Aviran on AVIRAN] check in: (0.0)  / None }
unit DMWinTuda;

interface

uses
  SysUtils, Windows, Classes, Graphics, Controls,DBCtrls,AtmComp,
  Forms, Dialogs, DB, DBTables, AtmAdvTable, TProgress, AppEvent, RxQuery,
  DBFilter;

type
  TDM_WinTuda = class(TDataModule)
    DS_Azmn: TDataSource;
    Tbl_Azmn: TAtmTable;
    Tbl_Shib: TAtmTable;
    DS_Shib: TDataSource;
    DB_WinTuda: TDatabase;
    Tbl_Rehev: TTable;
    Tbl_Nehag: TTable;
    Tbl_ShibShibAzmnNo: TIntegerField;
    Tbl_ShibShibAzmnDate: TDateTimeField;
    Tbl_ShibShibKind: TIntegerField;
    Tbl_ShibShibNo: TAutoIncField;
    Tbl_ShibShibBeginTime: TDateTimeField;
    Tbl_ShibShibEndTime: TDateTimeField;
    Tbl_ShibHovalaKind: TIntegerField;
    Tbl_ShibPriceKind: TIntegerField;
    Tbl_ShibShibTotalKm: TIntegerField;
    Tbl_ShibLakNo1: TIntegerField;
    Tbl_ShibLakNo2: TIntegerField;
    Tbl_ShibLakNo3: TIntegerField;
    Tbl_ShibPriceIsGlobal: TIntegerField;
    Tbl_ShibMaslulCode1: TIntegerField;
    Tbl_ShibMaslul1: TStringField;
    Tbl_ShibYeMida1: TIntegerField;
    Tbl_ShibYeMida2: TIntegerField;
    Tbl_ShibYeMida3: TIntegerField;
    Tbl_ShibYeMida4: TIntegerField;
    Tbl_ShibPrice1: TBCDField;
    Tbl_ShibPrice2: TBCDField;
    Tbl_ShibPrice3: TBCDField;
    Tbl_ShibPrice4: TBCDField;
    Tbl_ShibShibRem2: TStringField;
    Tbl_ShibShibRem3: TStringField;
    Tbl_ShibShibRem4: TStringField;
    Tbl_ShibDriverNo1: TIntegerField;
    Tbl_ShibDriverNo2: TIntegerField;
    Tbl_ShibDriverName: TStringField;
    Tbl_ShibCarNo: TIntegerField;
    Tbl_ShibCarNum: TStringField;
    Tbl_ShibLakCodeBizua: TIntegerField;
    Tbl_ShibLakHesbonit: TIntegerField;
    Tbl_ShibLakHesbonitDate: TDateTimeField;
    Tbl_ShibDrishNo: TIntegerField;
    Tbl_ShibDrishDate: TDateTimeField;
    Tbl_ShibDriverCodeBizua: TIntegerField;
    Tbl_ShibDriverZikuiNo: TIntegerField;
    Tbl_ShibDriverZikuiDate: TDateTimeField;
    Tbl_ShibCarCodeBizua: TIntegerField;
    Tbl_ShibCarZikuiNo: TIntegerField;
    Tbl_ShibCarZikuiDate: TDateTimeField;
    Tbl_ShibNefh: TBCDField;
    Tbl_ShibTrvlNo: TIntegerField;
    Tbl_ShibStarMetanNo: TIntegerField;
    Tbl_ShibGpsStatus: TIntegerField;
    Tbl_ShibGpsNo: TIntegerField;
    Tbl_ShibSugMetan: TIntegerField;
    Tbl_ShibShibTuda: TIntegerField;
    Tbl_ShibShibCarKind: TIntegerField;
    Tbl_ShibDriverMsg: TSmallintField;
    Tbl_ShibAzmnOk: TSmallintField;
    Tbl_ShibTnuaOk: TSmallintField;
    Tbl_ShibHasbonitOk: TSmallintField;
    Tbl_ShibFirstSpido: TIntegerField;
    Tbl_ShibLastSpido: TIntegerField;
    Tbl_ShibTudaKind: TIntegerField;
    Tbl_ShibNegrrNo: TIntegerField;
    Tbl_ShibUpdateRecordDate: TDateTimeField;
    Tbl_ShibMaklidName: TStringField;
    Tbl_ShibShibAsmcta: TIntegerField;
    Tbl_ShibShibRem1: TMemoField;
    TBL_KODTAVLA: TAtmTable;
    Tbl_ShibYeMida2Look: TStringField;
    TBL_KODTAVLASug_Tavla: TIntegerField;
    TBL_KODTAVLAKod_Tavla: TIntegerField;
    TBL_KODTAVLATeur_tavla: TStringField;
    Tbl_ShibYeMida3Look: TStringField;
    Tbl_ShibLookShemMahzeva: TStringField;
    Tbl_ShibYeMida1Look: TStringField;
    Tbl_Maslul: TAtmTable;
    Tbl_ShibYeMida4Look: TStringField;
    Tbl_AzmnAzmnNo: TIntegerField;
    Tbl_AzmnAzmnDate: TDateTimeField;
    Tbl_AzmnInvLakNo: TIntegerField;
    Tbl_AzmnGetLakNo: TIntegerField;
    Tbl_AzmnAsmcta: TIntegerField;
    Tbl_AzmnTuda: TIntegerField;
    Tbl_AzmnFromDate: TDateTimeField;
    Tbl_AzmnToDate: TDateTimeField;
    Tbl_AzmnFromTime: TDateTimeField;
    Tbl_AzmnToTime: TDateTimeField;
    Tbl_AzmnAzmnType: TIntegerField;
    Tbl_AzmnRem2: TStringField;
    Tbl_AzmnRem3: TStringField;
    Tbl_AzmnRem4: TStringField;
    Tbl_AzmnTotalPrice1: TBCDField;
    Tbl_AzmnTotalPrice2: TBCDField;
    Tbl_AzmnTotalPrice3: TBCDField;
    Tbl_AzmnTotalKm: TIntegerField;
    Tbl_AzmnTotalZikuim: TBCDField;
    Tbl_AzmnSunday: TSmallintField;
    Tbl_AzmnMonday: TSmallintField;
    Tbl_AzmnTuesday: TSmallintField;
    Tbl_AzmnWednesday: TSmallintField;
    Tbl_AzmnThursday: TSmallintField;
    Tbl_AzmnFriday: TSmallintField;
    Tbl_AzmnSaturday: TSmallintField;
    Tbl_AzmnDeleteDate: TDateTimeField;
    Tbl_AzmnOldAzmnType: TIntegerField;
    Tbl_AzmnAzmnCarKind: TIntegerField;
    Tbl_AzmnMkbleName: TStringField;
    Tbl_AzmnAzmnQuntity: TBCDField;
    Tbl_AzmnInvName: TStringField;
    Tbl_AzmnDirshNo: TIntegerField;
    Tbl_AzmnAzmnMaslulCode1: TIntegerField;
    Tbl_AzmnAzmnMaslulCode2: TIntegerField;
    Tbl_AzmnAzmnMaslulCode3: TIntegerField;
    Tbl_AzmnAzmnMaslulCode4: TIntegerField;
    Tbl_AzmnAzmnMaslul1: TStringField;
    Tbl_AzmnAzmnMaslul2: TStringField;
    Tbl_AzmnAzmnMaslul3: TStringField;
    Tbl_AzmnAzmnMaslul4: TStringField;
    Tbl_Lako: TAtmTable;
    Tbl_AzmnLakName: TStringField;
    Tbl_ShibShemNehag2Look: TStringField;
    Tbl_Mehiron: TAtmTable;
    Tbl_MehironCODE_MEHIRON: TIntegerField;
    Tbl_MehironMEHIRON_LINK_KEY: TIntegerField;
    Tbl_MehironRECORD_NUM: TAutoIncField;
    Tbl_MehironSUG_MEHIRON: TIntegerField;
    Tbl_MehironSHEM_MEHIRON: TStringField;
    Tbl_MehironFROM_DATE: TDateTimeField;
    Tbl_MehironTO_DATE: TDateTimeField;
    Tbl_MehironSUG_LINK: TIntegerField;
    Tbl_MehironSUG_HAVARA_TNUA: TIntegerField;
    Tbl_MehironDISCOUNT_PERCENT: TBCDField;
    Tbl_MehironSUG_DISCOUNT: TIntegerField;
    Tbl_MehironSUG_HIUV: TIntegerField;
    Tbl_MehironSUG_SHERUT: TIntegerField;
    Tbl_MehironSTRING_1: TStringField;
    Tbl_MehironSTRING_2: TStringField;
    Tbl_MehironSTRING_3: TStringField;
    Tbl_MehironSTRING_4: TStringField;
    Tbl_MehironSTRING_5: TStringField;
    Tbl_MehironBCD_1: TBCDField;
    Tbl_MehironBCD_2: TBCDField;
    Tbl_MehironBCD_3: TBCDField;
    Tbl_MehironBCD_4: TBCDField;
    Tbl_MehironBCD_5: TBCDField;
    Tbl_MehironINTEGER_1: TIntegerField;
    Tbl_MehironINTEGER_2: TIntegerField;
    Tbl_MehironINTEGER_3: TIntegerField;
    Tbl_MehironINTEGER_4: TIntegerField;
    Tbl_MehironINTEGER_5: TIntegerField;
    Tbl_ShibPriceQuntity1: TBCDField;
    Tbl_ShibPriceQuntity2: TBCDField;
    Tbl_ShibPriceQuntity3: TBCDField;
    Tbl_ShibPriceQuntity4: TBCDField;
    Tbl_ShibQuntMusa: TBCDField;
    Tbl_ShibTotalHour: TBCDField;
    Tbl_ShibTotalPointsInWay: TIntegerField;
    Tbl_ShibShibFromPalce: TStringField;
    Tbl_ShibShibToPalce: TStringField;
    Tbl_ShibQuntity1: TBCDField;
    Tbl_ShibQuntity2: TBCDField;
    Tbl_ShibQuntity3: TBCDField;
    Tbl_ShibQuntity4: TBCDField;
    Tbl_ShibLookMerakezLak: TStringField;
    Tbl_ShibShibTotalTime: TDateTimeField;
    Tbl_ShibXBegin: TIntegerField;
    Tbl_ShibYBegin: TIntegerField;
    Tbl_ShibNodeNumBBegin: TIntegerField;
    Tbl_ShibNodeSugSystemBegin: TIntegerField;
    Tbl_ShibNodeKodCityNameBegin: TIntegerField;
    Tbl_ShibXEnd: TIntegerField;
    Tbl_ShibYEnd: TIntegerField;
    Tbl_ShibNodeNumBEnd: TIntegerField;
    Tbl_ShibNodeSugSystemEnd: TIntegerField;
    Tbl_ShibNodeKodCityNameEnd: TIntegerField;
    Tbl_AzmnAzmnSugMetan: TIntegerField;
    Tbl_AzmnFirstAzmnNo: TIntegerField;
    Tbl_AzmnFromPlace: TStringField;
    Tbl_AzmnToPlace: TStringField;
    Tbl_AzmnPratim: TStringField;
    Tbl_AzmnAzmnCarNo: TIntegerField;
    Tbl_AzmnAzmnDriverNo: TIntegerField;
    Tbl_AzmnAzmnCarNum: TStringField;
    Tbl_AzmnCALENDARKIND: TIntegerField;
    Tbl_AzmnXBegin: TIntegerField;
    Tbl_AzmnYBegin: TIntegerField;
    Tbl_AzmnNodeNumBBegin: TIntegerField;
    Tbl_AzmnNodeSugSystemBegin: TIntegerField;
    Tbl_AzmnNodeKodCityNameBegin: TIntegerField;
    Tbl_AzmnXEnd: TIntegerField;
    Tbl_AzmnYEnd: TIntegerField;
    Tbl_AzmnNodeNumBEnd: TIntegerField;
    Tbl_AzmnNodeSugSystemEnd: TIntegerField;
    Tbl_AzmnNodeKodCityNameEnd: TIntegerField;
    Tbl_AzmnCodeMhiron: TIntegerField;
    Tbl_AzmnSochen: TStringField;
    Tbl_AzmnOnia: TStringField;
    Tbl_AzmnPutPlace: TStringField;
    Tbl_AzmnFindPlace: TStringField;
    Tbl_AzmnDamaged: TSmallintField;
    Tbl_AzmnAddr: TStringField;
    Tbl_AzmnAddrNo: TIntegerField;
    Tbl_AzmnCity: TStringField;
    Tbl_AzmnPhone: TStringField;
    Tbl_ShibAmil: TStringField;
    Tbl_ShibGateTuda: TIntegerField;
    Tbl_ShibTik: TIntegerField;
    Tbl_ShibCont1: TStringField;
    Tbl_ShibCont2: TStringField;
    Tbl_ShibBoxes: TIntegerField;
    Tbl_ShibDamaged: TSmallintField;
    Tbl_ShibHoze: TIntegerField;
    Tbl_ShibWaitTime: TDateTimeField;
    Tbl_ShibUnLoadTime: TDateTimeField;
    Tbl_ShibTimeWinFrom: TDateTimeField;
    Tbl_ShibTimeWinTo: TDateTimeField;
    Tbl_ShibShemLakoach1Look: TStringField;
    Tbl_AzmnMitzar: TIntegerField;
    Tbl_AzmnRshimon: TIntegerField;
    Tbl_AzmnAflagaNo: TStringField;
    Tbl_AzmnAflagaPlace: TStringField;
    Tbl_ShibMrakz: TStringField;
    UpdateSQL_TabSheetBuild: TUpdateSQL;
    DS_TabSheetBuild: TDataSource;
    RxDBFilter_TabSheetBuild: TRxDBFilter;
    RxQuery_TabSheetBuild: TRxQuery;
    Tbl_AzmnQuntity1: TBCDField;
    Tbl_AzmnNefh: TBCDField;
    Tbl_AzmnTotalTime: TDateTimeField;
    Tbl_AzmnMishkal: TBCDField;
    Tbl_ShibMishkal: TBCDField;
    Tbl_ShibCodeMahzeva: TIntegerField;
    Tbl_ShibYehusYear: TIntegerField;
    Tbl_ShibYehusMonth: TIntegerField;
    Tbl_ShibTeurMitanLook1: TStringField;
    Tbl_AzmnLookAzmnType: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DM_WinTudaDestroy(Sender: TObject);
    procedure Tbl_ShibDriverNo1Validate(Sender: TField);
    procedure Tbl_ShibBeforePost(DataSet: TDataSet);
    procedure Tbl_AzmnAfterInsert(DataSet: TDataSet);
    procedure Tbl_ShibMaslulCode1Validate(Sender: TField);
    procedure Tbl_ShibYeMida1Validate(Sender: TField);
    procedure Tbl_ShibQuntity1Validate(Sender: TField);
    procedure Tbl_AzmnAzmnNoValidate(Sender: TField);
    procedure Tbl_AzmnFromDateValidate(Sender: TField);
    procedure Tbl_AzmnToDateValidate(Sender: TField);
    procedure Tbl_AzmnBeforeCancel(DataSet: TDataSet);
    procedure Tbl_ShibAfterPost(DataSet: TDataSet);
    procedure Tbl_ShibAfterInsert(DataSet: TDataSet);
    procedure Tbl_ShibPrice1Validate(Sender: TField);
    procedure Tbl_ShibQuntity2Validate(Sender: TField);
    procedure Tbl_ShibYeMida2Validate(Sender: TField);
    procedure DS_AzmnDataChange(Sender: TObject; Field: TField);
    procedure DS_ShibStateChange(Sender: TObject);
    procedure Tbl_ShibShibBeginTimeGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure Tbl_ShibShibBeginTimeSetText(Sender: TField;
      const Text: String);
    procedure Tbl_ShibFirstSpidoValidate(Sender: TField);
    procedure Tbl_ShibCarNumValidate(Sender: TField);
    procedure RxQuery_TabSheetBuildBeforeClose(DataSet: TDataSet);
    procedure Tbl_ShibShibBeginTimeValidate(Sender: TField);
    procedure DB_WinTudaLogin(Database: TDatabase; LoginParams: TStrings);
    procedure Tbl_AzmnAfterPost(DataSet: TDataSet);
    procedure Tbl_AzmnAfterScroll(DataSet: TDataSet);
    procedure Tbl_ShibValidateLookupField(Sender: TObject; AField: TField;
      var IsValid: Boolean);
    procedure Tbl_AzmnBeforePost(DataSet: TDataSet);
    procedure DS_AzmnStateChange(Sender: TObject);
    procedure Tbl_ShibShibAzmnDateValidate(Sender: TField);
    procedure Tbl_ShibBeforeDelete(DataSet: TDataSet);
    procedure Tbl_AzmnAzmnTypeValidate(Sender: TField);
  private
    { private declarations }
  public
    { public declarations }
    Procedure PutDefaultsOnShib;
    Procedure FindNextHazmanaNum;
    Procedure BuildLookupList;
    Procedure CalcShibTotalPrices;
    procedure PutMerakezDataInField(MerakezName:String;TheField :TField);
    Procedure CheckForRange;
    procedure FillAzmnDays;
  end;

var
  DM_WinTuda: TDM_WinTuda;

implementation
Uses F_GlblWinTuda, Frm_WinTuda,AtmRutin,AtmConst,F_Progress;
{$R *.DFM}

procedure TDM_WinTuda.DataModuleCreate(Sender: TObject);
var
   i:longint;
   StrH :String;
begin
{  Try
     StrH:=GetTempDir;
     if StrH[Length(StrH)]='\' Then
        StrH:=Copy(StrH,1,Length(StrH)-1);
     Session.PrivateDir:=StrH;
  Except
        //Ignore
  End;
 }
     WinTudaDbPassword:=RunDllStringFuncNoParam('Permit.dll','GetCurDbPassword');
     WinTudaDBUserName:=RunDllStringFuncNoParam('Permit.dll','GetCurDbUserName');
//     DB_WinTuda.LoginPrompt:=WinTudaDBUserName='';
     DB_WinTuda.Connected:=False;
     DB_WinTuda.LoginPrompt:=True;
     if CurAliasName<>'' Then
        DB_WinTuda.AliasName:=CurAliasName;
     Tbl_KodTavla.Open;
     Frm_Progress:=TFrm_Progress.Create(Nil);
     Try
         Frm_Progress.AtmGauge1.MaxValue:=0;
         For I:=0 To ComponentCount-1 Do
             if Components[I] is TTable Then
                Frm_Progress.AtmGauge1.MaxValue:=Frm_Progress.AtmGauge1.MaxValue+1;
         Frm_Progress.Caption:='���� ������';
         Frm_Progress.Show;

         BuildLookupList;

         For I:=0 To ComponentCount-1 Do
         Begin
              if Components[I] is TTable Then
              Begin
                   Frm_Progress.Label1.Caption:=(Components[I] As TTable).TableName;
                   Frm_Progress.AtmGauge1.AddProgress(1);
                   if Not (Components[I] As TTable).Active Then
                      (Components[I] As TTable).Open;
                   Application.ProcessMessages;
              End;
         End;
         CheckForRange;
     Finally
            Frm_Progress.Free;
            Frm_Progress:=Nil;
     End;
end;

Procedure TDM_WinTuda.CheckForRange;
Begin
     RangeAzmanFrom:=1;
     RangeAzmnTo:=9999999;
     With Frm_GlblWinTuda Do
     Begin
          Qry_Generic.SQL.Clear;
          Qry_Generic.Active:=False;
          Qry_Generic.SQL.Add('Select * From Range');
          Qry_Generic.SQL.Add('Where UserName='''+WinTudaCurUserName+'''');
          Qry_Generic.SQL.Add('And RangeType='''+rtAtmAzmn+'''');
          Qry_Generic.Active:=True;
          if Not Qry_Generic.Eof Then
          Begin
              RangeAzmanFrom:=Qry_Generic.FieldByName('FromValue').AsInteger;
              if Qry_Generic.FieldByName('ToValue').AsInteger>0 Then
                 RangeAzmnTo:=Qry_Generic.FieldByName('ToValue').AsInteger;
          End;
          Qry_Generic.Active:=False;
     End;
End;

procedure TDM_WinTuda.DM_WinTudaDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     DB_WinTuda.Connected:=False;
end;

procedure TDM_WinTuda.Tbl_ShibDriverNo1Validate(Sender: TField);
begin
     if (Tbl_Shib.State<>dsBrowse) And (Sender.AsString<>'') Then
     Begin
          Tbl_Nehag.IndexFieldNames:='Kod_Nehag';
          if Tbl_Nehag.FindKey([Sender.Value]) Then
          Begin
             Tbl_Shib.FieldByName('DriverName').AsString:=Tbl_Nehag.FieldByName('Shem_Nehag').AsString;
             if (Tbl_ShibCarNum.IsNull) Or (WinTudaAlwaysBringRehevFromNehag) Then
                Tbl_ShibCarNum.AsString:=Tbl_Nehag.FieldByName('Mis_Rishui').AsString;
          End
          Else
              Begin
                   ShowMessage('��� �� ���� ������');
                   Abort;
              End;
     End;

end;

procedure TDM_WinTuda.Tbl_ShibBeforePost(DataSet: TDataSet);
begin
     if (Tbl_Shib.State=dsEdit) And (Tbl_ShibLakCodeBizua.AsInteger<>0) Then
     Begin
          MessageDlg('��� ������ ����� ����� ����� ���� �������',mtInformation,[mbOk],0);
          Abort;
          Exit;
     End;
     PutDefaultsOnShib;
     TnuaWasInInsert:=DataSet.State=dsInsert;
     if InNewHazmana And TnuaWasInInsert Then
        WinTudaTotalForHazmana:=WinTudaTotalForHazmana+Tbl_ShibPriceQuntity1.AsFloat
     Else
         InNewHazmana:=False;
end;

procedure TDM_WinTuda.Tbl_AzmnAfterInsert(DataSet: TDataSet);
begin
     FindFirstControlInTabOrder(FrmWinTuda).SetFocus;
     Tbl_Shib.Insert;
     (DataSet As TAtmTable).SavePressed:=False;
     WinTudaTotalForHazmana:=0;
end;

procedure TDM_WinTuda.Tbl_ShibMaslulCode1Validate(Sender: TField);
begin
     if (Tbl_Shib.State<>dsBrowse) And (Sender.AsString<>'') Then
     Begin
//          Tbl_Maslul.IndexName:='';
          Tbl_Maslul.IndexFieldNames:=IndFields_Maslul_PrimaryKey;
          if Tbl_Maslul.FindKey([Sender.Value]) Then
          Begin
             Tbl_Shib.FieldByName('Maslul1').AsString:=Tbl_Maslul.FieldByName('Name').AsString;
             if (Tbl_ShibShibTotalKm.IsNull) And (Not Tbl_Maslul.FieldByName('Distance_Maslul').IsNull) Then
                Tbl_ShibShibTotalKm.AsInteger:=Tbl_Maslul.FieldByName('Distance_Maslul').AsInteger;
          End
          Else
              Begin
                   ShowMessage('����� �� ����');
                   Abort;
              End;

     End;
end;

procedure TDM_WinTuda.Tbl_ShibYeMida1Validate(Sender: TField);
Var
   I :LongInt;
begin
     For I:=2 To 4 Do
         if Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).AsString='' Then
            Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).Value:=Tbl_Shib.FieldByName('YeMida1').Value;
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_ShibQuntity1Validate(Sender: TField);
Var
   I :LongInt;
begin
     For I:=2 To 4 Do
//         if Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsString='' Then
            Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).Value:=Tbl_Shib.FieldByName('Quntity1').Value;
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_AzmnAzmnNoValidate(Sender: TField);
Var
   StrH :String;
begin
{     if (Sender.DataSet.State=dsEdit) And (Sender.AsString<>'') Then
     Begin
          StrH:=Sender.AsString;
          Sender.DataSet.Cancel;
          TTable(Sender.DataSet).IndexFieldNames:=IndFields_Azmn_PrimKey;
          TTable(Sender.DataSet).FindNearest([StrH]);
     End;
}
     if (Sender.DataSet.State=dsInsert) And (Sender.AsString<>'')Then
     Begin
          With Frm_GlblWinTuda.Qry_Generic Do
          Begin
               Active:=False;
               Sql.Clear;
               Sql.Add('Select Count(*) CR');
               Sql.Add('From '+Tbl_Azmn.TableName);
               Sql.Add('Where AzmnNo='+Sender.AsString);
               Active:=True;
               if (FieldByName('CR').AsInteger>0) Then
               Begin
                    Active:=False;
                    ShowMessage('���� ����� ����');
                    Abort;
               End;
               Active:=False;
          End;
     End;
end;

procedure TDM_WinTuda.Tbl_AzmnFromDateValidate(Sender: TField);
begin
     if (Sender.Value>Tbl_Azmn.FieldByName('ToDate').Value) Or (Tbl_Azmn.FieldByName('ToDate').IsNull) Then
     Begin
          Tbl_Azmn.FieldByName('ToDate').Value:=Sender.Value;
     End;
     if Tbl_Shib.State<>dsBrowse Then
        if Tbl_Azmn.FieldByName('ToDate').AsString='' Then
           Tbl_Azmn.FieldByName('ToDate').Value:=Sender.Value;
     FillAzmnDays;
end;

procedure TDM_WinTuda.Tbl_AzmnToDateValidate(Sender: TField);
begin
     if Sender.Value<Tbl_Azmn.FieldByName('FromDate').Value Then
     Begin
{          ShowMessage('����� ����� ��� ������ ����');
          Abort;
          Exit;
}         Tbl_Azmn.FieldByName('FromDate').Value:=Sender.Value;
     End;
     if Tbl_Shib.State<>dsBrowse Then
        if Tbl_Shib.FieldByName('ShibAzmnDate').AsString='' Then
           Tbl_Shib.FieldByName('ShibAzmnDate').Value:=Tbl_Azmn.FieldByName('FromDate').Value;
     FillAzmnDays;
end;

procedure TDM_WinTuda.Tbl_AzmnBeforeCancel(DataSet: TDataSet);
begin
     Tbl_Shib.Cancel;
end;

procedure TDM_WinTuda.Tbl_ShibAfterPost(DataSet: TDataSet);
Var
   AzmnLastDate :TDateTime;
   MyControl:TWinControl;
   StrH:String;
begin
     if Tbl_Azmn.State<>dsBrowse Then
     Begin
          Tbl_Azmn.SavePressed:=True;
          Tbl_Azmn.Post;
     End;

     if WinTudaAutoCalcSumTuda Then
     Begin
          if InNewHazmana Then
          Begin
               StrH:=Format('%f ��"� �����',[WinTudaTotalForHazmana]);
               FrmWinTuda.StatusBar1.Panels[PnlNum_TotalHazmana].Text:=StrH;
          End
          Else
              FrmWinTuda.Action_CalcTotalTeudaAzmnExecute(FrmWinTuda.Action_CalcTotalTeudaAzmn);
     End;
{     if TnuaWasInInsert Then
     Begin
          AzmnLastDate:=Tbl_Azmn.FieldByName('ToDate').Value;
          if Tbl_Shib.FieldByName('ShibAzmnDate').Value<AzmnLastDate Then
          Begin
               Tbl_Shib.Insert;
               Tbl_Shib.FieldByName('ShibAzmnDate').Value:=Tbl_Azmn.FieldByName('FromDate').Value+1;
               MyControl:=FindFirstControlInTabOrder(FrmWinTuda.PageControl_Tnua);
               if MyControl<>Nil Then
                  MyControl.SetFocus;
          End;
     End;
}
end;


Procedure TDM_WinTuda.PutDefaultsOnShib;
Begin
     With Tbl_Shib Do
     Begin
         if (Tbl_ShibShibAzmnDate.IsNull) And (Tbl_Shib.State<>dsBrowse) Then
            Tbl_ShibShibAzmnDate.AsDateTime:=DM_WinTuda.Tbl_AzmnFromDate.AsDateTime;
         FieldByName('ShibAzmnNo').Value:=Tbl_Azmn.FieldByName('AzmnNo').Value;
         if FieldByName('LakNo1').IsNull Then
            FieldByName('LakNo1').Value:=Tbl_Azmn.FieldByName('InvLakNo').Value;
         FieldByName('ShibAsmcta').Value:=Tbl_Azmn.FieldByName('Asmcta').Value;
         if FieldByName('ShibAzmnDate').AsString='' Then
            FieldByName('ShibAzmnDate').Value:=Tbl_Azmn.FieldByName('FromDate').Value;
         FieldByName('UpdateRecordDate').AsDateTime:=Now;
         FieldByName('MaklidName').AsString:=WinTudaCurUserName;
         if State=dsInsert Then
         Begin
              Tbl_ShibYehusYear.AsInteger:=WinTudaYehusYear;
              Tbl_ShibYehusMonth.AsInteger:=WinTudaYehusMonth;
              Tbl_ShibShibKind.AsInteger:=WinTudaShibKind;
         End;
         if Tbl_ShibShibKind.IsNull Then
            Tbl_ShibShibKind.AsInteger:=WinTudaShibKind;
         PutMerakezDataInField(Tbl_ShibLookMerakezLak.AsString,Tbl_ShibMrakz);
     End;

End;
procedure TDM_WinTuda.Tbl_ShibAfterInsert(DataSet: TDataSet);
begin
     if FrmWinTuda.PageControl_Tnua.ActivePage.PageIndex>FrmWinTuda.TS_Perut2.PageIndex Then
        FrmWinTuda.PageControl_Tnua.ActivePage:=FrmWinTuda.TS_Tnua1;
     if Tbl_Azmn.State<>dsInsert Then FrmWinTuda.CopyMidPnlFields;
end;

Procedure TDM_WinTuda.FindNextHazmanaNum;
Var
   HN :LongInt;
Begin
     if Tbl_Azmn.State<>dsBrowse Then
     Begin
          With Frm_GlblWinTuda.Qry_Generic Do
          Begin
               Active:=False;
               Sql.Clear;
               Sql.Add('Select Max(AzmnNo) HazNum');
               Sql.Add('From '+Tbl_Azmn.TableName);
               Sql.Add('Where AzmnNo Between '+IntToStr(RangeAzmanFrom)+' and '+IntToStr(RangeAzmnTo));
               Active:=True;
               if (FieldByName('HazNum').AsInteger>=RangeAzmanFrom) And (FieldByName('HazNum').AsInteger<RangeAzmnTo) Then
                   HN:=(FieldByName('HazNum').AsInteger+1)
               Else
                 if (FieldByName('HazNum').AsInteger<RangeAzmanFrom) Then
                    HN:=RangeAzmanFrom
                 Else
                    if (FieldByName('HazNum').AsInteger>=RangeAzmnTo) Then
                    Begin
                        MessageDlg('���� �� ���� ������� ������ �� '+IntToStr(RangeAzmnTo),mtError,[mbOk],0);
                        Tbl_Azmn.Cancel;
                        Active:=False;
                        Exit;
                    End;
               Tbl_Azmn.FieldByName('AzmnNo').AsInteger:=HN;
               Active:=False;
          End;
          SetAtmDbEditF4State(FrmWinTuda.Panel_Top);
       End;
End;

Procedure TDM_WinTuda.BuildLookupList;
Var
   SList:TStringList;
Begin
  Try
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida1Look);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida2Look);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida3Look);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida4Look);
     SList.AddObject(IntToStr(SugTavla_SugMitan),Tbl_ShibTeurMitanLook1);
     FillKodTavlaLookupList(Tbl_KodTavla,SList);

  Finally
     SList.Free;
  End;

  //������
  With Frm_GlblWinTuda.Qry_Generic Do
  Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Select CodeMahzeva,NameMahzeva From Mahzeva');
      Active:=True;
      Tbl_ShibLookShemMahzeva.LookupList.Clear;
      First;
      While Not Eof Do
      Begin
          Tbl_ShibLookShemMahzeva.LookupList.Add(FieldByName('CodeMahzeva').Value,FieldByName('NameMahzeva').Value);
          Next;
      End;
  End;

  Tbl_AzmnLookAzmnType.LookupList.Clear;
  Tbl_AzmnLookAzmnType.LookupList.Add(0,'�����');
  Tbl_AzmnLookAzmnType.LookupList.Add(1,'������ ������');
  Tbl_AzmnLookAzmnType.LookupList.Add(2,'�� ���� ����');
  Tbl_AzmnLookAzmnType.LookupList.Add(3,'�� ���� �����');
  Tbl_AzmnLookAzmnType.LookupList.Add(4,'�����');
End;

procedure TDM_WinTuda.Tbl_ShibPrice1Validate(Sender: TField);
Begin
     CalcShibTotalPrices;
end;

Procedure TDM_WinTuda.CalcShibTotalPrices;
Var
   I :LongInt;
begin
     For I:=1 To 4 Do
         if (Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsString<>'') And
            (Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsString<>'') Then

            if (Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).AsString<>'0') And (Tbl_Shib.FieldByName('PriceIsGlobal').AsInteger=0{<>1}) Then
                 Tbl_Shib.FieldByName('PriceQuntity'+IntToStr(I)).Value:=(Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsFloat)*
                     (Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsFloat)
            Else
                 Tbl_Shib.FieldByName('PriceQuntity'+IntToStr(I)).Value:=(Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsFloat);
End;
procedure TDM_WinTuda.Tbl_ShibQuntity2Validate(Sender: TField);
begin
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_ShibYeMida2Validate(Sender: TField);
begin
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.PutMerakezDataInField(MerakezName:String;TheField :TField);
Var
   I,NumOfFields :LongInt;
   FieldName,StrH :String;
   FieldBuffer:String;
Begin
     FieldName:='';
     FieldBuffer:='';
     if MerakezName='' Then Exit;
     NumOfFields:=CountFieldsInSepString(MerakezName,',');
     Try
        For I:=1 To NumOfFields Do
        Begin
           FieldName:=GetFieldFromSeparateString(MerakezName,',',I);
           StrH:=Tbl_Shib.FieldByName(FieldName).AsString;
           if Length(StrH)> 10 Then
              StrH:=Copy(StrH,1,10);
           While Length(StrH)<10 Do
                 StrH:='0'+StrH;
           FieldBuffer:=FieldBuffer+StrH;

        End;
        if Tbl_Shib.State=dsBrowse Then
           Tbl_Shib.Edit;
        TheField.AsString:=FieldBuffer;
     Except On e:Exception Do
          ShowMessage('�� ���� ������ ����'+
                      #13+E.Message);
     End;
End;

procedure TDM_WinTuda.DS_AzmnDataChange(Sender: TObject; Field: TField);
begin
     if (FrmWinTuda<>Nil) Then
     Begin
        if (Tbl_Azmn.State=dsBrowse) Then
         Begin
            FrmWinTuda.MaskEdit_FromTime.Text:=Copy(GetFieldFromSeparateString(Tbl_AzmnFromDate.AsString,' ',2),1,5);
            FrmWinTuda.MaskEdit_ToTime.Text:=Copy(TimeToStr(Tbl_AzmnToDate.AsDateTime),1,5);
         End
     End;
end;

procedure TDM_WinTuda.DS_ShibStateChange(Sender: TObject);
begin
     Try
        if FrmWinTuda<>Nil Then
        Begin
           FrmWinTuda.StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
        End;
     Except
     End;
end;

procedure TDM_WinTuda.Tbl_ShibShibBeginTimeGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
     Text := FormatDateTime('hh:nn',Sender.AsDateTime);
end;

procedure TDM_WinTuda.Tbl_ShibShibBeginTimeSetText(Sender: TField;
  const Text: String);
begin
     Sender.AsDateTime := Trunc (Tbl_ShibShibAzmnDate.AsDateTime){Date} + Frac (StrToTime(Text));
end;

procedure TDM_WinTuda.Tbl_ShibFirstSpidoValidate(Sender: TField);
begin
     if (Not Tbl_ShibFirstSpido.IsNull) And (Tbl_ShibFirstSpido.AsInteger>0)
        And (Not Tbl_ShibLastSpido.IsNull) And (Tbl_ShibLastSpido.AsInteger>0) Then
     Begin
         if (Tbl_ShibFirstSpido.AsInteger>Tbl_ShibLastSpido.AsInteger) Then
         Begin
              MessageDlg('�������� ����� ���� ��������� ���',mtError,[mbOk],0);
              Abort;
         End
         Else
            Tbl_ShibShibTotalKm.AsInteger:=Tbl_ShibLastSpido.AsInteger-Tbl_ShibFirstSpido.AsInteger;
     End;

end;

procedure TDM_WinTuda.Tbl_ShibCarNumValidate(Sender: TField);
begin
     if Sender.AsString<>'' Then
     Begin
         Tbl_Rehev.IndexFieldNames:=IndFields_Rehev_PrimaryKey;
         if Tbl_Rehev.FindKey([Sender.Value]) Then
         Begin
            if Tbl_ShibDriverNo1.IsNull Then
               Tbl_Shib.FieldByName('DriverNo1').AsString:=Tbl_Rehev.FieldByName('Kod_Nehag').AsString;
         End
         Else
             Begin
                  ShowMessage('��� �� ���� ������');
                  Abort;
             End;
     End;
end;

procedure TDM_WinTuda.RxQuery_TabSheetBuildBeforeClose(DataSet: TDataSet);
begin
     RxQuery_TabSheetBuild.ApplyUpdates;
end;

procedure TDM_WinTuda.Tbl_ShibShibBeginTimeValidate(Sender: TField);
Var
   TimeH :TDateTime;
   StrH :String;
begin
    if (Not Tbl_ShibShibBeginTime.IsNull) And (Not Tbl_ShibShibEndTime.IsNull) Then
    Begin
         if Tbl_ShibShibBeginTime.AsDateTime>Tbl_ShibShibEndTime.AsDateTime Then
         Begin
              MessageDlg('��� ����� ����� ���� ����',mtError,[mbOk],0);
              Abort;
         End
         Else
         Begin
             TimeH:=Tbl_ShibShibEndTime.AsDateTime-Tbl_ShibShibBeginTime.AsDateTime;
             StrH:=TimeToStr(TimeH);
             Tbl_ShibTotalHour.AsFloat:=StrToInt(GetFieldFromSeparateString(StrH,':',1))+
                                        (StrToInt(GetFieldFromSeparateString(StrH,':',2))/60);
         End;
    End;

end;

procedure TDM_WinTuda.DB_WinTudaLogin(Database: TDatabase;
  LoginParams: TStrings);
begin
    LoginParams.Clear;
    LoginParams.Add('USER NAME='+WinTudaDBUserName);
    LoginParams.Add('PASSWORD='+WinTudaDBPassword);
end;

procedure TDM_WinTuda.Tbl_AzmnAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(FrmWinTuda);
end;

procedure TDM_WinTuda.Tbl_AzmnAfterScroll(DataSet: TDataSet);
begin
    InNewHazmana:=False;
end;

procedure TDM_WinTuda.Tbl_ShibValidateLookupField(Sender: TObject;
  AField: TField; var IsValid: Boolean);
begin
    if AField=Tbl_ShibLookMerakezLak Then
       IsValid:=True;
end;

procedure TDM_WinTuda.Tbl_AzmnBeforePost(DataSet: TDataSet);
begin
     if Tbl_Azmn.State=dsInsert Then
        FindNextHazmanaNum;
end;

procedure TDM_WinTuda.DS_AzmnStateChange(Sender: TObject);
begin
      if (Tbl_Azmn.State=dsInsert) Then
      Begin
           FrmWinTuda.MaskEdit_FromTime.Clear;
           FrmWinTuda.MaskEdit_ToTime.Clear;
           InNewHazmana := True;
      End;

     Try
        if FrmWinTuda<>Nil Then
        Begin
           FrmWinTuda.StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
           if Tbl_Azmn.State=dsBrowse Then
              SetAtmDbEditF4State(FrmWinTuda);
        End;
     Except
     End;
end;

procedure TDM_WinTuda.Tbl_ShibShibAzmnDateValidate(Sender: TField);
Var
   Day,Month,Year :Word;
begin
     DecodeDate(Sender.AsDateTime,Year,Month,Day);
     if (Year<>WinTudaYehusYear) or (Month<>WinTudaYehusMonth) Then
        if MessageDlg('����� ���� ����� ����� ��� ������',mtConfirmation,[mbYes,mbNo],0)=mrNo Then
           Abort;
end;

procedure TDM_WinTuda.Tbl_ShibBeforeDelete(DataSet: TDataSet);
begin
     if (Tbl_ShibLakCodeBizua.AsInteger<>0) Then
     Begin
          MessageDlg('��� ������ ����� ����� ����� ���� �������',mtInformation,[mbOk],0);
          Abort;
          Exit;
     End;
end;

procedure TDM_WinTuda.FillAzmnDays;
Var
   CurDate :TDateTime;
   InitValue :Integer;
Begin
     if Tbl_azmnAzmnType.AsInteger=atRavFix Then
        InitValue:=1
     Else
         InitValue:=0;
     Tbl_AzmnSunday.AsInteger:=InitValue;
     Tbl_AzmnMonday.AsInteger:=InitValue;
     Tbl_AzmnTuesday.AsInteger:=InitValue;
     Tbl_AzmnWednesday.AsInteger:=InitValue;
     Tbl_AzmnThursday.AsInteger:=InitValue;
     Tbl_AzmnFriday.AsInteger:=InitValue;
     Tbl_AzmnSaturday.AsInteger:=InitValue;

     CurDate:=Tbl_AzmnFromDate.AsDateTime;
     While (InitValue=0) And (CurDate<=Tbl_AzmnTodate.AsDateTime) Do
     Begin
          Case DayOfWeek(CurDate) of
               1:Tbl_AzmnSunday.AsInteger:=1;
               2:Tbl_AzmnMonday.AsInteger:=1;
               3:Tbl_AzmnTuesday.AsInteger:=1;
               4:Tbl_AzmnWednesday.AsInteger:=1;
               5:Tbl_AzmnThursday.AsInteger:=1;
               6:Tbl_AzmnFriday.AsInteger:=1;
               7:Tbl_AzmnSaturday.AsInteger:=1;
          End; //Case
          CurDate:=CurDate+1;
     End;
End;
procedure TDM_WinTuda.Tbl_AzmnAzmnTypeValidate(Sender: TField);
begin
     if (Not Tbl_AzmnFromDate.IsNull) And (Not Tbl_AzmnToDate.IsNull) Then
        FillAzmnDays;
end;

end.

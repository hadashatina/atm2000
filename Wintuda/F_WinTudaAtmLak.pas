
unit F_WinTudaAtmLak;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, RxQuery, AtmRxQuery, ComCtrls, ExtCtrls, DBCtrls, ToolWin,
  StdCtrls, Mask, AtmComp, ToolEdit, DBFilter, AtmTabSheetBuild, ImgList,
  ActnList, Grids, DBGrids, RXDBCtrl, AtmLookCombo,IniFiles, dxfClock,
  dxfTimer, dbnav797;

type
  TFrm_AtmShibLak = class(TForm)
    DS_AtmShibLak: TDataSource;
    UpdateSQL_AtmShibLak: TUpdateSQL;
    Qry_AtmShibLak: TAtmRxQuery;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    DBNavigator1: TAtmDbNavigator;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    Act_Search: TAction;
    ImageList_Buttons: TImageList;
    AtmDbHEdit2: TAtmDbHEdit;
    Label2: TLabel;
    Label3: TLabel;
    AtmDbHEdit_CodeSherut: TAtmDbHEdit;
    AtmDbHEdit4: TAtmDbHEdit;
    Lbl_Remark: TLabel;
    AtmDbHMemo_Remark: TAtmDbHMemo;
    PageControl1: TPageControl;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery_TSB: TRxQuery;
    DS_TSB: TDataSource;
    RxDBFilter_TSB: TRxDBFilter;
    Panel1: TPanel;
    ComboEdit_LakNo: TComboEdit;
    Label1: TLabel;
    Label5: TLabel;
    ComboEdit_CodeMadrich: TComboEdit;
    Lbl_ShemMadrich: TLabel;
    Lbl_LakName: TLabel;
    Qry_AtmShibLakShibAzmnDate: TDateTimeField;
    Qry_AtmShibLakShibKind: TIntegerField;
    Qry_AtmShibLakShibNo: TIntegerField;
    Qry_AtmShibLakLakNo1: TIntegerField;
    Qry_AtmShibLakMaslulCode1: TIntegerField;
    Qry_AtmShibLakMaslul1: TStringField;
    Qry_AtmShibLakDriverNo1: TIntegerField;
    Qry_AtmShibLakShibAzmnNo: TIntegerField;
    Qry_AtmShibLakMaklidName: TStringField;
    Qry_AtmShibLakDriverName: TStringField;
    Qry_AtmShibLakUpdateRecordDate: TDateTimeField;
    DBRadioGroup1: TDBRadioGroup;
    Qry_AtmShibLakLakCodeBizua: TIntegerField;
    ImageList_TSB: TImageList;
    Qry_AtmShibLakShibRem1: TMemoField;
    DBText_ShibNo: TDBText;
    Panel_Sherut: TPanel;
    Lbl_PhoneLak: TLabel;
    Qry_AtmShibLakShibRem2: TStringField;
    AtmDbHEdit1: TAtmDbHEdit;
    Label6: TLabel;
    Qry_AtmShibLakShibEndTime: TDateTimeField;
    Qry_AtmShibLakDriverNo2: TIntegerField;
    Label7: TLabel;
    AtmDbHEdit3: TAtmDbHEdit;
    AtmDbComboBox_Software: TAtmDbComboBox;
    Qry_AtmShibLakHovalaKind: TIntegerField;
    Label4: TLabel;
    AtmDbHEdit5: TAtmDbHEdit;
    Qry_AtmShibLakTotalHour: TBCDField;
    Label8: TLabel;
    AtmDbHEdit6: TAtmDbHEdit;
    DBText1: TDBText;
    Qry_AtmShibLakLookShemMetapel: TStringField;
    Label9: TLabel;
    DBText2: TDBText;
    Label10: TLabel;
    AtmDbHEdit7: TAtmDbHEdit;
    Qry_AtmShibLakShibRem3: TStringField;
    CheckBox_ReadOnly: TCheckBox;
    dxfTimer1: TdxfTimer;
    Lbl_RemarkFromLak: TLabel;
    Timer1: TTimer;
    ToolButton2: TToolButton;
    Act_Print: TAction;
    procedure ComboEdit_LakNoButtonClick(Sender: TObject);
    procedure ComboEdit_CodeMadrichButtonClick(Sender: TObject);
    procedure ComboEdit_CodeMadrichChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure Qry_AtmShibLakAfterOpen(DataSet: TDataSet);
    procedure Qry_AtmShibLakAfterInsert(DataSet: TDataSet);
    procedure Qry_AtmShibLakMaslulCode1Validate(Sender: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AtmTabSheetBuild1GridDblClick(Sender: TObject;
      ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
    procedure Qry_AtmShibLakBeforePost(DataSet: TDataSet);
    procedure Label1DblClick(Sender: TObject);
    procedure Qry_AtmShibLakLakCodeBizuaValidate(Sender: TField);
    procedure FormShow(Sender: TObject);
    procedure ComboEdit_LakNoExit(Sender: TObject);
    procedure Qry_AtmShibLakAfterPost(DataSet: TDataSet);
    procedure Qry_AtmShibLakDriverNo1Validate(Sender: TField);
    procedure DS_AtmShibLakStateChange(Sender: TObject);
    procedure Qry_AtmShibLakAfterCancel(DataSet: TDataSet);
    procedure dxfTimer1DblClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBNavigator1BeforeAction(Sender: TObject;
      Button: TmoNavBtns);
    procedure Act_PrintExecute(Sender: TObject);
    procedure Qry_AtmShibLakAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    StartTime :TDatetime;
    LogTimeStarted :Boolean;
    Procedure ShowStatusLakoach;
    procedure BuildLookup;
    Function CheckActiveTnuaToLak(LakNo :String):LongInt;
  public
    { Public declarations }
  end;

var
  Frm_AtmShibLak: TFrm_AtmShibLak;

implementation

uses DMWinTuda, F_GlblWinTuda, Frm_WinTuda, AtmRutin, AtmConst,
  RptAtmShibLak;

{$R *.DFM}

procedure TFrm_AtmShibLak.ComboEdit_LakNoButtonClick(Sender: TObject);
begin
  if Frm_GlblWinTuda.AtmAdvSearch_Lako.Execute Then
  Begin
    TComboEdit(Sender).Text:=Frm_GlblWinTuda.AtmAdvSearch_Lako.ReturnString;
    SelectNext(TWinControl(Sender),True,True);
  End;
end;

procedure TFrm_AtmShibLak.ComboEdit_CodeMadrichButtonClick(Sender: TObject);
begin
  if Frm_GlblWinTuda.AtmAdvSearch_Nehag.Execute Then
    TComboEdit(Sender).Text:=Frm_GlblWinTuda.AtmAdvSearch_Nehag.ReturnString;
end;

procedure TFrm_AtmShibLak.ComboEdit_CodeMadrichChange(Sender: TObject);
begin
  if DM_winTuda.Tbl_Nehag.FindKey([TComboEdit(Sender).Text]) Then
    Lbl_ShemMadrich.Caption:=DM_winTuda.Tbl_Nehag.FieldByName('Shem_Nehag').AsString;
end;

procedure TFrm_AtmShibLak.FormCreate(Sender: TObject);
Var
  F :TIniFile;
begin
  if Frm_GlblWinTuda=Nil Then
    Frm_GlblWinTuda:=TFrm_GlblWinTuda.Create(Self);
  if DM_WinTuda=Nil Then
    DM_WinTuda:=TDM_WinTuda.Create(Nil);
  AtmTabSheetBuild1.ScriptsDir:=DirectoryForScripts;
  AtmTabSheetBuild1.BuildTabsForGrids;

  F:=TIniFile.Create(WinTudaPrivateIniFileName);
  ComboEdit_CodeMadrich.Text:=F.ReadString(UserSectionForMain,KeyUserCode,'');
  F.Free;
  if ComboEdit_CodeMadrich.Text='' Then
    ComboEdit_CodeMadrichButtonClick(ComboEdit_CodeMadrich);

  BuildLookup;
end;

procedure TFrm_AtmShibLak.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Frm_AtmShibLak:=Nil;
  Action:=caFree;
end;

procedure TFrm_AtmShibLak.FormDestroy(Sender: TObject);
Var
  I :longInt;
begin
  For I:=0 To ComponentCount-1 Do
    if Components[i] is TDataset Then
      TDataset(Components[i]).Close;

  if FrmWinTuda=Nil Then
  Begin
    DM_WinTuda.Free;
    DM_WinTuda:=Nil;
    Frm_GlblWinTuda.Free;
    Frm_GlblWinTuda:=Nil;
  End;
end;

procedure TFrm_AtmShibLak.AtmTabSheetBuild1BeforeExecuteQuery(
  Sender: TObject);
begin
  if RxQuery_TSB.Params.FindParam('PLakNo')<>Nil Then
    RxQuery_TSB.ParamByName('PLaKNo').AsString:=ComboEdit_LakNo.Text;
  if RxQuery_TSB.Params.FindParam('PCodeNehag')<>Nil Then
    RxQuery_TSB.ParamByName('PCodeNehag').AsString:=ComboEdit_CodeMadrich.Text;
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakAfterOpen(DataSet: TDataSet);
begin
  if (CompareText(AtmTabSheetBuild1.CurID,'LakAllTnuot')<>0) Then
    if (AtmTabSheetBuild1.Query.Active) Then
      AtmTabSheetBuild1.RefreshQry(Nil)
    Else
      AtmTabSheetBuild1.RunQuery(AtmTabSheetBuild1.CurSortValue);
  LogTimeStarted :=False;
  dxfTimer1.Stop;
  dxfTimer1.Reset;
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakAfterInsert(DataSet: TDataSet);
begin
  AtmDbComboBox_Software.SetFocus;
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakMaslulCode1Validate(
  Sender: TField);
begin
  With DM_WinTuda.RxQuery_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Name From Maslul Where CODE_MASLUL='+Sender.AsString);
    Open;
    if Not (Eof And BOF) Then
      Qry_AtmShibLak.FieldByName('Maslul1').AsString:=FieldByName('NAME').AsString;
    CLose;
  End;
end;

procedure TFrm_AtmShibLak.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyboardManageForTableAction(ActiveControl,Key,Shift,DBNavigator1.DataSource.Dataset);
end;

procedure TFrm_AtmShibLak.AtmTabSheetBuild1GridDblClick(Sender: TObject;
  ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
begin
{  if (CompareText(AtmTabSheetBuild1.CurID,'LakHistory')=0) Or
     (CompareText(AtmTabSheetBuild1.CurID,'LakOpenTnuot')=0) Or
     (CompareText(AtmTabSheetBuild1.CurID,'LakAllTnuot')=0)Then}
  Begin
    if (Qry_AtmShibLak.State In [dsInsert,dsEdit]) Then
      if (Qry_AtmShibLak.FieldByName('MaslulCode1').IsNull) Then
        Qry_AtmShibLak.Cancel
      Else
        Qry_AtmShibLak.Post;
    Qry_AtmShibLak.Close;
    Qry_AtmShibLak.MacroByName('MWhereIndex').AsString:='LakNo1='+AtmTabSheetBuild1.Query.FieldByName('LakNo1').AsString+' And ShibNo='+AtmTabSheetBuild1.Query.FieldByName('ShibNo').AsString;
    Qry_AtmShibLak.Open;
    if (Qry_AtmShibLak.FieldByName('LakCodeBizua').AsInteger=0) And (Not CheckBox_ReadOnly.Checked) Then
    Begin
      Qry_AtmShibLak.Edit;
      Qry_AtmShibLak.FieldByName('LakCodeBizua').AsInteger:=2;
      Qry_AtmShibLak.AfterPost:=Nil;
      Qry_AtmShibLak.Post;
      Qry_AtmShibLak.AfterPost:=Qry_AtmShibLakAfterPost;
      if (CompareText(AtmTabSheetBuild1.CurID,'LakAllTnuot')<>0)Then
        AtmTabSheetBuild1.RefreshQry(Nil);
    End;
    if Qry_AtmShibLak.FieldByName('LakCodeBizua').AsInteger=2 Then
    Begin
      StartTime :=Now;
      LogTimeStarted :=True;
      dxfTimer1.Reset;
      dxfTimer1.Start;
    End;
  End;
  if (CompareText(AtmTabSheetBuild1.CurID,'LakOpenTnuot')=0) Or
     (CompareText(AtmTabSheetBuild1.CurID,'LakAllTnuot')=0) Or
     (CompareText(AtmTabSheetBuild1.CurID,'MadrichHistory')=0) Then
  Begin
    ComboEdit_LakNo.Text:=Qry_AtmShibLak.FieldByName('LakNo1').AsString;
    if ComboEdit_LakNo.Text<>'' Then
      if DM_winTuda.Tbl_Lako.FindKey([ComboEdit_LakNo.Text]) Then
      Begin
        Lbl_LakName.Caption:=DM_winTuda.Tbl_Lako.FieldByName('Shem_Lakoach').AsString;
        Lbl_PhoneLak.Caption:=DM_winTuda.Tbl_Lako.FieldByName('Tel_1').AsString;
        ShowStatusLakoach;
      End;
  End;
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakBeforePost(DataSet: TDataSet);
Var
  IntH :LongInt;
  Success :Boolean;
  StrH :STring;
begin
{  if Qry_AtmShibLak.State=dsEdit Then
    if Qry_AtmShibLak.FieldByName('LakCodeBizua').AsInteger<>1 Then
      if MessageDlg('������ ����� ����� ��� ������',mtConfirmation,[mbYes,mbNo],0)=mrNo Then
      Begin
        Abort;
        Exit;
      End;}
  dxfTimer1.Stop;
  With Qry_AtmShibLak Do
  Begin
    FieldByName('UpdateRecordDate').AsDateTime:=Now;
    FieldByName('MaklidName').AsString:=WinTudaCurUserName;
    FieldByName('ShibKind').AsInteger:=4; //�����
    FieldByName('ShibAzmnNo').AsString:=ComboEdit_CodeMadrich.Text;
    if (FieldByName('DriverNo1').IsNull) Or (State=dsEdit) Then
      FieldByName('DriverNo1').AsString:=ComboEdit_CodeMadrich.Text;
    if State=dsInsert Then
    Begin
      FieldByName('LakNo1').AsString:=ComboEdit_LakNo.Text;
      FieldByName('DriverNo2').AsString:=ComboEdit_CodeMadrich.Text;
    End;
    if (FieldByName('TotalHour').IsNull) And (Not FieldByName('ShibEndTime').IsNull) And (Not FieldByName('ShibAzmnDate').IsNull)Then
      FieldByName('TotalHour').AsInteger:=TimeToMinutes((FieldByName('ShibEndTime').AsDateTime-FieldByName('ShibAzmnDate').AsDateTime))
    Else
      if LogTimeStarted Then
      Begin
        IntH:=TimeToMinutes(Now-StartTime);
        StrH:=IntToStr(IntH);
        Try
          if InputQuery('��� �����','��� ��� ����� �����',StrH) Then
            FieldByName('TotalHour').AsInteger:=FieldByName('TotalHour').AsInteger+StrToInt(StrH);
        Except
        End;
      End;
    LogTimeStarted :=False;

  End;
end;

Procedure TFrm_AtmShibLak.ShowStatusLakoach;
Var
  StrH :String;
Begin
  With DM_WinTuda Do
  Begin
    if Tbl_Lako.FieldByName('Sug_Madad_2').AsInteger = 1 Then
      StrH:=' (����� ����) '
    Else
      StrH:='';
    if (Not Tbl_Lako.FieldByName('Taarich_Siyum').IsNull) And
       ((Tbl_Lako.FieldByName('Taarich_Siyum').AsDateTime-Now)<-45{��� ���� ��� ���� ���� 45}) Then
    Begin
      Panel_Sherut.Color:=clRed;
      Panel_Sherut.Caption:='���� �� �����'+StrH;
    End
    Else
    if (Not Tbl_Lako.FieldByName('Sug_Madad_3').IsNull) And
       (Tbl_Lako.FieldByName('Sug_Madad_3').AsInteger=0) Then
    Begin
      Panel_Sherut.Color:=clYellow;
      Panel_Sherut.Caption:='���� �� ����'+StrH;
    End
    Else
    Begin
      Panel_Sherut.Color:=clLime;
      Panel_Sherut.Caption:='���� �����'+StrH;
    End;

    Lbl_RemarkFromLak.Caption:=Tbl_Lako.FieldByName('String_1').AsString;
  End;
End;
procedure TFrm_AtmShibLak.Label1DblClick(Sender: TObject);
begin
  OpenCrtDll(fnLakoach,ComboEdit_LakNo.Text);
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakLakCodeBizuaValidate(
  Sender: TField);
begin
  if Sender.AsInteger=1 Then
    Qry_AtmShibLak.FieldByName('ShibEndTime').AsDateTime:=Now;
end;

procedure TFrm_AtmShibLak.FormShow(Sender: TObject);
begin
  AtmTabSheetBuild1.InitCurTab(AtmTabSheetBuild1.PageControl.ActivePage);
  Timer1Timer(Nil);
end;

procedure TFrm_AtmShibLak.BuildLookup;
Var
  SList :TStringList;
Begin
  SList:=TStringList.Create;
  SList.AddObject(IntToStr(Sug_Tavla_SugSoftware),AtmDbComboBox_Software);
  FillKodTavlaComboList(DM_WinTuda.TBL_KODTAVLA,SList);
  SList.Free;
End;
procedure TFrm_AtmShibLak.ComboEdit_LakNoExit(Sender: TObject);
Var
  IntH :LongInt;
begin
  if TComboEdit(Sender).Text='' Then
    Exit;
  if DM_winTuda.Tbl_Lako.FindKey([TComboEdit(Sender).Text]) Then
  Begin
    Lbl_LakName.Caption:=DM_winTuda.Tbl_Lako.FieldByName('Shem_Lakoach').AsString;
    Lbl_PhoneLak.Caption:=DM_winTuda.Tbl_Lako.FieldByName('Tel_1').AsString;

{    Qry_AtmShibLak.Close;
//    Qry_AtmShibLak.MacroByName('MWhereIndex').AsString:='LakNo1='+TComboEdit(Sender).Text;
    Qry_AtmShibLak.Open;}
    Qry_AtmShibLak.Last;
    IntH:=CheckActiveTnuaToLak(TComboEdit(Sender).Text);
    if IntH=0 Then
    Begin
      Qry_AtmShibLak.Append;
      Qry_AtmShibLak.FieldByName('ShibAzmnDate').AsDateTime:=Now;
    End
    Else
      Qry_AtmShibLak.FindKey([IntH]);
    ShowStatusLakoach;
  End
  Else
    MessageDlg('���� �� ����',mtError,[mbOk],0);
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakAfterPost(DataSet: TDataSet);
begin
//  ComboEdit_LakNo.SetFocus;
  if (CompareText(AtmTabSheetBuild1.CurID,'LakOpenTnuot')=0) Then
    AtmTabSheetBuild1.RefreshQry(Nil);
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakDriverNo1Validate(Sender: TField);
begin
  if DM_WinTuda.Tbl_Nehag.FindKey([Qry_AtmShibLakDriverNo1.AsInteger]) Then
    Qry_AtmShibLak.FieldByName('DriverName').AsString:=DM_WinTuda.Tbl_Nehag.FieldByName('Shem_Nehag').AsString
  Else
    Qry_AtmShibLak.FieldByName('DriverName').AsString:=Lbl_ShemMadrich.Caption;
end;

procedure TFrm_AtmShibLak.DS_AtmShibLakStateChange(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:=ConvertDSStateToText(TDataSet(Sender));
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakAfterCancel(DataSet: TDataSet);
begin
  LogTimeStarted :=False;
  dxfTimer1.Stop;
end;

Function TFrm_AtmShibLak.CheckActiveTnuaToLak(LakNo :String):LongInt;
Begin
  Result:=0;
  With DM_WinTuda.RxQuery_Temp Do
  Begin
    Sql.Clear;
    Sql.Add('Select ShibNo,ShibRem2 From ATMSHIBLAK');
    Sql.Add('Where LakCodeBizua<>1');
    Sql.Add('And LakNo1='+LakNo);
    Open;
    if EOF And BOF Then
      Result:=0
    Else
      if MessageDlg('����� �� �� ����� ����� �����'+
                          #13+FieldByName('ShibRem2').AsString+
                          #13+'��� ����� ����� ����',mtConfirmation,[mbYes,mbNo],0)=mrNo Then
        Result:=FieldByName('ShibNo').AsInteger
      Else
        Result:=0;
  End;
End;

procedure TFrm_AtmShibLak.dxfTimer1DblClick(Sender: TObject);
begin
  StartTime :=Now;
  LogTimeStarted :=True;
  dxfTimer1.Reset;
  dxfTimer1.Start;
end;

procedure TFrm_AtmShibLak.Timer1Timer(Sender: TObject);
Var
  F :TIniFile;
  LastMessageNum :LongInt;
begin
  Timer1.Enabled:=False;
  F:=TIniFile.Create(WinTudaPrivateIniFileName);
  Try
    LastMessageNum:=F.ReadInteger(UserSectionMokedAtm,KeyLastReadMessage,0);
    With DM_WinTuda.RxQuery_Temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Max(RecNum) MR From AtmPopup');
      Open;
      if FieldByName('MR').AsInteger >LastMessageNum Then
        if MessageDlg('���� '+IntToStr(FieldByName('MR').AsInteger-LastMessageNum)+' ������ �����'+
                      #13'��� ������ ���� ���� ���',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
        Begin
           OpenCrtDll(fnAtmPopup,IntToStr(LastMessageNum+1));
           F.WriteInteger(UserSectionMokedAtm,KeyLastReadMessage,FieldByName('MR').AsInteger);
        End;
        Close;
    End; //With
  Finally
    Timer1.Enabled:=True;
    F.Free;
  End;
end;

procedure TFrm_AtmShibLak.DBNavigator1BeforeAction(Sender: TObject;
  Button: TmoNavBtns);
begin
  if Button = nbaRefresh Then
  Begin
    DBNavigator1.DataSource.DataSet.Close;
    DBNavigator1.DataSource.DataSet.Open;
    Abort;
  End;
end;

procedure TFrm_AtmShibLak.Act_PrintExecute(Sender: TObject);
begin
  Frm_RptAtmShibLak:=TFrm_RptAtmShibLak.Create(Nil);
  Frm_RptAtmShibLak.Query1.ParamByName('PShibNo').AsInteger:=Qry_AtmShibLak.FieldByName('ShibNo').AsInteger;
  Frm_RptAtmShibLak.Query1.Open;
  Frm_RptAtmShibLak.QuickRep1.Print;
  Frm_RptAtmShibLak.Query1.Close;
  Frm_RptAtmShibLak.Free;
end;

procedure TFrm_AtmShibLak.Qry_AtmShibLakAfterScroll(DataSet: TDataSet);
begin
  Act_Print.Enabled:=Qry_AtmShibLak.FieldByName('ShibNo').AsInteger>0;
end;

end.

object Frm_LakoachAtm: TFrm_LakoachAtm
  Left = 2
  Top = 35
  Width = 634
  Height = 415
  BiDiMode = bdRightToLeft
  Caption = '����� ������'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000011111000000000099918900000000001177800000000000007700000003
    8300000777000007FF80000777000038FFF000777780007FF7F000007000038F
    F7F00000078007FF7FFF0000000038FF7FF8F00000000FF7FFF7FFF7FF00000F
    FF8F7F77F0000000000FFF7FF00000000000F77F00000000000000FF0000FF83
    0000FF030000FF070000E3830000E0010000C0010000C0010000800100008000
    0000000000000001000080030000C0030000F0070000FE070000FF8F0000}
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 626
    Height = 54
    Align = alTop
    TabOrder = 0
    object HebLabel1: TLabel
      Left = 566
      Top = 3
      Width = 48
      Height = 13
      Caption = '��� ����'
    end
    object HebLabel2: TLabel
      Left = 424
      Top = 5
      Width = 45
      Height = 13
      Caption = '�� ����'
    end
    object HebLabel3: TLabel
      Left = 579
      Top = 27
      Width = 33
      Height = 13
      Caption = '�����'
    end
    object HebLabel4: TLabel
      Left = 224
      Top = 31
      Width = 44
      Height = 13
      Caption = '��� ����'
    end
    object HebLabel5: TLabel
      Left = 131
      Top = 3
      Width = 57
      Height = 13
      Caption = '��'#39' ����"�'
    end
    object Kod: TAtmDbHEdit
      Tag = 1
      Left = 488
      Top = 2
      Width = 57
      Height = 21
      BiDiMode = bdRightToLeft
      DataField = 'Kod_Lakoach'
      DataSource = Ds_Lakoach
      ParentBiDiMode = False
      TabOrder = 0
      OnExit = KodExit
      UseF2ToRunSearch = True
      IsFixed = False
      IsSkipTab = False
      IsMust = True
      DefaultKind = dkString
      KeyToRunSearch = ksF2
      LinkLabel = HebLabel1
      LabelColorSelect = clActiveCaption
      LabelTextColorSelect = clCaptionText
      FixedColor = clBtnFace
      TabColor = 14862004
      NormalColor = clWindow
      SearchComponent = AtmAdvSearch_Lako
      SelectNextAfterSearch = True
      StatusBarPanelNum = -1
      Hebrew = False
      LocateRecordOnChange = True
    end
    object AtmDbHEdit2: TAtmDbHEdit
      Left = 194
      Top = 2
      Width = 225
      Height = 21
      BiDiMode = bdRightToLeft
      DataField = 'Shem_Lakoach'
      DataSource = Ds_Lakoach
      ParentBiDiMode = False
      TabOrder = 3
      UseF2ToRunSearch = False
      IsFixed = False
      IsSkipTab = False
      IsMust = True
      DefaultKind = dkString
      KeyToRunSearch = ksF2
      LinkLabel = HebLabel2
      LabelColorSelect = clActiveCaption
      LabelTextColorSelect = clCaptionText
      FixedColor = clBtnFace
      TabColor = 14862004
      NormalColor = clWindow
      SelectNextAfterSearch = True
      StatusBarPanelNum = -1
      Hebrew = True
      F9String = '���� 2'
      LocateRecordOnChange = False
    end
    object Kod_kvutza: TAtmDbHEdit
      Tag = 1
      Left = 488
      Top = 27
      Width = 57
      Height = 21
      BiDiMode = bdRightToLeft
      DataField = 'Kod_Kvutza'
      DataSource = Ds_Lakoach
      ParentBiDiMode = False
      TabOrder = 1
      UseF2ToRunSearch = True
      IsFixed = False
      IsSkipTab = False
      IsMust = True
      DefaultKind = dkString
      KeyToRunSearch = ksF2
      LinkLabel = HebLabel3
      LabelColorSelect = clActiveCaption
      LabelTextColorSelect = clCaptionText
      FixedColor = clBtnFace
      TabColor = 14862004
      NormalColor = clWindow
      SearchComponent = DM_AtmCrt.AtmAdvSearch_KodTavla
      SelectNextAfterSearch = True
      StatusBarPanelNum = -1
      Hebrew = False
      BeforeExecuteSearch = Kod_kvutzaBeforeExecuteSearch
      F9String = '1'
      LocateRecordOnChange = False
    end
    object Kod_Miyun: TAtmDbHEdit
      Tag = 1
      Left = 168
      Top = 27
      Width = 49
      Height = 21
      BiDiMode = bdRightToLeft
      DataField = 'Kod_Miyun'
      DataSource = Ds_Lakoach
      ParentBiDiMode = False
      TabOrder = 4
      OnExit = Kod_MiyunExit
      UseF2ToRunSearch = True
      IsFixed = False
      IsSkipTab = False
      IsMust = False
      DefaultKind = dkString
      KeyToRunSearch = ksF2
      LinkLabel = HebLabel4
      LabelColorSelect = clActiveCaption
      LabelTextColorSelect = clCaptionText
      FixedColor = clBtnFace
      TabColor = 14862004
      NormalColor = clWindow
      SearchComponent = DM_AtmCrt.AtmAdvSearch_KodTavla
      SelectNextAfterSearch = True
      StatusBarPanelNum = -1
      Hebrew = False
      BeforeExecuteSearch = Kod_MiyunBeforeExecuteSearch
      F9String = '0'
      LocateRecordOnChange = False
    end
    object AtmDbHEdit21: TAtmDbHEdit
      Left = 16
      Top = 2
      Width = 97
      Height = 21
      BiDiMode = bdRightToLeft
      DataField = 'Mis_Han'
      DataSource = Ds_Lakoach
      ParentBiDiMode = False
      TabOrder = 6
      UseF2ToRunSearch = False
      IsFixed = False
      IsSkipTab = False
      IsMust = False
      DefaultKind = dkString
      KeyToRunSearch = ksF2
      LinkLabel = HebLabel5
      LabelColorSelect = clActiveCaption
      LabelTextColorSelect = clCaptionText
      FixedColor = clBtnFace
      TabColor = 14862004
      NormalColor = clWindow
      SelectNextAfterSearch = True
      StatusBarPanelNum = -1
      Hebrew = True
      LocateRecordOnChange = False
    end
    object AtmDbComboBox_Group: TAtmDbComboBox
      Left = 282
      Top = 27
      Width = 203
      Height = 21
      TabStop = False
      Color = clInfoBk
      ItemHeight = 13
      Sorted = True
      TabOrder = 2
      DataSource = Ds_Lakoach
      DataField = 'Kod_Kvutza'
      SugTavlaIndex = 0
    end
    object AtmDbComboBox_Kod_Miyun: TAtmDbComboBox
      Left = 14
      Top = 27
      Width = 152
      Height = 21
      TabStop = False
      Color = clInfoBk
      ItemHeight = 13
      Sorted = True
      TabOrder = 5
      DataSource = Ds_Lakoach
      DataField = 'Kod_Miyun'
      SugTavlaIndex = 0
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 338
    Width = 626
    Height = 24
    Align = alBottom
    AutoSize = True
    ButtonWidth = 27
    Flat = True
    Images = DM_AtmCrt.ImageList_CrtButtons
    List = True
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ShowHint = True
    TabOrder = 2
    Transparent = True
    object DBNavigator1: TDBNavigator
      Left = 0
      Top = 0
      Width = 240
      Height = 22
      DataSource = Ds_Lakoach
      Flat = True
      Hints.Strings = (
        '����� ������-(Ctrl+Home)'
        '����� �����-(PgUp)'
        '����� ����-(PgDn)'
        '����� ������-(Ctrl+End)'
        '����� ����-(F11)'
        '��� �����-(Ctrl+Del)'
        '���� �����'
        '���� �����-(F3)'
        '��� �������'
        '����')
      TabOrder = 0
      BeforeAction = DBNavigator1BeforeAction
    end
    object ToolButton1: TToolButton
      Left = 240
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object TBtn_OpenSearch: TToolButton
      Left = 248
      Top = 0
      Action = DM_AtmCrt.Action_OpenSearch
      AutoSize = True
    end
    object ToolButton2: TToolButton
      Left = 275
      Top = 0
      Action = DM_AtmCrt.Action_OpenSearchForm
      AutoSize = True
    end
    object ToolButton3: TToolButton
      Left = 302
      Top = 0
      Action = DM_AtmCrt.Action_DupRecord
      AutoSize = True
    end
    object TBtn_Report: TToolButton
      Left = 329
      Top = 0
      Hint = '�����'
      AutoSize = True
      Caption = '�����'
      ImageIndex = 11
      OnClick = TBtn_ReportClick
    end
    object TBtn_SumAmoutTnuot: TToolButton
      Left = 356
      Top = 0
      Hint = '��� ���� �������'
      AutoSize = True
      ImageIndex = 2
      OnClick = TBtn_SumAmoutTnuotClick
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 54
    Width = 626
    Height = 284
    ActivePage = TS_Main
    Align = alClient
    HotTrack = True
    TabOrder = 1
    OnChange = PageControl1Change
    object TS_Main: TTabSheet
      Caption = '����� ����'
      object HebLabel14: TLabel
        Left = 319
        Top = 0
        Width = 40
        Height = 13
        Caption = '����� 1'
      end
      object HebLabel15: TLabel
        Left = 318
        Top = 24
        Width = 40
        Height = 13
        Caption = '����� 2'
      end
      object HebLabel16: TLabel
        Left = 317
        Top = 48
        Width = 40
        Height = 13
        Caption = '����� 3'
      end
      object HebLabel26: TLabel
        Left = 109
        Top = 168
        Width = 28
        Height = 13
        Caption = '�.��'#39
      end
      object HebLabel27: TLabel
        Left = 583
        Top = 192
        Width = 27
        Height = 13
        Caption = '����'
      end
      object HebLabel23: TLabel
        Left = 284
        Top = 75
        Width = 71
        Height = 13
        Caption = '����� �����'
      end
      object HebLabel24: TLabel
        Left = 111
        Top = 72
        Width = 60
        Height = 13
        Caption = '����� ����'
      end
      object Label1: TLabel
        Left = 285
        Top = 98
        Width = 72
        Height = 13
        Caption = '��� ����� ��'
      end
      object Label2: TLabel
        Left = 307
        Top = 122
        Width = 50
        Height = 13
        Caption = '��'#39' �����'
      end
      object Label3: TLabel
        Left = 294
        Top = 148
        Width = 63
        Height = 13
        Caption = '���� ������'
      end
      object Label4: TLabel
        Left = 293
        Top = 171
        Width = 64
        Height = 13
        Caption = '���� ���"�'
      end
      object Label5: TLabel
        Left = 110
        Top = 98
        Width = 63
        Height = 13
        Caption = '���� �� ���'
      end
      object Label7: TLabel
        Left = 113
        Top = 6
        Width = 62
        Height = 13
        Caption = '���� �����'
      end
      object Label8: TLabel
        Left = 111
        Top = 30
        Width = 64
        Height = 13
        Caption = '���� �����'
      end
      object Label9: TLabel
        Left = 119
        Top = 52
        Width = 56
        Height = 13
        Caption = '���� ����'
      end
      object DBText1: TDBText
        Left = 65
        Top = 53
        Width = 42
        Height = 13
        AutoSize = True
        Color = clLime
        DataField = 'Calc_ItratShaot'
        DataSource = Ds_Lakoach
        ParentColor = False
      end
      object HebGroupBox1: TGroupBox
        Left = 360
        Top = 0
        Width = 258
        Height = 97
        BiDiMode = bdRightToLeft
        Caption = '����� '
        ParentBiDiMode = False
        TabOrder = 0
        object HebLabel6: TLabel
          Left = 205
          Top = 18
          Width = 25
          Height = 13
          Caption = '����'
        end
        object HebLabel7: TLabel
          Left = 212
          Top = 40
          Width = 18
          Height = 13
          Caption = '���'
        end
        object HebLabel8: TLabel
          Left = 89
          Top = 44
          Width = 31
          Height = 13
          Caption = '�����'
        end
        object HebLabel9: TLabel
          Left = 209
          Top = 64
          Width = 21
          Height = 13
          Caption = '���'
        end
        object Spb_OpenMslDlgSearchPlace: TSpeedButton
          Left = 232
          Top = 15
          Width = 23
          Height = 22
          Hint = '����� ��������'
          Glyph.Data = {
            42020000424D4202000000000000420000002800000010000000100000000100
            1000030000000002000000000000000000000000000000000000007C0000E003
            00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0F000F000F000F000F001F7C1F7C1F7C
            1F7C1F7C1F7C0F000F000F000F000F000F000F000F000F000F000F000F000F00
            0F000F001F7C0F000F000F000F000F000F00EF3D0F00FF7F0F000F000F000F00
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3DEF3D0F00FF7FFF7FFF7FFF7FFF7F
            0F000F001F7C0F000F00EF3DEF3DEF3DEF3D0F001F7C0F00FF7FFF7FFF7FFF7F
            0F000F001F7C1F7C0F000F000F000F000F001F7C1F7C1F7C0F000F000F000F00
            0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C}
          ParentShowHint = False
          ShowHint = True
          OnClick = Spb_OpenMslDlgSearchPlaceClick
        end
        object HebLabel43: TLabel
          Left = 54
          Top = 20
          Width = 16
          Height = 13
          Caption = '��'#39
        end
        object AtmDbHEdit_City: TAtmDbHEdit
          Left = 122
          Top = 40
          Width = 81
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Ir_1'
          DataSource = Ds_Lakoach
          ParentBiDiMode = False
          TabOrder = 2
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel7
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          F9String = '��� ��'
          LocateRecordOnChange = False
        end
        object AtmDbHEdit5: TAtmDbHEdit
          Left = 10
          Top = 40
          Width = 73
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Mikud_1'
          DataSource = Ds_Lakoach
          ParentBiDiMode = False
          TabOrder = 3
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel8
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object AtmDbHEdit6: TAtmDbHEdit
          Left = 82
          Top = 64
          Width = 121
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Fax'
          DataSource = Ds_Lakoach
          ParentBiDiMode = False
          TabOrder = 4
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel9
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object AtmDbHEdit_Address: TAtmDbHEdit
          Left = 88
          Top = 16
          Width = 115
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Ktovet_1'
          DataSource = Ds_Lakoach
          ParentBiDiMode = False
          TabOrder = 0
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel6
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          F9String = '������'
          LocateRecordOnChange = False
        end
        object AtmDbHEdit_HouseNum: TAtmDbHEdit
          Left = 10
          Top = 16
          Width = 40
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Mis_Bait1'
          DataSource = Ds_Lakoach
          ParentBiDiMode = False
          TabOrder = 1
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel43
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          F9String = '5'
          LocateRecordOnChange = False
        end
      end
      object HebGroupBox2: TGroupBox
        Left = 360
        Top = 104
        Width = 258
        Height = 81
        BiDiMode = bdRightToLeft
        Caption = '����� �����'
        ParentBiDiMode = False
        TabOrder = 1
        object HebLabel11: TLabel
          Left = 208
          Top = 16
          Width = 33
          Height = 13
          Caption = '�����'
        end
        object HebLabel12: TLabel
          Left = 223
          Top = 48
          Width = 18
          Height = 13
          Caption = '���'
        end
        object HebLabel13: TLabel
          Left = 86
          Top = 52
          Width = 31
          Height = 13
          Caption = '�����'
        end
        object HebLabel44: TLabel
          Left = 54
          Top = 20
          Width = 16
          Height = 13
          Caption = '��'#39
        end
        object AtmDbHEdit_Address2: TAtmDbHEdit
          Left = 87
          Top = 16
          Width = 115
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Ktovet_2'
          DataSource = Ds_Lakoach
          Font.Charset = HEBREW_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentFont = False
          TabOrder = 0
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel11
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          LocateRecordOnChange = False
        end
        object AtmDbHEdit_City2: TAtmDbHEdit
          Left = 120
          Top = 48
          Width = 81
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Ir_2'
          DataSource = Ds_Lakoach
          Font.Charset = HEBREW_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentFont = False
          TabOrder = 2
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel12
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = True
          F9String = '����'
          LocateRecordOnChange = False
        end
        object AtmDbHEdit_ZipCode2: TAtmDbHEdit
          Left = 8
          Top = 48
          Width = 73
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Mikud_2'
          DataSource = Ds_Lakoach
          Font.Charset = HEBREW_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentFont = False
          TabOrder = 3
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel13
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object AtmDbHEdit_HouseNum2: TAtmDbHEdit
          Left = 9
          Top = 16
          Width = 40
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Mis_Bait2'
          DataSource = Ds_Lakoach
          ParentBiDiMode = False
          TabOrder = 1
          UseF2ToRunSearch = False
          IsFixed = False
          IsSkipTab = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = HebLabel44
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          TabColor = 14862004
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
      end
      object AtmDbHEdit1: TAtmDbHEdit
        Left = 181
        Top = 0
        Width = 102
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Tel_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 2
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel14
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        F9String = '03-998877'
        LocateRecordOnChange = False
      end
      object AtmDbHEdit10: TAtmDbHEdit
        Left = 181
        Top = 48
        Width = 102
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Ple_Phon'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 4
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel16
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
      object AtmDbHEdit17: TAtmDbHEdit
        Left = 181
        Top = 24
        Width = 102
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Tel_2'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 3
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel15
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
      object AtmDbHMemo1: TAtmDbHMemo
        Left = 18
        Top = 188
        Width = 561
        Height = 39
        BiDiMode = bdRightToLeft
        DataField = 'Earot'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 16
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        LinkLabel = HebLabel27
        NormalColor = clWindow
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        Hebrew = True
      end
      object AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox
        Left = -2
        Top = 163
        Width = 105
        Height = 21
        Color = clInfoBk
        ItemHeight = 13
        Sorted = True
        TabOrder = 8
        DataSource = Ds_Lakoach
        DataField = 'Kod_Tnai_Tashlum'
        SugTavlaIndex = 0
      end
      object AtmDBDateEdit1: TAtmDBDateEdit
        Left = 181
        Top = 72
        Width = 102
        Height = 21
        DataField = 'Taarich_Atchala'
        DataSource = Ds_Lakoach
        CheckOnExit = True
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        NumGlyphs = 2
        TabOrder = 5
        StartOfWeek = Sun
        Weekends = [Sat]
        YearDigits = dyFour
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        IsMust = False
        IsSkipTab = False
        IsFixed = False
        FixedColor = clBtnFace
        StatusBarPanelNum = -1
      end
      object AtmDBDateEdit3: TAtmDBDateEdit
        Left = 1
        Top = 70
        Width = 105
        Height = 21
        DataField = 'Taarich_Siyum'
        DataSource = Ds_Lakoach
        CheckOnExit = True
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        NumGlyphs = 2
        TabOrder = 6
        StartOfWeek = Sun
        Weekends = [Sat]
        YearDigits = dyFour
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        IsMust = False
        IsSkipTab = False
        IsFixed = False
        FixedColor = clBtnFace
        StatusBarPanelNum = -1
      end
      object AtmDbHEdit3: TAtmDbHEdit
        Left = 181
        Top = 96
        Width = 102
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Kod_Kesher'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 7
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label1
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = AtmAdvSearch_Lako
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
      object AtmDbHEdit4: TAtmDbHEdit
        Left = 181
        Top = 120
        Width = 102
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Tosefet_Achuz'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 9
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label2
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = AtmAdvSearch_Lako
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
      object AtmDbHEdit7: TAtmDbHEdit
        Left = 251
        Top = 143
        Width = 33
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Integer_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 11
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label3
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = DM_AtmCrt.AtmAdvSearch_KodTavla
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        BeforeExecuteSearch = AtmDbHEdit7BeforeExecuteSearch
        LocateRecordOnChange = False
      end
      object AtmDbComboBox_DatabaseType: TAtmDbComboBox
        Left = 144
        Top = 143
        Width = 105
        Height = 21
        Color = clInfoBk
        ItemHeight = 13
        Sorted = True
        TabOrder = 12
        DataSource = Ds_Lakoach
        DataField = 'Integer_1'
        SugTavlaIndex = 0
      end
      object AtmDbHEdit8: TAtmDbHEdit
        Left = 251
        Top = 166
        Width = 33
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Integer_2'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 14
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label4
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = DM_AtmCrt.AtmAdvSearch_KodTavla
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        BeforeExecuteSearch = AtmDbHEdit8BeforeExecuteSearch
        LocateRecordOnChange = False
      end
      object AtmDbComboBox_SugHan: TAtmDbComboBox
        Left = 144
        Top = 166
        Width = 105
        Height = 21
        Color = clInfoBk
        ItemHeight = 13
        Sorted = True
        TabOrder = 15
        DataSource = Ds_Lakoach
        DataField = 'Integer_2'
        SugTavlaIndex = 0
      end
      object AtmDbHEdit9: TAtmDbHEdit
        Left = 1
        Top = 96
        Width = 105
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Mutzmad_Achuz_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 10
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label5
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = AtmAdvSearch_Lako
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
      object DBCheckBox_Pail: TDBCheckBox
        Left = 32
        Top = 122
        Width = 97
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = '���� ����'
        DataField = 'Sug_Madad_3'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 13
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object Panel_Sherut: TPanel
        Left = 0
        Top = 222
        Width = 618
        Height = 34
        Align = alBottom
        BevelWidth = 3
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 17
      end
      object DBCheckBox1: TDBCheckBox
        Left = 32
        Top = 142
        Width = 97
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = '����� ����'
        DataField = 'Sug_Madad_2'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 18
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object AtmDbHEdit20: TAtmDbHEdit
        Left = 3
        Top = 4
        Width = 105
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Money_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 19
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label7
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
      object AtmDbHEdit22: TAtmDbHEdit
        Left = 3
        Top = 28
        Width = 105
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Money_2'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 20
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label8
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = AtmAdvSearch_Lako
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        LocateRecordOnChange = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = '���� �����'
      object HebLabel31: TLabel
        Left = 553
        Top = 119
        Width = 64
        Height = 13
        Caption = '��� �������'
      end
      object HebLabel32: TLabel
        Left = 317
        Top = 53
        Width = 52
        Height = 13
        Caption = '��'#39' �����'
      end
      object HebLabel33: TLabel
        Left = 317
        Top = 75
        Width = 24
        Height = 13
        Caption = '����'
      end
      object HebLabel34: TLabel
        Left = 591
        Top = 141
        Width = 26
        Height = 13
        Caption = '����'
      end
      object HebLabel36: TLabel
        Left = 557
        Top = 163
        Width = 60
        Height = 13
        Caption = '����� ����'
      end
      object HebLabel37: TLabel
        Left = 317
        Top = 8
        Width = 54
        Height = 13
        Caption = '����� �-%'
      end
      object HebLabel38: TLabel
        Left = 317
        Top = 31
        Width = 64
        Height = 13
        Caption = '��� ������'
      end
      object HebLabel39: TLabel
        Left = 317
        Top = 97
        Width = 42
        Height = 13
        Caption = '��� ���'
      end
      object HebLabel40: TLabel
        Left = 317
        Top = 119
        Width = 57
        Height = 13
        Caption = '����� ���'
      end
      object HebLabel41: TLabel
        Left = 317
        Top = 141
        Width = 50
        Height = 13
        Caption = '��� ����'
      end
      object HebLabel42: TLabel
        Left = 317
        Top = 163
        Width = 71
        Height = 13
        Caption = '���� ������'
      end
      object HebLabel19: TLabel
        Left = 555
        Top = 5
        Width = 60
        Height = 13
        Caption = '% �� �����'
      end
      object HebLabel20: TLabel
        Left = 538
        Top = 31
        Width = 79
        Height = 13
        Caption = '����� ���� ��'
      end
      object HebLabel29: TLabel
        Left = 567
        Top = 97
        Width = 49
        Height = 13
        Caption = '��� �����'
      end
      object SpeedButton1: TSpeedButton
        Left = 510
        Top = 100
        Width = 16
        Height = 15
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Layout = blGlyphBottom
        ParentFont = False
        OnClick = SpeedButton1Click
      end
      object HebLabel18: TLabel
        Left = 553
        Top = 74
        Width = 62
        Height = 13
        Caption = '��'#39' �.�./�.�.'
      end
      object HebLabel35: TLabel
        Left = 115
        Top = 5
        Width = 53
        Height = 13
        Caption = '��� �����'
      end
      object HebLabel30: TLabel
        Left = 115
        Top = 28
        Width = 43
        Height = 13
        Caption = '��� ����'
      end
      object HebLabel21: TLabel
        Left = 115
        Top = 52
        Width = 37
        Height = 13
        Caption = '% ����'
      end
      object HebLabel22: TLabel
        Left = 115
        Top = 76
        Width = 47
        Height = 13
        Caption = '��� ����'
      end
      object HebLabel10: TLabel
        Left = 115
        Top = 98
        Width = 57
        Height = 13
        Caption = '��� �����'
      end
      object HebLabel28: TLabel
        Left = 115
        Top = 122
        Width = 69
        Height = 13
        Caption = '<��� ������>'
        OnDblClick = HebLabel28DblClick
      end
      object Label6: TLabel
        Left = 545
        Top = 189
        Width = 70
        Height = 13
        Caption = '���� ������'
      end
      object HebLabel17: TLabel
        Left = 117
        Top = 144
        Width = 24
        Height = 13
        Caption = '����'
      end
      object HebLabel25: TLabel
        Left = 548
        Top = 215
        Width = 49
        Height = 13
        Caption = '��� ����'
      end
      object HebDBText3: TDBText
        Left = 247
        Top = 213
        Width = 173
        Height = 17
        BiDiMode = bdRightToLeft
        Color = clInfoBk
        DataField = 'LookRemark'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        ParentColor = False
      end
      object AtmDbHEdit27: TAtmDbHEdit
        Left = 408
        Top = 119
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Sug_Bitachon'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 4
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel31
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit28: TAtmDbHEdit
        Left = 408
        Top = 141
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Schum_Bitachon'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 5
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel34
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit29: TAtmDbHEdit
        Left = 408
        Top = 163
        Width = 103
        Height = 21
        BiDiMode = bdLeftToRight
        DataField = 'Taarich_Siyun_Bitachon'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 6
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel36
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit30: TAtmDbHEdit
        Left = 205
        Top = 8
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Tosefet_Achuz'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 7
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel37
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit31: TAtmDbHEdit
        Left = 205
        Top = 31
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Tosefet_Melel'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 8
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel38
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit32: TAtmDbHEdit
        Left = 205
        Top = 53
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Mis_Taktziv'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 9
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel32
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit33: TAtmDbHEdit
        Left = 205
        Top = 75
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Choze'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 10
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel33
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit34: TAtmDbHEdit
        Left = 205
        Top = 97
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Sug_Madad_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 11
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel39
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit36: TAtmDbHEdit
        Left = 205
        Top = 141
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Madad_Basis_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 13
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel41
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit37: TAtmDbHEdit
        Left = 205
        Top = 163
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Mutzmad_Achuz_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 14
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel42
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit13: TAtmDbHEdit
        Left = 408
        Top = 5
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Mas_Makor'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 0
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel19
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDBDateEdit2: TAtmDBDateEdit
        Left = 408
        Top = 29
        Width = 103
        Height = 21
        DataField = 'Taarich_Mas'
        DataSource = Ds_Lakoach
        CheckOnExit = True
        NumGlyphs = 2
        TabOrder = 1
        StartOfWeek = Sun
        Weekends = [Sat]
        YearDigits = dyFour
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        IsMust = False
        IsSkipTab = False
        IsFixed = False
        FixedColor = clBtnFace
        StatusBarPanelNum = -1
      end
      object AtmDBDateEdit4: TAtmDBDateEdit
        Left = 205
        Top = 119
        Width = 103
        Height = 21
        DataField = 'Taarich_Madad_1'
        DataSource = Ds_Lakoach
        CheckOnExit = True
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        NumGlyphs = 2
        TabOrder = 12
        StartOfWeek = Sun
        Weekends = [Sat]
        YearDigits = dyFour
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        IsMust = False
        IsSkipTab = False
        IsFixed = False
        FixedColor = clBtnFace
        StatusBarPanelNum = -1
      end
      object AtmDbHEdit24: TAtmDbHEdit
        Left = 408
        Top = 97
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Sug_Rikuz'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 3
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel29
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit19: TAtmDbHEdit
        Left = 408
        Top = 74
        Width = 103
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Osek_Morshe_ID'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 2
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel18
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit25: TAtmDbHEdit
        Left = 16
        Top = 5
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Sug_Koteret'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 15
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel35
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit26: TAtmDbHEdit
        Left = 16
        Top = 28
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Sug_Miyun'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 16
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel30
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit11: TAtmDbHEdit
        Left = 16
        Top = 52
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Achuz_Anacha_Amala'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 17
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel21
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit12: TAtmDbHEdit
        Left = 16
        Top = 76
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Sug_Anacha_Amala'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 18
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel22
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit15: TAtmDbHEdit
        Left = 16
        Top = 98
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Kod_Atzmada'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 19
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel10
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit_CodeMehiron: TAtmDbHEdit
        Left = 16
        Top = 121
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Kod_mechiron'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 20
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel28
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = AtmAdvSearch_Mehiron
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        F9String = '1'
        LocateRecordOnChange = False
      end
      object AtmDbHEdit16: TAtmDbHEdit
        Left = 311
        Top = 186
        Width = 200
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'String_1'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 21
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = Label6
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbHEdit18: TAtmDbHEdit
        Left = 15
        Top = 144
        Width = 91
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Ish_Kesher'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 22
        UseF2ToRunSearch = False
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel17
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = True
        LocateRecordOnChange = False
      end
      object AtmDbhedit14: TAtmDbHEdit
        Left = 422
        Top = 211
        Width = 89
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Kod_Eara'
        DataSource = Ds_Lakoach
        ParentBiDiMode = False
        TabOrder = 23
        UseF2ToRunSearch = True
        IsFixed = False
        IsSkipTab = False
        IsMust = False
        DefaultKind = dkString
        KeyToRunSearch = ksF2
        LinkLabel = HebLabel25
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        FixedColor = clBtnFace
        TabColor = 14862004
        NormalColor = clWindow
        SearchComponent = DM_AtmCrt.AtmAdvSearch_KodTavla
        SelectNextAfterSearch = True
        StatusBarPanelNum = -1
        Hebrew = False
        BeforeExecuteSearch = AtmDbhedit14BeforeExecuteSearch
        LocateRecordOnChange = False
      end
    end
    object TabSheet3: TTabSheet
      Caption = '����'
      object Grid_Lakoach: TDBGrid
        Left = 0
        Top = 0
        Width = 618
        Height = 208
        Align = alClient
        DataSource = Ds_Lakoach
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TS_Extra: TTabSheet
      Caption = '���� ������'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 362
    Width = 626
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SimplePanel = False
    SimpleText = 
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9 +
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9 +
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9
  end
  object Tbl_Lakoach: TAtmRxQuery
    Tag = 1
    CachedUpdates = True
    AfterInsert = Tbl_LakoachAfterInsert
    AfterPost = Tbl_LakoachAfterPost
    AfterScroll = Tbl_LakoachAfterScroll
    OnCalcFields = Tbl_LakoachCalcFields
    OnNewRecord = Tbl_LakoachNewRecord
    AutoRefresh = True
    DatabaseName = 'DB_AtmCrt'
    RequestLive = True
    SQL.Strings = (
      'Select * From Lakoach'
      'Order By %OrderFields')
    UpdateMode = upWhereChanged
    Macros = <
      item
        DataType = ftString
        Name = 'OrderFields'
        ParamType = ptInput
        Value = 'Kod_Lakoach'
      end>
    IndexFieldNames = 'Kod_Lakoach'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = True
    AutoValidateLookupFields = True
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = False
    Left = 284
    Top = 62
    object Tbl_LakoachKod_Lakoach: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'Kod_Lakoach'
      Required = True
    end
    object Tbl_LakoachShem_Lakoach: TStringField
      DisplayLabel = '�� ����'
      FieldName = 'Shem_Lakoach'
      Size = 30
    end
    object Tbl_LakoachKod_Kvutza: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = '�����'
      FieldName = 'Kod_Kvutza'
    end
    object Tbl_LakoachKod_Miyun: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = '��� ����'
      FieldName = 'Kod_Miyun'
    end
    object Tbl_LakoachMis_Han: TStringField
      DisplayLabel = '��'#39' ���"�'
      FieldName = 'Mis_Han'
      Size = 15
    end
    object Tbl_LakoachKtovet_1: TStringField
      DisplayLabel = '�����'
      FieldName = 'Ktovet_1'
      OnValidate = Tbl_LakoachKtovet_1Validate
      Size = 30
    end
    object Tbl_LakoachMis_Bait1: TStringField
      FieldName = 'Mis_Bait1'
      OnValidate = Tbl_LakoachKtovet_1Validate
      Size = 7
    end
    object Tbl_LakoachIr_1: TStringField
      DisplayLabel = '���'
      FieldName = 'Ir_1'
      OnValidate = Tbl_LakoachKtovet_1Validate
      Size = 15
    end
    object Tbl_LakoachMikud_1: TIntegerField
      DisplayLabel = '�����'
      FieldName = 'Mikud_1'
    end
    object Tbl_LakoachKtovet_2: TStringField
      DisplayLabel = '����� �����'
      FieldName = 'Ktovet_2'
      Size = 30
    end
    object Tbl_LakoachIr_2: TStringField
      DisplayLabel = '��� �����'
      FieldName = 'Ir_2'
      Size = 15
    end
    object Tbl_LakoachMis_Bait2: TStringField
      FieldName = 'Mis_Bait2'
      Size = 7
    end
    object Tbl_LakoachMikud_2: TIntegerField
      DisplayLabel = '����� ����'
      FieldName = 'Mikud_2'
    end
    object Tbl_LakoachTel_1: TStringField
      DisplayLabel = '�����'
      FieldName = 'Tel_1'
      Size = 13
    end
    object Tbl_LakoachTel_2: TStringField
      DisplayLabel = '����� 2'
      FieldName = 'Tel_2'
      Size = 13
    end
    object Tbl_LakoachFax: TStringField
      DisplayLabel = '���'
      FieldName = 'Fax'
      Size = 13
    end
    object Tbl_LakoachPle_Phon: TStringField
      DisplayLabel = '��� ���'
      FieldName = 'Ple_Phon'
      Size = 13
    end
    object Tbl_LakoachIsh_Kesher: TStringField
      DisplayLabel = '��� ���'
      FieldName = 'Ish_Kesher'
    end
    object Tbl_LakoachOsek_Morshe_ID: TStringField
      DisplayLabel = '��'#39' ���� �����'
      FieldName = 'Osek_Morshe_ID'
      Size = 12
    end
    object Tbl_LakoachKod_Tnai_Tashlum: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = '��� ���� �����'
      FieldName = 'Kod_Tnai_Tashlum'
    end
    object Tbl_LakoachKod_Eara: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'Kod_Eara'
    end
    object Tbl_LakoachSug_Anacha_Amala: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'Sug_Anacha_Amala'
    end
    object Tbl_LakoachKod_Kesher: TIntegerField
      DisplayLabel = '��� ���'
      FieldName = 'Kod_Kesher'
    end
    object Tbl_LakoachEarot: TMemoField
      DisplayLabel = '�����'
      FieldName = 'Earot'
      BlobType = ftMemo
      Size = 30
    end
    object Tbl_LakoachKod_Banehag: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'Kod_Banehag'
    end
    object Tbl_LakoachKod_Basapak: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'Kod_Basapak'
    end
    object Tbl_LakoachKod_Atzmada: TIntegerField
      DisplayLabel = '��� �����'
      FieldName = 'Kod_Atzmada'
    end
    object Tbl_LakoachKod_mechiron: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = '��� ������'
      FieldName = 'Kod_mechiron'
    end
    object Tbl_LakoachSug_Koteret: TIntegerField
      DisplayLabel = '��� �����'
      FieldName = 'Sug_Koteret'
    end
    object Tbl_LakoachSug_Miyun: TIntegerField
      DisplayLabel = '��� ����'
      FieldName = 'Sug_Miyun'
    end
    object Tbl_LakoachMis_Taktziv: TStringField
      DisplayLabel = '��'#39' �����'
      FieldName = 'Mis_Taktziv'
      Size = 25
    end
    object Tbl_LakoachChoze: TStringField
      DisplayLabel = '����'
      FieldName = 'Choze'
      Size = 25
    end
    object Tbl_LakoachSug_Bitachon: TIntegerField
      DisplayLabel = '��� ������'
      FieldName = 'Sug_Bitachon'
    end
    object Tbl_LakoachTosefet_Melel: TStringField
      DisplayLabel = '����� �����'
      FieldName = 'Tosefet_Melel'
      Size = 30
    end
    object Tbl_LakoachSug_Madad_1: TIntegerField
      DisplayLabel = '��� ��� 1'
      FieldName = 'Sug_Madad_1'
    end
    object Tbl_LakoachSug_Madad_2: TIntegerField
      DisplayLabel = '��� ��� 2'
      FieldName = 'Sug_Madad_2'
    end
    object Tbl_LakoachSug_Madad_3: TIntegerField
      DisplayLabel = '��� ��� 3'
      FieldName = 'Sug_Madad_3'
    end
    object Tbl_LakoachString_1: TStringField
      FieldName = 'String_1'
      Size = 30
    end
    object Tbl_LakoachString_2: TStringField
      FieldName = 'String_2'
      Size = 30
    end
    object Tbl_LakoachString_3: TStringField
      FieldName = 'String_3'
      Size = 30
    end
    object Tbl_LakoachString_4: TStringField
      FieldName = 'String_4'
      Size = 30
    end
    object Tbl_LakoachString_5: TStringField
      FieldName = 'String_5'
      Size = 30
    end
    object Tbl_LakoachInteger_1: TIntegerField
      FieldName = 'Integer_1'
    end
    object Tbl_LakoachInteger_2: TIntegerField
      FieldName = 'Integer_2'
    end
    object Tbl_LakoachInteger_3: TIntegerField
      FieldName = 'Integer_3'
    end
    object Tbl_LakoachInteger_4: TIntegerField
      FieldName = 'Integer_4'
    end
    object Tbl_LakoachInteger_5: TIntegerField
      FieldName = 'Integer_5'
    end
    object Tbl_LakoachTaarich_Osek_Morshe: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' ���� �����'
      FieldName = 'Taarich_Osek_Morshe'
    end
    object Tbl_LakoachTaarich_Mas: TDateTimeField
      Tag = 5
      DisplayLabel = '����� ��'
      FieldName = 'Taarich_Mas'
    end
    object Tbl_LakoachTaarich_Atchala: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' �����'
      FieldName = 'Taarich_Atchala'
    end
    object Tbl_LakoachTaarich_Siyum: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' ����'
      FieldName = 'Taarich_Siyum'
    end
    object Tbl_LakoachTaarich_Siyun_Bitachon: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' ���� �����'
      FieldName = 'Taarich_Siyun_Bitachon'
    end
    object Tbl_LakoachTaarich_Madad_1: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' ��� 1'
      FieldName = 'Taarich_Madad_1'
    end
    object Tbl_LakoachTaarich_Madad_2: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' ��� 2'
      FieldName = 'Taarich_Madad_2'
    end
    object Tbl_LakoachTaarich_madad_3: TDateTimeField
      Tag = 5
      DisplayLabel = '��'#39' ��� 3'
      FieldName = 'Taarich_madad_3'
    end
    object Tbl_LakoachDate_1: TDateTimeField
      Tag = 5
      FieldName = 'Date_1'
    end
    object Tbl_LakoachDate_2: TDateTimeField
      Tag = 5
      FieldName = 'Date_2'
    end
    object Tbl_LakoachDate_3: TDateTimeField
      Tag = 5
      FieldName = 'Date_3'
    end
    object Tbl_LakoachDate_4: TDateTimeField
      Tag = 5
      FieldName = 'Date_4'
    end
    object Tbl_LakoachDate_5: TDateTimeField
      Tag = 5
      FieldName = 'Date_5'
    end
    object Tbl_LakoachString_6: TStringField
      FieldName = 'String_6'
      Size = 30
    end
    object Tbl_LakoachString_7: TStringField
      FieldName = 'String_7'
      Size = 30
    end
    object Tbl_LakoachString_8: TStringField
      FieldName = 'String_8'
      Size = 30
    end
    object Tbl_LakoachSchum_Bitachon: TBCDField
      FieldName = 'Schum_Bitachon'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachYatza_Teudat_Mishloach: TBCDField
      FieldName = 'Yatza_Teudat_Mishloach'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachYatza_Cheshbon_Lo_Shula: TBCDField
      FieldName = 'Yatza_Cheshbon_Lo_Shula'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachChekim_Dachui_Lo_Nifra: TBCDField
      FieldName = 'Chekim_Dachui_Lo_Nifra'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_1: TBCDField
      FieldName = 'Money_1'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_2: TBCDField
      FieldName = 'Money_2'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_3: TBCDField
      FieldName = 'Money_3'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_4: TBCDField
      FieldName = 'Money_4'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_5: TBCDField
      FieldName = 'Money_5'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_6: TBCDField
      FieldName = 'Money_6'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMoney_7: TBCDField
      FieldName = 'Money_7'
      currency = True
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachInt_Kod_Kvutza: TIntegerField
      FieldName = 'Int_Kod_Kvutza'
    end
    object Tbl_LakoachInt_Kod_Miyun: TIntegerField
      FieldName = 'Int_Kod_Miyun'
    end
    object Tbl_LakoachInt_Kod_Tnai_Tashlum: TIntegerField
      FieldName = 'Int_Kod_Tnai_Tashlum'
    end
    object Tbl_LakoachMas_Makor: TBCDField
      FieldName = 'Mas_Makor'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachAchuz_Anacha_Amala: TBCDField
      FieldName = 'Achuz_Anacha_Amala'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachAchuz_Anacha_Amala_2: TBCDField
      FieldName = 'Achuz_Anacha_Amala_2'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachTosefet_Achuz: TBCDField
      FieldName = 'Tosefet_Achuz'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMadad_Basis_1: TBCDField
      FieldName = 'Madad_Basis_1'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMutzmad_Achuz_1: TBCDField
      FieldName = 'Mutzmad_Achuz_1'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMadad_Basis_2: TBCDField
      FieldName = 'Madad_Basis_2'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMutzmad_Achuz_2: TBCDField
      FieldName = 'Mutzmad_Achuz_2'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMadad_Basis_3: TBCDField
      FieldName = 'Madad_Basis_3'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachMutzmad_Achuz_3: TBCDField
      FieldName = 'Mutzmad_Achuz_3'
      Precision = 32
      Size = 3
    end
    object Tbl_LakoachLookKvutza: TStringField
      DisplayLabel = '���� �����'
      FieldKind = fkLookup
      FieldName = 'LookKvutza'
      KeyFields = 'Kod_Kvutza'
      LookupCache = True
      Lookup = True
    end
    object Tbl_LakoachLookMiun: TStringField
      DisplayLabel = '���� ����'
      FieldKind = fkLookup
      FieldName = 'LookMiun'
      KeyFields = 'Kod_Miyun'
      LookupCache = True
      Lookup = True
    end
    object Tbl_LakoachLookRemark: TStringField
      DisplayLabel = '���� ����'
      FieldKind = fkLookup
      FieldName = 'LookRemark'
      KeyFields = 'Kod_Eara'
      LookupCache = True
      Lookup = True
    end
    object Tbl_LakoachX: TIntegerField
      FieldName = 'X'
    end
    object Tbl_LakoachY: TIntegerField
      FieldName = 'Y'
    end
    object Tbl_LakoachSug_Rikuz: TStringField
      FieldName = 'Sug_Rikuz'
      Size = 40
    end
    object Tbl_LakoachNodeNumB: TIntegerField
      FieldName = 'NodeNumB'
    end
    object Tbl_LakoachNodeSugSystem: TIntegerField
      FieldName = 'NodeSugSystem'
    end
    object Tbl_LakoachNodeKodCityName: TIntegerField
      FieldName = 'NodeKodCityName'
    end
    object Tbl_LakoachCalc_ItratShaot: TBCDField
      FieldKind = fkCalculated
      FieldName = 'Calc_ItratShaot'
      Size = 2
      Calculated = True
    end
  end
  object Ds_Lakoach: TDataSource
    DataSet = Tbl_Lakoach
    OnStateChange = Ds_LakoachStateChange
    OnDataChange = Ds_LakoachDataChange
    Left = 280
    Top = 89
  end
  object Scale1: TScale
    Active = True
    X_ScreenWidth = 640
    Y_ScreenHeight = 480
    Left = 256
    Top = 258
  end
  object AtmAdvSearch_Lako: TAtmAdvSearch
    Caption = '����� ����'
    KeyIndex = 0
    SourceDataSet = DM_AtmCrt.Qry_Temp
    ShowFields.Strings = (
      'Kod_Lakoach'
      'Shem_Lakoach'
      'Kod_Kvutza')
    ShowFieldsHeader.Strings = (
      '��� ����'
      '�� ����'
      '�����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '150'
      '100')
    Sql.Strings = (
      'Select Kod_Lakoach,Shem_Lakoach,Kod_Kvutza'
      'From LAKOACH')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Lako'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 400
    Top = 272
  end
  object DualListDialog1: TDualListDialog
    Sorted = False
    Label1Caption = '���� ������'
    Label2Caption = '���� ������'
    OkBtnCaption = '�����'
    CancelBtnCaption = '���'
    HelpContext = 0
    List1.Strings = (
      'ShibAzmnNo'
      'ShibAzmnDate'
      'ShibKind'
      'ShibNo'
      'ShibToTime'
      'ShibBeginTime'
      'ShibEndTime'
      'HovalaKind'
      'PriceKind'
      'ShibTotalKm'
      'LakNo1'
      'LakNo2'
      'LakNo3'
      'LakNo4'
      'MaslulCode1'
      'MaslulCode2'
      'MaslulCode3'
      'MaslulCode4'
      'Maslul1'
      'Maslul2'
      'Maslul3'
      'Maslul4'
      'YeMida1'
      'YeMida2'
      'YeMida3'
      'YeMida4'
      'Quntity1;'
      'Quntity2'
      'Quntity3'
      'Quntity4'
      'Price1'
      'Price2'
      'Price3'
      'Price4'
      'ShibRem2'
      'ShibRem3'
      'ShibRem4'
      'DriverNo1'
      'DriverNo2'
      'DriverName'
      'CarNo'
      'CarNum'
      'LakCodeBizua'
      'LakHesbonit'
      'LakHesbonitDate'
      'DrishNo'
      'DrishDate'
      'DriverCodeBizua'
      'DriverZikuiNo'
      'DriverZikuiDate'
      'CarCodeBizua'
      'CarZikuiNo'
      'CarZikuiDate'
      'Nefh'
      'TrvlNo'
      'StarMetanNo'
      'GpsStatus'
      'GpsNo'
      'SugMetan'
      'ShibTuda'
      'ShibFromTime'
      'ShibCarKind'
      'DriverMsg'
      'AzmnOk'
      'TnuaOk'
      'HasbonitOk'
      'FirstSpido'
      'LastSpido'
      'TudaKind'
      'Mrakz'
      'NegrrNo'
      'UpdateRecordDate'
      'MaklidName'
      'ShibAsmcta')
    ShowHelp = False
    Left = 244
    Top = 44
  end
  object Query_MakavLako: TQuery
    DatabaseName = 'DB_AtmCrt'
    SQL.Strings = (
      'Select Makav.*,TotalHesbonitWithMAM-Total_Zikuy Itra from Makav')
    UpdateMode = upWhereChanged
    Left = 364
    Top = 267
  end
  object DS_MakavLako: TDataSource
    DataSet = Query_MakavLako
    Left = 334
    Top = 267
  end
  object AtmTabSheetBuild1: TAtmTabSheetBuild
    SetKeepGrid = False
    WebMode = False
    ScrFileName = 'TSBLakoach1.Scr'
    SqlFileName = 'TSBLakoach1.Sql'
    PageControl = PageControl1
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = DS_TabSheetBuild
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForCutColor = 'CutColor'
    ParamsKeyForSelectedColor = 'SelectedColor'
    ParamsKeyForExportColor = 'ExportColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = RxQuery_TabSheetBuild
    BDEQuery = RxQuery_TabSheetBuild
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    ParamsKeyForMultiSelect = 'MultiSelect'
    ParamsKeyForRowSelect = 'RowSelect'
    EditBiDiMode = bdLeftToRight
    BeforeExecuteQuery = AtmTabSheetBuild1BeforeExecuteQuery
    Left = 424
    Top = 48
  end
  object RxQuery_TabSheetBuild: TRxQuery
    DatabaseName = 'DB_AtmCrt'
    Macros = <>
    Left = 452
    Top = 50
  end
  object RxDBFilter_TabSheetBuild: TRxDBFilter
    Left = 482
    Top = 48
  end
  object DS_TabSheetBuild: TDataSource
    DataSet = RxQuery_TabSheetBuild
    Left = 510
    Top = 50
  end
  object AtmAdvSearch_Mehiron: TAtmAdvSearch
    Caption = '����� ������'
    KeyIndex = 0
    SourceDataSet = DM_AtmCrt.Qry_Temp
    ShowFields.Strings = (
      'Code_mehiron'
      'shem_mehiron'
      'sug_mehiron'
      'sug_hiuv'
      'sug_sherut')
    ShowFieldsHeader.Strings = (
      '��� ������'
      '�� ������'
      '��� ������'
      '��� ����'
      '��� ����')
    ShowFieldsDisplayWidth.Strings = (
      '70'
      '150'
      '70'
      '70'
      '70')
    Sql.Strings = (
      'Select Code_mehiron,shem_mehiron,sug_mehiron,sug_hiuv,sug_sherut'
      'From MEHIRLAK')
    ReturnFieldIndex = 0
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Mehiron'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 434
    Top = 264
  end
  object UpdateSQL_Lakoach: TUpdateSQL
    ModifySQL.Strings = (
      'update Lakoach'
      'set'
      '  Kod_Lakoach = :Kod_Lakoach,'
      '  Kod_Kvutza = :Kod_Kvutza,'
      '  Kod_Miyun = :Kod_Miyun,'
      '  Mis_Han = :Mis_Han,'
      '  Shem_Lakoach = :Shem_Lakoach,'
      '  Ktovet_1 = :Ktovet_1,'
      '  Mis_Bait1 = :Mis_Bait1,'
      '  Ir_1 = :Ir_1,'
      '  Mikud_1 = :Mikud_1,'
      '  Ktovet_2 = :Ktovet_2,'
      '  Mis_Bait2 = :Mis_Bait2,'
      '  Ir_2 = :Ir_2,'
      '  Mikud_2 = :Mikud_2,'
      '  Tel_1 = :Tel_1,'
      '  Tel_2 = :Tel_2,'
      '  Fax = :Fax,'
      '  Ple_Phon = :Ple_Phon,'
      '  Ish_Kesher = :Ish_Kesher,'
      '  Osek_Morshe_ID = :Osek_Morshe_ID,'
      '  Taarich_Osek_Morshe = :Taarich_Osek_Morshe,'
      '  Kod_Tnai_Tashlum = :Kod_Tnai_Tashlum,'
      '  Kod_Eara = :Kod_Eara,'
      '  Mas_Makor = :Mas_Makor,'
      '  Taarich_Mas = :Taarich_Mas,'
      '  Achuz_Anacha_Amala = :Achuz_Anacha_Amala,'
      '  Sug_Anacha_Amala = :Sug_Anacha_Amala,'
      '  Achuz_Anacha_Amala_2 = :Achuz_Anacha_Amala_2,'
      '  Kod_Kesher = :Kod_Kesher,'
      '  Taarich_Atchala = :Taarich_Atchala,'
      '  Taarich_Siyum = :Taarich_Siyum,'
      '  Earot = :Earot,'
      '  Kod_Banehag = :Kod_Banehag,'
      '  Kod_Basapak = :Kod_Basapak,'
      '  Kod_Atzmada = :Kod_Atzmada,'
      '  Kod_mechiron = :Kod_mechiron,'
      '  Sug_Rikuz = :Sug_Rikuz,'
      '  Sug_Koteret = :Sug_Koteret,'
      '  Sug_Miyun = :Sug_Miyun,'
      '  Mis_Taktziv = :Mis_Taktziv,'
      '  Choze = :Choze,'
      '  Sug_Bitachon = :Sug_Bitachon,'
      '  Schum_Bitachon = :Schum_Bitachon,'
      '  Taarich_Siyun_Bitachon = :Taarich_Siyun_Bitachon,'
      '  Tosefet_Achuz = :Tosefet_Achuz,'
      '  Tosefet_Melel = :Tosefet_Melel,'
      '  Sug_Madad_1 = :Sug_Madad_1,'
      '  Taarich_Madad_1 = :Taarich_Madad_1,'
      '  Madad_Basis_1 = :Madad_Basis_1,'
      '  Mutzmad_Achuz_1 = :Mutzmad_Achuz_1,'
      '  Sug_Madad_2 = :Sug_Madad_2,'
      '  Taarich_Madad_2 = :Taarich_Madad_2,'
      '  Madad_Basis_2 = :Madad_Basis_2,'
      '  Mutzmad_Achuz_2 = :Mutzmad_Achuz_2,'
      '  Sug_Madad_3 = :Sug_Madad_3,'
      '  Taarich_madad_3 = :Taarich_madad_3,'
      '  Madad_Basis_3 = :Madad_Basis_3,'
      '  Mutzmad_Achuz_3 = :Mutzmad_Achuz_3,'
      '  Yatza_Teudat_Mishloach = :Yatza_Teudat_Mishloach,'
      '  Yatza_Cheshbon_Lo_Shula = :Yatza_Cheshbon_Lo_Shula,'
      '  Chekim_Dachui_Lo_Nifra = :Chekim_Dachui_Lo_Nifra,'
      '  X = :X,'
      '  Y = :Y,'
      '  NodeNumB = :NodeNumB,'
      '  NodeSugSystem = :NodeSugSystem,'
      '  NodeKodCityName = :NodeKodCityName,'
      '  String_1 = :String_1,'
      '  String_2 = :String_2,'
      '  String_3 = :String_3,'
      '  String_4 = :String_4,'
      '  String_5 = :String_5,'
      '  String_6 = :String_6,'
      '  String_7 = :String_7,'
      '  String_8 = :String_8,'
      '  Integer_1 = :Integer_1,'
      '  Integer_2 = :Integer_2,'
      '  Integer_3 = :Integer_3,'
      '  Integer_4 = :Integer_4,'
      '  Integer_5 = :Integer_5,'
      '  Date_1 = :Date_1,'
      '  Date_2 = :Date_2,'
      '  Date_3 = :Date_3,'
      '  Date_4 = :Date_4,'
      '  Date_5 = :Date_5,'
      '  Money_1 = :Money_1,'
      '  Money_2 = :Money_2,'
      '  Money_3 = :Money_3,'
      '  Money_4 = :Money_4,'
      '  Money_5 = :Money_5,'
      '  Money_6 = :Money_6,'
      '  Money_7 = :Money_7,'
      '  Int_Kod_Kvutza = :Int_Kod_Kvutza,'
      '  Int_Kod_Miyun = :Int_Kod_Miyun,'
      '  Int_Kod_Tnai_Tashlum = :Int_Kod_Tnai_Tashlum'
      'where'
      '  Kod_Lakoach = :OLD_Kod_Lakoach')
    InsertSQL.Strings = (
      'insert into Lakoach'
      
        '  (Kod_Lakoach, Kod_Kvutza, Kod_Miyun, Mis_Han, Shem_Lakoach, Kt' +
        'ovet_1, '
      
        '   Mis_Bait1, Ir_1, Mikud_1, Ktovet_2, Mis_Bait2, Ir_2, Mikud_2,' +
        ' Tel_1, '
      
        '   Tel_2, Fax, Ple_Phon, Ish_Kesher, Osek_Morshe_ID, Taarich_Ose' +
        'k_Morshe, '
      
        '   Kod_Tnai_Tashlum, Kod_Eara, Mas_Makor, Taarich_Mas, Achuz_Ana' +
        'cha_Amala, '
      
        '   Sug_Anacha_Amala, Achuz_Anacha_Amala_2, Kod_Kesher, Taarich_A' +
        'tchala, '
      
        '   Taarich_Siyum, Earot, Kod_Banehag, Kod_Basapak, Kod_Atzmada, ' +
        'Kod_mechiron, '
      
        '   Sug_Rikuz, Sug_Koteret, Sug_Miyun, Mis_Taktziv, Choze, Sug_Bi' +
        'tachon, '
      
        '   Schum_Bitachon, Taarich_Siyun_Bitachon, Tosefet_Achuz, Tosefe' +
        't_Melel, '
      
        '   Sug_Madad_1, Taarich_Madad_1, Madad_Basis_1, Mutzmad_Achuz_1,' +
        ' Sug_Madad_2, '
      
        '   Taarich_Madad_2, Madad_Basis_2, Mutzmad_Achuz_2, Sug_Madad_3,' +
        ' Taarich_madad_3, '
      
        '   Madad_Basis_3, Mutzmad_Achuz_3, Yatza_Teudat_Mishloach, Yatza' +
        '_Cheshbon_Lo_Shula, '
      
        '   Chekim_Dachui_Lo_Nifra, X, Y, NodeNumB, NodeSugSystem, NodeKo' +
        'dCityName, '
      
        '   String_1, String_2, String_3, String_4, String_5, String_6, S' +
        'tring_7, '
      
        '   String_8, Integer_1, Integer_2, Integer_3, Integer_4, Integer' +
        '_5, Date_1, '
      
        '   Date_2, Date_3, Date_4, Date_5, Money_1, Money_2, Money_3, Mo' +
        'ney_4, '
      
        '   Money_5, Money_6, Money_7, Int_Kod_Kvutza, Int_Kod_Miyun, Int' +
        '_Kod_Tnai_Tashlum)'
      'values'
      
        '  (:Kod_Lakoach, :Kod_Kvutza, :Kod_Miyun, :Mis_Han, :Shem_Lakoac' +
        'h, :Ktovet_1, '
      
        '   :Mis_Bait1, :Ir_1, :Mikud_1, :Ktovet_2, :Mis_Bait2, :Ir_2, :M' +
        'ikud_2, '
      
        '   :Tel_1, :Tel_2, :Fax, :Ple_Phon, :Ish_Kesher, :Osek_Morshe_ID' +
        ', :Taarich_Osek_Morshe, '
      
        '   :Kod_Tnai_Tashlum, :Kod_Eara, :Mas_Makor, :Taarich_Mas, :Achu' +
        'z_Anacha_Amala, '
      
        '   :Sug_Anacha_Amala, :Achuz_Anacha_Amala_2, :Kod_Kesher, :Taari' +
        'ch_Atchala, '
      
        '   :Taarich_Siyum, :Earot, :Kod_Banehag, :Kod_Basapak, :Kod_Atzm' +
        'ada, :Kod_mechiron, '
      
        '   :Sug_Rikuz, :Sug_Koteret, :Sug_Miyun, :Mis_Taktziv, :Choze, :' +
        'Sug_Bitachon, '
      
        '   :Schum_Bitachon, :Taarich_Siyun_Bitachon, :Tosefet_Achuz, :To' +
        'sefet_Melel, '
      
        '   :Sug_Madad_1, :Taarich_Madad_1, :Madad_Basis_1, :Mutzmad_Achu' +
        'z_1, :Sug_Madad_2, '
      
        '   :Taarich_Madad_2, :Madad_Basis_2, :Mutzmad_Achuz_2, :Sug_Mada' +
        'd_3, :Taarich_madad_3, '
      
        '   :Madad_Basis_3, :Mutzmad_Achuz_3, :Yatza_Teudat_Mishloach, :Y' +
        'atza_Cheshbon_Lo_Shula, '
      
        '   :Chekim_Dachui_Lo_Nifra, :X, :Y, :NodeNumB, :NodeSugSystem, :' +
        'NodeKodCityName, '
      
        '   :String_1, :String_2, :String_3, :String_4, :String_5, :Strin' +
        'g_6, :String_7, '
      
        '   :String_8, :Integer_1, :Integer_2, :Integer_3, :Integer_4, :I' +
        'nteger_5, '
      
        '   :Date_1, :Date_2, :Date_3, :Date_4, :Date_5, :Money_1, :Money' +
        '_2, :Money_3, '
      
        '   :Money_4, :Money_5, :Money_6, :Money_7, :Int_Kod_Kvutza, :Int' +
        '_Kod_Miyun, '
      '   :Int_Kod_Tnai_Tashlum)')
    DeleteSQL.Strings = (
      'delete from Lakoach'
      'where'
      '  Kod_Lakoach = :OLD_Kod_Lakoach')
    Left = 280
    Top = 32
  end
  object DynamicControls1: TDynamicControls
    IniFileName = 'Frm_Lakoach'
    RegistryKey = 'Software\Atm\Atm2000\Forms\FrmLakoach'
    SaveIniFileNameToRegistry = True
    ExtraFieldsParent = TS_Extra
    ExtraFiledsFileName = 'Lakoach.Xtr'
    Left = 560
    Top = 46
  end
  object PopupMenu1: TPopupMenu
    Left = 400
    Top = 104
    object N1: TMenuItem
      Action = DM_AtmCrt.Act_ChooseDynamicControlFile
    end
    object N2: TMenuItem
      Action = DM_AtmCrt.Act_EditExtraFields
    end
  end
  object AtmTabSheetBuild2: TAtmTabSheetBuild
    SetKeepGrid = False
    WebMode = False
    ScrFileName = 'TSBLakoachAtm1.Scr'
    SqlFileName = 'TSBLakoachAtm1.Sql'
    SqlUpdateFileName = 'TSBLakoachAtm1.Upd'
    SqlDeleteFileName = 'TSBLakoachAtm1.Del'
    SqlInsertFileName = 'TSBLakoachAtm1.Ins'
    PageControl = PageControl1
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = DS_LakoachAtm
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForCutColor = 'CutColor'
    ParamsKeyForSelectedColor = 'SelectedColor'
    ParamsKeyForExportColor = 'ExportColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = RxQuery_TSBLakoachAtm
    BDEQuery = RxQuery_TSBLakoachAtm
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    BooleanImageList = DM_AtmCrt.ImageList_Boolean
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    ParamsKeyForMultiSelect = 'MultiSelect'
    ParamsKeyForRowSelect = 'RowSelect'
    EditBiDiMode = bdLeftToRight
    BeforeExecuteQuery = AtmTabSheetBuild2BeforeExecuteQuery
    AfterCreateGrid = AtmTabSheetBuild2AfterCreateGrid
    OnGridEditButtonClick = AtmTabSheetBuild2GridEditButtonClick
    Left = 426
    Top = 76
  end
  object RxQuery_TSBLakoachAtm: TRxQuery
    CachedUpdates = True
    AfterOpen = RxQuery_TSBLakoachAtmAfterOpen
    AfterInsert = RxQuery_TSBLakoachAtmAfterInsert
    BeforePost = RxQuery_TSBLakoachAtmBeforePost
    AfterPost = RxQuery_TSBLakoachAtmAfterPost
    AfterDelete = RxQuery_TSBLakoachAtmAfterDelete
    DatabaseName = 'DB_AtmCrt'
    RequestLive = True
    SQL.Strings = (
      'Select * From tziudatm')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_TSB2
    Macros = <>
    Left = 452
    Top = 78
  end
  object RxDBFilter_TSB_LakoachAtm: TRxDBFilter
    Left = 482
    Top = 76
  end
  object DS_LakoachAtm: TDataSource
    DataSet = RxQuery_TSBLakoachAtm
    Left = 510
    Top = 78
  end
  object UpdateSQL_TSB2: TUpdateSQL
    ModifySQL.Strings = (
      'update tziudatm'
      'set'
      '  CodeMaster = :CodeMaster,'
      '  CodeItem = :CodeItem,'
      '  Teur = :Teur,'
      '  Remark = :Remark,'
      '  BuyDate = :BuyDate,'
      '  HeshbonitNum = :HeshbonitNum,'
      '  AzmnNum = :AzmnNum,'
      '  Insured = :Insured,'
      '  DateEndInsurance = :DateEndInsurance,'
      '  SumSherut = :SumSherut,'
      '  CodeSapak = :CodeSapak,'
      '  DateEndInsuranceSapak = :DateEndInsuranceSapak,'
      '  HeshbonitSapak = :HeshbonitSapak,'
      '  SumAmlatSapak = :SumAmlatSapak,'
      '  PaiedSapak = :PaiedSapak,'
      '  Display = :Display,'
      '  MaklidName = :MaklidName,'
      '  LastUpdate = :LastUpdate'
      'where'
      '  RecordNum = :OLD_RecordNum')
    InsertSQL.Strings = (
      'insert into tziudatm'
      
        '  (CodeMaster, CodeItem, Teur, Remark, BuyDate, HeshbonitNum, Az' +
        'mnNum, '
      
        '   Insured, DateEndInsurance, SumSherut, CodeSapak, DateEndInsur' +
        'anceSapak, '
      
        '   HeshbonitSapak, SumAmlatSapak, PaiedSapak, Display, MaklidNam' +
        'e, LastUpdate)'
      'values'
      
        '  (:CodeMaster, :CodeItem, :Teur, :Remark, :BuyDate, :HeshbonitN' +
        'um, :AzmnNum, '
      
        '   :Insured, :DateEndInsurance, :SumSherut, :CodeSapak, :DateEnd' +
        'InsuranceSapak, '
      
        '   :HeshbonitSapak, :SumAmlatSapak, :PaiedSapak, :Display, :Makl' +
        'idName, '
      '   :LastUpdate)')
    DeleteSQL.Strings = (
      'delete from tziudatm'
      'where'
      '  RecordNum = :OLD_RecordNum')
    Left = 538
    Top = 78
  end
  object AtmAdvSearch_Maslul: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_AtmCrt.Qry_Temp
    ShowFields.Strings = (
      'Name'
      'CODE_MASLUL'
      'Motza'
      'Yaad'
      'Pratim')
    ShowFieldsHeader.Strings = (
      '��'
      '��� �����'
      '����'
      '���'
      '�����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '60'
      '100'
      '100'
      '100')
    Sql.Strings = (
      'Select CODE_MASLUL,Name,Motza,Yaad,Pratim'
      'From Maslul')
    ReturnFieldIndex = 1
    SeparateChar = ','
    IniSection = 'AtmAdvSearch_Maslul'
    KeepList = True
    EditBiDiMode = bdLeftToRight
    Left = 566
    Top = 77
  end
end

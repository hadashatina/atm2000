unit F_newlak;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, ToolWin, Db, DBTables, RxQuery, AtmRxQuery, StdCtrls,
  Mask, DBCtrls, AtmComp, Scale250, AdvSearch, AtmLookCombo,
  DynamicControls, dbnav797, DBFilter, AtmTabSheetBuild;

type
  TFrm_newlak = class(TForm)
    Panel1: TPanel;
    Kod_lak: TAtmDbHEdit;
    Shem_lak: TAtmDbHEdit;
    Label3: TLabel;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmDbComboBoxGroup: TAtmDbComboBox;
    Label2: TLabel;
    Label1: TLabel;
    PageControl1: TPageControl;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    DynamicControls1: TDynamicControls;
    Tbl_lakoach: TAtmRxQuery;
    Ds_lakoach: TDataSource;
    Tbl_lakoachKod_Lakoach: TIntegerField;
    Tbl_lakoachKod_Kvutza: TIntegerField;
    Tbl_lakoachKod_Miyun: TIntegerField;
    Tbl_lakoachMis_Han: TStringField;
    Tbl_lakoachShem_Lakoach: TStringField;
    Tbl_lakoachKtovet_1: TStringField;
    Tbl_lakoachMis_Bait1: TStringField;
    Tbl_lakoachIr_1: TStringField;
    Tbl_lakoachMikud_1: TIntegerField;
    Tbl_lakoachKtovet_2: TStringField;
    Tbl_lakoachMis_Bait2: TStringField;
    Tbl_lakoachIr_2: TStringField;
    Tbl_lakoachMikud_2: TIntegerField;
    Tbl_lakoachTel_1: TStringField;
    Tbl_lakoachTel_2: TStringField;
    Tbl_lakoachFax: TStringField;
    Tbl_lakoachPle_Phon: TStringField;
    Tbl_lakoachIsh_Kesher: TStringField;
    Tbl_lakoachOsek_Morshe_ID: TStringField;
    Tbl_lakoachTaarich_Osek_Morshe: TDateTimeField;
    Tbl_lakoachKod_Tnai_Tashlum: TIntegerField;
    Tbl_lakoachKod_Eara: TIntegerField;
    Tbl_lakoachMas_Makor: TBCDField;
    Tbl_lakoachTaarich_Mas: TDateTimeField;
    Tbl_lakoachAchuz_Anacha_Amala: TBCDField;
    Tbl_lakoachSug_Anacha_Amala: TIntegerField;
    Tbl_lakoachAchuz_Anacha_Amala_2: TBCDField;
    Tbl_lakoachKod_Kesher: TIntegerField;
    Tbl_lakoachTaarich_Atchala: TDateTimeField;
    Tbl_lakoachTaarich_Siyum: TDateTimeField;
    Tbl_lakoachEarot: TMemoField;
    Tbl_lakoachKod_Banehag: TIntegerField;
    Tbl_lakoachKod_Basapak: TIntegerField;
    Tbl_lakoachKod_Atzmada: TIntegerField;
    Tbl_lakoachKod_mechiron: TIntegerField;
    Tbl_lakoachSug_Rikuz: TStringField;
    Tbl_lakoachSug_Koteret: TIntegerField;
    Tbl_lakoachSug_Miyun: TIntegerField;
    Tbl_lakoachMis_Taktziv: TStringField;
    Tbl_lakoachChoze: TStringField;
    Tbl_lakoachSug_Bitachon: TIntegerField;
    Tbl_lakoachSchum_Bitachon: TBCDField;
    Tbl_lakoachTaarich_Siyun_Bitachon: TDateTimeField;
    Tbl_lakoachTosefet_Achuz: TBCDField;
    Tbl_lakoachTosefet_Melel: TStringField;
    Tbl_lakoachSug_Madad_1: TIntegerField;
    Tbl_lakoachTaarich_Madad_1: TDateTimeField;
    Tbl_lakoachMadad_Basis_1: TBCDField;
    Tbl_lakoachMutzmad_Achuz_1: TBCDField;
    Tbl_lakoachSug_Madad_2: TIntegerField;
    Tbl_lakoachTaarich_Madad_2: TDateTimeField;
    Tbl_lakoachMadad_Basis_2: TBCDField;
    Tbl_lakoachMutzmad_Achuz_2: TBCDField;
    Tbl_lakoachSug_Madad_3: TIntegerField;
    Tbl_lakoachTaarich_madad_3: TDateTimeField;
    Tbl_lakoachMadad_Basis_3: TBCDField;
    Tbl_lakoachMutzmad_Achuz_3: TBCDField;
    Tbl_lakoachYatza_Teudat_Mishloach: TBCDField;
    Tbl_lakoachYatza_Cheshbon_Lo_Shula: TBCDField;
    Tbl_lakoachChekim_Dachui_Lo_Nifra: TBCDField;
    Tbl_lakoachX: TIntegerField;
    Tbl_lakoachY: TIntegerField;
    Tbl_lakoachNodeNumB: TIntegerField;
    Tbl_lakoachNodeSugSystem: TIntegerField;
    Tbl_lakoachNodeKodCityName: TIntegerField;
    Tbl_lakoachString_1: TStringField;
    Tbl_lakoachString_2: TStringField;
    Tbl_lakoachString_3: TStringField;
    Tbl_lakoachString_4: TStringField;
    Tbl_lakoachString_5: TStringField;
    Tbl_lakoachString_6: TStringField;
    Tbl_lakoachString_7: TStringField;
    Tbl_lakoachString_8: TStringField;
    Tbl_lakoachInteger_1: TIntegerField;
    Tbl_lakoachInteger_2: TIntegerField;
    Tbl_lakoachInteger_3: TIntegerField;
    Tbl_lakoachInteger_4: TIntegerField;
    Tbl_lakoachInteger_5: TIntegerField;
    Tbl_lakoachDate_1: TDateTimeField;
    Tbl_lakoachDate_2: TDateTimeField;
    Tbl_lakoachDate_3: TDateTimeField;
    Tbl_lakoachDate_4: TDateTimeField;
    Tbl_lakoachDate_5: TDateTimeField;
    Tbl_lakoachMoney_1: TBCDField;
    Tbl_lakoachMoney_2: TBCDField;
    Tbl_lakoachMoney_3: TBCDField;
    Tbl_lakoachMoney_4: TBCDField;
    Tbl_lakoachMoney_5: TBCDField;
    Tbl_lakoachMoney_6: TBCDField;
    Tbl_lakoachMoney_7: TBCDField;
    Tbl_lakoachInt_Kod_Kvutza: TIntegerField;
    Tbl_lakoachInt_Kod_Miyun: TIntegerField;
    Tbl_lakoachInt_Kod_Tnai_Tashlum: TIntegerField;
    Tbl_lakoachLookGroup: TStringField;
    TabSheet1: TTabSheet;
    AtmDbNavigator1: TAtmDbNavigator;
    AtmAdvSearch_lak: TAtmAdvSearch;
    Label4: TLabel;
    AtmDbHEdit2: TAtmDbHEdit;
    TabSheet2: TTabSheet;
    Scale1: TScale;
    ToolButton1: TToolButton;
    TBtn_OpenSearch: TToolButton;
    AtmDbHEdit3: TAtmDbHEdit;
    Label5: TLabel;
    UpdateSQL1: TUpdateSQL;
    Qry_grouplak: TQuery;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery_Tabsheetbuild: TAtmRxQuery;
    RxDBFilter_TabsheetBuild: TRxDBFilter;
    Ds_Tabsheetbuild: TDataSource;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure Ds_lakoachDataChange(Sender: TObject; Field: TField);
    procedure Ds_lakoachStateChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Tbl_lakoachAfterInsert(DataSet: TDataSet);
    procedure Tbl_lakoachAfterPost(DataSet: TDataSet);
    procedure Tbl_lakoachNewRecord(DataSet: TDataSet);
    procedure AtmDbHEdit1BeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure Tbl_lakoachAfterScroll(DataSet: TDataSet);
    procedure Kod_lakExit(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure BuildLookupList;
    Procedure ShowForm(FormParams:ShortString);

    { Public declarations }
  end;

var
  Frm_newlak: TFrm_newlak;

implementation

uses AtmConst, AtmRutin, Crt_Glbl, DMAtmCrt;

{$R *.DFM}
Const
     PnlNum_DSState=0;
     PnlNum_Text=1;

procedure TFrm_newlak.FormKeyDown(Sender: TObject; var Key: Word;
     Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Lakoach);
end;

procedure TFrm_newlak.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     PageControl1.ActivePageIndex:=0;
     DynamicControls1.ExtraFiledsFileName:=CrtDirForScripts+DynamicControls1.ExtraFiledsFileName;
     DynamicControls1.Execute;
     BuildLookupList;

     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataSet) And (Components[I].Tag=1) Then
             (Components[I] As TDataSet).Open
          Else
              if (Components[I] is TAtmAdvSearch) Then
                  TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;

     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
{     AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
     AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;}
     AtmTabSheetBuild1.ScriptsDir:=CrtDirForScripts;
     AtmTabSheetBuild1.FilterDir:=CrtDirForCurUser;
     AtmTabSheetBuild1.BuildTabsForGrids;

  Finally
       Screen.Cursor:=crDefault;
  End;
end;


procedure TFrm_newlak.BuildLookupList;

var
   SList :TStringList;
   I :longInt;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_GroupLako),AtmDbComboBoxGroup);

     For I:=0 To ComponentCount-1 Do
     Begin
        if Components[i] is TAtmDbComboBox Then
          if TAtmDbComboBox(Components[i]).SugTavlaIndex>0 Then
            SList.AddObject(IntToStr(TAtmDbComboBox(Components[i]).SugTavlaIndex),Components[i]);
     End;

     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;


procedure TFrm_newlak.ShowForm(FormParams: ShortString);
begin
     Show;
     if FormParams<>'' Then
        Tbl_Lakoach.FindKey([FormParams])
     Else
//         Tbl_Lakoach.Insert;
       SetDatasetState(Tbl_Lakoach,CrtFirstTableState);

end;

procedure TFrm_newlak.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_NewLak:=Nil;
     Action:=caFree;

end;

procedure TFrm_newlak.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_newlak.Ds_lakoachDataChange(Sender: TObject; Field: TField);
begin
     if Field=Nil Then
        PageControl1Change(Self);

end;

procedure TFrm_newlak.Ds_lakoachStateChange(Sender: TObject);
begin
     DM_AtmCrt.Action_OpenSearchUpdate(TBtn_OpenSearch.Action);
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);

end;

procedure TFrm_newlak.PageControl1Change(Sender: TObject);
begin
  ATMDBNavigator1.DataSource:=Ds_Lakoach;
end;

procedure TFrm_newlak.Tbl_lakoachAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_newlak.Tbl_lakoachAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_newlak.Tbl_lakoachNewRecord(DataSet: TDataSet);
Var
   MaxLak :LongInt;
begin
     With DM_AtmCrt Do
     Begin
          Qry_Temp.Active:=False;
          With Qry_Temp.Sql Do
          Begin
               Clear;
               Add('Select Max(Kod_Lakoach) ML From Lakoach');
          End;
          Qry_Temp.Active:=True;
          MaxLak:=Qry_Temp.FieldByName('ML').AsInteger+1;
          Qry_Temp.Active:=False;
          Tbl_LakoachKod_lakoach.AsInteger:=MaxLak;
     End;
end;

procedure TFrm_newlak.AtmDbHEdit1BeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
   ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_GroupLako;
end;

procedure TFrm_newlak.AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
begin
  if TQuery(AtmTabSheetBuild1.Query).Params.FindParam('PLakNum')<>Nil Then
    TQuery(AtmTabSheetBuild1.Query).ParamByName('PLakNum').AsInteger:=Tbl_LakoachKod_Lakoach.AsInteger;

end;

procedure TFrm_newlak.Tbl_lakoachAfterScroll(DataSet: TDataSet);
begin
     if AtmTabSheetBuild1.Query.Active Then
        AtmTabSheetBuild1.RefreshQry(Nil);

end;

procedure TFrm_newlak.Kod_lakExit(Sender: TObject);
begin
    if Tbl_Lakoach.State=dsInsert Then
    Begin
      If Not Tbl_LakoachKod_Lakoach.IsNull Then
        If Not Tbl_Lakoach.FindKey([Kod_lak.Text]) Then Kod_lak.Text:=Kod;
    End;
end;


end.

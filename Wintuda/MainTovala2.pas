unit MainTovala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, ImgList, Menus, ExtCtrls, SpeedBar, Placemnt, jpeg, ExeMenu,
  IniFiles, StdCtrls, Buttons;


type
   TSadranProc = Procedure(DbUserName,DbPassword:ShortString;SystemCode,SubSystemCode :integer;ShibNo :Variant);
  
type
  TFrm_MainWinTuda = class(TForm)
    MainMenu1: TMainMenu;
    ActionList_Tovala: TActionList;
    ImageList_Buttons: TImageList;
    Act_WinTuda: TAction;
    N1: TMenuItem;
    SpeedBar1: TSpeedBar;
    Act_CrtOpenLako: TAction;
    NCrtMenu: TMenuItem;
    NCrtOpenLako: TMenuItem;
    Act_CrtOpenNehag: TAction;
    NCrtOpenNehag: TMenuItem;
    Act_CrtOpenRehev: TAction;
    NCrtOpenRehev: TMenuItem;
    Act_CrtOpenMaslul: TAction;
    NCrtOpenMaslul: TMenuItem;
    FormStorage_TovalaMain: TFormStorage;
    SpeedbarSection1: TSpeedbarSection;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    Act_CrtOpenMehiron: TAction;
    NCrtOpenMehiron: TMenuItem;
    SpeedItem6: TSpeedItem;
    N3: TMenuItem;
    NExit: TMenuItem;
    Panel1: TPanel;
    Image1: TImage;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    NMakavHeshbon: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    NEditSpeedBar: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    Act_OpenTavla: TAction;
    NOpenTavla: TMenuItem;
    N16: TMenuItem;
    NDefParams: TMenuItem;
    Act_HsbOpenMakav: TAction;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem7: TSpeedItem;
    Act_HsbPerutLak: TAction;
    N8: TMenuItem;
    Act_HsbCopyHeshbonit: TAction;
    Act_HsbHeshbonit: TAction;
    Act_HsbZikuyNehag: TAction;
    N19: TMenuItem;
    N20: TMenuItem;
    Act_HsbEditMname: TAction;
    N21: TMenuItem;
    Act_Kupa_Kabala: TAction;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    SpeedItem13: TSpeedItem;
    Act_AboutBox: TAction;
    N13: TMenuItem;
    N22: TMenuItem;
    Act_MeholelOpen: TAction;
    N23: TMenuItem;
    Act_MeholelReport: TAction;
    N18: TMenuItem;
    SpeedbarSection4: TSpeedbarSection;
    Spb_MeholelMain: TSpeedItem;
    Spb_MeholelReport: TSpeedItem;
    Act_Util_CopyMehiron: TAction;
    N24: TMenuItem;
    N25: TMenuItem;
    Act_DefRange: TAction;
    N26: TMenuItem;
    Act_CrtOpenMahzeva: TAction;
    N27: TMenuItem;
    SpeedItem14: TSpeedItem;
    Act_HsbPerutHiuvNehag: TAction;
    N28: TMenuItem;
    Act_HsbHeshbonitNehag: TAction;
    N29: TMenuItem;
    Act_HsbCopyHeshbonitNehag: TAction;
    N30: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    Act_CancelHeshbonLak: TAction;
    Act_CancelHeshbonNehag: TAction;
    N34: TMenuItem;
    N35: TMenuItem;
    Act_HsbOpenMakavNehag: TAction;
    N36: TMenuItem;
    SpeedItem15: TSpeedItem;
    Act_Interface_SonolSap: TAction;
    N33: TMenuItem;
    Act_Interface_SonolPrem: TAction;
    N37: TMenuItem;
    Act_MeholelCloseMeholel: TAction;
    N2: TMenuItem;
    N38: TMenuItem;
    Act_HsbOpenMakavPreforma: TAction;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    N43: TMenuItem;
    N44: TMenuItem;
    Act_HsbSikumLak: TAction;
    Act_HsbSikumCopyLak: TAction;
    Act_HsbSikumPerutLak: TAction;
    N45: TMenuItem;
    N46: TMenuItem;
    N47: TMenuItem;
    N48: TMenuItem;
    Act_HsbPreformaPerutMerukaz: TAction;
    N42: TMenuItem;
    Act_HsbPreformaMerukaz: TAction;
    N49: TMenuItem;
    N50: TMenuItem;
    Act_Kupa_Hafkada: TAction;
    Act_Kupa_PrintCopyHafkadot: TAction;
    N51: TMenuItem;
    Act_Crt_OpenTavla2: TAction;
    N210: TMenuItem;
    Act_Kupa_CancelHafkada: TAction;
    N52: TMenuItem;
    SpeedbarSection5: TSpeedbarSection;
    SpeedItem12: TSpeedItem;
    N53: TMenuItem;
    SpeedItem16: TSpeedItem;
    Act_Util_RaisePriceInTnua: TAction;
    N54: TMenuItem;
    Act_HsbPerutDolarRegular: TAction;
    Act_HsbDolar: TAction;
    Act_HsbCopyDolar: TAction;
    N55: TMenuItem;
    N56: TMenuItem;
    N57: TMenuItem;
    N58: TMenuItem;
    N59: TMenuItem;
    Act_Interface_Han: TAction;
    Act_HsbZikuyNehagMake: TAction;
    Act_HsbZikuyNehagCopyHeshbonit: TAction;
    N60: TMenuItem;
    N61: TMenuItem;
    N62: TMenuItem;
    Act_HsbZikuyLakPerut: TAction;
    Act_HsbZikuyLakMake: TAction;
    Act_HsbZikuyLakCopy: TAction;
    Act_HsbZikuyLakCancel: TAction;
    N63: TMenuItem;
    N64: TMenuItem;
    N65: TMenuItem;
    N66: TMenuItem;
    N67: TMenuItem;
    N68: TMenuItem;
    Act_WinTudaAtmLako: TAction;
    N70: TMenuItem;
    Act_MoDoh_RetzefTeuda: TAction;
    N71: TMenuItem;
    N72: TMenuItem;
    N73: TMenuItem;
    Act_HsbRikuz_Lak: TAction;
    N74: TMenuItem;
    Act_HsbRikuz_Nehag: TAction;
    N75: TMenuItem;
    N76: TMenuItem;
    N69: TMenuItem;
    Act_HsbZikuyLakDolar_Perut: TAction;
    Act_HsbZikuyLakDolar_Make: TAction;
    Act_HsbZikuyLakDolar_Copy: TAction;
    Act_HsbZikuyLakDolar_Cancel: TAction;
    N77: TMenuItem;
    N78: TMenuItem;
    N79: TMenuItem;
    N80: TMenuItem;
    N81: TMenuItem;
    Act_Interface_HanKabala: TAction;
    N82: TMenuItem;
    N83: TMenuItem;
    Act_HsbPerforma_Perut: TAction;
    Act_HsbPerforma_Make: TAction;
    Act_HsbPerforma_Copy: TAction;
    act_HsbPerforma_Cancel: TAction;
    N84: TMenuItem;
    N85: TMenuItem;
    N86: TMenuItem;
    N87: TMenuItem;
    N88: TMenuItem;
    Act_HsbMerukazPerut: TAction;
    Act_HsbMerukazMake: TAction;
    Act_HsbMerukazCopy: TAction;
    Act_HsbMerukazCancel: TAction;
    N89: TMenuItem;
    N90: TMenuItem;
    N91: TMenuItem;
    N92: TMenuItem;
    N93: TMenuItem;
    N94: TMenuItem;
    N95: TMenuItem;
    Act_HsbDolarCancel: TAction;
    N96: TMenuItem;
    N97: TMenuItem;
    Act_Util_DeleteMehiron: TAction;
    N98: TMenuItem;
    Act_Meholel_ReportKupa: TAction;
    N99: TMenuItem;
    Act_CrtOpenShaar: TAction;
    N100: TMenuItem;
    SpeedItem17: TSpeedItem;
    N101: TMenuItem;
    Act_Interface_HanHafada: TAction;
    N102: TMenuItem;
    Act_HsbZikuyNehagCancel: TAction;
    N103: TMenuItem;
    N104: TMenuItem;
    Act_HsbZikuyXReportNehag: TAction;
    X1: TMenuItem;
    ExeMenu1: TExeMenu;
    Act_EditExecuteMenu: TAction;
    NexecuteMenu: TMenuItem;
    N105: TMenuItem;
    N106: TMenuItem;
    Act_HsbPreformaHetekMerukaz: TAction;
    N17: TMenuItem;
    N107: TMenuItem;
    N108: TMenuItem;
    Act_Kupa_Kabla_Dolar: TAction;
    N109: TMenuItem;
    Act_Kupa_Hafkada_Dolar: TAction;
    N110: TMenuItem;
    N111: TMenuItem;
    N112: TMenuItem;
    N113: TMenuItem;
    N114: TMenuItem;
    N115: TMenuItem;
    N116: TMenuItem;
    N117: TMenuItem;
    Act_HsbPerutMizdamenLak: TAction;
    Act_HeshbonitMizdamenet: TAction;
    Act_HsbCopyHeshbonitMizdamenet: TAction;
    Act_CancelHeshbonMizdamenLak: TAction;
    Action2: TAction;
    Act_Kupa_Kabala_Nehag: TAction;
    N118: TMenuItem;
    N119: TMenuItem;
    SpeedItem18: TSpeedItem;
    Act_ElecComp_TrvPerut: TAction;
    Act_ElecComp_TrvMake: TAction;
    Act_ElecComp_TrvCopy: TAction;
    Act_ElecComp_TrvCancel: TAction;
    Act_ElecComp_DayPerut: TAction;
    Act_ElecComp_DayMake: TAction;
    Act_ElecComp_DayCopy: TAction;
    Act_ElecComp_DayCancel: TAction;
    N120: TMenuItem;
    N121: TMenuItem;
    N122: TMenuItem;
    N123: TMenuItem;
    N124: TMenuItem;
    N125: TMenuItem;
    N126: TMenuItem;
    N127: TMenuItem;
    N128: TMenuItem;
    N129: TMenuItem;
    N130: TMenuItem;
    Act_Interface_KlitaMeMishkal: TAction;
    N131: TMenuItem;
    Act_Util_TestTnua: TAction;
    N132: TMenuItem;
    Act_HsbSikumCancelLak: TAction;
    N133: TMenuItem;
    Act_HsbShowBitul: TAction;
    Act_DohLastNumbers: TAction;
    N134: TMenuItem;
    N135: TMenuItem;
    N136: TMenuItem;
    Act_CreateDiskForHashmal: TAction;
    Act_Doh_Tkinut_Tnuot: TAction;
    N137: TMenuItem;
    Act_Doh_Mehiron: TAction;
    N138: TMenuItem;
    Act_HeshbonitMasKabalaLakoach: TAction;
    Act_HeshbonitMasKabalaNehag: TAction;
    Act_HeshbonitMasKabalaDolar: TAction;
    N139: TMenuItem;
    N140: TMenuItem;
    N141: TMenuItem;
    N142: TMenuItem;
    SpeedItem19: TSpeedItem;
    N143: TMenuItem;
    N144: TMenuItem;
    N145: TMenuItem;
    N146: TMenuItem;
    N147: TMenuItem;
    Act_Mlay_In: TAction;
    Act_Mlay_out: TAction;
    Act_Mlay_Spira: TAction;
    N148: TMenuItem;
    N149: TMenuItem;
    Act_Mlay_Hazmana: TAction;
    Act_Mlay_Returns: TAction;
    N150: TMenuItem;
    N151: TMenuItem;
    Act_Kupa_Kabala_Matha: TAction;
    N152: TMenuItem;
    N153: TMenuItem;
    N154: TMenuItem;
    N155: TMenuItem;
    Act_Kupa_Hafkada_Matha: TAction;
    Act_Kupa_PrintCopyHafkadot_Mathea: TAction;
    Act_Kupa_CancelHafkada_Mathea: TAction;
    N156: TMenuItem;
    N157: TMenuItem;
    N158: TMenuItem;
    Act_HsbZikuyRehev: TAction;
    Act_HsbRikuz_Rehev: TAction;
    Act_Kupa_kabla_Han_Matha: TAction;
    Act_Kupa_kabla_HanHafkada: TAction;
    ActKupakablaHanMatha1: TMenuItem;
    ActKupakablaHanHafkada1: TMenuItem;
    N159: TMenuItem;
    Act_HesbonitMasHiuv: TAction;
    N160: TMenuItem;
    N161: TMenuItem;
    N162: TMenuItem;
    N163: TMenuItem;
    Act_HesbonitMasZikuy: TAction;
    Act_HesbonitMasMas: TAction;
    N164: TMenuItem;
    N165: TMenuItem;
    N166: TMenuItem;
    N167: TMenuItem;
    N168: TMenuItem;
    N169: TMenuItem;
    N170: TMenuItem;
    N171: TMenuItem;
    Act_HsbPerutLakBrinks: TAction;
    Act_HsbHeshbonitBrinks: TAction;
    Act_HsbCopyHeshbonitBrinks: TAction;
    Act_CancelHeshbonLakBrinks: TAction;
    Act_HsbShowBitulBrinks: TAction;
    N172: TMenuItem;
    MainMenuAsfi: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem20: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem27: TMenuItem;
    MenuItem31: TMenuItem;
    N173: TMenuItem;
    ActHesbonitMasMas1: TMenuItem;
    N174: TMenuItem;
    ActHesbonitMasHiuv2: TMenuItem;
    N175: TMenuItem;
    N176: TMenuItem;
    N177: TMenuItem;
    N178: TMenuItem;
    N179: TMenuItem;
    N180: TMenuItem;
    N181: TMenuItem;
    N182: TMenuItem;
    N183: TMenuItem;
    SpeedItem20: TSpeedItem;
    N184: TMenuItem;
    Act_HsbIska: TAction;
    ActHsbIska1: TMenuItem;
    N185: TMenuItem;
    N186: TMenuItem;
    NexecuteMenuAsfi: TMenuItem;
    N188: TMenuItem;
    N189: TMenuItem;
    N187: TMenuItem;
    Act_CrtOpenGazTuda: TAction;
    Act_HsbZikuyLakAsmctz: TAction;
    N190: TMenuItem;
    Act_HsbZikuyNehagAsmcta: TAction;
    N191: TMenuItem;
    N192: TMenuItem;
    Act_Interface_Checks: TAction;
    procedure Act_WinTudaExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Act_CrtOpenLakoExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Act_CrtOpenNehagExecute(Sender: TObject);
    procedure Act_CrtOpenRehevExecute(Sender: TObject);
    procedure Act_CrtOpenMaslulExecute(Sender: TObject);
    procedure NExitClick(Sender: TObject);
    procedure Act_CrtOpenMehironExecute(Sender: TObject);
    procedure NEditSpeedBarClick(Sender: TObject);
    procedure Act_OpenTavlaExecute(Sender: TObject);
    procedure Act_HsbOpenMakavExecute(Sender: TObject);
    procedure Act_HsbPerutLakExecute(Sender: TObject);
    procedure Act_HsbCopyHeshbonitExecute(Sender: TObject);
    procedure Act_HsbHeshbonitExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagExecute(Sender: TObject);
    procedure Act_HsbEditMnameExecute(Sender: TObject);
    procedure Act_Kupa_KabalaExecute(Sender: TObject);
    procedure Act_Hes(Sender: TObject);
    procedure Act_MeholelOpenExecute(Sender: TObject);
    procedure NDefParamsClick(Sender: TObject);
    procedure Act_MeholelReportExecute(Sender: TObject);
    procedure Act_Util_CopyMehironExecute(Sender: TObject);
    procedure Act_DefRangeExecute(Sender: TObject);
    procedure Act_CrtOpenMahzevaExecute(Sender: TObject);
    procedure Act_HsbPerutHiuvNehagExecute(Sender: TObject);
    procedure Act_HsbHeshbonitNehagExecute(Sender: TObject);
    procedure Act_HsbCopyHeshbonitNehagExecute(Sender: TObject);
    procedure Act_CancelHeshbonLakExecute(Sender: TObject);
    procedure Act_CancelHeshbonNehagExecute(Sender: TObject);
    procedure Act_HsbOpenMakavNehagExecute(Sender: TObject);
    procedure Act_Interface_SonolSapExecute(Sender: TObject);
    procedure Act_Interface_SonolPremExecute(Sender: TObject);
    procedure Act_MeholelCloseMeholelExecute(Sender: TObject);
    procedure Act_HsbOpenMakavPreformaExecute(Sender: TObject);
    procedure Act_HsbSikumLakExecute(Sender: TObject);
    procedure Act_HsbSikumCopyLakExecute(Sender: TObject);
    procedure Act_HsbSikumPerutLakExecute(Sender: TObject);
    procedure Act_HsbPreformaPerutMerukazExecute(Sender: TObject);
    procedure Act_HsbPreformaMerukazExecute(Sender: TObject);
    procedure Act_Kupa_HafkadaExecute(Sender: TObject);
    procedure Act_Kupa_PrintCopyHafkadotExecute(Sender: TObject);
    procedure Act_Crt_OpenTavla2Execute(Sender: TObject);
    procedure Act_Kupa_CancelHafkadaExecute(Sender: TObject);
    procedure Act_Util_RaisePriceInTnuaExecute(Sender: TObject);
    procedure Act_HsbPerutDolarRegularExecute(Sender: TObject);
    procedure Act_HsbDolarExecute(Sender: TObject);
    procedure Act_HsbCopyDolarExecute(Sender: TObject);
    procedure Act_Interface_HanExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagMakeExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagCopyHeshbonitExecute(Sender: TObject);
    procedure Act_HsbZikuyLakPerutExecute(Sender: TObject);
    procedure Act_HsbZikuyLakMakeExecute(Sender: TObject);
    procedure Act_HsbZikuyLakCopyExecute(Sender: TObject);
    procedure Act_HsbZikuyLakCancelExecute(Sender: TObject);
    procedure Act_WinTudaAtmLakoExecute(Sender: TObject);
    procedure Act_MoDoh_RetzefTeudaExecute(Sender: TObject);
    procedure Act_HsbRikuz_LakExecute(Sender: TObject);
    procedure Act_HsbRikuz_NehagExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_PerutExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_MakeExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_CopyExecute(Sender: TObject);
    procedure Act_HsbZikuyLakDolar_CancelExecute(Sender: TObject);
    procedure Act_Interface_HanKabalaExecute(Sender: TObject);
    procedure Act_HsbPerforma_PerutExecute(Sender: TObject);
    procedure Act_HsbPerforma_MakeExecute(Sender: TObject);
    procedure Act_HsbPerforma_CopyExecute(Sender: TObject);
    procedure act_HsbPerforma_CancelExecute(Sender: TObject);
    procedure Act_HsbMerukazPerutExecute(Sender: TObject);
    procedure Act_HsbMerukazMakeExecute(Sender: TObject);
    procedure Act_HsbMerukazCopyExecute(Sender: TObject);
    procedure Act_HsbMerukazCancelExecute(Sender: TObject);
    procedure Act_HsbDolarCancelExecute(Sender: TObject);
    procedure Act_Util_DeleteMehironExecute(Sender: TObject);
    procedure Act_Meholel_ReportKupaExecute(Sender: TObject);
    procedure Act_CrtOpenShaarExecute(Sender: TObject);
    procedure Act_Interface_HanHafadaExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagCancelExecute(Sender: TObject);
    procedure Act_HsbZikuyXReportNehagExecute(Sender: TObject);
    procedure Act_EditExecuteMenuExecute(Sender: TObject);
    procedure Act_HsbPreformaHetekMerukazExecute(Sender: TObject);
    procedure Act_Kupa_Kabla_DolarExecute(Sender: TObject);
    procedure Act_Kupa_Hafkada_DolarExecute(Sender: TObject);
    procedure Act_HsbPerutMizdamenLakExecute(Sender: TObject);
    procedure Act_HeshbonitMizdamenetExecute(Sender: TObject);
    procedure Act_HsbCopyHeshbonitMizdamenetExecute(Sender: TObject);
    procedure Act_CancelHeshbonMizdamenLakExecute(Sender: TObject);
    procedure Act_Kupa_Kabala_NehagExecute(Sender: TObject);
    procedure Act_ElecComp_TrvPerutExecute(Sender: TObject);
    procedure Act_Interface_KlitaMeMishkalExecute(Sender: TObject);
    procedure N132Click(Sender: TObject);
    procedure N124Click(Sender: TObject);
    procedure Action_Open_CallCenterExecute(Sender: TObject);
    procedure Act_Util_TestTnuaExecute(Sender: TObject);
    procedure Act_HsbShowBitulExecute(Sender: TObject);
    procedure Act_DohLastNumbersExecute(Sender: TObject);
    procedure Act_CreateDiskForHashmalExecute(Sender: TObject);
    procedure Act_Doh_Tkinut_TnuotExecute(Sender: TObject);
    procedure Act_Doh_MehironExecute(Sender: TObject);
    procedure Act_HeshbonitMasKabalaLakoachExecute(Sender: TObject);
    procedure N143Click(Sender: TObject);
    procedure Act_Mlay_InExecute(Sender: TObject);
    procedure Act_Kupa_Kabala_MathaExecute(Sender: TObject);
    procedure Act_Kupa_Hafkada_MathaExecute(Sender: TObject);
    procedure Act_Kupa_PrintCopyHafkadot_MatheaExecute(Sender: TObject);
    procedure Act_Kupa_CancelHafkada_MatheaExecute(Sender: TObject);
    procedure Act_HsbZikuyRehevExecute(Sender: TObject);
    procedure Act_HsbRikuz_RehevExecute(Sender: TObject);
    procedure Act_Kupa_kabla_Han_MathaExecute(Sender: TObject);
    procedure Act_Kupa_kabla_HanHafkadaExecute(Sender: TObject);
    procedure Act_HesbonitMasHiuvExecute(Sender: TObject);
    procedure Act_HsbPerutLakBrinksExecute(Sender: TObject);
    procedure Act_HsbHeshbonitBrinksExecute(Sender: TObject);
    procedure Act_HsbCopyHeshbonitBrinksExecute(Sender: TObject);
    procedure Act_CancelHeshbonLakBrinksExecute(Sender: TObject);
    procedure Act_HsbShowBitulBrinksExecute(Sender: TObject);
    procedure N172Click(Sender: TObject);
    procedure SpeedBar1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem31Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure Act_HsbIskaExecute(Sender: TObject);
    procedure Act_CrtOpenGazTudaExecute(Sender: TObject);
    procedure Act_HsbZikuyLakAsmctzExecute(Sender: TObject);
    procedure Act_HsbZikuyNehagAsmctaExecute(Sender: TObject);
    procedure Act_Interface_ChecksExecute(Sender: TObject);
  private
    { Private declarations }
      HGazTuda : THandle ;

  public
    { Public declarations }
    procedure EnableDisableMenues( Action : Boolean );
    procedure CrtClick(Sender: TObject);
  end;

// Dll Heshbon
{Procedure Edit_MName;External'WinHshbn.Dll';
Procedure Perot_To_Lak;External'WinHshbn.Dll';
Procedure CopyHshbonit;External'WinHshbn.Dll';
Procedure MakeHsbonit;External'WinHshbn.Dll';
Procedure Zikoy_To_Nehag;External'WinHshbn.Dll';
}


var
  Frm_MainWinTuda: TFrm_MainWinTuda;

implementation
{$R *.DFM}
Uses
    F_GlblWinTuda, Frm_WinTuda,AtmConst,AboutWinTuda,AtmRutin,
  F_WinTudaParam, F_WinTudaAtmLak, F_CallCenter, Frm_WinTudaNew;


Procedure PrintKopa(DbUserName,DbPassword:ShortString;SystemCode,SubSystemCode :integer;ShibNo :Variant);
var
   DN,DP:PChar;
   DllHandle : THandle;
   TmpProc : TSadranProc;
   TmpInt : Integer;
   TmpString : String;
Begin
     TmpString := '';
     TmpInt := 0;
     DN:=StrAlloc(Length('Modoh.dll')+1);
     DP:=StrAlloc(Length('PrintDohKopa')+1);
     Try
         StrPCopy(DN,'Modoh.dll');
         StrPCopy(DP,'PrintDohKopa');
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
           ShowMessage ('Not Found');
          @TmpProc := GetProcAddress(DllHandle,DP);
          if @TmpProc <>  nil  then
             TmpProc(TmpString,TmpString,tmpInt,tmpInt,tmpInt);
           FreeLibrary(DllHandle);
           DllHandle:=0;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;
  
  
procedure TFrm_MainWinTuda.Act_WinTudaExecute(Sender: TObject);
begin
  Act_WinTuda.Enabled:=False;
  Try
    ShowWinTuda(0);
  Finally
    Act_WinTuda.Enabled:=True;
  End;
end;

procedure TFrm_MainWinTuda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_MainWinTuda:=Nil;
     Action:=caFree;
     if FrmWinTuda<>Nil Then
     Begin
          FrmWinTuda.Free;
          FrmWinTuda:=Nil;
          //FrmWinTuda ��� ������� �������� �"� (DM,Glbl)
     End;
     if HGazTuda <> 0 then
       RunDllNoParam('GazTuda.Dll','DoneWinTuda',True ,HGazTuda);

end;

procedure TFrm_MainWinTuda.Act_CrtOpenLakoExecute(Sender: TObject);
begin
     if WinTudaSugSystem = ssMaz then  //In Case Of Mazkiroot
        OpenCrtDll(fnLakoachAtm,'')
     else
        OpenCrtDll(fnLakoach,'');
end;

procedure TFrm_MainWinTuda.EnableDisableMenues( Action : Boolean );
var
   i : Integer ;
   Tmp : TMenuItem ;
   Sl : TStringList ;
   F :TIniFile;


       function EableWinTudaMenuItem( M : TMenuItem ; Action : Boolean ; SL : TStringList): Boolean;
       var 
           i : Integer ;
       Begin
            if M.Count > 0 then
            begin
                 for i := 0 to M.Count -1 do
                     EableWinTudaMenuItem( M.Items[i] , Action , Sl);
            end;
            if Sl.IndexOf( M.Caption ) <> -1 then
               M.Visible := Action ;
            

       End;      
Begin
     Sl := TStringList.Create ;
     F:=TIniFile.Create(WinTudaCurIniFileName);
     try 
         F.ReadSection( 'TovlaMenu', Sl );
     
         for i := 0 to MainMenu1.Items.Count -1 do
         Begin
              Tmp := MainMenu1.Items[ i ] ;
              EableWinTudaMenuItem( Tmp , Action ,Sl );
         End;      

     Finally
            F.Free ;
            Sl.Free ;
     end;

End;      


procedure TFrm_MainWinTuda.FormCreate(Sender: TObject);
begin
  HGazTuda := 0 ;
  InitAtmCrt(CurAliasName,DirectoryForIniFiles);
  BuildCrtMenu(NCrtMenu,WinTudaCurIniFileName,'Crt',CrtClick);
//  if FileExists('c:\asfi.txt') then
//  EnableDisableMenues( False );
  Act_WinTudaAtmLako.Visible:=WinTudaSugSystem=ssMaz;
  Act_CrtOpenGazTuda.Visible:=WinTudaSugSystem=ssGazCompany ;
//  Action_Open_CallCenter.Visible := Act_WinTudaAtmLako.Visible;
//  N132.Visible := Act_WinTudaAtmLako.Visible; // ������ ������
  if Act_WinTudaAtmLako.Visible then
  begin
      SpeedBar1.AddItem(0,SpeedItem18);// The button that reprsents the Menu Above
      
      
  end;
     

  if WinTudaAsfiCompani then
  begin
     Self.Menu := MainMenuAsfi ; 
     SpeedItem5.Free ; // �����
     SpeedItem1.Free ; // ������
     SpeedItem3.Free ; // �������
     SpeedItem6.Free ; // ��������
     // ����� �����
     ExeMenu1.SubMenu := NexecuteMenuAsfi ;
    // SpeedBar1.AddItem(0,SpeedItem20);
    // SpeedItem19.Visible := True ;
     
  end;
   case WinTudaCompany Of
   //brinks
   3: begin
            N165.Visible := True ;
      end;

   end;
  
  ExeMenu1.IniFileName:=DirectoryForCurUser+ExeMenu1.IniFileName;
  ExeMenu1.FillMenu;

end;

procedure TFrm_MainWinTuda.FormDestroy(Sender: TObject);
begin
     DoneAtmCrt;
end;

procedure TFrm_MainWinTuda.Act_CrtOpenNehagExecute(Sender: TObject);
begin
     OpenCrtDll(fnNehag,'');
end;

procedure TFrm_MainWinTuda.Act_CrtOpenRehevExecute(Sender: TObject);
begin
     OpenCrtDll(fnRehev,'');
end;

procedure TFrm_MainWinTuda.Act_CrtOpenMaslulExecute(Sender: TObject);
begin
     OpenCrtDll(fnMaslul,'');
end;

procedure TFrm_MainWinTuda.NExitClick(Sender: TObject);
begin
     Close;
end;

procedure TFrm_MainWinTuda.Act_CrtOpenMehironExecute(Sender: TObject);
begin
     OpenCrtDll(fnMehiron,'');
end;

procedure TFrm_MainWinTuda.NEditSpeedBarClick(Sender: TObject);
begin
     SpeedBar1.Customize(0);
end;

procedure TFrm_MainWinTuda.Act_OpenTavlaExecute(Sender: TObject);
begin
     OpenCrtDll(fnTavla,'');
end;

procedure TFrm_MainWinTuda.Act_HsbOpenMakavExecute(Sender: TObject);
begin
     OpenCrtDll(fnMakav,'');
end;

procedure TFrm_MainWinTuda.Act_HsbPerutLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','Perot_To_Lak',True,H);
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(StandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyHeshbonitExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
//     RunDllNoParam('WinHshbn.Dll','CopyHshbonit',True,H);
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(StandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbHeshbonitExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','MakeHshbonit',True,H);
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(StandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','Zikuy_To_Nehag',True,H);
//     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbEditMnameExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','Edit_MName',True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_KabalaExecute(Sender: TObject);
begin
{     StrH:=DirectoryForExeFiles+'kabalot.exe';
     WinExec(PChar(StrH),SW_Restore);
}
//  RunDllProcPCharParam('Kabalot.Dll','ShowKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,WinTudaKabalaHandle);
   RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
   RunDllProcPCharParam('Kupa.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\','0'],True,WinTudaKabalaHandle);
end;

procedure TFrm_MainWinTuda.Act_Hes(Sender: TObject);
begin
     With TAboutBox.Create(Nil) Do
     Begin
          ShowModal;
          Free;
     End;

end;

procedure TFrm_MainWinTuda.Act_MeholelOpenExecute(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,WinTudaCurUserName,CurAliasName,False,WinTudaMeholelHandle);
     RunDllNoParam('Meholel.Dll','ShowMain',False,WinTudaMeholelHandle);
end;

procedure TFrm_MainWinTuda.NDefParamsClick(Sender: TObject);
begin
     With TFrm_WinTudaParams.Create(Self) Do
          ShowModal;
     // Free is done in the form onclose
end;

procedure TFrm_MainWinTuda.Act_MeholelReportExecute(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,WinTudaCurUserName,CurAliasName,False,WinTudaMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[''],False,WinTudaMeholelHandle);
end;

procedure TFrm_MainWinTuda.Act_Util_CopyMehironExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[WinTudaDBUserName,WinTudaDbPassword,CurAliasName,WinTudaSqlServerType],False,H);
     RunDllProcPCharParam('MhrnUtil.Dll','UpdateMehiron',['','',''],False,H);
     RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MainWinTuda.Act_DefRangeExecute(Sender: TObject);
begin
    OpenCrtDll(fnRange,'');
end;

procedure TFrm_MainWinTuda.Act_CrtOpenMahzevaExecute(Sender: TObject);
begin
    OpenCrtDll(fnMahzeva,'');
end;

procedure TFrm_MainWinTuda.Act_HsbPerutHiuvNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(HiyuvToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbHeshbonitNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(HiyuvToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyHeshbonitNehagExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(HiyuvToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_CancelHeshbonLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(StandardToLak)),True,H);
end;

procedure TFrm_MainWinTuda.Act_CancelHeshbonNehagExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(HiyuvToNehag)),True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbOpenMakavNehagExecute(Sender: TObject);
begin
    OpenCrtDll(fnMakavNehag,'');
end;

procedure TFrm_MainWinTuda.Act_Interface_SonolSapExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('GetMessDll','Call_Tenuot_Sonol',[CurAliasName,WinTudaDBUserName,WinTudaDBPassword,WinTudaPrivateIniFileName,DirectoryForScripts+WinTudaSqlServerType+'\'],True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_SonolPremExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('GetMessDll','Call_Prem_Nehag_Sonol',[CurAliasName,WinTudaDBUserName,WinTudaDBPassword,WinTudaCurIniFileName],True,H);
end;

procedure TFrm_MainWinTuda.CrtClick(Sender: TObject);
Begin
    OpenCrt(TComponent(Sender).Tag,'');
End;
procedure TFrm_MainWinTuda.Act_MeholelCloseMeholelExecute(Sender: TObject);
begin
  if WinTudaMeholelHandle > 0 Then
    RunDllNoParam('Meholel.Dll','CloseMeholel',True,WinTudaMeholelHandle);
end;

procedure TFrm_MainWinTuda.Act_HsbOpenMakavPreformaExecute(
  Sender: TObject);
begin
  OpenCrtDll(fnMakavPreforma,'');
end;

procedure TFrm_MainWinTuda.Act_HsbSikumLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Sicum)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbSikumCopyLakExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Sicum)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbSikumPerutLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Sicum)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPreformaPerutMerukazExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','Perut_PrephormaMerukezet',Ord(TSugHesbonit(PrephormaMerukezet)),True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPreformaMerukazExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDllNoParam('WinHshbn.Dll','DoHshbonitPrephormaMerukezet',True,H);
//  RunDll1IntParam('WinHshbn.Dll','DoHshbonitPrephormaMerukezet',Ord(TSugHesbonit(PrephormaMerukezet)),True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_HafkadaExecute(Sender: TObject);
begin
  RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
  RunDllProcPCharParam('Kupa.Dll','Hafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,'0'],False,WinTudaKupaHandle);
end;

procedure TFrm_MainWinTuda.Act_Kupa_PrintCopyHafkadotExecute(
  Sender: TObject);
Var
  HafNum,YYear,Daf:String;
  IntH :LongInt;
  F: TIniFile;
begin
  HafNum:=InputBox('����� ���� �����','���� �����','');
  Try
    IntH:=-1;
    IntH:=StrToInt(HafNum);
  Except
  End;
    if IntH>-1 Then
    Begin
      YYear:=InputBox('����� ���� �����','��� �����',IntToStr(WinTudaYehusYear));
      Try
        IntH:=-1;
        IntH:=StrToInt(YYear);
      Except
      End;

    if IntH>-1 Then
    Begin
      F:=TIniFile.Create(DirectoryForExeFiles+'AtmCfg.ini');//WinTudaCurIniFileName);    
      WinTudaKupaDaf := F.ReadInteger('Hafkada','TopLines',10);            
      Daf :=InputBox('����� ���� �����',
                 '������ �������',
                 IntToStr(WinTudaKupaDaf));
      Try
        IntH:=-1;
        
        IntH:=StrToInt(Daf);
        WinTudaKupaDaf := IntH;        
        F.WriteInteger('Hafkada','TopLines',WinTudaKupaDaf);
        F.Free;
      Except
      End;
    end;

      if IntH>-1 Then
      Begin
        RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
        RunDllProcPCharParam('Kupa.Dll','PrintCopyHafkada',
                             [WinTudaDBUserName,
                              WinTudaDBPassword,
                              CurAliasName,
                              HafNum,
                              YYear,
                              'False',
                              daf,
                              '1'],False,WinTudaKupaHandle);
      End;
    End;
end;

procedure TFrm_MainWinTuda.Act_Crt_OpenTavla2Execute(Sender: TObject);
begin
  OpenCrtDll(fnTavla2,'');
end;

procedure TFrm_MainWinTuda.Act_Kupa_CancelHafkadaExecute(Sender: TObject);
Var
  HafNum,YYear:String;
  IntH :LongInt;
begin
  HafNum:=InputBox('����� �����','���� �����','');
  Try
    IntH:=-1;
    IntH:=StrToInt(HafNum);
  Except
  End;
  if IntH>-1 Then
  Begin
    if IntH>-1 Then
    Begin
      YYear:=InputBox('����� ���� �����','��� �����',IntToStr(WinTudaYehusYear));
      Try
        IntH:=-1;
        IntH:=StrToInt(YYear);
      Except
      End;
      if IntH>-1 Then
      Begin
        RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
        RunDllProcPCharParam('Kupa.Dll','CancelHafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,HafNum,YYear],False,WinTudaKupaHandle);
      End;
    End;
  End;
end;

procedure TFrm_MainWinTuda.Act_Util_RaisePriceInTnuaExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[WinTudaDBUserName,WinTudaDbPassword,CurAliasName,WinTudaSqlServerType],False,H);
     RunDllProcPCharParam('MhrnUtil.Dll','DoUPdatePriceInTnua',[DirectoryForScripts+WinTudaSqlServerType+'\'],False,H);
     RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerutDolarRegularExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbDolarExecute(Sender: TObject);
Var
   H :THandle;
begin
   H:=0;
   RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyDolarExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_HanExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Han.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Han.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Han.Dll','DoneAtmHan',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','Make_Zikuy_To_Nehag',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagCopyHeshbonitExecute(
  Sender: TObject);
Var
   H :THandle;
begin
   H:=0;
   RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyToNehag)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakPerutExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyShekel)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(ZikuyShekel)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakCopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyShekel)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyShekel)),True,H);
end;

procedure TFrm_MainWinTuda.Act_WinTudaAtmLakoExecute(Sender: TObject);
begin
  if Frm_AtmShibLak=Nil Then
    Frm_AtmShibLak:=TFrm_AtmShibLak.Create(Nil);
  Frm_AtmShibLak.Show;
end;

procedure TFrm_MainWinTuda.Act_MoDoh_RetzefTeudaExecute(Sender: TObject);
begin
  RunDllFunc1StringParam('MoDoh.Dll','ShowRetzef','ShibTuda');
end;

procedure TFrm_MainWinTuda.Act_HsbRikuz_LakExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','RikuzForLakNehag',0,-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbRikuz_NehagExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','RikuzForLakNehag',1,1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_PerutExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyDolar)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_MakeExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(ZikuyDolar)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_CopyExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyDolar)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakDolar_CancelExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyDolar)),True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_HanKabalaExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Kab.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Kab.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Kab.Dll','DoneAtmHan',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerforma_PerutExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Prephorma)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerforma_MakeExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Prephorma)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerforma_CopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Prephorma)),-1,True,H);
end;

procedure TFrm_MainWinTuda.act_HsbPerforma_CancelExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(Prephorma)),True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazPerutExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazCopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbMerukazCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(Merukezet)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbDolarCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(DolaritStandardToLak)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_Util_DeleteMehironExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[WinTudaDBUserName,WinTudaDbPassword,CurAliasName,WinTudaSqlServerType],False,H);
  RunDllNoParam('MhrnUtil.Dll','DelMehiron',False,H);
  RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MainWinTuda.Act_Meholel_ReportKupaExecute(Sender: TObject);
Var
   H :THandle;
   A : variant ;
begin
  H:=0;
//  RunDllProcPCharParam('Kopa_Report.Dll','ReportKopa_Init',[CurAliasName,WinTudaDBUserName,WinTudaDbPassword,WinTudaPrivateIniFileName,DirectoryForScripts+WinTudaSqlServerType+'\'],False,H);
//  RunDllNoParam('Kopa_Report.Dll','ReportKopa_Done',True,H);
//    RunDllProcPCharParam('MoDoh.Dll','PrintDohKopa',[WinTudaDBUserName,WinTudaDbPassword,IntToStr(WinTudaSugSystem),IntToStr(WinTudaSubSystemCode),0 
//    RunDllProcPCharParam('MoDoh.Dll','PrintDohKopa',['','','0','0','0'],True,H);

 // New Doch Kopa 04/11/02 By rani
      PrintKopa( WinTudaDBUserName,WinTudaDbPassword ,0,0, A );
end;

procedure TFrm_MainWinTuda.Act_CrtOpenShaarExecute(Sender: TObject);
begin
  OpenCrtDll(fnShaar,'');
end;

procedure TFrm_MainWinTuda.Act_Interface_HanHafadaExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Haf.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Haf.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Haf.Dll','DoneAtmHan',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyToNehag)),True,H);
End;
procedure TFrm_MainWinTuda.Act_HsbZikuyXReportNehagExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','Perut_PrephormaMerukezet',Ord(TSugHesbonit(PrephormaMerukezetToNehag)),True,H);
end;

procedure TFrm_MainWinTuda.Act_EditExecuteMenuExecute(Sender: TObject);
begin
  exeMenu1.EditExeList;
end;

procedure TFrm_MainWinTuda.Act_HsbPreformaHetekMerukazExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDllNoParam('WinHshbn.Dll','DoHetekHshbonitPrephormaMerukezet',True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_Kabla_DolarExecute(Sender: TObject);
begin
   RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
   RunDllProcPCharParam('Kupa.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\','1'],True,WinTudaKabalaHandle);
end;

procedure TFrm_MainWinTuda.Act_Kupa_Hafkada_DolarExecute(Sender: TObject);
begin
  RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
  RunDllProcPCharParam('Kupa.Dll','Hafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,'2'],False,WinTudaKupaHandle);
end;

procedure TFrm_MainWinTuda.Act_HsbPerutMizdamenLakExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','Perot_To_Lak',True,H);
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(MasMizdamen)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HeshbonitMizdamenetExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','MakeHshbonit',True,H);
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(MasMizdamen)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbCopyHeshbonitMizdamenetExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
//     RunDllNoParam('WinHshbn.Dll','CopyHshbonit',True,H);
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(MasMizdamen)),-1,True,H);
end;

procedure TFrm_MainWinTuda.Act_CancelHeshbonMizdamenLakExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(MasMizdamen)),True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_Kabala_NehagExecute(Sender: TObject);
begin
   RunDllProcPCharParam('Kupa.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
   RunDllProcPCharParam('Kupa.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\','10'],True,WinTudaKabalaHandle);
end;

procedure TFrm_MainWinTuda.Act_ElecComp_TrvPerutExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','ShowHevratHashmal',TAction(Sender).Tag, TAction(Sender).HelpContext ,True,H);
end;

procedure TFrm_MainWinTuda.Act_Interface_KlitaMeMishkalExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('GetMessDll','Call_Tenuot_Mishkal',[CurAliasName,WinTudaDBUserName,WinTudaDBPassword,WinTudaPrivateIniFileName,DirectoryForScripts+WinTudaSqlServerType+'\'],True,H);

end;

procedure TFrm_MainWinTuda.N132Click(Sender: TObject);
begin
     OpenCrt(fnTormim, '');
end;

procedure TFrm_MainWinTuda.N124Click(Sender: TObject);
var
   H: THandle;
begin
  H:=0;
  RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(Hashmal)),True,H);

end;

procedure TFrm_MainWinTuda.Action_Open_CallCenterExecute(Sender: TObject);
begin
  if Frm_CallCenter=Nil Then
    Frm_CallCenter:=TFrm_CallCenter.Create(Nil);
  Frm_CallCenter.Show;
end;

procedure TFrm_MainWinTuda.Act_Util_TestTnuaExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','DohTestShib',True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbShowBitulExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','ShowBitulHshbonit',Ord(TSugHesbonit(StandardToLak{ZikuyShekel})),True,H);
end;

procedure TFrm_MainWinTuda.Act_DohLastNumbersExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','DohLastNumbers',True,H);
end;

procedure TFrm_MainWinTuda.Act_CreateDiskForHashmalExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','DiskForHevratHashmal',True,H);
end;

procedure TFrm_MainWinTuda.Act_Doh_Tkinut_TnuotExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','DohTestShib',True,H);
    
end;

procedure TFrm_MainWinTuda.Act_Doh_MehironExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;                         
     RunDllNoParam('MODOH.Dll','DohMehiron',True,H);   
end;

procedure TFrm_MainWinTuda.Act_HeshbonitMasKabalaLakoachExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;                         
     RunDllProcPCharParam('MasKabala.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
     RunDllProcPCharParam('MasKabala.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\',IntToStr((Sender as TAction).Tag)],True, H);

end;

procedure TFrm_MainWinTuda.N143Click(Sender: TObject);
begin
     //OpenCrtDll( fnMosad ,'');
     //OpenCrtDll(fnMusa1,'');
    OpenCrtDll(fnPritim,'');
   
   {Test} //OpenCrtDll(fnStation,'');
//          OpenCrtDll(fnMusa1 ,'');
   
   
{     if FormWinTudaNew =Nil Then
        FormWinTudaNew :=TFormWinTudaNew.Create(Nil);
     FormWinTudaNew.Show;}
end;

procedure TFrm_MainWinTuda.Act_Mlay_InExecute(Sender: TObject);
var
   H : THandle ;
begin
     H:=0;                         
     if WinTudaMelayReg then
     begin
          RunDllProcPCharParam('Mlay.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
          RunDllProcPCharParam('Mlay.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\',IntToStr((Sender as TAction).Tag)],True,H);
     end
     else
         showmessage('���� ���� ���� ������ ����� ����');

end;

procedure TFrm_MainWinTuda.Act_Kupa_Kabala_MathaExecute(Sender: TObject);
var
   H : THandle ;
begin
     H := 0 ;
   RunDllProcPCharParam('KupaM.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
   RunDllProcPCharParam('KupaM.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\','1'],True,H);
end;

procedure TFrm_MainWinTuda.Act_Kupa_Hafkada_MathaExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
//  RunDllProcPCharParam('KupaM.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,WinTudaKabalaHandle);
  RunDllProcPCharParam('KupaM.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
//  RunDllProcPCharParam('KupaM.Dll','Hafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,'2'],False,WinTudaKupaHandle);
  RunDllProcPCharParam('KupaM.Dll','Hafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,'2'],False,H);

end;

procedure TFrm_MainWinTuda.Act_Kupa_PrintCopyHafkadot_MatheaExecute(
  Sender: TObject);
Var
  HafNum,YYear,Daf:String;
  IntH :LongInt;
  F: TIniFile;
  H  : THandle ;
begin
     H := 0 ;
  HafNum:=InputBox('����� ���� �����','���� �����','');
  Try
    IntH:=-1;
    IntH:=StrToInt(HafNum);
  Except
  End;
    if IntH>-1 Then
    Begin
      YYear:=InputBox('����� ���� �����','��� �����',IntToStr(WinTudaYehusYear));
      Try
        IntH:=-1;
        IntH:=StrToInt(YYear);
      Except
      End;

    if IntH>-1 Then
    Begin
      F:=TIniFile.Create(DirectoryForExeFiles+'AtmCfg.ini');//WinTudaCurIniFileName);    
      WinTudaKupaDaf := F.ReadInteger('Hafkada','TopLines',10);            
      Daf :=InputBox('����� ���� �����',
                 '������ �������',
                 IntToStr(WinTudaKupaDaf));
      Try
        IntH:=-1;
        
        IntH:=StrToInt(Daf);
        WinTudaKupaDaf := IntH;        
        F.WriteInteger('Hafkada','TopLines',WinTudaKupaDaf);
        F.Free;
      Except
      End;
    end;

      if IntH>-1 Then
      Begin
        RunDllProcPCharParam('KupaM.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
        RunDllProcPCharParam('KupaM.Dll','PrintCopyHafkada',
                             [WinTudaDBUserName,
                              WinTudaDBPassword,
                              CurAliasName,
                              HafNum,
                              YYear,
                              'False',
                              daf,
                              '1'],False,H);
      End;
    End;
end;



procedure TFrm_MainWinTuda.Act_Kupa_CancelHafkada_MatheaExecute(
  Sender: TObject);
Var
  HafNum,YYear:String;
  IntH :LongInt;
  H : THandle ;
begin
     H := 0 ;
  HafNum:=InputBox('����� �����','���� �����','');
  Try
    IntH:=-1;
    IntH:=StrToInt(HafNum);
  Except
  End;
  if IntH>-1 Then
  Begin
    if IntH>-1 Then
    Begin
      YYear:=InputBox('����� ���� �����','��� �����',IntToStr(WinTudaYehusYear));
      Try
        IntH:=-1;
        IntH:=StrToInt(YYear);
      Except
      End;
      if IntH>-1 Then
      Begin
        RunDllProcPCharParam('KupaM.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
        RunDllProcPCharParam('KupaM.Dll','CancelHafkada',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,HafNum,YYear],False,H);
      End;
    End;
  End;
end;

procedure TFrm_MainWinTuda.Act_HsbZikuyRehevExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyToRehev)),-1,True,H);
     
end;

procedure TFrm_MainWinTuda.Act_HsbRikuz_RehevExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','RikuzForLakNehag',2,1,True,H);

end;

procedure TFrm_MainWinTuda.Act_Kupa_kabla_Han_MathaExecute(
  Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2KabM.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2KabM.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2KabM.Dll','DoneAtmHan',True,H);

end;

procedure TFrm_MainWinTuda.Act_Kupa_kabla_HanHafkadaExecute(
  Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2HafM.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2HafM.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2HafM.Dll','DoneAtmHan',True,H);

end;

procedure TFrm_MainWinTuda.Act_HesbonitMasHiuvExecute(Sender: TObject);
var
   H : THandle ;
begin
     H := 0 ;
   RunDllProcPCharParam('HesZikuy.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
   RunDllProcPCharParam('HesZikuy.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\',IntToStr((Sender as TAction).Tag)],True,H);
end;

procedure TFrm_MainWinTuda.Act_HsbPerutLakBrinksExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','Perot_To_Lak',True,H);
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Brinks)),-1,True,H);

end;

procedure TFrm_MainWinTuda.Act_HsbHeshbonitBrinksExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
//     RunDllNoParam('WinHshbn.Dll','MakeHshbonit',True,H);
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(Brinks)),-1,True,H);

end;

procedure TFrm_MainWinTuda.Act_HsbCopyHeshbonitBrinksExecute(
  Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
//     RunDllNoParam('WinHshbn.Dll','CopyHshbonit',True,H);
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(Brinks)),-1,True,H);

end;

procedure TFrm_MainWinTuda.Act_CancelHeshbonLakBrinksExecute(
  Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(StandardToLak)),True,H);

end;

procedure TFrm_MainWinTuda.Act_HsbShowBitulBrinksExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllNoParam('WinHshbn.Dll','ShowBitulHshbonit',True,H);
end;

procedure TFrm_MainWinTuda.N172Click(Sender: TObject);
begin
     OpenCrtDll(fnCompCalendar,'');
end;


procedure TFrm_MainWinTuda.SpeedBar1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if ((ssShift in Shift) and ( ssCtrl in Shift ) and 
        ( x > SpeedBar1.Width - 20 ))then
         SpeedBar1.Customize(0);
    

end;

procedure TFrm_MainWinTuda.MenuItem8Click(Sender: TObject);
begin
     OpenCrtDll(fnPritim,'');

end;

procedure TFrm_MainWinTuda.MenuItem31Click(Sender: TObject);
begin
     With TFrm_WinTudaParams.Create(Self) Do
          ShowModal;

end;

procedure TFrm_MainWinTuda.MenuItem3Click(Sender: TObject);
begin
     Close;
end;

procedure TFrm_MainWinTuda.Act_HsbIskaExecute(Sender: TObject);
var
   H : THandle ;
begin
   H := 0 ;
   RunDllProcPCharParam('HesEska.Dll','InitKupaDll',[WinTudaCurIniFileName,DirectoryForScripts],False,H);
   RunDllProcPCharParam('HesEska.Dll','DoKabala',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName,DirectoryForScripts+WinTudaSqlServerType+'\',IntToStr((Sender as TAction).Tag)],True,H);
   
end;

procedure TFrm_MainWinTuda.Act_CrtOpenGazTudaExecute(Sender: TObject);
begin
     //if HGazTuda = 0 then
     begin
          RunDllProcPCharParam('GazTuda.Dll','InitGazWinTuda',[ CurAliasName,DirectoryForScripts , WinTudaDBUserName , WinTudaDBPassword , WinTudaCurUserName ],False ,HGazTuda);
          RunDllProcPCharParam('GazTuda.Dll','OpenWinTuda',['0'],False,HGazTuda);
     End
     //else
     //    RunDllProcPCharParam('GazTuda.Dll','ShowGazTuda',['0'],False,HGazTuda);


end;

procedure TFrm_MainWinTuda.Act_HsbZikuyLakAsmctzExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','ShowBitulHshbonit',Ord(TSugHesbonit(ZikuyShekel)),True,H);

end;

procedure TFrm_MainWinTuda.Act_HsbZikuyNehagAsmctaExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','ShowBitulHshbonit',Ord(TSugHesbonit(ZikuyToNehag)),True,H);

end;

procedure TFrm_MainWinTuda.Act_Interface_ChecksExecute(Sender: TObject);
Var
  H :THandle;
begin
  H :=0;
  RunDllProcPCharParam('Atm2Sap.Dll','InitAtmHan',[WinTudaDBUserName,WinTudaDBPassword,CurAliasName],False,H);
  RunDllNoParam('Atm2Sap.Dll','ShowAtmHan',False,H);
  RunDllNoParam('Atm2Sap.Dll','DoneAtmHan',True,H);

end;

end.
       

unit F_PicNehag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, AtmAdvTable, ExtCtrls;

type
  TFrm_PictureNehag = class(TForm)
    Image1: TImage;
    Tbl_Nehag: TAtmTable;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    CurrentNehagFile :String;
  public
    { Public declarations }
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_PictureNehag: TFrm_PictureNehag;

implementation
Uses DmAtmcrt;

{$R *.DFM}

Procedure TFrm_PictureNehag.ShowForm(FormParams:ShortString);
Begin
     if (FormParams<>'') And Tbl_Nehag.FindKey([FormParams]) Then
     Begin
          CurrentNehagFile:=Tbl_Nehag.FieldByName('Maslul_Bmp').AsString;
          Caption:=Tbl_Nehag.FieldByName('Shem_Nehag').AsString;
          Show;
     End
     Else
     Begin
          Free;
          Frm_PictureNehag:=Nil;
     End;
End;
procedure TFrm_PictureNehag.FormCreate(Sender: TObject);
begin
     Tbl_Nehag.Open;
end;

procedure TFrm_PictureNehag.FormDestroy(Sender: TObject);
begin
     Tbl_Nehag.Close;
end;

procedure TFrm_PictureNehag.FormShow(Sender: TObject);
begin
     Image1.Picture.LoadFromFile(CurrentNehagFile);
     Self.Width:=(Image1.Canvas.ClipRect.Right-Image1.Canvas.ClipRect.Left)+5;
     Self.Height:=(Image1.Canvas.ClipRect.Bottom-Image1.Canvas.ClipRect.Top)+5;
end;

procedure TFrm_PictureNehag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_PictureNehag:=Nil;
     Action:=caFree;
end;

end.

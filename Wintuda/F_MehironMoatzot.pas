unit F_MehironMoatzot;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, Mask, ExtCtrls,
  ToolWin, ComCtrls, AtmComp, ToolEdit, RXDBCtrl, AtmDBDateEdit, AtmListDlg,
  AtmAdvTable, Scale250, AdvSearch, AtmLookCombo, AppEvent,IniFiles,
  Placemnt, Menus, DynamicControls, RxMemDS;

type
  TFrm_MehironMoatzot = class(TForm)
    Tbl_MehironChildSUG_HIUV: TIntegerField;
    Tbl_MehironChildSUG_SHERUT: TIntegerField;
    Tbl_MehironChildTEUR: TStringField;
    Tbl_MehironChildYEHIDAT_MIDA: TIntegerField;
    Tbl_MehironChildFROM_VALUE: TBCDField;
    Tbl_MehironChildTO_VALUE: TBCDField;
    Tbl_MehironChildPRICE1: TBCDField;
    Tbl_MehironChildPRICE2: TBCDField;
    Tbl_MehironChildPRICE3: TBCDField;
    Tbl_MehironChildPRICE4: TBCDField;
    DS_Mehiron: TDataSource;
    Tbl_Mehiron: TAtmTable;
    Tbl_MehironChild: TAtmTable;
    DS_MehironChild: TDataSource;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    Tbl_MehironChildRECORD_NUM: TAutoIncField;
    Tbl_MehironChildMEHIRON_LINK_KEY: TIntegerField;
    Tbl_MehironChildCODE_MEHIRON: TIntegerField;
    Tbl_MehironChildSTRING1: TStringField;
    Tbl_MehironChildSTRING2: TStringField;
    Tbl_MehironChildSTRING3: TStringField;
    Tbl_MehironChildSTRING4: TStringField;
    Tbl_MehironChildSTRING5: TStringField;
    Tbl_MehironChildBCD1: TBCDField;
    Tbl_MehironChildBCD2: TBCDField;
    Tbl_MehironChildBCD3: TBCDField;
    Tbl_MehironChildBCD4: TBCDField;
    Tbl_MehironChildBCD5: TBCDField;
    Tbl_MehironChildINTEGER1: TIntegerField;
    Tbl_MehironChildINTEGER2: TIntegerField;
    Tbl_MehironChildINTEGER3: TIntegerField;
    Tbl_MehironChildINTEGER4: TIntegerField;
    Tbl_MehironChildINTEGER5: TIntegerField;
    Tbl_MehironChildSUG_KAMUT_FIELD_NAME: TStringField;
    ToolButton1: TToolButton;
    TBtn_OpenSearch: TToolButton;
    AtmListDlg_TnuaFields: TAtmListDlg;
    StatusBar1: TStatusBar;
    Tbl_MehironChildSUG_SHERUT_IN_TNUA: TIntegerField;
    Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME: TStringField;
    Qry1: TQuery;
    DS_Qry1: TDataSource;
    PageControl1: TPageControl;
    TS_Advance: TTabSheet;
    Panel2: TPanel;
    Lbl_CodeMehiron: TLabel;
    Lbl_SugMehiron: TLabel;
    Lbl_ShemMehiron: TLabel;
    Lbl_FromDate: TLabel;
    Lbl_ToDate: TLabel;
    Lbl_SugHiuv: TLabel;
    Lbl_CodeSherut: TLabel;
    EditCODE_MEHIRON: TAtmDbHEdit;
    EditSUG_MEHIRON: TAtmDbHEdit;
    EditSHEM_MEHIRON: TAtmDbHEdit;
    AtmDBDateEdit1: TAtmDBDateEdit;
    AtmDBDateEdit2: TAtmDBDateEdit;
    RG_SugKishur: TDBRadioGroup;
    EG_SugHavaraLeTnua: TDBRadioGroup;
    AtmDbHEdit_SugHiuv: TAtmDbHEdit;
    AtmDbHEdit_SugSherut: TAtmDbHEdit;
    Panel3: TPanel;
    RxDBGrid_MehirChild: TRxDBGrid;
    TS_QryMehiron: TTabSheet;
    RxDBGrid_Qry: TRxDBGrid;
    Scale1: TScale;
    AtmDbComboBox_SugHiuv: TAtmDbComboBox;
    Tbl_Maslul: TTable;
    DBText1: TDBText;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    AtmDbComboBox_SugMehiron: TAtmDbComboBox;
    AppEvents1: TAppEvents;
    Tbl_MehironChildLookYehidatMida: TStringField;
    TBtn_NewMehiron: TToolButton;
    TS_Simple: TTabSheet;
    Panel1: TPanel;
    Lbl_SimpCodeMehiron: TLabel;
    Lbl_SimpSugMehiron: TLabel;
    Lbl_SimpShemMehiron: TLabel;
    Lbl_SimpFromDate: TLabel;
    Lbl_SimpToDate: TLabel;
    Lbl_SimpSugHiuv: TLabel;
    Lbl_SimpSugSherut: TLabel;
    DBText_SimpTeurSherut: TDBText;
    AtmDbHEdit_SimpCodeMehiron: TAtmDbHEdit;
    AtmDbHEdit_SimpSugMehiron: TAtmDbHEdit;
    AtmDbHEdit_SimpShemMehiron: TAtmDbHEdit;
    AtmDBDateEdit_SimpFomDate: TAtmDBDateEdit;
    AtmDBDateEdit_SimpToDate: TAtmDBDateEdit;
    AtmDbHEdit_SimpSugHiuv: TAtmDbHEdit;
    AtmDbHEdit_SimpleSugSherut: TAtmDbHEdit;
    AtmDbComboBox_SugHiuv2: TAtmDbComboBox;
    AtmDbComboBox_SugMehiron2: TAtmDbComboBox;
    Panel4: TPanel;
    Lbl_SimpPrice1: TLabel;
    Lbl_SimpPrice2: TLabel;
    Lbl_SimpPrice3: TLabel;
    Lbl_SimpPrice4: TLabel;
    DBEdit_SimpPrice1: TAtmDbHEdit;
    DBEdit_SimpPrice2: TAtmDbHEdit;
    DBEdit_SimpPrice3: TAtmDbHEdit;
    DBEdit_SimpPrice4: TAtmDbHEdit;
    Tbl_MehironCODE_MEHIRON: TIntegerField;
    Tbl_MehironMEHIRON_LINK_KEY: TIntegerField;
    Tbl_MehironRECORD_NUM: TAutoIncField;
    Tbl_MehironSUG_MEHIRON: TIntegerField;
    Tbl_MehironSUG_HIUV: TIntegerField;
    Tbl_MehironSUG_SHERUT: TIntegerField;
    Tbl_MehironSHEM_MEHIRON: TStringField;
    Tbl_MehironFROM_DATE: TDateTimeField;
    Tbl_MehironTO_DATE: TDateTimeField;
    Tbl_MehironSUG_LINK: TIntegerField;
    Tbl_MehironSUG_HAVARA_TNUA: TIntegerField;
    Tbl_MehironDISCOUNT_PERCENT: TBCDField;
    Tbl_MehironSUG_DISCOUNT: TIntegerField;
    Tbl_MehironSTRING_1: TStringField;
    Tbl_MehironSTRING_2: TStringField;
    Tbl_MehironSTRING_3: TStringField;
    Tbl_MehironSTRING_4: TStringField;
    Tbl_MehironSTRING_5: TStringField;
    Tbl_MehironBCD_1: TBCDField;
    Tbl_MehironBCD_2: TBCDField;
    Tbl_MehironBCD_3: TBCDField;
    Tbl_MehironBCD_4: TBCDField;
    Tbl_MehironBCD_5: TBCDField;
    Tbl_MehironINTEGER_1: TIntegerField;
    Tbl_MehironINTEGER_2: TIntegerField;
    Tbl_MehironINTEGER_3: TIntegerField;
    Tbl_MehironINTEGER_4: TIntegerField;
    Tbl_MehironINTEGER_5: TIntegerField;
    Tbl_MehironLookSugHiuv: TStringField;
    Tbl_MehironLookSugMehiron: TStringField;
    Tbl_MehironLookMaslulName: TStringField;
    ToolButton2: TToolButton;
    FormStorage_Mehiron: TFormStorage;
    TBtn_PrintGrid: TToolButton;
    AtmAdvSearch_Mehiron: TAtmAdvSearch;
    Lbl_SimpYeMida: TLabel;
    AtmDbHEdit_Simple_YehidatMida: TAtmDbHEdit;
    AtmDbComboBox_Simple_YeMida: TAtmDbComboBox;
    DynamicControls1: TDynamicControls;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Tbl_MehironChildSUG_HISHUV: TIntegerField;
    Tbl_MehironChildSUG_BHIRA: TIntegerField;
    Tbl_MehironChildRANGE_FIELD_NAME: TStringField;
    Tbl_MehironChildRANGE_FIELD_DISPLAY_NAME: TStringField;
    Tbl_MehironChildCALC_GROUP: TIntegerField;
    Tbl_MehironChildTeurSugHishuv: TStringField;
    AtmListDlg_SugHishuv: TAtmListDlg;
    Tbl_MehironChildLookSugBhira: TStringField;
    AtmListDlg_SugBhira: TAtmListDlg;
    TBtn_Report: TToolButton;
    TBtn_CopyMehiron: TToolButton;
    TBtn_DeleteFullMehiron: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Tbl_MehironAfterInsert(DataSet: TDataSet);
    procedure Tbl_MehironAfterPost(DataSet: TDataSet);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MehironChildAfterPost(DataSet: TDataSet);
    procedure DBGrid1Enter(Sender: TObject);
    procedure Tbl_MehironChildBeforePost(DataSet: TDataSet);
    procedure DBGrid1Exit(Sender: TObject);
    procedure DBGrid1EditButtonClick(Sender: TObject);
    procedure Tbl_MehironBeforePost(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure RxDBGrid_QryGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure DS_MehironStateChange(Sender: TObject);
    procedure Tbl_MehironChildSUG_SHERUT_IN_TNUAValidate(Sender: TField);
    procedure RxDBGrid_MehirChildColEnter(Sender: TObject);
    procedure TBtn_OpenSearchClick(Sender: TObject);
    procedure AppEvents1ActiveControlChange(Sender: TObject);
    procedure TBtn_NewMehironClick(Sender: TObject);
    procedure Tbl_MehironLookMaslulNameValidate(Sender: TField);
    procedure Lbl_CodeSherutDblClick(Sender: TObject);
    procedure EditCODE_MEHIRONExit(Sender: TObject);
    procedure Panel4Enter(Sender: TObject);
    procedure Panel4Exit(Sender: TObject);
    procedure TBtn_PrintGridClick(Sender: TObject);
    procedure EditSUG_MEHIRONBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmDbHEdit_SugHiuvBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmDbHEdit_Simple_YehidatMidaBeforeExecuteSearch(
      Sender: TObject; var ContinueExecute: Boolean);
    procedure Tbl_MehironChildAfterInsert(DataSet: TDataSet);
    procedure RxDBGrid_QryDblClick(Sender: TObject);
    procedure RxDBGrid_MehirChildKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure TBtn_CopyMehironClick(Sender: TObject);
    procedure TBtn_DeleteFullMehironClick(Sender: TObject);
    procedure Tbl_MehironFROM_DATEValidate(Sender: TField);
    procedure DS_MehironChildStateChange(Sender: TObject);
  private
    { private declarations }
    LastHiuv,LastSherut :LongInt;
    CurBackground :TColor;
    IniFileName:String;
    CodeKM,CodeHour,CodeStudent,CodeFix :LongInt;
    procedure ReadFixCodes;
  public
    { public declarations }
    Procedure ShowForm(FormParams:ShortString);
    Procedure ChangeDSForNavigator(TheNavi :TDBNavigator);
    Procedure BuildLookupList;
  end;

var
  Frm_MehironMoatzot: TFrm_MehironMoatzot;

implementation
Uses DMAtmCrt,AtmConst,AtmRutin,Crt_Glbl;
{$R *.DFM}

Const
     PnlNum_DSState=0;
     
procedure TFrm_MehironMoatzot.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     BuildLookupList;

     DynamicControls1.Execute;
     Scale1.DoScaleNow;

     PageControl1.ActivePage:=Ts_Advance;
     DS_Mehiron.OnStateChange:=Nil;  //�� �����
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
          Begin
              try
               (Components[I] As TTable).Active := True;
              except on e:exception do
                 showMessage (E.Message)
              end;
          End;
         if Components[i] is TAtmAdvSearch Then
            TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
  Finally
         Screen.Cursor:=crDefault;
         DS_Mehiron.OnStateChange:=DS_MehironStateChange;
  End;
end;

procedure TFrm_MehironMoatzot.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_MehironMoatzot.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_MehironMoatzot:=Nil;
     Action:=caFree;
end;

procedure TFrm_MehironMoatzot.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var
   TheDS :TDataSet;
begin
     if Not Visible Then Exit;
     if (Key=VK_F11) Then
        TheDS:=DS_Mehiron.DataSet
     Else
         TheDs:=GetDataSetFromTControl(ActiveControl);
     if TheDs<>Nil Then
        KeyboardManageForTableAction(ActiveControl,Key,Shift,TAtmTable(TheDs))
     Else
        KeyboardManageForTableAction(ActiveControl,Key,Shift,Tbl_Mehiron);
end;

procedure TFrm_MehironMoatzot.Tbl_MehironAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(PageControl1.ActivePage).SetFocus;
end;

procedure TFrm_MehironMoatzot.Tbl_MehironAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     //����� ��� ����� ��� ���
     if Tbl_MehironMEHIRON_LINK_KEY.AsInteger=0 Then
     Begin
         Tbl_Mehiron.AfterPost:=Nil;
         Try
             Tbl_Mehiron.Refresh;
             Tbl_Mehiron.Edit;
             Tbl_MehironMEHIRON_LINK_KEY.AsInteger:=Tbl_MehironCODE_MEHIRON.AsInteger * 10000 +
                                                    Tbl_MehironRECORD_NUM.AsInteger;
             Tbl_Mehiron.Post;
         Finally
                Tbl_Mehiron.AfterPost:=Tbl_MehironAfterPost;
         End;
     End;
end;

procedure TFrm_MehironMoatzot.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Not Visible Then Exit;
     ChangeDSForNavigator(TDBNavigator(Sender));

     Case Button Of
        nbPost,nbInsert: DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
        nbRefresh      : BuildLookupList;
     End; //Case
end;

Procedure TFrm_MehironMoatzot.ChangeDSForNavigator(TheNavi :TDBNavigator);
Var
   TheDS :TDataSource;
begin
     if Not Visible Then Exit;
     TheDs:=GetDataSourceFromTComponent(ActiveControl);
     if (TheDs<>Nil) And (TheNavi.DataSource<>TheDs) Then
        TheNavi.DataSource:=TheDs;
     TBtn_NewMehiron.Enabled:=TheNavi.DataSource.DataSet Is TTable;
End;

procedure TFrm_MehironMoatzot.Tbl_MehironChildAfterPost(DataSet: TDataSet);
begin
     if (Tbl_MehironChildMEHIRON_LINK_KEY.AsInteger=0) And
        (Tbl_MehironChild.State=dsBrowse) Then
     Begin
         Tbl_MehironChild.Edit;
         Tbl_MehironChildMEHIRON_LINK_KEY.AsInteger:=Tbl_MehironMEHIRON_LINK_KEY.AsInteger;
         Tbl_MehironChild.Post;
     End;
end;

procedure TFrm_MehironMoatzot.DBGrid1Enter(Sender: TObject);
Var
   I :longInt;
begin
     ChangeDSForNavigator(DBNavigator);
     if Tbl_Mehiron.State=dsInsert Then
     Begin
          Tbl_Mehiron.Post;
          if (Tbl_MehironChild.RecordCount = 0) Then
          Begin
               For I:=1 To 5 Do
               Begin
                   if Tbl_MehironChild.State=dsBrowse Then
                      Tbl_MehironChild.Append;
                   Case I Of
                      1: Begin
                             Tbl_MehironChildSUG_SHERUT_IN_TNUA.Value:=Tbl_MehironSUG_SHERUT.Value;
                             if Tbl_MehironLookMaslulName.AsString='' Then
                                Tbl_MehironChildTEUR.AsString:=Tbl_MehironSHEM_MEHIRON.AsString
                             Else
                                 Tbl_MehironChildTEUR.AsString:=Tbl_MehironLookMaslulName.AsString;
                         End;
                      2:if CodeKM >0 Then
                        Begin
                             Tbl_MehironChildSUG_SHERUT_IN_TNUA.AsInteger:=CodeKM;
                             Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString:='ShibTotalKm';
                             Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME.AsString:='�"�';
                        End;
                      3:if CodeHour>0 Then
                        Begin
                           Tbl_MehironChildSUG_SHERUT_IN_TNUA.AsInteger:=CodeHour;
                           Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString:='TotalHour';
                           Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME.AsString:='���� ����';
                        End;
                      4:if CodeStudent>0 Then
                        Begin
                           Tbl_MehironChildSUG_SHERUT_IN_TNUA.AsInteger:=CodeStudent;
                           Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString:='QuntMusa';
                           Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME.AsString:='���� ������';
                        End;
                      5:if CodeFix>0 Then
                        Begin
                           Tbl_MehironChildSUG_SHERUT_IN_TNUA.AsInteger:=CodeFix;
//                           Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString:='QuntMusa';
//                           Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME.AsString:='���� ������';
                        End;
                   End; //Case
                   if Tbl_MehironChildSUG_SHERUT_IN_TNUA.AsInteger>0 Then
                   Begin
                      Tbl_MehironChildFROM_VALUE.AsInteger:=0;
                      Tbl_MehironChildTO_VALUE.AsInteger:=999999;
                      Tbl_MehironChild.Post
                   End
                   Else
                      Tbl_MehironChild.Cancel;
               End;

          End;
     End;
end;

procedure TFrm_MehironMoatzot.Tbl_MehironChildBeforePost(DataSet: TDataSet);
begin
     if (Tbl_MehironChildSUG_HISHUV.AsInteger=slRelative) And
        (Not Tbl_MehironChildSUG_KAMUT_FIELD_NAME.IsNull) And
        (Not Tbl_MehironChildRANGE_FIELD_NAME.IsNull) And
        (Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString<>Tbl_MehironChildRANGE_FIELD_NAME.AsString) Then
     Begin
          MessageDlg('������ ��� ���� ��� ���� ���� ����� ��� ��� ���� ������',mtError,[mbOk],0);
          Abort;
          Exit;
     End;

     //����� ��� ����� ��� ���
     if Tbl_MehironChildMEHIRON_LINK_KEY.AsInteger=0 Then
         Tbl_MehironChildMEHIRON_LINK_KEY.AsInteger:=Tbl_MehironMEHIRON_LINK_KEY.AsInteger;
     if Tbl_MehironChildSUG_HIUV.IsNull Then
        Tbl_MehironChildSUG_HIUV.Value:=Tbl_MehironSUG_HIUV.Value;
     if Tbl_MehironChildSUG_SHERUT.IsNull Then
        Tbl_MehironChildSUG_SHERUT.Value:=Tbl_MehironSUG_SHERUT.Value;
     if Tbl_MehironChildCODE_MEHIRON.IsNull Then
        Tbl_MehironChildCODE_MEHIRON.Value:=Tbl_MehironCODE_MEHIRON.Value;
end;

procedure TFrm_MehironMoatzot.DBGrid1Exit(Sender: TObject);
begin
     ChangeDSForNavigator(DBNavigator);
end;

procedure TFrm_MehironMoatzot.DBGrid1EditButtonClick(Sender: TObject);
begin
     if RxDBGrid_MehirChild.SelectedField=Tbl_MehironChildYEHIDAT_MIDA Then
     Begin
          (Dm_AtmCrt.AtmAdvSearch_KodTavla.SourceDataSet As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_YeMida;
          if Dm_AtmCrt.AtmAdvSearch_KodTavla.Execute Then
          Begin
               if Tbl_MehironChild.State=dsBrowse Then
                  Tbl_MehironChild.Edit;
               Tbl_MehironChildYEHIDAT_MIDA.AsString:=Dm_AtmCrt.AtmAdvSearch_KodTavla.ReturnString;
               Exit;
          End;
     End;

     if RxDBGrid_MehirChild.SelectedField=Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME Then
         if AtmListDlg_TnuaFields.Execute Then
         Begin
            if Tbl_MehironChild.State=dsBrowse Then
               Tbl_MehironChild.Edit;
            if Trim(AtmListDlg_TnuaFields.ReturnString)='����' Then
            Begin
                 Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString:='';
                 Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME.AsString:='';
            End
            Else
            Begin
                 Tbl_MehironChildSUG_KAMUT_FIELD_NAME.AsString:=GetFieldFromSeparateString(AtmListDlg_TnuaFields.ReturnString,'_',1);
                 Tbl_MehironChildSUG_KAMUT_DISPLAY_NAME.AsString:=GetFieldFromSeparateString(AtmListDlg_TnuaFields.ReturnString,'_',2);
            End;
            Exit;
         End;

     if RxDBGrid_MehirChild.SelectedField=Tbl_MehironChildRANGE_FIELD_DISPLAY_NAME Then
         if AtmListDlg_TnuaFields.Execute Then
         Begin
            if Tbl_MehironChild.State=dsBrowse Then
               Tbl_MehironChild.Edit;
            if Trim(AtmListDlg_TnuaFields.ReturnString)='����' Then
            Begin
                 Tbl_MehironChildRANGE_FIELD_NAME.AsString:='';
                 Tbl_MehironChildRANGE_FIELD_DISPLAY_NAME.AsString:='';
            End
            Else
            Begin
                 Tbl_MehironChildRANGE_FIELD_NAME.AsString:=GetFieldFromSeparateString(AtmListDlg_TnuaFields.ReturnString,'_',1);
                 Tbl_MehironChildRANGE_FIELD_DISPLAY_NAME.AsString:=GetFieldFromSeparateString(AtmListDlg_TnuaFields.ReturnString,'_',2);
            End;
            Exit;
         End;

         if RxDBGrid_MehirChild.SelectedField=Tbl_MehironChildTeurSugHishuv Then
         Begin
             if AtmListDlg_SugHishuv.Execute Then
             Begin
                if Tbl_MehironChild.State=dsBrowse Then
                   Tbl_MehironChild.Edit;
                Tbl_MehironChildSUG_HISHUV.AsInteger:=AtmListDlg_SugHishuv.Items.IndexOf(AtmListDlg_SugHishuv.ReturnString);
                Exit;
             End;
         End;

         if RxDBGrid_MehirChild.SelectedField=Tbl_MehironChildLookSugBhira Then
         Begin
             if AtmListDlg_SugBhira.Execute Then
             Begin
                if Tbl_MehironChild.State=dsBrowse Then
                   Tbl_MehironChild.Edit;
                Tbl_MehironChildSUG_BHIRA.AsInteger:=AtmListDlg_SugBhira.Items.IndexOf(AtmListDlg_SugBhira.ReturnString);
                Exit;
             End;
         End;
        if RxDBGrid_MehirChild.Columns[RxDBGrid_MehirChild.SelectedIndex].ButtonStyle=cbsEllipsis Then
        Begin
           DBGrid1EditButtonClick(RxDBGrid_MehirChild);
           Exit;
        End;

end;

procedure TFrm_MehironMoatzot.Tbl_MehironBeforePost(DataSet: TDataSet);
begin
     if (Tbl_Mehiron.State=dsEdit) And (Tbl_MehironChild.RecordCount=1) And (PageControl1.ActivePage=TS_Simple)Then
     Begin
          if Tbl_MehironChild.FieldByName('SUG_SHERUT').AsInteger<>Tbl_Mehiron.FieldByName('SUG_SHERUT').AsInteger Then
          Begin
               if Tbl_MehironChild.State=dsBrowse Then
                  Tbl_MehironChild.Edit;
               Tbl_MehironChild.FieldByName('SUG_SHERUT').AsInteger:=Tbl_Mehiron.FieldByName('SUG_SHERUT').AsInteger;
               Tbl_MehironChild.FieldByName('SUG_SHERUT_IN_TNUA').AsInteger:=Tbl_Mehiron.FieldByName('SUG_SHERUT').AsInteger;
          End;
     End;

     if Tbl_Mehiron.State=dsInsert Then
     With Qry1 Do
     Begin
          ShortDateFormat:='MM/DD/YYYY';
          Try
              Active:=False;
              Sql.Clear;
              Sql.Add('Select * From '+Tbl_Mehiron.TableName);
              Sql.Add('Where CODE_MEHIRON='+Tbl_MehironCODE_MEHIRON.AsString);
              Sql.Add('And SUG_HIUV='+Tbl_MehironSUG_HIUV.AsString);
              Sql.Add('And SUG_SHERUT='+Tbl_MehironSUG_SHERUT.AsString);
              Sql.Add('And TO_DATE>'''+DateToStr(Tbl_MehironFROM_DATE.Value)+'''');
              ShortDateFormat:='DD/MM/YYYY';
              Active:=True;
              if RecordCount > 0 Then
              Begin
                   ShowMessage('������ �� ��� ���� ������');
                   FindFirstControlInTabOrder(PageControl1.ActivePage).SetFocus;
                   Abort;
              End
          Finally
              ShortDateFormat:='DD/MM/YYYY';
              Active:=False;
          End;
     End;//With Qry1

end;

procedure TFrm_MehironMoatzot.PageControl1Change(Sender: TObject);
begin
     TBtn_PrintGrid.Visible:=PageControl1.ActivePage=TS_QryMehiron;
     ChangeDSForNavigator(DBNavigator);
     if (PageControl1.ActivePage=TS_Simple) Or (PageControl1.ActivePage=TS_Advance) Then
        Qry1.Active:=False;
     if PageControl1.ActivePage=TS_QryMehiron Then
     Begin
          LastHiuv:=-1;
          LastSherut:=-1;
          With Qry1 Do
          Begin
              Active:=False;
              Sql.Clear;
              Sql.Add('Select * From MhirChld');
              Sql.Add('Where CODE_MEHIRON='+EditCODE_MEHIRON.Text);
              Sql.Add('Order By Sug_Hiuv,SUG_SHERUT');
              //����� ����� ���� �������
              Active:=True;
          End;
     End;
end;

procedure TFrm_MehironMoatzot.RxDBGrid_QryGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
{     if (RxDBGrid_Qry.DataSource.DataSet.FieldByName('SUG_HIUV').AsInteger<>LastHiuv) Or
        (RxDBGrid_Qry.DataSource.DataSet.FieldByName('SUG_SHERUT').AsInteger<>LastSherut) Then
     Begin
          if CurBackground=clRed Then
             CurBackground:=Background
          Else
              CurBackground:=clRed;
          LastHiuv:=RxDBGrid_Qry.DataSource.DataSet.FieldByName('SUG_HIUV').AsInteger;
          LastSherut:=RxDBGrid_Qry.DataSource.DataSet.FieldByName('SUG_SHERUT').AsInteger;
     End;
  Background:=CurBackground;
}
end;

procedure TFrm_MehironMoatzot.DS_MehironStateChange(Sender: TObject);
begin
//     DM_AtmCrt.Action_OpenSearchUpdate(TBtn_OpenSearch.Action);
       AppEvents1ActiveControlChange(Self);
       StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

Procedure TFrm_MehironMoatzot.BuildLookupList;
Var
   SList:TStringList;
   I :longInt;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_SugMitan),Tbl_MehironLookSugHiuv);
     SList.AddObject(IntToStr(SugTavla_SugMehiron),Tbl_MehironLookSugMehiron);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_MehironChildLookYehidatMida);
     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_SugMitan),AtmDbComboBox_SugHiuv);
     SList.AddObject(IntToStr(SugTavla_SugMitan),AtmDbComboBox_SugHiuv2);
     SList.AddObject(IntToStr(SugTavla_SugMehiron),AtmDbComboBox_SugMehiron);
     SList.AddObject(IntToStr(SugTavla_SugMehiron),AtmDbComboBox_SugMehiron2);
     SList.AddObject(IntToStr(SugTavla_YeMida),AtmDbComboBox_Simple_YeMida);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     Tbl_MehironChildTeurSugHishuv.LookupList.Clear;
     With Tbl_MehironChildTeurSugHishuv Do
     Begin
          For I:=0 To AtmListDlg_SugHishuv.Items.Count-1 Do
              LookupList.Add(I,AtmListDlg_SugHishuv.Items[i]);
     End;

     Tbl_MehironChildLookSugBhira.LookupList.Clear;
     With Tbl_MehironChildLookSugBhira Do
     Begin
          For I:=0 To AtmListDlg_SugBhira.Items.Count-1 Do
              LookupList.Add(I,AtmListDlg_SugBhira.Items[i]);
     End;

End;

procedure TFrm_MehironMoatzot.Tbl_MehironChildSUG_SHERUT_IN_TNUAValidate(
  Sender: TField);
begin
     if Not Tbl_MehironChildSUG_SHERUT_IN_TNUA.IsNull Then
     Begin
          if Tbl_Maslul.FindKey([Tbl_MehironChildSUG_SHERUT_IN_TNUA.Value]) Then
             Tbl_MehironChildTEUR.AsString:=Tbl_Maslul.FieldByName('Name').AsString;
     End;

end;

procedure TFrm_MehironMoatzot.RxDBGrid_MehirChildColEnter(Sender: TObject);
begin
  TBtn_OpenSearch.Enabled:=CompareText(RxDBGrid_MehirChild.SelectedField.FieldName,Tbl_MehironChildSUG_SHERUT_IN_TNUA.FieldName)=0;
  TBtn_OpenSearch.Enabled:=(TBtn_OpenSearch.Enabled) Or (TDbGrid(ActiveControl).Columns[TDbGrid(ActiveControl).SelectedIndex].ButtonStyle=cbsEllipsis);
end;

procedure TFrm_MehironMoatzot.TBtn_OpenSearchClick(Sender: TObject);
begin
     if ActiveControl is TAtmDbHEdit Then
       (ActiveControl As TAtmDbHEdit).ExecSearch;

     if ActiveControl = RxDBGrid_MehirChild then
        if RxDBGrid_MehirChild.SelectedField=Tbl_MehironChildSUG_SHERUT_IN_TNUA Then
           if AtmAdvSearch_Maslul.Execute Then
           Begin
               if Tbl_MehironChild.State=dsBrowse Then
                  Tbl_MehironChild.Edit;
               Tbl_MehironChildSUG_SHERUT_IN_TNUA.AsString:=AtmAdvSearch_Maslul.ReturnString;
           End;

end;

procedure TFrm_MehironMoatzot.AppEvents1ActiveControlChange(Sender: TObject);
Var
   TheDs :TDataSource;
begin
     if Not Visible Then Exit;
     if ActiveControl is TAtmDbHEdit Then
        TBtn_OpenSearch.Enabled:=(ActiveControl As TAtmDbHEdit).EnableSearch
     Else
        if ActiveControl<> RxDBGrid_MehirChild Then
           TBtn_OpenSearch.Enabled:=False;

     TheDs:=GetDataSourceFromTControl(ActiveControl);
     if (TheDs<>Nil) And (DBNavigator.DataSource<>TheDs) Then
        DBNavigator.DataSource:=TheDs;

end;

procedure TFrm_MehironMoatzot.TBtn_NewMehironClick(Sender: TObject);
begin
     Tbl_Mehiron.Insert;
end;

Procedure TFrm_MehironMoatzot.ShowForm(FormParams:ShortString);
Var
   StrH:String;
Begin
     Show;
     StrH:=GetFieldFromSeparateString(FormParams,',',1);
     if StrH<>'' Then
          Tbl_Mehiron.FindKey([StrH])
     Else
          Tbl_Mehiron.Insert;

     StrH:=GetFieldFromSeparateString(FormParams,',',2);
     if StrH<>'' Then
        IniFileName:=StrH
     Else
         IniFileName:=ExtractFilePath(Application.ExeName)+Atm2000IniFileName;
         
     ReadFixCodes;
End;

procedure TFrm_MehironMoatzot.Tbl_MehironLookMaslulNameValidate(Sender: TField);
begin
     if Tbl_MehironSHEM_MEHIRON.IsNull Then
        Tbl_MehironSHEM_MEHIRON.AsString:=Sender.AsString;
end;

procedure TFrm_MehironMoatzot.Lbl_CodeSherutDblClick(Sender: TObject);
begin
     OpenAtmCrt(fnMaslul,AtmDbHEdit_SugSherut.Text);
end;

procedure TFrm_MehironMoatzot.EditCODE_MEHIRONExit(Sender: TObject);
begin
  Try
     Screen.Cursor:=crHourGlass;
     if Tbl_MehironCODE_MEHIRON.IsNull Then
     Begin
          Qry1.Active:=False;
          With Qry1.Sql Do
          Begin
               Clear;
               Add('Select Max(CODE_MEHIRON) MaxCode');
               Add('From '+Tbl_Mehiron.TableName);
          End;
          Qry1.Active:=True;
          Tbl_MehironCODE_MEHIRON.AsInteger:=Qry1.FieldByName('MaxCode').AsInteger+1;
     End;
  Finally
         Qry1.Active:=False;
         Screen.Cursor:=crDefault;
  End;

     if (Tbl_Mehiron.State=dsInsert) {And (Tbl_MehironSHEM_MEHIRON.IsNull)} Then
  Begin
      Qry1.Active:=False;
      Screen.Cursor:=crHourGlass;
      Try
          With Qry1.Sql Do
          Begin
               Clear;
               Add('Select Distinct SHEM_MEHIRON,Record_Num From '+Tbl_Mehiron.TableName);
               Add('Where CODE_MEHIRON='+Tbl_MehironCODE_MEHIRON.AsString);
               Add('And SHEM_MEHIRON is Not NULL');
               Add('Order By Record_Num');
          End;
          Qry1.Active:=True;

          Qry1.First;
          if Not (Qry1.Eof) Then
          Begin
             Qry1.Last;
             Tbl_MehironSHEM_MEHIRON.AsString:=Qry1.FieldByName('SHEM_MEHIRON').AsString;
          End;
      Finally
             Qry1.Active:=False;
             Screen.Cursor:=crDefault;
      End;

  End;

end;

procedure TFrm_MehironMoatzot.Panel4Enter(Sender: TObject);
begin
     ChangeDSForNavigator(DBNavigator);
     DBNavigator.VisibleButtons:=[nbFirst,nbPrior,nbNext,nbLast,nbDelete,nbEdit,nbPost,nbCancel,nbRefresh];
     if Tbl_Mehiron.State<>dsBrowse Then
     Begin
          Tbl_Mehiron.Post;
          if (Tbl_MehironChild.RecordCount = 0) Then
          Begin
               if Tbl_MehironChild.State=dsBrowse Then
                  Tbl_MehironChild.Insert;
               Tbl_MehironChildSUG_SHERUT_IN_TNUA.Value:=Tbl_MehironSUG_SHERUT.Value;
               if Tbl_MehironLookMaslulName.AsString='' Then
                  Tbl_MehironChildTEUR.AsString:=Tbl_MehironSHEM_MEHIRON.AsString
               Else
                   Tbl_MehironChildTEUR.AsString:=Tbl_MehironLookMaslulName.AsString;
          End;
     End;
end;

procedure TFrm_MehironMoatzot.Panel4Exit(Sender: TObject);
begin
     ChangeDSForNavigator(DBNavigator);
     DBNavigator.VisibleButtons:=[nbInsert,nbFirst,nbPrior,nbNext,nbLast,nbDelete,nbEdit,nbPost,nbCancel,nbRefresh];
end;


procedure TFrm_MehironMoatzot.TBtn_PrintGridClick(Sender: TObject);
begin
     PrintDbGrid('��� ������ ',RxDBGrid_Qry);
end;

procedure TFrm_MehironMoatzot.EditSUG_MEHIRONBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugMehiron;
end;

procedure TFrm_MehironMoatzot.AtmDbHEdit_SugHiuvBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugMitan;
end;

procedure TFrm_MehironMoatzot.AtmDbHEdit_Simple_YehidatMidaBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_YeMida;
end;

procedure TFrm_MehironMoatzot.Tbl_MehironChildAfterInsert(DataSet: TDataSet);
begin
     if PageControl1.ActivePage=TS_Advance Then
        RxDBGrid_MehirChild.SelectedIndex:=0;
end;

procedure TFrm_MehironMoatzot.RxDBGrid_QryDblClick(Sender: TObject);
begin
     With Qry1 Do
          Tbl_Mehiron.Locate('MEHIRON_LINK_KEY',FieldByName('MEHIRON_LINK_KEY').Value,[]);
     PageControl1.ActivePage:=Ts_Advance;
end;

procedure TFrm_MehironMoatzot.RxDBGrid_MehirChildKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if Key=VK_F2 Then
     Begin
          if TBtn_OpenSearch.Enabled Then
             TBtn_OpenSearchClick(TBtn_OpenSearch)
          Else
          if RxDBGrid_MehirChild.Columns[RxDBGrid_MehirChild.SelectedIndex].ButtonStyle=cbsEllipsis Then
          Begin
            DBGrid1EditButtonClick(RxDBGrid_MehirChild);
            RxDBGrid_MehirChild.SelectedIndex:=RxDBGrid_MehirChild.SelectedIndex+1;
          End;
     End;
end;

procedure TFrm_MehironMoatzot.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msMehiron],False,CrtMeholelHandle);
end;

procedure TFrm_MehironMoatzot.TBtn_CopyMehironClick(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDllProcPCharParam('MhrnUtil.Dll','InitMehironUtil',[DM_AtmCrt.DBUserName,DM_AtmCrt.DbPassword,CurCrtAliasName],False,H);
     RunDllProcPCharParam('MhrnUtil.Dll','UpdateMehiron',[Tbl_MehironCODE_MEHIRON.AsString,Tbl_MehironSUG_HIUV.AsString,Tbl_MehironSUG_SHERUT.AsString],False,H);
     RunDllNoParam('MhrnUtil.Dll','DoneMehironUtil',True,H);
end;

procedure TFrm_MehironMoatzot.TBtn_DeleteFullMehironClick(Sender: TObject);
begin
     Tbl_Mehiron.Cancel;
     Beep(200,100);
     if MessageDlg('����� �� ����� �� �� ������� �� �� ���� ����� ������ ���� ������ ��. ��� ������ ������',mtConfirmation,[mbNo,mbYes],0)=mrYes Then
     Begin
       Try
         Screen.Cursor:=crHourGlass;
         With Dm_AtmCrt Do
         Begin
             Qry_Temp.Active:=False;
             Qry_Temp.SQL.Clear;
             Qry_Temp.Sql.Add('Delete From mhirchld Where CODE_MEHIRON='+EditCODE_MEHIRON.Text);
             Qry_Temp.ExecSQL;
             Qry_Temp.SQL.Clear;
             Qry_Temp.Sql.Add('Delete From mehirlak Where CODE_MEHIRON='+EditCODE_MEHIRON.Text);
             Qry_Temp.ExecSQL;
             Qry_Temp.SQL.Clear;
         End;
       Finally
              Screen.Cursor:=crDefault;
       End;
       Tbl_Mehiron.Refresh;
       ShowMessage('������ ����� ������');
     End;
end;

procedure TFrm_MehironMoatzot.ReadFixCodes;
Var
   F :TIniFile;
Begin
     F:=TIniFile.Create(IniFileName);
     Try
        CodeKM:=F.ReadInteger('Prices','KmPrice',0);
        CodeHour:=F.ReadInteger('Prices','HourPrice',0);
        CodeStudent:=F.ReadInteger('Prices','CapitaPrice',0);
        CodeFix:=F.ReadInteger('Prices','FixPrice',0);
     Finally
            F.Free;
     End;

End;
procedure TFrm_MehironMoatzot.Tbl_MehironFROM_DATEValidate(Sender: TField);
begin
  if (Tbl_Mehiron.FieldByName('FROM_DATE').AsDateTime > Tbl_Mehiron.FieldByName('TO_DATE').AsDateTime) And
     (Not Tbl_Mehiron.FieldByName('TO_DATE').IsNull) Then
  Begin
    MessageDlg('����� ����� ���� ������ ����',mtError,[mbOk],0);
    Abort;
  End;
end;

procedure TFrm_MehironMoatzot.DS_MehironChildStateChange(Sender: TObject);
begin
  AppEvents1ActiveControlChange(Self);
  StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);

  Tbl_MehironSUG_HIUV.ReadOnly:=Tbl_Mehiron.State=dsEdit;
  AtmDbHEdit_SugHiuv.ReadOnly:=Tbl_MehironSUG_HIUV.ReadOnly;
  Tbl_MehironSUG_SHERUT.ReadOnly:=Tbl_Mehiron.State=dsEdit;
  AtmDbHEdit_SugSherut.ReadOnly:=Tbl_MehironSUG_SHERUT.ReadOnly;
end;

end.

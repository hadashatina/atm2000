unit F_Musa;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, DBTables, Mask, ExtCtrls, AtmComp, ComCtrls,
  AtmAdvTable, ToolWin, AdvSearch, AtmLookCombo, Scale250,MslDllInterface,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, Buttons, Grids, DBGrids, RxQuery,
  AtmRxQuery, dbnav797;

type
  TFrm_Musa = class(TForm)
    Tbl_MusaCODE_MUSA: TIntegerField;
    Tbl_MusaFIRST_NAME: TStringField;
    Tbl_MusaLAST_NAME: TStringField;
    Tbl_MusaTZ: TIntegerField;
    Tbl_MusaBIRTH_DATE: TDateTimeField;
    Tbl_MusaCITY_SYMBOL: TIntegerField;
    Tbl_MusaCITY_NAME: TStringField;
    Tbl_MusaSTREET_SYMBOL: TIntegerField;
    Tbl_MusaSTREET_NAME: TStringField;
    Tbl_MusaHOUSE_NUM: TStringField;
    Tbl_MusaAPARTMENT_NUM: TIntegerField;
    Tbl_MusaPHONE1: TStringField;
    Tbl_MusaPHONE2: TStringField;
    Tbl_MusaMISPAR_RASHUT: TIntegerField;
    Tbl_MusaSYMBOL_MOSAD: TIntegerField;
    Tbl_MusaSYMBOL_YESHUV_LIMUD: TIntegerField;
    Tbl_MusaREMARK: TStringField;
    DS_Musa: TDataSource;
    Panel2: TPanel;
    Tbl_Musa: TAtmRxQuery;
    Tbl_MusaCODE_SUG_MUSA: TIntegerField;
    Tbl_MusaCLASS: TIntegerField;
    Tbl_MusaMAKBILA: TIntegerField;
    Tbl_MusaMEGAMA: TIntegerField;
    Tbl_MusaCODE_STATION: TIntegerField;
    Tbl_MusaCODE_SUG_CHINUH: TIntegerField;
    Tbl_MusaCODE_EXCEPTION: TIntegerField;
    Tbl_MusaCODE_ZAKAUT: TIntegerField;
    Tbl_MusaCODE_BUS_PRICE1: TIntegerField;
    Tbl_MusaCODE_BUS_PRICE2: TIntegerField;
    Tbl_MusaTOTAL_BUS_DRIVES: TIntegerField;
    Tbl_MusaCODE_DATA_SOURCE: TIntegerField;
    Tbl_MusaDISTANCE: TIntegerField;
    Tbl_MusaLAST_UPDATE: TDateTimeField;
    Tbl_MusaSTRING_1: TStringField;
    Tbl_MusaSTRING_2: TStringField;
    Tbl_MusaSTRING_3: TStringField;
    Tbl_MusaSTRING_4: TStringField;
    Tbl_MusaSTRING_5: TStringField;
    Tbl_MusaSTRING_6: TStringField;
    Tbl_MusaSTRING_7: TStringField;
    Tbl_MusaSTRING_8: TStringField;
    Tbl_MusaSTRING_9: TStringField;
    Tbl_MusaSTRING_10: TStringField;
    Tbl_MusaINTEGER_1: TIntegerField;
    Tbl_MusaINTEGER_2: TIntegerField;
    Tbl_MusaINTEGER_3: TIntegerField;
    Tbl_MusaINTEGER_4: TIntegerField;
    Tbl_MusaINTEGER_5: TIntegerField;
    Tbl_MusaINTEGER_6: TIntegerField;
    Tbl_MusaINTEGER_7: TIntegerField;
    Tbl_MusaINTEGER_8: TIntegerField;
    Tbl_MusaINTEGER_9: TIntegerField;
    Tbl_MusaINTEGER_10: TIntegerField;
    Tbl_MusaDATE_1: TDateTimeField;
    Tbl_MusaDATE_2: TDateTimeField;
    Tbl_MusaDATE_3: TDateTimeField;
    Tbl_MusaDATE_4: TDateTimeField;
    Tbl_MusaDATE_5: TDateTimeField;
    Tbl_MusaDATE_6: TDateTimeField;
    Tbl_MusaDATE_7: TDateTimeField;
    Tbl_MusaDATE_8: TDateTimeField;
    Tbl_MusaDATE_9: TDateTimeField;
    Tbl_MusaDATE_10: TDateTimeField;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    TS_ShiuhMosad: TTabSheet;
    Panel3: TPanel;
    Lbl_Code: TLabel;
    Lbl_CodeSugMusa: TLabel;
    EditCODE_MUSA: TAtmDbHEdit;
    Lbl_FirstName: TLabel;
    Lbl_LastName: TLabel;
    EditFIRST_NAME: TAtmDbHEdit;
    EditLAST_NAME: TAtmDbHEdit;
    Lbl_TZ: TLabel;
    EditTZ: TAtmDbHEdit;
    AtmDbHEdit_SchoolYear: TAtmDbHEdit;
    Lbl_SchoolYear: TLabel;
    Tbl_MusaSCHOOL_YEAR: TIntegerField;
    Tbl_MusaCODE_ZAKAUT_MANBAS: TIntegerField;
    Tbl_MusaLookClass: TStringField;
    Tbl_Mosad: TAtmRxQuery;
    Tbl_MusaLookNameMosad: TStringField;
    ToolBar1: TToolBar;
    DBNavigator: TAtmDbNavigator;
    ToolButton1: TToolButton;
    TBtn_OpenSearch: TToolButton;
    AtmAdvSearch_Mosad: TAtmAdvSearch;
    Tbl_MusaCODE_BUS_PRICE3: TIntegerField;
    Tbl_Station: TAtmRxQuery;
    Tbl_StationRASHUT_NUM: TIntegerField;
    Tbl_StationCODE_STAITION: TIntegerField;
    Tbl_MusaLookRashutNum: TStringField;
    AtmAdvSearch_Station: TAtmAdvSearch;
    Tbl_StationSHEM_YESHUV: TStringField;
    Tbl_StationNAME_STATION: TStringField;
    Tbl_StationSUG_STATION: TIntegerField;
    Tbl_StationCODE_RECORD: TAutoIncField;
    Tbl_StationSEMEL_YESHUV: TIntegerField;
    Tbl_StationX: TIntegerField;
    Tbl_StationY: TIntegerField;
    AtmDbComboBox_SugMusa: TAtmDbComboBox;
    Scale1: TScale;
    Tbl_MusaX: TIntegerField;
    Tbl_MusaY: TIntegerField;
    AtmAdvSearch_SemelYeshuv: TAtmAdvSearch;
    StatusBar1: TStatusBar;
    Tbl_MusaNodeNumB: TIntegerField;
    Tbl_MusaNodeSugSystem: TIntegerField;
    Tbl_MusaNodeKodCityName: TIntegerField;
    Tbl_KodTavla2: TTable;
    AtmAdvSearch_Rashut: TAtmAdvSearch;
    Panel1: TPanel;
    Lbl_BirthDate: TLabel;
    Lbl_YeshuvSym: TLabel;
    Lbl_CityName: TLabel;
    Lbl_StreetSym: TLabel;
    Lbl_StreetName: TLabel;
    Lbl_HouseNum: TLabel;
    Lbl_Apartment: TLabel;
    Lbl_Phone1: TLabel;
    Lbl_Phone2: TLabel;
    Lbl_Remark: TLabel;
    Lbl_Station: TLabel;
    DBText9: TDBText;
    Lbl_NumRashut: TLabel;
    DBText_RashutName: TDBText;
    EditCITY_SYMBOL: TAtmDbHEdit;
    EditCITY_NAME: TAtmDbHEdit;
    EditSTREET_SYMBOL: TAtmDbHEdit;
    EditSTREET_NAME: TAtmDbHEdit;
    EditHOUSE_NUM: TAtmDbHEdit;
    EditAPARTMENT_NUM: TAtmDbHEdit;
    EditPHONE: TAtmDbHEdit;
    EditPHONE2: TAtmDbHEdit;
    EditREMARK: TAtmDbHEdit;
    DBEdit4_CodeStation: TAtmDbHEdit;
    EditMISPAR_RASHUT: TAtmDbHEdit;
    AtmDBDateEdit1: TAtmDBDateEdit;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Lbl_MosadSym: TLabel;
    Lbl_CityLimudSym: TLabel;
    DBText1: TDBText;
    DBText6: TDBText;
    Label14: TLabel;
    DBText8: TDBText;
    Label15: TLabel;
    DBEdit_CodeClass: TAtmDbHEdit;
    DBEdit_CodeMakbika: TAtmDbHEdit;
    DBEdit_TOTAL_BUS_DRIVES: TAtmDbHEdit;
    DBEdit_CODE_DATA_SOURCE: TAtmDbHEdit;
    DBEdit_DISTANCE: TAtmDbHEdit;
    DBEdit_LastUpdate: TAtmDbHEdit;
    EditSYMBOL_MOSAD: TAtmDbHEdit;
    EditSYMBOL_YESHUV_LIMUD: TAtmDbHEdit;
    DBCB_ZakautManbas: TDBCheckBox;
    AtmDbComboBox_SugChinuh: TAtmDbComboBox;
    AtmDbComboBox_Megama: TAtmDbComboBox;
    AtmDbComboBox_ZakautNesia: TAtmDbComboBox;
    AtmDbComboBox_CodeException: TAtmDbComboBox;
    Spb_CalcDistFromMosad: TSpeedButton;
    TS_MusaGrid: TTabSheet;
    RxDBGrid1: TRxDBGrid;
    Query_Musa: TQuery;
    DS_QryMusa: TDataSource;
    Tbtn_ChooseMslAddress: TToolButton;
    Tbl_MusaDIST_FROM_MSL: TIntegerField;
    AtmDbHEdit2: TAtmDbHEdit;
    Label4: TLabel;
    AtmDbComboBox_BusPrice1: TAtmDbComboBox;
    AtmDbComboBox_BusPrice2: TAtmDbComboBox;
    AtmDbComboBox_BusPrice3: TAtmDbComboBox;
    ToolButton2: TToolButton;
    TBtn_Report: TToolButton;
    Query_MusaCODE_MUSA: TIntegerField;
    Query_MusaSCHOOL_YEAR: TIntegerField;
    Query_MusaCODE_SUG_MUSA: TIntegerField;
    Query_MusaFIRST_NAME: TStringField;
    Query_MusaLAST_NAME: TStringField;
    Query_MusaTZ: TIntegerField;
    Query_MusaBIRTH_DATE: TDateTimeField;
    Query_MusaCITY_SYMBOL: TIntegerField;
    Query_MusaCITY_NAME: TStringField;
    Query_MusaSTREET_SYMBOL: TIntegerField;
    Query_MusaSTREET_NAME: TStringField;
    Query_MusaHOUSE_NUM: TStringField;
    Query_MusaAPARTMENT_NUM: TIntegerField;
    Query_MusaPHONE1: TStringField;
    Query_MusaPHONE2: TStringField;
    Query_MusaMISPAR_RASHUT: TIntegerField;
    Query_MusaSYMBOL_MOSAD: TIntegerField;
    Query_MusaSYMBOL_YESHUV_LIMUD: TIntegerField;
    Query_MusaCLASS: TIntegerField;
    Query_MusaMAKBILA: TIntegerField;
    Query_MusaMEGAMA: TIntegerField;
    Query_MusaCODE_STATION: TIntegerField;
    Query_MusaCODE_SUG_CHINUH: TIntegerField;
    Query_MusaCODE_EXCEPTION: TIntegerField;
    Query_MusaCODE_ZAKAUT: TIntegerField;
    Query_MusaCODE_BUS_PRICE1: TIntegerField;
    Query_MusaCODE_BUS_PRICE2: TIntegerField;
    Query_MusaCODE_BUS_PRICE3: TIntegerField;
    Query_MusaTOTAL_BUS_DRIVES: TIntegerField;
    Query_MusaCODE_DATA_SOURCE: TIntegerField;
    Query_MusaDISTANCE: TIntegerField;
    Query_MusaCODE_ZAKAUT_MANBAS: TIntegerField;
    Query_MusaX: TIntegerField;
    Query_MusaY: TIntegerField;
    Query_MusaNodeNumB: TIntegerField;
    Query_MusaNodeSugSystem: TIntegerField;
    Query_MusaNodeKodCityName: TIntegerField;
    Query_MusaLAST_UPDATE: TDateTimeField;
    Query_MusaREMARK: TStringField;
    Query_MusaSTRING_1: TStringField;
    Query_MusaSTRING_2: TStringField;
    Query_MusaSTRING_3: TStringField;
    Query_MusaSTRING_4: TStringField;
    Query_MusaSTRING_5: TStringField;
    Query_MusaSTRING_6: TStringField;
    Query_MusaSTRING_7: TStringField;
    Query_MusaSTRING_8: TStringField;
    Query_MusaSTRING_9: TStringField;
    Query_MusaSTRING_10: TStringField;
    Query_MusaINTEGER_1: TIntegerField;
    Query_MusaINTEGER_2: TIntegerField;
    Query_MusaINTEGER_3: TIntegerField;
    Query_MusaINTEGER_4: TIntegerField;
    Query_MusaINTEGER_5: TIntegerField;
    Query_MusaINTEGER_6: TIntegerField;
    Query_MusaINTEGER_7: TIntegerField;
    Query_MusaINTEGER_8: TIntegerField;
    Query_MusaINTEGER_9: TIntegerField;
    Query_MusaINTEGER_10: TIntegerField;
    Query_MusaDATE_1: TDateTimeField;
    Query_MusaDATE_2: TDateTimeField;
    Query_MusaDATE_3: TDateTimeField;
    Query_MusaDATE_4: TDateTimeField;
    Query_MusaDATE_5: TDateTimeField;
    Query_MusaDATE_6: TDateTimeField;
    Query_MusaDATE_7: TDateTimeField;
    Query_MusaDATE_8: TDateTimeField;
    Query_MusaDATE_9: TDateTimeField;
    Query_MusaDATE_10: TDateTimeField;
    Query_MusaDIST_FROM_MSL: TIntegerField;
    Query_MusaLookNameMosad: TStringField;
    AtmAdvSearch_CodeMuda: TAtmAdvSearch;
    UpdateSQL_Musa: TUpdateSQL;
    Tbl_MusaLookNameStation: TStringField;
    Label16: TLabel;
    AtmDBDateEdit2: TAtmDBDateEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Tbl_MusaAfterInsert(DataSet: TDataSet);
    procedure Tbl_MusaAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBEdit_CodeSugMusaBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DBEdit_CodeClassBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DBEdit_CodeSugChinuhBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DBEdit_CodeExceptionBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DBEdit_CodeMegamaBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DBEdit_CodeZakautBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DS_MusaStateChange(Sender: TObject);
    procedure Tbl_MusaBeforePost(DataSet: TDataSet);
    procedure AtmAdvSearch_SemelYeshuvAfterExecute(Sender: TObject);
    procedure Tbl_MusaTOTAL_BUS_DRIVESValidate(Sender: TField);
    procedure AtmAdvSearch_StationBeforeExecute(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Tbl_MusaCITY_NAMEValidate(Sender: TField);
    procedure Tbl_MusaSTREET_NAMEValidate(Sender: TField);
    procedure Tbl_MusaAfterScroll(DataSet: TDataSet);
    procedure Spb_CalcDistFromMosadClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure RxDBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure RxDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Lbl_MosadSymDblClick(Sender: TObject);
    procedure Tbtn_ChooseMslAddressClick(Sender: TObject);
    procedure Tbl_MusaSYMBOL_MOSADValidate(Sender: TField);
    procedure RxDBGrid1DblClick(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure Tbl_MusaCODE_MUSAValidate(Sender: TField);
    procedure Tbl_MusaSYMBOL_YESHUV_LIMUDValidate(Sender: TField);
    procedure EditCITY_SYMBOLBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure EditCITY_NAMEBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Tbl_MusaCITY_SYMBOLValidate(Sender: TField);
    procedure DBNavigatorBeforeAction(Sender: TObject; Button: TmoNavBtns);
  private
    { private declarations }
    RerutnToProc:TRerutnToProc;
    OkToUpdateNode :Boolean; //Put OkToUpdateNode:=False; AfterPost And AfterScroll Events
    DontRunMslThread :Boolean; //���� ������ ����� �������� TRUE ����
    CurSortFieldName:String;
    procedure RunThreadToCheckAddress;
    Procedure PutNodeDataInRecord(TheNode :TNode);
    procedure ThreadCheckAddressDone(Sender :TObject);
    procedure RunQryMosad(OrderByFieldName :String);
    Procedure ChangeLabelsForSoroka;
  public
    { public declarations }
    Procedure BuildLookupList;
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_Musa: TFrm_Musa;

implementation
Uses Crt_Glbl,AtmRutin,DMAtmCrt,AtmConst;
{$R *.DFM}

procedure TFrm_Musa.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
//     ShowMessage('Start Check Msl');
     Spb_CalcDistFromMosad.Enabled:=MslDllLoaded;
     Tbtn_ChooseMslAddress.Enabled:=MslDllLoaded;
     PageControl1.ActivePage:=TS_Main;
//     ShowMessage('Start BuildLookupList');
     BuildLookupList;

     if CrtSQLMode=1 Then
     Begin
       Tbl_Musa.MacroByName(Tbl_Musa.MacroForWhere).AsString:='CODE_MUSA = 1';
       Tbl_Musa.GetOnlyOneRecord:=True;
     End;
     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataset) Then
          Begin
//             ShowMessage('Open Table '+(Components[I] As TTable).TableName);
              if (Components[I] is TTable) Or (Components[I].Tag<>0) Then
                (Components[I] As TDataset).Open;
          End;
     End;

//     ShowMessage('End Open Tables');
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
  Finally
         Screen.Cursor:=crDefault;
//         ShowMessage('End All');
  End;
end;

procedure TFrm_Musa.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');

end;

procedure TFrm_Musa.Tbl_MusaAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_Musa.Tbl_MusaAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     OkToUpdateNode:=False;
end;

procedure TFrm_Musa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Musa:=Nil;
     Action:=caFree;
end;

procedure TFrm_Musa.DBEdit_CodeSugMusaBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugMusa;
end;

procedure TFrm_Musa.DBEdit_CodeClassBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_DargatClass;
end;

procedure TFrm_Musa.DBEdit_CodeSugChinuhBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugHinuch;
end;

procedure TFrm_Musa.DBEdit_CodeExceptionBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Harig;
end;

procedure TFrm_Musa.DBEdit_CodeMegamaBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Megama;
end;

procedure TFrm_Musa.DBEdit_CodeZakautBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
    ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_ZakautHasaa;
end;

procedure TFrm_Musa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Musa);
end;

procedure TFrm_Musa.DS_MusaStateChange(Sender: TObject);
begin
     DM_AtmCrt.Action_OpenSearchUpdate(TBtn_OpenSearch.Action);
end;

procedure TFrm_Musa.Tbl_MusaBeforePost(DataSet: TDataSet);
begin
  if Tbl_Musa.FieldByName('SCHOOL_YEAR').IsNull Then
  Begin
    MessageDlg('��� ����� ��� ��� ���� �� ������ ���',mtInformation,[mbOk],0);
    Abort;
    Exit;
  End;
  if (Tbl_Musa.State=dsInsert) And (Tbl_MusaCODE_MUSA.IsNull) Then
  Begin
     With DM_AtmCrt.Qry_Temp Do
     Begin
          Active:=False;
          Sql.Clear;
          Sql.Add('Select Max(Code_Musa) MaxMusa');
          Sql.Add('From MUSA');
          Active:=True;
          Tbl_MusaCODE_MUSA.AsInteger:=FieldByName('MaxMusa').AsInteger+1;
          Active:=False;
     End;
  End;
  Tbl_MusaLast_Update.AsDateTime:=Now;
  Tbl_MusaCode_Data_Source.AsInteger:=1;
end;

procedure TFrm_Musa.AtmAdvSearch_SemelYeshuvAfterExecute(Sender: TObject);
begin
     if Tbl_Musa.State=dsBrowse Then
        Tbl_Musa.Edit;
end;

procedure TFrm_Musa.Tbl_MusaTOTAL_BUS_DRIVESValidate(Sender: TField);
Var
   I :Integer;
begin
     I:=0;
     if Tbl_MusaCODE_BUS_PRICE1.AsInteger>0 Then
        Inc(I);
     if Tbl_MusaCODE_BUS_PRICE2.AsInteger>0 Then
        Inc(I);
     if Tbl_MusaCODE_BUS_PRICE3.AsInteger>0 Then
        Inc(I);
     if Not ((Tbl_MusaTOTAL_BUS_DRIVES.AsInteger>-1) and
              (Tbl_MusaTOTAL_BUS_DRIVES.AsInteger <= I)) Then
     Begin
          ShowMessage(Tbl_MusaTOTAL_BUS_DRIVES.ConstraintErrorMessage);
          Abort;
     End;
end;

procedure TFrm_Musa.AtmAdvSearch_StationBeforeExecute(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     With AtmAdvSearch_Station.Sql Do
     Begin
          Clear;
          Add('Select * From STATIONS');
          Add('Where SEMEL_YESHUV='+Tbl_MusaCITY_SYMBOL.AsString);
     End;
end;


Procedure TFrm_Musa.BuildLookupList;
Var
   SList:TStringList;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_DargatClass),Tbl_MusaLookClass);
     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(ST2_Rashut),Tbl_MusaLookRashutNum);
     FillKodTavlaLookupList(Tbl_KodTavla2,SList);
     SList.Free;


     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_SugMusa),AtmDbComboBox_SugMusa);
     SList.AddObject(IntToStr(SugTavla_SugHinuch),AtmDbComboBox_SugChinuh);
     SList.AddObject(IntToStr(SugTavla_Megama),AtmDbComboBox_Megama);
     SList.AddObject(IntToStr(SugTavla_ZakautHasaa),AtmDbComboBox_ZakautNesia);
     SList.AddObject(IntToStr(SugTavla_Harig),AtmDbComboBox_CodeException);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);

     SList.Clear;
     With DM_AtmCrt.Qry_Temp Do
     Begin
          Active:=False;
          Sql.Clear;
//          Sql.Add('Select Sug_Tavla,Teur_Tavla,Kod_Tavla2'); Old
          Sql.Add('Select Sug_Tavla,Teur_Tavla,Kod_Tavla');
          Sql.Add('From KodTvla2');
          Active:=True;
     End;
     SList.AddObject(IntToStr(ST2_TaarifBus),AtmDbComboBox_BusPrice1);
     SList.AddObject(IntToStr(ST2_TaarifBus),AtmDbComboBox_BusPrice2);
     SList.AddObject(IntToStr(ST2_TaarifBus),AtmDbComboBox_BusPrice3);
     FillKodTavla2ComboList(DM_AtmCrt.Qry_Temp,SList,'Kod_Tavla','Teur_Tavla');
     DM_AtmCrt.Qry_Temp.Active:=False;
     SList.Free;
End;

procedure TFrm_Musa.RunThreadToCheckAddress;
Begin
   if DontRunMslThread Then
      Exit;
   if (Not ThreadCheckAddressEnded) And (ThreadCheckAddress<>Nil) Then
   Begin
      ThreadCheckAddress.Terminate;
        RerutnToProc:=RunThreadToCheckAddress;
        StatusBar1.SimpleText:='���� ������ ����� ����';
        Exit;
      End;
   ThreadCheckAddress:= TThreadCheckAddress.Create(Tbl_MusaCITY_NAME.AsString,Tbl_MusaSTREET_NAME.AsString,Tbl_MusaHOUSE_NUM.AsString);
   ThreadCheckAddress.OnTerminate := ThreadCheckAddressDone;
   StatusBar1.SimpleText:='���� ����� ��������...';
   OkToUpdateNode:=True;
   RerutnToProc:=Nil;
End;

procedure TFrm_Musa.ThreadCheckAddressDone(Sender :TObject);
Begin
     if (ThreadCheckAddressEnded) And (ThreadCheckAddressRes<>1) Then
     Begin
          StatusBar1.SimpleText:='����� ����� '+ThreadCheckAddressReturnNode.NameOfPoint;
          if OkToUpdateNode Then
             PutNodeDataInRecord(ThreadCheckAddressReturnNode);
     End
     Else
     Begin
        if ThreadCheckAddressRes=faLibraryNotLoaded Then
           StatusBar1.SimpleText:=''
        Else
        Begin
             StatusBar1.SimpleText:='����� �� ����� ��������';
             Beep(200,100);
        End;
     End;
     ThreadCheckAddress:=Nil;
     if @RerutnToProc<>Nil Then RerutnToProc;
End;

Procedure TFrm_Musa.PutNodeDataInRecord(TheNode :TNode);
Begin
    Tbl_MusaX.AsInteger:=TheNode.Crd.X;
    Tbl_MusaY.AsInteger:=TheNode.Crd.Y;
    Tbl_MusaNodeNumB.AsInteger:=TheNode.Numb;
    Tbl_MusaNodeSugSystem.AsInteger:=TheNode.SugSystem;
    Tbl_MusaNodeKodCityName.AsInteger:=TheNode.KodCityName;
End;

procedure TFrm_Musa.Tbl_MusaCITY_NAMEValidate(Sender: TField);
begin
    if Not Tbl_MusaCITY_NAME.IsNull Then RunThreadToCheckAddress;
end;

procedure TFrm_Musa.Tbl_MusaSTREET_NAMEValidate(Sender: TField);
begin
     if (Not Tbl_MusaSTREET_NAME.IsNull) And (Not Tbl_MusaCITY_NAME.IsNull) And
        (Not Tbl_MusaHOUSE_NUM.IsNull) Then
        RunThreadToCheckAddress;
end;

procedure TFrm_Musa.Tbl_MusaAfterScroll(DataSet: TDataSet);
begin
     OkToUpdateNode:=False;
end;

procedure TFrm_Musa.Spb_CalcDistFromMosadClick(Sender: TObject);
Var
   TempNode :TNode;
   TempNodeObj :TNodeClass;
   ListOfNodes :TList;
   Dist,Zman :DWord;
begin
     if (Tbl_MusaNodeNumB.IsNull) Or (Tbl_MusaNodeNumB.AsInteger<=0) Then
     Begin
          ShowMessage('�� ����� ����� ���� ����� ��');
          Exit;
     End;
     if Not Tbl_Mosad.FindKey([Tbl_MusaSYMBOL_MOSAD.Value]) Then
     Begin
          ShowMessage('�� ���� ��� ����');
          Exit;
     End;
     if (Tbl_Mosad.FieldByName('NodeNumB').IsNull) Or (Tbl_Mosad.FieldByName('NodeNumB').AsInteger<=0) Then
     Begin
          ShowMessage('�� ����� ����� ���� ����� ��');
          Exit;
     End;

     Try
        ListOfNodes:=TList.Create;
       //����� �����
       TempNode.Numb:=Tbl_MusaNodeNumB.AsInteger;
       TempNode.SugSystem:=Tbl_MusaNodeSugSystem.AsInteger;
       TempNode.KodCityName:=Tbl_MusaNodeKodCityName.AsInteger;
       TempNode.Crd.X:=Tbl_MusaX.AsInteger;
       TempNode.Crd.Y:=Tbl_MusaY.AsInteger;
       TempNodeObj:=TNodeClass.Create(TempNode);
       ListOfNodes.Add(TempNodeObj);
       //����� �����
       TempNode.Numb:=Tbl_Mosad.FieldByName('NodeNumB').AsInteger;
       TempNode.SugSystem:=Tbl_Mosad.FieldByName('NodeSugSystem').AsInteger;
       TempNode.KodCityName:=Tbl_Mosad.FieldByName('NodeKodCityName').AsInteger;
       TempNode.Crd.X:=Tbl_Mosad.FieldByName('X').AsInteger;
       TempNode.Crd.Y:=Tbl_Mosad.FieldByName('Y').AsInteger;
       TempNodeObj:=TNodeClass.Create(TempNode);
       ListOfNodes.Add(TempNodeObj);
       if MslCalcMaslul(ListOfNodes,Dist,Zman,'�') Then
       Begin
          if Tbl_Musa.State=dsBrowse Then
             Tbl_Musa.EDit;
          Tbl_MusaDIST_FROM_MSL.AsInteger:=Dist;
          if Tbl_MusaDISTANCE.IsNull Then
             Tbl_MusaDISTANCE.AsInteger:=Dist;
       End
       Else
           ShowMessage('����� ����');
     Finally
         //Free TList
         While ListOfNodes.Count>0 Do
         Begin
              TempNodeObj:=TNodeClass(ListOfNodes.Items[0]);
              ListOfNodes.Delete(0);
              TempNodeObj.Free;
         End;
         ListOfNodes.Free;
     End;
end;

procedure TFrm_Musa.PageControl1Change(Sender: TObject);
begin
     Tbtn_ChooseMslAddress.Enabled:=(MslDllLoaded) And (PageControl1.ActivePage=TS_Main);
     if Query_Musa.Active Then
        Query_Musa.Active:=False;
     if PageControl1.ActivePage=TS_MusaGrid Then
     Begin
          RunQryMosad('CODE_MUSA');
          DBNavigator.DataSource:=DS_QryMusa;
     End
     Else
         DBNavigator.DataSource:=DS_Musa;

end;

procedure TFrm_Musa.RunQryMosad(OrderByFieldName :String);
Begin
  Try
     Screen.Cursor:=crHourGlass;
      CurSortFieldName:=OrderByFieldName;
      With Query_Musa Do
      Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From MUSA');
           Sql.Add('Order By '+OrderByFieldName);
           Active:=True;
      End;
  Finally
         Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_Musa.RxDBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     if CompareText(Field.FieldName,CurSortFieldName)=0 Then
        Background:=clInfoBk;

end;

procedure TFrm_Musa.RxDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
     RunQryMosad(Field.FieldName);
end;

procedure TFrm_Musa.Lbl_MosadSymDblClick(Sender: TObject);
begin
     OpenAtmCrt(fnMosad,EditSYMBOL_MOSAD.Text);
end;

procedure TFrm_Musa.Tbtn_ChooseMslAddressClick(Sender: TObject);
Var
   TheNode :TNode;
begin
     if MslFindNodeFromDlgSearchPlace(TheNode,'') Then
     Begin
          DontRunMslThread:=True;
          if Tbl_Musa.State=dsBrowse Then
             Tbl_Musa.Edit;
          Tbl_MusaCITY_NAME.AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,1);
          Tbl_MusaSTREET_NAME.AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,2);
          Tbl_MusaHOUSE_NUM.AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,3);

          PutNodeDataInRecord(TheNode);
          DontRunMslThread:=False;
     End;
end;

Procedure TFrm_Musa.ShowForm(FormParams:ShortString);
Begin
     if UPPERCASE(Trim(FormParams))='SOROKA' Then
        ChangeLabelsForSoroka;
     Show;
End;

Procedure TFrm_Musa.ChangeLabelsForSoroka;
Begin
End;
procedure TFrm_Musa.Tbl_MusaSYMBOL_MOSADValidate(Sender: TField);
begin
     if Tbl_Mosad.FindKey([Tbl_MusaSYMBOL_MOSAD.AsString]) Then
        Tbl_MusaSYMBOL_YESHUV_LIMUD.AsString:=Tbl_Mosad.FieldByName('SEMEL_YESHUV').AsString;
end;

procedure TFrm_Musa.RxDBGrid1DblClick(Sender: TObject);
begin
     if Tbl_Musa.FindKey([Query_Musa.FieldByName('CODE_MUSA').Value]) Then
     Begin
        PageControl1.ActivePage:=TS_Main;
        DBNavigator.DataSource:=DS_Musa;
     End;

end;

procedure TFrm_Musa.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msMusa],False,CrtMeholelHandle);
end;

procedure TFrm_Musa.Tbl_MusaCODE_MUSAValidate(Sender: TField);
begin
     if Tbl_Musa.State=dsInsert Then
     With DM_AtmCrt.Qry_Temp Do
     Begin
         Active:=False;
         Sql.Clear;
         Sql.Add('Select Count(*) C From Musa Where Code_Musa='+Sender.AsString);
         Active:=True;
         if FieldByName('C').AsInteger >0 Then
         Begin
              Active:=False;
              MessageDlg('��� �� ���� ������',mtError,[mbOk],0);
              Abort;
         End;
         Active:=False;
     End;
end;

procedure TFrm_Musa.Tbl_MusaSYMBOL_YESHUV_LIMUDValidate(Sender: TField);
begin
{    Tbl_MusaCITY_SYMBOL.AsString:=GetFieldFromSeparateString(AtmAdvSearch_SemelYeshuv.ReturnString,',',2);
    Tbl_MusaCITY_NAME.AsString:=GetFieldFromSeparateString(AtmAdvSearch_SemelYeshuv.ReturnString,',',1);
    Tbl_MusaMISPAR_RASHUT.AsString:=GetFieldFromSeparateString(AtmAdvSearch_SemelYeshuv.ReturnString,',',3);
}
end;

procedure TFrm_Musa.EditCITY_SYMBOLBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  EditCITY_SYMBOL.SearchComponent.ReturnFieldIndex:=1;
end;

procedure TFrm_Musa.EditCITY_NAMEBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  EditCITY_NAME.SearchComponent.ReturnFieldIndex:=0;
end;

procedure TFrm_Musa.Tbl_MusaCITY_SYMBOLValidate(Sender: TField);
begin
  With DM_AtmCrt.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select NameCity From City Where CodeCity='+Sender.AsString);
    Open;
    if Not Eof Then
      Tbl_Musa.FieldByName('CITY_NAME').AsString:=FieldByName('NameCity').AsString;
    Close;
  end;
end;

procedure TFrm_Musa.DBNavigatorBeforeAction(Sender: TObject;
  Button: TmoNavBtns);
begin
  Case Button Of
  nbaPost   :((Sender As TAtmDBNavigator).DataSource.DataSet As TAtmRxQuery).SavePressed:=True;
  nbaRefresh: Begin
               (Sender As TAtmDBNavigator).DataSource.DataSet.Close;
               (Sender As TAtmDBNavigator).DataSource.DataSet.Open;
               BuildLookupList;
               Abort;
             End;
  nbaDelete : if MessageDlg('��� ����� ����� ��',mtConfirmation,[mbYes,mbNo],0)<>mrYes Then
                Abort;
  End; //Case

end;

end.

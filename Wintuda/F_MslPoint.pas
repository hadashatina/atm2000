unit F_MslPoint;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, RXCtrls, ExtCtrls,  Buttons,MslDllInterface;

type
  TFrm_MslPoint = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    RxLabel1: TLabel;
    RxLabel2: TLabel;
    Panel4: TPanel;
    EdtFromPlace: TEdit;
    EdtToPlace: TEdit;
    SG_MslPoints: TStringGrid;
    Panel5: TPanel;
    BitBtn2: TBitBtn;
    RxLabel4: TLabel;
    EdtTotalKm: TEdit;
    BitBtn3: TBitBtn;
    Spb_Yad: TSpeedButton;
    Spb_Motza: TSpeedButton;
    Spb_CalcMaslul: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Spb_MotzaClick(Sender: TObject);
    procedure Spb_YadClick(Sender: TObject);
    procedure Spb_CalcMaslulClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
      private
    { Private declarations }
  public
    AutoDoMaslul :Boolean;
    procedure MslLoop;
    Function  ChooseMotza :Boolean;
    Function  ChooseYad :Boolean;
    Procedure LoopChooseWay;
    Function  ChooseWay(GridLine :LongInt) :Boolean;
    procedure DoMaslul(CalculateMaslul :Boolean);
  end;

Function OpenMslPointForm(TheNodeList :TNodeList;AutoCalcMaslul :Boolean) :Boolean;

var
  Frm_MslPoint: TFrm_MslPoint;

implementation

{$R *.DFM}

Var
  TempNodeObj :TNodeClass;
  ListOfNodes :TList;
  FromNode,ToNode,VarNode:TNode;
  Dist,Zman:DWord;
  CompFlag:Boolean;
  TotalDist:Integer;

procedure TFrm_MslPoint.MslLoop;
begin
  if ChooseMotza Then
     if ChooseYad Then
     Begin
        LoopChooseWay;
        DoMaslul(AutoDoMaslul);
     End;

end;

Function TFrm_MslPoint.ChooseMotza :Boolean;
Begin
     EdtTotalKm.Text:='';
     VarNode:=FromNode;
     if (MslFindNodeFromDlgSearchPlace(VarNode,'����')) Then
     Begin
         EdtFromPlace.Text:=VarNode.NameOfPoint;
         FromNode:=VarNode;
         Result:=True;
     End
     Else
         Result:=False;
End;

Function TFrm_MslPoint.ChooseYad :Boolean;
Begin
//     FillChar(VarNode,SizeOf(VarNode),#0);
     EdtTotalKm.Text:='';
     VarNode:=ToNode;
     if (MslFindNodeFromDlgSearchPlace(VarNode,'���')) then
     Begin
          ToNode:=VarNode;
          EdtToPlace.Text:=VarNode.NameOfPoint;
          Result:=True;
     End
     Else
         Result:=False;
End;

Procedure TFrm_MslPoint.LoopChooseWay;
Begin
     SG_MslPoints.RowCount:=SG_MslPoints.RowCount+1;
     While ChooseWay(SG_MslPoints.RowCount-1) Do
           SG_MslPoints.RowCount:=SG_MslPoints.RowCount+1;
     SG_MslPoints.RowCount:=SG_MslPoints.RowCount-1;
End;

Function TFrm_MslPoint.ChooseWay(GridLine :LongInt) :Boolean;
Begin
     EdtTotalKm.Text:='';
     With SG_MslPoints do
     begin
        if (MslFindNodeFromDlgSearchPlace(VarNode,'���')) then
        Begin
             TempNodeObj:=TNodeClass.Create(VarNode);
             Cells[0,GridLine]:='����� '+IntToStr(GridLine);
             Cells[1,GridLine]:=VarNode.NameOfPoint;
             Objects[0,GridLine]:=TempNodeObj;
             Result:=True;
        End
        Else
            Result:=False;
     end;
End;

procedure TFrm_MslPoint.DoMaslul(CalculateMaslul :Boolean);
Var
   I :LongInt;
  TotalDist:Real;
  CompFlag:Boolean;
Begin
  if ListOfNodes.Count>1 Then
  Begin
     For I:=0 To 1 Do
     Begin
          TNodeClass(ListOfNodes.Items[0]).Free;
          ListOfNodes.Delete(0);
     End;
  End;
  ListOfNodes.Clear;

  ListOfNodes.Add(TNodeClass.Create(FromNode));
  if SG_MslPoints.RowCount>1 Then
     For I:=1 To SG_MslPoints.RowCount-1 Do
         ListOfNodes.Add(SG_MslPoints.Objects[0,I]);
  ListOfNodes.Add(TNodeClass.Create(ToNode));

  if CalculateMaslul Then
  Begin
      TotalDist:=0;
      CompFlag:=FALSE;
      if MslCalcMaslul(ListOfNodes,Dist,Zman,'�') Then
       begin
         CompFlag:=TRUE;
{         for i:=0 to ListOfNodes.Count-1 do
          with TNodeClass(ListOfNodes.Items[i]) do
           if (i < ListOfNodes.Count-1 ) then
            TotalDist:=TotalDist+LDist;}
         TotalDist:=Dist;
         EdtTotalKm.Text:=IntToStr(Round(TotalDist /1000));
       end
      Else
       ShowMessage('����� ����');
  End;
End;

procedure TFrm_MslPoint.FormCreate(Sender: TObject);
begin
  With SG_MslPoints do
   begin
     RowCount:=1;
     Cells[1,0]:='������ ����';
   end;
  AutoDoMaslul:=True;
  ListOfNodes:=TList.Create;
  FillChar(FromNode,SizeOf(FromNode),#0);
  FillChar(ToNode,SizeOf(ToNode),#0);
end;
procedure TFrm_MslPoint.FormDestroy(Sender: TObject);
var
  TempNodeObj :TNodeClass;
begin
  While ListOfNodes.Count>0 Do
    Begin
      TempNodeObj:=TNodeClass(ListOfNodes.Items[0]);
      ListOfNodes.Delete(0);
      TempNodeObj.Free;
    End;
  ListOfNodes.Free;
end;

procedure TFrm_MslPoint.FormActivate(Sender: TObject);
begin
  MslLoop;
end;

procedure TFrm_MslPoint.BitBtn3Click(Sender: TObject);
begin
  Close;
end;

procedure TFrm_MslPoint.Spb_MotzaClick(Sender: TObject);
begin
     ChooseMotza;
end;

procedure TFrm_MslPoint.Spb_YadClick(Sender: TObject);
begin
     ChooseYad;
end;

Function OpenMslPointForm(TheNodeList :TNodeList;AutoCalcMaslul :Boolean) :Boolean;
Var
   I :longInt;
Begin
     Frm_MslPoint:=TFrm_MslPoint.Create(Nil);
     Frm_MslPoint.AutoDoMaslul:=AutoCalcMaslul;
     if TheNodeList.Count>0 Then
     Begin
          FromNode:=TNodeClass(TheNodeList.Items[0]).LNode;
          Frm_MslPoint.EdtFromPlace.Text:=FromNode.NameOfPoint;
          if TheNodeList.Count>2 Then
          Begin
              Frm_MslPoint.SG_MslPoints.RowCount:=TheNodeList.Count-2;
              For I:=1 To TheNodeList.Count-2 Do
              Begin
                  Frm_MslPoint.SG_MslPoints.Objects[0,I]:=TheNodeList.Items[I];
                  Frm_MslPoint.SG_MslPoints.Cells[0,I]:='����� '+IntToStr(I);
                  Frm_MslPoint.SG_MslPoints.Cells[1,I]:=TNodeClass(TheNodeList.Items[I]).LNode.NameOfPoint;
              End;
          End;
          ToNode:=TNodeClass(TheNodeList.Items[TheNodeList.Count-1]).LNode;
          Frm_MslPoint.EdtToPlace.Text:=ToNode.NameOfPoint;
     End;

     if Frm_MslPoint.ShowModal=mrOk Then
     Begin
         if  ((Frm_MslPoint.EdtTotalKm.Text='') Or (ListOfNodes.Count<1)) Then
            Frm_MslPoint.DoMaslul(AutoCalcMaslul);
         TheNodeList.Clear;
         For I:=0 To ListOfNodes.Count-1 Do
             TheNodeList.Add(ListOfNodes.Items[I]);
         Result:=True;
     End
     Else
         Result:=False;
End;

procedure TFrm_MslPoint.Spb_CalcMaslulClick(Sender: TObject);
begin
  DoMaslul(True);
end;

procedure TFrm_MslPoint.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Case Key Of
          VK_F1:Spb_MotzaClick(Self);
          VK_F2:Spb_YadClick(Self);
          VK_F3:Spb_CalcMaslulClick(Self);
     End;
end;

end.

unit Ftavla2;

interface

uses
  Bde, DBTables, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, DB, StdCtrls, DBCtrls,ExtCtrls, Buttons, Scale250, Grids,
  DBGrids,AtmAdvTable;

type
  TFrm_Tavla2 = class(TForm)
    DBGrid_KodTavla: TDBGrid;
    HebDBGrid2: TDBGrid;
    Panel1: TPanel;
    SpeedButton2: TSpeedButton;
    DBNavigator1: TDBNavigator;
    SpeedButton1: TSpeedButton;
    Scale1: TScale;
    Tbl_Kod_Tavla: TAtmTable;
    Tbl_Kod_TavlaSug_Tavla: TIntegerField;
    Tbl_Kod_TavlaKod_Tavla: TIntegerField;
    Tbl_Kod_TavlaTeur_tavla: TStringField;
    Ds_Kod_Tavla: TDataSource;
    Panel2: TPanel;
    HebDBText1: TDBText;
    Tbl_Sug_Tavla: TAtmTable;
    Tbl_Sug_TavlaSug_Tavla: TIntegerField;
    Tbl_Sug_TavlaTeur_tavla: TStringField;
    Ds_Sug_Tavla: TDataSource;
    Tbl_Kod_Temp: TAtmTable;
    Tbl_Kod_TempSug_Tavla: TIntegerField;
    Tbl_Kod_TempKod_Tavla: TIntegerField;
    Tbl_Kod_TempTeur_tavla: TStringField;
    Tbl_Sug_Temp: TAtmTable;
    Tbl_Sug_TempSug_Tavla: TIntegerField;
    Tbl_Sug_TempTeur_tavla: TStringField;
    Tbl_Kod_TavlaDate_Tavla: TDateTimeField;
    Tbl_Kod_TavlaTeur_Tavla2: TStringField;
    Tbl_Sug_TavlaKey_Tavla: TStringField;
    Splitter1: TSplitter;
    Tbl_Kod_TavlaKod_Tavla2: TIntegerField;
    Tbl_Kod_TempKod_Tavla2: TIntegerField;
    Tbl_Kod_TempDate_Tavla: TDateTimeField;
    Tbl_Kod_TempTeur_Tavla2: TStringField;
    procedure HebDBGrid2Enter(Sender: TObject);
    procedure DBGrid_KodTavlaEnter(Sender: TObject);
    procedure HebDBGrid2Exit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Kod_TavlaAfterInsert(DataSet: TDataSet);
    procedure Kod_TavlaBeforePost(DataSet: TDataSet);
    procedure Kod_TavlaAfterPost(DataSet: TDataSet);
    procedure Kod_TavlaBeforeDelete(DataSet: TDataSet);
    procedure Sug_TavlaAfterInsert(DataSet: TDataSet);
    procedure Sug_TavlaBeforePost(DataSet: TDataSet);
    procedure Sug_TavlaAfterPost(DataSet: TDataSet);
    procedure Sug_TavlaBeforeDelete(DataSet: TDataSet);
    procedure Kod_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
              var Action: TDataAction);
    procedure Sug_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
              var Action: TDataAction);
    procedure FormDestroy(Sender: TObject);
    procedure Tbl_Sug_TavlaAfterScroll(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_Tavla2: TFrm_Tavla2;

const
  eKeyViol = 9729;               {���� ����}

implementation


{$R *.DFM}
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.HebDBGrid2Enter(Sender: TObject);
begin            {DataSource - ����� �}
    DBNavigator1.DataSource:=DS_Sug_Tavla;
    DBNavigator1.VisibleButtons := [nbFirst,nbLast,nbNext,nbPrior];
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.DBGrid_KodTavlaEnter(Sender: TObject);
begin
   DBNavigator1.DataSource:=DS_Kod_Tavla;
   DbNavigator1.VisibleButtons := [nbFirst,nbLast,nbNext,nbPrior,nbCancel,nbPost,nbEdit,nbRefresh,nbDelete,nbInsert];
end;
//-----------------------------------------------------------------------
//    ����� ����� ������ ����� ������� ��� ����� ����� ��� �����
procedure TFrm_Tavla2.HebDBGrid2Exit(Sender: TObject);
begin
   if TBL_Sug_Tavla.State in [DsEdit, DsInsert] then
      TBL_sug_Tavla.Post;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.SpeedButton2Click(Sender: TObject);
begin
   Close;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.SpeedButton1Click(Sender: TObject);
begin
  IF DBNavigator1.DataSource=DS_Sug_Tavla  then
     begin
       DBNavigator1.DataSource:=DS_Kod_Tavla;
       DBGrid_KodTavla.SetFocus;
     end
  else
     begin
       DBNavigator1.DataSource:=DS_Sug_Tavla;
       HebDBGrid2.SetFocus;
     end;
end;
//-----------------------------------------------------------------------
//            Form - � DataMode.Ttable - ����� ��������� ��
procedure TFrm_Tavla2.FormCreate(Sender: TObject);
Var
   I :longInt;
begin
   For I:=0 To ComponentCount-1 Do
   Begin
        if Components[I] is TTable Then
           (Components[I] As TTable).Open;
   End;

   Tbl_Kod_Tavla.AfterInsert := Kod_TavlaAfterInsert;
   Tbl_Kod_Tavla.BeforePost  := Kod_TavlaBeforePost;
   Tbl_Kod_Tavla.AfterPost   := Kod_TavlaAfterPost;
   Tbl_Kod_Tavla.BeforeDelete:= Kod_TavlaBeforeDelete;
   Tbl_Sug_Tavla.AfterInsert := Sug_TavlaAfterInsert;
   Tbl_Sug_Tavla.BeforePost  := Sug_TavlaBeforePost;
   Tbl_Sug_Tavla.AfterPost   := Sug_TavlaAfterPost;
   Tbl_Sug_Tavla.BeforeDelete:= Sug_TavlaBeforeDelete;
   Tbl_Kod_Tavla.OnPostError := Kod_TavlaOnPostError;
   Tbl_Sug_Tavla.OnPostError := Sug_TavlaOnPostError;


end;
//-----------------------------------------------------------------------
//                       ���� ��� ����� ���
procedure TFrm_Tavla2.Kod_TavlaAfterInsert(DataSet: TDataSet);
begin
   Tbl_Kod_Temp.Filter:= 'Sug_Tavla='+IntToStr(Tbl_Sug_TavlaSug_Tavla.Value);
   Tbl_Kod_Temp.Last;
   Tbl_Kod_TavlaKod_Tavla.AsInteger:=Tbl_Kod_TempKod_Tavla.AsInteger+1;
end;
//-----------------------------------------------------------------------
//           ����� ���� ���� �� ������ �� ���� ��� ������
procedure TFrm_Tavla2.Kod_TavlaBeforePost(DataSet: TDataSet);
begin
   if (Ds_Kod_Tavla.State = DsInsert)         and
       (Tbl_Kod_TavlaTeur_Tavla.AsString = '') then
        begin
          Tbl_Kod_Tavla.Cancel;
          Tbl_Kod_Tavla.Edit;
        end;

   if (Tbl_Kod_TavlaSug_Tavla.Value = 26) and
    ((Tbl_Kod_TavlaKod_Tavla.Value = 1)  or
     (Tbl_Kod_TavlaKod_Tavla.Value = 2)  or
     (Tbl_Kod_TavlaKod_Tavla.Value = 3)  or
     (Tbl_Kod_TavlaTeur_Tavla.Value = '�����')  or
     (Tbl_Kod_TavlaTeur_Tavla.Value = '�����')  or
     (Tbl_Kod_TavlaTeur_Tavla.Value = '�����')) then
     begin
       Showmessage ('�� ���� ����� ����� ���');
       Tbl_Kod_Tavla.Cancel;
       Tbl_Kod_Tavla.Edit;
     end;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.Kod_TavlaAfterPost(DataSet: TDataSet);
begin
  Dbisavechanges (Tbl_Kod_Tavla.handle);
  DataSet.Refresh;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.Sug_TavlaAfterPost(DataSet: TDataSet);
begin
   Dbisavechanges (Tbl_Sug_Tavla.handle);
   DataSet.Refresh;
end;
//-----------------------------------------------------------------------
//                  ���� ��� ����� ���
procedure TFrm_Tavla2.Sug_TavlaAfterInsert(DataSet: TDataSet);
begin
   Tbl_Sug_Temp.Last;
   Tbl_Sug_TavlaSug_Tavla.AsInteger:= Tbl_Sug_TempSug_Tavla.AsInteger+1;
end;
//-----------------------------------------------------------------------
//           ����� ���� ���� �� ������ �� ���� ��� ������
procedure TFrm_Tavla2.Sug_TavlaBeforePost(DataSet: TDataSet);
begin
   if (Ds_Sug_Tavla.State = DsInsert)         and
      (Tbl_Sug_TavlaTeur_Tavla.AsString = '') then
      begin
        Tbl_Sug_Tavla.Cancel;
        Tbl_Sug_Tavla.Edit;
      end;
end;
//-----------------------------------------------------------------------
//  Master > Dedails ����� ������ ��� - ��� ���� ������ ���� ���� ���
procedure TFrm_Tavla2.Sug_TavlaBeforeDelete(DataSet: TDataSet);
begin
  if MessageDlg('? ��� ���� �� ������', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
     begin
       while TBL_Kod_Tavla.RecordCount > 0 do
             TBL_Kod_Tavla.Delete;
     end
  else
     Abort;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.Kod_TavlaBeforeDelete(DataSet: TDataSet);
begin
   if MessageDlg('? ��� ���� �� ������', mtConfirmation,[mbYes, mbNo], 0) = mrNo then
      Abort;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.Kod_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (E is EDBEngineError) then
     if (E as EDBEngineError).Errors[0].Errorcode = eKeyViol then
        begin
          MessageDlg ('���� ����', mtWarning, [mbOK],0);
          Abort;
        end;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.Sug_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (E is EDBEngineError) then
     if (E as EDBEngineError).Errors[0].Errorcode = eKeyViol then
        begin
          MessageDlg ('���� ����', mtWarning, [mbOK],0);
          Abort;
        end;
end;
//-----------------------------------------------------------------------
procedure TFrm_Tavla2.FormDestroy(Sender: TObject);
Var
   I :longInt;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
end;

procedure TFrm_Tavla2.Tbl_Sug_TavlaAfterScroll(DataSet: TDataSet);
begin
     Tbl_Kod_Tavla.IndexFieldNames:=Tbl_Sug_Tavla.FieldByName('Key_Tavla').AsString;
     DBGrid_KodTavla.Columns[0].Visible:=Not (Tbl_Sug_Tavla.FieldByName('Key_Tavla').AsString<>'');
//     DBGrid_KodTavla.Columns[1].Visible:=Tbl_Sug_Tavla.FieldByName('Key_Tavla').AsString<>'';
     DBGrid_KodTavla.Columns[2].Visible:=Tbl_Sug_Tavla.FieldByName('Key_Tavla').AsString<>'';
end;

procedure TFrm_Tavla2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Tavla2:=Nil;
     Action:=caFree;
end;

end.

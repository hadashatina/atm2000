unit F_CopyTnua;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBFilter, DBTables, RxQuery, AtmTabSheetBuild, ExtCtrls, StdCtrls,
  Buttons, Grids, DBGrids, RXDBCtrl, Mask, ToolEdit, ComCtrls, CheckLst,
  AtmRxQuery;

type
  TFrm_CopyTnua = class(TForm)
    Panel_Bottom: TPanel;
    Panel_Grid: TPanel;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery1: TRxQuery;
    RxDBFilter1: TRxDBFilter;
    DataSource1: TDataSource;
    Btn_ShowGrid: TBitBtn;
    UpdateSQL1: TUpdateSQL;
    Btn_DoCopy: TBitBtn;
    Panel_Top: TPanel;
    DateEdit_DDate: TDateEdit;
    Label1: TLabel;
    Btn_Cancel: TBitBtn;
    ProgressBar1: TProgressBar;
    CB_DoDays: TCheckBox;
    Splitter1: TSplitter;
    Lbl_DayOfweek: TLabel;
    Btn_SelectAll: TBitBtn;
    Tbl_Shib: TAtmRxQuery;
    Tbl_ShibShibAzmnNo: TIntegerField;
    Tbl_ShibShibAzmnDate: TDateTimeField;
    Tbl_ShibShibKind: TIntegerField;
    Tbl_ShibShibNo: TAutoIncField;
    Tbl_ShibShibBeginTime: TDateTimeField;
    Tbl_ShibShibEndTime: TDateTimeField;
    Tbl_ShibHovalaKind: TIntegerField;
    Tbl_ShibPriceKind: TIntegerField;
    Tbl_ShibShibTotalKm: TIntegerField;
    Tbl_ShibLakNo1: TIntegerField;
    Tbl_ShibLakNo2: TIntegerField;
    Tbl_ShibLakNo3: TIntegerField;
    Tbl_ShibPriceIsGlobal: TIntegerField;
    Tbl_ShibMaslulCode1: TIntegerField;
    Tbl_ShibMaslul1: TStringField;
    Tbl_ShibYeMida1: TIntegerField;
    Tbl_ShibYeMida2: TIntegerField;
    Tbl_ShibYeMida3: TIntegerField;
    Tbl_ShibYeMida4: TIntegerField;
    Tbl_ShibPrice1: TBCDField;
    Tbl_ShibPrice2: TBCDField;
    Tbl_ShibPrice3: TBCDField;
    Tbl_ShibPrice4: TBCDField;
    Tbl_ShibShibRem2: TStringField;
    Tbl_ShibShibRem3: TStringField;
    Tbl_ShibShibRem4: TStringField;
    Tbl_ShibQuntity1: TBCDField;
    Tbl_ShibQuntity2: TBCDField;
    Tbl_ShibQuntity3: TBCDField;
    Tbl_ShibQuntity4: TBCDField;
    Tbl_ShibPriceQuntity1: TBCDField;
    Tbl_ShibPriceQuntity2: TBCDField;
    Tbl_ShibPriceQuntity3: TBCDField;
    Tbl_ShibPriceQuntity4: TBCDField;
    Tbl_ShibDriverNo1: TIntegerField;
    Tbl_ShibDriverNo2: TIntegerField;
    Tbl_ShibDriverName: TStringField;
    Tbl_ShibCarNo: TIntegerField;
    Tbl_ShibCarNum: TStringField;
    Tbl_ShibLakCodeBizua: TIntegerField;
    Tbl_ShibLakHesbonit: TIntegerField;
    Tbl_ShibLakHesbonitDate: TDateTimeField;
    Tbl_ShibDrishNo: TIntegerField;
    Tbl_ShibDrishDate: TDateTimeField;
    Tbl_ShibDriverCodeBizua: TIntegerField;
    Tbl_ShibDriverZikuiNo: TIntegerField;
    Tbl_ShibDriverZikuiDate: TDateTimeField;
    Tbl_ShibCarCodeBizua: TIntegerField;
    Tbl_ShibCarZikuiNo: TIntegerField;
    Tbl_ShibCarZikuiDate: TDateTimeField;
    Tbl_ShibNefh: TBCDField;
    Tbl_ShibTrvlNo: TIntegerField;
    Tbl_ShibStarMetanNo: TIntegerField;
    Tbl_ShibGpsStatus: TIntegerField;
    Tbl_ShibGpsNo: TIntegerField;
    Tbl_ShibSugMetan: TIntegerField;
    Tbl_ShibShibTuda: TIntegerField;
    Tbl_ShibShibCarKind: TIntegerField;
    Tbl_ShibDriverMsg: TSmallintField;
    Tbl_ShibAzmnOk: TSmallintField;
    Tbl_ShibTnuaOk: TSmallintField;
    Tbl_ShibHasbonitOk: TSmallintField;
    Tbl_ShibFirstSpido: TIntegerField;
    Tbl_ShibLastSpido: TIntegerField;
    Tbl_ShibTudaKind: TIntegerField;
    Tbl_ShibNegrrNo: TIntegerField;
    Tbl_ShibUpdateRecordDate: TDateTimeField;
    Tbl_ShibMaklidName: TStringField;
    Tbl_ShibShibAsmcta: TIntegerField;
    Tbl_ShibShibRem1: TMemoField;
    Tbl_ShibQuntMusa: TBCDField;
    Tbl_ShibTotalHour: TBCDField;
    Tbl_ShibTotalPointsInWay: TIntegerField;
    Tbl_ShibShibFromPalce: TStringField;
    Tbl_ShibShibToPalce: TStringField;
    Tbl_ShibShibTotalTime: TDateTimeField;
    Tbl_ShibXBegin: TIntegerField;
    Tbl_ShibYBegin: TIntegerField;
    Tbl_ShibNodeNumBBegin: TIntegerField;
    Tbl_ShibNodeSugSystemBegin: TIntegerField;
    Tbl_ShibNodeKodCityNameBegin: TIntegerField;
    Tbl_ShibXEnd: TIntegerField;
    Tbl_ShibYEnd: TIntegerField;
    Tbl_ShibNodeNumBEnd: TIntegerField;
    Tbl_ShibNodeSugSystemEnd: TIntegerField;
    Tbl_ShibNodeKodCityNameEnd: TIntegerField;
    Tbl_ShibAmil: TStringField;
    Tbl_ShibGateTuda: TIntegerField;
    Tbl_ShibTik: TIntegerField;
    Tbl_ShibCont1: TStringField;
    Tbl_ShibCont2: TStringField;
    Tbl_ShibBoxes: TIntegerField;
    Tbl_ShibDamaged: TSmallintField;
    Tbl_ShibHoze: TIntegerField;
    Tbl_ShibWaitTime: TDateTimeField;
    Tbl_ShibUnLoadTime: TDateTimeField;
    Tbl_ShibTimeWinFrom: TDateTimeField;
    Tbl_ShibTimeWinTo: TDateTimeField;
    Tbl_ShibMrakz: TStringField;
    Tbl_ShibMishkal: TBCDField;
    Tbl_ShibCodeMahzeva: TIntegerField;
    Tbl_ShibYehusYear: TIntegerField;
    Tbl_ShibYehusMonth: TIntegerField;
    Tbl_Azmn: TAtmRxQuery;
    Tbl_AzmnAzmnNo: TIntegerField;
    Tbl_AzmnAzmnDate: TDateTimeField;
    Tbl_AzmnInvLakNo: TIntegerField;
    Tbl_AzmnGetLakNo: TIntegerField;
    Tbl_AzmnAsmcta: TIntegerField;
    Tbl_AzmnTuda: TIntegerField;
    Tbl_AzmnFromDate: TDateTimeField;
    Tbl_AzmnToDate: TDateTimeField;
    Tbl_AzmnFromTime: TDateTimeField;
    Tbl_AzmnToTime: TDateTimeField;
    Tbl_AzmnAzmnType: TIntegerField;
    Tbl_AzmnRem2: TStringField;
    Tbl_AzmnRem3: TStringField;
    Tbl_AzmnRem4: TStringField;
    Tbl_AzmnTotalPrice1: TBCDField;
    Tbl_AzmnTotalPrice2: TBCDField;
    Tbl_AzmnTotalPrice3: TBCDField;
    Tbl_AzmnTotalKm: TIntegerField;
    Tbl_AzmnSunday: TSmallintField;
    Tbl_AzmnMonday: TSmallintField;
    Tbl_AzmnTuesday: TSmallintField;
    Tbl_AzmnWednesday: TSmallintField;
    Tbl_AzmnThursday: TSmallintField;
    Tbl_AzmnFriday: TSmallintField;
    Tbl_AzmnSaturday: TSmallintField;
    Tbl_AzmnDeleteDate: TDateTimeField;
    Tbl_AzmnOldAzmnType: TIntegerField;
    Tbl_AzmnAzmnCarKind: TIntegerField;
    Tbl_AzmnAzmnSugMetan: TIntegerField;
    Tbl_AzmnMkbleName: TStringField;
    Tbl_AzmnAzmnQuntity: TBCDField;
    Tbl_AzmnQuntity1: TBCDField;
    Tbl_AzmnInvName: TStringField;
    Tbl_AzmnDirshNo: TIntegerField;
    Tbl_AzmnFirstAzmnNo: TIntegerField;
    Tbl_AzmnAzmnMaslulCode1: TIntegerField;
    Tbl_AzmnAzmnMaslulCode2: TIntegerField;
    Tbl_AzmnAzmnMaslulCode3: TIntegerField;
    Tbl_AzmnAzmnMaslulCode4: TIntegerField;
    Tbl_AzmnAzmnMaslul1: TStringField;
    Tbl_AzmnAzmnMaslul2: TStringField;
    Tbl_AzmnAzmnMaslul3: TStringField;
    Tbl_AzmnAzmnMaslul4: TStringField;
    Tbl_AzmnFromPlace: TStringField;
    Tbl_AzmnToPlace: TStringField;
    Tbl_AzmnPratim: TStringField;
    Tbl_AzmnAzmnCarNo: TIntegerField;
    Tbl_AzmnAzmnDriverNo: TIntegerField;
    Tbl_AzmnAzmnCarNum: TStringField;
    Tbl_AzmnCodeMhiron: TIntegerField;
    Tbl_AzmnMitzar: TIntegerField;
    Tbl_AzmnSochen: TStringField;
    Tbl_AzmnRshimon: TIntegerField;
    Tbl_AzmnOnia: TStringField;
    Tbl_AzmnAflagaNo: TStringField;
    Tbl_AzmnAflagaPlace: TStringField;
    Tbl_AzmnNefh: TBCDField;
    Tbl_AzmnPutPlace: TStringField;
    Tbl_AzmnFindPlace: TStringField;
    Tbl_AzmnDamaged: TSmallintField;
    Tbl_AzmnAddr: TStringField;
    Tbl_AzmnAddrNo: TIntegerField;
    Tbl_AzmnCity: TStringField;
    Tbl_AzmnPhone: TStringField;
    Tbl_AzmnTotalTime: TDateTimeField;
    Tbl_AzmnMishkal: TBCDField;
    Tbl_AzmnTotalZikuim: TBCDField;
    Tbl_AzmnCALENDARKIND: TIntegerField;
    Tbl_AzmnXBegin: TIntegerField;
    Tbl_AzmnYBegin: TIntegerField;
    Tbl_AzmnNodeNumBBegin: TIntegerField;
    Tbl_AzmnNodeSugSystemBegin: TIntegerField;
    Tbl_AzmnNodeKodCityNameBegin: TIntegerField;
    Tbl_AzmnXEnd: TIntegerField;
    Tbl_AzmnYEnd: TIntegerField;
    Tbl_AzmnNodeNumBEnd: TIntegerField;
    Tbl_AzmnNodeSugSystemEnd: TIntegerField;
    Tbl_AzmnNodeKodCityNameEnd: TIntegerField;
    Tbl_AzmnHovalaKind: TIntegerField;
    Tbl_AzmnMaklidName: TStringField;
    Tbl_AzmnUpdateDate: TDateTimeField;
    Tbl_AzmnCont1: TStringField;
    Tbl_AzmnCont2: TStringField;
    Tbl_AzmnTotalAfterDiscount: TBCDField;
    Tbl_AzmnTotalWithMam: TBCDField;
    UpdateSQL_Azmn: TUpdateSQL;
    UpdateSQL_Shib: TUpdateSQL;
    procedure Btn_ShowGridClick(Sender: TObject);
    procedure Btn_DoCopyClick(Sender: TObject);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DateEdit_DDateAcceptDate(Sender: TObject;
      var ADate: TDateTime; var Action: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure AtmTabSheetBuild1GetCellParams(Sender: TObject;
      Field: TField; AFont: TFont; var Background: TColor;
      Highlight: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure RxQuery1AfterEdit(DataSet: TDataSet);
    procedure AtmTabSheetBuild1ChangeSort(Sender: TObject;
      SortFieldsList: TStringList; var Continue: Boolean);
    procedure Btn_SelectAllClick(Sender: TObject);
    procedure RxQuery1AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    StartAzmn,EndAzmn :longInt;
    DataChanged       :Boolean;
    Procedure CreateNewTnua(HazList:TStringList);
    Procedure CreateNewHazmana(Var CurHazNum:LongInt);
    procedure GridBtnClick(Sender: TObject);
    procedure CalcShibTotalPrices;
    Procedure FindNextHazmanaNum;
  public
    { Public declarations }
    CopiedRecords :TList;
    CopyMethod :LongInt;//0-Fix Azmn To Regular, 1-Hatzaot Mehir To Hazmana
    HatzaaNumberToProcess :longInt;
    Procedure CodeNehagValidate(Sender: TField);
  end;

var
  Frm_CopyTnua: TFrm_CopyTnua;

implementation

uses DMWinTuda,AtmRutin, F_GlblWinTuda, AtmConst;

{$R *.DFM}

procedure TFrm_CopyTnua.Btn_ShowGridClick(Sender: TObject);
Var
   I :LongInt;
begin
     CopiedRecords.Clear;
     AtmTabSheetBuild1.BuildDbGrid(Panel_Grid);
     AtmTabSheetBuild1.CurRxDbGrid.MultiSelect:=True;
     AtmTabSheetBuild1.CurRxDbGrid.Options:=AtmTabSheetBuild1.CurRxDbGrid.Options+[dgMultiSelect];
     AtmTabSheetBuild1.RunQuery('ShibAzmnNo');

     For I:=0 To AtmTabSheetBuild1.CurRxDbGrid.Columns.Count-1 Do
         if (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'CarNum')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'LakNo1')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'Shem_Lakoach')=0) Or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'DriverNo1')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'DriverName')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'SugMetan')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'TeurHiuv')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'Maslul1')=0) or
            (CompareText(AtmTabSheetBuild1.CurRxDbGrid.Columns[i].FieldName,'MaslulCode1')=0) Then
                AtmTabSheetBuild1.CurRxDbGrid.Columns[i].ButtonStyle:=cbsEllipsis;

     AtmTabSheetBuild1.CurRxDbGrid.OnEditButtonClick:=GridBtnClick;
     Btn_DoCopy.Enabled:=True;
     Btn_SelectAll.Enabled:=True;
end;

procedure TFrm_CopyTnua.Btn_DoCopyClick(Sender: TObject);
Var
   I :longInt;
   HazList :TStringList;
begin
  Screen.Cursor:=crHourGlass;
//  FormStyle:=fsStayOnTop;
  StartAzmn:=-1;
  HazList:=TStringList.Create;
{  DM_WinTuda.Tbl_ShibShibAzmnDate.OnValidate:=Nil;
  DM_WinTuda.Tbl_ShibCarNum.OnValidate:=Nil;
  DM_WinTuda.Tbl_ShibDriverNo1.OnValidate:=Nil;}
  if AtmTabSheetBuild1.Query.State in [dsEdit,dsInsert] Then
     AtmTabSheetBuild1.Query.Post;
  Tbl_Azmn.Open;
  Tbl_Shib.Open;
  Try
{     DM_WinTuda.Tbl_Shib.DisableControls;
     DM_WinTuda.Tbl_Azmn.DisableControls;}
     ProgressBar1.Max:=AtmTabSheetBuild1.CurRxDbGrid.SelectedRows.Count;
     While AtmTabSheetBuild1.CurRxDbGrid.SelectedRows.Count>0 Do
     Begin
         ProgressBar1.StepIt;
         AtmTabSheetBuild1.Query.GotoBookmark(TBookmark(AtmTabSheetBuild1.CurRxDbGrid.SelectedRows[0]));
         Application.ProcessMessages;
         CreateNewTnua(HazList);
         Show;
         Application.ProcessMessages;
         AtmTabSheetBuild1.CurRxDbGrid.ToggleRowSelection;
         CopiedRecords.Add(AtmTabSheetBuild1.Query.GetBookmark);
     End;
  Finally
{         DM_WinTuda.Tbl_Shib.EnableControls;
         DM_WinTuda.Tbl_Azmn.EnableControls;}
         FormStyle:=fsNormal;
         Show;
         HazList.Free;
{         DM_WinTuda.Tbl_ShibShibAzmnDate.OnValidate:=DM_WinTuda.Tbl_ShibShibAzmnDateValidate;
         DM_WinTuda.Tbl_ShibCarNum.OnValidate:=DM_WinTuda.Tbl_ShibCarNumValidate;
         DM_WinTuda.Tbl_ShibDriverNo1.OnValidate:=DM_WinTuda.Tbl_ShibDriverNo1Validate;}
         Tbl_Azmn.Close;
         Tbl_Shib.Close;
         Screen.Cursor:=crDefault;
         MessageDlg('����� ������ ���� '+IntToStr(StartAzmn)+'-'+IntToStr(EndAzmn),mtInformation,[mbOk],0);
  End;
end;

Procedure TFrm_CopyTnua.CreateNewTnua(HazList:TStringList);
Var
   CurHazNum,Ind :LongInt;
   YY,MM,DD :Word;
Begin
     DecodeDate(DateEdit_DDate.Date,YY,MM,DD);
     CurHazNum:=AtmTabSheetBuild1.Query.FieldByName('ShibAzmnNo').AsInteger;
     Ind:=HazList.IndexOfName(IntToStr(CurHazNum));
     if Ind>-1 Then
        CurHazNum:=StrToInt(HazList.Values[IntToStr(CurHazNum)])
     Else
     Begin
         CreateNewHazmana(CurHazNum);
         HazList.Add(AtmTabSheetBuild1.Query.FieldByName('ShibAzmnNo').AsString+'='+IntToStr(CurHazNum));
     End;
     if StartAzmn=-1 Then
        StartAzmn:=CurHazNum;
     EndAzmn:=CurHazNum;
     CopyOneRecord(AtmTabSheetBuild1.Query,Tbl_Shib,False,True); //����� ����� ����� �����
//     With Dm_WinTuda Do
     Begin
          CalcShibTotalPrices;
          Tbl_Shib.FieldByName('ShibAzmnNo').AsInteger:=CurHazNum;
          Tbl_Shib.FieldByName('ShibAzmnDate').AsDateTime:=DateEdit_DDate.Date;
          Tbl_Shib.FieldByName('ShibKind').AsInteger:=skNormal;
          Tbl_Shib.FieldByName('AzmnOk').AsInteger:=1;
          Tbl_Shib.FieldByName('YehusYear').AsInteger:=YY;
          Tbl_Shib.FieldByName('YehusMonth').AsInteger:=MM;
          Tbl_Shib.SavePressed:=True;
          Tbl_Shib.Post;
     End;
End;

Procedure TFrm_CopyTnua.CreateNewHazmana(Var CurHazNum:LongInt);
Begin
//   With DM_WinTuda Do
   Begin
       Tbl_Azmn.Append;
       FindNextHazmanaNum;
       Tbl_Azmn.FieldByName('InvLakNo').AsInteger:=AtmTabSheetBuild1.Query.FieldByName('LakNo1').AsInteger;
       Tbl_Azmn.FieldByName('InvName').AsString:=AtmTabsheetBuild1.Query.FieldByName('Shem_Lakoach').AsString;
       Tbl_Azmn.FieldByName('FromDate').AsDateTime:=DateEdit_DDate.Date;{AtmTabSheetBuild1.Query.FieldByName('ShibAzmnDate').AsDateTime;}
       Tbl_Azmn.FieldByName('ToDate').AsDateTime:=DateEdit_DDate.Date;//AtmTabSheetBuild1.Query.FieldByName('ShibAzmnDate').AsDateTime;
       if CopyMethod=1 Then //����� ���� �������
       Begin
         Tbl_Azmn.FieldByName('Asmcta').AsInteger:=AtmTabsheetBuild1.Query.FieldByName('AzmnAsmcta').AsInteger;
         Tbl_Azmn.FieldByName('Phone').AsInteger:=AtmTabsheetBuild1.Query.FieldByName('Phone').AsInteger;
         Tbl_Azmn.FieldByName('TotalPrice3').AsInteger:=AtmTabsheetBuild1.Query.FieldByName('ShaarDolar').AsInteger;
       End;
       Tbl_Azmn.SavePressed:=True;
       Tbl_Azmn.Post;
       CurHazNum:=Tbl_Azmn.FieldByName('AzmnNo').AsInteger;
   End;
End;

procedure TFrm_CopyTnua.AtmTabSheetBuild1BeforeExecuteQuery(
  Sender: TObject);
Var
   StrH :String;
   I :LongInt;
begin
  DataChanged:=False;
  if (CopyMethod = 0) Then //������ ������ ��������
  Begin
    ShortDateFormat:='MM/DD/YYYY';
    Try
      StrH:=DateToStr(DateEdit_DDate.Date);
      RxQuery1.MacroByName('DestDate').AsString:=''''+StrH+'''';
    Finally
      ShortDateFormat:='DD/MM/YYYY';
    End;

    StrH:='';
    if CB_DoDays.Checked Then
    Begin
       Case DayOfWeek(DateEdit_DDate.Date) Of
         1:StrH:=StrH+' And Sunday=1';
         2:StrH:=StrH+' And Monday=1';
         3:StrH:=StrH+' And Tuesday=1';
         4:StrH:=StrH+' And Wednesday=1';
         5:StrH:=StrH+' And Thursday=1';
         6:StrH:=StrH+' And Friday=1';
         7:StrH:=StrH+' And Saturday=1';
       End; //Case;
    End;
    RxQuery1.MacroByName('DayIsTrue').AsString:=StrH;
  End
  Else //CopyMethod=1 ����� ���� �������
  Begin
   RxQuery1.MacroByName('MAzmnNo').AsString:=IntToStr(HatzaaNumberToProcess);
  End;
End;


procedure TFrm_CopyTnua.Btn_CancelClick(Sender: TObject);
begin
     Close;
end;

procedure TFrm_CopyTnua.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Frm_CopyTnua:=Nil;
    Action:=caFree;
    if RxQuery1.Active Then
    Begin
      DM_WinTuda.RefreshMainQuerys;
      DM_WinTuda.Tbl_Azmn.Last;
    End;
end;

procedure TFrm_CopyTnua.DateEdit_DDateAcceptDate(Sender: TObject;
  var ADate: TDateTime; var Action: Boolean);
begin
     if AtmTabSheetBuild1.Query.Active Then
        AtmTabSheetBuild1.Query.Active:=False;
     Btn_ShowGrid.Enabled:=True;
     Lbl_DayOfweek.Caption:='������ �'+LongDayNames[DayOfWeek(ADate)];
end;

procedure TFrm_CopyTnua.GridBtnClick(Sender: TObject);
Begin
     if CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'CarNum')=0 Then
        if Frm_GlblWinTuda.AtmAdvSearch_Rehev.Execute Then
        Begin
           if TRxDbGrid(Sender).DataSource.DataSet.State=dsBrowse Then
              TRxDbGrid(Sender).DataSource.DataSet.Edit;
           TRxDbGrid(Sender).SelectedField.AsString:=Frm_GlblWinTuda.AtmAdvSearch_Rehev.ReturnString;
        End;

     if (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'LakNo1')=0) or
        (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'Shem_Lakoach')=0) Then
        if Frm_GlblWinTuda.AtmAdvSearch_Lako.Execute Then
        Begin
           if TRxDbGrid(Sender).DataSource.DataSet.State=dsBrowse Then
              TRxDbGrid(Sender).DataSource.DataSet.Edit;
           TRxDbGrid(Sender).DataSource.DataSet.FieldByName('LakNo1').AsString:=Frm_GlblWinTuda.AtmAdvSearch_Lako.ReturnString;
           if DM_WinTuda.Tbl_Lako.FindKey([StrTOInt(Frm_GlblWinTuda.AtmAdvSearch_Lako.ReturnString)]) Then
              TRxDbGrid(Sender).DataSource.DataSet.FieldByName('Shem_Lakoach').AsString:=DM_WinTuda.Tbl_Lako.FieldByName('Shem_Lakoach').AsString;
        End;

     if (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'DriverNo1')=0) or
        (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'DriverName')=0) Then
        if Frm_GlblWinTuda.AtmAdvSearch_Nehag.Execute Then
        Begin
           if TRxDbGrid(Sender).DataSource.DataSet.State=dsBrowse Then
              TRxDbGrid(Sender).DataSource.DataSet.Edit;
           TRxDbGrid(Sender).DataSource.DataSet.FieldByName('DriverNo1').AsString:=Frm_GlblWinTuda.AtmAdvSearch_Nehag.ReturnString;
        End;

     if (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'SugMetan')=0) or
        (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'TeurHiuv')=0) Then
     Begin
        TQuery(Frm_GlblWinTuda.AtmAdvSearch_KodTavla.SourceDataSet).ParamByName('PSugTavla').AsInteger:=SugTavla_SugMitan;
        if Frm_GlblWinTuda.AtmAdvSearch_KodTavla.Execute Then
        Begin
           if TRxDbGrid(Sender).DataSource.DataSet.State=dsBrowse Then
              TRxDbGrid(Sender).DataSource.DataSet.Edit;
           TRxDbGrid(Sender).DataSource.DataSet.FieldByName('SugMetan').AsString:=Frm_GlblWinTuda.AtmAdvSearch_KodTavla.ReturnString;
           if DM_WinTuda.TBL_KODTAVLA.FindKey([SugTavla_SugMitan,StrToInt(Frm_GlblWinTuda.AtmAdvSearch_KodTavla.ReturnString)]) Then
              TRxDbGrid(Sender).DataSource.DataSet.FieldByName('TeurHiuv').AsString:=DM_WinTuda.TBL_KODTAVLA.FieldByName('Teur_Tavla').AsString;
        End;
     End;

     if (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'MaslulCode1')=0) or
        (CompareText(TRxDbGrid(Sender).SelectedField.FieldName,'Maslul1')=0) Then
        if Frm_GlblWinTuda.AtmAdvSearch_Maslul.Execute Then
        Begin
           if TRxDbGrid(Sender).DataSource.DataSet.State=dsBrowse Then
              TRxDbGrid(Sender).DataSource.DataSet.Edit;
           TRxDbGrid(Sender).DataSource.DataSet.FieldByName('MaslulCode1').AsString:=Frm_GlblWinTuda.AtmAdvSearch_Maslul.ReturnString;
           if DM_WinTuda.Tbl_Maslul.FindKey([StrTOInt(Frm_GlblWinTuda.AtmAdvSearch_Maslul.ReturnString)]) Then
              TRxDbGrid(Sender).DataSource.DataSet.FieldByName('Maslul1').AsString:=DM_WinTuda.Tbl_Maslul.FieldByName('Name').AsString;
        End;
End;

procedure TFrm_CopyTnua.FormCreate(Sender: TObject);
begin
  CopyMethod:=0;//������ ������ ��������
  AtmTabSheetBuild1.ScriptsDir:=DirectoryForScripts;
  CopiedRecords:=TList.Create;
end;

procedure TFrm_CopyTnua.AtmTabSheetBuild1GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
Var
   I:LongInt;
   CurBookMark :TBookMark;
begin
     CurBookMark:=AtmTabSheetBuild1.Query.GetBookMark;
     For I:=0 To CopiedRecords.Count-1 Do
         if AtmTabSheetBuild1.Query.CompareBookMarks(CurBookMark,TBookmark(CopiedRecords[I]))=0 Then
         Begin
            Background:=clRed;
            Break;
         End;
end;

procedure TFrm_CopyTnua.FormDestroy(Sender: TObject);
begin
      CopiedRecords.Clear;
      CopiedRecords.Free;
end;

procedure TFrm_CopyTnua.RxQuery1AfterEdit(DataSet: TDataSet);
begin
     DataChanged:=True;
end;

procedure TFrm_CopyTnua.AtmTabSheetBuild1ChangeSort(Sender: TObject;
  SortFieldsList: TStringList; var Continue: Boolean);
begin
     if DataChanged Then
        Continue:=MessageDlg('����� ����� ����� ����� �� �������� ����� �� �����. ��� ������ ������',mtConfirmation,[mbYes,mbNo],0)=mrYes;
end;

procedure TFrm_CopyTnua.Btn_SelectAllClick(Sender: TObject);
begin
  if AtmTabSheetBuild1.CurRxDbGrid<>Nil Then
    AtmTabSheetBuild1.CurRxDbGrid.SelectAll;
end;

Procedure TFrm_CopyTnua.CodeNehagValidate(Sender: TField);
Begin
  if DM_WinTuda.Tbl_Nehag.FindKey([Sender.AsInteger]) Then
  Begin
    RxQuery1.FieldByName('DriverName').AsString:=DM_WinTuda.Tbl_Nehag.FieldByName('Shem_Nehag').AsString;
    if WinTudaAlwaysBringRehevFromNehag Then
      RxQuery1.FieldByName('CarNum').AsString:=DM_WinTuda.Tbl_Nehag.FieldByName('Mis_Rishui').AsString;
  End;
End;

procedure TFrm_CopyTnua.RxQuery1AfterOpen(DataSet: TDataSet);
begin
  RxQuery1.FieldByName('DriverNo1').OnValidate:=CodeNehagValidate;
end;

Procedure TFrm_CopyTnua.CalcShibTotalPrices;
Var
   I :LongInt;
begin
  For I:=1 To 4 Do
    if (Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsString<>'') And
       (Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsString<>'') Then

      if (Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).AsString<>'0') And (Tbl_Shib.FieldByName('PriceIsGlobal').AsInteger=0{<>1}) Then
           Tbl_Shib.FieldByName('PriceQuntity'+IntToStr(I)).Value:=(Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsFloat)*
               (Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsFloat)
      Else
           Tbl_Shib.FieldByName('PriceQuntity'+IntToStr(I)).Value:=(Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsFloat);
End;

Procedure TFrm_CopyTnua.FindNextHazmanaNum;
Var
   HN :LongInt;
Begin
     if Tbl_Azmn.State<>dsBrowse Then
     Begin
          With Frm_GlblWinTuda.Qry_Generic Do
          Begin
               Active:=False;
               Sql.Clear;
               Sql.Add('Select Max(AzmnNo) HazNum');
               Sql.Add('From ATMAZMN');
               Sql.Add('Where AzmnNo Between '+IntToStr(RangeAzmanFrom)+' and '+IntToStr(RangeAzmnTo));
               Active:=True;
               if (FieldByName('HazNum').AsInteger>=RangeAzmanFrom) And (FieldByName('HazNum').AsInteger<RangeAzmnTo) Then
                   HN:=(FieldByName('HazNum').AsInteger+1)
               Else
                 if (FieldByName('HazNum').AsInteger<RangeAzmanFrom) Then
                    HN:=RangeAzmanFrom
                 Else
                    if (FieldByName('HazNum').AsInteger>=RangeAzmnTo) Then
                    Begin
                        MessageDlg('���� �� ���� ������� ������ �� '+IntToStr(RangeAzmnTo),mtError,[mbOk],0);
                        Tbl_Azmn.Cancel;
                        Active:=False;
                        Exit;
                    End;
               Tbl_Azmn.FieldByName('AzmnNo').AsInteger:=HN;
               Active:=False;
          End;
       End;
End;

end.

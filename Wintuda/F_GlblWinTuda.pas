{ 16/03/99 11:23:05 > [Aviran on AVIRAN] check in: (0.0)  / None }
unit F_GlblWinTuda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TProgress, AdvSearch, Db, DBTables,Math, AtmAdvTable, Menus,IniFiles,Registry,
  RxQuery;


Const
     PnlNum_TotalTuda=0;
     PnlNum_TotalHazmana=1;
     PnlNum_DSState=2;
     PnlNum_Yehus=3;
     PnlNum_Help=4;

type
  TFrm_GlblWinTuda = class(TForm)
    AtmAdvSearch_Rehev: TAtmAdvSearch;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    Query_KodTavla: TQuery;
    AtmAdvSearch_KodTavla: TAtmAdvSearch;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    AtmAdvSearch_Lako: TAtmAdvSearch;
    Qry_Generic: TRxQuery;
    AtmAdvSearch_Mehiron: TAtmAdvSearch;
    AtmAdvSearch_MaslulMehiron: TAtmAdvSearch;
    AtmAdvSearch_SugTavlaMehiron: TAtmAdvSearch;
    AtmAdvSearch_Mahzeva: TAtmAdvSearch;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AtmAdvSearch_MaslulMehironBeforeExecQuery(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_SugTavlaMehironBeforeExecQuery(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_RehevBeforeExecQuery(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_LakoBeforeExecQuery(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_NehagBeforeExecQuery(Sender: TObject;
      var ContinueExecute: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    Function SumTotalForHazmana(HazmanaNum:LongInt):Real;
    Function SumTotalForTuda(TudaNum:LongInt):Real;
    Function FindMehiron(CodeLak,CodeMehiron,CodeHiuv,CodeSherut:LongInt;TheDate :TDateTime):Boolean;
    Procedure MehironPrice(MehironKey,CodeHiuv,CodeSherut,SugLink:LongInt;
                           TnuaDataSet:TDataSet;Var Price1,Price2,Price3,Price4:Real);
    Procedure MehironBuildRecordForEachPrice(MehironKey,CodeHiuv,CodeSherut,SugLink:LongInt;TnuaDataSet:TTable);
    Procedure MehironBuildRecordForEachPrice2(MehironKey,CodeHiuv,
                                              CodeSherut,DefaultSugLink:LongInt;TnuaDataSet:TDataSet);
  end;

  Procedure CreateGlobalForm;
  Procedure CreateDM;
  Procedure ShowWinTuda(HazmanaNum :LongInt);
  Procedure StartTovala(AutoStartWinTuda :Boolean);
  Procedure DestroyWinTuda;
  procedure OpenCrtDll(CrtNumber:LongInt;TheMainValue:ShortString);
  Procedure InitVariables(CurAlias,CurIni:ShortString);

  ////////////////////////////////////////////////////
  Procedure InitAtmCrt(AliasName,DirForIni:ShortString);External 'AtmCrt.Dll';
  Procedure OpenCrt(FormNum :LongInt;MainValue :ShortString);External 'AtmCrt.Dll';
  Procedure DoneAtmCrt;External 'AtmCrt.Dll';

Const
  ssTovala =0;//�����
  ssMaz    =1;//�������
var
  Frm_GlblWinTuda: TFrm_GlblWinTuda;
  TnuaWasInInsert :Boolean;
  CurAliasName :ShortString;
  DirectoryForIniFiles :ShortString;
  DirectoryForExeFiles :ShortString;
  DirectoryForScripts  :ShortString;
  DirectoryForCurUser  :ShortString;
  WinTudaCurUserName   :ShortString;
  WinTudaCurIniFileName :ShortString;
  WinTudaPrivateIniFileName:String; //Private Ini file name for each user
  WinTudaMAM :Real;
  WinTudaAutoCalcSumTuda :Boolean;//����� ������� ���"� �����
  WinTudaTotalForHazmana :Real; //��"� ������ ����� ������ ���� ����
  WinTudaAlwaysBringRehevFromNehag :Boolean; //��� ���� ����
  InNewHazmana:Boolean; //����� ����
  ActAfterPostNew:SmallInt;//�� ����� ���� ����� �� ����� ����
  InAction :Boolean;
  WinTudaMeholelHandle :THandle;
  WinTudaKupaHandle :THandle;
  WinTudaKabalaHandle:THandle;
  WinTudaDBUserName,WinTudaDBPassword :ShortString;
  RangeAzmanFrom,RangeAzmnTo :LongInt;
  WinTudaYehusYear,WinTudaYehusMonth:Word;
  WinTudaShibKind :LongInt; //��� �����
  WinTudaPriceKind:SmallInt; //��� ����
  WinTudaNehagCheckFieldName :String; //�� ���� ����� �� ����� ���
  WinTudaFirstTableState:Byte;//��� ���� ������ ������ ����
  WinTudaSqlMode :Byte; //0=Local,1=Sql
  WinTudaSugSystem :Integer; //0=Tovala,1=Mazkirut
  WinTudaCheckValueExistFieldName :String; //�� ���� ���� ������
  WinTudaSqlServerType :String;
  WinTudaLakMastBeEqual :Boolean; //���� ���� ����� ����� ������ ����� ����
  WinTudaShowOnlyActiveCar:Boolean;//��� ����� ������ ����
  WinTudaShowOnlyActiveClient :Boolean; //��� ������ ������ ����
  WinTudaShowOnlyActiveDriver:Boolean; //����� ����� ������ ����
  WinTudaAtmAzmnFileName,WinTudaAtmShibFileName :String;
implementation
Uses DMWinTuda, Frm_WinTuda,AtmRutin,MainTovala,AtmConst;
{$R *.DFM}

Type
    TMehirRecord = Class
        FMaslulCode1 :LongInt;
        FMaslul1:String;
        FYEHIDAT_MIDA:LongInt;
        FPrice1,
        FPrice2,
        FPrice3,
        FPrice4 :Real;
        FSugBhira,
        FCalc_Group :LongInt;
    End;

Function TFrm_GlblWinTuda.SumTotalForHazmana(HazmanaNum:LongInt):Real;
Begin
     Result:=0;
     With Qry_Generic Do
     Begin
          Active:=False;
          Sql.Clear;
          Sql.Add('Select Sum(PriceQuntity1) As Total');
          Sql.Add('From ATMSHIB');
          Sql.Add('Where ShibAzmnNo='+IntToStr(HazmanaNum));
          Active:=True;
          Result:=FieldByName('Total').AsFloat;
          Active:=False;
     End;
End;

Function TFrm_GlblWinTuda.SumTotalForTuda(TudaNum:LongInt):Real;
Begin
     Result:=0;
     With Qry_Generic Do
     Begin
          Active:=False;
          Sql.Clear;
          Sql.Add('Select Sum(Price1*Quntity1) As Total');
          Sql.Add('From ATMSHIB');
          Sql.Add('Where ShibTuda='+IntToStr(TudaNum));
          Active:=True;
          Result:=FieldByName('Total').AsFloat;
          Active:=False;
     End;
End;

Procedure CreateGlobalForm;
Begin
     if Frm_GlblWinTuda=Nil Then
        Frm_GlblWinTuda:=TFrm_GlblWinTuda.Create(Nil);
End;

Procedure CreateDM;
Begin
     if DM_WinTuda=Nil Then
        DM_WinTuda:=TDM_WinTuda.Create(Nil);
End;

Procedure ShowWinTuda(HazmanaNum :LongInt);
Begin
  if InAction Then Exit;
  InAction:=True;
  Try
     CreateGlobalForm;
     if FrmWinTuda=Nil Then
        FrmWinTuda:=TFrmWinTuda.Create(Nil);
     FrmWinTuda.Show;
  Finally
        InAction:=False;
  End;
End;

Procedure StartTovala(AutoStartWinTuda :Boolean);
Begin
     if Frm_MainWinTuda=Nil Then
        Frm_MainWinTuda:=TFrm_MainWinTuda.Create(Nil);
     Frm_MainWinTuda.Show;
     if AutoStartWinTuda Then
        ShowWinTuda(0);
End;

Procedure DestroyWinTuda;
Begin
     if FrmWinTuda<>Nil Then
     Begin
          FrmWinTuda.Free;
          FrmWinTuda:=Nil;
          //FrmWinTuda ��� ������� �������� �"� (DM,Glbl)
     End;
     if Frm_MainWinTuda<>Nil Then
     Begin
          Frm_MainWinTuda.Free;
          Frm_MainWinTuda:=Nil;
     End;
End;

procedure TFrm_GlblWinTuda.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TQuery Then
             (Components[I] As TQuery).Active:=False;
     End;
end;

Function TFrm_GlblWinTuda.FindMehiron(CodeLak,CodeMehiron,CodeHiuv,CodeSherut:LongInt;TheDate :TDateTime):Boolean;
Begin
     Result:=False;
     With DM_WinTuda Do
     Begin
          if (CodeMehiron<=0) And (CodeLak>0) And (Tbl_Lako.FindKey([CodeLak])) Then
          Begin
               if Tbl_Lako.FieldByName('Kod_Mechiron').AsString<>'' Then
                  CodeMehiron:=Tbl_Lako.FieldByName('Kod_Mechiron').AsInteger;
          End;
          if CodeMehiron>0 Then
          Begin
               Tbl_Mehiron.FindNearest([CodeMehiron,CodeHiuv,CodeSherut,TheDate]);
               if (Tbl_Mehiron.FieldByName('CODE_MEHIRON').AsInteger=CodeMehiron) And
                  (Tbl_Mehiron.FieldByName('SUG_HIUV').AsInteger=CodeHiuv) And
                  (Tbl_Mehiron.FieldByName('SUG_SHERUT').AsInteger=CodeSherut) And
                  (TDate(Tbl_Mehiron.FieldByName('TO_DATE').AsDateTime)>=TDate(TheDate)) Then
                  Result:=True;
          End;// if CodeMehiron>0
     End;//With DM_WinTuda
End;

Procedure TFrm_GlblWinTuda.MehironPrice(MehironKey,CodeHiuv,CodeSherut,SugLink:LongInt;
                       TnuaDataSet:TDataSet;Var Price1,Price2,Price3,Price4:Real);
Var
   TempKamut :Real;
   TempPrice1,TempPrice2,TempPrice3,TempPrice4:Real;
Begin
     Price1:=0;
     Price2:=0;
     Price3:=0;
     Price4:=0;
     With Qry_Generic Do
     Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From MHIRCHLD');
           Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
{           Sql.Add('And SUG_HIUV='+IntToStr(CodeHiuv));
           Sql.Add('And SUG_SHERUT='+IntToStr(CodeSherut));}
           Active:=True;

           First;
           While Not Eof Do
           Begin
                TempKamut:=0;
                if FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'' Then
                   if TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsString<>'' Then
                      TempKamut:=TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsFloat;
                Try
                    //����� ��� �����
                    if (FieldByName('FROM_VALUE').AsString<>'') Then
                       if (FieldByName('FROM_VALUE').AsFloat)>TempKamut Then
                          Raise ERangeError.Create('');
                    if (FieldByName('TO_VALUE').AsString<>'') Then
                       if ((FieldByName('TO_VALUE').AsFloat)<TempKamut) And (SugLink<>slRelative) Then
                          Raise ERangeError.Create('');

                    if (CodeSherut<>FieldByName('SUG_SHERUT_IN_TNUA').AsInteger) And (SugLink=slRelative) Then
                    Begin
                         if (FieldByName('FROM_VALUE').AsFloat<=TempKamut) Then
                         Begin
                             if (FieldByName('TO_VALUE').AsFloat<=TempKamut) Then
                                TempKamut:=FieldByName('TO_VALUE').AsFloat-FieldByName('FROM_VALUE').AsFloat
                             Else
                                 TempKamut:=TempKamut-FieldByName('FROM_VALUE').AsFloat;
                         End;
                    End;

                    Begin//����� ���� �����
                         TempPrice1:=FieldByName('Price1').AsFloat*TempKamut;
                         TempPrice2:=FieldByName('Price2').AsFloat*TempKamut;
                         TempPrice3:=FieldByName('Price3').AsFloat*TempKamut;
                         TempPrice4:=FieldByName('Price4').AsFloat*TempKamut;
                    End;

                    Case SugLink of
                      slSum,slRelative:  //����� ����
                        Begin
                           Price1:=Price1+TempPrice1;
                           Price2:=Price2+TempPrice2;
                           Price3:=Price3+TempPrice3;
                           Price4:=Price4+TempPrice4;
                        End;
                      slMax: //�������
                        Begin
                           Price1:=Max(Price1,TempPrice1);
                           Price2:=Max(Price2,TempPrice2);
                           Price3:=Max(Price3,TempPrice3);
                           Price4:=Max(Price4,TempPrice4);
                        End;
                    End; //Case

                Except on  ERangeError Do;
                End;

                Next;
           End;// While Not Eof
     End;//With Qry_Generic
End;


Procedure TFrm_GlblWinTuda.MehironBuildRecordForEachPrice(MehironKey,CodeHiuv,
                                                          CodeSherut,SugLink:LongInt;TnuaDataSet:TTable);
Var
   TempKamut,
   TempPrice1,
   TempPrice2,
   TempPrice3,
   TempPrice4,
   Price1,
   Price2,
   Price3,
   Price4        :Real;
   SugSherutList :TStringList;
   I,J           :LongInt;
   FirstTnuaNum  :LongInt;

Procedure CalcPrice(TheQry :TQuery;FirstLine :Boolean);
Var
   KamutIsOther :Boolean;
   Ic1 :Char;
Begin
     KamutIsOther:=False;
     Price1:=0;
     Price2:=0;
     Price3:=0;
     Price4:=0;
     With TheQry Do
     Begin
       First;
       While Not Eof Do
       Begin
         if ((FieldByName('RANGE_FIELD_NAME').AsString<>'') And (TnuaDataSet.FindField(FieldByName('RANGE_FIELD_NAME').AsString)<>Nil)) Or
            ((FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'') And (TnuaDataSet.FindField(FieldByName('SUG_KAMUT_FIELD_NAME').AsString)<>Nil)) Or
            ((FieldByName('RANGE_FIELD_NAME').AsString='') And (FieldByName('SUG_KAMUT_FIELD_NAME').AsString='')) Then
         Begin
            TempKamut:=1;
            if FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'' Then
               if TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsString<>'' Then
               Begin
                  TempKamut:=TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsFloat;
                  KamutIsOther:=True;
                  For Ic1 := '1' To '4' Do
                      TnuaDataSet.FieldByName('Quntity'+Ic1).AsFloat:=TempKamut;
                  TnuaDataSet.FieldByName('PriceIsGlobal').AsInteger:=FirstTnuaNum;
               End;
            Try
                //����� ��� �����
                if (FieldByName('FROM_VALUE').AsString<>'') Then
                   if (FieldByName('FROM_VALUE').AsFloat)>TempKamut Then
                      Raise ERangeError.Create('');
                if (FieldByName('TO_VALUE').AsString<>'') And (SugLink<>slRelative) Then
                   if (FieldByName('TO_VALUE').AsFloat)<TempKamut Then
                      Raise ERangeError.Create('');

                if (Not FirstLine) And (SugLink=slRelative) Then
                Begin
                     if (FieldByName('FROM_VALUE').AsFloat<=TempKamut) Then
                     Begin
                         if (FieldByName('TO_VALUE').AsFloat<=TempKamut) Then
                            TempKamut:=FieldByName('TO_VALUE').AsFloat-FieldByName('FROM_VALUE').AsFloat
                         Else
                             TempKamut:=TempKamut-FieldByName('FROM_VALUE').AsFloat;
                     End;
                End;

                Begin//����� ���� �����
                     TempPrice1:=FieldByName('Price1').AsFloat*TempKamut;
                     TempPrice2:=FieldByName('Price2').AsFloat*TempKamut;
                     TempPrice3:=FieldByName('Price3').AsFloat*TempKamut;
                     TempPrice4:=FieldByName('Price4').AsFloat*TempKamut;
                End;

{                if KamutIsOther Then
                Begin

                End;
 }
                Case SugLink of
                  slSum,slRelative : //��� ����, ����� ����
                    Begin
                       Price1:=Price1+TempPrice1;
                       Price2:=Price2+TempPrice2;
                       Price3:=Price3+TempPrice3;
                       Price4:=Price4+TempPrice4;
                    End;
                  slMax: //�������
                    Begin
                       Price1:=Max(Price1,TempPrice1);
                       Price2:=Max(Price2,TempPrice2);
                       Price3:=Max(Price3,TempPrice3);
                       Price4:=Max(Price4,TempPrice4);
                    End;
                End; //Case

            Except on  ERangeError Do;
            End;
         End;//       if ((FieldByName('RANGE_FIELD_NAME').AsString<>'') And (TnuaDataSet.FindField(FieldByName('RANGE_FIELD_NAME').AsString)<>Nil)) Or

         Next;
       End;// While Not Eof
     End; //With
End;

Begin
  Try
     SugSherutList :=TStringList.Create;
     With Qry_Generic Do
     Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From MHIRCHLD');
           Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
           Sql.Add('And SUG_SHERUT_IN_TNUA='+IntToStr(CodeSherut){DM_WinTuda.Tbl_ShibMaslulCode1.AsString});
           Active:=True;
           if RecordCount=0 Then
           Begin
              Active:=False;
              ShowMessage('�� ���� ��� ���� ��');
              Exit;
           End;
           CalcPrice(Qry_Generic,True);

           if TnuaDataSet.State=dsBrowse Then
              TnuaDataSet.Edit;

           TnuaDataSet.FieldByName('Price1').AsFloat:=Price1;
           TnuaDataSet.FieldByName('Price2').AsFloat:=Price2;
           TnuaDataSet.FieldByName('Price3').AsFloat:=Price3;
           TnuaDataSet.FieldByName('Price4').AsFloat:=Price4;
           MarkSaveOnTable(TnuaDataSet);
           TnuaDataSet.Post;

{           FirstTnuaNum:= TnuaDataSet.FieldByName('ShibNo').AsInteger;
           if FirstTnuaNum=0 Then
             FirstTnuaNum:=TnuaDataSet.FieldByName('CalcShibNo').AsInteger;
}
           FirstTnuaNum:=DM_WinTuda.GetCurShibNo;
           //����� �� ���� ����� �������
           Active:=False;
           Sql.Clear;
           Sql.Add('Select Distinct SUG_SHERUT_IN_TNUA From MHIRCHLD');
           Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
           Sql.Add('And SUG_SHERUT_IN_TNUA<>'+IntToStr(CodeSherut){DM_WinTuda.Tbl_ShibMaslulCode1.AsString});
           Active:=True;
           First;
           While Not Eof Do
           Begin
                 SugSherutList.Add(FieldByName('SUG_SHERUT_IN_TNUA').AsSTring);
                 Next;
           End;

           Qry_Generic.Active:=False;

           For I:=0 To SugSherutList.Count-1 Do
           Begin
                 Qry_Generic.Active:=False;
                 Sql.Clear;
                 Sql.Add('Select * From MHIRCHLD');
                 Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
                 Sql.Add('And SUG_SHERUT_IN_TNUA='+SugSherutList[i]);
                 Active:=True;
                 if RecordCount=0 Then
                    Break;

                 DuplicateOneRecord(TnuaDataSet,False,True);
                 CalcPrice(Qry_Generic,False);

                 TnuaDataSet.FieldByName('Price1').AsFloat:=Price1;
                 TnuaDataSet.FieldByName('Price2').AsFloat:=Price2;
                 TnuaDataSet.FieldByName('Price3').AsFloat:=Price3;
                 TnuaDataSet.FieldByName('Price4').AsFloat:=Price4;
                 TnuaDataSet.FieldByName('MaslulCode1').AsString:=FieldByName('SUG_SHERUT_IN_TNUA').AsString;
                 TnuaDataSet.FieldByName('Maslul1').AsString:=FieldByName('Teur').AsString;
                 if FieldByName('YEHIDAT_MIDA').AsString<>'' Then
                 For J:=1 To 4 Do
                     TnuaDataSet.FieldByName('YeMida'+IntToStr(J)).AsString:=FieldByName('YEHIDAT_MIDA').AsString;
                 MarkSaveOnTable(TnuaDataSet);
                 TnuaDataSet.Post;
           End; //For
     End;//With Qry_Generic
  Finally
         SugSherutList.Free;
         Qry_Generic.Active:=False;
  End;
End;

procedure OpenCrtDll(CrtNumber:LongInt;TheMainValue:ShortString);
Type
  TOpenCrt =  procedure(CrtNum:LongInt;MainValue:ShortString);
Var
  Handle : THandle;
  FileName : Pchar;
  ProcName :PChar;
  OpenCrtFunc : TOpenCrt;

begin
     OpenCrt(CrtNumber,TheMainValue);
{
  GetMem(FileName,100);
  GetMem(ProcName,100);

  StrPcopy (FileName,'AtmCrt.Dll');
  try
  Handle := LoadLibrary(FileName);
  if Handle <> 0 then
     begin
       StrPcopy (ProcName,'OpenCrt');
       @OpenCrtFunc := GetProcAddress(Handle,ProcName);
       if @OpenCrtFunc <> nil then
          begin
            OpenCrtFunc(CrtNumber,TheMainValue);
          end;
       FreeLibrary (Handle);
     end;
  finally
    FreeMem(FileName);
    FreeMem(ProcName);
  end;
}
end;

Procedure InitVariables(CurAlias,CurIni:ShortString);
Var
   F :TIniFile;
   R :TRegistry;
   StrH :String;
   DD :Word;
Begin
     DecodeDate(Now,WinTudaYehusYear,WinTudaYehusMonth,DD);
     WinTudaAtmAzmnFileName:='ATMAZMN';
     WinTudaAtmShibFileName:='ATMSHIB';
     WinTudaKupaHandle:=0;
     CurAliasName:=CurAlias;
     DirectoryForIniFiles:=CurIni;
     DirectoryForExeFiles:=ExtractFilePath(Application.ExeName);
     DirectoryForScripts:=DirectoryForExeFiles+'Scripts\';
     WinTudaCurIniFileName:=RunDllFunc1StringParam('Selectiv.Dll','GetCurrentCompanyIni',DirectoryForIniFiles+Atm2000IniFileName);
     WinTudaPrivateIniFileName:='User'+WinTudaCurIniFileName;
     if WinTudaCurIniFileName<>'' Then
        WinTudaCurIniFileName:=DirectoryForIniFiles+WinTudaCurIniFileName
     Else
         WinTudaCurIniFileName:=DirectoryForIniFiles+Atm2000IniFileName;

     F:=TIniFile.Create(WinTudaCurIniFileName);
     Try
        WinTudaMAM:=F.ReadFloat(SectionForMain,KeyForMAM,17);

        WinTudaAutoCalcSumTuda:=F.ReadBool(SectionForWinTuda,KeyForAutoCalcTotalTeuda,False);
        ActAfterPostNew:=F.ReadInteger(SectionForWinTuda,KeyForActionAfterPostNew,apnNewShib);
        WinTudaAlwaysBringRehevFromNehag:=F.ReadBool(SectionForWinTuda,KeyForBringRehevFromNehag,False);
        WinTudaNehagCheckFieldName:=F.ReadString(SectionForWinTuda,KeyForGetNehagBy,'Kod_Nehag');
        WinTudaFirstTableState:=F.ReadInteger(SectionForMain,FirstStateRecord,tsLast);
        WinTudaSqlMode:=F.ReadInteger(SectionForMain,SqlMode,0);
        WinTudaSqlServerType:=F.ReadString(SectionForMain,SQLServerType,'Paradox');
        WinTudaSugSystem:=F.ReadInteger(SectionForMain,IniWinTudaSugSystem,ssTovala);
        WinTudaCheckValueExistFieldName:=F.ReadString(SectionForWinTuda,KeyForRetzefTeuda,'');//��� ������
        WinTudaLakMastBeEqual:=F.ReadBool(SectionForWinTuda,KeyForLakMustBeEqual,True);
     Finally
            F.Free;
     End;

     F:=TIniFile.Create(WinTudaPrivateIniFileName);
     Try
        WinTudaShowOnlyActiveCar:=F.ReadBool(UserSectionForMain,KeyShowOnlyActiveCar,False);
        WinTudaShowOnlyActiveClient:=F.ReadBool(UserSectionForMain,KeyShowOnlyActiveClient,False);
        WinTudaShowOnlyActiveDriver:=F.ReadBool(UserSectionForMain,KeyShowOnlyActiveDriver,False);
     Finally
      F.Free;
     End;

     R:=TRegistry.Create;
     Try
        R.RootKey:=RegRootForAtm;
        R.OpenKey(RegKeyForAtm2000,True);
        if R.ValueExists(RegValueForScriptsFiles) Then
           StrH:=R.ReadString(RegValueForScriptsFiles)+'\';
        if Trim(StrH)<>'' Then
          DirectoryForScripts:=StrH;

        WinTudaCurUserName:=RunDllStringFuncNoParam('Permit.Dll','GetCurrentUser');
        if R.ValueExists(RegValueForUsersDir) Then
        Begin
           StrH:=R.ReadString(RegValueForUsersDir)+'\';
           if Trim(StrH)<>'' Then
             DirectoryForCurUser:=StrH;
           if WinTudaCurUserName<>'' Then
              DirectoryForCurUser:=DirectoryForCurUser+WinTudaCurUserName+'\';
        End
        Else
            DirectoryForCurUser:=DirectoryForExeFiles+'Users\'+WinTudaCurUserName+'\';
        WinTudaPrivateIniFileName:=DirectoryForCurUser+WinTudaPrivateIniFileName;
     {        if R.ValueExists(RegValueForInstallDir) Then
           DE_InstallDir.Text:=R.ReadString(RegValueForInstallDir);}
     Finally
            R.Free;
     End;

     WinTudaDbPassword:=RunDllStringFuncNoParam('Permit.dll','GetCurDbPassword');
     WinTudaDBUserName:=RunDllStringFuncNoParam('Permit.dll','GetCurDbUserName');
End;


procedure TFrm_GlblWinTuda.FormCreate(Sender: TObject);
Var
   I :longInt;
begin
  InAction:=False;
  For I:=0 To ComponentCount-1 Do
     if Components[i] is TAtmAdvSearch Then
        TAtmAdvSearch(Components[i]).IniFileName:=DirectoryForIniFiles+Name+'.Ini';
end;

procedure TFrm_GlblWinTuda.AtmAdvSearch_MaslulMehironBeforeExecQuery(Sender: TObject;
  var ContinueExecute: Boolean);
begin
//     TQuery(AtmAdvSearch_MaslulMehiron.SourceDataSet).Params.ParamByName('PCodeLak').DataType:=ftInteger;
     TQuery(AtmAdvSearch_MaslulMehiron.SourceDataSet).Params.ParamValues['PCodeLak']:=StrToInt(FrmWinTuda.AtmDbHEdit_Lakoach.Text);
     if DM_WinTuda.Tbl_ShibSugMetan.AsString<>'' Then
         TRxQuery(AtmAdvSearch_MaslulMehiron.SourceDataSet).Macros.ParamValues['SugHiuv']:='And '+DM_WinTuda.Tbl_ShibSugMetan.AsString+'= Mehirlak.Sug_Hiuv'
     Else
          TRxQuery(AtmAdvSearch_MaslulMehiron.SourceDataSet).Macros.ParamValues['SugHiuv']:='';

end;

procedure TFrm_GlblWinTuda.AtmAdvSearch_SugTavlaMehironBeforeExecQuery(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     TQuery(AtmAdvSearch_MaslulMehiron.SourceDataSet).Params.ParamValues['PCodeLak']:=StrToInt(FrmWinTuda.AtmDbHEdit_Lakoach.Text);
     TQuery(AtmAdvSearch_MaslulMehiron.SourceDataSet).Params.ParamValues['PSugTavla']:=SugTavla_SugMitan;
end;



Procedure TFrm_GlblWinTuda.MehironBuildRecordForEachPrice2(MehironKey,CodeHiuv,
                                                          CodeSherut,DefaultSugLink:LongInt;TnuaDataSet:TDataSet);
Var
   TempKamut,
   TempPrice1,
   TempPrice2,
   TempPrice3,
   TempPrice4,
   Price1,
   Price2,
   Price3,
   Price4 ,
   TotalPrice1,
   TotalPrice2,
   TotalPrice3,
   TotalPrice4       :Real;
   SugSherutList :TStringList;
   I,J           :LongInt;
   BufferList :TList;
   MehirRecord:TMehirRecord;
   MaxMehir,MinMehir:TMehirRecord;
   SugLink:LongInt;
   FirstTnuaBM :TBookMark;
   DoMaxMinMehir:Boolean;
   FirstTnuaNum:longInt;
   EV :TFieldNotifyEvent;

Procedure PutMehirRecordInTnua(MR :TMehirRecord);
Var
   J :Integer;
Begin
   TnuaDataSet.GotoBookmark(FirstTnuaBM);
   DuplicateOneRecord(TnuaDataSet,False,True);
   TnuaDataSet.FieldByName('Price1').AsFloat:=Mr.FPrice1;
   TnuaDataSet.FieldByName('Price2').AsFloat:=Mr.FPrice2;
   TnuaDataSet.FieldByName('Price3').AsFloat:=Mr.FPrice3;
   TnuaDataSet.FieldByName('Price4').AsFloat:=Mr.FPrice4;
   TnuaDataSet.FieldByName('MaslulCode1').AsInteger:=Mr.FMaslulCode1;
   TnuaDataSet.FieldByName('Maslul1').AsString:=Mr.FMaslul1;
   TnuaDataSet.FieldByName('PriceIsGlobal').AsInteger:=FirstTnuaNum;
   if Mr.FYehidat_Mida>-1 Then
   For J:=1 To 4 Do
       TnuaDataSet.FieldByName('YeMida'+IntToStr(J)).AsInteger:=Mr.FYehidat_Mida;
   MarkSaveOnTable(TnuaDataSet);
   TnuaDataSet.Post;
End;

Procedure CalcPrice(TheQry :TQuery;FirstLine :Boolean);
Var
   Ic1 :Char;
   RangeKamut :Real;
Begin
     Price1:=0;
     Price2:=0;
     Price3:=0;
     Price4:=0;

     if Not FirstLine Then
     Begin
       TempKamut:=0;
       For Ic1 := '1' To '4' Do
       Begin
         EV:=TnuaDataSet.FieldByName('Quntity'+Ic1).OnValidate;
         TnuaDataSet.FieldByName('Quntity'+Ic1).OnValidate:=Nil;
         TnuaDataSet.FieldByName('Quntity'+Ic1).AsFloat:=0;
         TnuaDataSet.FieldByName('Quntity'+Ic1).OnValidate:=EV;
       End;
     End
     Else
       TempKamut:=1;
     With Qry_Generic Do
     Begin
       First;
       While Not Eof Do
       Begin
           if ((FieldByName('RANGE_FIELD_NAME').AsString<>'') And (TnuaDataSet.FindField(FieldByName('RANGE_FIELD_NAME').AsString)<>Nil)) Or
              ((FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'') And (TnuaDataSet.FindField(FieldByName('SUG_KAMUT_FIELD_NAME').AsString)<>Nil)) Or
              ((FieldByName('RANGE_FIELD_NAME').AsString='') And (FieldByName('SUG_KAMUT_FIELD_NAME').AsString='')) Then
           Begin
                SugLink:=DefaultSugLink;
                if Not FieldByName('SUG_HISHUV').IsNull Then
                   SugLink:=FieldByName('SUG_HISHUV').AsInteger;

                if FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'' Then
                   if TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsString<>'' Then
                   Begin
                      TempKamut:=TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsFloat;
{                      For Ic1 := '1' To '4' Do ����� ����
                          TnuaDataSet.FieldByName('Quntity'+Ic1).AsFloat:=TempKamut;}
                      TnuaDataSet.FieldByName('PriceIsGlobal').AsInteger:=FirstTnuaNum;
                   End;
                Try
                  RangeKamut:=TempKamut;
                  if FieldByName('RANGE_FIELD_NAME').AsString<>'' Then
                     if TnuaDataSet.FieldByName(FieldByName('RANGE_FIELD_NAME').AsString).AsString<>'' Then
                        RangeKamut:=TnuaDataSet.FieldByName(FieldByName('RANGE_FIELD_NAME').AsString).AsFloat;
                    //����� ��� �����
                    if (FieldByName('FROM_VALUE').AsString<>'') Then
                       if (FieldByName('FROM_VALUE').AsFloat)>RangeKamut{TempKamut} Then
                          Raise ERangeError.Create('');
                    if (FieldByName('TO_VALUE').AsString<>'') Then
                       if ((FieldByName('TO_VALUE').AsFloat)<RangeKamut{TempKamut}) And (SugLink<>slRelative) Then
                          Raise ERangeError.Create('');

                    if (Not FirstLine) And (SugLink=slRelative) Then
                    Begin
                         if (FieldByName('FROM_VALUE').AsFloat<=TempKamut) Then
                         Begin
                             if (FieldByName('TO_VALUE').AsFloat<=TempKamut) Then
                                TempKamut:=FieldByName('TO_VALUE').AsFloat-FieldByName('FROM_VALUE').AsFloat+1
                             Else
                                 TempKamut:=TempKamut-FieldByName('FROM_VALUE').AsFloat+1;
                         End;
                    End;


                if FieldByName('SUG_KAMUT_FIELD_NAME').AsString<>'' Then
                   if TnuaDataSet.FieldByName(FieldByName('SUG_KAMUT_FIELD_NAME').AsString).AsString<>'' Then
                    For Ic1 := '1' To '4' Do
                    Begin
                      EV:=TnuaDataSet.FieldByName('Quntity'+Ic1).OnValidate;
                      TnuaDataSet.FieldByName('Quntity'+Ic1).OnValidate:=Nil;
                      TnuaDataSet.FieldByName('Quntity'+Ic1).AsFloat:=TnuaDataSet.FieldByName('Quntity'+Ic1).AsFloat+TempKamut;
                      TnuaDataSet.FieldByName('Quntity'+Ic1).OnValidate:=EV;
                    End;

                    if SugLink = slFix Then
                       TempKamut:=1;
                    Begin//����� ���� �����
                         TempPrice1:=FieldByName('Price1').AsFloat*TempKamut;
                         TempPrice2:=FieldByName('Price2').AsFloat*TempKamut;
                         TempPrice3:=FieldByName('Price3').AsFloat*TempKamut;
                         TempPrice4:=FieldByName('Price4').AsFloat*TempKamut;
                    End;

                    Case SugLink of
                      slSum,slRelative,slFix:  //����� ����
                        Begin
                                Price1:=Price1+TempPrice1;
                                Price2:=Price2+TempPrice2;
                                Price3:=Price3+TempPrice3;
                                Price4:=Price4+TempPrice4;
                        End;

                      slMax: //�������
                        Begin
                           Price1:=Max(Price1,TempPrice1);
                           Price2:=Max(Price2,TempPrice2);
                           Price3:=Max(Price3,TempPrice3);
                           Price4:=Max(Price4,TempPrice4);
                        End;
                      slMaxTotal:
                         Begin
                           MaxMehir.FMaslulCode1:=FieldByName('SUG_SHERUT_IN_TNUA').AsInteger;
                           MaxMehir.FMaslul1:=FieldByName('TEUR').AsString;
                           if FieldByName('YEHIDAT_MIDA').AsString<>'' Then
                              MaxMehir.FYEHIDAT_MIDA:=FieldByName('YEHIDAT_MIDA').AsInteger
                           Else
                               MaxMehir.FYEHIDAT_MIDA:=-1;
                           MaxMehir.FPrice1:=TempPrice1;
                           MaxMehir.FPrice2:=TempPrice2;
                           MaxMehir.FPrice3:=TempPrice3;
                           MaxMehir.FPrice4:=TempPrice4;
                           MaxMehir.FCalc_Group:=FieldByName('CALC_GROUP').AsInteger;
                         End;
                      slMinTotal:
                         Begin
                           MinMehir.FMaslulCode1:=FieldByName('SUG_SHERUT_IN_TNUA').AsInteger;
                           MinMehir.FMaslul1:=FieldByName('TEUR').AsString;
                           if FieldByName('YEHIDAT_MIDA').AsString<>'' Then
                              MinMehir.FYEHIDAT_MIDA:=FieldByName('YEHIDAT_MIDA').AsInteger
                           Else
                               MinMehir.FYEHIDAT_MIDA:=-1;
                           MinMehir.FPrice1:=TempPrice1;
                           MinMehir.FPrice2:=TempPrice2;
                           MinMehir.FPrice3:=TempPrice3;
                           MinMehir.FPrice4:=TempPrice4;
                           MinMehir.FCalc_Group:=FieldByName('CALC_GROUP').AsInteger;
                         End;
                    End; //Case

                Except on  ERangeError Do;
                End;
           End; //If
           Next;
       End;// While Not Eof
     End;//With Qry_Generic
End;

Procedure InsertPriceToTnua;
Begin
  EV:=TnuaDataSet.FieldByName('Price1').OnValidate;
  TnuaDataSet.FieldByName('Price1').OnValidate:=Nil;
  TnuaDataSet.FieldByName('Price1').AsFloat:=Price1;
  TnuaDataSet.FieldByName('Price1').OnValidate:=EV;
  EV:=TnuaDataSet.FieldByName('Price2').OnValidate;
  TnuaDataSet.FieldByName('Price2').OnValidate:=Nil;
  TnuaDataSet.FieldByName('Price2').AsFloat:=Price2;
  TnuaDataSet.FieldByName('Price2').OnValidate:=EV;
  EV:=TnuaDataSet.FieldByName('Price3').OnValidate;
  TnuaDataSet.FieldByName('Price3').OnValidate:=Nil;
  TnuaDataSet.FieldByName('Price3').AsFloat:=Price3;
  TnuaDataSet.FieldByName('Price3').OnValidate:=EV;
  EV:=TnuaDataSet.FieldByName('Price4').OnValidate;
  TnuaDataSet.FieldByName('Price4').OnValidate:=Nil;
  TnuaDataSet.FieldByName('Price4').AsFloat:=Price4;
  TnuaDataSet.FieldByName('Price4').OnValidate:=EV;
End;

Begin
     FirstTnuaNum:=0;
     BufferList:=TList.Create;
     MaxMehir:=TMehirRecord.Create;
     MaxMehir.FMaslulCode1:=-1;
     MinMehir:=TMehirRecord.Create;
     MinMehir.FMaslulCode1:=-1;
     TotalPrice1:=0;
     TotalPrice2:=0;
     TotalPrice3:=0;
     TotalPrice4:=0;
     TnuaDataset.DisableControls;
     Screen.Cursor:=crHourGlass;
  Try
     SugSherutList :=TStringList.Create;
     With Qry_Generic Do
     Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From MHIRCHLD');
           Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
           Sql.Add('And SUG_SHERUT_IN_TNUA='+IntToStr(CodeSherut){DM_WinTuda.Tbl_ShibMaslulCode1.AsString});
           Active:=True;
           if RecordCount=0 Then
           Begin
              Active:=False;
              ShowMessage('�� ���� ��� ���� ��');
              Exit;
           End;
           CalcPrice(Qry_Generic,True);

           if TnuaDataSet.State=dsBrowse Then
              TnuaDataSet.Edit;

           TotalPrice1:=TotalPrice1+Price1;
           TotalPrice2:=TotalPrice2+Price2;
           TotalPrice3:=TotalPrice3+Price3;
           TotalPrice4:=TotalPrice4+Price4;

{           EV:=TnuaDataSet.FieldByName('Price1').OnValidate;
           TnuaDataSet.FieldByName('Price1').OnValidate:=Nil;
           TnuaDataSet.FieldByName('Price1').AsFloat:=Price1;
           TnuaDataSet.FieldByName('Price1').OnValidate:=EV;
           EV:=TnuaDataSet.FieldByName('Price2').OnValidate;
           TnuaDataSet.FieldByName('Price2').OnValidate:=Nil;
           TnuaDataSet.FieldByName('Price2').AsFloat:=Price2;
           TnuaDataSet.FieldByName('Price2').OnValidate:=EV;
           EV:=TnuaDataSet.FieldByName('Price3').OnValidate;
           TnuaDataSet.FieldByName('Price3').OnValidate:=Nil;
           TnuaDataSet.FieldByName('Price3').AsFloat:=Price3;
           TnuaDataSet.FieldByName('Price3').OnValidate:=EV;
           EV:=TnuaDataSet.FieldByName('Price4').OnValidate;
           TnuaDataSet.FieldByName('Price4').OnValidate:=Nil;
           TnuaDataSet.FieldByName('Price4').AsFloat:=Price4;
           TnuaDataSet.FieldByName('Price4').OnValidate:=EV;
}
           InsertPriceToTnua;
           DM_WinTuda.CalcShibTotalPrices;

           MarkSaveOnTable(TnuaDataSet);
           TnuaDataSet.Post;
           FirstTnuaBM:=TnuaDataSet.GetBookmark;

{           FirstTnuaNum:= TnuaDataSet.FieldByName('ShibNo').AsInteger;
           if FirstTnuaNum=0 Then
             FirstTnuaNum:=TnuaDataSet.FieldByName('CalcShibNo').AsInteger;
}
           FirstTnuaNum:=DM_WinTuda.GetCurShibNo;
           //����� �� ���� ����� �������
           Active:=False;
           Sql.Clear;
           Sql.Add('Select Distinct SUG_SHERUT_IN_TNUA From MHIRCHLD');
           Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
           Sql.Add('And SUG_SHERUT_IN_TNUA<>'+IntToStr(CodeSherut){DM_WinTuda.Tbl_ShibMaslulCode1.AsString});
           Active:=True;
           First;
           While Not Eof Do
           Begin
                 SugSherutList.Add(FieldByName('SUG_SHERUT_IN_TNUA').AsSTring);
                 Next;
           End;

           Qry_Generic.Active:=False;

           For I:=0 To SugSherutList.Count-1 Do
           Begin
                 Qry_Generic.Active:=False;
                 Sql.Clear;
                 Sql.Add('Select * From MHIRCHLD');
                 Sql.Add('Where MEHIRON_LINK_KEY='+IntToStr(MehironKey));
                 Sql.Add('And SUG_SHERUT_IN_TNUA='+SugSherutList[i]);
                 Active:=True;
                 if RecordCount=0 Then
                    Break;

                 TnuaDataSet.GotoBookmark(FirstTnuaBM);
                 DuplicateOneRecord(TnuaDataSet,False,True,True);
                 CalcPrice(Qry_Generic,False);

                 First;
                 // ����� ��� ������ �� �� ����� �� ��� �� ���
                 if (Not FieldByName('SUG_BHIRA').IsNull) And
                    (FieldByName('SUG_BHIRA').AsInteger<>sbNone) Then
                 Begin
                      MehirRecord:=TMehirRecord.Create;
                      MehirRecord.FMaslulCode1:=FieldByName('SUG_SHERUT_IN_TNUA').AsInteger;
                      MehirRecord.FMaslul1:=FieldByName('TEUR').AsString;
                      if FieldByName('YEHIDAT_MIDA').AsString<>'' Then
                         MehirRecord.FYEHIDAT_MIDA:=FieldByName('YEHIDAT_MIDA').AsInteger
                      Else
                          MehirRecord.FYEHIDAT_MIDA:=-1;
                      MehirRecord.FPrice1:=Price1;
                      MehirRecord.FPrice2:=Price2;
                      MehirRecord.FPrice3:=Price3;
                      MehirRecord.FPrice4:=Price4;
                      MehirRecord.FCalc_Group:=FieldByName('CALC_GROUP').AsInteger;
                      MehirRecord.FSugBhira:=FieldByName('SUG_BHIRA').AsInteger;
                      BufferList.Add(MehirRecord);
                      TnuaDataSet.Cancel;
                 End
                 Else
                 Begin
                     TotalPrice1:=TotalPrice1+Price1;
                     TotalPrice2:=TotalPrice2+Price2;
                     TotalPrice3:=TotalPrice3+Price3;
                     TotalPrice4:=TotalPrice4+Price4;

{                     TnuaDataSet.FieldByName('Price1').AsFloat:=Price1;
                     TnuaDataSet.FieldByName('Price2').AsFloat:=Price2;
                     TnuaDataSet.FieldByName('Price3').AsFloat:=Price3;
                     TnuaDataSet.FieldByName('Price4').AsFloat:=Price4;
}
                     InsertPriceToTnua;
                     TnuaDataSet.FieldByName('PriceIsGlobal').AsInteger:=FirstTnuaNum;
                     TnuaDataSet.FieldByName('MaslulCode1').AsString:=FieldByName('SUG_SHERUT_IN_TNUA').AsString;
                     TnuaDataSet.FieldByName('Maslul1').AsString:=FieldByName('Teur').AsString;
                     if FieldByName('YEHIDAT_MIDA').AsString<>'' Then
                     For J:=1 To 4 Do
                     Begin
                       EV:=TnuaDataSet.FieldByName('YeMida'+IntToStr(J)).OnValidate;
                       TnuaDataSet.FieldByName('YeMida'+IntToStr(J)).OnValidate:=Nil;
                       TnuaDataSet.FieldByName('YeMida'+IntToStr(J)).AsString:=FieldByName('YEHIDAT_MIDA').AsString;
                       TnuaDataSet.FieldByName('YeMida'+IntToStr(J)).OnValidate:=EV;
                     End;

                     if (Price1=0) And (Price2=0) And (Price3=0) And (Price4=0) Then
                         TnuaDataSet.Cancel
                     Else
                     Begin
                          DM_WinTuda.CalcShibTotalPrices;
                          MarkSaveOnTable(TnuaDataSet);
                          TnuaDataSet.Post;
                     End;
                 End;
           End; //For
     End;//With Qry_Generic

     //����� ���� ������ ������ ����� ��� ��� �����
     MehirRecord:=Nil;
     if BufferList.Count>0 Then
        MehirRecord:=TMehirRecord(BufferList.Items[0]);
     For I:=0 To BufferList.Count-1 Do
     Begin
          Case MehirRecord.FSugBhira of
               sbMax: if MehirRecord.FPrice1<TMehirRecord(BufferList.Items[i]).FPrice1 Then
                         MehirRecord:=TMehirRecord(BufferList.Items[i]);
               sbMin: if MehirRecord.FPrice1>TMehirRecord(BufferList.Items[i]).FPrice1 Then
                         MehirRecord:=TMehirRecord(BufferList.Items[i]);
          End;
     End;
     if MehirRecord<>Nil Then
     Begin
          TotalPrice1:=TotalPrice1+MehirRecord.FPrice1;
          TotalPrice2:=TotalPrice2+MehirRecord.FPrice2;
          TotalPrice3:=TotalPrice3+MehirRecord.FPrice3;
          TotalPrice4:=TotalPrice4+MehirRecord.FPrice4;
          PutMehirRecordInTnua(MehirRecord);
     End;

     //����� ���� ���' ����' ����

     DoMaxMinMehir:=True;
     if (MaxMehir.FMaslulCode1>-1) And (MinMehir.FMaslulCode1>-1) Then
        if MaxMehir.FPrice1<MinMehir.FPrice1 Then
        Begin
            MessageDlg('���� ������� ��� ����� ������� ������� ��',mtError,[mbOk],0);
            DoMaxMinMehir:=False;
        End;
     if DoMaxMinMehir Then
     Begin
         if MaxMehir.FMaslulCode1>-1 Then
         Begin
            if MaxMehir.FPrice1<TotalPrice1 Then
            Begin
              MaxMehir.FPrice1:=MaxMehir.FPrice1-TotalPrice1;
              if MaxMehir.FPrice2<TotalPrice2 Then
                 MaxMehir.FPrice2:=MaxMehir.FPrice2-TotalPrice2;
              if MaxMehir.FPrice3<TotalPrice3 Then
                 MaxMehir.FPrice3:=MaxMehir.FPrice3-TotalPrice3;
              if MaxMehir.FPrice4<TotalPrice4 Then
                 MaxMehir.FPrice4:=MaxMehir.FPrice4-TotalPrice4;
              PutMehirRecordInTnua(MaxMehir);
            End;
         End;

         if MinMehir.FMaslulCode1>-1 Then
            if MinMehir.FPrice1>TotalPrice1 Then
            Begin
                MinMehir.FPrice1:=MinMehir.FPrice1-TotalPrice1;
                if MinMehir.FPrice2>TotalPrice2 Then
                   MinMehir.FPrice2:=MinMehir.FPrice2-TotalPrice2;
                if MinMehir.FPrice3>TotalPrice3 Then
                   MinMehir.FPrice3:=MinMehir.FPrice3-TotalPrice3;
                if MinMehir.FPrice4>TotalPrice4 Then
                   MinMehir.FPrice4:=MinMehir.FPrice4-TotalPrice4;
                PutMehirRecordInTnua(MinMehir);
            End;
     End;

  Finally
         SugSherutList.Free;
         Qry_Generic.Active:=False;
         MaxMehir.Free;
         MinMehir.Free;
         TnuaDataSet.EnableControls;
         Screen.Cursor:=crDefault;
         While BufferList.Count>0 Do
         Begin
             TMehirRecord(BufferList.Items[0]).Free;
             BufferList.Delete(0);
         End;
         BufferList.Clear;
         BufferList.Free;
  End;
End;


procedure TFrm_GlblWinTuda.AtmAdvSearch_RehevBeforeExecQuery(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  if WinTudaShowOnlyActiveCar Then
    Qry_Generic.MacroByName('MOnlyActive').AsString:='Rehev_Pail = 1';
end;

procedure TFrm_GlblWinTuda.AtmAdvSearch_LakoBeforeExecQuery(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  if WinTudaShowOnlyActiveClient Then
    Qry_Generic.MacroByName('MOnlyActive').AsString:='Kod_Kesher = 1';

end;

procedure TFrm_GlblWinTuda.AtmAdvSearch_NehagBeforeExecQuery(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  if WinTudaShowOnlyActiveDriver Then
    Qry_Generic.MacroByName('MOnlyActive').AsString:='Nehag_Pail = 1';
end;

end.

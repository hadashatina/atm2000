unit Call;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, HebForm, Menus, ExtCtrls;

type
  TFrm_Sdrn = class(TForm)
    Show: TButton;
    Timer1: TTimer;
    Modify: TButton;
    Btn_ShowSherutForm: TButton;
    Btn_SendForm: TButton;
    BBn_SendStr: TButton;
    Btn_SendHebStr: TButton;
    procedure ShowClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ModifyClick(Sender: TObject);
    procedure Btn_ShowSherutFormClick(Sender: TObject);
    procedure Btn_SendFormClick(Sender: TObject);
    procedure BBn_SendStrClick(Sender: TObject);
    procedure Btn_SendHebStrClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Const
  InterVal = 5000;

var
  Frm_Sdrn: TFrm_Sdrn;

//  StrMessage : String;
//  procedure DoneMsgDll;far;external 'GetMessdll.Dll';
//  procedure ChangeDefs;far;external 'GetMessdll.Dll';
//  procedure DeleteMessage;far;external 'GetMessdll.Dll';
//  function  ViewMessage(NewMessage,APhoneNum,ASugMsg,
//     AOrdNumber,AMsgText :PChar) : DWord;far;external 'GetMessdll.Dll';
//  procedure CallSherutForm;far;external 'GetMessdll.Dll';
//  procedure SendStr(PhoneTxt,Msg : pchar );far;external 'GetMessdll.Dll';
//  procedure SendHebStr(PhoneTxt,Msg : pchar );far;external 'GetMessdll.Dll';
//  procedure SendForm(PhoneTxt,
//                   ShtarTxt,
//                   LakNameTxt,
//                  MozaTxt, IadTxt,ShaaTxt, MitanTxt, KamutTxt,RemarkTxt : pchar );far;external 'GetMessdll.Dll';

implementation

uses IniFiles,MslDllInterface;

{$R *.DFM}
procedure TFrm_Sdrn.FormCreate(Sender: TObject);
begin
  LoadMsgDll ;
  Timer1.Interval := Interval;
end;
//----------------------------------------------------------------------
procedure TFrm_Sdrn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Timer1.Enabled := False;  //    Timer  ����� �
  MsgDoneMsgDll;
  FreeMsgDll;
end;
  { **************** }
//                   Timer   ����� �
procedure TFrm_Sdrn.ShowClick(Sender: TObject);
begin
  Timer1.Enabled := True;
end;
//----------------------------------------------------------------------

//                       ����� ��� �� ����� �����
procedure TFrm_Sdrn.Timer1Timer(Sender: TObject);
var
  NewMessage : String;
  PhoneNum,SugMsg,OrdNumber,MsgText:string;
  HlpStr : Pchar;
  APhoneNum,ASugMsg,AOrdNumber,AMsgText :PChar;
  MsgOK : DWord;
  Ix1 : integer;
begin
  GetMem (HlpStr,500);
  GetMem (APhoneNum,500);
  GetMem (ASugMsg,500);
  GetMem (AOrdNumber,500);
  GetMem (AMsgText,500);
  try

  Timer1.Enabled := False;  //    Timer  ����� �
  Application.ProcessMessages;
  MsgOK :=MsgViewMessage(HlpStr,APhoneNum,ASugMsg,AOrdNumber,AMsgText);
  if  MsgOK <> 0 then
  begin
    if MsgOK = 1 then {no perut}
      ShowMessage (HlpStr);
    if MsgOK = 2 then { iesh perut}
       ShowMessage (APhoneNum+' ����� : '+
                    #13+
                      ASugMsg + ' ��� ' +
                    #13+
                      AOrdNumber + '�����  ' +
                    #13+
                      AMsgText
                    );
  end;
  finally
    FreeMem (HlpStr,500);
    FreeMem (APhoneNum,500);
    FreeMem (ASugMsg,500);
    FreeMem (AOrdNumber,500);
    FreeMem (AMsgText,500);
    Timer1.Enabled := TRUE; // Timer  ����� �
  end;
end;
//----------------------------------------------------------------------
procedure TFrm_Sdrn.ModifyClick(Sender: TObject);
begin
  MsgChangeDefs;
end;
//----------------------------------------------------------------------



procedure TFrm_Sdrn.Btn_ShowSherutFormClick(Sender: TObject);
begin
  MsgCallSherutForm;
end;

procedure TFrm_Sdrn.Btn_SendFormClick(Sender: TObject);
begin
  MsgSendForm( pchar(''),
            pchar('5564'),
            pchar('����') ,
            pchar('����'),
            pchar('�� ���� ���� 10')  ,
            pchar('10:00') ,
            pchar('������ ����') ,
            pchar('15') ,
            pchar('���')
                   );
end;

procedure TFrm_Sdrn.BBn_SendStrClick(Sender: TObject);
begin
  MsgSendStr(pchar(''),
          pchar('5564') );
end;

procedure TFrm_Sdrn.Btn_SendHebStrClick(Sender: TObject);
begin
  MsgSendHebStr(pchar(''),
             pchar('�����') );
end;



end.

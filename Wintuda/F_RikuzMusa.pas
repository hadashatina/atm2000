unit F_RikuzMusa;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, DBTables, Mask, ExtCtrls, ComCtrls,
  AtmAdvTable, AtmComp, Grids, DBGrids, ToolWin, DBFilter, AdvSearch,
  AtmLookCombo;

type
  TFrm_RikuzMusa = class(TForm)
    Tbl_RikuzMusaSCHOOL_YEAR: TIntegerField;
    Tbl_RikuzMusaSEMEL_MODAD: TIntegerField;
    Tbl_RikuzMusaSEMEL_YESHUV: TIntegerField;
    Tbl_RikuzMusaCODE_DARGAT_CLASS: TIntegerField;
    Tbl_RikuzMusaCODE_MEGAMA: TIntegerField;
    Tbl_RikuzMusaCODE_STATUS_SHIB: TIntegerField;
    Tbl_RikuzMusaCODE_MAKBILA: TIntegerField;
    Tbl_RikuzMusaLAST_UPDATE: TDateTimeField;
    DS_RikuzMuas: TDataSource;
    Panel2: TPanel;
    Tbl_RikuzMusa: TAtmTable;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Tbl_RikuzMusaDATA_SOURCE: TIntegerField;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    Tbl_RikuzMusaLookMegama: TStringField;
    Tbl_RikuzMusaLookClass: TStringField;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ToolButton1: TToolButton;
    TBtn_OpenSearch: TToolButton;
    Panel1: TPanel;
    Label1: TLabel;
    EditSCHOOL_YEAR: TAtmDbHEdit;
    Label2: TLabel;
    EditSEMEL_MODAD: TAtmDbHEdit;
    Tbl_RikuzMusaCODE_STATION: TIntegerField;
    Tbl_RikuzMusaTOTAL_ADULTS: TIntegerField;
    Tbl_RikuzMusaTOTAL_YOUNG: TIntegerField;
    Tbl_RikuzMusaTOTAL_TEACHER: TIntegerField;
    Tbl_RikuzMusaSUG_MASLUL: TIntegerField;
    Tbl_RikuzMusaHOUR: TDateTimeField;
    Tbl_RikuzMusaDAY1: TIntegerField;
    Tbl_RikuzMusaDAY2: TIntegerField;
    Tbl_RikuzMusaDAY3: TIntegerField;
    Tbl_RikuzMusaDAY4: TIntegerField;
    Tbl_RikuzMusaDAY5: TIntegerField;
    Tbl_RikuzMusaDAY6: TIntegerField;
    Tbl_RikuzMusaDAY7: TIntegerField;
    DBRG_SugMaslul: TDBRadioGroup;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    EditCODE_DARGAT_CLASS: TAtmDbHEdit;
    EditCODE_MEGAMA: TAtmDbHEdit;
    EditCODE_MAKBILA: TAtmDbHEdit;
    GroupBox1: TGroupBox;
    Label14: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    Panel3: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    EditLAST_UPDATE: TAtmDbHEdit;
    DBGrid_RikMusaDetail: TDBGrid;
    Tbl_Station: TAtmTable;
    Tbl_RikuzMusaLookNameStation: TStringField;
    Tbtn_NewSchool: TToolButton;
    RxDBFilter1: TRxDBFilter;
    Tbl_RikuzMusaTemp: TAtmTable;
    TBtn_ViewRecords: TToolButton;
    AtmAdvSearch_ViewRecords: TAtmAdvSearch;
    Qry_ViewRecords: TQuery;
    AtmAdvSearch_YearMosad: TAtmAdvSearch;
    Lbl_Hour: TLabel;
    DBEdit1: TAtmDbHEdit;
    Tbl_Mosad: TAtmTable;
    AtmAdvSearch_Mosad: TAtmAdvSearch;
    Tbl_RikuzMusaLookNameMosad: TStringField;
    DBText6: TDBText;
    AtmDbComboBox_CODE_MEGAMA: TAtmDbComboBox;
    AtmDbComboBox_CODE_DARGAT_CLASS: TAtmDbComboBox;
    AtmAdvSearch_SemelYeshuv: TAtmAdvSearch;
    TBtn_Report: TToolButton;
    AtmDbComboBox_StatusShib: TAtmDbComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Tbl_RikuzMusaAfterInsert(DataSet: TDataSet);
    procedure Tbl_RikuzMusaAfterPost(DataSet: TDataSet);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_RikuzMusaBeforePost(DataSet: TDataSet);
    procedure EditCODE_DARGAT_CLASSBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure EditCODE_MEGAMABeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Tbtn_NewSchoolClick(Sender: TObject);
    procedure Tbl_RikuzMusaBeforeInsert(DataSet: TDataSet);
    procedure TBtn_ViewRecordsClick(Sender: TObject);
    procedure AtmAdvSearch_ViewRecordsBeforeExecute(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_ViewRecordsAfterExecute(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    TempFilter :TStringList;
  end;

Const
     EmptyFilter='CODE_STATUS_SHIB=089208934';
var
  Frm_RikuzMusa: TFrm_RikuzMusa;

implementation
Uses Crt_Glbl,AtmRutin,DMAtmCrt,AtmConst;
{$R *.DFM}

procedure TFrm_RikuzMusa.FormCreate(Sender: TObject);
var
   i:longint;
   SList :TStringList;
begin
     TempFilter:=TStringList.Create;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_DargatClass),Tbl_RikuzMusaLookClass);
     SList.AddObject(IntToStr(SugTavla_Megama),Tbl_RikuzMusaLookMegama);
     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_Megama),AtmDbComboBox_CODE_MEGAMA);
     SList.AddObject(IntToStr(SugTavla_DargatClass),AtmDbComboBox_CODE_DARGAT_CLASS);
     SList.AddObject(IntToStr(SugTavla_StatusShibMusa),AtmDbComboBox_StatusShib);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;


     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
end;

procedure TFrm_RikuzMusa.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     TempFilter.Free;
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');

end;

procedure TFrm_RikuzMusa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_RikuzMusa);
end;

procedure TFrm_RikuzMusa.Tbl_RikuzMusaAfterInsert(DataSet: TDataSet);
Var
   I :LongInt;
begin
     SetAtmDbEditF4State(Self);
     if ((rXDBFilter1.Filter.Count>0) And
        (rXDBFilter1.Filter[0]<>EmptyFilter)) Or(rXDBFilter1.Filter.Count=0)  Then
     Begin
          Tbl_RikuzMusa.FieldByName('SCHOOL_YEAR').Value:=Tbl_RikuzMusaTemp.FieldByName('SCHOOL_YEAR').Value;
          Tbl_RikuzMusa.FieldByName('SEMEL_MODAD').Value:=Tbl_RikuzMusaTemp.FieldByName('SEMEL_MODAD').Value;
          Tbl_RikuzMusa.FieldByName('CODE_DARGAT_CLASS').Value:=Tbl_RikuzMusaTemp.FieldByName('CODE_DARGAT_CLASS').Value;
          Tbl_RikuzMusa.FieldByName('CODE_MAKBILA').Value:=Tbl_RikuzMusaTemp.FieldByName('CODE_MAKBILA').Value;
          Tbl_RikuzMusa.FieldByName('SUG_MASLUL').Value:=Tbl_RikuzMusaTemp.FieldByName('SUG_MASLUL').Value;
          Tbl_RikuzMusa.FieldByName('CODE_MEGAMA').Value:=Tbl_RikuzMusaTemp.FieldByName('CODE_MEGAMA').Value;
          Tbl_RikuzMusa.FieldByName('HOUR').Value:=Tbl_RikuzMusaTemp.FieldByName('HOUR').Value;
          For I:=1 To 7 Do
              Tbl_RikuzMusa.FieldByName('Day'+IntToStr(I)).Value:=Tbl_RikuzMusaTemp.FieldByName('Day'+IntToStr(I)).Value;
     End;

end;

procedure TFrm_RikuzMusa.Tbl_RikuzMusaAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     if (rXDBFilter1.Filter.Count>0) And
        (rXDBFilter1.Filter[0]=EmptyFilter) Then
     Begin
         RxDBFilter1.Active:=False;
         RxDBFilter1.Filter.Clear;
         RxDBFilter1.Filter.AddStrings(TempFilter);
         RxDBFilter1.Active:=True;
//         rXDBFilter1.Filter.SaveToFile('C:\filter.sql');
     End;
end;

procedure TFrm_RikuzMusa.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
end;

procedure TFrm_RikuzMusa.Tbl_RikuzMusaBeforePost(DataSet: TDataSet);
Var
   I :LongInt;
   Bool :Boolean;
begin
     Tbl_RikuzMusa.FieldByName('Last_Update').AsDateTime:=Now;
     Bool:=False;
     For I:=1 To 7 Do
         Bool:=(Bool) Or (Tbl_RikuzMusa.FieldByName('Day'+IntToStr(I)).AsInteger=1);
     if Not Bool Then
     Begin
         ShowMessage('�� ����� ���� ������');
         Abort;
         Exit;
     End;

     if Tbl_RikuzMusaSUG_MASLUL.AsInteger=0 Then
        Tbl_RikuzMusaHOUR.Clear;

     if Not TAtmTable(DataSet).ValidateLookupFields Then
          Exit;

     if (rXDBFilter1.Filter.Count>0) And
        (rXDBFilter1.Filter[0]=EmptyFilter) Then
     Begin
         TempFilter.Clear;
         TempFilter.Add('SCHOOL_YEAR='+Tbl_RikuzMusaSCHOOL_YEAR.AsString +' And');
         TempFilter.Add('SEMEL_MODAD='+Tbl_RikuzMusaSEMEL_MODAD.AsString +' And');
         TempFilter.Add('CODE_DARGAT_CLASS='+Tbl_RikuzMusaCODE_DARGAT_CLASS.AsString +' And');
         TempFilter.Add('CODE_MAKBILA='+Tbl_RikuzMusaCODE_MAKBILA.AsString +' And');
         TempFilter.Add('SUG_MASLUL='+Tbl_RikuzMusaSUG_MASLUL.AsString +' And');
         TempFilter.Add('CODE_MEGAMA='+Tbl_RikuzMusaCODE_MEGAMA.AsString);
     End;
end;

procedure TFrm_RikuzMusa.EditCODE_DARGAT_CLASSBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_DargatClass;
end;

procedure TFrm_RikuzMusa.EditCODE_MEGAMABeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Megama;
end;

procedure TFrm_RikuzMusa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_RikuzMusa:=Nil;
     Action:=caFree;
end;

procedure TFrm_RikuzMusa.Tbtn_NewSchoolClick(Sender: TObject);
begin
     RxDBFilter1.Active:=False;
     RxDBFilter1.Filter.Clear;
     rXDBFilter1.Filter.Add(EmptyFilter);
     ActiveControl:=FindFirstControlInTabOrder(Self);
     RxDBFilter1.Active:=True;
end;

procedure TFrm_RikuzMusa.Tbl_RikuzMusaBeforeInsert(DataSet: TDataSet);
begin
     if ((rXDBFilter1.Filter.Count>0) And
        (rXDBFilter1.Filter[0]<>EmptyFilter)) Or(rXDBFilter1.Filter.Count=0)  Then
        Tbl_RikuzMusaTemp.GotoCurrent(Tbl_RikuzMusa);
end;

procedure TFrm_RikuzMusa.TBtn_ViewRecordsClick(Sender: TObject);
begin
     AtmAdvSearch_ViewRecords.Execute;
end;

procedure TFrm_RikuzMusa.AtmAdvSearch_ViewRecordsBeforeExecute(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     With DM_AtmCrt.Qry_Temp Do
     Begin
          Active:=False;
          Sql.Clear;
          Sql.Add('Select Distinct SCHOOL_YEAR,SEMEL_MODAD,NAME_MOSAD');
          Sql.Add('From RikMusa,MOSAD');
          Sql.Add('Where RikMusa.Semel_Modad=Mosad.Semel_Mosad');
     End;
     if AtmAdvSearch_YearMosad.Execute Then
     Begin
          Qry_ViewRecords.ParamByName('PSCHOOL_YEAR').Value:=
             StrToInt(GetFieldFromSeparateString(AtmAdvSearch_YearMosad.ReturnString,AtmAdvSearch_YearMosad.SeparateChar,1));
          Qry_ViewRecords.ParamByName('PSEMEL_MOSAD').Value:=
             StrTOInt(GetFieldFromSeparateString(AtmAdvSearch_YearMosad.ReturnString,AtmAdvSearch_YearMosad.SeparateChar,2));
     End;
end;

procedure TFrm_RikuzMusa.AtmAdvSearch_ViewRecordsAfterExecute(
  Sender: TObject);
Var
   StrH:String;
   I :LongInt;
begin
     StrH:=AtmAdvSearch_ViewRecords.ReturnString;
     if AtmAdvSearch_ViewRecords.Success Then
     Begin
         RxDBFilter1.Active:=False;
         RxDBFilter1.Filter.Clear;
         rXDBFilter1.Filter.Add('SCHOOL_YEAR='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,1) +' And');
         rXDBFilter1.Filter.Add('SEMEL_MODAD='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,2) +' And');
         rXDBFilter1.Filter.Add('CODE_DARGAT_CLASS='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,3) +' And');
         rXDBFilter1.Filter.Add('CODE_MAKBILA='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,4)+' And');
         rXDBFilter1.Filter.Add('SUG_MASLUL='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,5)+' And');
         rXDBFilter1.Filter.Add('CODE_MEGAMA='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,6));
         For I:=1 To 7 Do
             rXDBFilter1.Filter.Add('And DAY'+IntToStr(I)+'='+GetFieldFromSeparateString(StrH,AtmAdvSearch_ViewRecords.SeparateChar,6+I));
         rXDBFilter1.Filter.SaveToFile('c:\Aviran.Sql');
         RxDBFilter1.Active:=True;
         Tbl_RikuzMusa.First;
     End;
end;

procedure TFrm_RikuzMusa.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msRikuzMusa],False,CrtMeholelHandle);

end;

end.

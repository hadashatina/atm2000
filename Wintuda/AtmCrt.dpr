{ 16/03/99 11:20:01 > [Aviran on AVIRAN] check in: (1.0)  / None }
{ 16/03/99 10:57:16 > [Aviran on AVIRAN] check in: (0.1)  /  }
library AtmCrt;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ExceptionLog,
  SysUtils,
  Classes,
  IniFiles,
  Dialogs,
  Windows,
  Crt_Glbl in 'Crt_Glbl.pas',
  DMAtmCrt in 'DMAtmCrt.pas' {DM_AtmCrt: TDataModule},
  F_Calendar in 'F_Calendar.pas' {Frm_Calendar},
  F_Maslul in 'F_Maslul.pas' {Frm_Maslul},
  F_Mosad in 'F_Mosad.pas' {Frm_Mosad},
  F_Musa1 in 'F_Musa1.pas' {Frm_Musa1},
  F_RikuzMusa in 'F_RikuzMusa.pas' {Frm_RikuzMusa},
  F_SchoolHour in 'F_SchoolHour.pas' {Frm_SchoolHour},
  F_Site in 'F_Site.pas' {Frm_Site},
  F_Station in 'F_Station.pas' {Frm_Stations},
  AtmRutin in 'I:\ShareUnit\AtmRutin.pas',
  AtmConst in 'I:\ShareUnit\AtmConst.pas',
  Tavla in 'Tavla.pas' {F_Tavla},
  F_Shaar in 'F_Shaar.pas' {Frm_Shaar},
  F_Rehev in 'F_Rehev.pas' {Frm_Rehev},
  f_Nehag in 'f_Nehag.pas' {Frm_Nehag},
  F_Lakoach in 'F_Lakoach.pas' {Frm_Lakoach},
  F_MehironMoatzot in 'F_MehironMoatzot.pas' {Frm_MehironMoatzot},
  F_MakavHeshbonNehag in 'F_MakavHeshbonNehag.pas' {Frm_MakavHeshbonNehag},
  MslDllInterface in 'I:\ShareUnit\MslDllInterface.pas',
  FrmSearch in 'FrmSearch.pas' {Frm_Search},
  F_PicNehag in 'F_PicNehag.pas' {Frm_PictureNehag},
  F_MslPoint in 'F_MslPoint.pas' {Frm_MslPoint},
  F_Musa in 'F_Musa.pas' {Frm_Musa},
  F_ParamsMoatzot in 'F_ParamsMoatzot.pas' {Frm_ParamsMoatzot},
  F_MaslulSoroka in 'F_MaslulSoroka.pas' {Frm_MaslulSoroka},
  F_Range in 'F_Range.pas' {Frm_Range},
  F_Mahzeva in 'F_Mahzeva.pas' {Frm_Mahzeva},
  F_MakavHeshbon in 'F_MakavHeshbon.pas' {Frm_MakavHeshbon},
  F_Mehiron in 'F_Mehiron.pas' {Frm_Mehiron},
  F_LakoachAtm in 'F_LakoachAtm.pas' {Frm_LakoachAtm},
  F_RangeDateCode in 'F_RangeDateCode.pas' {Frm_RangeDateCode},
  F_MakavPreforma in 'F_MakavPreforma.pas' {Frm_MakavPreforma},
  F_AtmPopup in 'F_AtmPopup.pas' {Frm_AtmPopup};

{$R *.RES}

Procedure OpenCrt(FormNum :LongInt;MainValue :ShortString);
Begin
     OpenAtmCrt(FormNum,MainValue);
End;

Procedure InitAtmCrt(AliasName,DirForIni:ShortString);
Begin
Try
     DirectoryForIni:=DirForIni;
     CrtDirForCurUser:=DirectoryForIni;
     MslDllLoaded:=LoadMslDll;
     CurCrtAliasName:=AliasName;
     InitCrtVariables;
     CreateDataMode(AliasName);
Except On E:Exception Do
  ShowMessage(e.Message);
End;
End;

Procedure DoneAtmCrt;
Begin
     FreeMslDll;
     MslDllLoaded:=False;
     FreeDllHandle(CrtMeholelHandle);
     FreeAllForms;
     DestroyDataMode;
End;

Procedure MyDllProc(Reason :Integer);
Begin
     if Reason = DLL_PROCESS_DETACH Then
        DoneAtmCrt;
End;


Exports InitAtmCrt,
        DoneAtmCrt,
        OpenCrt,
        OpenMslPointForm;
Begin
     DLLProc:=@MyDLLProc;
end.


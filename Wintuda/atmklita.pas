unit atmklita;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, HMenus, ExtCtrls, HebForm, StdCtrls, HLabel, HDBEdit, Mask,
  DBCtrls, HDBMemo;

type
  TFrmKlita = class(TForm)
    Panel1: TPanel;
    HebMainMenu1: THebMainMenu;
    HebMainMenu: TMenuItem;
    N1: TMenuItem;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    HebForm1: THebForm;
    HebLabel1: THebLabel;
    HebLabel2: THebLabel;
    HebLabel3: THebLabel;
    HebLabel4: THebLabel;
    HebLabel5: THebLabel;
    HebLabel6: THebLabel;
    HebLabel7: THebLabel;
    HebLabel8: THebLabel;
    HebLabel9: THebLabel;
    HebLabel10: THebLabel;
    HebLabel11: THebLabel;
    HebLabel12: THebLabel;
    HebLabel13: THebLabel;
    HebLabel14: THebLabel;
    HebLabel15: THebLabel;
    HebLabel16: THebLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    HebDBEdit1: THebDBEdit;
    HebDBEdit2: THebDBEdit;
    HebDBEdit3: THebDBEdit;
    HebDBMemo1: THebDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmKlita: TFrmKlita;

implementation
 Uses
  UniDataModule1;

{$R *.DFM}




procedure TFrmKlita.FormCreate(Sender: TObject);
begin
   with AtmDataModule do
    begin
      TblAzmn.Open;
      TblShib.Open;
    end;
end;

procedure TFrmKlita.FormDestroy(Sender: TObject);
begin
   with AtmDataModule do
    begin
      TblAzmn.Close;
      TblShib.Close;
    end;  
end;

end.

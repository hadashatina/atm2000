unit Grf_data;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, Menus, Buttons,IniFiles,DB;
//------------   DEFINITIONS -----------------
const
       DrawRectWidth = 4;
            {width of Shulaim for rectangle in terms of RowWidth}
       MaxNumOfCars = 100;   {only for Lists capacity}
       MaxNumOfShibForCar = 100;
       TextOfEmpty =' �� ����� ';
// --------- VARIABLES  -------------
var BegPoint,
    EndPoint : TDateTime;
    MajorDeltaX : longint; {pixels}
    MajorDeltaT : TDateTime ;
    DrawColWidth : longint;
    SugMajor : byte;
    MajorValue,OldMajorValue : longint;
    NumSubMajor : longint;
type
  TShibPair = record
    LNo_Rishui :shortString;
    LBegTime   : TDateTime;
  end;
  TLinePair = record
    XBeg,XEnd : real;
  end;
var LinePair : TLinePair;
type TCarObject = class
        LNo_Rishui : shortstring;
        LNo_Kod    : longint;
        LNo_Group  : longint;
        LShem_Nehag,
        LRem       : ShortString;
        constructor Create(NRish : shortstring;NGrp,NOrd:longint;NShem_Nehag,NRem:ShortString);
      end;
var CarObject : TCarObject;
type TCarList = class(TList)
         constructor Create ;
         procedure  BuildList;
         destructor Destroy ; override;
     end;
var  CarList : TCarList;
type TShibObject = class
        LCarNo     : longint;
        LShem_Nehag,
        LNo_Rishui : shortstring;
        LBegTime,
        LEndTime   : TDateTime;
        LBegPoint,
        LEndPoint  : real;
        LRec_No,
        LDriverNo1,
        L_Status,
        LAzmnNo   : longint;
        LLakName,
        LFromPlace,
        LToPlace,
        LAzmnCarKind:ShortString;
        constructor Create(NRish,NShem_Nehag:shortstring;
                BTime,ETime : tdatetime;NRecNo,NCarNo,NDriverNo1,NAzmnNo:longint;
                NLakName,NFromPlace,NToPlace,NAzmnCarKind:ShortString);
      end;
var ShibObject : TShibObject;
type TShibList = class(TList)
         constructor Create;
         destructor Destroy ; override;
         function GetShibPairFromListMember(NMemb : longint):TShibPair;
         function GetRishui(NMemb : longint):shortstring;
         function FindShib(NRish : shortstring):longint;
         function FindFirstShib(NRish : shortstring):longint;
     end;
var  ShibList : TShibList;
var MajorList : TStringList;
              { ********************** }

//---------- PROCEDURES -------------
 procedure InitLists;
 procedure DoneLists;
 function CompareShibObject(Shib_1,Shib_2 : pointer):integer;
 function CompareCarObject_Rishui(Car_1,Car_2 : pointer):integer;
 function CompareCarObject_Kod(Car_1,Car_2 : pointer):integer;
 function CompareCarObject_Group(Car_1,Car_2 : pointer):integer;
 function GetInform(CarItem,ShibItem:longint):shortstring;
 function OverLap(ShibItem_1,ShibItem_2:longint):boolean;
 function FindOverLapLine(ShibItem_1,ShibItem_2:longint):TLinePair;
 procedure ChangeMajorDeltaT(SugMajor:longint);
 procedure RestoreDefaultValuesForDraw(SugMajor:longint);
 procedure ReadGraphIniOptions;
 procedure WriteGraphIniOptions;

              { *********************** }
implementation
uses Grf_Opt,UniDataModule1;
        { Methods Of TCarObject}
Constructor TCarObject.Create(NRish : shortstring;NGrp,NOrd:longint;NShem_Nehag,NRem:ShortString);
begin
   inherited Create;
    LNo_Rishui :=NRish;
    LNo_Group  :=NGrp;
    LNo_Kod  :=NOrd;
    LShem_Nehag:=NShem_Nehag;
    LRem:=NRem;
end;
    { ************* }
    { Methods Of TCarList}
constructor TCarList.Create;
begin
  inherited Create;
  Capacity:= MaxNumOfCars;
end;
   { ************* }
procedure TCarList.BuildList;
var ix1 : longint;
    HelpStrC,HelpStrD : shortstring;
        { ************ }
      function CarExists : boolean;
      var ix2: longint;
      begin
        result:=FALSE;
        for ix2:=0 to CarList.Count -1 do
        begin
          CarObject:=CarList.Items[ix2];
          if ShibObject.LNo_Rishui = CarObject.LNo_Rishui then
          begin
           result:=TRUE;
           break;
          end;
        end;
      end;
        { *********** }
begin
 with  AtmDataModule.QryCarShib  do
  begin
    Open;
    First;
    while not(EOF) do
    begin
      CarObject:=TCarObject.Create(
             FieldByName('Mis_Rishui').AsString, {Rishui}
             FieldByName('Kod_Kvutza').AsInteger,{Kvutza}
             FieldByName('Mis_Pnimi').AsInteger,
             FieldByName('Shem_Nehag').AsString,
             FieldByName('Mis_Rishui').AsString);{Pnimi}
      Add(CarObject);
      Next;
    end;
  end;
  for ix1:=0 to ShibList.Count - 1 do
  begin
    ShibObject:=ShibList.Items[ix1];
    if not(CarExists) then
    begin
      with AtmDataModule.QrySadrnDbGrid do
      begin
        {First; }
        MoveBy(ShibObject.LRec_No - {1}RecNo);
        CarObject:=TCarObject.Create(
             ShibObject.LNo_Rishui,          {Rishui}
             0,                              {Kvutza}
             FieldByName('CarNo').AsInteger,
             FieldByName('Shem_Nehag').AsString,
             FieldByName('AzmnNo').AsString+' '+FieldByName('Teur_Tavla').AsString);{Rem}
      end;
      Add(CarObject);
    end;
  end;
end;
   { ************* }
destructor TCarList.Destroy;
var ix1 : longint;
begin
  for ix1:=0 to Count -1 do
  begin
    CarObject:=Items[ix1];
    CarObject.Free;
  end;
  inherited destroy;
end;
    { ***************** }
        { Methods Of TShibObject}
Constructor TShibObject.Create(NRish,NShem_Nehag:shortstring;
            BTime,ETime : tdatetime;NRecNo,NCarNo,NDriverNo1,NAzmnNo:longint;
            NLakName,NFromPlace,NToPlace,NAzmnCarKind:ShortString);
begin
  inherited Create;
    LNo_Rishui:=NRish;
    LBegTime:=BTime;
    LEndTime:=ETime;
    LBegPoint:=(LBegTime - BegPoint) /(EndPoint - BegPoint);
    LEndPoint:=(LEndTime - BegPoint)/(EndPoint - BegPoint);
    LRec_No:=NRecNo;
    L_Status := 0;
    LCarNo:=NCarNo;
    LDriverNo1:=NDriverNo1;
    LShem_Nehag:=NShem_Nehag;
    LAzmnNo:=NAzmnNo;
    LLakName:=NLakName;
    LFromPlace:=NFromPlace;
    LToPlace:=NToPlace;
    LAzmnCarKind:=NAzmnCarKind;
end;
    { ************* }
    { Methods Of TShibList}
constructor TShibList.Create;
var ix1,ix2,ix3 : longint;
    HelpStrC,HelpStrD,HelpRem : shortstring;
    BegTime,EndTime : TDateTime;
    EmptyCount : longint;
begin
  inherited Create;
  Capacity:= MaxNumOfCars * MaxNumOfShibForCar;


  with AtmDataModule.QrySadrnDbGrid do
  begin
    First;
    EmptyCount:=0;
    while not(EOF) do
    begin
      BegTime:= FieldByName('ShibBeginTime').AsDateTime;
      EndTime:= FieldByName('ShibEndTime').AsDateTime;
      if FieldByName('CarNo').AsInteger <> 0 then
        HelpStrC:=FieldByName('CarNum').AsString
      else
      begin
         inc(EmptyCount);
         HelpStrC:=TextOfEmpty+ inttostr(EmptyCount);
        end;
      ShibObject:=TShibObject.Create(
                HelpStrC{Rishui},
                FieldByName('Shem_Nehag').AsString,
                BegTime{BTime},
                EndTime{ETime},
                RecNo,
                FieldByName('CarNo').AsInteger,
                FieldByName('DriverNo1').AsInteger,
                FieldByName('AzmnNo').AsInteger,
                FieldByName('Shem_Lakoach').AsString,
                FieldByName('FromPlace').AsString,
                FieldByName('ToPlace').AsString,
                FieldByName('Teur_Tavla').AsString
                );
      Add(ShibObject);
      Next;
    end;
  end;
    { ------------------------ }
end;
   { ************* }
destructor TShibList.Destroy;
var ix1 : longint;
begin
  for ix1:=0 to Count -1 do
  begin
    ShibObject:=Items[ix1];
    ShibObject.Free;
  end;
  inherited destroy;
end;
    { ***************** }
function TShibList.GetShibPairFromListMember(NMemb : longint):TShibPair;
var MyShibPair : TShibPair;
begin
  ShibObject:=Items[NMemb];
  with ShibObject do
  begin
    MyShibPair.LNo_Rishui:=LNo_Rishui;
    MyShibPair.LBegTime:=LBegTime;
  end;
  GetShibPairFromListMember:=MyShibPair;
end;
    { ************** }
function TShibList.GetRishui(NMemb : longint):shortstring;
begin
  ShibObject:=Items[NMemb];
  Result:=ShibObject.LNo_Rishui;
end;
    { ************** }
function TShibList.FindShib(NRish : shortstring):longint;
   {returns Number of some Shib in sorted List for No_Rishui
            or -1 if not found}
var N3 : longint;
   { ***** }
   function Div2AndComp(N1,N2:longint;NRish : shortstring):integer;
   begin
      if GetRishui(N2) < NRish then
      begin
         Div2AndComp:=-1;
         exit;
      end;
      if GetRishui(N1) > NRish then
      begin
         Div2AndComp:=-1;
         exit;
      end;
      if (N1 = N2) or (N2=(N1+1)) then
      begin
         if GetRishui(N1) = NRish then
         begin
           Div2AndComp:=N1;
           exit;
         end
         else
          if GetRishui(N2) = NRish then
          begin
           Div2AndComp:=N2;
           exit;
          end
          else
          begin
           Div2AndComp:=-1;
           exit;
          end;
      end;
      N3:=N1 + (N2 - N1) div 2;
      Case CompareText(NRish,GetRishui(N3)) of
         -MaxInt+1..-1:Div2AndComp:=Div2AndComp(N1,N3,NRish);
          0:begin
              Div2AndComp:=N3;
              exit;
            end;
          1..MaxInt:Div2AndComp:=Div2AndComp(N3,N2,NRish);
      end;{case}
   end;
begin
  Result:=Div2AndComp(0,Count -1,NRish);
end;
    { ************* }
function TShibList.FindFirstShib(NRish : shortstring):longint;
   {returns the first Shib in sorted ShibList for certain No_Rishui}
var HelpInt : longint;
    ix1 : longint;
begin
  HelpInt:=FindShib(NRish);
  if HelpInt = (-1) then
  begin
    Result:=-1;exit;
  end;
  Result:=0;
  for ix1:=HelpInt downto 0 do
  begin
    if (NRish <> GetRishui(ix1)) then
    begin
      Result:=ix1+1;
      Break;
    end;
  end;
end;
    { ************** }
function CompareShibObject(Shib_1,Shib_2 : pointer):integer;
begin
 { order of sort : No_Rishui,BegTime }
  if TShibObject(Shib_1).LNo_Rishui < TshibObject(Shib_2).LNo_Rishui then
  begin
    Result:=-1;exit;
  end;
  if TshibObject(Shib_1).LNo_Rishui > TshibObject(Shib_2).LNo_Rishui then
  begin
    Result:=+1;exit;
  end;
  if TshibObject(Shib_1).LBegTime < TshibObject(Shib_2).LBegTime then
  begin
    Result:=-1;exit;
  end;
  if TshibObject(Shib_1).LBegTime > TshibObject(Shib_2).LBegTime then
  begin
    Result:=+1;exit;
  end;
  if TshibObject(Shib_1).LBegTime = TshibObject(Shib_2).LBegTime then
  begin
    Result:=0;exit;
  end;
end;
  { *************** }
function CompareCarObject_Rishui(Car_1,Car_2 : pointer):integer;
begin
 { order of sort : No_Rishui }
  if TCarObject(Car_1).LNo_Rishui < TCarObject(Car_2).LNo_Rishui then
  begin
    Result:=-1;exit;
  end;
  if TCarObject(Car_1).LNo_Rishui = TCarObject(Car_2).LNo_Rishui then
  begin
    Result:=0;exit;
  end;
  if TCarObject(Car_1).LNo_Rishui > TCarObject(Car_2).LNo_Rishui then
  begin
    Result:=+1;exit;
  end;
end;
  { *************** }
function CompareCarObject_Kod(Car_1,Car_2 : pointer):integer;
begin
 { order of sort : No_Kod }
  if TCarObject(Car_1).LNo_Kod < TCarObject(Car_2).LNo_Kod then
  begin
    Result:=-1;exit;
  end;
  if TCarObject(Car_1).LNo_Kod = TCarObject(Car_2).LNo_Kod then
  begin
    Result:=0;exit;
  end;
  if TCarObject(Car_1).LNo_Kod > TCarObject(Car_2).LNo_Kod then
  begin
    Result:=+1;exit;
  end;
end;
  { *************** }
function CompareCarObject_Group(Car_1,Car_2 : pointer):integer;
begin
 { order of sort : No_Group }
  if TCarObject(Car_1).LNo_Group < TCarObject(Car_2).LNo_Group then
  begin
    Result:=-1;exit;
  end;
  if TCarObject(Car_1).LNo_Group = TCarObject(Car_2).LNo_Group then
  begin
    Result:=0;exit;
  end;
  if TCarObject(Car_1).LNo_Group > TCarObject(Car_2).LNo_Group then
  begin
    Result:=+1;exit;
  end;
end;
  { *************** }
procedure DoneLists;
begin
  CarList.Free;
  ShibList.Free;
end;
   { ************** }
function GetInform(CarItem,ShibItem:longint):shortstring;
Const
 CR = #13;
var HintCarObject : TCarObject;
    HintShibObject: TShibObject;
begin
  if (CarItem <> (-1)) and (ShibItem <> (-1)) then
  begin               {Dima 29/03/99}
    HintShibObject:=ShibList.Items[ShibItem];
    HintCarObject:=CarList.Items[CarItem];
    with HintShibObject do
    Result:=' �����='+inttostr(LAzmnNo)
             +' ���=' + inttostr(HintCarObject.LNo_Kod)
             +' �����='+LNo_Rishui
             +' ���=' + LShem_Nehag
             +' ���= ' + timetostr(LBegTime)+ '  '+  timetostr(LEndTime)
             +' ����='+LLakName
             +' ����='+LFromPlace
             +' ���='+LToPlace;
  end;
end;
   { ***************** }
function OverLap(ShibItem_1,ShibItem_2:longint):boolean;
var Shib_1,Shib_2: TShibObject;
begin
  Result:=FALSE;
  if (ShibItem_1 <> (-1)) and (ShibItem_2 <> (-1)) then
  begin
    Shib_1:=ShibList.Items[ShibItem_1];
    Shib_2:=ShibList.Items[ShibItem_2];
    if (abs(Shib_1.LBegTime - Shib_2.LBegTime) < 0.001) and
       (abs(Shib_1.LEndTime - Shib_2.LEndTime) < 0.001) then
    begin
      Result:=TRUE;
      exit;
    end;
    if  (
       (Shib_1.LBegPoint >= Shib_2.LBegPoint) and
       (Shib_1.LBegPoint <= Shib_2.LEndPoint)
       ) or
       (
       (Shib_1.LEndPoint >= Shib_2.LBegPoint) and
       (Shib_1.LEndPoint <= Shib_2.LEndPoint)
       ) or
       (
       (Shib_2.LBegPoint >= Shib_1.LBegPoint) and
       (Shib_2.LBegPoint <= Shib_1.LEndPoint)
       ) or
       (
       (Shib_2.LEndPoint >= Shib_1.LBegPoint) and
       (Shib_2.LEndPoint <= Shib_1.LEndPoint)
       )  then
    Result:=TRUE;
  end;
end;
   { ***************** }
function FindOverLapLine(ShibItem_1,ShibItem_2:longint):TLinePair;
var Shib_1,Shib_2: TShibObject;
    LinePair : TLinePair;
begin
  Result.XBeg:=0.0;Result.XEnd:=0.0;
  if OverLap(ShibItem_1,ShibItem_2) then
  begin
    Shib_1:=ShibList.Items[ShibItem_1];
    Shib_2:=ShibList.Items[ShibItem_2];
    if (Shib_1.LBegPoint >= Shib_2.LBegPoint)  then
      Result.XBeg:=Shib_1.LBegPoint
    else
      Result.XBeg:=Shib_2.LBegPoint;
    if (Shib_1.LEndPoint <= Shib_2.LEndPoint)  then
      Result.XEnd:=Shib_1.LEndPoint
    else
      Result.XEnd:=Shib_2.LEndPoint;
  end;
end;
   { ***************** }

procedure InitLists;
begin
  BegPoint:=trunc(Now);
  EndPoint:=trunc(Now+1);
  ReadGraphIniOptions;
  ChangeMajorDeltaT(SugMajor);
  MajorDeltaT := MajorDeltaT * MajorValue ;
  DrawColWidth := round((EndPoint - BegPoint)/ MajorDeltaT) * MajorDeltaX;
     { ------------------------- }
  ShibList:=TShibList.Create;
  ShibList.Sort(CompareShibObject);
  CarList:=TCarList.Create;
  CarList.BuildList;
  CarList.Sort(CompareCarObject_Rishui);
end;
   { ************* }
procedure ReadGraphIniOptions;
var
  AtmMainIniFile:TIniFile;
  WinDir:PChar;
begin
  GetMem(WinDir, 144);
  GetWindowsDirectory(WinDir, 144);
  AtmMainIniFile:=TIniFile.Create(WinDir+'\AtmMain.Ini');
  try
   with AtmMainIniFile do
   begin
     SugMajor:=ReadInteger('Graph Options','Number Of Scale',3); {default = hour}
     MajorValue:=ReadInteger('Graph Options','Quantity of Scale',2); {default = 2 hours}
     MajorDeltaX:=ReadInteger('Graph Options','Length Of Scale',100); {default = 100 pixels}
   end;
  finally
   AtmMainIniFile.Free;
   FreeMem(WinDir);
  end;
end;
  { ************** }
procedure WriteGraphIniOptions;
var
  AtmMainIniFile:TIniFile;
  WinDir:PChar;
begin
  GetMem(WinDir, 144);
  GetWindowsDirectory(WinDir, 144);
  AtmMainIniFile:=TIniFile.Create(WinDir+'\AtmMain.Ini');
  try
   with AtmMainIniFile do
   begin
     WriteInteger('Graph Options','Number Of Scale',SugMajor);
     WriteInteger('Graph Options','Quantity of Scale',MajorValue);
     WriteInteger('Graph Options','Length Of Scale',MajorDeltaX);
   end;
  finally
   AtmMainIniFile.Free;
   FreeMem(WinDir);
  end;
end;
  { ************** }
procedure ChangeMajorDeltaT(SugMajor:longint);
begin
  case SugMajor of
    0:begin
        MajorDeltaT:=strtodatetime('00:05') ; {5 minutes}
      end;
    1:begin
        MajorDeltaT:=strtodatetime('00:15') ; {1/4 hour}
      end;
    2:begin
        MajorDeltaT:=strtodatetime('00:30') ; {1/2 hour}
      end;
    3:begin
        MajorDeltaT:=strtodatetime('01:00') ; {1 hour}
      end;
    4:begin
        MajorDeltaT:=strtodatetime('02:00') ; {2 hours}
      end;
    5:begin
        MajorDeltaT:=strtodatetime('06:00') ; {6 hours}
      end;
    6:begin
        MajorDeltaT:=1; {day}
      end;
    7:begin
        MajorDeltaT:=7;{week}
      end;
    8..10:
      begin
        MajorDeltaT:=31;{month}
      end;
  end;
end;
  { ********************* }
procedure RestoreDefaultValuesForDraw(SugMajor:longint);
begin
  case SugMajor of
    0:begin
         {5 minutes}
        MajorValue:=6;
        MajorDeltaX:=100;
      end;
    1:begin
        {1/4 hour}
        MajorValue:=4;
        MajorDeltaX:=100;
      end;
    2:begin
         {1/2 hour}
        MajorValue:=4;
        MajorDeltaX:=100;
      end;
    3:begin
        {1 hour}
        MajorValue:=6;
        MajorDeltaX:=100;
      end;
    4:begin
        {2 hours}
        MajorValue:={12}6;
        MajorDeltaX:=100;
      end;
    5:begin
        {6 hours}
        MajorValue:=1;
        MajorDeltaX:=100;
      end;
    6:begin
        {day}
        MajorValue:=1;
        MajorDeltaX:=100;
      end;
    7:begin
        {week}
        MajorValue:=1;
        MajorDeltaX:=100;
      end;
    8..10:
      begin
        {month}
        MajorValue:=1;
        MajorDeltaX:=100;
      end;
  end;
end;
  { ************** Initialization ************ }
begin
  MajorList:=TStringList.Create;
  with MajorList do
  begin
     Clear;
  {0}   Add('��� ����');
  {1}   Add('��� ���');
  {2}   Add('��� ���');
  {3}   Add('���');
  {4}   Add('������' );
  {5}   Add('�� ����' );
  {6}   Add('���');
  {7}   Add('����');
  {8}   Add('����');
  {9}   Add('�����');
  {10}  Add('���');
  end;
  ReadGraphIniOptions;
  WriteGraphIniOptions;
  ChangeMajorDeltaT(SugMajor);
end.

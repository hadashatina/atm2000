{ 16/03/99 10:57:04 > [Aviran on AVIRAN] check in: (0.1)  /  }

unit AtmConst;

interface
Uses Windows;


(*********** PERMIT - ������ **************)

Const
  MaxPermitOptions =9;     // ��� �������� �� ��� �����
                       // 1 �� �����
    PermitOpen = 2;    // 2 �����
    PermitIns = 3;     // 3 ���
    PermitBrowse = 4;  // 4 �����
    PermitEdit = 5;    // 5 �����

    PermitPerut = 6;    // 6 �����
    PermitMake = 7;    // 6 ����
    PermitCopy = 8;    // 6 ����
    PermitCanc = 9;    // 6 �����


(*********** FORM #  - ��' ������ *********)

Const
//WinTuda ����� ����� ����� �����
// {}    :Wintuda -��� � Crt -����� ������� ��� ������ �� �
{}    fnHenProfMerukaz = 18;    //  �������� �������� �������
{}    fnZikLak       = 30;    //  ������� �������
{}    fnZikLakDol    = 31;    //  ������� �������: �����
{}    fnZikRehev     = 32;    //  ������� ������
{}    fnZikNehag     = 49;    //  ������� ������
    fnWinTuda      = 54;    //  ����� ������ ������
    fnCompDet      = 55;    //  ���� ����
    fnMainButtons  = 56;    //  ������ �����
    fnParams       = 57;    //  ��� ������ ����
    fnHenLak       = 58;    //  �������� ����
    fnHenLakMerukaz  = 59;    //  �������� �������
    fnHenLakSikum  = 60;    //  �������� �����
    fnHenLakMizd   = 61;    //  �������� �������
    fnHenNehag     = 62;    //  �������� ����� (���� �����
    fnLakRikuz      = 63;    //  ������� �����
    fnHenProf      = 64;    //  �������� ��������
    fnHenLakDol    = 65;    //  �������� ���� - �����
    fnRehevRikuz      = 66;    //  ������� ����
    fnNehagRikuz      = 67;    //  ������� ����

    fnHashmalYom      =90;
    fnHashmalTrv      =91;
    fnMokedAtm        =92;
    fnAboutBox        =93;
//AtmCrt ����� ����� ����� ������
// {}    :Wintuda -��� � Crt -����� ������� ��� ������ �� �
  fnCalendar      = 1;
  fnMaslul        = 2;
  fnMaslulMoatza  = 3;
  fnMosad         = 4;
  fnMusa          = 5;
  fnRikuzMusa     = 6;
  fnSchoolHour    = 7;
  fnSite          = 8;
  fnStation       = 9;
  fnTavla         = 10;
  fnTavla2        = 11;
  fnRehev         = 12;
  fnNehag         = 13;
  fnLakoach       = 14;
  fnMehiron       = 15;
  fnMakav         = 16; //���� ��������
  fnRehevNoNehag  = 17; //����� ��� ��� ����� ������ ���
{}  fnSearchForm    = 18; //����� �� �� ���
  fnMusa1         = 19; //����� ���� ������ ������
  fnPictureNehag  = 20; //����� �����
  fnMaslulSoroka  = 21; //������� �������
  fnParamsMoatzot = 22; //���� ������ ������� �������
  fnRange         = 23; //����� ������
  fnMahzeva       = 24; //������
  fnMehironMoatzot= 25; //������ ������
  fnMakavNehag    = 26; //���� �-� �����
  fnLakoachAtm    = 27;//����� ������ ���� �������
  fnMakavPreforma = 28;//���� �-� ��������
  fnShaar         = 29; //���� ����
{}  fnAtmPopup      = 30;
{}  fnNewLak        = 31;
{}  FnMLai          = 32;
  fnTormim        = 33; // ����� ������ ���"�
  fnPritim        = 34; // ����� ������
  fnCity          = 35; //����� �������(����)
  fnCompCalendar  = 36; // ����� ��� ��� �����
  fnKabala        = 37; // ����� ��� ��� �����
  fnHesZM         = 38; // �� ����� �����
  fnHesZA         = 39; // �� ����� ����-���
  fnHesZE         = 40; // �� ����� ������
  fnHesZH         = 41; // �� ����� ������
  fnHafk          = 42; // �����
  fnMlay          = 43; // ����
  fnMasKA         = 44; // ������ ����-���
  fnMasKW         = 45; // ������ �����-����
  fnMasKM         = 46; // ������ �����
  fnEska          = 47; // ������� ����
  fnHatz          = 48; // ����/�����
{}  fnSmartM        = 49;
  fnSapak         = 50; // �����
  fnHshbon        = 51; // ������� ����� (��� ���
  fnPermit        = 52;  // ����� ������ �� ���� ������
  fnPrintAll      = 53;  // ����� ������ �� ���� ������


{      If CrtOpt in [6,7,8,9] Then
        If (PermitTbl[CrtCnt,1] in [18,30..32,49,55..67]) Then
          SetVal2:=0
        Else
          SetVal2:=2;

      If CrtOpt in [2,3,4,5] Then
        If (PermitTbl[CrtCnt,1] in [18,30..32,49,55..67]) Then
          SetVal2:=2
        Else
           SetVal2:=0;

      If (CrtOpt in [2]) And (PermitTbl[CrtCnt,1] in
         // Wintuda
          [fnCompDet, fnMainButtons, fnParams, fnRehevRikuz, fnLakRikuz, fnNehagRikuz,
         // AtmCrt
           fnPrintAll, fnPermit]) Then
                 }
(*********** FORM NAME - ����� ��� **********)

Const
  MaxCrt =67;   // ���� ������� �� ����� ����� ������
  
PermitGroupWinTuda=[18,30..32,49,55..67];  // �� �"������" ����� ������ ���������

PermitGroupShow2=// Wintuda
          [fnCompDet, fnMainButtons, fnParams, fnRehevRikuz, fnLakRikuz, fnNehagRikuz,
            // AtmCrt
           fnPrintAll, fnPermit];

PermitGroupShow6to9=PermitGroupWinTuda-PermitGroupShow2;

PermitGroupShow2to5=[1..MaxCrt]-PermitGroupShow6to9-PermitGroupShow2;

  { WINTUDA ���� - �� "fn" ������� �� ����    ,CRT ������ - ��  }
  { ���� �� ���� ������ ����� ����� ���� ���� �������� �������� }
  CrtDefNames : array[1..MaxCrt] of String =
{01}  ('��� ���',                      {02}    '�������',
{03}    '������� ������',              {04}    '������',
{05}    '������',                      {06}    '������: �����',
{07}    '����',                        {08}    '�����',
{09}    '�����',                       {10}    '������ ������',
{11}    '������ 2',                    {12}    '�����',
{13}    '�����',                       {14}    '������',
{15}    '��������',                    {16}    '���� ��������',
{17}    '�����: ��� ����� ������',
{18}    '�������� �������� �������',   // fnHenProfRikuz
{19}    '������: ������ ������',       {20}    '����� �����',
{21}    '������� ���-�����',           {22}    '������ �������',
{23}    '������',                      {24}    '������',
{25}    '������ ������',               {26}    '���� �������� �����/�����',
{27}    '���� �����',                  {28}    '���� �������� �������',
{29}    '�����',
{30}    '������� �������',          // fnZikLak
{31}    '������� �������: �����',   // fnZikLakDol
{32}    '������� ������',           // fnZikRehev
{33}    '������',                      {34}    '������',
{35}    '����',                        {36}    '��� ��� �����',
{37}    '�����',
{38}    '�������� ����/�����/��: �����',
{39}    '�������� ����/�����/��: �������',
{40}    '�������� ����/�����/��: �����',
{41}    '�������� ����/�����/��: ��. ����',
{42}    '������',                      {43}    '����',
{44}    '�������� ��-����: �������',   {45}    '�������� ��-����: ������ ������',
{46}    '�������� ��-����: �����',     {47}    '�������� ����',
{48}    '�����',
{49}    '������� ������',           // fnZikNehag
{50}    '�����',                       {51}    '�������� �����',
{52}    '������',
{53}    '����� ����',               // fnPrintAll
{54}    '����� ������ ������',      // fnWinTuda
{55}    '���� ����',                // fnCompDet
{56}    '������ �����',             // fnMainButtons
{57}    '������',                   // fnParams
{58}    '�������� ����',            // fnHenLak
{59}    '�������� �������',         // fnHenLakMerukaz
{60}    '�������� �����',           // fnHenLakSikum
{61}    '�������� �������',         // fnHenLakMizd
{62}    '�������� �����',           // fnHenNehag
{63}    '������� �����',            // fnLakRikuz
{64}    '�������� ��������',        // fnHenProf
{65}    '�������� ����: �����',     // fnHenLakDol
{66}    '������� ������',           // fnRehevRikuz
{67}    '������� ������');          // fnNehagRikuz

(******* BUTTONS - ������ ********)

Const

  MaxImgButtons = 43;   // Wintuda ���� ������ ������ ���� ����� ��
  WTMaxImg      = 14;   // WinTuda ���� �������� ��� ����� ����
  MaxImg        = 114;  // AtmCrt ���� �������� ��� ����� ����

  (*** WINTUDA ������ ������� �� ***)
  btnWTNav      = 0;    // �������
  btnWTNavEdit  = 1;    // ������� ���� �����
  btnWTSmartBtn = 2;    // ����� ���

  (*** ATMCRT ������ ������� �� ***)
  btnNav        = 0;    // �������
  btnCopyRec    = 1;    // ���� �����
  btnSearch     = 2;    // �����
  btnRpt        = 3;    // ���
  btnAddr       = 4;    // ����� ��������
  btnMap        = 5;
  btnDist       = 6;
  btnSeq        = 7;
  btnMhrDel     = 8;
  btnMhrCpy     = 9;
  btnMhrFlt     = 10;
  btnMhrNew     = 11;
  btnSmart      = 12;
  btnDrvLak     = 13;
  btnYear       = 14;
  btnPrnt       = 15;
  btnNew        = 16;
  btnUpdF       = 17;
  btnItra       = 18;
  btnMrgn       = 19;
  btnKab1       = 20;
  btnKabAll     = 21;
  btnConfig     = 22;
  btnWord       = 23;
  btnEmail      = 24;
  btnMas        = 25;
  btnKab        = 26;
  btnDown       = 27;
  btnUp         = 28;
  btnOpenHesh   = 29;
  NavEditEna    = 30;   // ������� ���� �����
  btnMhrFltDown = 33;   // ����� ������
  btnCancelHesh = 35;   // ����� �������
  btnClearKab   = 36;   // ����� ����
  btnKabDel     = 37;   // ����� �������� ������ ���� �����
  MainTSBGrid   = 'GridTavla';   // �� �����


(************ SYSTEM TABLES - ������ ������ ********)

Const //����� ������
     SugTavla_GroupSapak  = 1;//����� ���
     SugTavla_GroupNehag  = 2;//����� ���
     SugTavla_GroupRehev  = 3;//����� ���
     SugTavla_Yatzran     = 5;//����
     SugTavla_Nefah       = 6;//��� ����
     SugTavla_SugRehev    = 7;//��� ���
     SugTavla_SugDelek    = 8;//��� ���
     SugTavla_YeMida      = 9;//����� ����
     SugTavla_PnimiRehev  = 10;//���� �����
     SugTavla_Tziud       = 11;//����
     SugTavla_TnaeyTashlum= 12;//���� �����
     SugTavla_MiunSapak   = 13;//���� �����
     SugTavla_MiunNehag   = 14;//���� �����

     SugTavla_MiunRehev   = 16;//���� �����
     SugTavla_GroupLako   = 17;//����� ������
     SugTavla_MiunLako    = 18;//����� ������
     SugTavla_Agaf        = 19;//���
     SugTavla_Machlaka    = 20;//�����
     SugTavla_CodeRemark  = 21;//��� ����
     SugTavla_Bituach     = 24;//����� ����

     SugTavla_StatusRehev = 27;//����� ����
     SugTavla_MedaNehag   = 28;//���� �� ����
     SugTavla_Degem       = 30;//��� ���
     SugTavla_Shaar       = 31; //���
     SugTavla_Azmana      = 32;//���� �����
     SugTavla_BalutRehev  = 33;//����� ���

     SugTavla_Status_Hoze =34;     // ����� ����-����
     SugTavla_Hoze_Srv    =35;     // ���� ����� - ����
     SugTavla_Hoze_Sug_Rehev  =36; // ���� ��� -���� ����
     SugTavla_Sug_Pool    =37;     // ��� ��� - ����
     SugTavla_Sibat_Haavara_Hoze =38; // ���� ����� - ���� ����
     SugTavla_Status_Bakasha_Internet = 39; // ����� ����� ������ �������
     SugTavla_Sug_Heshbonit=40;  // ���� �������� ����

     SugTavla_GroupParit  = 301; //����� ������
     SugTavla_IfyunParit  = 302;// ����� ����
     SugTavla_SugParit    = 303;// ��� ����
     SugTavla_TzevaParit  = 304;// ��� ����
     SugTavla_ShenSnif    = 350;// ���� ������
     SugTavla_Matbeot     = 351;//���� ������ ������

// (**********  ������ ��� - ���� ������/��������/������  (*************** ���, 22/4/200
     SugTavla_HazStatus   = 5000;//����� ����� (����/�����/�� ���
     SugTavla_ShibStatus  = 5001;//����� ����� - �����, ���� ����, ���� ���, ���� �����

//�����
     SugTavla_SugMitan1      = 100;// ��� ���� �� ������
     SugTavla_Bankim         = 101;//���� �����
     SugTavla_Matbea         = 102;//�� ����
     SugTavla_SugHovala      = 103;//��� �����
     SugTavla_CellularStatus = 104;//����� ������
     SugTavla_GroupMaslul    = 105; //����� �������
     SugTavla_HeshbonLehiuv  = 106;//����� ����� ���"�
     SugTavla_SugTashlum     = 107;//���� �����
     SugTavla_Status         = 108;//���� �����
     SugTavla_HanMatbea      = 109;//���"� ������
     SugTavla_ToEzor         = 110;//�������
// ������
     SugTavla_SugMitan           = 500;//��� ����-����
     SugTavla_SugHinuch          = 501;//��� �����
     SugTavla_DargatClass        = 502;//���� ����
     SugTavla_Megama             = 503;//����
     SugTavla_Harig              = 504;//��� ����
     SugTavla_IsufPizur          = 505;//����� �����
     SugTavla_ZakautHasaa        = 506;//����� ����
     SugTavla_HarigBitzua        = 507;//���� �����
     SugTavla_NoseHasaa          = 508;//���� ����
     SugTavla_SugMusa            = 509;//��� ����
     SugTavla_SugDay             = 510;//��� ��� ���� ���
     SugTavla_SugHazmana         = 511;//��� �����-����
     SugTavla_StatusHazmana      = 512;//����� �����-����
     SugTavla_StatusShibMusa     = 513;//����� ����� ������
     SugTavla_SugCalendar        = 514;//��� ��� ���
     SugTavla_SugMehiron         = 515;//��� ������
     SugTavla_SugStation         = 516;//��� ����
     SugTavla_SugPikuach         = 517;//��� �����
     SugTavla_SugMaslul          = 518;//��� �����(�����/�����
     SugTavla_IfyunMaslul        = 519;//������ ����� (�����,������� ���
     SugTavla_AchuzHishtatfut    = 520;//���� �������=0 �������� �������
     SugTavla_Def                = 530;//����
//���� �������
     SugTavla_DatabaseType   = 1000;//��� ���� �������
     SugTavla_SugInterfaceHan= 1001;//��� ���� ����"�
     Sug_Tavla_SugSoftware   = 1002;//��� �����
// ����� �����
     SugTavla_SugMitanShkila = 601;
     SugTavla_SugTashlumShkila = 602;

// ����� ��"�
     SugTavla_SibatHantzcha    = 26;//1100; // ����� ����� �����
     SugTavla_Kehila           = 1101; // ������
     SugTavla_Aguda            = 1102; // ������
     SugTavla_KodMiunTormim    = 1103; // ���� ����

// ����� ��
     SugTavla_MatzavTzovar     = 21; // ��� ����
     SugTavla_SugGas           = 8; // ��� ��
     SugTavla_Dhifut           = 504;//��� ������
     sugTavla_SugMadNefah      = 517 ; // ��� �� ����

Const //����� ������ 2 KodTvla2
    ST2_BankHafkadot        = 101;
    //����� ������� Kod_Tavla=���� �����;
    //Teur_Tavla=�� ����,���� ����,��� ���;
    //Teur_Tavla2=���� ���"� �� ����
    ST2_GroupRehev_MailList = 3;//����� E-MAIL ������ ������
    ST2_Rashut              = 501;//������
    ST2_TaarifBus           = 502;//������ ������ �������-���� ��� �����
    ST2_Mishmert            = 503; //����� �������
    ST2_Store               = 504; //����� ������
    ST2_Pritim              = 505; //����� ������

Const //���� ������
 //���
     IndFields_Rehev_PrimaryKey='MIS_RISHUI';//���� ���� ���
 //������
     IndFields_Azmn_PrimKey='AZMNNO';//���� ���� ������
 //Pass
     IndFields_UserName='USERNAME';
 //Maslul
     IndFields_Maslul_PrimaryKey='CODE_MASLUL';
//AtmShib
     IndFields_AtmShib_PrimaryKey='SHIBAZMNNO;SHIBAZMNDATE;SHIBKIND;SHIBNO';
//AtmAzmn
      IndFields_AtmAzmn_PrimaryKey='AZMNNO';


Const
  //Registry ������
     RegRootForAtm :HKEY=HKEY_LOCAL_MACHINE;
     RegKeyForAtm='\SOFTWARE\Atm';
     RegCurrentUserForAtm :HKEY=HKEY_CURRENT_USER;
     RegKeyForDohMas='\DohMas';
     RegKeyForDohPerotDelekTaditran = '\PerotDTadiran';
     RegKeyForDohDelek='\Dohot';
     RegKeyForMaslulon='\SOFTWARE\Atm\Maslulon';
     RegKeyPermit='\SOFTWARE\Atm\Permit';
     RegKeySelect='\SOFTWARE\Atm\Selectiv';
     RegKeyNzr98='\SOFTWARE\Atm\Nzr98';
     RegKeyNzr2000='\SOFTWARE\Atm\Nzr2000';
     RegKeyForSadran='\SOFTWARE\Atm\Saa';
     RegKeyForTovala='\SOFTWARE\Atm\Tovala';
     RegKeyForAtmTool='\SOFTWARE\Atm\AtmTool';
     {$ifdef Atm2003}
        RegKeyForAtm2000='\Software\Atm\Atm2003';
        RegKeyForAtm2003='\Software\Atm\Atm2003';
    {$else}
        RegKeyForAtm2000='\Software\Atm\Atm2000';
    {$endif}




     RegKeyForFormsData='\Software\Atm\Atm2003\Forms';//������ �� ����� �� ��� ���� ��� ���� �� ��� ��� (HKEY_CURRENT_USER)

(**** DIRECTORIES ****)
     RegValueForScriptsFiles='Scripts';//���� ������ ��������
     RegValueForBin='BinFiles';//Exe & Dll �����
     RegValueForUsersDir='UsersDir';//����� ������ ������
     RegValueForImagesDir='Images';//���� ������ ��������
     RegValueForInstallDir='InstallDir';//����� ����� ������
     RegValueForImagesFiles='Images';//���� ������ ��������


(**** PERMIT / SELECTIVE ****)
     RegKeyForCurrentUser='CurrentUser';
     RegValueForUserName='UserName';         // �� ����� ������
     RegValueForPassword='Password';         // ���� ������
     RegValueForDBUserName='DBUserName';     // �� ����� ����� �������
     RegValueForDBPassword='DBPassword';     // ���� ����� �������
     RegValueForLastLoginName='LastLoginName';  // �� ������ ������ ����� ������ ����� ��
     RegValueForCurrentCompanyName='CurCompanyName';//�� ����� ������� ������
     RegValueForCurrentCompanyIni='CurCompanyIni';//�� ���� ������ ����� ������ Comp.ini


     RegKeyForAppFileName='AppFileName';//�� ���������
     RegKeyForMapImage='MaslulMapImageDir';//����� ���� ���� �� �������

(**** ������ ����� 2003 ****)
     RegKeyAtmMenuIsAtm2003='AtmMenuIsAtm2003';
     RegKeyForFont='CurFontName';
     RegKeyForFontSize='CurFontSize';
     RegValForItraTokef='HazShowItraTokef'; // ���� ���� ����� ��� ���� ����' ������
     RegValCloseAtmMenu='AutoCloseAtmMenu';
     RegValShowLakInMehir='ShowLakInMehir';
     RegValCopyPriceA2B='CopyPriceA2B';
     RegValShowMehiron='RegValShowMehiron';
     RegValFocusLakAfterF12='FocusLakAfterF12';
     RegValMlayONLY='MlayOnly';
     RegVal10ShibsCurUser='Last10ShibsOnlyForCurUser';
     RegValAgentSignHaz='AgentSignatureInHaz';
     RegValLimitOpenWin='LimitOpenWin';
     RegValShowYearDialog='ShowYearDialog';
     RegValCtrlShortCut='CtrlShortCut';
     RegValDoDblBuffer='DoDubbleBuffer';
     RegValLakEndDate='ShowLakEndDate';
     RegValUseMsl='UseMsl';
     RegValMakorLast='PrintMakorLast';
     RegValChangeAsm='ChangeAsm';
     RegValHazFromPrit='SelToHazFromPrit'; // ��� ����� ������ ���� ����� ���� ���� ������ �� �������
     RegValDoautoRehev='DoAutoInsertRehev';       // ����� ��� ��� �������� �� �� ����, ������ ������ ���
     RegValDoAutoSetNehegInRehev='SetNehagInRehev';  // ���� ���� ���� ��������, ��� ����� ��. ���

     { ����� ����� �� ����2003 - ���. }
//     RegKeyPermitTbl='\Software\Atm\Atm2003\Permit'; // ������ (������
//     RegKeyPermitTbl='\Software\Atm\Atm2003\Permit'; // ������ (������
//     RegAtmForCurrentCompanyName='CurCompanyName';//�� ����� �������
//     RegAtmForCurrentCompanyIni='CurCompanyIni';//�� ���� ������ ����� ������
//     RegValPermitUser='User';
//     RegValPermitDBUser='DBUser';
//     RegValPermitDBPass='DBPass';
//     RegValSelCompIni='CompIni';
//     RegValSelCompName='CompName';

Const//�������
     SuperUserName='B1B1B1B1';
     PasswordEncryptKey='AvIrAn MoRdO';
     PasswordDecryptKey=184;

Const //��� ����
     poPrice1           = 1;
     poPrice2           = 2;
     poPrice3           = 3;
     poPrice4           = 4;
     poPrice1PlusPrice2 = 5;
     poAllPrices        = 6;

Const //��� ����� �������
      slSum      = 0;//����
      slMax      = 1;//�������
      slRelative = 2;//��� ����
      slMaxTotal = 3;//������� ����� ����� �����
      slMinTotal = 4;//������� ���� ��� ������ ���� �� �� ������� ����� ��
      slFix      = 5;//���� ���� ��� �����

Const//��� ������ ������� ���� ����� ���� �����
     sbNone       = 0;//
     sbMax        = 1;//������� ���� ������ ������
     sbMin        = 2;//������� ���� ������ ������

Const //������ ����� ���� ����� �� ����� ���
     apnNewShib = 0;
     apnNewAzmn = 1;
     apnNone    = 2;

Const //ShibKind ��� �����
     skDeleted    = -1; //����� �����
     skNormal     =  1; //����� �����
     skHiuvNehag  =  2; //���� �����
     skZikuyLak   =  3; //����� ����
     skMasMizdamen=  4; //������� �� ������
     skShkila     =  5; //����� ����� ����� �����
     skShkilaDeleted  = -5; // ����� ������ ����� �����

Const //AzmnType ��� �����
     atDeleted       = 0;//�����
     atTkufaMizdamen = 1;//������ ������
     atOnceTrip      = 2;//�� ���� ����-������
     atRavFix        = 3;//�� ���� �����
     atTurist        = 4;//�����
     atBitachon      = 5;// ����"�
     atHiuvNehag     = 11;//���� �����
     atZikuyLak      = 12;//����� ������
     atMasMizdamen   = 13 ; //������� �� ������

Const //���� ����� ������ ���� �������
  tsInsert = 0;
  tsFirst  = 1;
  tsLast   = 2;

Const //������ �����
    Grp_Erua = 0;
    Grp_Hotz = 1;
    Grp_Haz  = 2;
    Grp_Havara =3;
    Grp_Hatraa = 4;
    Grp_Cheshbon = 5;
    Grp_Tavla = 6;
    Grp_Kartasot = 8;
    Grp_Teuna = 12;
    Grp_Mitztaber = 9;

    Sh_Tipul = 0;
    Sh_Lakoach= 1;
    Sh_Mehiron= 2;
    Sh_Sapak= 4;
    Sh_nehag= 3;
    Sh_Rehev= 5;
    Sh_None = -1;

    msRehev        = '�����';
    msAzmnMizdamen = '������ �������';
    msAzmnKavua    = '������ ������';
    msKartasot     = '������';
    msCalendar     = '����� ���';
    msLakoach     = '������';
    msMosad        = '������';
    msMusa         = '������';
    msSchoolHour   = '����� ����';
    msNehag        = '�����';
    msSapak        = '�����';    
    msRikuzMusa    = '����� ������';
    msStation      = '�����';
    msNihuli       = '��������';
    msPrivate      = '������';
    msTnua         = '������';
    msMaslul       = '�������';
    msMehiron      = '��������';

Const //MNAME �����
     Mname_Key  = 'Atm2000Mname';
     MnameDecryptKey = 182;

Const //��� ���� ����
     Amla_In_Total  = 0;
     Amla_Per_Tnua = 1;

Const
     MehironDefaultToDate='01/01/2100';

Const
     Version_WinTuda = '2.0.0.1911';
     Version_AtmCrt  = '1.0.1';

Const//������
     rtAtmAzmn='AtmAzmn';//���� ������

Const //���� �����
      Atm2000IniFileName = 'AtmCfg.Ini';//EXE ����� ��� �� �
         SectionForMain = 'Main';
           KeyForCompanyName = 'CompanyName';//String �� �����
           KeyForMAM = 'MAM';//Real ��"�
           KeyForAliasName = 'Alias';//String
           SqlDateFormat = 'SqlDateFormat';//Sql����� ������ �
           FirstStateRecord = 'FirstStateRecord';//0=tsInsert,1=tsFirst,2=tsLast ������ ������ ����� ���� ��
           CrtMaslulForBrinksKey = 'CrtMaslulForBrinks'; // // ��� ����� �������� ����� ��� ����� �������
           CrtPritimRegKey        = 'CrtPritimReg'; //��� ����� �� ����� ������ ������ ������
           CrtMslInMosadKey          = 'MslForMosad'; // ��� ������ ����� ������� ������ ������
           CrtMehironAsKit       = 'CrtMehironAsKit'; // ����� ������� ������ ������� �����
           CrtStationsNumbering  = 'CrtStationsNumbering'; // ����� ����� �� �� ��� ��� �����
           SqlMode = 'SqlMode';//0=Local,1=Client/Server
           IniWinTudaSugSystem='WinTudaSugSystem';//0=Tovala,1=Maz
           SQLServerType='SQLServerType';//Paradox,Oracle,MSSQL,Interbase,Sybase
           KeyAliasForMname = 'AliasForMName';//MName �� ����� ������ ����� ��
           KeyAtmIndexKeyFooter = 'AtmIndexKeyFooter'; //AtmIndex ������� ������ ���� ����� ��
           KeyCrtSqlMehironBatch = 'SqlMehironBatch' ; // �� ���� ���� ����� �������� ������� �� ������

           SectionForWinTuda = 'WinTuda';
           KeyForAutoCalcTotalTeuda = 'AutoCalcTotalTuda';//Boolean ����� ������� �� ��"� ������
           KeyForActionAfterPostNew = 'ActionAfterPostNew';//Integer ������ ����� ���� ����� �� ����� ���
           KeyForBringRehevFromNehag = 'BringRehevFromNehag';//Boolean ��� ��� ���� ���� ������ ���� ����
           KeyForCheckRehev          = 'CheckRehevFromCard';// ��� ��� ���� ���� ������
           KeyForStopCalcInRange = 'StopCalcInRange'; // ��� ������ ����� ���� ���� ����� ����� ���� ��� �����
           KeyForDatesReadOnly = 'DatesReadOnly'; // ���� ����� ������ � ReadOnly
           KeyForGetNehagBy = 'GetNehagBy';//�� ���� ����� �� ����� �� ����
           KeyForRetzefTeuda = 'RetzefFieldName';//�� ���� ���� ����� ��� ������
           KeyUpdateDefaultsScript = 'UpdateDefaultsScript' ;
           KeyAdmonAsag            = 'AdmonAsag' ; // ����� ������ �� �������� �� ������ ���� )���� ���� ���������
           KeyForRetzefTeudaAction = 'RetzefFailAction'; // ����� ��� �� ������ ���� ����� �� ������
           KeyForLakMustBeEqual = 'LakMustBeEqual'; //���� ����� ����� ���� ����
           KeyForMelayReg       = 'MelayReg';//��� ����� �� ����� ������ ������ �����
           KeyForCompareShibAsmcta = 'CompareUpdateAzmcta' ; // ��� ������ ������ ������ ������� ������
           KeyForShowSaveZikuyNehag = 'ShowSaveZikuyNehag' ; // ��� ������ ����� ����� ���� ����� ��� ��� ����� ���� ���� � ���� ����� ���� �������
           KeyForAsfiCompani        = 'AsfiCompani' ; // Menu ��� ���� ���� - ����
           KeyForBellCompani        = 'BellCompani' ;
           KeyForFieldForDiscount   = 'FieldsForDiscount' ; // �� ��� ������� ���� ���� ������ � ����� �
           KeyForOverriteMaslulPrice = 'OverwriteMaslulPrice'; //// ��� ����� ���� ���� ������ ���� ����� ���� ������
           KeyCrtFromLakRange        = 'FromLakRange' ;
           KeyCrtToLakRange          = 'ToLakRange' ; 
           IniWinTudaCalcMehirFromAzmn = 'CalaPriceFromAzmn' ;
           KeyCrtLoZkayHasaot          = 'LoZkayHasaot' ;
           KeyForLoadSearchFromFile    = 'LoadSearchFromFile' ; // ��� ������ �� ���� ����� ����� ������ 

           SectionForRange='Range';//������
           KeyForAzmnFrom='FromAzmn';//LongInt ���� ������
           KeyForAzmnTo='ToAzmn';//LongInt ���� ������

           SectionForMoatzot='Moatzot';
           KeyForSemelRashut='SemelRashut';//Integer ��� ����
           KeyForAgeYoung='AgeYoung';//Integer �� ��� �� ��� ��� ����
           KeyForKMMezakeYoung='KMMezakeYoung';//Integer �"� ���� ���� �������
           KeyForKMMezakeAd='KMMezakeAd';//Integer �"� ���� ���� �������
           KeyForRatioAdYo='RatioAdYo';//Real ��� ������ ������

           SectionForReplaceCrtNumber='ReplaceCrtNum'; //����� ����� ������

           SectionForShibDefault='Tbl_ShibDefault'; //������ ���� �� �������� ������ �������

Const
    UserAtm2000IniFileName = 'UserAtmCfg.Ini';//In the User Folder
      UserSectionForMain = 'Main';
        KeyShowOnlyActiveCar = 'ShowOnlyActiveCar';//����� �� ����� ������
        KeyShowOnlyActiveClient = 'ShowOnlyActiveClient';//��� ������ ������ ����
        KeyShowOnlyActiveDriver = 'ShowOnlyActiveDriver';//����� ����� ������ ����
        KeyUserCode = 'UserCode';
      UserSectionMokedAtm = 'MokedAtm';
        KeyLastReadMessage = 'LastReadMessageNum'; //���� ������ ������� ������
        KeyMokedAlias      = 'MokedAlias' ; // � Alias ���� ���� �����
        


Const             {���� ���}
      Nzr2000IniFileName = 'Netzer.Ini';  //EXE ����� ��� �� �
         NzrSectionForMain = 'Main';
           NzrKeyForCompanyName = 'CompanyName';//String �� �����
           NzrKeyForMAM = 'MAM';//Real ��"�
           NzrKeyForAliasName = 'Alias';//String

Const
     HebBidiKeyboard='0000040D';

Const
  OracleDateFormatSql='ALTER SESSION SET NLS_DATE_FORMAT = ''MM/DD/YYYY HH24:MI:SS''';//���� ����� �� ������ �� �����
//  OracleDateFormatSql='ALTER SESSION SET NLS_DATE_FORMAT = ''MM/DD/YYYY HH24:MI:SS''';//���� ����� �� ������ �� �����
  SybaseDateFormatSql='set dateformat mdy';
Const
    {�� ����� ���� ������ ��� �����}
    SubSystemSoroka=400;
    SubSystemMhir  =401;

Const //���� �������� ����� HesbonitKind
  hkShekel = 1;  //������� �����
  hkDolar  = 2;  //������� ������
  hkLloMam = 3;  // ������� ��� ��"�
  hkZikuyNehag = 4;//����� ����
  hkPrephorma = 5; //������� �����
  hkPrephormaDolarit=6;//������� ������
  HkMasMizdamen=7;//������� �� ������
  HkHashmal=8;//������� ���� ����
  HkHiyuvZikuyShekel=9 ;// �-� ���� /�����/�� ������
  HkHiyuvZikuyDolar=10 ;// �-� ���� /�����/�� ������
  hkKabala = 11; // ����  �����
  hkZikuyShekel  = 12; //������� ����� ���
  hkHyuvNehag  = 13;//���� ���
  hkZikuyDolar = 14; //������� ����� ����
  hkKabalaD    = 15 ; // ���� �������
  hkKabalaMas  = 16 ; // �-� ��/���� �����
  hkKabalaMasD = 17 ; // �-� ��/���� �������
  hkKabalaM    = 18 ; // ����  ���"�
  hkHshbSapak  = 19 ; // ������� ����
  hkHshbZikBit  = 21 ; // ������� ����� ������
  hkHshbZikBitDolarit  = 22 ; // ������� ����� ������ �������
  hkHshbZikBitLelomam  = 23 ; // ������� ����� ������ ��� ��"�
  hkKabalaMasBitul     = 24 ; // �-� ��/���� ����� ����� ������
  hkHshbZikBitHiuyuvNehag = 25 ; // ������� ����� ������ ���� ���
  hkMAsMakor = 500; //����� �� ����� ����
  hkItrot = 501; //����� ����� ����
  hkRibit = 502; //����� ���� ������ ����
Const //��� ����� ������

  cbkOpen = 0; //���� �����
  cbkPrintSource = 1; //��� ����
  cbkUpdateOnly  = 2; //����� ������ ����

Const //���� ����� ������ �1-100 ������ ����� , �100 ����� �� �����
  stCheck      = 0; //���
  stMezoman    = 1; //�����
  stHavaraBank = 100; //����� ������
  stMasBamakor = 101; //�� ����� �� ���� �����

Const
  KopaKabalaNumForMezomanIn = -100; //���� ���� �������� ����� ������� �������
  KopaKabalaNumForMezomanOut = -101;//���� ���� �������� ������ ������� (�����
  KopaKabalaNumForMezomanInDolar = -102; //  ���� ���� �������� ����� ������� ������� ������
  KopaKabalaNumForMezomanOutDolar = -103;//(���� ���� �������� ������ ������� (����� ������

Type
   TFieldWitnLookup   =(AzmnMaslulCode1,AzmnMaslulCode2,AzmnMaslulCode3,AzmnMaslulCode4,HovalaKind,DriverNo1,MaslulCode1);
Const
   PisFields  : array [TFieldWitnLookup] of String = ('AzmnMaslulCode1','AzmnMaslulCode2','AzmnMaslulCode3','AzmnMaslulCode4','HovalaKind','DriverNo1','MaslulCode1');
   DescFields : array [TFieldWitnLookup] of String = ('AzmnMaslul1','AzmnMaslul2','AzmnMaslul3','AzmnMaslul4','Sug_hovala','DriverName','Maslul1');
const {���� ������ ������ PriceKind}
   pkShekel = 0;{���� �����}
   pkLeloMam = 1; {��� ��"�}

const // ���� ����� �����
     kaNoMaam = 3; // ������� ����� ����� ��� ���

Const {���� ������� ������}
    srKod_Lak = 1;
    srKod_KvutzatLak = 2;
    srKod_Nehag = 3;
    srKod_Hyuv = 4;
    srMis_Azmn = 5;
    srKod_Maslul = 6;
    srHovala_Kind = 7;
    srMerakez = 8;
    srKod_KvutzatNehag = 9;
    srCarNum = 10;
    srKodMiyun = 11;
    seKod_KvutzaRehev=12;

Const //�� ������ �� ���� ������� AtmIndex ������ �
  aiHeshbonitLak       = 'HsbLak';//�������� �����
  aiHeshbonitLakAdd    = 'HsbLakAddition';//�������� �����  ����� �����
  aiHeshbonitdolarit   = 'HsbLakDolarit';{������� ������ �� ��� ����� ���� ����}
  aiHeshbonitNehag     = 'HsbNehag'; //�������� ������
  aiZikuyToNehag       = 'HsbZikuyNehag'; //����� ������
  aiHeshbonitMerosh    = 'HsbMerosh'; //�������� ����
  aiHeshbonitArmy      = 'HsbArmy'; //�������� ����"�
  aiHeshbonitZikuy     = 'HsbZikuy'; //�������� �����
  aiHeshbonitMasKabala = 'HsbMasKabala'; //������� �� ����
  aiHeshbonitMas       = 'HsbMas'; //������� ����/�����/��
  aiProforma           = 'Proforma';//��������
  aiKabala             = 'Kabala' ; //�����
  aiKabalaSap          = 'KabalaSap' ; //����� ������
  aiKabalaM            = 'KabalaM'; //(����� ���"� (�������
  aiHafkada            = 'Hafkada'; //������ �����
  aiHafkadaM           = 'HafkadaM'; //������ ���"�
  aiShtarMetan         = 'ShtarMetan';//��� ����
  aiVizaTayaroot       = 'VizaTayaroot';//���� ������
  aiMasMizdamen        = 'MasMizdamen';//������� �� ������
  aiHashmal            = 'Hashmal';//������� ���� ����
  aiTeodatMishloachIn  = 'TeodatMishloachIn';   // ����� ����� ������
  aiTeodatMishloachOut = 'TeodatMishloachOut'; // ����� ����� ������
  aiTeodatMishloachAvr = 'TeodatMishloachAvr'; // ����� ����� ������
  aiTeodatMishloachHaz = 'TeodatMishloachHaz'; // ����� ����� ������
  aiTeodatMishloachRet = 'TeodatMishloachRet'; // ����� ����� ������
  aiNispachLeRakevet   = 'NispachLeRakevet'; //����� ����� ����� ����
  aiTMishlochRakevet   = 'TMishloachRakevet';//����� ����� ����
  aiTHachsanaRakevet   = 'THachsanaRakevet';//����� ����� �� ����
  aiTeudaToNehagRakevet   = 'TeudaToNehagRakevet';//����� ���� ����
  aiTeudaToLakRakevet   = 'TeudaToLakRakevet';//����� ����� ����
  aiHesbonEska          = 'HesbonEska'; // ����� �����
  aiHoratTashlumMada    = 'HoratTashlum';//����� ����� ��"� ���
  aiHazmana             = 'HesbHazmana'; // �����/����
  aiHeshbonitZikuyBitul = 'HeshbonitZikuyBitul';//������� ����� ����� ����� �������
  aiHeshbonitZikuyBitulDolari = 'HeshbonitZikuyBitulD';//������� ����� ����� ����� ������� �������
  aiHeshbonitMasKabalaBitul  = 'HsbMasKabalaBitul'; //������� �� ����
  aiHeshbonitZikuyBitulHiyuvNehag = 'HeshbonitZikBitHiyNe';//������� ����� ����� ����� ���� ���

  aiTodaIndex           = 'TudaNo';//��� ����

Const{MaskEdit}
    meDate =  '!90/90/0000;1;_';
    meTime = '!90:00;1;_';
    ShekelSymbol =  Chr (0164);
    EUROSymbol =  Chr (0128);
    DolarSymbol = '$';

Const {Messages Numbers}
      WM_USER                   =  1024;
      ATMM_OPEN_NETZER_REHEV    =  WM_USER+100;
      ATMM_TORMIM_CLOSED        =  WM_USER+200;

Type
   TDatabaseType = (SqlServer,Oracle,Standard);
   TAzmanaType   = (Soroka,Mer);
   TSugHesbonit  = (Sicum,StandardToLak,PrephormaMerukezet,Prephorma,DolaritStandardToLak,DolaritSicum,ZikuyToNehag,HiyuvToNehag,ZikuyShekel,ZikuyDolar,Merukezet,Azmana,AtzatMechir,PrephormaMerukezetToNehag,PrephormaDolarit,ZikuyToRehev,MasMizdamen,Hashmal,Brinks,StandardToLakSicumToRikuz,ZikuyFromBitul,Leasing,ZikuyFromBitulDolarit,ZikuyFromBitulHiyuvNehag,ZikuyFromBitulHiyuvBrinks);

Type
   TPermitTbl = Array[0..MaxCrt,1..MaxPermitOptions] of Integer;
   TPermitPtr = ^TPermitTbl;

implementation

end.




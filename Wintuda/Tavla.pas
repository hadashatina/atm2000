unit Tavla;

interface

uses
  Bde, DBTables, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, StdCtrls, DBCtrls,
  ExtCtrls, Buttons, Scale250, Grids, DBGrids, AtmAdvTable,AtmRutin;

type
  TF_Tavla = class(TForm)
    DBGrid_KodTavla: TDBGrid;
    HebDBGrid2: TDBGrid;
    Panel1: TPanel;
    SpeedButton2: TSpeedButton;
    DBNavigator1: TDBNavigator;
    SpeedButton1: TSpeedButton;
    Scale1: TScale;
    Tbl_Kod_Tavla: TAtmTable;
    Tbl_Kod_TavlaSug_Tavla: TIntegerField;
    Tbl_Kod_TavlaKod_Tavla: TIntegerField;
    Tbl_Kod_TavlaTeur_tavla: TStringField;
    Ds_Kod_Tavla: TDataSource;
    Panel2: TPanel;
    HebDBText1: TDBText;
    Tbl_Sug_Tavla: TAtmTable;
    Tbl_Sug_TavlaSug_Tavla: TIntegerField;
    Tbl_Sug_TavlaTeur_tavla: TStringField;
    Ds_Sug_Tavla: TDataSource;
    Splitter1: TSplitter;
    Query1: TQuery;
    procedure HebDBGrid2Enter(Sender: TObject);
    procedure DBGrid_KodTavlaEnter(Sender: TObject);
    procedure HebDBGrid2Exit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Kod_TavlaAfterInsert(DataSet: TDataSet);
    procedure Kod_TavlaBeforePost(DataSet: TDataSet);
    procedure Kod_TavlaAfterPost(DataSet: TDataSet);
    procedure Kod_TavlaBeforeDelete(DataSet: TDataSet);
    procedure Sug_TavlaAfterInsert(DataSet: TDataSet);
    procedure Sug_TavlaBeforePost(DataSet: TDataSet);
    procedure Sug_TavlaAfterPost(DataSet: TDataSet);
    procedure Sug_TavlaBeforeDelete(DataSet: TDataSet);
    procedure Kod_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
              var Action: TDataAction);
    procedure Sug_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
              var Action: TDataAction);
    procedure FormDestroy(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  F_Tavla: TF_Tavla;

const
  eKeyViol = 9729;               {���� ����}

implementation

uses DMAtmCrt;


{$R *.DFM}
//-----------------------------------------------------------------------
procedure TF_Tavla.HebDBGrid2Enter(Sender: TObject);
begin            {DataSource - ����� �}
    DBNavigator1.DataSource:=DS_Sug_Tavla;
    DBNavigator1.VisibleButtons := [nbFirst,nbLast,nbNext,nbPrior];
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.DBGrid_KodTavlaEnter(Sender: TObject);
begin
   DBNavigator1.DataSource:=DS_Kod_Tavla;
   DbNavigator1.VisibleButtons := [nbFirst,nbLast,nbNext,nbPrior,nbCancel,nbPost,nbEdit,nbRefresh,nbDelete,nbInsert];
end;
//-----------------------------------------------------------------------
//    ����� ����� ������ ����� ������� ��� ����� ����� ��� �����
procedure TF_Tavla.HebDBGrid2Exit(Sender: TObject);
begin
   if TBL_Sug_Tavla.State in [DsEdit, DsInsert] then
      TBL_sug_Tavla.Post;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.SpeedButton2Click(Sender: TObject);
begin
   Close;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.SpeedButton1Click(Sender: TObject);
begin
  IF DBNavigator1.DataSource=DS_Sug_Tavla  then
     begin
       DBNavigator1.DataSource:=DS_Kod_Tavla;
       DBGrid_KodTavla.SetFocus;
     end
  else
     begin
       DBNavigator1.DataSource:=DS_Sug_Tavla;
       HebDBGrid2.SetFocus;
     end;
end;
//-----------------------------------------------------------------------
//            Form - � DataMode.Ttable - ����� ��������� ��
procedure TF_Tavla.FormCreate(Sender: TObject);
Var
   I :longInt;
begin
   For I:=0 To ComponentCount-1 Do
   Begin
        if Components[I] is TTable Then
           (Components[I] As TTable).Open;
   End;

   Tbl_Kod_Tavla.AfterInsert := Kod_TavlaAfterInsert;
   Tbl_Kod_Tavla.BeforePost  := Kod_TavlaBeforePost;
   Tbl_Kod_Tavla.AfterPost   := Kod_TavlaAfterPost;
   Tbl_Kod_Tavla.BeforeDelete:= Kod_TavlaBeforeDelete;
   Tbl_Sug_Tavla.AfterInsert := Sug_TavlaAfterInsert;
   Tbl_Sug_Tavla.BeforePost  := Sug_TavlaBeforePost;
   Tbl_Sug_Tavla.AfterPost   := Sug_TavlaAfterPost;
   Tbl_Sug_Tavla.BeforeDelete:= Sug_TavlaBeforeDelete;
   Tbl_Kod_Tavla.OnPostError := Kod_TavlaOnPostError;
   Tbl_Sug_Tavla.OnPostError := Sug_TavlaOnPostError;


end;
//-----------------------------------------------------------------------
//                       ���� ��� ����� ���
procedure TF_Tavla.Kod_TavlaAfterInsert(DataSet: TDataSet);
begin
   Query1.Active:=False;
   Query1.Sql.Clear;
   Query1.Sql.Add('Select Max(Kod_Tavla) MK From '+Tbl_Kod_Tavla.TableName);
   Query1.SQL.Add('Where Sug_Tavla='+Tbl_Sug_TavlaSug_Tavla.AsString);
   Query1.Active:=True;
   Try
      Tbl_Kod_TavlaKod_Tavla.AsInteger:=Query1.FieldByName('MK').AsInteger+1;
   Finally
          Query1.Active:=False;
   End;
end;
//-----------------------------------------------------------------------
//           ����� ���� ���� �� ������ �� ���� ��� ������
procedure TF_Tavla.Kod_TavlaBeforePost(DataSet: TDataSet);
begin
   if (Ds_Kod_Tavla.State = DsInsert)         and
       (Tbl_Kod_TavlaTeur_Tavla.AsString = '') then
        begin
          Tbl_Kod_Tavla.Cancel;
          Tbl_Kod_Tavla.Edit;
        end;

   if (Tbl_Kod_TavlaSug_Tavla.Value = 26) and
    ((Tbl_Kod_TavlaKod_Tavla.Value = 1)  or
     (Tbl_Kod_TavlaKod_Tavla.Value = 2)  or
     (Tbl_Kod_TavlaKod_Tavla.Value = 3)  or
     (Tbl_Kod_TavlaTeur_Tavla.Value = '�����')  or
     (Tbl_Kod_TavlaTeur_Tavla.Value = '�����')  or
     (Tbl_Kod_TavlaTeur_Tavla.Value = '�����')) then
     begin
       Showmessage ('�� ���� ����� ����� ���');
       Tbl_Kod_Tavla.Cancel;
       Tbl_Kod_Tavla.Edit;
     end;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.Kod_TavlaAfterPost(DataSet: TDataSet);
begin
  Dbisavechanges (Tbl_Kod_Tavla.handle);
  DataSet.Refresh;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.Sug_TavlaAfterPost(DataSet: TDataSet);
begin
   Dbisavechanges (Tbl_Sug_Tavla.handle);
   DataSet.Refresh;
end;
//-----------------------------------------------------------------------
//                  ���� ��� ����� ���
procedure TF_Tavla.Sug_TavlaAfterInsert(DataSet: TDataSet);
begin
   Query1.Active:=False;
   Query1.Sql.Clear;
   Query1.Sql.Add('Select Max(Sug_Tavla) MK From '+Tbl_Sug_Tavla.TableName);
   Query1.Active:=True;
   Try
      Tbl_Sug_TavlaSug_Tavla.AsInteger:=Query1.FieldByName('MK').AsInteger+1;
   Finally
          Query1.Active:=False;
   End;
end;
//-----------------------------------------------------------------------
//           ����� ���� ���� �� ������ �� ���� ��� ������
procedure TF_Tavla.Sug_TavlaBeforePost(DataSet: TDataSet);
begin
   if (Ds_Sug_Tavla.State = DsInsert)         and
      (Tbl_Sug_TavlaTeur_Tavla.AsString = '') then
      begin
        Tbl_Sug_Tavla.Cancel;
        Tbl_Sug_Tavla.Edit;
      end;
end;
//-----------------------------------------------------------------------
//  Master > Dedails ����� ������ ��� - ��� ���� ������ ���� ���� ���
procedure TF_Tavla.Sug_TavlaBeforeDelete(DataSet: TDataSet);
begin
  if MessageDlg('? ��� ���� �� ������', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
     begin
       while TBL_Kod_Tavla.RecordCount > 0 do
             TBL_Kod_Tavla.Delete;
     end
  else
     Abort;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.Kod_TavlaBeforeDelete(DataSet: TDataSet);
begin
   if MessageDlg('? ��� ���� �� ������', mtConfirmation,[mbYes, mbNo], 0) = mrNo then
      Abort;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.Kod_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (E is EDBEngineError) then
     if (E as EDBEngineError).Errors[0].Errorcode = eKeyViol then
        begin
          MessageDlg ('���� ����', mtWarning, [mbOK],0);
          Abort;
        end;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.Sug_TavlaOnPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (E is EDBEngineError) then
     if (E as EDBEngineError).Errors[0].Errorcode = eKeyViol then
        begin
          MessageDlg ('���� ����', mtWarning, [mbOK],0);
          Abort;
        end;
end;
//-----------------------------------------------------------------------
procedure TF_Tavla.FormDestroy(Sender: TObject);
Var
   I :longInt;
begin
  For I:=0 To ComponentCount-1 Do
  Begin
      if Components[I] is TTable Then
         (Components[I] As TTable).Close;
  End;
  if DM_AtmCrt.Tbl_KodTavla.Active Then
  Begin
    DM_AtmCrt.Tbl_KodTavla.Close;
    DM_AtmCrt.Tbl_KodTavla.Open;
  End;
end;

Procedure TF_Tavla.ShowForm(FormParams:ShortString);
Var
   StrH,St2 :String;
Begin
// Form Params Has 2 Params 1-Sug_Tavla, 2-Kod_Tavla

     if FormParams<>'' Then
     Begin
        StrH:=GetFieldFromSeparateString(FormParams,',',1);
        if Tbl_Sug_Tavla.FindKey([StrH]) Then
        Begin
           St2:=GetFieldFromSeparateString(FormParams,',',2);
           if Not Tbl_Kod_Tavla.FindKey([StrH,St2]) Then
           Begin
             Tbl_Kod_Tavla.Insert;
             Tbl_Kod_TavlaKod_Tavla.AsString:=StrH;
             DBGrid_KodTavla.SelectedField:=Tbl_Kod_TavlaTeur_tavla;
           End;
           ActiveControl:=DBGrid_KodTavla;
        End;
     End;
     ShowModal;
End;

end.

unit F_AtmPopup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, DBCtrls, AtmComp, ToolEdit, RXDBCtrl, AtmDBDateEdit,
  Mask, Db, DBTables, RxQuery, AtmRxQuery, ExtCtrls, ToolWin, dbnav797,Printers;

type
  TFrm_AtmPopup = class(TForm)
    UpdateSQL_AtmPopup: TUpdateSQL;
    Tbl_AtmPopup: TAtmRxQuery;
    Ds_AtmPopup: TDataSource;
    HebLabel1: TLabel;
    Kod: TAtmDbHEdit;
    Label1: TLabel;
    AtmDbHEdit1: TAtmDbHEdit;
    HebLabel23: TLabel;
    AtmDBDateEdit1: TAtmDBDateEdit;
    Label2: TLabel;
    AtmDbHMemo1: TAtmDbHMemo;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    DBNavigator1: TAtmDbNavigator;
    ToolButton1: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    Procedure PrintMessage;
  public
    { Public declarations }
    procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_AtmPopup: TFrm_AtmPopup;

implementation

uses AtmConst, AtmRutin, DMAtmCrt;

{$R *.DFM}

procedure TFrm_AtmPopup.FormCreate(Sender: TObject);
Var
  I :longInt;
begin
  Try
    Screen.Cursor:=crHourGlass;
    For I:=0 To ComponentCount-1 Do
    Begin
        if (Components[I] is TDataSet) Then
           (Components[I] As TDataSet).Open;
    End;
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_AtmPopup.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyboardManageForTableAction(Sender,Key,Shift,Tbl_AtmPopup);
end;

procedure TFrm_AtmPopup.FormDestroy(Sender: TObject);
var
   i:longint;
begin
  For I:=0 To ComponentCount-1 Do
  Begin
      if Components[I] is TDataSet Then
         (Components[I] As TDataSet).Close;
  End;
end;

procedure TFrm_AtmPopup.ShowForm(FormParams:ShortString);
Begin
  Show;
  if FormParams<>'' Then
    Tbl_AtmPopup.FindKey([FormParams])
  Else
    Tbl_AtmPopup.Last;
End;
procedure TFrm_AtmPopup.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Frm_AtmPopup:=Nil;
  Action:=caFree;
end;

Procedure TFrm_AtmPopup.PrintMessage;
Var
  I:LongInt;
  TmpRect :TRect;
Begin
  Printer.BeginDoc;
  Screen.Cursor:=crHourGlass;
  Try
    Printer.Title:='����� �����';
    Printer.Canvas.TextFlags:=ETO_RTLREADING; //���� ����
    Printer.Canvas.Font.Charset:=HEBREW_CHARSET;
    Printer.Canvas.Font.Name:='David';
    Printer.Canvas.Font.Size:=14;
    TmpRect.Bottom:=10;
    TmpRect.Left:=5;
    Printer.Canvas.TextOut(TmpRect.Left,TmpRect.Bottom,AtmDBDateEdit1.Text+' ('+Kod.Text+') ����� ����');
    TmpRect.Left:=Printer.Canvas.ClipRect.Right-
            Printer.Canvas.TextWidth('����� �'+Tbl_AtmPopup.FieldByName('MassageAuthor').AsString) -10;
    TmpRect.Bottom:=TmpRect.Bottom+Printer.Canvas.TextHeight('�')+10;
    Printer.Canvas.TextOut(TmpRect.Left,TmpRect.Bottom,'����� �'+Tbl_AtmPopup.FieldByName('MassageAuthor').AsString);
    TmpRect.Bottom:=TmpRect.Bottom+Printer.Canvas.TextHeight('�')+10;
    For I:=0 To AtmDbHMemo1.Lines.Count-1 Do
    Begin
      TmpRect.Left:=Printer.Canvas.ClipRect.Right-
            Printer.Canvas.TextWidth(AtmDbHMemo1.Lines[I]) -10;
      Printer.Canvas.TextOut(TmpRect.Left,TmpRect.Bottom+10,
              AtmDbHMemo1.Lines[I]);
      TmpRect.Bottom:=TmpRect.Bottom+Printer.Canvas.TextHeight('�')+10;
    End;

  Finally
    Printer.EndDoc;
    Screen.Cursor:=crDefault;
  End;

End;

procedure TFrm_AtmPopup.ToolButton1Click(Sender: TObject);
begin
  PrintMessage;
end;

end.

{ 16/03/99 11:20:05 > [Aviran on AVIRAN] check in: (1.0)  / None }

unit Crt_Glbl;
interface
Uses DB,Forms,Classes,Windows,IniFiles,AtmAdvTable,AtmComp,Controls,
     Messages,AdvSearch,Grids,StdCtrls,ComCtrls,Dialogs,DbTables,
     AtmDBDateEdit,Registry,SysUtils,AtmRxQuery;

   Procedure DoSaveOnTable(TheTable :TDataset);//����� ����� �� ����� �����

   Procedure CreateDataMode(AliasName:ShortString);
   Procedure DestroyDataMode;
   Procedure WriteMapImagePath(ThePath:String);
   Function ReadMapImagePath:String;
   Procedure OpenAtmCrt(FormNum :LongInt;MainValue :ShortString);
   Function CheckOpenForm(FormName:String) :Boolean;
   Procedure FreeAllForms;
   Procedure InitCrtVariables;
Var
   DirectoryForIni  :String;
   CrtDirForExeFiles:ShortString;
   CrtDirForScripts :ShortString;
   CrtDirForCurUser :ShortString;
   CrtCurUserName   :ShortString;
   CrtCurIniFileName:ShortString;
   CrtMapImagePath :String;
   CrtMAM           :Real;
   MslDllLoaded     :Boolean;
   CurCrtAliasName  :ShortString;
   CrtMeholelHandle :THandle;
   CrtSqlDateFormat :ShortString;
   CrtFirstTableState  :Byte; //��� ���� ������ ������ ����
   CrtSQLServerType :String;
   CrtSQLMode :LongInt;
implementation
Uses
  DMAtmCrt,F_Calendar,F_Maslul,F_MaslulMoatza,F_Mosad,F_Musa,F_RikuzMusa,
  F_SchoolHour,F_Site,F_Station,AtmRutin,AtmConst,Tavla,Ftavla2,F_Rehev,
  F_Nehag,F_Lakoach,F_Mehiron,F_MakavHeshbon,FrmSearch,F_Musa1,F_PicNehag,
  F_MaslulSoroka,F_ParamsMoatzot,F_Range,F_Mahzeva,F_MehironMoatzot,
  F_MakavHeshbonNehag,F_LakoachAtm,F_MakavPreforma,F_Shaar,F_AtmPopup;



Var
  CrtReplaceCrtNumber :TStringList;

Procedure InitCrtVariables;
Var
   F :TIniFile;
   R :TRegistry;
Begin
     //   ShowMessage('1');
     if CrtReplaceCrtNumber<>Nil Then
      CrtReplaceCrtNumber.Free;
     CrtReplaceCrtNumber:=TStringList.Create;
     CrtDirForExeFiles:=ExtractFilePath(Application.ExeName);
     CrtDirForScripts:=CrtDirForExeFiles;
    //   ShowMessage('2');
     Try
        CrtCurIniFileName:=RunDllFunc1StringParam('Selectiv.Dll','GetCurrentCompanyIni',DirectoryForIni+Atm2000IniFileName);
   //     ShowMessage(CrtCurIniFileName);
     Except
     End;
     if CrtCurIniFileName<>'' Then
        CrtCurIniFileName:=DirectoryForIni+CrtCurIniFileName
     Else
         CrtCurIniFileName:=DirectoryForIni+Atm2000IniFileName;
   //  ShowMessage(CrtCurIniFileName+'   7');
     R:=TRegistry.Create;
     Try
   //   ShowMessage('3');
        R.RootKey:=RegRootForAtm;
        R.OpenKey(RegKeyForAtm2000,True);
        if R.ValueExists(RegValueForScriptsFiles) Then
           CrtDirForScripts:=R.ReadString(RegValueForScriptsFiles)+'\'
        Else
          CrtDirForScripts:=ExtractFilePath(Application.ExeName)+'Scripts\';
        Try
           CrtCurUserName:=RunDllStringFuncNoParam('Permit.Dll','GetCurrentUser');
        Except
              CrtCurUserName:='Default';
        End;
        Try
            if R.ValueExists(RegValueForUsersDir) Then
            Begin
               CrtDirForCurUser:=R.ReadString(RegValueForUsersDir)+'\';
            End
            Else
                CrtDirForCurUser:=ExtractFilePath(Application.ExeName)+'Users\';
        Except
        End;
        if CrtCurUserName<>'' Then
           CrtDirForCurUser:=CrtDirForCurUser+CrtCurUserName+'\';

{        if R.ValueExists(RegValueForInstallDir) Then
           DE_InstallDir.Text:=R.ReadString(RegValueForInstallDir);}
     Finally
            R.Free;
     End;

     F:=TIniFile.Create(CrtCurIniFileName);
     Try
        CrtSqlDateFormat:=F.ReadString(SectionForMain,SqlDateFormat,ShortDateFormat);
        CrtFirstTableState:=F.ReadInteger(SectionForMain,FirstStateRecord,tsFirst);
        CrtSQLServerType:=F.ReadString(SectionForMain,SQLServerType,'Paradox');
        CrtSQLMode:=F.ReadInteger(SectionForMain,SqlMode,0);
        CrtMAM:=F.ReadFloat(SectionForMain,KeyForMAM,17);
        F.ReadSectionValues(SectionForReplaceCrtNumber,CrtReplaceCrtNumber);
     Finally
         F.Free;
     End;
     CrtMapImagePath:=ReadMapImagePath;
End;


Procedure DoSaveOnTable(TheTable :TDataset);
Begin
    if TheTable is TAtmTable Then
      TAtmTable(TheTable).SavePressed:=True
    Else
      if TheTable is TAtmRxQuery Then
        TatmRxQuery(TheTable).SavePressed:=True;
End;

Procedure CreateDataMode(AliasName:ShortString);
Begin
     if DM_AtmCrt=Nil Then
     Begin
        DM_AtmCrt:=TDM_AtmCrt.Create(Nil);
     End;
End;

Procedure DestroyDataMode;
Begin
     if DM_AtmCrt<>Nil Then
     Begin
//          DM_AtmCrt.Database_AtmCrt.Connected:=False;
          DM_AtmCrt.Free;
          DM_AtmCrt:=Nil;
     End;
End;

Procedure WriteMapImagePath(ThePath:String);
Var
   R :TRegistry;
begin
  R:=TRegistry.Create;
  Try
     R.RootKey:=RegRootForAtm;
     R.OpenKey(RegKeyForTovala,True);
     R.WriteString(RegKeyForMapImage,ThePath);
  Finally
         R.Free;
  End;
End;

Function ReadMapImagePath:String;
Var
   R :TRegistry;
   StrH :String;
begin
  R:=TRegistry.Create;
  Try
     R.RootKey:=RegRootForAtm;
     R.OpenKey(RegKeyForTovala,True);
     if R.ValueExists(RegKeyForMapImage) Then
     Begin
        StrH:=R.ReadString(RegKeyForMapImage);

        if (StrH<>'') And (StrH[Length(StrH)]<>'\') Then
            StrH:=StrH+'\';
     End
     Else
        StrH:='';
     Result:=StrH;
  Finally
         R.Free;
  End;
End;

Procedure OpenAtmCrt(FormNum :LongInt;MainValue :ShortString);
Var
  StrH :String;
Begin
  Try
     if CrtReplaceCrtNumber=Nil Then
       InitCrtVariables;
     CreateDataMode('');
     StrH:=CrtReplaceCrtNumber.Values[IntToStr(FormNum)];
     if StrH<>'' Then
       FormNum:=StrToInt(StrH);
     Case FormNum Of
        fnCalendar     : Begin
                            if CheckOpenForm('Frm_Calendar') Then
                            Begin
                              if Frm_Calendar = Nil Then
                                 Frm_Calendar:=TFrm_Calendar.Create(Nil);
                              Frm_Calendar.Show;
                            End;
                         End;
        fnMaslul       : Begin
                            if CheckOpenForm('Frm_Maslul') Then
                            Begin
                              if Frm_Maslul = Nil Then
                                 Frm_Maslul:=TFrm_Maslul.Create(Nil);
                              Frm_Maslul.ShowForm(MainValue);
                            End;
                         End;
        fnMaslulMoatza : Begin
                            if CheckOpenForm('Frm_MsllMoatsa') Then
                            Begin
                              if Frm_MsllMoatsa = Nil Then
                                 Frm_MsllMoatsa:=TFrm_MsllMoatsa.Create(Nil);
                              Frm_MsllMoatsa.Show;
                            End;
                         End;
        fnMaslulSoroka :Begin
                          if CheckOpenForm('Frm_MaslulSoroka') Then
                          Begin
                             if Frm_MaslulSoroka = Nil Then
                                Frm_MaslulSoroka:=TFrm_MaslulSoroka.Create(Nil);
                             Frm_MaslulSoroka.ShowForm(MainValue);
                          End;
                        End;
        fnMosad        : Begin
                            if CheckOpenForm('Frm_Mosad') Then
                            Begin
                              if Frm_Mosad = Nil Then
                                 Frm_Mosad:=TFrm_Mosad.Create(Nil);
                              Frm_Mosad.ShowForm(MainValue);
                          End;
                         End;
        fnMusa         : Begin
                            if CheckOpenForm('Frm_Musa') Then
                            Begin
                              if Frm_Musa = Nil Then
                                 Frm_Musa:=TFrm_Musa.Create(Nil);
                              Frm_Musa.Show;
                            End;
                         End;
        fnMusa1        : Begin
                            if CheckOpenForm('Frm_Musa1') Then
                            Begin
                             if Frm_Musa1 = Nil Then
                                Frm_Musa1:=TFrm_Musa1.Create(Nil);
                             Frm_Musa1.ShowForm(MainValue);
                            End;
                         End;
        fnRikuzMusa    : Begin
                            if CheckOpenForm('Frm_RikuzMusa') Then
                            Begin
                              if Frm_RikuzMusa = Nil Then
                                 Frm_RikuzMusa:=TFrm_RikuzMusa.Create(Nil);
                              Frm_RikuzMusa.Show;
                            End;
                         End;
        fnSchoolHour   : Begin
                            if CheckOpenForm('Frm_SchoolHour') Then
                            Begin
                              if Frm_SchoolHour = Nil Then
                                 Frm_SchoolHour:=TFrm_SchoolHour.Create(Nil);
                              Frm_SchoolHour.Show;
                            End;
                         End;
        fnSite         : Begin
                            if CheckOpenForm('Frm_Site') Then
                            Begin
                              if Frm_Site = Nil Then
                                 Frm_Site:=TFrm_Site.Create(Nil);
                              Frm_Site.Show;
                            End;
                         End;
        fnStation      : Begin
                            if CheckOpenForm('Frm_Stations') Then
                            Begin
                              if Frm_Stations = Nil Then
                                 Frm_Stations:=TFrm_Stations.Create(Nil);
                              Frm_Stations.ShowForm(MainValue);;
                            End;
                         End;
        fnTavla        : Begin
                            if CheckOpenForm('F_Tavla') Then
                            Begin
                              if F_Tavla=Nil Then
                                 F_Tavla:=TF_Tavla.Create(Nil);
                              F_Tavla.ShowForm(MainValue);
                              F_Tavla.Free;
                              F_Tavla:=Nil;
                            End;
                         End;
        fnTavla2       : Begin
                            if CheckOpenForm('Frm_Tavla2') Then
                            Begin
                              if Frm_Tavla2=Nil Then
                                 Frm_Tavla2:=TFrm_Tavla2.Create(Nil);
                              Frm_Tavla2.ShowModal;
                            End;
                         End;
        fnRehev        : Begin
                              if CheckOpenForm('Frm_Rehev') Then
                              Begin
                                  if (Frm_Rehev = Nil) Then
                                     Frm_Rehev:=TFrm_Rehev.Create(Nil);
                                  Frm_Rehev.ShowForm(MainValue);
                              End;
                         End;
        fnRehevNoNehag : Begin
                            if CheckOpenForm('Frm_Rehev') Then
                            Begin
                              if Frm_Rehev = Nil Then
                                 Frm_Rehev:=TFrm_Rehev.Create(Nil);
                              Frm_Rehev.ShowFormNoNehag(MainValue);
                            End;
                         End;
        fnNehag        : Begin
                            if CheckOpenForm('Frm_Nehag') Then
                            Begin
                              if Frm_Nehag = Nil Then
                                 Frm_Nehag:=TFrm_Nehag.Create(Nil);
                              Frm_Nehag.ShowForm(MainValue);
                            End;
                         End;
        fnLakoach        : Begin
                            if CheckOpenForm('Frm_Lakoach') Then
                            Begin
                              if Frm_Lakoach = Nil Then
                                 Frm_Lakoach:=TFrm_Lakoach.Create(Nil);
                              Frm_Lakoach.ShowForm(MainValue);
                              //Frm_Lakoach.Show;
                            End;
                         End;
        fnMehiron        : Begin
                            if CheckOpenForm('Frm_Mehiron') Then
                            Begin
                              if Frm_Mehiron = Nil Then
                                 Frm_Mehiron:=TFrm_Mehiron.Create(Nil);
                              Frm_Mehiron.ShowForm(MainValue);
                            End;
                           End;
        fnMakav         : Begin
                            if CheckOpenForm('Frm_MakavHeshbon') Then
                            Begin
                              if Frm_MakavHeshbon = Nil Then
                                 Frm_MakavHeshbon:=TFrm_MakavHeshbon.Create(Nil);
                              Frm_MakavHeshbon.Show;
                            End;
                         End;

        fnSearchForm    :Begin
                             if Frm_Search=Nil Then
                             Begin
                                  Frm_Search:=TFrm_Search.Create(Nil);
                                  Frm_Search.StartSearch;
                             End
                             Else
                                 ShowMessage('�� ���� ���� �� ���� ���� ��� �� �����');
                         End;
        fnPictureNehag  :Begin
                              if Frm_PictureNehag=Nil Then
                                  Frm_PictureNehag:=TFrm_PictureNehag.Create(Nil);
                              Frm_PictureNehag.ShowForm(MainValue);
                         End;
        fnParamsMoatzot :Begin
                            if CheckOpenForm('Frm_ParamsMoatzot') Then
                            Begin
                              if Frm_ParamsMoatzot=Nil Then
                                 Frm_ParamsMoatzot:=TFrm_ParamsMoatzot.Create(Nil);
                              Frm_ParamsMoatzot.ShowForm(MainValue);
                            End;
                         End;
        fnRange         :Begin
                            if CheckOpenForm('Frm_Range') Then
                            Begin
                              if Frm_Range=Nil Then
                                 Frm_Range:=TFrm_Range.Create(Nil);
                              Frm_Range.ShowForm(MainValue);
                            End;
                         End;
        fnMahzeva        :Begin
                            if CheckOpenForm('Frm_Mahzeva') Then
                            Begin
                              if Frm_Mahzeva=Nil Then
                                 Frm_Mahzeva:=TFrm_Mahzeva.Create(Nil);
                              Frm_Mahzeva.ShowForm(MainValue);
                            End;
                         End;
        fnMehironMoatzot :Begin
                            if CheckOpenForm('Frm_Mehiron') Then
                            Begin
                              if Frm_Mehiron = Nil Then
                                 Frm_MehironMoatzot:=TFrm_MehironMoatzot.Create(Nil);
                              Frm_MehironMoatzot.ShowForm(MainValue);
                            End;
                         End;
        fnMakavNehag    :Begin
                            if CheckOpenForm('Frm_MakavHeshbonNehag') Then
                            Begin
                              if Frm_MakavHeshbonNehag = Nil Then
                                 Frm_MakavHeshbonNehag:=TFrm_MakavHeshbonNehag.Create(Nil);
                              Frm_MakavHeshbonNehag.ShowForm(MainValue);
                            End;
                         End;
        fnLakoachAtm    :Begin
                            if CheckOpenForm('Frm_LakoachAtm') Then
                            Begin
                              if Frm_LakoachAtm = Nil Then
                                 Frm_LakoachAtm:=TFrm_LakoachAtm.Create(Nil);
                              Frm_LakoachAtm.ShowForm(MainValue);
                            End;
                         End;
        fnMakavPreforma : Begin
                            if CheckOpenForm('Frm_MakavPreforma') Then
                            Begin
                              if Frm_MakavPreforma = Nil Then
                                 Frm_MakavPreforma:=TFrm_MakavPreforma.Create(Nil);
                              Frm_MakavPreforma.Show;
                            End;
                         End;
        fnShaar        : Begin
                            if CheckOpenForm('Frm_Shaar') Then
                            Begin
                              if Frm_Shaar = Nil Then
                                 Frm_Shaar:=TFrm_Shaar.Create(Nil);
                              Frm_Shaar.ShowForm(MainValue);
                          End;
                         End;
        fnAtmPopup    : Begin
                            if CheckOpenForm('Frm_Atmpopup') Then
                            Begin
                              if Frm_AtmPopup = Nil Then
                                 Frm_AtmPopup:=TFrm_AtmPopup.Create(Nil);
                              Frm_AtmPopup.ShowForm(MainValue);
                          End;
                         End;
// David For Test
{        fnNewlak        : Begin
                            if CheckOpenForm('Frm_NewLak') Then
                            Begin
                              if Frm_Newlak = Nil Then
                                 Frm_NewLak:=TFrm_Newlak.Create(Nil);
                              Frm_Newlak.ShowForm(MainValue);
                            End;
                         End;
 }
     End;//Case
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
End;

Function CheckOpenForm(FormName:String) :Boolean;
Var
   H :THandle;
Begin
  Try
     Result:=True;
     H:=0;
     if RunDllFunc2PCharParamBool('Permit.Dll','CheckFormAccess',CrtCurUserName,FormName,True,H) Then
        Result:=True
     Else
     Begin
          MessageDlg('��� �� ����� ����� ���� ��',mtInformation,[mbOk],0);
          Result:=False;
     End;
  Except
  End;
End;

Procedure FreeAllForms;
Begin
  CrtReplaceCrtNumber.Free;
  CrtReplaceCrtNumber:=Nil;
  if Frm_Calendar <> Nil Then
  Begin
     Frm_Calendar.Free;
     Frm_Calendar:=Nil
  End;
  if Frm_Maslul <> Nil Then
  Begin
     Frm_Maslul.Free;
     Frm_Maslul:=Nil;
  End;
  if Frm_MsllMoatsa <> Nil Then
  Begin
     Frm_MsllMoatsa.Free;
     Frm_MsllMoatsa:=Nil;
  End;
  if Frm_MaslulSoroka<>Nil Then
  Begin
      Frm_MaslulSoroka.Free;
      Frm_MaslulSoroka:=Nil;
  End;
  if Frm_Mosad <> Nil Then
  Begin
     Frm_Mosad.Free;
     Frm_Mosad:=Nil;
  End;
  if Frm_Musa <> Nil Then
  Begin
     Frm_Musa.Free;
     Frm_Musa:=Nil;
  End;
  if Frm_Musa1<>Nil Then
  Begin
       Frm_Musa1.Free;
       Frm_Musa1:=Nil;
  End;
  if Frm_RikuzMusa <> Nil Then
  Begin
     Frm_RikuzMusa.Free;
     Frm_RikuzMusa:=Nil;
  End;
  if Frm_SchoolHour <> Nil Then
  Begin
     Frm_SchoolHour.Free;
     Frm_SchoolHour:=Nil;
  End;
  if Frm_Site <> Nil Then
  Begin
     Frm_Site.Free;
     Frm_Site:=Nil;
  End;
  if Frm_Stations <> Nil Then
  Begin
     Frm_Stations.Free;
     Frm_Stations:=Nil;
  End;
  if F_Tavla <> Nil Then
  Begin
     F_Tavla.Free;
     F_Tavla:=Nil;
  End;
  if Frm_Tavla2<>Nil Then
  Begin
       Frm_Tavla2.Free;
       Frm_Tavla2:=Nil;
  End;
  if Frm_Mehiron<>Nil Then
  Begin
     Frm_Mehiron.Free;
     Frm_Mehiron:=Nil;
  End;
  if Frm_MakavHeshbon<>Nil Then
  Begin
     Frm_MakavHeshbon.Free;
     Frm_MakavHeshbon:=Nil;
  End;
  if Frm_Rehev<>Nil Then
  Begin
     Frm_Rehev.Free;
     Frm_Rehev:=Nil;
  End;
  if Frm_Nehag<>Nil Then
  Begin
     Frm_Nehag.Free;
     Frm_Nehag:=Nil;
  End;
  if Frm_Lakoach <>Nil Then
  Begin
     Frm_Lakoach.Free;
     Frm_Lakoach:=Nil;
  End;
  if Frm_Search<>Nil Then
  Begin
      Frm_Search.Free;
      Frm_Search:=Nil;
  End;
  if Frm_PictureNehag<>Nil Then
  Begin
      Frm_PictureNehag.Free;
      Frm_PictureNehag:=Nil;
  End;
  if Frm_Range<>Nil Then
  Begin
      Frm_Range.Free;
      Frm_Range:=Nil;
  End;
  if Frm_Mahzeva<>Nil Then
  Begin
       Frm_Mahzeva.Free;
       Frm_Mahzeva:=Nil;
  End;
  if Frm_MehironMoatzot<>Nil Then
  Begin
     Frm_MehironMoatzot.Free;
     Frm_MehironMoatzot:=Nil;
  End;
  if Frm_MakavHeshbonNehag<>Nil Then
  Begin
      Frm_MakavHeshbonNehag.Free;
      Frm_MakavHeshbonNehag:=Nil;
  End;
  if Frm_LakoachAtm<>Nil Then
  Begin
    Frm_LakoachAtm.Free;
    Frm_LakoachAtm:=Nil;
  End;
  if Frm_MakavPreforma<>Nil Then
  Begin
     Frm_MakavPreforma.Free;
     Frm_MakavPreforma:=Nil;
  End;
  if Frm_Shaar<>Nil Then
  Begin
    Frm_Shaar.Free;
    Frm_Shaar:=Nil;
  End;
  if Frm_AtmPopup<>Nil Then
  Begin
    Frm_AtmPopup.Free;
    Frm_AtmPopup:=Nil;
  End ;
{
  if Frm_newlak<>Nil Then
  Begin
    Frm_Newlak.Free;
    Frm_Newlak:=Nil;
  End;
}
End;


Begin
    CrtMeholelHandle:=0;
end.

object Frm_UpdatePriceFromMehiron: TFrm_UpdatePriceFromMehiron
  Left = 106
  Top = 82
  Width = 443
  Height = 367
  BiDiMode = bdRightToLeft
  Caption = '����� ������ ������� ��� ������'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 435
    Height = 321
    ActivePage = TabSheet1
    Align = alClient
    Style = tsFlatButtons
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '��� ������'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 290
        Align = alClient
        BevelInner = bvLowered
        TabOrder = 0
        object RG_PriceFieldName: TRadioGroup
          Left = 2
          Top = 2
          Width = 423
          Height = 36
          Align = alTop
          Caption = '����� ����'
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            '���� �'
            '���� �'
            '���� �'
            '���� �'
            '����')
          TabOrder = 0
        end
        object RG_SumType: TRadioGroup
          Left = 2
          Top = 38
          Width = 423
          Height = 36
          Align = alTop
          Caption = '������'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '���� ���� � 0'
            '���� ���� � 0'
            '����')
          TabOrder = 1
        end
        object GroupBox1: TGroupBox
          Left = 2
          Top = 74
          Width = 423
          Height = 183
          Align = alClient
          Caption = '�������'
          TabOrder = 2
          object Label1: TLabel
            Left = 383
            Top = 18
            Width = 33
            Height = 13
            Caption = '�����'
          end
          object Label2: TLabel
            Left = 205
            Top = 18
            Width = 43
            Height = 13
            Caption = '�� ����'
          end
          object Label3: TLabel
            Left = 391
            Top = 42
            Width = 25
            Height = 13
            Caption = '����'
          end
          object Label4: TLabel
            Left = 213
            Top = 42
            Width = 35
            Height = 13
            Caption = '�� ���'
          end
          object Label5: TLabel
            Left = 364
            Top = 66
            Width = 52
            Height = 13
            Caption = '���� ����'
          end
          object Label6: TLabel
            Left = 186
            Top = 66
            Width = 62
            Height = 13
            Caption = '�� ��� ����'
          end
          object Label7: TLabel
            Left = 361
            Top = 90
            Width = 55
            Height = 13
            Caption = '���� ����'
          end
          object Label8: TLabel
            Left = 183
            Top = 90
            Width = 65
            Height = 13
            Caption = '�� ��� ����'
          end
          object Label9: TLabel
            Left = 376
            Top = 114
            Width = 38
            Height = 13
            Caption = '������'
          end
          object Label10: TLabel
            Left = 199
            Top = 114
            Width = 48
            Height = 13
            Caption = '�� �����'
          end
          object Label11: TLabel
            Left = 197
            Top = 160
            Width = 50
            Height = 13
            Caption = '�� �����'
          end
          object Label12: TLabel
            Left = 375
            Top = 160
            Width = 40
            Height = 13
            Caption = '������'
          end
          object Label13: TLabel
            Left = 376
            Top = 138
            Width = 38
            Height = 13
            Caption = '������'
          end
          object Label14: TLabel
            Left = 199
            Top = 138
            Width = 48
            Height = 13
            Caption = '�� �����'
          end
          object SpinEdit_FromLak: TSpinEdit
            Left = 267
            Top = 14
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 0
            Value = 0
          end
          object SpinEdit_ToLak: TSpinEdit
            Left = 89
            Top = 14
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 1
            Value = 99999999
          end
          object SpinEdit_FromNehag: TSpinEdit
            Left = 267
            Top = 38
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 2
            Value = 0
          end
          object SpinEdit_ToNehag: TSpinEdit
            Left = 89
            Top = 38
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 3
            Value = 99999999
          end
          object SpinEdit_FromMetan: TSpinEdit
            Left = 267
            Top = 62
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 4
            Value = 0
          end
          object SpinEdit_ToMetan: TSpinEdit
            Left = 89
            Top = 62
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 5
            Value = 99999999
          end
          object SpinEdit_FromMaslul: TSpinEdit
            Left = 267
            Top = 86
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 6
            Value = 0
          end
          object SpinEdit_ToMaslul: TSpinEdit
            Left = 89
            Top = 86
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 7
            Value = 99999999
          end
          object SpinEdit_FromTnua: TSpinEdit
            Left = 267
            Top = 110
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 8
            Value = 0
          end
          object SpinEdit_ToTnua: TSpinEdit
            Left = 89
            Top = 110
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 9
            Value = 99999999
          end
          object DateEdit_ToDate: TDateEdit
            Left = 89
            Top = 158
            Width = 90
            Height = 21
            CheckOnExit = True
            DefaultToday = True
            DialogTitle = '��� �����'
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            NumGlyphs = 2
            StartOfWeek = Sun
            Weekends = [Sat]
            YearDigits = dyFour
            TabOrder = 13
          end
          object DateEdit_FromDate: TDateEdit
            Left = 267
            Top = 158
            Width = 90
            Height = 21
            CheckOnExit = True
            DefaultToday = True
            DialogTitle = '��� �����'
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            NumGlyphs = 2
            StartOfWeek = Sun
            Weekends = [Sat]
            YearDigits = dyFour
            TabOrder = 12
          end
          object SpinEdit_FromAzmn: TSpinEdit
            Left = 267
            Top = 134
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 10
            Value = 0
          end
          object SpinEdit_ToAzmn: TSpinEdit
            Left = 89
            Top = 134
            Width = 90
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 11
            Value = 99999999
          end
        end
        object Panel2: TPanel
          Left = 2
          Top = 257
          Width = 423
          Height = 31
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 3
          object Btn_StartUPdate: TBitBtn
            Left = 128
            Top = 2
            Width = 75
            Height = 25
            Caption = '����'
            Default = True
            TabOrder = 0
            OnClick = Btn_StartUPdateClick
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333330000333333333333333333333333F33333333333
              00003333344333333333333333388F3333333333000033334224333333333333
              338338F3333333330000333422224333333333333833338F3333333300003342
              222224333333333383333338F3333333000034222A22224333333338F338F333
              8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
              33333338F83338F338F33333000033A33333A222433333338333338F338F3333
              0000333333333A222433333333333338F338F33300003333333333A222433333
              333333338F338F33000033333333333A222433333333333338F338F300003333
              33333333A222433333333333338F338F00003333333333333A22433333333333
              3338F38F000033333333333333A223333333333333338F830000333333333333
              333A333333333333333338330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object Btn_Close: TBitBtn
            Left = 210
            Top = 2
            Width = 75
            Height = 25
            Cancel = True
            Caption = '����'
            ModalResult = 2
            TabOrder = 1
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
              F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
              000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
              338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
              45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
              3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
              F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
              000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
              338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
              4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
              8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
              333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
              0000}
            NumGlyphs = 2
          end
        end
      end
    end
  end
  object ProgressBar1: TProgressBar
    Left = 16
    Top = 291
    Width = 104
    Height = 16
    Min = 0
    Max = 100
    Step = 1
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 321
    Width = 435
    Height = 19
    Panels = <
      item
        Style = psOwnerDraw
        Width = 300
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
end

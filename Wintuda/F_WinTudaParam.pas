unit F_WinTudaParam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,IniFiles, Buttons, ExtCtrls, Mask, ToolEdit, ComCtrls,Registry;

type
  TFrm_WinTudaParams = class(TForm)
    Panel1: TPanel;
    Btn_Ok: TBitBtn;
    Btn_Cancel: TBitBtn;
    PageControl1: TPageControl;
    TS_ParamWinTuda: TTabSheet;
    GroupBox1: TGroupBox;
    CB_AutoCalcTotalHeshbon: TCheckBox;
    RG_ActAfterPostNew: TRadioGroup;
    TS_Globals: TTabSheet;
    TS_Directories: TTabSheet;
    Label1: TLabel;
    Edit_Mam: TEdit;
    DE_Bin: TDirectoryEdit;
    Label2: TLabel;
    DE_Scripts: TDirectoryEdit;
    Label3: TLabel;
    DE_Users: TDirectoryEdit;
    Label5: TLabel;
    DE_InstallDir: TDirectoryEdit;
    Label6: TLabel;
    CB_BringRehevFromNehag: TCheckBox;
    RG_GetNehagBy: TRadioGroup;
    RG_FirstTableState: TRadioGroup;
    RG_SqlMode: TRadioGroup;
    RG_RezefTeuda: TRadioGroup;
    CB_WinTudaLakMastBeEqual: TCheckBox;
    RG_SqlServerType: TRadioGroup;
    GroupBox2: TGroupBox;
    CB_ShowOnlyActiveCar: TCheckBox;
    CB_ShowOnlyActiveClient: TCheckBox;
    CB_ShowOnlyActiveDriver: TCheckBox;
    procedure Btn_OkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Procedure FillVcl;
    Procedure ReadDirsFromRegistry;
    Procedure WriteDirsToRegistry;
  public
    { Public declarations }
  end;

var
  Frm_WinTudaParams: TFrm_WinTudaParams;

implementation

Uses F_GlblWinTuda, AtmConst;
{$R *.DFM}

procedure TFrm_WinTudaParams.Btn_OkClick(Sender: TObject);
Var
   F :TIniFile;
begin
     WinTudaAutoCalcSumTuda:=CB_AutoCalcTotalHeshbon.Checked;
     WinTudaMAM:=StrToFloat(Edit_Mam.Text);
     ActAfterPostNew:=RG_ActAfterPostNew.ItemIndex;
     WinTudaAlwaysBringRehevFromNehag:=CB_BringRehevFromNehag.Checked;
     WinTudaFirstTableState:=RG_FirstTableState.ItemIndex;
     WinTudaSqlMode:=RG_SqlMode.ItemIndex;
     WinTudaSqlServerType:=RG_SqlServerType.Items[RG_SqlServerType.ItemIndex];
     WinTudaLakMastBeEqual:=CB_WinTudaLakMastBeEqual.Checked;
     WinTudaShowOnlyActiveCar:=CB_ShowOnlyActiveCar.Checked;
     WinTudaShowOnlyActiveClient:=CB_ShowOnlyActiveClient.Checked;
     WinTudaShowOnlyActiveDriver:=CB_ShowOnlyActiveDriver.Checked;
     Case RG_RezefTeuda.ItemIndex Of
       0: WinTudaCheckValueExistFieldName:='';
       1: WinTudaCheckValueExistFieldName:='ShibTuda';
       2: WinTudaCheckValueExistFieldName:='ShibAsmcta';
     End; //Case;

     F:=TInifile.Create(WinTudaCurIniFileName);
     Try
         F.WriteInteger(SectionForWinTuda,KeyForActionAfterPostNew,ActAfterPostNew);
         F.WriteBool(SectionForWinTuda,KeyForAutoCalcTotalTeuda,WinTudaAutoCalcSumTuda);
         F.WriteBool(SectionForWinTuda,KeyForBringRehevFromNehag,WinTudaAlwaysBringRehevFromNehag);

         Case RG_GetNehagBy.ItemIndex Of
            0:WinTudaNehagCheckFieldName:='Kod_Nehag';
            1:WinTudaNehagCheckFieldName:='Ish_Kesher';
         End; //Case
         F.WriteString(SectionForWinTuda,KeyForGetNehagBy,WinTudaNehagCheckFieldName);
         F.WriteString(SectionForWinTuda,KeyForRetzefTeuda,WinTudaCheckValueExistFieldName);//��� ������
         F.WriteBool(SectionForWinTuda,KeyForLakMustBeEqual,WinTudaLakMastBeEqual);

         F.WriteFloat(SectionForMain,KeyForMAM,WinTudaMAM);
         F.WriteInteger(SectionForMain,FirstStateRecord,WinTudaFirstTableState);
         F.WriteInteger(SectionForMain,SqlMode,WinTudaSqlMode);
         F.WriteString(SectionForMain,SQLServerType,WinTudaSqlServerType);

     Finally
            F.Free;
     End;
     WriteDirsToRegistry;

     F:=TIniFile.Create(WinTudaPrivateIniFileName);
     Try
        F.WriteBool(UserSectionForMain,KeyShowOnlyActiveCar,WinTudaShowOnlyActiveCar);
        F.WriteBool(UserSectionForMain,KeyShowOnlyActiveClient,WinTudaShowOnlyActiveClient);
        F.WriteBool(UserSectionForMain,KeyShowOnlyActiveDriver,WinTudaShowOnlyActiveDriver);
     Finally
      F.Free;
     End;

end;

Procedure TFrm_WinTudaParams.FillVcl;
Var
  I :LongInt;
Begin
     Edit_Mam.Text:=FloatToStr(WinTudaMAM);
     CB_AutoCalcTotalHeshbon.Checked:=WinTudaAutoCalcSumTuda;
     RG_ActAfterPostNew.ItemIndex:=ActAfterPostNew;
     CB_BringRehevFromNehag.Checked:=WinTudaAlwaysBringRehevFromNehag;

     if CompareText(WinTudaNehagCheckFieldName,'Kod_Nehag')=0 Then
        RG_GetNehagBy.ItemIndex:=0
     Else
     if CompareText(WinTudaNehagCheckFieldName,'Ish_Kesher')=0 Then
        RG_GetNehagBy.ItemIndex:=1;

     if WinTudaCheckValueExistFieldName='' Then RG_RezefTeuda.ItemIndex:=0
     Else
       if CompareText(WinTudaCheckValueExistFieldName,'ShibTuda')=0 Then RG_RezefTeuda.ItemIndex:=1
       Else
         if CompareText(WinTudaCheckValueExistFieldName,'ShibAsmcta')=0 Then RG_RezefTeuda.ItemIndex:=2;

     RG_FirstTableState.ItemIndex:=WinTudaFirstTableState;
     RG_SqlMode.ItemIndex:=WinTudaSqlMode;
     CB_WinTudaLakMastBeEqual.Checked:=WinTudaLakMastBeEqual;
     CB_ShowOnlyActiveCar.Checked:=WinTudaShowOnlyActiveCar;
     CB_ShowOnlyActiveClient.Checked:=WinTudaShowOnlyActiveClient;
     CB_ShowOnlyActiveDriver.Checked:=WinTudaShowOnlyActiveDriver;
     
     For I:=0 To RG_SqlServerType.Items.Count-1 Do
       if CompareText(WinTudaSqlServerType,RG_SqlServerType.Items[i])=0 Then
       Begin
         RG_SqlServerType.ItemIndex:=I;
         Break;
       End;

     ReadDirsFromRegistry;
End;

procedure TFrm_WinTudaParams.FormCreate(Sender: TObject);
begin
     FillVcl;
end;

procedure TFrm_WinTudaParams.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if Frm_WinTudaParams<>Nil Then
        Frm_WinTudaParams:=Nil;
     Action:=caFree;
end;

Procedure TFrm_WinTudaParams.ReadDirsFromRegistry;
Var
   R :TRegistry;
Begin
     R:=TRegistry.Create;
     Try
        R.RootKey:=RegRootForAtm;
        R.OpenKey(RegKeyForAtm2000,True);
        if R.ValueExists(RegValueForBin) Then
           DE_Bin.Text:=R.ReadString(RegValueForBin);
        if R.ValueExists(RegValueForScriptsFiles) Then
           DE_Scripts.Text:=R.ReadString(RegValueForScriptsFiles);
        if R.ValueExists(RegValueForUsersDir) Then
           DE_Users.Text:=R.ReadString(RegValueForUsersDir);
        if R.ValueExists(RegValueForInstallDir) Then
           DE_InstallDir.Text:=R.ReadString(RegValueForInstallDir);
     Finally
            R.Free;
     End;
End;

Procedure TFrm_WinTudaParams.WriteDirsToRegistry;
Var
   R :TRegistry;
Begin
     R:=TRegistry.Create;
     Try
        R.RootKey:=RegRootForAtm;
        R.OpenKey(RegKeyForAtm2000,True);
           R.WriteString(RegValueForBin,DE_Bin.Text);
           R.WriteString(RegValueForScriptsFiles,DE_Scripts.Text);
           R.WriteString(RegValueForUsersDir,DE_Users.Text);
           R.WriteString(RegValueForInstallDir,DE_InstallDir.Text);
     Finally
            R.Free;
     End;
End;
end.

unit HlpRutin;

interface

uses
   DBTables, classes, SysUtils,Controls,DB,AtmComp,AtmLookupCombo,DbSrtGrd,
   HebDbGrid,AtmDbDateEdit,Dialogs,AtmListDlg,quickrpt,Printers ,QrPrnTr,
   Windows,ComCtrls,DBCtrls,DBGrids;
Type
   CharSet= Set of Char;

//-------------------- ������� ����� ������
function LeftToRight(InStr:shortstring):shortstring;
//������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                    FieldNum :LongInt): String;
//--------------- ������� ������ ����� ����� ���� ������
Function DelChars(TheString :ShortString;CharsToDel :CharSet):ShortString;
//---------------- �������� ������ ���� ���� ����� �����
procedure CreateNewTable(Sender: TComponent;
     DbNameIn,TblNameIn,DBNameOut,TblNameOut:string;
     TblTypeOut : TTableType );
                      {����� ����� �����}
Function CalcDateTashlum(FromDate :TDateTime;PayCond :ShortString) : TDateTime;

Function GetDataSetFromTControl(TheControl :TControl):TDataSet;
Function GetDataSourceFromTControl(TheControl :TControl):TDataSource;
Function GetDataDataFieldNameFromTControl(TheControl :TControl):ShortString;
Function GetLinkLabelTextFromTControl(TheControl :TControl):ShortString;
Procedure DuplicateOneRecord(TheTable :TTable;PostAfterDup :Boolean);
Function ChooseFaxPrinterName :String;
Function SetPrinterByName(Name :String;ThePrinter :TPrinter) :Boolean;
function GetProductVersion(FileName :String): String;
Function FindFirstControlInTabOrder(ParentControl :TWinControl):TWinControl;

//----------------------------------
function IsLeapYear(AYear: Integer): Boolean;
function DaysPerMonth(AYear, AMonth: Integer): Integer;
function DaysInPeriod(Date1, Date2: TDateTime): Longint;
function ValidDate(ADate: TDateTime): Boolean;
Procedure FillKodTavlaLookupList(TheDataset:TDataset;FieldsList :TStringList);
implementation

//-----------------------------------------------------------------------
//                          ������� ����� ������
function LeftToRight(InStr:shortstring):shortstring;
 var ix1:integer;
     HelpStr:string;
begin
  HelpStr:='';
  for ix1:= Length(InStr) downto 1 do
     HelpStr:=HelpStr+InStr[ix1];
  LeftToRight:=HelpStr;
end;
//-----------------------------------------------------------------------
//         ������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                         FieldNum:LongInt): String;
Var
   StrH :String;
   Ix1,FieldCount,FieldPos :LongInt;

Begin
    if FieldNum=1 Then
    Begin
        if TheString[1]=Separator Then
             Result:=''
        Else
        Begin
             StrH:=Copy(TheString,1,Pos(Separator,TheString)-1);
             if StrH='' Then StrH:=TheString;
             {EndOfString:=Copy(TheString,Pos(Separator,TheString)+1,Length(TheString)-Length(StrH)+1);}
             Result:=StrH;
        End;
    End
    Else {FieldNum>1}
    Begin
         FieldCount:=1;
         For Ix1:=1 To Length(TheString) Do
            if TheString[Ix1]=Separator Then
            Begin
                 FieldPos:=Ix1+1;
                 FieldCount:=FieldCount+1;
                 if FieldCount=FieldNum Then Break;
            End;
         if (FieldCount=FieldNum) And (FieldPos<=Length(TheString)) Then
         Begin
             StrH:=Copy(TheString,FieldPos,Length(TheString)-FieldPos+1);
             if Pos(Separator,StrH)>0 Then
                StrH:=Copy(StrH,1,Pos(Separator,StrH)-1);
             Result:=StrH;
         End
         Else
             Result:='';
    End;
End;
//-----------------------------------------------------------------------
//               ������� ������ ����� ����� ���� ������
Function DelChars(TheString :ShortString;CharsToDel :CharSet):ShortString;
Var
   I    :Integer;
   StrH :ShortString;
Begin
     StrH:='';
     For I:=1 To Length(TheString) Do
        if Not (TheString[I] in CharsToDel) Then
           StrH:=StrH+TheString[I];
     DelChars:=StrH;
End; {DelChars}
//-----------------------------------------------------------------------
//                �������� ������ ���� ���� ����� �����
procedure CreateNewTable(Sender: TComponent;
  DbNameIn,TblNameIn,DBNameOut,TblNameOut:string;
  TblTypeOut : TTableType );
var ix1:integer;
    MyTable,NewTable:TTable;
begin
  MyTable:=TTable.Create(Sender);
  NewTable:=TTable.Create(Sender);
  with MyTable do
  begin
    TableName:=TblNameIn;
    DataBaseName:=DbNameIn;
    IndexDefs.Update;
    FieldDefs.Update;
    NewTable.TableName:=TblNameOut;
    NewTable.TableType:=TblTypeOut;
    NewTable.DataBaseName:=DBNameOut;
    NewTable.IndexDefs.Assign(IndexDefs);
    NewTable.FieldDefs.Assign(FieldDefs);
    NewTable.CreateTable;
  end;
  MyTable.Free;
  NewTable.Free;
end;
//-----------------------------------------------------------------------
//                       ����� ����� ����� �� ����
Function CalcDateTashlum(FromDate :TDateTime;PayCond :ShortString) : TDateTime;
Var
   Day,Month,Year :Word;
   Res            :TDateTime;
   Str1           :ShortString;
   I              :LongInt;
   FoundNum       :Boolean;
   AddValue       :LongInt;
Begin
  DecodeDate(FromDate, Year, Month, Day);
//------------------- ����� �����
  if Pos('����',PayCond)> 0 Then
     begin
       Day := 1;
       if Month < 12 then Inc(Month)
          else begin
               Inc(Year);
               Month := 1;
          end;
       FromDate := EncodeDate(Year, Month, Day);
       FromDate := FromDate -1;
     end;
//------------------- ����� �����
  I:=1;
  FoundNum:=True;
  Str1:='';
  While (FoundNum) And (I<=Length(PayCond)) Do
    Begin
      if (PayCond[I]>='0') And (PayCond[I]<='9') Then
         Str1:=Str1+PayCond[I]
      Else
         if Str1<>''Then
            FoundNum:=False;
      Inc(I);
    End;
//-------------------------
  if Str1 > '' then
     Result:= FromDate + StrToInt(Str1)
  else
     Result:= FromDate;
End;

Function GetDataSetFromTControl(TheControl :TControl):TDataSet;
Begin
  if (TheControl is TDBEdit) Then
      Result:=((TheControl As TDBEdit).DataSource.DataSet)
  Else
  if (TheControl is TDBLookupComboBox) Then
      Result:=((TheControl As TDBLookupComboBox).DataSource.DataSet)
  Else
  if (TheControl is TDbSrtGrd) Then
      Result:=((TheControl As TDbSrtGrd).DataSource.DataSet)
  Else
  if (TheControl is TDbGrid) Then
      Result:=((TheControl As TDbGrid).DataSource.DataSet)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).DataSource.DataSet)
  Else
      Result:=Nil;
End;

Function GetDataSourceFromTControl(TheControl :TControl):TDataSource;
Begin
  if (TheControl is TDBEdit) Then
      Result:=((TheControl As TDBEdit).DataSource)
  Else
  if (TheControl is TDBLookupComboBox) Then
      Result:=((TheControl As TDBLookupComboBox).DataSource)
  Else
  if (TheControl is TDbSrtGrd) Then
      Result:=((TheControl As TDbSrtGrd).DataSource)
  Else
  if (TheControl is TDbGrid) Then
      Result:=((TheControl As TDbGrid).DataSource)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).DataSource)
  Else
      Result:=Nil;
End;


Function GetDataDataFieldNameFromTControl(TheControl :TControl):ShortString;
Begin
  if (TheControl is TAtmDBHEdit) Then
      Result:=((TheControl As TAtmDBHEdit).DataField)
  Else
  if (TheControl is TAtmDBLookupCombo) Then
      Result:=((TheControl As TAtmDBLookupCombo).DataField)
  Else
  if (TheControl is TDbSrtGrd) Then
      Result:=((TheControl As TDbSrtGrd).SelectedField.FieldName)
  Else
  if (TheControl is THebDbGrid) Then
      Result:=((TheControl As THebDbGrid).SelectedField.FieldName)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).DataField)
  Else
      Result:='';
End;


Function GetLinkLabelTextFromTControl(TheControl :TControl):ShortString;
Begin
  if (TheControl is TAtmDBHEdit) Then
      Result:=((TheControl As TAtmDBHEdit).LinkLabel.Caption)
  Else
  if (TheControl is TAtmDBLookupCombo) Then
      Result:=((TheControl As TAtmDBLookupCombo).LinkLabel.Caption)
  Else
  if (TheControl is TAtmDbDateEdit) Then
      Result:=((TheControl As TAtmDbDateEdit).LinkLabel.Caption)
  Else
      Result:='';
End;

Procedure DuplicateOneRecord(TheTable :TTable;PostAfterDup :Boolean);
Var
   TempTable :TTable;
   I :LOngInt;
Begin
     if TheTable.State<>dsBrowse Then
     Begin
          ShowMessage('������ ������� �� ����� - ��� ������ ������');
          Exit;
     End;

     TempTable:=TTable.Create(TheTable.Owner);
     TempTable.DatabaseName:=TheTable.DatabaseName;
     TempTable.TableName:=TheTable.TableName;
     TempTable.MasterSource:=TheTable.MasterSource;
     TempTable.MasterFields:=TheTable.MasterFields;
     TempTable.IndexName:=TheTable.IndexName;
     TempTable.Open;
     TempTable.GotoCurrent(TheTable);
     TheTable.Insert;
     For I:=0 To TempTable.FieldDefs.Count-1 Do
     Begin
          if (TempTable.Fields.Fields[I].FieldKind=fkData) And
              (TempTable.Fields.Fields[I].DataType<>ftAutoInc) And
              (CompareText(TempTable.Fields.Fields[I].FieldName,'Tnua')<>0) Then
          Begin
            Try
               TheTable.FieldByName(TempTable.Fields.Fields[I].FieldName).Value:=
                    TempTable.FieldByName(TempTable.Fields.Fields[I].FieldName).Value;
            Except On Exception Do; End;
          End;
     End;
     TempTable.Close;
     TempTable.Free;
     if PostAfterDup Then
        TheTable.Post;
End;

Function ChooseFaxPrinterName :String;
Var
   TheList :TAtmListDlg;
   P :TPrinter;
Begin
     P:=TPrinter.Create;
     TheList:=TAtmListDlg.Create(Nil);
     Try
        TheList.Caption:='��� ����� ���';
        TheList.Items.AddStrings(p.Printers);
        if TheList.Execute Then
           Result:=TheList.ReturnString
        Else
            Result:='';
     Finally
            P.Free;
            TheList.Free;
     End;
End;

Function SetPrinterByName(Name :String;ThePrinter :TPrinter) :Boolean;
Var
   I :LongInt;
Begin
     Result:=False;
     For I:=0 To ThePrinter.Printers.Count-1 Do
         if CompareText(Name,ThePrinter.Printers[I])=0 Then
         Begin
              ThePrinter.PrinterIndex:=I;
              Result:=True;
              Break;
         End;
End;

function GetProductVersion(FileName :String): String;
var
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
  V1,V2,V :DWord;
  StrH : String;
begin
  begin
    if FileName='' Then FileName:=ParamStr(0);
    InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
    if InfoSize <> 0 then
    begin
      GetMem(VerBuf, InfoSize);
      try
        if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
          if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
          Begin
                V1 := FI.dwProductVersionMS;
                V2 := Fi.dwProductVersionLS;

                V:=V1 Div $FFFF;
                StrH:=IntToStr(V);

                V:=0;
                Move(V1,V,2);
                StrH:=StrH+'.'+IntToStr(V);


                V:=V2 Div $FFFF;
                StrH:=StrH+'.'+IntToStr(V);
                V:=0;
                Move(V2,V,2);
                StrH:=StrH+' (Build '+IntToStr(V)+')';;
          End;
      finally
        FreeMem(VerBuf);
      end;
    end;
  end;
  Result := StrH;
end;
//----------------------------------------------------------------------
function IsLeapYear(AYear: Integer): Boolean;
begin
  Result := (AYear mod 4 = 0) and ((AYear mod 100 <> 0) or (AYear mod 400 = 0));
end;
//-----------------------------------------------------------------------
function DaysPerMonth(AYear, AMonth: Integer): Integer;
const
  DaysInMonth: array[1..12] of Integer =
    (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := DaysInMonth[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then Inc(Result); { leap-year Feb is special }
end;
//-----------------------------------------------------------------------
function IsValidDate(Y, M, D: Word): Boolean;
begin
  Result := (Y >= 1) and (Y <= 9999) and (M >= 1) and (M <= 12) and
    (D >= 1) and (D <= DaysPerMonth(Y, M));
end;

function ValidDate(ADate: TDateTime): Boolean;
var
  Year, Month, Day: Word;
begin
  try
    DecodeDate(ADate, Year, Month, Day);
    Result := IsValidDate(Year, Month, Day);
  except
    Result := False;
  end;
end;

function DaysInPeriod(Date1, Date2: TDateTime): Longint;
begin
  if ValidDate(Date1) and ValidDate(Date2) then
    Result := Abs(Trunc(Date2) - Trunc(Date1)) + 1
  else Result := 0;
end;

Function FindFirstControlInTabOrder(ParentControl :TWinControl):TWinControl;
Var
   T :TWinControl;
   Li :TList;
begin
  Try
     T:=Nil;
     Li:=TList.Create;
     ParentControl.GetTaborderList(Li);
     if Li.Count>0 Then
     Repeat
         T:=TWinControl(Li.Items[0]);
         Li.Free;
         Li:=TList.Create;
         T.GetTaborderList(Li);
     Until Li.Count<=0;
  Finally
         Li.Free;
         Result:=T;
  End;
End;

Procedure FillKodTavlaLookupList(TheDataset:TDataset;FieldsList :TStringList);
Var
   TheField :TField;
   StrH     :String;
   Ind      :LongInt;
Begin
     For Ind:=0 To FieldsList.Count-1 Do
     Begin
          TheField:=TField(FieldsList.Objects[Ind]);
          TheField.LookupList.Clear;
     End;
     TheDataSet.First;
     While Not TheDataSet.Eof Do
     Begin
          StrH:=TheDataSet.FieldByName('Sug_Tavla').AsString;
          For Ind:=0 To FieldsList.Count-1 Do
          if CompareText(FieldsList[Ind],StrH)=0 Then
          Begin
               TheField:=TField(FieldsList.Objects[Ind]);
               TheField.LookupList.Add(TheDataSet.FieldByName('Kod_Tavla').Value,
                                       TheDataSet.FieldByName('Teur_Tavla').Value);
          End;
          TheDataSet.Next;
     End;
End;

end.

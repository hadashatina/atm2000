{ 16/03/99 11:23:05 > [Aviran on AVIRAN] check in: (0.0)  / None }
unit DMWinTuda;

interface

uses
  SysUtils, Windows, Classes, Graphics, Controls,DBCtrls,AtmComp,
  Forms, Dialogs, DB, DBTables, AtmAdvTable, TProgress, AppEvent, RxQuery,
  DBFilter, AtmRxQuery,ExtCtrls;

type
  TDM_WinTuda = class(TDataModule)
    DS_Azmn: TDataSource;
    Tbl_Azmn: TAtmRxQuery;
    Tbl_Shib: TAtmRxQuery;
    DS_Shib: TDataSource;
    DB_WinTuda: TDatabase;
    Tbl_Rehev: TAtmRxQuery;
    Tbl_Nehag: TAtmRxQuery;
    Tbl_ShibShibAzmnNo: TIntegerField;
    Tbl_ShibShibAzmnDate: TDateTimeField;
    Tbl_ShibShibKind: TIntegerField;
    Tbl_ShibShibNo: TAutoIncField;
    Tbl_ShibShibBeginTime: TDateTimeField;
    Tbl_ShibShibEndTime: TDateTimeField;
    Tbl_ShibHovalaKind: TIntegerField;
    Tbl_ShibPriceKind: TIntegerField;
    Tbl_ShibShibTotalKm: TIntegerField;
    Tbl_ShibLakNo1: TIntegerField;
    Tbl_ShibLakNo2: TIntegerField;
    Tbl_ShibLakNo3: TIntegerField;
    Tbl_ShibPriceIsGlobal: TIntegerField;
    Tbl_ShibMaslulCode1: TIntegerField;
    Tbl_ShibMaslul1: TStringField;
    Tbl_ShibYeMida1: TIntegerField;
    Tbl_ShibYeMida2: TIntegerField;
    Tbl_ShibYeMida3: TIntegerField;
    Tbl_ShibYeMida4: TIntegerField;
    Tbl_ShibPrice1: TBCDField;
    Tbl_ShibPrice2: TBCDField;
    Tbl_ShibPrice3: TBCDField;
    Tbl_ShibPrice4: TBCDField;
    Tbl_ShibShibRem2: TStringField;
    Tbl_ShibShibRem3: TStringField;
    Tbl_ShibShibRem4: TStringField;
    Tbl_ShibDriverNo1: TIntegerField;
    Tbl_ShibDriverNo2: TIntegerField;
    Tbl_ShibDriverName: TStringField;
    Tbl_ShibCarNo: TIntegerField;
    Tbl_ShibCarNum: TStringField;
    Tbl_ShibLakCodeBizua: TIntegerField;
    Tbl_ShibLakHesbonit: TIntegerField;
    Tbl_ShibLakHesbonitDate: TDateTimeField;
    Tbl_ShibDrishNo: TIntegerField;
    Tbl_ShibDrishDate: TDateTimeField;
    Tbl_ShibDriverCodeBizua: TIntegerField;
    Tbl_ShibDriverZikuiNo: TIntegerField;
    Tbl_ShibDriverZikuiDate: TDateTimeField;
    Tbl_ShibCarCodeBizua: TIntegerField;
    Tbl_ShibCarZikuiNo: TIntegerField;
    Tbl_ShibCarZikuiDate: TDateTimeField;
    Tbl_ShibNefh: TBCDField;
    Tbl_ShibTrvlNo: TIntegerField;
    Tbl_ShibStarMetanNo: TIntegerField;
    Tbl_ShibGpsStatus: TIntegerField;
    Tbl_ShibGpsNo: TIntegerField;
    Tbl_ShibSugMetan: TIntegerField;
    Tbl_ShibShibTuda: TIntegerField;
    Tbl_ShibShibCarKind: TIntegerField;
    Tbl_ShibDriverMsg: TSmallintField;
    Tbl_ShibAzmnOk: TSmallintField;
    Tbl_ShibTnuaOk: TSmallintField;
    Tbl_ShibHasbonitOk: TSmallintField;
    Tbl_ShibFirstSpido: TIntegerField;
    Tbl_ShibLastSpido: TIntegerField;
    Tbl_ShibTudaKind: TIntegerField;
    Tbl_ShibNegrrNo: TIntegerField;
    Tbl_ShibUpdateRecordDate: TDateTimeField;
    Tbl_ShibMaklidName: TStringField;
    Tbl_ShibShibAsmcta: TIntegerField;
    Tbl_ShibShibRem1: TMemoField;
    TBL_KODTAVLA: TAtmRxQuery;
    Tbl_ShibYeMida2Look: TStringField;
    TBL_KODTAVLASug_Tavla: TIntegerField;
    TBL_KODTAVLAKod_Tavla: TIntegerField;
    TBL_KODTAVLATeur_tavla: TStringField;
    Tbl_ShibYeMida3Look: TStringField;
    Tbl_ShibLookShemMahzeva: TStringField;
    Tbl_ShibYeMida1Look: TStringField;
    Tbl_Maslul: TAtmRxQuery;
    Tbl_ShibYeMida4Look: TStringField;
    Tbl_Lako: TAtmRxQuery;
    Tbl_ShibShemNehag2Look: TStringField;
    Tbl_Mehiron: TAtmTable;
    Tbl_ShibPriceQuntity1: TBCDField;
    Tbl_ShibPriceQuntity2: TBCDField;
    Tbl_ShibPriceQuntity3: TBCDField;
    Tbl_ShibPriceQuntity4: TBCDField;
    Tbl_ShibQuntMusa: TBCDField;
    Tbl_ShibTotalHour: TBCDField;
    Tbl_ShibTotalPointsInWay: TIntegerField;
    Tbl_ShibShibFromPalce: TStringField;
    Tbl_ShibShibToPalce: TStringField;
    Tbl_ShibQuntity1: TBCDField;
    Tbl_ShibQuntity2: TBCDField;
    Tbl_ShibQuntity3: TBCDField;
    Tbl_ShibQuntity4: TBCDField;
    Tbl_ShibLookMerakezLak: TStringField;
    Tbl_ShibShibTotalTime: TDateTimeField;
    Tbl_ShibXBegin: TIntegerField;
    Tbl_ShibYBegin: TIntegerField;
    Tbl_ShibNodeNumBBegin: TIntegerField;
    Tbl_ShibNodeSugSystemBegin: TIntegerField;
    Tbl_ShibNodeKodCityNameBegin: TIntegerField;
    Tbl_ShibXEnd: TIntegerField;
    Tbl_ShibYEnd: TIntegerField;
    Tbl_ShibNodeNumBEnd: TIntegerField;
    Tbl_ShibNodeSugSystemEnd: TIntegerField;
    Tbl_ShibNodeKodCityNameEnd: TIntegerField;
    Tbl_ShibAmil: TStringField;
    Tbl_ShibGateTuda: TIntegerField;
    Tbl_ShibTik: TIntegerField;
    Tbl_ShibCont1: TStringField;
    Tbl_ShibCont2: TStringField;
    Tbl_ShibBoxes: TIntegerField;
    Tbl_ShibDamaged: TSmallintField;
    Tbl_ShibHoze: TIntegerField;
    Tbl_ShibWaitTime: TDateTimeField;
    Tbl_ShibUnLoadTime: TDateTimeField;
    Tbl_ShibTimeWinFrom: TDateTimeField;
    Tbl_ShibTimeWinTo: TDateTimeField;
    Tbl_ShibShemLakoach1Look: TStringField;
    Tbl_ShibMrakz: TStringField;
    UpdateSQL_TabSheetBuild: TUpdateSQL;
    DS_TabSheetBuild: TDataSource;
    RxDBFilter_TabSheetBuild: TRxDBFilter;
    RxQuery_TabSheetBuild: TRxQuery;
    Tbl_ShibMishkal: TBCDField;
    Tbl_ShibCodeMahzeva: TIntegerField;
    Tbl_ShibYehusYear: TIntegerField;
    Tbl_ShibYehusMonth: TIntegerField;
    Tbl_ShibTeurMitanLook1: TStringField;
    Tbl_AzmnLookAzmnType: TStringField;
    UpdateSQL_Azmn: TUpdateSQL;
    Tbl_AzmnAzmnNo: TIntegerField;
    Tbl_AzmnAzmnDate: TDateTimeField;
    Tbl_AzmnInvLakNo: TIntegerField;
    Tbl_AzmnGetLakNo: TIntegerField;
    Tbl_AzmnAsmcta: TIntegerField;
    Tbl_AzmnTuda: TIntegerField;
    Tbl_AzmnFromDate: TDateTimeField;
    Tbl_AzmnToDate: TDateTimeField;
    Tbl_AzmnFromTime: TDateTimeField;
    Tbl_AzmnToTime: TDateTimeField;
    Tbl_AzmnAzmnType: TIntegerField;
    Tbl_AzmnRem2: TStringField;
    Tbl_AzmnRem3: TStringField;
    Tbl_AzmnRem4: TStringField;
    Tbl_AzmnTotalPrice1: TBCDField;
    Tbl_AzmnTotalPrice2: TBCDField;
    Tbl_AzmnTotalPrice3: TBCDField;
    Tbl_AzmnTotalKm: TIntegerField;
    Tbl_AzmnSunday: TSmallintField;
    Tbl_AzmnMonday: TSmallintField;
    Tbl_AzmnTuesday: TSmallintField;
    Tbl_AzmnWednesday: TSmallintField;
    Tbl_AzmnThursday: TSmallintField;
    Tbl_AzmnFriday: TSmallintField;
    Tbl_AzmnSaturday: TSmallintField;
    Tbl_AzmnDeleteDate: TDateTimeField;
    Tbl_AzmnOldAzmnType: TIntegerField;
    Tbl_AzmnAzmnCarKind: TIntegerField;
    Tbl_AzmnAzmnSugMetan: TIntegerField;
    Tbl_AzmnMkbleName: TStringField;
    Tbl_AzmnAzmnQuntity: TBCDField;
    Tbl_AzmnQuntity1: TBCDField;
    Tbl_AzmnInvName: TStringField;
    Tbl_AzmnDirshNo: TIntegerField;
    Tbl_AzmnFirstAzmnNo: TIntegerField;
    Tbl_AzmnAzmnMaslulCode1: TIntegerField;
    Tbl_AzmnAzmnMaslulCode2: TIntegerField;
    Tbl_AzmnAzmnMaslulCode3: TIntegerField;
    Tbl_AzmnAzmnMaslulCode4: TIntegerField;
    Tbl_AzmnAzmnMaslul1: TStringField;
    Tbl_AzmnAzmnMaslul2: TStringField;
    Tbl_AzmnAzmnMaslul3: TStringField;
    Tbl_AzmnAzmnMaslul4: TStringField;
    Tbl_AzmnFromPlace: TStringField;
    Tbl_AzmnToPlace: TStringField;
    Tbl_AzmnPratim: TStringField;
    Tbl_AzmnAzmnCarNo: TIntegerField;
    Tbl_AzmnAzmnDriverNo: TIntegerField;
    Tbl_AzmnAzmnCarNum: TStringField;
    Tbl_AzmnCodeMhiron: TIntegerField;
    Tbl_AzmnMitzar: TIntegerField;
    Tbl_AzmnSochen: TStringField;
    Tbl_AzmnRshimon: TIntegerField;
    Tbl_AzmnOnia: TStringField;
    Tbl_AzmnAflagaNo: TStringField;
    Tbl_AzmnAflagaPlace: TStringField;
    Tbl_AzmnNefh: TBCDField;
    Tbl_AzmnPutPlace: TStringField;
    Tbl_AzmnFindPlace: TStringField;
    Tbl_AzmnDamaged: TSmallintField;
    Tbl_AzmnAddr: TStringField;
    Tbl_AzmnAddrNo: TIntegerField;
    Tbl_AzmnCity: TStringField;
    Tbl_AzmnPhone: TStringField;
    Tbl_AzmnTotalTime: TDateTimeField;
    Tbl_AzmnMishkal: TBCDField;
    Tbl_AzmnLakName: TStringField;
    Tbl_AzmnTotalZikuim: TBCDField;
    Tbl_AzmnCALENDARKIND: TIntegerField;
    Tbl_AzmnXBegin: TIntegerField;
    Tbl_AzmnYBegin: TIntegerField;
    Tbl_AzmnNodeNumBBegin: TIntegerField;
    Tbl_AzmnNodeSugSystemBegin: TIntegerField;
    Tbl_AzmnNodeKodCityNameBegin: TIntegerField;
    Tbl_AzmnXEnd: TIntegerField;
    Tbl_AzmnYEnd: TIntegerField;
    Tbl_AzmnNodeNumBEnd: TIntegerField;
    Tbl_AzmnNodeSugSystemEnd: TIntegerField;
    Tbl_AzmnNodeKodCityNameEnd: TIntegerField;
    Tbl_AzmnHovalaKind: TIntegerField;
    Tbl_AzmnMaklidName: TStringField;
    Tbl_AzmnUpdateDate: TDateTimeField;
    Tbl_AzmnCont1: TStringField;
    Tbl_AzmnCont2: TStringField;
    RxQuery_Temp: TRxQuery;
    Tbl_AzmnTotalWithMam: TBCDField;
    Tbl_AzmnTotalAfterDiscount: TBCDField;
    UpdateSQL_Shib: TUpdateSQL;
    Tbl_AzmnLookPhoneLak: TStringField;
    RxQuery_GetShibNo: TRxQuery;
    Tbl_ShibLookHovalaKind: TStringField;
    UpdateSQL_TSB2: TUpdateSQL;
    DS_TSB2: TDataSource;
    RxDBFilter_TSB2: TRxDBFilter;
    RxQuery_TSB2: TRxQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DM_WinTudaDestroy(Sender: TObject);
    procedure Tbl_ShibDriverNo1Validate(Sender: TField);
    procedure Tbl_ShibBeforePost(DataSet: TDataSet);
    procedure Tbl_AzmnAfterInsert(DataSet: TDataSet);
    procedure Tbl_ShibMaslulCode1Validate(Sender: TField);
    procedure Tbl_ShibYeMida1Validate(Sender: TField);
    procedure Tbl_ShibQuntity1Validate(Sender: TField);
    procedure Tbl_AzmnAzmnNoValidate(Sender: TField);
    procedure Tbl_AzmnFromDateValidate(Sender: TField);
    procedure Tbl_AzmnToDateValidate(Sender: TField);
    procedure Tbl_AzmnBeforeCancel(DataSet: TDataSet);
    procedure Tbl_ShibAfterPost(DataSet: TDataSet);
    procedure Tbl_ShibAfterInsert(DataSet: TDataSet);
    procedure Tbl_ShibPrice1Validate(Sender: TField);
    procedure Tbl_ShibQuntity2Validate(Sender: TField);
    procedure Tbl_ShibYeMida2Validate(Sender: TField);
    procedure DS_AzmnDataChange(Sender: TObject; Field: TField);
    procedure DS_ShibStateChange(Sender: TObject);
    procedure Tbl_ShibShibBeginTimeGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure Tbl_ShibShibBeginTimeSetText(Sender: TField;
      const Text: String);
    procedure Tbl_ShibFirstSpidoValidate(Sender: TField);
    procedure Tbl_ShibCarNumValidate(Sender: TField);
    procedure RxQuery_TabSheetBuildBeforeClose(DataSet: TDataSet);
    procedure Tbl_ShibShibBeginTimeValidate(Sender: TField);
    procedure DB_WinTudaLogin(Database: TDatabase; LoginParams: TStrings);
    procedure Tbl_AzmnAfterPost(DataSet: TDataSet);
    procedure Tbl_AzmnAfterScroll(DataSet: TDataSet);
    procedure Tbl_ShibValidateLookupField(Sender: TObject; AField: TField;
      var IsValid: Boolean);
    procedure Tbl_AzmnBeforePost(DataSet: TDataSet);
    procedure DS_AzmnStateChange(Sender: TObject);
    procedure Tbl_ShibShibAzmnDateValidate(Sender: TField);
    procedure Tbl_ShibBeforeDelete(DataSet: TDataSet);
    procedure Tbl_AzmnAzmnTypeValidate(Sender: TField);
    procedure Tbl_ShibDriverNo1SetText(Sender: TField; const Text: String);
    procedure Tbl_AzmnAzmnNoChange(Sender: TField);
    procedure Tbl_AzmnInvLakNoChange(Sender: TField);
    procedure Tbl_ShibPrice2Validate(Sender: TField);
    procedure Tbl_ShibUpdateError(DataSet: TDataSet; E: EDatabaseError;
      UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure Tbl_AzmnValidateLookupField(Sender: TObject; AField: TField;
      var IsValid: Boolean);
    procedure Tbl_ShibBeforeEdit(DataSet: TDataSet);
    procedure Tbl_ShibLakNo1Validate(Sender: TField);
    procedure DB_WinTudaAfterConnect(Sender: TObject);
    procedure Tbl_ShibShibAzmnDateSetText(Sender: TField;
      const Text: String);
  private
    { private declarations }
    CurAzmnNo:String;
  public
    { public declarations }
    Procedure PutDefaultsOnShib;
    Procedure FindNextHazmanaNum;
    Procedure BuildLookupList;
    Procedure CalcShibTotalPrices;
    procedure PutMerakezDataInField(MerakezName:String;TheField :TField);
    Procedure CheckForRange;
    procedure FillAzmnDays;
    procedure Tbl_AzmnCalcFieldsMaz(DataSet: TDataSet);
    Procedure DoShibDetails(AzmnNo :String);
    Procedure RefreshMainQuerys;
    Function  CheckRetzefTeuda:Boolean;
    Function  GetCurShibNo :LongInt;
    Procedure SetReadOnlyFields;
  end;

var
  DM_WinTuda: TDM_WinTuda;

implementation
Uses {F_GlblWinTuda,} Frm_WinTuda,AtmRutin,AtmConst,F_Progress,
  F_GlblWinTuda;
{$R *.DFM}

procedure TDM_WinTuda.DataModuleCreate(Sender: TObject);
var
   i:longint;
   StrH :String;
begin
{  Try
     StrH:=GetTempDir;
     if StrH[Length(StrH)]='\' Then
        StrH:=Copy(StrH,1,Length(StrH)-1);
     Session.PrivateDir:=StrH;
  Except
        //Ignore
  End;
 }
//     DB_WinTuda.LoginPrompt:=WinTudaDBUserName='';
     DB_WinTuda.Connected:=False;
     DB_WinTuda.LoginPrompt:=True;
     if CurAliasName<>'' Then
        DB_WinTuda.AliasName:=CurAliasName;
     Tbl_KodTavla.Open;
     Frm_Progress:=TFrm_Progress.Create(Nil);
     Try
         if WinTudaSqlMode=0 Then
           Tbl_Shib.MacroByName('MasterField').AsString:=''
         Else
           if WinTudaSqlMode=1 Then
           Begin
             Tbl_Shib.MacroByName('MasterField').AsString:='ShibAzmnNo=1 And ';
             Tbl_Azmn.MacroByName(Tbl_Azmn.MAcroForWhere).AsString:='AzmnNo = 1';
             Tbl_Azmn.GetOnlyOneRecord:=True;
           End;

         Frm_Progress.AtmGauge1.MaxValue:=0;
         For I:=0 To ComponentCount-1 Do
             if (Components[I] is TDataSet) And (Components[I].Tag=1) Then
                Frm_Progress.AtmGauge1.MaxValue:=Frm_Progress.AtmGauge1.MaxValue+1;
         Frm_Progress.Caption:='���� ������';
         Frm_Progress.Show;

         BuildLookupList;

         For I:=0 To ComponentCount-1 Do
         Begin
              if (Components[I] is TDataSet) And (Components[I].Tag=1) Then
              Begin
                   Frm_Progress.Label1.Caption:=Components[I].Name;
                   Frm_Progress.AtmGauge1.AddProgress(1);
                   Application.ProcessMessages;
                   if Not (Components[I] As TDataSet).Active Then
                   Begin
                      (Components[I] As TDataSet).Open;
                   End;
              End;
         End;
         CheckForRange;
     Finally
            Frm_Progress.Free;
            Frm_Progress:=Nil;
     End;
end;

Procedure TDM_WinTuda.CheckForRange;
Begin
     RangeAzmanFrom:=1;
     RangeAzmnTo:=99999999;
     With Frm_GlblWinTuda Do
     Begin
          Qry_Generic.SQL.Clear;
          Qry_Generic.Active:=False;
          Qry_Generic.SQL.Add('Select * From Range');
          Qry_Generic.SQL.Add('Where UserName='''+WinTudaCurUserName+'''');
          Qry_Generic.SQL.Add('And RangeType='''+rtAtmAzmn+'''');
          Qry_Generic.Active:=True;
          if Not Qry_Generic.Eof Then
          Begin
              RangeAzmanFrom:=Qry_Generic.FieldByName('FromValue').AsInteger;
              if Qry_Generic.FieldByName('ToValue').AsInteger>0 Then
                 RangeAzmnTo:=Qry_Generic.FieldByName('ToValue').AsInteger;
          End;
          Qry_Generic.Active:=False;
     End;
End;

procedure TDM_WinTuda.DM_WinTudaDestroy(Sender: TObject);
var
   i:longint;
   Frm :TForm;
begin
  Frm:=TForm.Create(Nil);
  Try
     Frm.FormStyle:=fsStayOnTop;
     Frm.BorderStyle:=bsNone;
     Frm.Width:=250;
     Frm.Height:=60;
     Frm.Position:=poScreenCenter;
     With TPanel.Create(Frm) Do
     Begin
        Parent:=Frm;
        Align:=alClient;
        Caption:='���� ������...�� ����';
     End;
     Frm.Show;
     Application.ProcessMessages;
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
     DB_WinTuda.Connected:=False;
  Finally
    Frm.Free;
  End;
end;

procedure TDM_WinTuda.Tbl_ShibDriverNo1Validate(Sender: TField);
begin
     if (Tbl_Shib.State<>dsBrowse) And (Sender.AsString<>'') Then
     Begin
          Tbl_Nehag.IndexFieldNames:='Kod_Nehag';
          if Tbl_Nehag.FindKey([Sender.Value]) Then
          Begin
             Tbl_Shib.FieldByName('DriverName').AsString:=Tbl_Nehag.FieldByName('Shem_Nehag').AsString;
             if (Tbl_ShibCarNum.IsNull) Or (WinTudaAlwaysBringRehevFromNehag) Then
                Tbl_ShibCarNum.AsString:=Trim(Tbl_Nehag.FieldByName('Mis_Rishui').AsString);
          End
          Else
              Begin
                   ShowMessage('��� �� ���� ������');
                   Abort;
              End;
     End;

end;

procedure TDM_WinTuda.Tbl_ShibBeforePost(DataSet: TDataSet);
Var
  S :TStringList;
begin
{     if (Tbl_Shib.State=dsEdit) And (Tbl_ShibLakCodeBizua.AsInteger<>0) Then
     Begin
          MessageDlg('��� ������ ����� ����� ����� ���� �������',mtInformation,[mbOk],0);
          Abort;
          Exit;
     End;
}
     if Tbl_Azmn.FieldByName('Asmcta').AsInteger<>Tbl_Shib.FieldByName('ShibAsmcta').AsInteger Then
        Case MessageDlg('������ ������ ���� ������� ������ ��� ����� ������ �����',mtConfirmation,[mbYes,mbNo,mbCancel],0) Of
          mrYes: Tbl_Shib.FieldByName('ShibAsmcta').AsInteger:=Tbl_Azmn.FieldByName('Asmcta').AsInteger;
          mrCancel :Begin
                      Abort;
                      Exit;
                    End;
        End; //Case

     if Not CheckRetzefTeuda Then
     Begin
       MessageDlg('�����/������ ���� ���� ',mtInformation,[mbOk],0);
       Abort;
       Exit;
     End;

     PutDefaultsOnShib;
     if Tbl_Shib.State = dsInsert Then
       PutLookupFieldsAsDefaultFromIni(WinTudaCurIniFileName,SectionForShibDefault,Tbl_Shib,Tbl_Shib,RxQuery_Temp);

     TnuaWasInInsert:=DataSet.State=dsInsert;
     if InNewHazmana And TnuaWasInInsert Then
        WinTudaTotalForHazmana:=WinTudaTotalForHazmana+Tbl_ShibPriceQuntity1.AsFloat
     Else
         InNewHazmana:=False;

     if (Tbl_Shib.State=dsInsert) And (Tbl_Shib.Database.IsSQLBased) Then
     Begin
       BuildSqlScriptForInsert(TStringList(UpdateSQL_Shib.InsertSQL),Tbl_Shib,WinTudaAtmShibFileName);
       UpdateSQL_Shib.InsertSQL.SaveToFile('c:\InsertShib.sql');
     End;
end;

procedure TDM_WinTuda.Tbl_AzmnAfterInsert(DataSet: TDataSet);
begin
  FindFirstControlInTabOrder(FrmWinTuda).SetFocus;
  Tbl_Shib.Append;
  Tbl_Azmn.SavePressed:=False;
  WinTudaTotalForHazmana:=0;
  Case WinTudaShibKind of
    skHiuvNehag :Tbl_Azmn.FieldByName('AzmnType').AsInteger:=atHiuvNehag;
    skZikuyLak  :Tbl_Azmn.FieldByName('AzmnType').AsInteger:=atZikuyLak;
  End;
end;

procedure TDM_WinTuda.Tbl_ShibMaslulCode1Validate(Sender: TField);
begin
     if (Tbl_Shib.State<>dsBrowse) And (Sender.AsString<>'') Then
     Begin
//          Tbl_Maslul.IndexName:='';
          Tbl_Maslul.IndexFieldNames:=IndFields_Maslul_PrimaryKey;
          if Tbl_Maslul.FindKey([Sender.Value]) Then
          Begin
             Tbl_Shib.FieldByName('Maslul1').AsString:=Tbl_Maslul.FieldByName('Name').AsString;
             if {(Tbl_ShibShibTotalKm.IsNull) And} (Not Tbl_Maslul.FieldByName('Distance_Maslul').IsNull) Then
               if (Tbl_ShibShibTotalKm.IsNull) Or (MessageDlg('��� ����� �"� ������ �������',mtConfirmation,[mbYes,mbNo],0)=mrYes) Then
                Tbl_ShibShibTotalKm.AsInteger:=Tbl_Maslul.FieldByName('Distance_Maslul').AsInteger;
          End
          Else
              Begin
                   ShowMessage('����� �� ����');
                   Abort;
              End;

     End;
end;

procedure TDM_WinTuda.Tbl_ShibYeMida1Validate(Sender: TField);
Var
   I :LongInt;
begin
     For I:=2 To 4 Do
//     if Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).AsString='' Then
          Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).Value:=Tbl_Shib.FieldByName('YeMida1').Value;
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_ShibQuntity1Validate(Sender: TField);
Var
   I :LongInt;
begin
  if WinTudaSugSystem=ssMaz Then
    Tbl_Shib.FieldByName('Quntity2').AsFloat:=0
  Else
    For I:=2 To 4 Do
//         if Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsString='' Then
      Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).Value:=Tbl_Shib.FieldByName('Quntity1').Value;
  CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_AzmnAzmnNoValidate(Sender: TField);
Var
   StrH :String;
begin
{     if (Sender.DataSet.State=dsEdit) And (Sender.AsString<>'') Then
     Begin
          StrH:=Sender.AsString;
          Sender.DataSet.Cancel;
          TTable(Sender.DataSet).IndexFieldNames:=IndFields_Azmn_PrimKey;
          TTable(Sender.DataSet).FindNearest([StrH]);
     End;
}
     if (Sender.DataSet.State=dsInsert) And (Sender.AsString<>'')Then
     Begin
          With Frm_GlblWinTuda.Qry_Generic Do
          Begin
               Active:=False;
               Sql.Clear;
               Sql.Add('Select Count(*) CR');
               Sql.Add('From '+WinTudaAtmAzmnFileName);
               Sql.Add('Where AzmnNo='+Sender.AsString);
               Active:=True;
               if (FieldByName('CR').AsInteger>0) Then
               Begin
                    Active:=False;
                    ShowMessage('���� ����� ����');
                    Abort;
               End;
               Active:=False;
          End;
     End;
end;

procedure TDM_WinTuda.Tbl_AzmnFromDateValidate(Sender: TField);
begin
     if (Sender.Value>Tbl_Azmn.FieldByName('ToDate').Value) Or (Tbl_Azmn.FieldByName('ToDate').IsNull) Then
     Begin
          Tbl_Azmn.FieldByName('ToDate').Value:=Sender.Value;
     End;
     if Tbl_Shib.State<>dsBrowse Then
        if Tbl_Azmn.FieldByName('ToDate').AsString='' Then
           Tbl_Azmn.FieldByName('ToDate').Value:=Sender.Value;
     FillAzmnDays;
end;

procedure TDM_WinTuda.Tbl_AzmnToDateValidate(Sender: TField);
begin
     if Sender.Value<Tbl_Azmn.FieldByName('FromDate').Value Then
     Begin
{          ShowMessage('����� ����� ��� ������ ����');
          Abort;
          Exit;
}         Tbl_Azmn.FieldByName('FromDate').Value:=Sender.Value;
     End;
     if Tbl_Shib.State<>dsBrowse Then
        if Tbl_Shib.FieldByName('ShibAzmnDate').AsString='' Then
           Tbl_Shib.FieldByName('ShibAzmnDate').Value:=Tbl_Azmn.FieldByName('FromDate').Value;
     FillAzmnDays;
end;

procedure TDM_WinTuda.Tbl_AzmnBeforeCancel(DataSet: TDataSet);
begin
     Tbl_Shib.Cancel;
end;

procedure TDM_WinTuda.Tbl_ShibAfterPost(DataSet: TDataSet);
Var
   AzmnLastDate :TDateTime;
   MyControl:TWinControl;
   StrH:String;
begin
     if Tbl_Azmn.State<>dsBrowse Then
     Begin
          Tbl_Azmn.SavePressed:=True;
          Tbl_Azmn.Post;
     End;

     if WinTudaAutoCalcSumTuda Then
     Begin
          if InNewHazmana Then
          Begin
               StrH:=Format('%f ��"� �����',[WinTudaTotalForHazmana]);
               FrmWinTuda.StatusBar1.Panels[PnlNum_TotalHazmana].Text:=StrH;
          End
          Else
              FrmWinTuda.Action_CalcTotalTeudaAzmnExecute(FrmWinTuda.Action_CalcTotalTeudaAzmn);
     End;
{     if TnuaWasInInsert Then
     Begin
          AzmnLastDate:=Tbl_Azmn.FieldByName('ToDate').Value;
          if Tbl_Shib.FieldByName('ShibAzmnDate').Value<AzmnLastDate Then
          Begin
               Tbl_Shib.Insert;
               Tbl_Shib.FieldByName('ShibAzmnDate').Value:=Tbl_Azmn.FieldByName('FromDate').Value+1;
               MyControl:=FindFirstControlInTabOrder(FrmWinTuda.PageControl_Tnua);
               if MyControl<>Nil Then
                  MyControl.SetFocus;
          End;
     End;
}
  Try
    Tbl_Shib.Database.ApplyUpdates([Tbl_Shib As TdbDataset]);
  Except On e:Exception Do
    ShowMessage(e.message);
  End;
end;

Function TDM_WinTuda.GetCurShibNo :LongInt;
Var
  IntH :LongInt;
Begin
  IntH:=Tbl_Shib.FieldByName('ShibNo').AsInteger;
  if IntH=0 Then
  Begin
    RxQuery_GetShibNo.ParamByName('PAzmnNo').AsInteger:=Tbl_Shib.FieldByName('ShibAzmnNo').AsInteger;
    RxQuery_GetShibNo.ParamByName('PUpdateDate').AsDateTime:=Tbl_Shib.FieldByName('UpdateRecordDate').AsDateTime;
    RxQuery_GetShibNo.Open;
    IntH:=RxQuery_GetShibNo.FieldByName('ShibNo').AsInteger;
    RxQuery_GetShibNo.Close;
  End;
  Result:=IntH;
End;

Procedure TDM_WinTuda.PutDefaultsOnShib;
Var
  I :longInt;
  EV:TFieldNotifyEvent;
  C :Char;
Begin
  With Tbl_Shib Do
  Begin
    For I:=0 To FieldCount-1 Do
      Fields[i].ReadOnly:=False;
     For I:=1 To 4 Do
       if Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).IsNull Then
       Begin
         EV:=Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).OnValidate;
         Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).OnValidate:=Nil;
         Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).AsInteger:=1;
         Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).OnValidate:=EV;
       End;
    For C:='1' To '4' Do
    if FieldByName('Quntity'+C).IsNull Then
    Begin
      EV:=FieldByName('Quntity'+C).OnValidate;
      FieldByName('Quntity'+C).OnValidate:=Nil;
      FieldByName('Quntity'+C).AsInteger:=1;
      FieldByName('Quntity'+C).OnValidate:=EV;
    End;
    if FieldByName('Price1').IsNull Then
      FieldByName('Price1').AsInteger:=0;
    if (Tbl_ShibShibAzmnDate.IsNull) And (Tbl_Shib.State<>dsBrowse) Then
      Tbl_ShibShibAzmnDate.AsDateTime:=DM_WinTuda.Tbl_AzmnFromDate.AsDateTime;
    if FieldByName('LakNo1').IsNull Then
      FieldByName('LakNo1').AsInteger:=Tbl_Azmn.FieldByName('InvLakNo').AsInteger;
    FieldByName('ShibAzmnNo').AsInteger:=Tbl_Azmn.FieldByName('AzmnNo').AsInteger;
    if FieldByName('ShibAsmcta').AsInteger=0 Then
     FieldByName('ShibAsmcta').AsInteger:=Tbl_Azmn.FieldByName('Asmcta').AsInteger;
    if FieldByName('ShibAzmnDate').AsString='' Then
      FieldByName('ShibAzmnDate').AsDateTime:=Tbl_Azmn.FieldByName('FromDate').AsDateTime;
    FieldByName('UpdateRecordDate').AsDateTime:=Now;
    FieldByName('MaklidName').AsString:=WinTudaCurUserName;
    if State=dsInsert Then
    Begin
        Tbl_ShibAzmnOk.AsInteger:=1;
        Tbl_ShibYehusYear.AsInteger:=WinTudaYehusYear;
        Tbl_ShibYehusMonth.AsInteger:=WinTudaYehusMonth;
        Tbl_ShibShibKind.AsInteger:=WinTudaShibKind;
        Tbl_ShibPriceKind.AsInteger:=WinTudaPriceKind;
    End;
    if Tbl_ShibShibKind.IsNull Then
      Tbl_ShibShibKind.AsInteger:=WinTudaShibKind;
    PutMerakezDataInField(Tbl_ShibLookMerakezLak.AsString,Tbl_ShibMrakz);
  End;

End;
procedure TDM_WinTuda.Tbl_ShibAfterInsert(DataSet: TDataSet);
begin
     if FrmWinTuda.PageControl_Tnua.ActivePage.PageIndex>FrmWinTuda.TS_Perut2.PageIndex Then
        FrmWinTuda.PageControl_Tnua.ActivePage:=FrmWinTuda.TS_Tnua1;
     if Tbl_Azmn.State<>dsInsert Then
     Begin
       FrmWinTuda.CopyMidPnlFields;
       PutDefaultsOnShib;
     End;
end;

Procedure TDM_WinTuda.FindNextHazmanaNum;
Var
   HN :LongInt;
Begin
     if Tbl_Azmn.State<>dsBrowse Then
     Begin
          With Frm_GlblWinTuda.Qry_Generic Do
          Begin
               Active:=False;
               Sql.Clear;
               Sql.Add('Select Max(AzmnNo) HazNum');
               Sql.Add('From '+WinTudaAtmAzmnFileName);
               Sql.Add('Where AzmnNo Between '+IntToStr(RangeAzmanFrom)+' and '+IntToStr(RangeAzmnTo));
               Active:=True;
               if (FieldByName('HazNum').AsInteger>=RangeAzmanFrom) And (FieldByName('HazNum').AsInteger<RangeAzmnTo) Then
                   HN:=(FieldByName('HazNum').AsInteger+1)
               Else
                 if (FieldByName('HazNum').AsInteger<RangeAzmanFrom) Then
                    HN:=RangeAzmanFrom
                 Else
                    if (FieldByName('HazNum').AsInteger>=RangeAzmnTo) Then
                    Begin
                        MessageDlg('���� �� ���� ������� ������ �� '+IntToStr(RangeAzmnTo),mtError,[mbOk],0);
                        Tbl_Azmn.Cancel;
                        Active:=False;
                        Exit;
                    End;
               Tbl_Azmn.FieldByName('AzmnNo').AsInteger:=HN;
               Active:=False;
          End;
          SetAtmDbEditF4State(FrmWinTuda.Panel_Top);
       End;
End;

Procedure TDM_WinTuda.BuildLookupList;
Var
   SList:TStringList;
Begin
  Try
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida1Look);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida2Look);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida3Look);
     SList.AddObject(IntToStr(SugTavla_YeMida),Tbl_ShibYemida4Look);
     SList.AddObject(IntToStr(SugTavla_SugMitan),Tbl_ShibTeurMitanLook1);
     SList.AddObject(IntToStr(SugTavla_SugHovala),Tbl_ShibLookHovalaKind);
     FillKodTavlaLookupList(Tbl_KodTavla,SList);

  Finally
     SList.Free;
  End;

  //������
  With Frm_GlblWinTuda.Qry_Generic Do
  Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Select CodeMahzeva,NameMahzeva From Mahzeva');
      Active:=True;
      Tbl_ShibLookShemMahzeva.LookupList.Clear;
      First;
      While Not Eof Do
      Begin
          Tbl_ShibLookShemMahzeva.LookupList.Add(FieldByName('CodeMahzeva').Value,FieldByName('NameMahzeva').Value);
          Next;
      End;
  End;

  Tbl_AzmnLookAzmnType.LookupList.Clear;
  Case WinTudaSugSystem Of
    ssMaz    :Begin
                Tbl_AzmnLookAzmnType.LookupList.Add(0,'�����');
                Tbl_AzmnLookAzmnType.LookupList.Add(1,'���� ����');
                Tbl_AzmnLookAzmnType.LookupList.Add(2,'�����');
                Tbl_AzmnLookAzmnType.LookupList.Add(3,'�� ���� �����');
                Tbl_AzmnLookAzmnType.LookupList.Add(4,'�����');
{                Tbl_AzmnLookAzmnType.LookupList.Add(5,'����"�');}
                Tbl_AzmnLookAzmnType.LookupList.Add(atHiuvNehag,'���� ����');
                Tbl_AzmnLookAzmnType.LookupList.Add(atZikuyLak,'����� ����');
               End;
    ssTovala :Begin
                Tbl_AzmnLookAzmnType.LookupList.Add(0,'�����');
                Tbl_AzmnLookAzmnType.LookupList.Add(1,'������ ������');
                Tbl_AzmnLookAzmnType.LookupList.Add(2,'������');
                Tbl_AzmnLookAzmnType.LookupList.Add(3,'�� ���� �����');
                Tbl_AzmnLookAzmnType.LookupList.Add(4,'�����');
                Tbl_AzmnLookAzmnType.LookupList.Add(5,'����"�');
                Tbl_AzmnLookAzmnType.LookupList.Add(atHiuvNehag,'���� ���');
                Tbl_AzmnLookAzmnType.LookupList.Add(atZikuyLak,'����� ����');
              End;
  End; //Case
End;

procedure TDM_WinTuda.Tbl_ShibPrice1Validate(Sender: TField);
Begin
  if (WinTudaSugSystem=ssTovala) And (Tbl_Shib.FieldByName('Price2').AsInteger=0) Then
    FrmWinTuda.Action_Price2SubHanForLakFromPrice1Execute(Nil);
  CalcShibTotalPrices;
end;

Procedure TDM_WinTuda.CalcShibTotalPrices;
Var
   I :LongInt;
begin
  For I:=1 To 4 Do
    if (Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsString<>'') And
       (Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsString<>'') Then

      if (Tbl_Shib.FieldByName('YeMida'+IntToStr(I)).AsString<>'0') And (Tbl_Shib.FieldByName('PriceIsGlobal').AsInteger=0{<>1}) Then
           Tbl_Shib.FieldByName('PriceQuntity'+IntToStr(I)).Value:=(Tbl_Shib.FieldByName('Quntity'+IntToStr(I)).AsFloat)*
               (Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsFloat)
      Else
           Tbl_Shib.FieldByName('PriceQuntity'+IntToStr(I)).Value:=(Tbl_Shib.FieldByName('Price'+IntToStr(I)).AsFloat);
End;
procedure TDM_WinTuda.Tbl_ShibQuntity2Validate(Sender: TField);
begin
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_ShibYeMida2Validate(Sender: TField);
begin
     CalcShibTotalPrices;
end;

procedure TDM_WinTuda.PutMerakezDataInField(MerakezName:String;TheField :TField);
Var
   I,NumOfFields :LongInt;
   FieldName,StrH :String;
   FieldBuffer:String;
Begin
     FieldName:='';
     FieldBuffer:='';
     if Trim(MerakezName)='' Then Exit;
     NumOfFields:=CountFieldsInSepString(MerakezName,',');
     Try
        For I:=1 To NumOfFields Do
        Begin
           FieldName:=GetFieldFromSeparateString(MerakezName,',',I);
           if Tbl_Shib.FieldByName(FieldName) Is TDateTimeField Then
             StrH:=FormatDateTime('yyyymmdd',Tbl_Shib.FieldByName(FieldName).AsDateTime)
           Else
             StrH:=Tbl_Shib.FieldByName(FieldName).AsString;
           if Length(StrH)> 10 Then
              StrH:=Copy(StrH,1,10);
           While Length(StrH)<10 Do
           Begin
                 if Tbl_Shib.FieldByName(FieldName) is TNumericField Then
                    StrH:='0'+StrH
                 Else
                   if Tbl_Shib.FieldByName(FieldName) Is TDateTimeField Then
                     StrH:=StrH+'0'
                   Else
                     StrH:=StrH+' ';
           End;
           FieldBuffer:=FieldBuffer+StrH;

        End;
        if Tbl_Shib.State=dsBrowse Then
           Tbl_Shib.Edit;
        TheField.AsString:=FieldBuffer;
     Except On e:Exception Do
          ShowMessage('�� ���� ������ ����'+
                      #13+E.Message);
     End;
End;

procedure TDM_WinTuda.DS_AzmnDataChange(Sender: TObject; Field: TField);
begin
     if (FrmWinTuda<>Nil) Then
     Begin
        if (Tbl_Azmn.State=dsBrowse) Then
         Begin
            FrmWinTuda.MaskEdit_FromTime.Text:=Copy(GetFieldFromSeparateString(Tbl_AzmnFromDate.AsString,' ',2),1,5);
            FrmWinTuda.MaskEdit_ToTime.Text:=Copy(TimeToStr(Tbl_AzmnToDate.AsDateTime),1,5);
         End
     End;
end;

procedure TDM_WinTuda.DS_ShibStateChange(Sender: TObject);
begin
   Try
      if FrmWinTuda<>Nil Then
      Begin
         FrmWinTuda.StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
      End;
   Except
   End;
   SetReadOnlyFields;
end;

procedure TDM_WinTuda.Tbl_ShibShibBeginTimeGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
     Text := FormatDateTime('hh:nn',Sender.AsDateTime);
end;

procedure TDM_WinTuda.Tbl_ShibShibBeginTimeSetText(Sender: TField;
  const Text: String);
begin
     Sender.AsDateTime := Trunc (Tbl_ShibShibAzmnDate.AsDateTime){Date} + Frac (StrToTime(Text));
end;

procedure TDM_WinTuda.Tbl_ShibFirstSpidoValidate(Sender: TField);
begin
     if (Not Tbl_ShibFirstSpido.IsNull) And (Tbl_ShibFirstSpido.AsInteger>0)
        And (Not Tbl_ShibLastSpido.IsNull) And (Tbl_ShibLastSpido.AsInteger>0) Then
     Begin
         if (Tbl_ShibFirstSpido.AsInteger>Tbl_ShibLastSpido.AsInteger) Then
         Begin
              MessageDlg('�������� ����� ���� ��������� ���',mtError,[mbOk],0);
              Abort;
         End
         Else
            Tbl_ShibShibTotalKm.AsInteger:=Tbl_ShibLastSpido.AsInteger-Tbl_ShibFirstSpido.AsInteger;
     End;

end;

procedure TDM_WinTuda.Tbl_ShibCarNumValidate(Sender: TField);
begin
     if Sender.AsString<>'' Then
     Begin
         Tbl_Rehev.IndexFieldNames:=IndFields_Rehev_PrimaryKey;
         if Tbl_Rehev.FindKey([Sender.Value]) Then
         Begin
            if Tbl_ShibDriverNo1.IsNull Then
               Tbl_Shib.FieldByName('DriverNo1').AsString:=Tbl_Rehev.FieldByName('Kod_Nehag').AsString;
         End
         Else
             Begin
                  ShowMessage('��� �� ���� ������');
                  Sender.AsString:='';
                  //Abort;
             End;
     End;
end;

procedure TDM_WinTuda.RxQuery_TabSheetBuildBeforeClose(DataSet: TDataSet);
begin
     RxQuery_TabSheetBuild.ApplyUpdates;
end;

procedure TDM_WinTuda.Tbl_ShibShibBeginTimeValidate(Sender: TField);
Var
   TimeH :TDateTime;
   StrH :String;
begin
    if (Not Tbl_ShibShibBeginTime.IsNull) And (Not Tbl_ShibShibEndTime.IsNull) Then
    Begin
         if Tbl_ShibShibBeginTime.AsDateTime>Tbl_ShibShibEndTime.AsDateTime Then
         Begin
              MessageDlg('��� ����� ����� ���� ����',mtError,[mbOk],0);
              Abort;
         End
         Else
         Begin
             TimeH:=Tbl_ShibShibEndTime.AsDateTime-Tbl_ShibShibBeginTime.AsDateTime;
             StrH:=TimeToStr(TimeH);
             Tbl_ShibTotalHour.AsFloat:=StrToInt(GetFieldFromSeparateString(StrH,':',1))+
                                        (StrToInt(GetFieldFromSeparateString(StrH,':',2))/60);
         End;
    End;

end;

procedure TDM_WinTuda.DB_WinTudaLogin(Database: TDatabase;
  LoginParams: TStrings);
begin
    LoginParams.Clear;
    LoginParams.Add('USER NAME='+WinTudaDBUserName);
    LoginParams.Add('PASSWORD='+WinTudaDBPassword);
end;

procedure TDM_WinTuda.Tbl_AzmnAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(FrmWinTuda);
end;

procedure TDM_WinTuda.Tbl_AzmnAfterScroll(DataSet: TDataSet);
begin
    InNewHazmana:=False;
    DoShibDetails(Tbl_AzmnAzmnNo.AsString);
end;

procedure TDM_WinTuda.Tbl_ShibValidateLookupField(Sender: TObject;
  AField: TField; var IsValid: Boolean);
begin
    if AField=Tbl_ShibLookMerakezLak Then
       IsValid:=True;
end;

procedure TDM_WinTuda.Tbl_AzmnBeforePost(DataSet: TDataSet);
begin
     if Tbl_Azmn.State=dsInsert Then
        FindNextHazmanaNum;
     if (Tbl_Azmn.State=dsInsert) And (Tbl_Azmn.Database.IsSQLBased) Then
       BuildSqlScriptForInsert(TStringList(UpdateSQL_Azmn.InsertSQL),Tbl_Azmn,WinTudaAtmAzmnFileName);
     UpdateSQL_Azmn.InsertSQL.SaveToFile('c:\InsertAzmn.sql');
end;

procedure TDM_WinTuda.DS_AzmnStateChange(Sender: TObject);
begin
      if (Tbl_Azmn.State=dsInsert) Then
      Begin
           FrmWinTuda.MaskEdit_FromTime.Clear;
           FrmWinTuda.MaskEdit_ToTime.Clear;
           InNewHazmana := True;
      End;

     Try
        if FrmWinTuda<>Nil Then
        Begin
           FrmWinTuda.StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
           if Tbl_Azmn.State=dsBrowse Then
              SetAtmDbEditF4State(FrmWinTuda);
        End;
     Except
     End;
end;

procedure TDM_WinTuda.Tbl_ShibShibAzmnDateValidate(Sender: TField);
Var
   Day,Month,Year :Word;
begin
  DecodeDate(Sender.AsDateTime,Year,Month,Day);
  if (Year<>WinTudaYehusYear) or (Month<>WinTudaYehusMonth) Then
    if MessageDlg('����� ���� ����� ����� ��� ������',mtConfirmation,[mbYes,mbNo],0)=mrNo Then
       Abort;
end;

procedure TDM_WinTuda.Tbl_ShibBeforeDelete(DataSet: TDataSet);
begin
     if (Tbl_ShibLakCodeBizua.AsInteger<>0) Then
     Begin
          MessageDlg('��� ������ ����� ����� ����� ���� �������',mtInformation,[mbOk],0);
          Abort;
          Exit;
     End;
end;

procedure TDM_WinTuda.FillAzmnDays;
Var
   CurDate :TDateTime;
   InitValue :Integer;
Begin
     if Tbl_azmnAzmnType.AsInteger=atRavFix Then
        InitValue:=1
     Else
         InitValue:=0;
     Tbl_AzmnSunday.AsInteger:=InitValue;
     Tbl_AzmnMonday.AsInteger:=InitValue;
     Tbl_AzmnTuesday.AsInteger:=InitValue;
     Tbl_AzmnWednesday.AsInteger:=InitValue;
     Tbl_AzmnThursday.AsInteger:=InitValue;
     Tbl_AzmnFriday.AsInteger:=InitValue;
     Tbl_AzmnSaturday.AsInteger:=InitValue;

     CurDate:=Tbl_AzmnFromDate.AsDateTime;
     While (InitValue=0) And (CurDate<=Tbl_AzmnTodate.AsDateTime) Do
     Begin
          Case DayOfWeek(CurDate) of
               1:Tbl_AzmnSunday.AsInteger:=1;
               2:Tbl_AzmnMonday.AsInteger:=1;
               3:Tbl_AzmnTuesday.AsInteger:=1;
               4:Tbl_AzmnWednesday.AsInteger:=1;
               5:Tbl_AzmnThursday.AsInteger:=1;
               6:Tbl_AzmnFriday.AsInteger:=1;
               7:Tbl_AzmnSaturday.AsInteger:=1;
          End; //Case
          CurDate:=CurDate+1;
     End;
End;
procedure TDM_WinTuda.Tbl_AzmnAzmnTypeValidate(Sender: TField);
begin
     if (Not Tbl_AzmnFromDate.IsNull) And (Not Tbl_AzmnToDate.IsNull) Then
        FillAzmnDays;
end;

procedure TDM_WinTuda.Tbl_ShibDriverNo1SetText(Sender: TField;
  const Text: String);
begin
  if (Text<>'') And (CompareText(WinTudaNehagCheckFieldName,'Kod_Nehag')<>0) Then
  Begin
      RxQuery_Temp.Active:=False;
      RxQuery_Temp.Sql.Clear;
      RxQuery_Temp.Sql.Add('Select Kod_Nehag From Nehag');
      RxQuery_Temp.Sql.Add('Where '+WinTudaNehagCheckFieldName+'='''+Text+'''');
      RxQuery_Temp.Active:=True;
      Try
        RxQuery_Temp.First;
        if Not RxQuery_Temp.Eof Then
          Sender.AsString:=RxQuery_Temp.FieldByName('Kod_Nehag').AsString
        Else
          Sender.AsString:='';
      Finally
        RxQuery_Temp.Active:=False;
      End;
  End
  Else
    Sender.AsString:=Text;
end;

procedure TDM_WinTuda.Tbl_AzmnCalcFieldsMaz(DataSet: TDataSet);
begin
  Tbl_AzmnTotalAfterDiscount.AsFloat:=Tbl_AzmnTotalPrice1.AsFloat-Tbl_AzmnTotalPrice2.AsFloat;
  Tbl_AzmnTotalWithMam.AsFloat:=Tbl_AzmnTotalPrice3.AsFloat+Tbl_AzmnTotalAfterDiscount.AsFloat;
end;

Procedure TDM_WinTuda.DoShibDetails(AzmnNo :String);
Begin
  if CurAzmnNo=AzmnNo Then
    Exit;
  if WinTudaSqlMode=0 Then  //Local(Paradox)
  Begin
    if (Tbl_Shib.State in [dsInsert,dsEdit]) Then
      if (AzmnNo<>'') Then
        Tbl_Shib.Post
      Else
        Tbl_Shib.Cancel;
    if AzmnNo='' Then
      AzmnNo:='-1';
//    Tbl_Shib.DisableControls;
    Tbl_Shib.Filtered:=False;
    Tbl_Shib.Filter:='ShibAzmnNo='+AzmnNo;
    Tbl_Shib.Filtered:=True;
//    Tbl_Shib.EnableControls;
    CurAzmnNo:=AzmnNo;
  End
  Else
  if WinTudaSqlMode=1 Then //Client/Server
  Begin
    if AzmnNo='' Then
      AzmnNo:='-1';
//    Tbl_Shib.DisableControls;
    Tbl_Shib.Active:=False;
    Tbl_Shib.MacroByName('MasterField').AsString:='ShibAzmnNo='+AzmnNo+' And ';
    Tbl_Shib.Active:=True;
//    Tbl_Shib.EnableControls;
    CurAzmnNo:=AzmnNo;
  End;
End;
procedure TDM_WinTuda.Tbl_AzmnAzmnNoChange(Sender: TField);
begin
  DoShibDetails(Sender.AsString);
end;

Procedure TDM_WinTuda.RefreshMainQuerys;
Begin
  Tbl_Azmn.Active:=False;
  Tbl_Shib.Active:=False;
  Tbl_Azmn.Active:=True;
  Tbl_Shib.Active:=True;
  BuildLookupList;
End;

procedure TDM_WinTuda.Tbl_AzmnInvLakNoChange(Sender: TField);
Var
  RecCount :LongInt;
  CurAzmnNo :LongInt;
begin
  Tbl_Azmn.FieldByName('InvName').AsString:=Tbl_Azmn.FieldByName('LakName').AsString;
  if Tbl_Azmn.State<>dsInsert Then
  Begin
    With RxQuery_Temp Do
    Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Select Count(*) CR From '+WinTudaAtmShibFileName+' Where ShibAzmnNo='+Tbl_Azmn.FieldByName('AzmnNo').AsString);
      Active:=True;
      RecCount:=FieldByName('CR').AsInteger;
      Active:=False;
      if RecCount>0 Then
        if MessageDlg('��� ����� ���� ��������',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
        Begin
          Try
            CurAzmnNo:=Tbl_Azmn.FieldByName('AzmnNo').AsInteger;
            Tbl_Azmn.SavePressed:=True;
            Try
              Tbl_AzmnInvLakNo.OnChange:=Nil;
              Tbl_Azmn.Post;
            Finally
              Tbl_AzmnInvLakNo.OnChange:=Tbl_AzmnInvLakNoChange;
            End;
            Screen.Cursor:=crSqlWait;
            Sql.Clear;
            Sql.Add('Update '+WinTudaAtmShibFileName+' Set LakNo1='+Tbl_Azmn.FieldByName('InvLakNo').AsString);
            Sql.Add('Where ShibAzmnNo='+Tbl_Azmn.FieldByName('AzmnNo').AsString);
            RxQuery_Temp.ExecSQL;
            RefreshMainQuerys;
            Tbl_Azmn.Locate('AzmnNo',CurAzmnNo,[]);
          Finally
            Screen.Cursor:=crDefault;
          End;
        End;
    End;
  End;
end;

procedure TDM_WinTuda.Tbl_ShibPrice2Validate(Sender: TField);
begin
  if WinTudaSugSystem=ssMaz Then
  Begin
    if Tbl_Azmn.FieldByName('TotalPrice3').AsFloat>0 Then
      if Tbl_Shib.FieldByName('Price2').AsFloat>0 Then
        Tbl_Shib.FieldByName('Price1').AsFloat:=Tbl_Azmn.FieldByName('TotalPrice3').AsFloat*Tbl_Shib.FieldByName('Price2').AsFloat;
  End;
  CalcShibTotalPrices;
end;

procedure TDM_WinTuda.Tbl_ShibUpdateError(DataSet: TDataSet;
  E: EDatabaseError; UpdateKind: TUpdateKind;
  var UpdateAction: TUpdateAction);
begin
  ShowMessage(e.Message+' '+DataSet.Name+' '+IntToStr(Ord(UpdateKind)));
end;

procedure TDM_WinTuda.Tbl_AzmnValidateLookupField(Sender: TObject;
  AField: TField; var IsValid: Boolean);
begin
  if AField=Tbl_AzmnLookPhoneLak Then
    IsValid:=True;
end;

Function TDM_WinTuda.CheckRetzefTeuda:Boolean;
Begin
  if (Trim(WinTudaCheckValueExistFieldName)='') Or
     (Tbl_Shib.FieldByName(WinTudaCheckValueExistFieldName).AsInteger = 0) Then
  Begin
    Result:=True;
    Exit;
  End;
  With RxQuery_Temp Do
  Begin
    Active:=False;
    Sql.Clear;
    Sql.Add('Select ShibNo From '+WinTudaAtmShibFileName+' Where '+WinTudaCheckValueExistFieldName+' = '+Tbl_Shib.FieldByName(WinTudaCheckValueExistFieldName).AsString);
    Sql.Add('And LakNo1 = '+Tbl_Shib.FieldByName('LakNo1').AsString);
    Active:=True;
    Try
      if Eof And Bof Then
        Result:=True
      Else
        Result:=False;
    Finally
      Active:=False;
    End;
  End;
End;
procedure TDM_WinTuda.Tbl_ShibBeforeEdit(DataSet: TDataSet);
begin
  if Tbl_Shib.FieldByName('ShibNo').AsInteger=0 Then
    if MessageDlg('���� ����� ����� �� ���� ����� �� ����� ������� ������'+
                  #13'��� ����� ���',mtWarning,[mbYes,mbNo],0)=mrYes Then
    Begin
      if WinTudaSqlMode=0 Then //Paradox (Filter)
      Begin
        Tbl_Shib.Close;
        Tbl_Shib.Open;
      End;
      DoShibDetails(Tbl_AzmnAzmnNo.AsString);
    End
    Else
      Abort;
end;

procedure TDM_WinTuda.Tbl_ShibLakNo1Validate(Sender: TField);
begin
  if WinTudaLakMastBeEqual Then
    if (Sender.AsInteger<>0) And (Sender.AsInteger <> Tbl_Azmn.FieldByName('InvLakNo').AsInteger) Then
    Begin
      MessageDlg('���� ����� ���� ����� ��� ���� ����',mtError,[mbOk],0);
      Abort;
    End;
end;

procedure TDM_WinTuda.DB_WinTudaAfterConnect(Sender: TObject);
begin
  if CompareText(WinTudaSqlServerType,'Oracle')=0 Then
  Begin
    DB_WinTuda.StartTransaction;
    DB_WinTuda.Execute(OracleDateFormatSql);
    DB_WinTuda.Commit;
  End;

  if CompareText(WinTudaSqlServerType,'Sybase')=0 Then
  Begin
    DB_WinTuda.StartTransaction;
    DB_WinTuda.Execute(SybaseDateFormatSql);
    DB_WinTuda.Commit;
  End;
end;

procedure TDM_WinTuda.Tbl_ShibShibAzmnDateSetText(Sender: TField;
  const Text: String);
begin
  Sender.AsDateTime:=Trunc(Sender.AsFloat);
end;

Procedure TDM_WinTuda.SetReadOnlyFields;
Var
  I :longInt;
Begin
  Screen.Cursor:=crHourGlass;
  Tbl_Shib.DisableControls;
  Try
    if (Tbl_Shib.State<>dsInsert) And (Tbl_Shib.FieldByName('LakCodeBizua').AsInteger<>0) Or (Tbl_Shib.FieldByName('LakHesbonit').AsInteger<>0) Then
    Begin
      With Tbl_Shib Do
      Begin
        For I:=0 To FieldCount-1 Do
          Fields[i].ReadOnly:=True;
        FieldByName('DriverNo1').ReadOnly:=False;
        FieldByName('DriverName').ReadOnly:=False;
        FieldByName('CarNum').ReadOnly:=False;
        FieldByName('HovalaKind').ReadOnly:=False;
        FieldByName('ShibTuda').ReadOnly:=False;
        FieldByName('ShibRem1').ReadOnly:=False;
        FieldByName('PriceQuntity1').ReadOnly:=False;
        For I:=2 To 4 Do
        Begin
          FieldByName('YeMida'+IntToStr(I)).ReadOnly:=False;
          FieldByName('Quntity'+IntToStr(I)).ReadOnly:=False;
          FieldByName('Price'+IntToStr(I)).ReadOnly:=False;
          FieldByName('PriceQuntity'+IntToStr(I)).ReadOnly:=False;
        End;
      End;
    End
    Else
      For I:=0 To Tbl_Shib.FieldCount-1 Do
        Tbl_Shib.Fields[i].ReadOnly:=False;
  Finally
    Tbl_Shib.EnableControls;
    Screen.Cursor:=crDefault;
  End;
End;

end.

unit F_Calendar;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, Mask, ComCtrls, DBTables, ExtCtrls, AtmComp,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, AtmAdvTable,Dialogs, ToolWin,
  AtmLookCombo, Spin, Menus, Yearplan, RxQuery, AtmRxQuery;

type
  TFrm_Calendar = class(TForm)
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ToolBar1: TToolBar;
    TBtn_RefreshCalendar: TToolButton;
    Panel1: TPanel;
    Label1: TLabel;
    TBtn_Report: TToolButton;
    YearPlanner1: TYearPlanner;
    PopupMenu_SugDay: TPopupMenu;
    SpinEdit_Year: TSpinEdit;
    AtmRxQuery_Calendar: TAtmRxQuery;
    Shape_Color7: TShape;
    Shape_Color8: TShape;
    Shape_Color5: TShape;
    Shape_Color6: TShape;
    Shape_Color3: TShape;
    Shape_Color4: TShape;
    Shape_Color1: TShape;
    Shape_Color2: TShape;
    Label_Color1: TLabel;
    Label_Color2: TLabel;
    Label_Color3: TLabel;
    Label_Color4: TLabel;
    Label_Color5: TLabel;
    Label_Color6: TLabel;
    Label_Color7: TLabel;
    Label_Color8: TLabel;
    Label9: TLabel;
    CB_SugDay: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MonthCalendar1Click(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure SpinEdit_YearChange(Sender: TObject);
    procedure CB_SugDayChange(Sender: TObject);
    procedure TBtn_RefreshCalendarClick(Sender: TObject);
  private
    { private declarations }
    Procedure BuildSugDayMenu;
    Function  SugDayToColor(SugDay :LongInt):TColor;
    Procedure LoadCalendarAndUpdateView;
  public
    Procedure DaySelected(Sender :TObject);
  end;

var
  Frm_Calendar: TFrm_Calendar;

implementation
Uses Crt_Glbl,AtmRutin,DmAtmCrt,AtmConst;
{$R *.DFM}

procedure TFrm_Calendar.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     DM_AtmCrt.Query_KodTavla.ParamByName('PSugTavla').AsInteger:=SugTavla_SugCalendar;
     DM_AtmCrt.Query_KodTavla.Active:=True;
     Try
         CB_SugDay.Items.Clear;
         DM_AtmCrt.Query_KodTavla.First;
         While Not DM_AtmCrt.Query_KodTavla.Eof Do
         Begin
              CB_SugDay.Items.Add(DM_AtmCrt.Query_KodTavla.FieldByName('Teur_Tavla').AsString);
              DM_AtmCrt.Query_KodTavla.Next;
         End;
     Finally
            DM_AtmCrt.Query_KodTavla.Active:=False;
     End;
//     LoadCalendarAndUpdateView;
  Finally
     Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_Calendar.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_Calendar.FormShow(Sender: TObject);
begin
  BuildSugDayMenu;
  FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_Calendar.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
//   IsAppShowHint: Boolean;
   i: Integer;
   hw: THintWindow;
begin
  for i := 0 to YearPlanner1.ComponentCount-1 do
  begin
       if YearPlanner1.Components[i] is THintWindow then
       begin
            hw := (YearPlanner1.Components[i] as THintWindow);
            hw.ReleaseHandle;
            Break;
       end;
  end;        
//  YearPlanner1.HintDelay := 500;
{  IsAppShowHint := Application.ShowHint;
  Application.ShowHint := False;
  Application.ProcessMessages;
 } 
  Frm_Calendar:=Nil;
  Action:=caFree;
  
//  Application.ShowHint := IsAppShowHint;
end;

procedure TFrm_Calendar.MonthCalendar1Click(Sender: TObject);
Var
   StrH :String;
begin
end;

procedure TFrm_Calendar.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msCalendar],False,CrtMeholelHandle);
end;

Procedure TFrm_Calendar.BuildSugDayMenu;
Var
  TM :TMenuItem;
  StrH :String;
Begin
    With DM_AtmCrt.Qry_Temp Do //����� ���� �����
    Begin
      TM:=TMenuItem.Create(PopupMenu_SugDay);
      TM.Caption:='��� ����� ����';
      TM.Tag:=-1;
      TM.OnClick:=DaySelected;
      PopupMenu_SugDay.Items.Add(TM);
      Active:=False;
      Sql.Clear;
      Sql.Add('Select * From KODTAVLA');
      Sql.Add('Where Sug_Tavla='+IntToStr(SugTavla_SugDay));
      Try
        Active:=True;
        First;
        While Not Eof Do
        Begin
          TM:=TMenuItem.Create(PopupMenu_SugDay);
          TM.Caption:=FieldByName('Teur_Tavla').AsString;
          TM.Tag:=FieldByName('Kod_Tavla').AsInteger;
          TM.OnClick:=DaySelected;
          PopupMenu_SugDay.Items.Add(TM);

          StrH:='Label_Color'+FieldByName('Kod_Tavla').AsString;
          if Self.FindComponent(Trim(StrH))<>Nil Then
          Begin
            TWinControl(Self.FindComponent('Shape_Color'+FieldByName('Kod_Tavla').AsString)).Visible:=True;
            TWinControl(Self.FindComponent('Label_Color'+FieldByName('Kod_Tavla').AsString)).Visible:=True;
            TLabel(Self.FindComponent('Label_Color'+FieldByName('Kod_Tavla').AsString)).Caption:=FieldByName('Teur_Tavla').AsString;
          End;
          Next;
        End;
      Finally
        Active:=False;
      End;
    End;
End;

Procedure TFrm_Calendar.DaySelected(Sender :TObject);
Var
  m,d :Word;
  CurDate:TDateTime;
  CurDateString :String;
  CurDescription :String; //����
Begin
  d:=YearPlanner1.CurrentDate.Day;
  m:=YearPlanner1.CurrentDate.Month;
  if YearPlanner1.CellData[M,D].CellTag=TMenuItem(Sender).Tag Then
    Exit;
  CurDate:=EncodeDate(SpinEdit_Year.Value,M,D);
  CurDateString:=DateToSqlStr(CurDate,'mm/dd/yyyy');
  CurDescription:=YearPlanner1.CellData[m,d].CellHint;

  if YearPlanner1.CellData[M,D].CellTag>-1 Then //��� ��� ����
  Begin
    if TMenuItem(Sender).Tag=-1 Then //����� ���� ���� - ����� ������
    With DM_AtmCrt.Qry_Temp Do
    Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Delete From CALENDAR');
      Sql.Add('Where SCHOOL_YEAR='+SpinEdit_Year.Text);
      Sql.Add('And CODE_CALENDER='+IntToStr(CB_SugDay.ItemIndex));
      Sql.Add('And DAY_DATE='''+CurDateString+'''');
      ExecSQL;
    End
    Else
    Begin //����� ���
      With DM_AtmCrt.Qry_Temp Do
      Begin
        CurDescription:=InputBox('����','��� ����',CurDescription);
        Active:=False;
        Sql.Clear;
        Sql.Add('Update CALENDAR');
        Sql.Add('Set DESCRIPTION='''+CurDescription+''',');
        Sql.Add('DAY_OF_WEEK='''+ShortDayNames[DayOfWeek(EncodeDate(SpinEdit_Year.Value,M,D))]+''',');
        Sql.Add('CODE_SUG_DAY='+IntToStr(TMenuItem(Sender).Tag));
        Sql.Add('Where SCHOOL_YEAR='+SpinEdit_Year.Text);
        Sql.Add('And CODE_CALENDER='+IntToStr(CB_SugDay.ItemIndex));
        Sql.Add('And DAY_DATE='''+CurDateString+'''');
        ExecSQL;
      End;
    End;
  End
  Else //��� ��� ���� �� ������ ���
  Begin
    With DM_AtmCrt.Qry_Temp Do
    Begin
      CurDescription:=InputBox('����','��� ����',CurDescription);
      Active:=False;
      Sql.Clear;
      Sql.Add('Insert into CALENDAR');
      Sql.Add('(SCHOOL_YEAR,CODE_CALENDER,DAY_DATE,DESCRIPTION,DAY_OF_WEEK,CODE_SUG_DAY)');
      Sql.Add('Values');
      Sql.Add('('+SpinEdit_Year.Text+',');
      Sql.Add(IntToStr(CB_SugDay.ItemIndex)+',');
      Sql.Add(''''+CurDateString+''',');
      Sql.Add(''''+CurDescription+''',');
      Sql.Add(''''+ShortDayNames[DayOfWeek(CurDate)]+''',');
      Sql.Add(IntToStr(TMenuItem(Sender).Tag)+')');
      ExecSQL;
    End;
  End;

  YearPlanner1.CellData[m,d].CellTag:=TMenuItem(Sender).Tag;
  YearPlanner1.CellData[m,d].CellColor:=SugDayToColor(TMenuItem(Sender).Tag);
  YearPlanner1.CellData[m,d].CustomColor:=TMenuItem(Sender).Tag<>-1;
  YearPlanner1.CellData[m,d].CellHint:=CurDescription;
  YearPlanner1.Invalidate;

End;

procedure TFrm_Calendar.SpinEdit_YearChange(Sender: TObject);
begin
  YearPlanner1.Year:=SpinEdit_Year.Value;
  LoadCalendarAndUpdateView;
end;

Function TFrm_Calendar.SugDayToColor(SugDay :LongInt):TColor;
Begin
  Case SugDay Of
    1:Result:=clGreen; //�����
    2:Result:=clFuchsia;//�����
    3:Result:=clYellow;//�������
    4:Result:=clBlue;//��� ���
    5:Result:=clAqua;//�����
    6:Result:=clLime;//����� ������
    7:Result:=clPurple//����
  Else
    Result:=clRed;
  End;//Case

End;

Procedure TFrm_Calendar.LoadCalendarAndUpdateView;
Var
  D,M,Y :Word;
  I,J :LongInt;
Begin
    For I:=1 To 12 Do
      For J:=1 To 31 Do
      Begin
        YearPlanner1.CellData[I,J].CellHint:='';
        YearPlanner1.CellData[I,J].CustomColor:=False;
        YearPlanner1.CellData[I,J].CellColor:=YearPlanner1.DayColor;
        YearPlanner1.CellData[I,J].CellTag:=-1;
      End;

    With AtmRxQuery_Calendar Do //���� ��� ����
    Begin
      Active:=False;
      ParamByName('PYear').AsInteger:=SpinEdit_Year.Value;
      ParamByName('PCodeCalendar').AsInteger:=CB_SugDay.ItemIndex;
      Screen.Cursor:=crSqlWait;
      Active:=True;
      Try
        First;
        While Not Eof Do
        Begin
          DecodeDate(FieldByName('DAY_DATE').AsDateTime,y,m,d);
          YearPlanner1.CellData[m,d].CellTag:=FieldByName('CODE_SUG_DAY').AsInteger;
          YearPlanner1.CellData[m,d].CellColor:=SugDayToColor(FieldByName('CODE_SUG_DAY').AsInteger);
          YearPlanner1.CellData[m,d].CustomColor:=True;
          YearPlanner1.CellData[m,d].CellHint:=FieldByName('DESCRIPTION').AsString;
          if Trim(YearPlanner1.CellData[m,d].CellHint)='' Then
            For I:=0 To PopupMenu_SugDay.Items.Count-1 Do
              if TMenuItem(PopupMenu_SugDay.Items[i]).Tag=FieldByName('CODE_SUG_DAY').AsInteger Then
              Begin
                YearPlanner1.CellData[m,d].CellHint:=TMenuItem(PopupMenu_SugDay.Items[i]).Caption;
                Break;
              End;
          Next;
        End;
      Finally
        Active:=False;
        Screen.Cursor:=crDefault;
      End;
    End;
    YearPlanner1.Invalidate;
End;


procedure TFrm_Calendar.CB_SugDayChange(Sender: TObject);
begin
  LoadCalendarAndUpdateView;
end;

procedure TFrm_Calendar.TBtn_RefreshCalendarClick(Sender: TObject);
begin
  LoadCalendarAndUpdateView;
end;

end.

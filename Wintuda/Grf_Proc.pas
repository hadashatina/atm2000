unit Grf_Proc;

interface
  { ******************* InterFace *************** }
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  ComCtrls,Forms, Dialogs, ExtCtrls, Menus, Buttons,IniFiles,DB,Grids;
var
    XRel : real;
    ARow,ACol : longint;
    MyRect : TRect;
    InDragging : boolean;
    ShibMembStartDrag,
    CarMembStartDrag,
    ShibMembEndDrag,
    CarMembEndDrag  : longint;
    DraggingType : integer;
    DraggingItem : longint;
    XStart,YStart : integer;
    Sgr_LeftCol_No : integer;
    DrawRect : TRect;
    CarItemFound,ShibItemFound: longint;
    PositionOld : longint;
    EnterRightGrid : boolean;  {Dima 29/03/99}
procedure DefPlaceOfMouse(X,Y:integer;LGrid : TStringGrid);
procedure InitDragging;
procedure InitGrids;
procedure DrawRGridHeader(Canv:TCanvas;Rect: TRect);
procedure DrawRGridCell(Canv : TCanvas; NRow: Integer;Rect: TRect;
          LGrid : TStringGrid);
procedure StartDraggingShib(LGrid : TStringGrid;X,Y:integer);
procedure ContinueDraggingShib(LGrid : TStringGrid;X,Y:integer);
procedure EndDraggingShib(LGrid : TStringGrid;X,Y:integer);
function  FoundListItems:boolean;
procedure ActionForShibItem(LGrid : TStringGrid;CarItem,ShibItem:longint);
procedure DelCarEmpty(CarItem:longint);
function  ShibOverLaps(ShibItemFrom,CarItem : longint):boolean;
procedure RefreshGraf(InRecNo:Longint);
implementation
  { ********************** implementation *********** }

uses UniSadran,Grf_Data,UniDataModule1;
              { ************** }
procedure InitDragging;
begin
  InDragging:=FALSE;
  ShibMembStartDrag:=-1;
  CarMembStartDrag:=-1;
  ShibMembEndDrag:=-1;
  CarMembEndDrag:=-1;
end;
  { ************** }
procedure InitGrids;
var ix1 : longint;

begin
  {PositionOld:=FrmSadran.SCBxRight.HorzScrollBar.Position;}
  with FrmSadran.SGr_Left do
  begin
    RowCount:=CarList.Count+1;
    ColCount:=2;
    cells[0,0]:='�����';
    Colwidths[0]:=0;
    cells[1,0]:='�����+��� ���';
    {cells[1,0]:='���';cells[2,0]:='�����';}
    for ix1:= 1 to CarList.Count  do
    begin
      CarObject:=CarList.Items[ix1-1];
      with CarObject do
      begin
        cells[0,ix1]:=LNo_Rishui;
        cells[1,ix1]:=LRem;
      end;
    end;
  end;
  with FrmSadran.SGr_Right do
  begin
    ColCount:=1;
    RowCount:=FrmSadran.SGr_Left.RowCount;
    DefaultColWidth:=DrawColWidth;
    Width:=DrawColWidth;
  end;
  {FrmSadran.SCBxRight.HorzScrollBar.Position:=PositionOld;}
end;
       { ******************** }

procedure DrawRGridHeader(Canv:TCanvas;Rect: TRect);
var HelpStr : shortstring;
    ix1,ix2 : longint;
begin
  with Canv do
  begin
    with Rect do
    begin
      Font.Size:= (Bottom - Top) div 4;
      Pen.Color:=clBlack;
      for ix1:=0 {1} to round((EndPoint - BegPoint)/ MajorDeltaT) - 1 do
      begin
        case SugMajor of
          0..5: {hour}
            begin
              LongTimeFormat:='HH:MM';
              HelpStr:= timetostr(BegPoint + MajorDeltaT * (ix1 {- 1}));
              case SugMajor of
                 0:NumSubMajor:=5;
                 1:NumSubMajor:=3;
                 2:NumSubMajor:=6;
                 3:NumSubMajor:=4;
                 4:NumSubMajor:=8;
                 5:NumSubMajor:=6;
              end;
            end;
          6: {day}
            begin
              LongDateFormat:='DD/MM/YY';
              HelpStr:= Datetostr(BegPoint + MajorDeltaT * (ix1 { -1}));
              NumSubMajor:=24;
            end;
        end;{case}
        NumSubMajor:=NumSubMajor * MajorValue;
        textout(Left+ MajorDeltaX* (ix1{-1})+
           ( MajorDeltaX - TextWidth(HelpStr))div 2 ,Top,HelpStr);
        MoveTo(Left+ MajorDeltaX* ix1 ,Top );
        LineTo(Left+ MajorDeltaX* ix1,Bottom  div 2 );
        for ix2:= 1 to NumSubMajor do
        begin
            MoveTo(Left+ MajorDeltaX* ix1 + round(MajorDeltaX* (ix2-1)/NumSubMajor),Bottom  div 2 );
            LineTo(Left+ MajorDeltaX* ix1 + round(MajorDeltaX* (ix2-1)/NumSubMajor),Bottom  );
        end;
      end;
      MoveTo(Left,Top + (Bottom - Top) div 2 );
      LineTo(Right,Top + (Bottom - Top) div 2 );
    end;
  end;
end;
  { ****************** }
procedure DrawRGridCell(Canv : TCanvas; NRow: Integer;Rect: TRect;
          LGrid : TStringGrid);
var MyRect : TRect;
    ShibMemb : longint;
    ix1,ix2:longint;
    MisRishui : shortstring;
    LineWidth : longint;
    HelpStr : shortstring;
begin
  with Canv do
  begin
    if NRow = 0 then
      DrawRGridHeader(Canv,Rect);
    if NRow <> 0 then
    with ShibList do
    begin
      LineWidth:=Rect.Right - Rect.Left;
      MisRishui:=LGrid.cells[0,NRow];
      ShibMemb:=FindFirstShib(MisRishui);
      if ShibMemb <> (-1) then
      for ix1:= ShibMemb to (Count -1) do
      begin
        ShibObject:=Items[ix1];
        with ShibObject do
        begin
          if LNo_Rishui <> MisRishui then
           break;
          Pen.Color:=clBlack;
          if pos(TextOfEmpty,LNo_Rishui) <> 0 then
            Brush.Color:=clYellow
          else
            Brush.Color:=clGreen;
          with MyRect do
          begin
            Left:={Rect.Left+}round(ShibObject.LBegPoint * LineWidth);
            Right:={Rect.Left+}round(ShibObject.LEndPoint * LineWidth);
            Top:=Rect.Top+round((Rect.Bottom - Rect.Top)/DrawRectWidth);
            Bottom:=Rect.Bottom-round((Rect.Bottom - Rect.Top)/DrawRectWidth);
            Rectangle(Left,Top,Right,Bottom);
          end;
        end;
      end;
          {**** OverLapping ********}
      for ix1:= 0 to (Count -1) do
        for ix2 :=(ix1+1) to (Count - 1) do
        if OverLap(ix1,ix2) and
          (TShibObject(Items[ix1]).LNo_Rishui = TShibObject(Items[ix2]).LNo_Rishui) and
          (MisRishui =TShibObject(Items[ix1]).LNo_Rishui) then
        begin
          LinePair:=FindOverLapLine(ix1,ix2);
          with MyRect do
          begin
            Left:=Rect.Left+round(LinePair.XBeg * LineWidth);
            Right:=Rect.Left+round(LinePair.XEnd * LineWidth);
            Top:=Rect.Top+round((Rect.Bottom - Rect.Top)/DrawRectWidth);
            Bottom:=Rect.Bottom-round((Rect.Bottom - Rect.Top)/DrawRectWidth);
            Brush.Color:=clRed;
            Rectangle(Left,Top,Right,Bottom);
          end;
        end;
      end;
  end;
end;
    { **************** }
procedure DefPlaceOfMouse(X,Y:integer;LGrid : TStringGrid);
begin
  XRel:=X / LGrid.Width;
  with LGrid do
  begin
    ARow:= MouseCoord(X,Y).Y;
    ACol:= MouseCoord(X,Y).X;
  end;
end;
  { **************** }
function FoundListItems:boolean;
var
    ix1 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    LineWidth : longint;
begin
      Result:=FALSE;
      CarItemFound:=-1;
      ShibItemFound:=-1;
      CarObject:=CarList.Items[ARow-1];
      with CarObject do
      begin
        MisRishui:=LNo_Rishui;
        ShibMemb:=ShibList.FindFirstShib(MisRishui);
        if ShibMemb <> (-1) then
        for ix1:=  ShibMemb to (ShibList.Count -1) do
        begin
          ShibObject:=ShibList.Items[ix1];
          with ShibObject do
          if LNo_Rishui = MisRishui then
          begin
            if (LBegPoint <= XRel) and
               (LEndPoint >= XRel) then
            begin
              CarItemFound:=ARow-1;
              ShibItemFound:=ix1;
              Result:=TRUE;
              break;
            end; {XRel inside}
          end; {ShibObj}
        end; {loop ix1}
      end; {CarObj}
end;
  { ****************** }
procedure StartDraggingShib(LGrid : TStringGrid;X,Y:integer);
var
    ix1 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    LineWidth : longint;
begin
    with LGrid do
    begin
      CarObject:=CarList.Items[ARow-1];
      with CarObject do
      begin
        MisRishui:=LNo_Rishui;
        ShibMemb:=ShibList.FindFirstShib(MisRishui);
        if ShibMemb <> (-1) then
        for ix1:=  ShibMemb to (ShibList.Count -1) do
        begin
          ShibObject:=ShibList.Items[ix1];
          with ShibObject do
          if LNo_Rishui = MisRishui then
          begin
            if (LBegPoint <= XRel) and
               (LEndPoint >= XRel) then
            begin
              InDragging:=TRUE;
              CarMembStartDrag:=ARow-1;
              XStart:=X;YStart:=Y;
              {
              if (XRel - LBegPoint) < (LEndPoint-LBegPoint)*0.25 then
              begin
                DraggingType:=(-1);
                Screen.Cursor:=crSizeWE;
              end
              else
              }
                if (XRel - LBegPoint) > (LEndPoint-LBegPoint)*0.75 then
                begin
                  DraggingType:=1;
                  Screen.Cursor:=crSizeWE;
                end
                else
                begin
                  DraggingType:=0;
                  Screen.Cursor:=crSize;
                end;
              ShibMembStartDrag:=ix1;
              MyRect:=CellRect(ACol,ARow);
              with MyRect do
              begin
                LineWidth:=Right - Left;
                DrawRect.Left:=Left+round(LBegPoint * LineWidth);
                DrawRect.Right:=Left+round(LEndPoint * LineWidth);
                DrawRect.Top:=Top+round((Bottom - Top)/DrawRectWidth);
                DrawRect.Bottom:=Bottom-round((Bottom - Top)/DrawRectWidth);
              end;
              Canvas.DrawFocusRect(DrawRect);
              break;
            end; {XRel inside}
          end; {ShibObj}
        end; {loop ix1}
      end; {CarObj}
    end; {LGrid}
end;
  { ****************** }
procedure EndDraggingShib(LGrid : TStringGrid;X,Y:integer);
var
    ix1 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    DeltaOld,HelpReal : real;
    BegCarEmpty : boolean;
begin
    InDragging:=FALSE;
    BegCarEmpty:=FALSE;
    with LGrid do
    begin
        CarObject:=CarList.Items[ARow-1];
        with CarObject do
        begin
          MisRishui:=LNo_Rishui;
          ShibObject:=ShibList.Items[ShibMembStartDrag];
          with ShibObject do
          begin
            Screen.Cursor:=crDefault;
            if ((ARow-1) = CarMembStartDrag) then
            begin
              CarMembEndDrag:=ARow-1;
              if abs(XStart - X) > 3 then
              begin
                if DraggingType=(-1) then  {Left}
                  LBegPoint := XRel;
                if DraggingType=(+1) then  {Right}
                  LEndPoint := XRel;
                if DraggingType=0 then  {Center}
                begin
                  DeltaOld:=LEndPoint-LBegPoint;
                  LBegPoint := XRel - DeltaOld/2 ;
                  LEndPoint := XRel + DeltaOld/2;
                end;
                if LBegPoint > LEndPoint then
                begin
                  HelpReal:=LEndPoint;
                  LEndPoint:=LBegPoint;
                  LBegPoint:=HelpReal;
                end;
                LBegTime:= BegPoint + LBegPoint /(EndPoint - BegPoint);
                LEndTime:= BegPoint + LEndPoint /(EndPoint - BegPoint);
              end;
            end
            else
            if (pos(TextOfEmpty,
                 TCarObject(CarList.Items[ARow-1]).LNo_Rishui) = 0 ) then
            Begin
              LNo_Rishui:=CarObject.LNo_Rishui;
              LCarNo:=CarObject.LNo_Kod;
              LShem_Nehag:=CarObject.LShem_Nehag;
              BegCarEmpty:= (pos(TextOfEmpty,
                 TCarObject(CarList.Items[CarMembStartDrag]).LNo_Rishui) <> 0 );
            end;
            L_Status:=1;
            ShibList.Sort(CompareShibObject);
            InitGrids;
          end;
        end;
    end;
    if BegCarEmpty then
      DelCarEmpty(CarMembStartDrag);
end;
  { ******************* }
procedure DelCarEmpty(CarItem:longint);
begin
  if CarItem <> (-1) then
  begin
    CarObject:=CarList.Items[CarItem];
    CarObject.Free;
    CarList.Delete(CarItem);
    InitGrids;
  end;
end;
  { **************** }
procedure ActionForShibItem(LGrid : TStringGrid;CarItem,ShibItem:longint);
begin
  if (CarItem <> (-1)) and (ShibItem <> (-1)) then
  begin
    CarObject:=CarList.Items[CarItem];
    ShibObject:=ShibList.Items[ShibItem];

    LGrid.Hint:=GetInform(CarItem,ShibItem);
    with ShibObject do       {Dima 29/03/99}
    begin
      {
      if (XRel - LBegPoint) < (LEndPoint-LBegPoint)*0.25 then
        Screen.Cursor:=crSizeWE
      else
      }
        if (XRel - LBegPoint) > (LEndPoint-LBegPoint)*0.75 then
           Screen.Cursor:=crSizeWE
        else
           Screen.Cursor:=crSize;
    end;
  end;
end;
  { ******************* }
procedure ContinueDraggingShib(LGrid : TStringGrid;X,Y:integer);
var
    ix1,ix2 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    LineWidth : longint;
begin
  with LGrid do
  begin
    CarObject:=CarList.Items[ARow-1];
    MisRishui:=CarObject.LNo_Rishui;
    ShibObject:=ShibList.Items[ShibMembStartDrag];
    Canvas.DrawFocusRect(DrawRect);
    MyRect:=CellRect(ACol,ARow);
    with MyRect do
    begin
      if ShibObject.LNo_Rishui = MisRishui then
      begin
        LineWidth:=Right - Left;
        (*
        if DraggingType=(-1) then  {Left}
           DrawRect.Left:=Left+round(XRel * LineWidth);
        *)
        if DraggingType=(+1) then  {Right}
          DrawRect.Right:=Left+round(XRel * LineWidth);
        if DraggingType=(0) then  {Center}
        begin
          DrawRect.Left:=Left+
              round((ShibObject.LBegPoint + (X-XStart)/LineWidth) * LineWidth);
          DrawRect.Right:=Left+
              round((ShibObject.LEndPoint + (X-XStart)/LineWidth) * LineWidth);
        end;
        DrawRect.Top:=Top+round((Bottom - Top)/DrawRectWidth);
        DrawRect.Bottom:=Bottom-round((Bottom - Top)/DrawRectWidth);
      end;
    end;
    Canvas.DrawFocusRect(DrawRect);
  end;
end;
  { ***************** }
function ShibOverLaps(ShibItemFrom,CarItem : longint):boolean;
var
    ShibMemb : longint;
    ix1:longint;
    MisRishui : shortstring;
begin
  Result:=FALSE;
  if ((ShibItemFrom <> (-1)) and (CarItem <> (-1))) then
  with ShibList do
  begin
    MisRishui:=TCarObject(CarList.Items[CarItem]).LNo_Rishui;
    ShibMemb:=FindFirstShib(MisRishui);
    if ShibMemb <> (-1) then
    for ix1:= ShibMemb to (Count -1) do
     if OverLap(ShibItemFrom,ix1) and
        (ShibItemFrom <> ix1) and
        (MisRishui <> TShibObject(Items[ShibItemFrom]).LNo_Rishui) and
        (MisRishui =TShibObject(Items[ix1]).LNo_Rishui) then
        begin
          Result:=TRUE;
          Break;
        end;
  end;
end;
  { ***************** }
procedure RefreshGraf(InRecNo:LongInt);
 begin
   with AtmdataModule.QrySadrnDbGrid do
     begin
       DisableControls;
       DoneLists;
       InitDragging;
       InitLists;
       InitGrids;
       EnableControls;
       MoveBy(InRecNo - RecNo);
     end;
 end;
  { ******************** }
end.

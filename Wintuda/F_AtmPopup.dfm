object Frm_AtmPopup: TFrm_AtmPopup
  Left = 113
  Top = 151
  Width = 435
  Height = 246
  BiDiMode = bdRightToLeft
  Caption = '������'
  Color = clBtnFace
  Font.Charset = HEBREW_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object HebLabel1: TLabel
    Left = 360
    Top = 7
    Width = 63
    Height = 13
    Caption = '���� �����'
  end
  object Label1: TLabel
    Left = 337
    Top = 31
    Width = 88
    Height = 13
    Caption = '�� ���� ������'
  end
  object HebLabel23: TLabel
    Left = 252
    Top = 7
    Width = 33
    Height = 13
    Caption = '�����'
  end
  object Label2: TLabel
    Left = 0
    Top = 55
    Width = 427
    Height = 13
    Align = alBottom
    Caption = '�����'
  end
  object Kod: TAtmDbHEdit
    Tag = 1
    Left = 296
    Top = 4
    Width = 59
    Height = 21
    BiDiMode = bdRightToLeft
    DataField = 'RecNum'
    DataSource = Ds_AtmPopup
    ParentBiDiMode = False
    TabOrder = 0
    UseF2ToRunSearch = True
    IsFixed = False
    IsMust = True
    DefaultKind = dkString
    KeyToRunSearch = ksF2
    LinkLabel = HebLabel1
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    FixedColor = clBtnFace
    NormalColor = clWindow
    SelectNextAfterSearch = True
    StatusBarPanelNum = -1
    Hebrew = False
    LocateRecordOnChange = True
  end
  object AtmDbHEdit1: TAtmDbHEdit
    Tag = 1
    Left = 176
    Top = 28
    Width = 156
    Height = 21
    BiDiMode = bdRightToLeft
    DataField = 'MassageAuthor'
    DataSource = Ds_AtmPopup
    ParentBiDiMode = False
    TabOrder = 2
    UseF2ToRunSearch = True
    IsFixed = False
    IsMust = True
    DefaultKind = dkString
    KeyToRunSearch = ksF2
    LinkLabel = Label1
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    FixedColor = clBtnFace
    NormalColor = clWindow
    SelectNextAfterSearch = True
    StatusBarPanelNum = -1
    Hebrew = True
    LocateRecordOnChange = True
  end
  object AtmDBDateEdit1: TAtmDBDateEdit
    Left = 146
    Top = 4
    Width = 102
    Height = 21
    DataField = 'MessageDate'
    DataSource = Ds_AtmPopup
    CheckOnExit = True
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    NumGlyphs = 2
    TabOrder = 1
    StartOfWeek = Sun
    Weekends = [Sat]
    YearDigits = dyFour
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    IsFixed = False
    FixedColor = clBtnFace
    StatusBarPanelNum = -1
  end
  object AtmDbHMemo1: TAtmDbHMemo
    Left = 0
    Top = 68
    Width = 427
    Height = 108
    Align = alBottom
    BiDiMode = bdRightToLeft
    DataField = 'MessageBody'
    DataSource = Ds_AtmPopup
    ParentBiDiMode = False
    TabOrder = 3
    IsFixed = False
    IsMust = False
    LinkLabel = Label2
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    Hebrew = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 200
    Width = 427
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SimplePanel = False
    SimpleText = 
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9 +
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9 +
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 176
    Width = 427
    Height = 24
    Align = alBottom
    AutoSize = True
    ButtonWidth = 27
    Flat = True
    Images = DM_AtmCrt.ImageList_CrtButtons
    List = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    Transparent = True
    object DBNavigator1: TAtmDbNavigator
      Left = 0
      Top = 0
      Width = 270
      Height = 22
      VisibleBtns.nbBkSet = False
      VisibleBtns.nbBkGoto = False
      VisibleBtns.nbBkClear = False
      VisibleBtns.nbPriorSet = False
      VisibleBtns.nbNextSet = False
      DataSource = Ds_AtmPopup
      Flat = True
      TabOrder = 0
    end
    object ToolButton1: TToolButton
      Left = 270
      Top = 0
      Caption = 'ToolButton1'
      ImageIndex = 10
      OnClick = ToolButton1Click
    end
  end
  object UpdateSQL_AtmPopup: TUpdateSQL
    ModifySQL.Strings = (
      'update AtmPopup'
      'set'
      '  MessageDate = :MessageDate,'
      '  MassageAuthor = :MassageAuthor,'
      '  MessageBody = :MessageBody'
      'where'
      '  RecNum = :OLD_RecNum')
    InsertSQL.Strings = (
      'insert into AtmPopup'
      '  (MessageDate, MassageAuthor, MessageBody)'
      'values'
      '  (:MessageDate, :MassageAuthor, :MessageBody)')
    DeleteSQL.Strings = (
      'delete from AtmPopup'
      'where'
      '  RecNum = :OLD_RecNum')
    Left = 16
    Top = 14
  end
  object Tbl_AtmPopup: TAtmRxQuery
    Tag = 1
    CachedUpdates = True
    AutoRefresh = True
    DatabaseName = 'DB_AtmCrt'
    SQL.Strings = (
      'Select * '
      'From AtmPopup'
      'Where %MWhereIndex'
      'Order By %OrderFields')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_AtmPopup
    Macros = <
      item
        DataType = ftString
        Name = 'MWhereIndex'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'OrderFields'
        ParamType = ptInput
        Value = 'RecNum'
      end>
    IndexFieldNames = 'RecNum'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = True
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = True
    Left = 18
    Top = 44
  end
  object Ds_AtmPopup: TDataSource
    DataSet = Tbl_AtmPopup
    Left = 16
    Top = 71
  end
end

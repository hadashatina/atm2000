SELECT 
  KB.SHEMLAKOACH AS SHEM_LAKOACH, K.KODBITZUA, sum(schom) schom,
  CAST('' AS DATETIME) As tarichperaon, misparkabala,
  misparhafkada, tarichkabala, KODMATBEA, CODESUGTASHLUM
FROM 
  KOPA K, KABALA KB
where
 K.kodbitzua = 0 and
 K.MISPARKABALA > 0 AND
 KB.KABALANUMBER = K.MISPARKABALA AND
 TARICHKABALA BETWEEN '01/01/2001' AND '03/01/2001' 
 

AND KODMATBEA = 0
AND CODESUGTASHLUM < 100
 Group by KB.SHEMLAKOACH, K.KODBITZUA, misparkabala, misparhafkada, tarichkabala, KODMATBEA, CODESUGTASHLUM
 HAVING SUM(SCHOM) <> 0

UNION

SELECT 
  KB.SHEMLAKOACH AS SHEM_LAKOACH, K.KODBITZUA, cast(schom as float), 
  cast(tarichperaon as DATETIME) aS tarichperaon, misparkabala, 
  misparhafkada, tarichkabala, KODMATBEA, CODESUGTASHLUM
FROM KOPA K, KABALA KB
where 
 K.MISPARKABALA = KB.KABALANUMBER AND
 K.MISPARKABALA >= 0 AND 
 K.KODBITZUA = 1 AND
 TARICHKABALA BETWEEN '01/01/2001' AND '03/01/2001' 
And
(
 (Tarichperaon < tarichhafkada)
Or
 (Tarichperaon is Null) 
)

AND KODMATBEA = 0
AND CODESUGTASHLUM < 100

UNION

SELECT 
  cast('' as char(50)) SHEM_LAKOACH, KODBITZUA, 
  sum(schom), CAST('' AS DATETIME) As tarichperaon, misparkabala,
  misparhafkada, tarichkabala, KODMATBEA, CODESUGTASHLUM
FROM KOPA K
where 
 KODBITZUA = 1 and
 TARICHKABALA BETWEEN '01/01/2001' AND '03/01/2001'
 AND
 Tarichperaon <= tarichhafkada
 And 
Tarichperaon <>null

AND KODMATBEA = 0
AND CODESUGTASHLUM < 100
 Group by K.KODBITZUA, misparkabala, misparhafkada, tarichkabala, KODMATBEA, CODESUGTASHLUM
 HAVING SUM(SCHOM) <> 0
ORDER BY TARICHKABALA, K.KODBITZUA, MISPARHAFKADA, MISPARKABALA, SCHOM

unit F_UpdPriceFromMehiron;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ToolEdit, Spin, ExtCtrls, ComCtrls,DB, Grids,
  DBGrids;

type
  TFrm_UpdatePriceFromMehiron = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    RG_PriceFieldName: TRadioGroup;
    RG_SumType: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    SpinEdit_FromLak: TSpinEdit;
    SpinEdit_ToLak: TSpinEdit;
    SpinEdit_FromNehag: TSpinEdit;
    SpinEdit_ToNehag: TSpinEdit;
    SpinEdit_FromMetan: TSpinEdit;
    SpinEdit_ToMetan: TSpinEdit;
    SpinEdit_FromMaslul: TSpinEdit;
    SpinEdit_ToMaslul: TSpinEdit;
    SpinEdit_FromTnua: TSpinEdit;
    SpinEdit_ToTnua: TSpinEdit;
    DateEdit_ToDate: TDateEdit;
    DateEdit_FromDate: TDateEdit;
    Panel2: TPanel;
    Btn_StartUPdate: TBitBtn;
    Btn_Close: TBitBtn;
    Label13: TLabel;
    Label14: TLabel;
    SpinEdit_FromAzmn: TSpinEdit;
    SpinEdit_ToAzmn: TSpinEdit;
    ProgressBar1: TProgressBar;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Btn_StartUPdateClick(Sender: TObject);
  private
    { Private declarations }
    OldShibQry :TStringList;
    OldWinTudaSqlMode:LongInt;
    procedure ChangeShibQry;
    Procedure PutParamsInSql;
  public
    { Public declarations }
  end;

var
  Frm_UpdatePriceFromMehiron: TFrm_UpdatePriceFromMehiron;

implementation

uses DMWinTuda, Frm_WinTuda, AtmConst, F_GlblWinTuda;

{$R *.DFM}

procedure TFrm_UpdatePriceFromMehiron.ChangeShibQry;
Begin
  With DM_WinTuda.Tbl_Shib Do
  Begin
    Close;
    Filtered:=False;
    Sql.Clear;
    Sql.Add('Select * From '+WinTudaAtmShibFileName);
    Sql.Add('Where');
    Sql.Add('LakNo1 Between :PFromLak And :PToLak');
    Sql.Add('And DriverNo1 Between :PFromDriver And :PToDriver');
    Sql.Add('And SugMetan Between :PFromMetan And :PToMetan');
    Sql.Add('And MaslulCode1 Between :PFromMaslul And :PToMaslul');
    Sql.Add('And ShibNo Between :PFromTnua And :PToTnua');
    Sql.Add('And ShibAzmnDate Between :PFromDate And :PToDate');
    Sql.Add('And (LakHesbonit = 0 Or LakHesbonit Is NULL)');
    Sql.Add('And ShibAzmnNo Between :PFromAzmn And :PToAzmn');
    Sql.Add('And ShibKind > -1');
    Sql.Add('And PriceIsGlobal = 0');
    Sql.Add('And %MPriceFieldName %MPriceCondition');
    Sql.Add('Order By %OrderFields');
  End; //With
End;

procedure TFrm_UpdatePriceFromMehiron.FormCreate(Sender: TObject);
begin
  ProgressBar1.Parent:=StatusBar1;
  ProgressBar1.Top:=2;
  ProgressBar1.Width:=StatusBar1.Panels[0].Width;
  ProgressBar1.Height:=StatusBar1.Height-2;
  OldShibQry:=TStringList.Create;
  OldShibQry.AddStrings(DM_WinTuda.Tbl_Shib.SQL);
  DM_WinTuda.Tbl_ShibShibAzmnDate.OnValidate:=Nil;
  OldWinTudaSqlMode:=WinTudaSqlMode;
  WinTudaSqlMode:=9;
end;

procedure TFrm_UpdatePriceFromMehiron.FormDestroy(Sender: TObject);
begin
  WinTudaSqlMode:=OldWinTudaSqlMode;
  DM_WinTuda.Tbl_ShibShibAzmnDate.OnValidate:=DM_WinTuda.Tbl_ShibShibAzmnDateValidate;
  DM_WinTuda.Tbl_Shib.Close;
  DM_WinTuda.Tbl_Shib.SQL.Clear;
  DM_WinTuda.Tbl_Shib.SQL.AddStrings(OldShibQry);
  if WinTudaSqlMode=0 Then
    DM_WinTuda.Tbl_Shib.MacroByName('MasterField').AsString:=''
  Else
    if WinTudaSqlMode=1 Then
      DM_WinTuda.Tbl_Shib.MacroByName('MasterField').AsString:='ShibAzmnNo=1 And ';
    DM_WinTuda.Tbl_Shib.MacroByName('TableName').AsString:=WinTudaAtmShibFileName;
//  DM_WinTuda.Tbl_Shib.Open;
  DM_WinTuda.RefreshMainQuerys;
end;

Procedure TFrm_UpdatePriceFromMehiron.PutParamsInSql;
Var
  PriceType :LongInt;
  DateH :TDateTime;
Begin
  Try
    Screen.Cursor:=crSQLWait;

    PriceType :=RG_PriceFieldName.ItemIndex+1;
    if PriceType > 4 Then PriceType:=1;
    With DM_WinTuda.Tbl_Shib  Do
    Begin
      MacroByName('MPriceFieldName').AsString:='Price'+IntToStr(PriceType);

      ParamByName('PFromLak').AsInteger:=SpinEdit_FromLak.Value;
      ParamByName('PToLak').AsInteger:=SpinEdit_ToLak.Value;

      ParamByName('PFromDriver').AsInteger:=SpinEdit_FromNehag.Value;
      ParamByName('PToDriver').Asinteger:=SpinEdit_ToNehag.Value;

      ParamByName('PFromMetan').AsInteger:=SpinEdit_FromMetan.Value;
      ParamByName('PToMetan').AsInteger:=SpinEdit_ToMetan.Value;

      ParamByName('PFromMaslul').AsInteger:=SpinEdit_FromMaslul.Value;
      ParamByName('PToMaslul').AsInteger:=SpinEdit_ToMaslul.Value;

      ParamByName('PFromTnua').AsInteger:=SpinEdit_FromTnua.Value;
      ParamByName('PToTnua').Asinteger:=SpinEdit_ToTnua.Value;

      ParamByName('PFromAzmn').AsInteger:=SpinEdit_FromAzmn.Value;
      ParamByName('PToAzmn').Asinteger:=SpinEdit_ToAzmn.Value;

      DateH:=Trunc(DateEdit_FromDate.Date);
      ParamByName('PFromDate').AsDateTime:=DateH;
      DateH:=Trunc(DateEdit_ToDate.Date)+0.9999;
      ParamByName('PToDate').AsDateTime:=DateH;

      Case RG_SumType.ItemIndex Of
        0:MacroByName('MPriceCondition').AsString:='<>0';
        1:MacroByName('MPriceCondition').AsString:='=0';
        2:MacroByName('MPriceCondition').AsString:='Is Not NULL';
      End;
    End;
  Finally
    Screen.Cursor:=crDefault;
  End;
End;
procedure TFrm_UpdatePriceFromMehiron.Btn_StartUPdateClick(
  Sender: TObject);
Var
  CurBM :TBookmark;
begin
  Screen.Cursor:=crSQLWait;
  Btn_StartUPdate.Enabled:=False;
  Btn_Close.Enabled:=False;
  Try
    ChangeShibQry;
    PutParamsInSql;
    DM_WinTuda.Tbl_Shib.Open;
    DM_WinTuda.Tbl_Shib.Last;
    DM_WinTuda.Tbl_Shib.First;
    ProgressBar1.Max:=DM_WinTuda.Tbl_Shib.RecordCount+1;
    ProgressBar1.Position:=0;
    While Not DM_WinTuda.Tbl_Shib.Eof Do
    Begin
      Screen.Cursor:=crSQLWait;
      ProgressBar1.StepIt;
      StatusBar1.Panels[1].Text:='����� '+DM_WinTuda.Tbl_Shib.FieldByName('ShibAzmnNo').AsString;
      Application.ProcessMessages;
      if DM_WinTuda.Tbl_Shib.FieldByName('PriceIsGlobal').AsInteger=0 Then
      Begin
        CurBM:=DM_WinTuda.Tbl_Shib.GetBookmark; //����� ������ �������
        DM_WinTuda.Tbl_Azmn.FindKey([DM_WinTuda.Tbl_Shib.FieldByName('ShibAzmnNo').AsInteger]);
        //����� �� ������� ����� �"� �������
        With DM_WinTuda.RxQuery_Temp Do
        Begin
          Close;
          Sql.Clear;
          Sql.Add('Update AtmShib');
          Sql.Add('Set ShibKind=-1');
          Sql.Add('Where ShibAzmnNo = '+DM_WinTuda.Tbl_Shib.FieldByName('ShibAzmnNo').AsString); //���� �� ����� �������������� ����
          Sql.Add('And PriceIsGlobal = '+DM_WinTuda.Tbl_Shib.FieldByName('ShibNo').AsString);
          ExecSQL;
        End;

        Case RG_PriceFieldName.ItemIndex Of
          0: FrmWinTuda.PutPriceInField(poPrice1,-1,'Price1');
          1: FrmWinTuda.PutPriceInField(poPrice2,-1,'Price2');
          2: FrmWinTuda.PutPriceInField(poPrice3,-1,'Price3');
          3: FrmWinTuda.PutPriceInField(poPrice4,-1,'Price4');
          4: FrmWinTuda.PutPriceInField(poAllPrices,-1);
        End; //Case

        if DM_WinTuda.Tbl_Shib.State<>dsBrowse Then
        Begin
          DM_WinTuda.Tbl_Shib.SavePressed:=True;
          DM_WinTuda.Tbl_Shib.Post;
        End;

        DM_WinTuda.Tbl_Shib.GotoBookmark(CurBM);
        DM_WinTuda.Tbl_Shib.Next;
      End
      Else
        DM_WinTuda.Tbl_Shib.Next;
    End;
  Finally;
    DM_WinTuda.Tbl_Shib.Close;
    Btn_StartUPdate.Enabled:=True;
    Btn_Close.Enabled:=True;
    Screen.Cursor:=crDefault;
    ProgressBar1.Position:=0;
    StatusBar1.Panels[1].Text:='';
  End;
end;

end.

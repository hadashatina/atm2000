unit F_ParamsMoatzot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,IniFiles;

type
  TFrm_ParamsMoatzot = class(TForm)
    SemelRashut: TEdit;
    Lbl_SemelRashut: TLabel;
    AgeYoung: TEdit;
    Lbl_AgeYoung: TLabel;
    KMMezakeYoung: TEdit;
    Lbl_KmMezakeYoung: TLabel;
    KMMezakeAd: TEdit;
    Lbl_KMMezakeAd: TLabel;
    RatioAdYo: TEdit;
    Label1: TLabel;
    Panel1: TPanel;
    Btn_Ok: TBitBtn;
    BitBtn2: TBitBtn;
    CB_ReportBitzua: TCheckBox;
    Label2: TLabel;
    PercentHishtatfutHinuch: TEdit;
    procedure Btn_OkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    IniFileName:String;
    Procedure FillVcl;
    Procedure PutValueIntoKodTavla(TheCode:LongInt;TheValue:String);
  public
    { Public declarations }
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_ParamsMoatzot: TFrm_ParamsMoatzot;

implementation

uses AtmConst, DMAtmCrt;

{$R *.DFM}

procedure TFrm_ParamsMoatzot.Btn_OkClick(Sender: TObject);
Var
   F :TIniFile;
   I :LongInt;
begin
     F:=TIniFile.Create(IniFileName);
     Try
         For I:=0 To ComponentCount-1 Do
         Begin
             if Components[I] is TEdit Then
                F.WriteString(SectionForMoatzot,Components[i].Name,TEdit(Components[I]).Text);
             if Components[I] is TCheckBox Then
                F.WriteBool(SectionForMoatzot,Components[i].Name,TCheckBox(Components[I]).Checked);
         End;
         PutValueIntoKodTavla(2,AgeYoung.Text);
         PutValueIntoKodTavla(3,KMMezakeYoung.Text);
         PutValueIntoKodTavla(4,KMMezakeAd.Text);
         PutValueIntoKodTavla(5,RatioAdYo.Text);
     Finally
            F.Free;
     End;
end;

Procedure TFrm_ParamsMoatzot.ShowForm(FormParams:ShortString);
Begin
     if FormParams<>'' Then
        IniFileName:=FormParams
     Else
         IniFileName:=ExtractFilePath(Application.ExeName)+Atm2000IniFileName;
     ShowModal;
End;

procedure TFrm_ParamsMoatzot.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_ParamsMoatzot:=Nil;
     Action:=caFree;
end;

procedure TFrm_ParamsMoatzot.FormShow(Sender: TObject);
Var
   F :TIniFile;
   SL :TStringList;
   I :longInt;
begin
     F:=TIniFile.Create(IniFileName);
     SL:=TStringList.Create;
     Try
         F.ReadSectionValues(SectionForMoatzot,Sl);
         For I:=0 To Sl.Count-1 Do
             if FindComponent(Sl.Names[I])<>Nil Then
             Begin
                if FindComponent(Sl.Names[I]) is TEdit Then
                   TEdit(FindComponent(Sl.Names[I])).Text:=Sl.Values[Sl.Names[I]];

                if FindComponent(Sl.Names[I]) is TCheckBox Then
                   TCheckBox(FindComponent(Sl.Names[I])).Checked:=Sl.Values[Sl.Names[I]]='1';
             End;
         FillVcl; //After The For
     Finally
            F.Free;
            Sl.Free;
     End;
end;

Procedure TFrm_ParamsMoatzot.FillVcl;
Begin
  With DM_AtmCrt,Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select * From KODTAVLA Where Sug_Tavla='+IntToStr(SugTavla_AchuzHishtatfut));
    Open;
    First;
    While Not Eof Do
    Begin
      Case FieldByName('Kod_Tavla').AsInteger Of
        2:AgeYoung.Text:=FieldByName('Teur_Tavla').AsString; //��� ������
        3:KMMezakeYoung.Text:=FieldByName('Teur_Tavla').AsString;//�"� ���� ������
        4:KMMezakeAd.Text:=FieldByName('Teur_Tavla').AsString;//�"� ���� ������
        5:RatioAdYo.Text:=FieldByName('Teur_Tavla').AsString;//��� ������/������
      End; //Case;
      Next;
    End;
    Close;
  End;
End;

Procedure TFrm_ParamsMoatzot.PutValueIntoKodTavla(TheCode:LongInt;TheValue:String);
Begin
  With DM_AtmCrt,Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select * From KODTAVLA Where Sug_Tavla='+IntToStr(SugTavla_AchuzHishtatfut));
    Sql.Add('And Kod_Tavla='+IntToStr(TheCode));
    Open;
    if Eof And BOF Then
    Begin //New
      Close;
      Sql.Add('Insert Into KODTAVLA');
      Sql.Add('(SUG_TAVLA,KOD_TAVLA,TEUR_TAVLA)');
      Sql.Add('Values');
      Sql.Add(IntToStr(SugTavla_AchuzHishtatfut)+','+IntToStr(TheCode)+','+TheValue);
      ExecSQL;
    End
    Else
    Begin
      Close;
      Sql.Add('Update KODTAVLA');
      Sql.Add('Set TEUR_TAVLA='+TheValue);
      Sql.Add('Where Sug_Tavla='+IntToStr(SugTavla_AchuzHishtatfut));
      Sql.Add('And Kod_Tavla='+IntToStr(TheCode));
      ExecSQL;
    End;
  End;
End;

end.

unit F_MakavHeshbon;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, ToolWin, ComCtrls, DBTables, Mask, ExtCtrls,
  AtmAdvTable, AtmComp, Scale250, AtmLookCombo, AdvSearch, Grids, DBGrids,
  RXDBCtrl, ToolEdit, DBFilter, RxQuery, AtmTabSheetBuild, AtmDBDateEdit,
  Dialogs, Menus, AtmRxQuery;

type
  TFrm_MakavHeshbon = class(TForm)
    Tbl_MakavHesbonitNumber: TIntegerField;
    Tbl_MakavCodeLakoach: TIntegerField;
    Tbl_MakavLakoachGroup: TIntegerField;
    Tbl_MakavHesbonitDate: TDateTimeField;
    Tbl_MakavHesbonitKind: TIntegerField;
    Tbl_MakavMerakez: TStringField;
    Tbl_MakavTotalHesbonitWithMAM: TBCDField;
    Tbl_MakavMAMHesbonit: TBCDField;
    Tbl_MakavMAMPercent: TBCDField;
    Tbl_MakavMAMRound: TBCDField;
    Tbl_MakavDiscountWork: TBCDField;
    Tbl_MakavDiscountParts: TBCDField;
    Tbl_MakavAchuz_Anacha_Lakoach: TBCDField;
    Tbl_MakavAchuz_Anacha_Klali: TBCDField;
    Tbl_MakavPratimToHan: TStringField;
    Tbl_MakavCodeHaavaraToHan: TIntegerField;
    Tbl_MakavMaklidName: TStringField;
    Tbl_MakavDateHafakatHeshbonit: TDateTimeField;
    Tbl_MakavCodeMeasher: TIntegerField;
    Tbl_MakavSumPaied: TBCDField;
    Tbl_MakavHanForMam: TStringField;
    Tbl_MakavYehusMonth: TIntegerField;
    Tbl_MakavYehusYear: TIntegerField;
    Tbl_MakavPaymentCondition: TIntegerField;
    Tbl_MakavStatus: TIntegerField;
    Tbl_MakavNumberOfPayments: TIntegerField;
    Tbl_MakavDateForFirstPayment: TDateTimeField;
    Tbl_MakavOrderNum: TIntegerField;
    Tbl_MakavTotalSumFromTnua: TBCDField;
    Tbl_MakavTotalSumPriceKamut: TBCDField;
    Tbl_MakavDatePiraon: TDateTimeField;
    Tbl_MakavKvutzaLeChiyuv: TIntegerField;
    Tbl_MakavMisHaavaraToHan: TIntegerField;
    Tbl_MakavDate_Havara: TDateTimeField;
    Tbl_MakavTotal_Zikuy: TBCDField;
    Tbl_MakavDate_Zikuy: TDateTimeField;
    Tbl_MakavMis_Kabala1: TIntegerField;
    Tbl_MakavMis_Kabala2: TIntegerField;
    Tbl_MakavMis_Kabala3: TIntegerField;
    Tbl_MakavMis_Kabala4: TIntegerField;
    Tbl_MakavMis_Kabala5: TIntegerField;
    Tbl_MakavDate_Kabala1: TDateTimeField;
    Tbl_MakavDate_Kabala2: TDateTimeField;
    Tbl_MakavDate_Kabala3: TDateTimeField;
    Tbl_MakavDate_Kabala4: TDateTimeField;
    Tbl_MakavDate_Kabala5: TDateTimeField;
    Tbl_MakavLast_Update: TDateTimeField;
    Tbl_MakavCheckNum: TMemoField;
    DS_Makav: TDataSource;
    Tbl_Makav: TAtmRxQuery;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    TS_Zikuy: TTabSheet;
    Tbl_MakavHanForLak: TStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    Scale1: TScale;
    Tbl_Lako: TAtmTable;
    Tbl_LakoKod_Lakoach: TIntegerField;
    Tbl_LakoKod_Kvutza: TIntegerField;
    Tbl_LakoShem_Lakoach: TStringField;
    Tbl_MakavLookLakoachName: TStringField;
    AtmAdvSearch_Lako: TAtmAdvSearch;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    Tbl_MakavIncom_Type: TIntegerField;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    QRY_Grigs: TRxQuery;
    RxDBFilter1: TRxDBFilter;
    DS_Grids: TDataSource;
    ToolButton3: TToolButton;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    EditHesbonitNumber: TAtmDbHEdit;
    Label4: TLabel;
    Lbl_CodeLakoach: TLabel;
    EditCodeLakoach: TAtmDbHEdit;
    DBText1: TDBText;
    Label7: TLabel;
    EditTotalHesbonitWithMAM: TAtmDbHEdit;
    Label3: TLabel;
    EditLakoachGroup: TAtmDbHEdit;
    Label28: TLabel;
    EditDateForFirstPayment: TAtmDBDateEdit;
    Label25: TLabel;
    AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox;
    GroupBox4: TGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label27: TLabel;
    EditTotalSumFromTnua: TAtmDbHEdit;
    EditTotalSumPriceKamut: TAtmDbHEdit;
    EditMAMHesbonit: TAtmDbHEdit;
    EditMAMPercent: TAtmDbHEdit;
    EditMAMRound: TAtmDbHEdit;
    EditAchuz_Anacha_Lakoach: TAtmDbHEdit;
    EditAchuz_Anacha_Klali: TAtmDbHEdit;
    EditNumberOfPayments: TAtmDbHEdit;
    Tbl_MakavYitra: TCurrencyField;
    AtmDbHEdit2: TAtmDbHEdit;
    Label2: TLabel;
    Label48: TLabel;
    Label17: TLabel;
    EditLast_Update: TAtmDbHEdit;
    EditMaklidName: TAtmDbHEdit;
    Label23: TLabel;
    EditYehusMonth: TAtmDbHEdit;
    Label29: TLabel;
    EditOrderNum: TAtmDbHEdit;
    Label36: TLabel;
    EditTotal_Zikuy: TAtmDbHEdit;
    AtmDBDateEdit_HeshbonitDate: TAtmDBDateEdit;
    Tbl_LakoKod_Tnai_Tashlum: TIntegerField;
    Label37: TLabel;
    EditDate_Zikuy: TAtmDBDateEdit;
    Label38: TLabel;
    Label43: TLabel;
    EditMis_Kabala: TAtmDbHEdit;
    EditDate_Kabala: TAtmDBDateEdit;
    Panel_KodLak: TPanel;
    Label11: TLabel;
    Label_NameLak: TLabel;
    ComboEdit_CodeLak: TComboEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label15: TLabel;
    Label12: TLabel;
    EditMisHaavaraToHan: TAtmDbHEdit;
    EditCodeHaavaraToHan: TAtmDbHEdit;
    EditDate_Havara: TAtmDbHEdit;
    EditPratimToHan: TAtmDbHEdit;
    DBEdit_IncomType: TAtmDbHEdit;
    Panel3: TPanel;
    DBRadioGroup_HesbonitKind: TDBRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    Label32: TLabel;
    EditDatePiraon: TAtmDBDateEdit;
    Label18: TLabel;
    AtmDBDateEdit_DateHafaka: TAtmDBDateEdit;
    Label24: TLabel;
    EditYehusYear: TAtmDbHEdit;
    Tbl_MakavSug_Anacha_Amala: TIntegerField;
    Tbl_MakavShaarDolar: TBCDField;
    Tbl_MakavRemFactor: TStringField;
    Tbl_MakavRem1: TMemoField;
    Tbl_MakavTotalAmalaForNehag: TBCDField;
    Tbl_MakavTotalMasMakor: TBCDField;
    Tbl_MakavAchuzMasMakor: TBCDField;
    Tbl_MakavSugPrice: TIntegerField;
    Tbl_MakavKod_sapak: TIntegerField;
    Tbl_MakavSugCalcMasMakor: TIntegerField;
    Tbl_MakavInteger_1: TIntegerField;
    Tbl_MakavInteger_2: TIntegerField;
    Tbl_MakavInteger_3: TIntegerField;
    Tbl_MakavInteger_4: TIntegerField;
    Tbl_MakavInteger_5: TIntegerField;
    Tbl_MakavInteger_6: TIntegerField;
    Tbl_MakavInteger_7: TIntegerField;
    Tbl_MakavInteger_8: TIntegerField;
    Tbl_MakavInteger_9: TIntegerField;
    Tbl_MakavInteger_10: TIntegerField;
    Tbl_MakavBcd_1: TBCDField;
    Tbl_MakavBcd_2: TBCDField;
    Tbl_MakavBcd_3: TBCDField;
    Tbl_MakavBcd_4: TBCDField;
    Tbl_MakavBcd_5: TBCDField;
    Tbl_MakavBcd_6: TBCDField;
    Tbl_MakavBcd_7: TBCDField;
    Tbl_MakavBcd_8: TBCDField;
    Tbl_MakavBcd_9: TBCDField;
    Tbl_MakavBcd_10: TBCDField;
    Tbl_MakavString_1: TStringField;
    Tbl_MakavString_2: TStringField;
    Tbl_MakavString_3: TStringField;
    Tbl_MakavString_4: TStringField;
    Tbl_MakavString_5: TStringField;
    Tbl_MakavString_6: TStringField;
    Tbl_MakavString_7: TStringField;
    Tbl_MakavString_8: TStringField;
    Tbl_MakavString_9: TStringField;
    Tbl_MakavString_10: TStringField;
    Tbl_MakavDate_1: TDateTimeField;
    Tbl_MakavDate_2: TDateTimeField;
    Tbl_MakavDate_3: TDateTimeField;
    Tbl_MakavDate_4: TDateTimeField;
    Tbl_MakavDate_5: TDateTimeField;
    Tbl_MakavDate_6: TDateTimeField;
    Tbl_MakavDate_7: TDateTimeField;
    Tbl_MakavDate_8: TDateTimeField;
    Tbl_MakavDate_9: TDateTimeField;
    Tbl_MakavDate_10: TDateTimeField;
    Label5: TLabel;
    AtmDbHEdit_TotalMasMakor: TAtmDbHEdit;
    Label6: TLabel;
    AtmDbHEdit_HanForMasMakor: TAtmDbHEdit;
    PopupMenu1: TPopupMenu;
    NHeshbonitKindFilter1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    AtmRxQuery1: TAtmRxQuery;
    UpdateSQL_Makav: TUpdateSQL;
    TBtn_Filter: TToolButton;
    N2: TMenuItem;
    N1: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    Panel_HeshbonitKind: TPanel;
    N9: TMenuItem;
    N10: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MakavAfterInsert(DataSet: TDataSet);
    procedure Tbl_MakavAfterPost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Change(Sender: TObject);
    procedure EditCodeLakoachBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure ComboEdit_CodeLakButtonClick(Sender: TObject);
    procedure Lbl_CodeLakoachDblClick(Sender: TObject);
    procedure Tbl_MakavCalcFields(DataSet: TDataSet);
    procedure Tbl_MakavCodeLakoachValidate(Sender: TField);
    procedure DS_MakavStateChange(Sender: TObject);
    procedure RxDBGrid_MakavDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure AtmTabSheetBuild1GridDblClick(Sender: TObject;
      ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
    procedure EditHesbonitNumberBeforeAutoLocateRecord(Sender: TObject;
      var ContinueOperation: Boolean);
    procedure NHeshbonitKindFilter1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure DBRadioGroup_HesbonitKindChange(Sender: TObject);
  private
    { private declarations }
    CurSortFieldName :ShortString;
  public
    { public declarations }
    Procedure BuildLookupList;
  end;

var
  Frm_MakavHeshbon: TFrm_MakavHeshbon;

implementation
Uses DMAtmCrt,AtmConst,AtmRutin,Crt_Glbl;
{$R *.DFM}

procedure TFrm_MakavHeshbon.FormCreate(Sender: TObject);
Var
   I:LongInt;
   dd,mm,yy :Word;
Begin
  Try
     Screen.Cursor:=crHourGlass;
     DecodeDate(Now,yy,mm,dd);
     BuildLookupList;
     PageControl1.ActivePage:=TS_Main;
     Tbl_Makav.MacroByName('MWhereYear').AsString:='YEHUSYEAR = '+IntToStr(yy);
     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataset) And (Components[i].Tag=1)Then
             (Components[I] As TDataset).Open;
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');

{     AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
     AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;}
     AtmTabSheetBuild1.ScriptsDir:=CrtDirForScripts;
     AtmTabSheetBuild1.FilterDir:=CrtDirForCurUser;
     AtmTabSheetBuild1.BuildTabsForGrids;
     if FindComponent('TS_MakavLak')<>Nil Then
     Begin
       Panel_KodLak.Parent:=TWinControl(FindComponent('TS_MakavLak'));
       Panel_KodLak.Align:=alTop;
       Panel_KodLak.Visible:=True;
     End;
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_MakavHeshbon.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataset).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_MakavHeshbon.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_MakavHeshbon:=Nil;
     Action:=caFree;
end;

procedure TFrm_MakavHeshbon.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Not Visible Then Exit;
//     ChangeDSForNavigator(TDBNavigator(Sender));
     if Button=nbPost Then
       ((Sender As TDBNavigator).DataSource.DataSet As TAtmRxQuery).SavePressed:=True;
end;

procedure TFrm_MakavHeshbon.Tbl_MakavAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_MakavHeshbon.Tbl_MakavAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_MakavHeshbon.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(ActiveControl,Key,Shift,Tbl_Makav);
end;

Procedure TFrm_MakavHeshbon.BuildLookupList;
Var
   SList :TStringList;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_TnaeyTashlum),AtmDbComboBox_TnaeyTashlum);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;

procedure TFrm_MakavHeshbon.PageControl1Change(Sender: TObject);
begin
{  ComboEdit_CodeLak.Text:=Tbl_Makav.FieldByName('CodeLakoach').AsString;
  Label_NameLak.Caption:=Tbl_MakavLookLakoachName.AsString;
}
end;

procedure TFrm_MakavHeshbon.EditCodeLakoachBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     EditCodeLakoach.SearchComponent.ReturnFieldIndex:=0;
end;

procedure TFrm_MakavHeshbon.ComboEdit_CodeLakButtonClick(Sender: TObject);
begin
     AtmAdvSearch_Lako.ReturnFieldIndex:=-1;
     if AtmAdvSearch_Lako.Execute Then
     Begin
          ComboEdit_CodeLak.Text:=GetFieldFromSeparateString(AtmAdvSearch_Lako.ReturnString,AtmAdvSearch_Lako.SeparateChar,1);
          Label_NameLak.Caption:=GetFieldFromSeparateString(AtmAdvSearch_Lako.ReturnString,AtmAdvSearch_Lako.SeparateChar,3);
//          RunQryMakav(CurSortFieldName);
          if AtmTabSheetBuild1.Query.Active Then
            AtmTabSheetBuild1.RefreshQry(Nil);
     End;
end;

procedure TFrm_MakavHeshbon.Lbl_CodeLakoachDblClick(Sender: TObject);
begin
     OpenAtmCrt(fnLakoach,EditCodeLakoach.Text);
end;

procedure TFrm_MakavHeshbon.Tbl_MakavCalcFields(DataSet: TDataSet);
begin
  Tbl_MakavYitra.AsFloat:=Abs(Tbl_MakavTotalHesbonitWithMAM.AsFloat)-Tbl_MakavTotal_Zikuy.AsFloat;
end;

procedure TFrm_MakavHeshbon.Tbl_MakavCodeLakoachValidate(Sender: TField);
begin
     if Tbl_Lako.FindKey([Tbl_MakavCodeLakoach.Value]) Then
     Begin
        if Tbl_Makav.State=dsInsert Then
          Tbl_MakavPaymentCondition.AsInteger:=Tbl_LakoKod_Tnai_Tashlum.AsInteger;
     End
     ELse
     Begin
          MessageDlg('���� �� ����',mtError,[mbOk],0);
          Abort;
     End;
end;

procedure TFrm_MakavHeshbon.DS_MakavStateChange(Sender: TObject);
begin
  Case Tbl_Makav.State of
    dsInsert: Tbl_Makav.FieldByName('HesbonitKind').ReadOnly:=False;
    dsBrowse,dsEdit:Tbl_Makav.FieldByName('HesbonitKind').ReadOnly:=True;
  End;

end;

procedure TFrm_MakavHeshbon.RxDBGrid_MakavDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (gdFocused in State) And (TDbGrid(Sender).Canvas.Brush.Color=clInfoBk) Then
  Begin
    TDbGrid(Sender).Canvas.Brush.Color:=clNavy;
    TDbGrid(Sender).Canvas.FillRect(Rect);
    TDbGrid(Sender).Canvas.Font.Color:=clWhite;
    TDbGrid(Sender).Canvas.TextRect(Rect,Rect.Left+1,Rect.Top+1,Column.Field.AsString);
  End;
end;

procedure TFrm_MakavHeshbon.AtmTabSheetBuild1BeforeExecuteQuery(
  Sender: TObject);
begin
  if (AtmTabSheetBuild1.Query.Params.FindParam('PLakNum')<>Nil) And (Trim(ComboEdit_CodeLak.Text)<>'') Then
    AtmTabSheetBuild1.Query.ParamByName('PLakNum').AsInteger:=StrToInt(ComboEdit_CodeLak.Text);
end;

procedure TFrm_MakavHeshbon.AtmTabSheetBuild1GridDblClick(Sender: TObject;
  ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
begin
  if CompareText(ATabSheet.Name,'TS_MakavLak') =0 Then
  Begin
    Tbl_Makav.FindKey([AtmTabSheetBuild1.Query.FieldByName('HESBONITKIND').Value,
                       AtmTabSheetBuild1.Query.FieldByName('YEHUSYEAR').Value,
                       AtmTabSheetBuild1.Query.FieldByName('HesbonitNumber').Value]);
    PageControl1.ActivePage:=TS_Main;
  End;
end;

procedure TFrm_MakavHeshbon.EditHesbonitNumberBeforeAutoLocateRecord(
  Sender: TObject; var ContinueOperation: Boolean);
Var
  St1,St2,St3 :String;
begin
  St1:=Tbl_Makav.FieldByName('HesbonitKind').AsString;
  St2:=Tbl_Makav.FieldByName('YehusYear').AsString;
  St3:=EditHesbonitNumber.Text;
  Tbl_Makav.Cancel;
  Tbl_Makav.FindKey([St1,St2,St3]);
  ContinueOperation:=False;
end;

procedure TFrm_MakavHeshbon.NHeshbonitKindFilter1Click(Sender: TObject);
begin
  Tbl_Makav.Close;
  Tbl_Makav.MacroByName('MWhereHeshbonitKind').AsString:='HESBONITKIND = '+IntToStr(TComponent(Sender).Tag);
  Tbl_Makav.Open;
end;

procedure TFrm_MakavHeshbon.N2Click(Sender: TObject);
begin
  Tbl_Makav.Close;
  Tbl_Makav.MacroByName('MWhereHeshbonitKind').AsString:='0=0';
  Tbl_Makav.Open;
end;

procedure TFrm_MakavHeshbon.N7Click(Sender: TObject);
Var
  StrH :String;
  IntH :LongInt;
begin
  if InputQuery('��� �����','��� ��� �����',StrH) Then
  Begin
    Try
      IntH:=StrToInt(StrH);
      if (IntH<1900) Or (IntH>2100) Then
        Exit;
      Tbl_Makav.Close;
      Tbl_Makav.MacroByName('MWhereYear').AsString:='YEHUSYEAR = '+StrH;
      Tbl_Makav.Open;
    Except
      MessageDlg('��� �� �����',mtError,[mbOk],0);
    End;
  End;
end;

procedure TFrm_MakavHeshbon.N8Click(Sender: TObject);
begin
  Tbl_Makav.Close;
  Tbl_Makav.MacroByName('MWhereYear').AsString:='0=0';
  Tbl_Makav.Open;
end;

procedure TFrm_MakavHeshbon.DBRadioGroup_HesbonitKindChange(
  Sender: TObject);
begin
  if (DBRadioGroup_HesbonitKind.Items.Count>0) And (DBRadioGroup_HesbonitKind.ItemIndex>-1) Then
    Panel_HeshbonitKind.Caption:=DBRadioGroup_HesbonitKind.Items[DBRadioGroup_HesbonitKind.ItemIndex];
end;

end.


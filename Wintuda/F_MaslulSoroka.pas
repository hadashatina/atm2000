unit F_MaslulSoroka;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, DBTables, Mask, ExtCtrls, AtmComp, ComCtrls,
  AtmAdvTable, Scale250, ToolWin, Menus, GIFImage,Registry, Buttons,
  ToolEdit, AtmLookCombo, ActnList,MslDllInterface, AdvSearch,
  DynamicControls, DBFilter, RxQuery, AtmTabSheetBuild, Grids, DBGrids,
  RXDBCtrl;

type
  TFrm_MaslulSoroka = class(TForm)
    DS_Maslul: TDataSource;
    Tbl_Maslul: TAtmTable;
    Scale1: TScale;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    OpenDialog1: TOpenDialog;
    Tbl_MaslulCODE_MASLUL: TIntegerField;
    Tbl_MaslulCODE_PIZUL: TIntegerField;
    Tbl_MaslulCODE_TKUFA: TIntegerField;
    Tbl_MaslulCODE_KAV: TIntegerField;
    Tbl_MaslulGROUP_MASLUL: TIntegerField;
    Tbl_MaslulCODE_CLIENT: TIntegerField;
    Tbl_MaslulNAME: TStringField;
    Tbl_MaslulMOTZA: TStringField;
    Tbl_MaslulYAAD: TStringField;
    Tbl_MaslulPRATIM: TStringField;
    Tbl_MaslulSUG_MASLUL: TIntegerField;
    Tbl_MaslulDISTANCE_MASLUL: TBCDField;
    Tbl_MaslulTIME_MASLUL: TIntegerField;
    Tbl_MaslulKAMUT: TBCDField;
    Tbl_MaslulHOUR_START: TDateTimeField;
    Tbl_MaslulHOUR_END: TDateTimeField;
    Tbl_MaslulDATE_START_MASLUL: TDateTimeField;
    Tbl_MaslulDATE_END_MASLUL: TDateTimeField;
    Tbl_MaslulIFYUN: TIntegerField;
    Tbl_MaslulSUG_PIKUACH: TIntegerField;
    Tbl_MaslulHIUV_MISRAD_HINUCH: TIntegerField;
    Tbl_MaslulSUG_HINUCH: TIntegerField;
    Tbl_MaslulTEUR_MUGBAL: TStringField;
    Tbl_MaslulCODE_LUACH: TIntegerField;
    Tbl_MaslulSCHOOL_YEAR: TIntegerField;
    Tbl_MaslulDAY1: TIntegerField;
    Tbl_MaslulDAY2: TIntegerField;
    Tbl_MaslulDAY3: TIntegerField;
    Tbl_MaslulDAY4: TIntegerField;
    Tbl_MaslulDAY5: TIntegerField;
    Tbl_MaslulDAY6: TIntegerField;
    Tbl_MaslulDAY7: TIntegerField;
    Tbl_MaslulPRICE_GLOBAL: TBCDField;
    Tbl_MaslulPRICE_PER_HOUR: TBCDField;
    Tbl_MaslulPRICE_KM: TBCDField;
    Tbl_MaslulMAP_FILENAME: TStringField;
    Tbl_MaslulCODE_MEHIRON: TIntegerField;
    Tbl_MaslulTOTAL_ADULT: TIntegerField;
    Tbl_MaslulTOTAL_YOUNG: TIntegerField;
    Tbl_MaslulTOTAL_TEACHER: TIntegerField;
    Tbl_MaslulTOTAL_MUSAIM: TIntegerField;
    Tbl_MaslulSUG_REHEV1: TIntegerField;
    Tbl_MaslulSUG_REHEV2: TIntegerField;
    Tbl_MaslulSUG_REHEV3: TIntegerField;
    Tbl_MaslulKAMUT_REHEV1: TIntegerField;
    Tbl_MaslulKAMUT_REHEV2: TIntegerField;
    Tbl_MaslulKAMUT_REHEV3: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL1: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL2: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL3: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL1: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL2: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL3: TIntegerField;
    Tbl_MaslulSTRING_1: TStringField;
    Tbl_MaslulSTRING_2: TStringField;
    Tbl_MaslulSTRING_3: TStringField;
    Tbl_MaslulSTRING_4: TStringField;
    Tbl_MaslulSTRING_5: TStringField;
    Tbl_MaslulSTRING_6: TStringField;
    Tbl_MaslulSTRING_7: TStringField;
    Tbl_MaslulSTRING_8: TStringField;
    Tbl_MaslulSTRING_9: TStringField;
    Tbl_MaslulSTRING_10: TStringField;
    Tbl_MaslulBCD_1: TBCDField;
    Tbl_MaslulBCD_2: TBCDField;
    Tbl_MaslulBCD_3: TBCDField;
    Tbl_MaslulBCD_4: TBCDField;
    Tbl_MaslulBCD_5: TBCDField;
    Tbl_MaslulBCD_6: TBCDField;
    Tbl_MaslulBCD_7: TBCDField;
    Tbl_MaslulBCD_8: TBCDField;
    Tbl_MaslulBCD_9: TBCDField;
    Tbl_MaslulBCD_10: TBCDField;
    Tbl_MaslulMaslulAzmnNo: TIntegerField;
    Tbl_MaslulMaslulCarNo: TIntegerField;
    Tbl_MaslulMaslulDriverNo: TIntegerField;
    Tbl_MaslulPricePerMusa: TBCDField;
    Tbl_MaslulPriceConst: TBCDField;
    Tbl_MaslulPayedPrice: TBCDField;
    Tbl_MaslulXBegin: TIntegerField;
    Tbl_MaslulYBegin: TIntegerField;
    Tbl_MaslulNodeNumBBegin: TIntegerField;
    Tbl_MaslulNodeSugSystemBegin: TIntegerField;
    Tbl_MaslulNodeKodCityNameBegin: TIntegerField;
    Tbl_MaslulXEnd: TIntegerField;
    Tbl_MaslulYEnd: TIntegerField;
    Tbl_MaslulNodeNumBEnd: TIntegerField;
    Tbl_MaslulNodeSugSystemEnd: TIntegerField;
    Tbl_MaslulNodeKodCityNameEnd: TIntegerField;
    Tbl_MaslulMaslulForMhiron: TIntegerField;
    Tbl_MaslulDays: TStringField;
    Tbl_MaslulMaxMusa: TIntegerField;
    Tbl_MaslulRem: TMemoField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    Action_ChooseFromMaslulon: TAction;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    TBtn_DistanceFromMsl: TToolButton;
    Tbl_MaslulLookKvutza: TStringField;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    DynamicControls1: TDynamicControls;
    Tbl_MslStat: TAtmTable;
    DS_MslStat: TDataSource;
    Tbl_City: TTable;
    Tbl_MslStatCODE_MASLUL: TIntegerField;
    Tbl_MslStatPOSITION_IN_MASLUL: TIntegerField;
    Tbl_MslStatSEMEL_YESHUV: TIntegerField;
    Tbl_MslStatCODE_STATION: TIntegerField;
    Tbl_MslStatTOTAL_ADULT: TIntegerField;
    Tbl_MslStatTOTAL_YOUNG: TIntegerField;
    Tbl_MslStatTOTAL_TEACHER: TIntegerField;
    Tbl_MslStatTOTAL_MUSAIM: TIntegerField;
    Tbl_MslStatTIME_ARIVAL_TO_STATION: TDateTimeField;
    Tbl_MslStatTIME_ARIVAL_TO_TARGET: TDateTimeField;
    Tbl_MslStatDOWN_UP: TIntegerField;
    Tbl_MslStatDISTANCE_FROM_PREV_STAT: TIntegerField;
    Tbl_MslStatDISTANCE_FROM_FIRST_PNT: TIntegerField;
    Tbl_MslStatCityName: TStringField;
    Panel2: TPanel;
    PageControl_Maslul: TPageControl;
    TS_Maslul: TTabSheet;
    Lbl_CodeMaslul: TLabel;
    Lbl_Group: TLabel;
    Lbl_Name: TLabel;
    Lbl_Motza: TLabel;
    Lbl_Yaad: TLabel;
    Lbl_Pratim: TLabel;
    Lbl_Distance: TLabel;
    Lbl_time: TLabel;
    Lbl_HourStart: TLabel;
    Lbl_HourEnd: TLabel;
    Lbl_SugMaslul: TLabel;
    Lbl_Ifun: TLabel;
    DBText1: TDBText;
    Label8: TLabel;
    EditCODE_MASLUL: TAtmDbHEdit;
    EditGROUP: TAtmDbHEdit;
    EditNAMEMaslul: TAtmDbHEdit;
    EditMOTZA: TAtmDbHEdit;
    EditYAAD: TAtmDbHEdit;
    EditPRATIM: TAtmDbHEdit;
    EditDISTANCE_MASLUL: TAtmDbHEdit;
    EditTIME_MASLUL: TAtmDbHEdit;
    EditHOUR_START: TAtmDbHEdit;
    EditHOUR_END: TAtmDbHEdit;
    GB_Days: TGroupBox;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    AtmDbComboBox_SugMaslul: TAtmDbComboBox;
    AtmDbComboBox_Ifyun: TAtmDbComboBox;
    RxDBGrid1: TRxDBGrid;
    AtmDbHEdit_TotalMusaim: TAtmDbHEdit;
    TS_Picture: TTabSheet;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    GIFImage1: TGIFImage;
    Panel1: TPanel;
    Spb_OpenFile: TSpeedButton;
    Label7: TLabel;
    AtmDbHEdit1: TAtmDbHEdit;
    DirectoryEdit1: TDirectoryEdit;
    AtmAdvSearch_CityCode: TAtmAdvSearch;
    Label9: TLabel;
    AtmDbHEdit2: TAtmDbHEdit;
    Label10: TLabel;
    Label11: TLabel;
    AtmDbHEdit3: TAtmDbHEdit;
    AtmDbHEdit4: TAtmDbHEdit;
    Qry_Nehag: TQuery;
    Tbl_MaslulLookShemNehag: TStringField;
    DBText2: TDBText;
    procedure FormCreate(Sender: TObject);
    procedure EditHOUR_ENDDblClick(Sender: TObject);
    procedure DBRG_IfyunEnter(Sender: TObject);
    procedure DBRG_IfyunExit(Sender: TObject);
    procedure GB_DaysEnter(Sender: TObject);
    procedure GB_DaysExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MaslulAfterPost(DataSet: TDataSet);
    procedure Tbl_MaslulAfterInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PageControl_MaslulChange(Sender: TObject);
    procedure Tbl_MaslulAfterScroll(DataSet: TDataSet);
    procedure Spb_OpenFileClick(Sender: TObject);
    procedure DirectoryEdit1AfterDialog(Sender: TObject; var Name: String;
      var Action: Boolean);
    procedure EditCODE_MASLULExit(Sender: TObject);
    procedure Action_ChooseFromMaslulonExecute(Sender: TObject);
    procedure Action_ChooseFromMaslulonUpdate(Sender: TObject);
    procedure EditMOTZAEnter(Sender: TObject);
    procedure TBtn_DistanceFromMslClick(Sender: TObject);
    procedure DS_MaslulStateChange(Sender: TObject);
    procedure EditGROUPBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure EditNAMEMaslulEnter(Sender: TObject);
    procedure EditNAMEMaslulKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RxDBGrid1Enter(Sender: TObject);
    procedure RxDBGrid1Exit(Sender: TObject);
    procedure Tbl_MslStatBeforePost(DataSet: TDataSet);
    procedure RxDBGrid1EditButtonClick(Sender: TObject);
  private
    { private declarations }
    MapImagePath :String;
    Procedure ShowMapImage;
    Function ReadMapImagePath:String;
    Procedure WriteMapImagePath(ThePath:String);
    Procedure PutNodeDataInRecord(TheNode :TNode);
  public
    { public declarations }
    Procedure ShowForm(FormParams:ShortString);
    Procedure BuildLookupList;
  end;

var
  Frm_MaslulSoroka: TFrm_MaslulSoroka;

implementation
Uses Crt_Glbl,AtmRutin,AtmConst, DMAtmCrt;
{$R *.DFM}
Const
     PnlNum_DSState=0;

procedure TFrm_MaslulSoroka.FormCreate(Sender: TObject);
var
   i:longint;
Begin
  Try
     Screen.Cursor:=crHourGlass;
     PageControl_Maslul.ActivePage:=TS_Maslul;
     MapImagePath:=ReadMapImagePath;
     DirectoryEdit1.InitialDir:=MapImagePath;
     DirectoryEdit1.Text:=MapImagePath;

     BuildLookupList;
     DynamicControls1.Execute;
     Scale1.DoScaleNow;

     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;

          if Components[i] is TAtmAdvSearch Then
                  TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
  Finally
     Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_MaslulSoroka.EditHOUR_ENDDblClick(Sender: TObject);
begin
     SelectFirst;
end;

procedure TFrm_MaslulSoroka.DBRG_IfyunEnter(Sender: TObject);
begin
     (Sender As TDBRadioGroup).Font.Color:=clActiveCaption;
end;

procedure TFrm_MaslulSoroka.DBRG_IfyunExit(Sender: TObject);
begin
     (Sender As TDBRadioGroup).Font.Color:=clBlack;
end;

procedure TFrm_MaslulSoroka.GB_DaysEnter(Sender: TObject);
begin
     (Sender As TGroupBox).Font.Color:=clActiveCaption;
end;

procedure TFrm_MaslulSoroka.GB_DaysExit(Sender: TObject);
begin
     (Sender As TGroupBox).Font.Color:=clBlack;
end;

procedure TFrm_MaslulSoroka.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_MaslulSoroka.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Maslul);
end;

procedure TFrm_MaslulSoroka.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
     if Button=nbRefresh Then
        BuildLookupList;
end;

procedure TFrm_MaslulSoroka.Tbl_MaslulAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_MaslulSoroka.Tbl_MaslulAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_MaslulSoroka.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_MaslulSoroka:=Nil;
     Action:=caFree;
end;

Procedure TFrm_MaslulSoroka.ShowForm(FormParams:ShortString);
Begin
     Show;
     if FormParams<>'' Then
          Tbl_Maslul.FindKey([FormParams])
     Else
         Tbl_Maslul.Insert;
End;

Procedure TFrm_MaslulSoroka.ShowMapImage;
Var
   StrH,FileExt :String;

Begin
     Image1.Visible:=False;
     GifImage1.Visible:=False;
     StrH:=Tbl_Maslul.FieldByName('MAP_FILENAME').AsString;
     StrH:=MapImagePath+StrH;
     if FileExists(StrH) Then
     Begin
          FileExt:=ExtractFileExtention(StrH);
          FileExt:=UPPERCASE(FileExt);
          if FileExt='BMP' Then
          Begin
               Image1.Picture.LoadFromFile(StrH);
               Image1.Visible:=True;
          End
          Else
              if (FileExt='GIF') Or (FileExt='MSL') Then
              Begin
                   GifImage1.LoadFromFile(StrH);
                   GifImage1.Visible:=True;
              End;
     End;
End;

Function TFrm_MaslulSoroka.ReadMapImagePath:String;
Var
   R :TRegistry;
   StrH :String;
begin
  R:=TRegistry.Create;
  Try
     R.RootKey:=RegRootForAtm;
     R.OpenKey(RegKeyForTovala,True);
     if R.ValueExists(RegKeyForMapImage) Then
     Begin
        StrH:=R.ReadString(RegKeyForMapImage);

        if (StrH<>'') And (StrH[Length(StrH)]<>'\') Then
            StrH:=StrH+'\';
     End
     Else
        StrH:='';
     Result:=StrH;
  Finally
         R.Free;
  End;
End;


Procedure TFrm_MaslulSoroka.WriteMapImagePath(ThePath:String);
Var
   R :TRegistry;
begin
  R:=TRegistry.Create;
  Try
     R.RootKey:=RegRootForAtm;
     R.OpenKey(RegKeyForTovala,True);
     R.WriteString(RegKeyForMapImage,ThePath);
  Finally
         R.Free;
  End;
End;
procedure TFrm_MaslulSoroka.PageControl_MaslulChange(Sender: TObject);
begin
     if PageControl_Maslul.ActivePage=TS_Picture Then
        ShowMapImage;
end;

procedure TFrm_MaslulSoroka.Tbl_MaslulAfterScroll(DataSet: TDataSet);
begin
     if PageControl_Maslul.ActivePage=TS_Picture Then
        ShowMapImage;

end;

procedure TFrm_MaslulSoroka.Spb_OpenFileClick(Sender: TObject);
Var
   StrH :String;
begin
     OpenDialog1.InitialDir:=MapImagePath;
     if OpenDialog1.Execute Then
     Begin
          StrH:=ExtractFilePath(OpenDialog1.FileName);
          if CompareText(StrH,MapImagePath)<>0 Then
          Begin
             ShowMessage('�� �������� ����� ���� �� ������ �����');
             Exit;
          End
          Else
          Begin
               if Tbl_Maslul.State=dsBrowse Then
                  Tbl_Maslul.Edit;
               Tbl_Maslul.FieldByName('MAP_FILENAME').AsString:=ExtractFileName(OpenDialog1.FileName);
               ShowMapImage;
          End;
     End;
end;

procedure TFrm_MaslulSoroka.DirectoryEdit1AfterDialog(Sender: TObject;
  var Name: String; var Action: Boolean);
begin
    WriteMapImagePath(Name);
    MapImagePath:=ReadMapImagePath;
    DirectoryEdit1.InitialDir:=MapImagePath;
    DirectoryEdit1.Text:=MapImagePath;
end;

procedure TFrm_MaslulSoroka.EditCODE_MASLULExit(Sender: TObject);
begin
  Try
     if Tbl_Maslul.State<>dsInsert Then
        Exit;
     Screen.Cursor:=crHourGlass;
     if EditCODE_MASLUL.Text='' Then
     Begin
          DM_AtmCrt.Qry_Temp.Active:=False;
          With DM_AtmCrt.Qry_Temp.Sql Do
          Begin
               Clear;
               Add('Select Max('+EditCODE_MASLUL.DataField+') MaxCode');
               Add('From '+Tbl_Maslul.TableName);
          End;
          DM_AtmCrt.Qry_Temp.Active:=True;
          Tbl_Maslul.FieldByName(EditCODE_MASLUL.DataField).AsInteger:=DM_AtmCrt.Qry_Temp.FieldByName('MaxCode').AsInteger+1;
     End;
  Finally
         DM_AtmCrt.Qry_Temp.Active:=False;
         Screen.Cursor:=crDefault;
  End;
end;

Procedure TFrm_MaslulSoroka.BuildLookupList;
Var
   SList:TStringList;
Begin
     SList:=TStringList.Create;
     Try
         SList.AddObject(IntToStr(SugTavla_GroupMaslul),Tbl_MaslulLookKvutza);
         FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);

         SList.Clear;
         SList.AddObject(IntToStr(SugTavla_SugMaslul),AtmDbComboBox_SugMaslul);
         SList.AddObject(IntToStr(SugTavla_IfyunMaslul),AtmDbComboBox_Ifyun);
         FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     Finally
            SList.Free;
     End;
End;

Procedure TFrm_MaslulSoroka.PutNodeDataInRecord(TheNode :TNode);
Begin
    if ActiveControl=EditMOTZA Then
    Begin
         Tbl_MaslulXBegin.AsInteger:=TheNode.Crd.X;
         Tbl_MaslulYBegin.AsInteger:=TheNode.Crd.Y;
         Tbl_MaslulNodeNumBBegin.AsInteger:=TheNode.Numb;
         Tbl_MaslulNodeSugSystemBegin.AsInteger:=TheNode.SugSystem;
         Tbl_MaslulNodeKodCityNameBegin.AsInteger:=TheNode.KodCityName;
    End
    ELse
    Begin
         Tbl_MaslulXEnd.AsInteger:=TheNode.Crd.X;
         Tbl_MaslulYEnd.AsInteger:=TheNode.Crd.Y;
         Tbl_MaslulNodeNumBEnd.AsInteger:=TheNode.Numb;
         Tbl_MaslulNodeSugSystemEnd.AsInteger:=TheNode.SugSystem;
         Tbl_MaslulNodeKodCityNameEnd.AsInteger:=TheNode.KodCityName;
    End;
End;

procedure TFrm_MaslulSoroka.Action_ChooseFromMaslulonExecute(Sender: TObject);
Var
   TheNode :TNode;
begin
     if Not MslDllLoaded Then
        Exit;
     if MslFindNodeFromDlgSearchPlace(TheNode,'') Then
     Begin
          if Tbl_Maslul.State=dsBrowse Then
             Tbl_Maslul.Edit;
          if ActiveControl=EditMOTZA Then
             Tbl_MaslulMOTZA.AsString:=TheNode.NameOfPoint
          Else
             Tbl_MaslulYAAD.AsString:=TheNode.NameOfPoint;

          PutNodeDataInRecord(TheNode);
     End;
end;

procedure TFrm_MaslulSoroka.Action_ChooseFromMaslulonUpdate(Sender: TObject);
begin
     Action_ChooseFromMaslulon.Enabled:=(ActiveControl=EditMOTZA) Or (ActiveControl=EditYAAD);
end;

procedure TFrm_MaslulSoroka.EditMOTZAEnter(Sender: TObject);
begin
     Action_ChooseFromMaslulonUpdate(Sender);
end;

procedure TFrm_MaslulSoroka.TBtn_DistanceFromMslClick(Sender: TObject);
Var
   TempNode :TNode;
   TempNodeObj :TNodeClass;
   ListOfNodes :TList;
   Dist,Zman :DWord;
begin
     if (Tbl_MaslulNodeNumBBegin.IsNull) Or (Tbl_MaslulNodeNumBBegin.AsInteger<=0) Then
     Begin
          ShowMessage('���� ���� �� ����');
          Exit;
     End;
     if (Tbl_Maslul.FieldByName('NodeNumBEnd').IsNull) Or (Tbl_Maslul.FieldByName('NodeNumBEnd').AsInteger<=0) Then
     Begin
          ShowMessage('���� ��� �� ����');
          Exit;
     End;

     Try
        ListOfNodes:=TList.Create;
       //����
       TempNode.Numb:=Tbl_MaslulNodeNumBBegin.AsInteger;
       TempNode.SugSystem:=Tbl_MaslulNodeSugSystemBegin.AsInteger;
       TempNode.KodCityName:=Tbl_MaslulNodeKodCityNameBegin.AsInteger;
       TempNode.Crd.X:=Tbl_MaslulXBegin.AsInteger;
       TempNode.Crd.Y:=Tbl_MaslulYBegin.AsInteger;
       TempNodeObj:=TNodeClass.Create(TempNode);
       ListOfNodes.Add(TempNodeObj);
       //���
       TempNode.Numb:=Tbl_Maslul.FieldByName('NodeNumBEnd').AsInteger;
       TempNode.SugSystem:=Tbl_Maslul.FieldByName('NodeSugSystemEnd').AsInteger;
       TempNode.KodCityName:=Tbl_Maslul.FieldByName('NodeKodCityNameEnd').AsInteger;
       TempNode.Crd.X:=Tbl_Maslul.FieldByName('XEnd').AsInteger;
       TempNode.Crd.Y:=Tbl_Maslul.FieldByName('YEnd').AsInteger;
       TempNodeObj:=TNodeClass.Create(TempNode);
       ListOfNodes.Add(TempNodeObj);
       if MslCalcMaslul(ListOfNodes,Dist,Zman,'�') Then
       Begin
          if Tbl_Maslul.State=dsBrowse Then
             Tbl_Maslul.EDit;
          Tbl_MaslulDISTANCE_MASLUL.AsFloat:=Dist/1000;
       End
       Else
           ShowMessage('����� ����');
     Finally
         //Free TList
         While ListOfNodes.Count>0 Do
         Begin
              TempNodeObj:=TNodeClass(ListOfNodes.Items[0]);
              ListOfNodes.Delete(0);
              TempNodeObj.Free;
         End;
         ListOfNodes.Free;
     End;

end;

procedure TFrm_MaslulSoroka.DS_MaslulStateChange(Sender: TObject);
begin
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

procedure TFrm_MaslulSoroka.EditGROUPBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_GroupMaslul;
end;

procedure TFrm_MaslulSoroka.EditNAMEMaslulEnter(Sender: TObject);
begin
     if EditNAMEMaslul.DataSource.DataSet.State=dsInsert Then
        EditNAMEMaslul.Text:=EditMOTZA.Text+' '+EditYAAD.Text;
end;

procedure TFrm_MaslulSoroka.EditNAMEMaslulKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key=VK_F5 Then
     Begin
        if EditNAMEMaslul.DataSource.DataSet.State=dsBrowse Then
           EditNAMEMaslul.DataSource.DataSet.Edit;
        EditNAMEMaslul.Text:=EditMOTZA.Text+' '+EditYAAD.Text;
     End;
end;


procedure TFrm_MaslulSoroka.RxDBGrid1Enter(Sender: TObject);
begin
     DBNavigator.DataSource:=DS_MslStat;
end;

procedure TFrm_MaslulSoroka.RxDBGrid1Exit(Sender: TObject);
begin
     DBNavigator.DataSource:=DS_Maslul;
end;

procedure TFrm_MaslulSoroka.Tbl_MslStatBeforePost(DataSet: TDataSet);
begin
     if DataSet.State=dsInsert Then
        With DM_AtmCrt.Qry_Temp Do
        Begin
             Active:=False;
             Sql.Clear;
             Sql.Add('Select Count(*) MC From MsllStat Where CODE_MASLUL='+Tbl_MaslulCODE_MASLUL.AsString);
             Active:=True;
             Tbl_MslStatPOSITION_IN_MASLUL.AsInteger:=FieldByName('MC').AsInteger+1;
             Active:=False;
        End;
end;

procedure TFrm_MaslulSoroka.RxDBGrid1EditButtonClick(Sender: TObject);
begin
     if AtmAdvSearch_CityCode.Execute Then
     Begin
        if Tbl_MslStat.State=dsBrowse Then
           Tbl_MslStat.Edit;
        Tbl_MslStatSEMEL_YESHUV.AsString:=AtmAdvSearch_CityCode.ReturnString;
     End;
end;

end.

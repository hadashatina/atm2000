unit MslDllInterface;

interface
Uses Dialogs,Windows,Classes,Forms,SysUtils,Graphics;//,VCLUtils;

Const //DLL �������
   MslSeparator = '_';
   //FindNodeFromAddr ���� ���� ��������
   faLibraryNotLoaded = -1;
   faAddrFound = 0;       //0 if Coord found,
   faCityNotFound = 1;    //1 if City not found,
   faStreetNotFound = 2;  //2 if Street not found,
   faHouseNumNotFound = 3;//3 if Bait not found
type
  ELibraryNotFound = Class(Exception);


  str_47 = string[47];
  TCoordPoint = record
        X,Y : longint;
  end;
  TNode   = record
      Numb:longint;
      Crd : TCoordPoint;
      NameOfPoint:str_47;
      TStop :SmallInt;
      SugSystem : byte;
      KodCityName : longint;
  end;
  TNodeClass = Class
             LNode :TNode;
             LDist,
             LTime : DWord;
             Constructor Create(TheNode :TNode);
  end;
  TNodeList = Class(TList)
     Destructor Destroy;Override;
  End;

type
    TMaslulPoint = class
      LNodeNumb  : longint;
      LX,LY      : longint;
      constructor Create(ANumb,AX,AY : longint);
    end;
var OneMaslulPoint : TMaslulPoint;
type
    TMaslulPointsList = class(TList)
       destructor Destroy ; override;
    end;
var MaslulPointsList : TMaslulPointsList;
type
    TOneMaslul = class
      LName  : string;
      LColor : TColor;
      LTBeg,LTEnd : TDateTime;
      LMaslulPointsList : TMaslulPointsList;
      constructor Create(AName : string;AColor : TColor;ATBeg,ATEnd : TDateTime);
      destructor Destroy; override;
    end;
var OneMaslul : TOneMaslul;
type
    TMasluls_List = class(TList)
       destructor Destroy ; override;
       function   PointInMasluls_list(Crd:TCoordPoint;RadSearch : integer;
            var NumMaslul,NumStation: longint):boolean;
       procedure DrawOnCanvas(Canv : TCanvas);
    end;
var Masluls_List : TMasluls_List;
type
  TExtPlaceClass = Class
             ECrdOld,
             ECrdNew  : TCoordPoint;
             EKod : longint;
             EName : str_47;
             ENumbOld,
             ENumbNew : longint; {zavit}
             ESelected : boolean;
             EPict : {TBitMap}shortstring;
             Constructor Create(AX,AY,AKod,ANumb:longint;
                                AName,AFileBmpName : string);
             Destructor Destroy ;override;
             function GetNewCrd : TCoordPoint;
  end;
var OneExtPlace : TExtPlaceClass;
type
  TExtPlacesList = Class(TList)
     destructor Destroy ; override;
  end;
var ExtPlacesList : TExtPlacesList;
type
     TDoneMslDll  = procedure   ;
     TShowMapRef = procedure;
     TFindNodeFromSearchPlace = function  (var ANode : TNode;Cap : PChar):boolean;
     TCalcMaslul = function  (InList : TList;var MetrOfDist,SecOfTime : DWord;SugCalc :Char):boolean;
     TCalcDistAndTime = function(InList : TList;var MetrOfDist,SecOfTime : DWord;SugCalc:char):boolean;
     TDoLastMaslulReport=function  :boolean;
     TFindNodeFromAddr= function(NameCity,NameStreet,NumBait:pchar;
                         var ANode : TNode):word;
     TFindNodeFromCityAndTwoStreets= function(NameCity,Street1,Street2:pchar;
                         var ANode : TNode):word;
     TChooseNodeFromMap=Function(Var ANode :TNODE):Boolean;
     TChoosePointFromNearCrd =function(CrdX,CrdY:longint;Var ReturnNode :TNode):Boolean;
     TFindNodeFromCrd=function(X,Y : longint;var ANode : TNode):boolean;
     TGoToCrd = function  (CrdX,CrdY:longint):boolean;
     TDrawBmpForXYR = function  (CrdX,CrdY,Rad,N_Mtk,AngleBmp:longint;
                       FileBmp : string;FileNameOut : PChar):boolean;
     TDrawListOfPlaces = Function (ExtPlacesList : {TList}TExtPlacesList;RIn:longint;
                             OutFileName : PChar):boolean;
     TDrawLastMaslul = function : boolean;
     TCalcMaslulToListOfNodes=function(var ANode:TNode;OutList : TList;SugCalc:char):boolean;
     TShowMasluls_List=function(Masluls_List : TMasluls_List):boolean;

     TViewMessage=function(NewMessage,APhoneNum,ASugMsg,AOrdNumber,AMsgText :PChar;
                           var AX,AY : longint) : DWord;
     TCallSherutForm=procedure;
     TChangeDefs = procedure;
     TSendStr= procedure(PhoneTxt,Msg : pchar );
     TSendHebStr=procedure (PhoneTxt,Msg : pchar );
     TSendForm=procedure (PhoneTxt,
                   ShtarTxt,
                   LakNameTxt,
                   MozaTxt,
                   IadTxt,
                   ShaaTxt,
                   MitanTxt,
                   KamutTxt,
                   RemarkText : pchar
                   );
     TDoneMsgDll=procedure ;
var
    MslDll_Handle : THandle;
    MsgDll_Handle : THandle;
    SrAdDoneDll : TDoneMslDll;
    SrAdShowMapRef : TShowMapRef;
    SrAdFindNodeFromSearchPlace : TFindNodeFromSearchPlace;
    SrAdCalcMaslul : TCalcMaslul;
    SrAdCalcDistAndTime:TCalcDistAndTime;
    SrAdDoLastMaslulReport : TDoLastMaslulReport;
    SrAdFindNodeFromAddr : TFindNodeFromAddr;
    SrAdFindNodeFromCityAndTwoStreets : TFindNodeFromCityAndTwoStreets;
    SrAdChooseNodeFromMap:TChooseNodeFromMap;
    SrAdChoosePointFromNearCrd :TChoosePointFromNearCrd;

    SrAdGoToCrd : TGoToCrd;
    SrAdDrawBmpForXYR : TDrawBmpForXYR;
    SrAdDrawListOfPlaces : TDrawListOfPlaces;
    SrAdFindNodeFromCrd : TFindNodeFromCrd;
    SrAdDrawLastMaslul : TDrawLastMaslul;
    SrAdCalcMaslulToListOfNodes : TCalcMaslulToListOfNodes;
    SrAdShowMasluls_List : TShowMasluls_List;

    SrAdViewMessage : TViewMessage;
    SrAdCallSherutForm : TCallSherutForm;
    SrAdChangeDefs : TChangeDefs;
    SrAdSendStr : TSendStr;
    SrAdSendHebStr : TSendHebStr;
    SrAdSendForm : TSendForm;
    SrAdDoneMsgDll : TDoneMsgDll;
Type
    TThreadCheckAddress = Class(TThread)
       protected
         City,Street,HouseNum:String;
         procedure Execute; override;
       public
         constructor Create(TheCity,TheStreet,TheHouseNum:String);
     End;
Type
    TRerutnToProc = Procedure of Object;
Var
   ThreadCheckAddress:TThreadCheckAddress;
   ThreadCheckAddressEnded :Boolean;
   ThreadCheckAddressReturnNode :TNode;
   ThreadCheckAddressRes :LongInt;


Function LoadMslDll :Boolean;

Procedure FreeMslDll;

procedure MslShowMapRef;
Function MslFindNodeFromDlgSearchPlace(Var ANode :TNode;InStr:string) :Boolean;
Function MslCalcMaslul(OutList :TList;Var Dist,Zman :DWord;SugCalc :Char):Boolean;
Function MslCalcDistAndTime(OutList :TList;Var Dist,Zman :DWord;SugCalc :Char):Boolean;
Function MslShowLastReport :Boolean;
Function MslFindNodeFromAddress(TheCity,TheStreet,TheHouseNum :String;Var ReturnNode :TNode):Word;
Function MslFindNodeFromCityAndTwoStreets(TheCity,TheStreet1,TheStreet2 :String;Var ReturnNode :TNode):Word;
Function MslChoosePoint(Var ReturnNode :TNode):Boolean;
Function MslChoosePointFromNearCrd(CrdX,CrdY:longint;Var ReturnNode :TNode):Boolean;
Function MslGotoXY(X,Y :LongInt):Boolean; //�� �� ���� ����� False �����
Function MslDrawBmpForXYR(CrdX,CrdY,Rad,N_Mtk,AngleBmp:longint;
                       FileBmp,FileNameOut : PChar):boolean;
Function MslDrawListOfPlaces(ExtPlacesList : {TList}TExtPlacesList;RIn:longint;
                             OutFileName : PChar):boolean;
Function MslShowLastMaslul :Boolean; //����� ��� ������ ����
Function MslCalcMaslulToNodeList (var ANode:TNode;OutList : TList;SugCalc:char):boolean;
Function CompareNodeClassDistance(Obj1,Obj2 :Pointer) :Integer;
Function MslFindNodeFromCrd(X,Y:longint;Var ANode :TNode) :Boolean;
function  ShowMslList:boolean;
procedure FreeMasluls_List;

function  LoadMsgDll :Boolean;
Procedure FreeMsgDll;
Function  MsgViewMessage(NewMessage,APhoneNum,ASugMsg,AOrdNumber,AMsgText :PChar;
                         var AX,AY : longint) : DWord;
procedure MsgCallSherutForm;
procedure MsgChangeDefs;
procedure MsgSendStr(PhoneTxt,Msg : pchar );
procedure MsgSendHebStr(PhoneTxt,Msg : pchar );
procedure MsgSendForm(PhoneTxt,
                   ShtarTxt,
                   LakNameTxt,
                   MozaTxt,
                   IadTxt,
                   ShaaTxt,
                   MitanTxt,
                   KamutTxt,
                   RemarkText : pchar
                   );
procedure MsgDoneMsgDll;
{
Function  DrawListOfPlaces(ExtPlacesList : TExtPlacesList;RIn:longint;
                             OutFileName : PChar):boolean;
     external 'msl_func.dll';
Function FindNodeFromAddress(TheCity,TheStreet,TheHouseNum :String;Var ReturnNode :TNode):Word;
     external 'msl_func.dll';
Function FindNodeFromCityAndTwoStreets(TheCity,TheStreet1,TheStreet2 :String;Var ReturnNode :TNode):Word;
     external 'msl_func.dll';
Function ChoosePoint(Var ReturnNode :TNode):Boolean;
     external 'msl_func.dll';
Function ChoosePointFromNearCrd(CrdX,CrdY:longint;Var ReturnNode :TNode):Boolean;
     external 'msl_func.dll';
}
implementation
  { **************************** }
Constructor TNodeClass.Create(TheNode :TNode);
Begin
     Inherited Create;
     LDist:=0;
     LTime:=0;
     LNode:=TheNode;
End;

constructor TThreadCheckAddress.Create(TheCity,TheStreet,TheHouseNum:String);
begin
 inherited Create(False);
 FreeOnTerminate := True;
 ThreadCheckAddressEnded:=False;
 priority:=tpNormal;
 City:=TheCity;
 Street:=TheStreet;
 HouseNum:=TheHouseNum;
end;

Procedure TThreadCheckAddress.Execute;
Var
   MslDllLoaded:Boolean;
Begin
  Try
     MslDllLoaded:=MslDll_Handle<>0;
     Try
        if Not MslDllLoaded Then
           LoadMslDll;
     Except On ELibraryNotFound Do
         Begin
            ThreadCheckAddressRes:=-1;
            Terminate;
         End;
     End;
     if Terminated Then
        Exit;
     ThreadCheckAddressRes:=MslFindNodeFromAddress(City,Street,HouseNum,ThreadCheckAddressReturnNode);
  Finally
     if  Not MslDllLoaded Then
        FreeMslDll;
  End;
     ThreadCheckAddressEnded:=True;
End;

constructor TMaslulPoint.Create(ANumb,AX,AY : longint);
begin
  Inherited Create;
  LNodeNumb := ANumb;
  LX:=AX;LY:=AY;
end;

Destructor TNodeList.Destroy;
Var
   I :LongInt;
   TmpObj :TNodeClass;
Begin
  Try
     For I:=0 To Count-1 Do
     Begin
          TmpObj:=TNodeClass(Items[I]);
          TmpObj.Free;
     End;
  Finally
     Clear;
     Inherited Destroy;
  End;
End;
          { ************** }
        { Methods Of TMaslulPointsList}
destructor TMaslulPointsList.Destroy;
var ix1 : longint;
begin
  for ix1:=0 to Count -1 do
  begin
    OneMaslulPoint:=Items[ix1];
    OneMaslulPoint.Free;
  end;
  inherited Destroy;
end;
    { ***************** }
       { Methods Of TOneMaslul}
constructor TOneMaslul.Create(AName : string;AColor : TColor;ATBeg,ATEnd : TDateTime);
begin
  Inherited Create;
  LName := AName;
  LColor :=AColor;
  LTBeg:=ATBeg;LTEnd:=ATEnd;
  LMaslulPointsList:=TMaslulPointsList.Create;
end;
     { ************* }
destructor TOneMaslul.Destroy;
begin
  LMaslulPointsList.Free;
  inherited Destroy;
end;
    { ***************** }
        { Methods Of TMasluls_List}
destructor TMasluls_List.Destroy;
var ix1 : longint;
begin
  for ix1:=0 to Count -1 do
  begin
    OneMaslul:=Items[ix1];
    OneMaslul.Free;
  end;
  inherited Destroy;
end;
    { ***************** }
function TMasluls_List.PointInMasluls_list(Crd:TCoordPoint;RadSearch : integer;
            var NumMaslul,NumStation: longint):boolean;
begin

end;
  { ************* }
procedure TMasluls_List.DrawOnCanvas(Canv : TCanvas);
begin

end;
 { ****************** }
        { Methods Of TExtPlaceClass}
Constructor TExtPlaceClass.Create(AX,AY,AKod,ANumb:longint;
                            AName,AFileBmpName : string);
begin
       Inherited Create;
//       ECrdOld:=TCoordPoint(Point(AX,AY));
//       ECrdNew:=TCoordPoint(Point(AX,AY));
       ECrdOld.X := AX;ECrdNew.X := AX;
       ECrdOld.Y := AY;ECrdNew.Y := AY;
       EKod := AKOd;
       EName:= AName;
       ENumbOld:=ANumb; ENumbNew:=ANumb;
       ESelected := FALSE;
       EPict := {TBitMap.Create}AFileBmpName;
       {EPict.LoadFromFile(AFileBmpName);}
end;
    { ************* }
Destructor TExtPlaceClass.Destroy;
begin
 { EPict.Free; }
  inherited Destroy;
end;
  { ************* }
    { Methods Of TExtPlacesList}
destructor TExtPlacesList.Destroy;
var ix1 : longint;
begin
  for ix1:=0 to Count -1 do
    TExtPlaceClass(Items[ix1]).Free;
  clear;
  inherited destroy;
end;
    { ***************** }
function TExtPlaceClass.GetNewCrd : TCoordPoint;
var MyCoord : TCoordPoint;
begin
       MyCoord.X:=ECrdNew.X ;
       MyCoord.Y:=ECrdNew.Y ;
       Result:=MyCoord;
end;
    { ************* }

procedure FreeMasluls_List;
begin
  if Masluls_List <> nil then
    Masluls_List.Free;
  Masluls_List:=Nil;
end;
  { ************* }

Function LoadMslDll :Boolean;
Begin
  if MslDll_Handle=0 Then
  Begin
       MslDll_Handle:=LoadLibrary('Msl_Func.dll');
{       if MslDll_Handle = 0 then
          Raise ELibraryNotFound.Create('Msl_Func.Dll Not Found');}
       Result:= MslDll_Handle <> 0;
  End;
End;


Procedure FreeMslDll;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdDoneDll:=
       GetProcAddress(MslDll_Handle,'DoneDll');
    if @SrAdDoneDll <>  nil  then
       SrAdDoneDll;
    @SrAdDoneDll:=nil;
    FreeMasluls_List;
    FreeLibrary(MslDll_Handle);
    MslDll_Handle:=0;
  End;
End;
   { **************** }
procedure MslShowMapRef;
begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdShowMapRef:=
       GetProcAddress(MslDll_Handle,'ShowMapRef');
    if @SrAdShowMapRef <>  nil  then
      SrAdShowMapRef;
  End
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
end;
  { ***************** }
Function MslFindNodeFromDlgSearchPlace(Var ANode :TNode;InStr : string) :Boolean;
var Pch : array[0..79] of Char;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdFindNodeFromSearchPlace:=
       GetProcAddress(MslDll_Handle,'FindNodeFromSearchPlace');
    if @SrAdFindNodeFromSearchPlace <>  nil  then
    begin
      StrPCopy(PCh,InStr);
      Result:=SrAdFindNodeFromSearchPlace(ANode,PCh);
    end;
  End
  Else
   Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
  { ***************** }
Function MslFindNodeFromCrd(X,Y:longint;Var ANode :TNode) :Boolean;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdFindNodeFromCrd:=
       GetProcAddress(MslDll_Handle,'FindNodeFromCrd');
    if @SrAdFindNodeFromCrd <>  nil  then
      Result:=SrAdFindNodeFromCrd(X,Y,ANode);
  End
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
  { ***************** }

Function MslCalcMaslul(OutList :TList;Var Dist,Zman :DWord;SugCalc :Char):Boolean;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdCalcMaslul:=
       GetProcAddress(MslDll_Handle,'CalcMaslul');
    if @SrAdCalcMaslul <>  nil  then
      Result:=SrAdCalcMaslul(OutList,Dist,Zman,SugCalc);
  End
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;

Function MslCalcDistAndTime(OutList :TList;Var Dist,Zman :DWord;SugCalc :Char):Boolean;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdCalcDistAndTime:=
       GetProcAddress(MslDll_Handle,'CalcDistAndTime');
    if @SrAdCalcDistAndTime <>  nil  then
      Result:=SrAdCalcDistAndTime(OutList,Dist,Zman,SugCalc);
  End
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;

Function MslShowLastReport:Boolean;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdDoLastMaslulReport:=
       GetProcAddress(MslDll_Handle,'DoLastMaslulReport');
    if @SrAdDoLastMaslulReport <>  nil  then
      Result:=SrAdDoLastMaslulReport;
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;

Function MslFindNodeFromAddress(TheCity,TheStreet,TheHouseNum :String;Var ReturnNode :TNode):Word;
Var
   ACity,AStreet,ANumber : PChar;
begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdFindNodeFromAddr:=
       GetProcAddress(MslDll_Handle,'FindNodeFromAddr');
    if @SrAdFindNodeFromAddr <>  nil  then
    Try
    begin
      ACity:=StrAlloc(30);
      AStreet:=StrAlloc(30);
      ANumber:=StrAlloc(10);
      StrPCopy(ACity,TheCity);
      StrPCopy(AStreet,TheStreet);
      StrPCopy(ANumber,TheHouseNum);
      Result:=SrAdFindNodeFromAddr(ACity,AStreet,ANumber,ReturnNode);
      StrDispose(ACity);
      StrDispose(AStreet);
      StrDispose(ANumber);
    end
    Except
    End;
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
  { *************** }
Function MslFindNodeFromCityAndTwoStreets(TheCity,TheStreet1,TheStreet2 :String;Var ReturnNode :TNode):Word;
Var
   ACity,AStreet1,AStreet2 : PChar;
begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdFindNodeFromCityAndTwoStreets:=
       GetProcAddress(MslDll_Handle,'FindNodeFromCityAndTwoStreets');
    if @SrAdFindNodeFromCityAndTwoStreets <>  nil  then
    begin
      ACity:=StrAlloc(30);
      AStreet1:=StrAlloc(30);
      AStreet2:=StrAlloc(30);
      StrPCopy(ACity,TheCity);
      StrPCopy(AStreet1,TheStreet1);
      StrPCopy(AStreet2,TheStreet2);
      Result:=SrAdFindNodeFromCityAndTwoStreets(ACity,AStreet1,AStreet2,ReturnNode);
      StrDispose(ACity);
      StrDispose(AStreet1);
      StrDispose(AStreet2);
    end;
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
  { *************** }


Function MslChoosePoint(Var ReturnNode :TNode):Boolean;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdChooseNodeFromMap:=
       GetProcAddress(MslDll_Handle,'ChooseNodeFromMap');
    if @SrAdChooseNodeFromMap <>  nil  then
    begin
      Result:=SrAdChooseNodeFromMap(ReturnNode);
    end;
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;

Function MslChoosePointFromNearCrd(CrdX,CrdY:longint;Var ReturnNode :TNode):Boolean;
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdChoosePointFromNearCrd:=
       GetProcAddress(MslDll_Handle,'GoToCrdAndChooseNodeFromMap');
    if @SrAdChoosePointFromNearCrd <>  nil  then
    begin
      Result:=SrAdChoosePointFromNearCrd(CrdX,CrdY,ReturnNode);
    end;
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;

Function MslGotoXY(X,Y :LongInt):Boolean;
//�� �� ���� ����� False �����
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdGoToCrd:=GetProcAddress(MslDll_Handle,'GoToCrd');
    if @SrAdGoToCrd <>  nil  then
      Result:=SrAdGoToCrd(X,Y);
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
    { ************** }
Function MslDrawBmpForXYR(CrdX,CrdY,Rad,N_Mtk,AngleBmp:longint;
                       FileBmp,FileNameOut : PChar):boolean;
//�� �� ���� ����� False �����
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdDrawBmpForXYR:=GetProcAddress(MslDll_Handle,'DrawBmpForXYR');
    if @SrAdDrawBmpForXYR <>  nil  then
      Result:=SrAdDrawBmpForXYR(CrdX,CrdY,Rad,N_Mtk,AngleBmp,FileBmp,FileNameOut);
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
   { *************** }
Function MslDrawListOfPlaces(ExtPlacesList : {TList}TExtPlacesList;RIn:longint;
                             OutFileName : PChar):boolean;
//�� �� ���� ����� False �����
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdDrawListOfPlaces:=
          GetProcAddress(MslDll_Handle,'DrawListOfPlaces');
    if @SrAdDrawListOfPlaces <>  nil  then
      Result:=SrAdDrawListOfPlaces(ExtPlacesList,RIn,OutFileName);
  end
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;
   { *************** }
Function MslShowLastMaslul :Boolean;
//����� ��� ������ ����
Begin
  if MslDll_Handle <> 0 then
  begin
    @SrAdDrawLastMaslul:=
       GetProcAddress(MslDll_Handle,'DrawLastMaslul');
    if @SrAdDrawLastMaslul <>  nil  then
      Result:=SrAdDrawLastMaslul;
  End
  Else
      Raise ELibraryNotFound.Create('Msl_Func.Dll Not Loaded');
End;

Function MslCalcMaslulToNodeList (var ANode:TNode;OutList : TList;SugCalc:char):boolean;
Begin
{ ANode = ����� ������� �� ������� ����
  InList = ��� ����� �� ������ ���� ������ �� ����� ����� ������ ����� Tlist of TNodeClass
  SugCalc = � - ����� ��� ���� �-����� ��� ���
 }
  if MslDll_Handle <> 0 then
  begin
    @SrAdCalcMaslulToListOfNodes:=
       GetProcAddress(MslDll_Handle,'CalcMaslulToListOfNodes');
    if @SrAdCalcMaslulToListOfNodes <>  nil  then
       Result:=SrAdCalcMaslulToListOfNodes(ANode,OutList,SugCalc);
  End;
End;

Function CompareNodeClassDistance(Obj1,Obj2 :Pointer) :Integer;
Begin
     if TNodeClass(Obj1).LDist > TNodeClass(Obj2).LDist Then
        Result:= 1
     Else
       if TNodeClass(Obj1).LDist = TNodeClass(Obj2).LDist Then
          Result:=0
       Else
           Result:=-1;
End;
  { ********************** }

procedure CreateMasluls_List;
var Icon1 : TIcon;
begin
  Masluls_List:=TMasluls_List.Create;
  OneMaslul:=TOneMaslul.Create('11-111-11',clGreen,
             Now - strtotime('01:00'),Now + strtotime('01:00') );
  with OneMaslul do
  begin
    OneMaslulPoint:=TMaslulPoint.Create(1,126300,1115400);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,127200,1115500);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,127400,1115900);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,127300,1115400);
    LMaslulPointsList.Add(OneMaslulPoint);
  end;
  Masluls_List.Add(OneMaslul);
  OneMaslul:=TOneMaslul.Create('22-222-22',clRed,
             Now - strtotime('02:00'),Now + strtotime('01:00') );
  with OneMaslul do
  begin
    OneMaslulPoint:=TMaslulPoint.Create(1,125300,1116400);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,126200,1116500);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,126400,1116700);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,126700,1116800);
    LMaslulPointsList.Add(OneMaslulPoint);
  end;
  Masluls_List.Add(OneMaslul);
  OneMaslul:=TOneMaslul.Create('33-333-33',clBlue,
             Now - strtotime('01:00'),Now + strtotime('00:30') );
  with OneMaslul do
  begin
    OneMaslulPoint:=TMaslulPoint.Create(1,124500,1114200);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,124800,1114500);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,125200,1114700);
    LMaslulPointsList.Add(OneMaslulPoint);
    OneMaslulPoint:=TMaslulPoint.Create(1,125100,1114900);
    LMaslulPointsList.Add(OneMaslulPoint);
  end;
  Masluls_List.Add(OneMaslul);
end;
  { ************* }
function ShowMslList:boolean;
begin
  {
  if Masluls_List <> nil then
     FreeMasluls_List;
  try
    CreateMasluls_List;
  }
    if MslDll_Handle <> 0 then
    begin
      @SrAdShowMasluls_List:=
         GetProcAddress(MslDll_Handle,'ShowMasluls_List');
      if @SrAdShowMasluls_List <>  nil  then
      begin
        if SrAdShowMasluls_List(Masluls_List) then;
      end;
    end;
  {
  finally
    FreeMasluls_List;
  end;
  }
end;
  { ***************** }
Function LoadMsgDll :Boolean;
Begin
  if MsgDll_Handle=0 Then
  Begin
       MsgDll_Handle:=LoadLibrary('GetMessDll.dll');
       Result:= MsgDll_Handle <> 0;
  End;
End;
  { ************** }
Procedure FreeMsgDll;
Begin
  if MsgDll_Handle <> 0 then
  begin
    {
    @SrAdDoneMsgDll:=
       GetProcAddress(MsgDll_Handle,'DoneMsgDll');
    if @SrAdDoneMsgDll <>  nil  then
       SrAdDoneMsgDll;
    }   
    FreeLibrary(MsgDll_Handle);
    MsgDll_Handle:=0;
  End;
End;
   { **************** }
Function MsgViewMessage(NewMessage,APhoneNum,ASugMsg,AOrdNumber,AMsgText :PChar;
         var AX,AY : longint) : DWord;
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdViewMessage:=
       GetProcAddress(MsgDll_Handle,'ViewMessage');
    if @SrAdViewMessage <>  nil  then
    begin
      Result:=SrAdViewMessage(NewMessage,APhoneNum,ASugMsg,AOrdNumber,AMsgText,AX,AY);
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }
procedure MsgCallSherutForm;
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdCallSherutForm:=
       GetProcAddress(MsgDll_Handle,'CallSherutForm');
    if @SrAdCallSherutForm <>  nil  then
    begin
      SrAdCallSherutForm;
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }
procedure MsgChangeDefs;
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdChangeDefs:=
       GetProcAddress(MsgDll_Handle,'ChangeDefs');
    if @SrAdChangeDefs <>  nil  then
    begin
      SrAdChangeDefs;
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }
procedure MsgSendStr(PhoneTxt,Msg : pchar );
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdSendStr:=
       GetProcAddress(MsgDll_Handle,'SendStr');
    if @SrAdSendStr <>  nil  then
    begin
      SrAdSendStr(PhoneTxt,Msg) ;
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }
procedure MsgSendHebStr(PhoneTxt,Msg : pchar );
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdSendHebStr:=
       GetProcAddress(MsgDll_Handle,'SendHebStr');
    if @SrAdSendHebStr <>  nil  then
    begin
      SrAdSendHebStr(PhoneTxt,Msg) ;
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }
procedure MsgSendForm(PhoneTxt,
                   ShtarTxt,
                   LakNameTxt,
                   MozaTxt,
                   IadTxt,
                   ShaaTxt,
                   MitanTxt,
                   KamutTxt,
                   RemarkText : pchar
                   );
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdSendForm:=
       GetProcAddress(MsgDll_Handle,'SendForm');
    if @SrAdSendForm <>  nil  then
    begin
      SrAdSendForm(PhoneTxt,
                   ShtarTxt,
                   LakNameTxt,
                   MozaTxt,
                   IadTxt,
                   ShaaTxt,
                   MitanTxt,
                   KamutTxt,
                   RemarkText) ;
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }
procedure MsgDoneMsgDll;
Begin
  if MsgDll_Handle <> 0 then
  begin
    @SrAdDoneMsgDll:=
       GetProcAddress(MsgDll_Handle,'DoneMsgDll');
    if @SrAdDoneMsgDll <>  nil  then
    begin
      SrAdDoneMsgDll ;
    end;
  End
  Else
      Raise ELibraryNotFound.Create('GetMessDll.Dll Not Loaded');
End;
  { ***************** }

Begin
     MslDll_Handle:=0;
     MsgDll_Handle:=0;
     ThreadCheckAddress:=Nil;
     Masluls_List:=nil;
end.





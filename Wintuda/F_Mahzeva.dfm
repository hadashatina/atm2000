�
 TFRM_MAHZEVA 0�<  TPF0TFrm_MahzevaFrm_MahzevaLeftVTopBWidth�HeightSBiDiModebdRightToLeftCaption������Color	clBtnFace
ParentFont	
KeyPreview	OldCreateOrder	ParentBiDiModePositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroy	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TPageControlPageControl1Left Top Width�Height
ActivePageTS_MainAlignalClientStyletsFlatButtonsTabOrder  	TTabSheetTS_MainCaption����� ����� TPanel
Panel_MainLeft Top Width�Height� AlignalClient
BevelInner	bvLoweredTabOrder  TLabelLbl_CodeMahzevaLeft�TopWidth9HeightCaption	��� �����FocusControlEditCodeMahzeva  TLabelLbl_GroupMahzevaLeftiTopWidth!HeightCaption�����FocusControlEditGroupMahzeva  TLabelLbl_NameMahzevaLeftTopWidth6HeightCaption�� �����FocusControlEditNameMahzeva  TLabelLbl_AddressLeft�Top,Width!HeightCaption�����FocusControlEditAddress  TLabelLbl_CityLeft� Top,WidthHeightCaption���FocusControlEditCity  TLabelLbl_ZipCodeLeftQTop,WidthHeightCaption�����FocusControlEditZipCode  TLabel	Lbl_PhoneLeft�TopPWidth(HeightCaption����� 1FocusControl	EditPhone  TLabel
Lbl_Phone2LeftLTopPWidth(HeightCaption����� 2FocusControl
EditPhone2  TLabelLbl_FaxLeftTopPWidthHeightCaption���FocusControlEditFax  TLabelLbl_DiscountPercentLeft�TopxWidth6HeightCaption	���� ����FocusControlEditDiscountPercent  TLabelLbl_MasBamakorLeft<TopxWidth9HeightCaption	%�� �����FocusControlEditMasBamakor  TLabelLbl_TnaeyTashlumLeft� TopxWidth?HeightCaption
���� �����  TLabel
Lbl_MisHanLeft�Top� Width2HeightCaption	��' ���"�FocusControl
EditMisHan  TLabelLbl_CodeMehironLeft� Top� Width9HeightCaption
��� ������FocusControlEditCodeMehiron  TLabelLbl_LastUpdateLeft�Top� WidthAHeightCaption����� �����FocusControlEditLastUpdate  TLabelLbl_MaklidNameLeftTop� Width4HeightCaption�� �����FocusControlEditMaklidName  TAtmDbHEditEditCodeMahzevaLeft�TopWidth7HeightBiDiModebdRightToLeft	DataFieldCodeMahzeva
DataSource
DS_MahzevaParentBiDiModeTabOrder 	OnKeyDownEditCodeMahzevaKeyDownUseF2ToRunSearch	IsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_CodeMahzevaLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSearchComponentAtmAdvSearch_MahzevaSelectNextAfterSearch	StatusBarPanelNum�HebrewF9String1LocateRecordOnChange	  TAtmDbHEditEditGroupMahzevaLeftUTopWidth7HeightBiDiModebdRightToLeft	DataFieldGroupMahzeva
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_GroupMahzevaLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewF9String1LocateRecordOnChange  TAtmDbHEditEditNameMahzevaLeftxTopWidth� HeightBiDiModebdRightToLeft	DataFieldNameMahzeva
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_NameMahzevaLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�Hebrew	F9String����� ������LocateRecordOnChange  TAtmDbHEditEditAddressLeft� Top;Width� HeightBiDiModebdRightToLeft	DataFieldAddress
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_AddressLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�Hebrew	F9String����� ���������LocateRecordOnChange  TAtmDbHEditEditCityLeftyTop;WidthsHeightBiDiModebdRightToLeft	DataFieldCity
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_CityLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�Hebrew	F9String	���� ����LocateRecordOnChange  TAtmDbHEditEditZipCodeLeft<Top;Width7HeightBiDiModebdLeftToRight	DataFieldZipCode
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_ZipCodeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEdit	EditPhoneLeft}Top_WidthKHeightBiDiModebdLeftToRight	DataFieldPhone1
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabel	Lbl_PhoneLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEdit
EditPhone2Left,Top_WidthKHeightBiDiModebdLeftToRight	DataFieldPhone2
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabel
Lbl_Phone2LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditFaxLeft� Top_WidthKHeightBiDiModebdLeftToRight	DataFieldFax
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_FaxLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditDiscountPercentLeft}Top� WidthKHeightBiDiModebdRightToLeft	DataFieldDiscountPercent
DataSource
DS_MahzevaParentBiDiModeTabOrder	UseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_DiscountPercentLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditMasBamakorLeft,Top� WidthKHeightBiDiModebdRightToLeft	DataField
MasBamakor
DataSource
DS_MahzevaParentBiDiModeTabOrder
UseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_MasBamakorLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEdit
EditMisHanLeft.Top� Width� HeightBiDiModebdRightToLeft	DataFieldMisHan
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabel
Lbl_MisHanLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�Hebrew	LocateRecordOnChange  TAtmDbHEditEditCodeMehironLeft� Top� Width7HeightBiDiModebdRightToLeft	DataFieldCodeMehiron
DataSource
DS_MahzevaParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLbl_CodeMehironLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditLastUpdateLeftQTop� WidthxHeightBiDiModebdRightToLeftColor	clBtnFace	DataField
LastUpdate
DataSource
DS_MahzevaEnabledParentBiDiModeReadOnly	TabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�Hebrew	LocateRecordOnChange  TAtmDbHEditEditMaklidNameLeft� Top� WidthsHeightBiDiModebdRightToLeftColor	clBtnFace	DataField
MaklidName
DataSource
DS_MahzevaEnabledParentBiDiModeReadOnly	TabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�Hebrew	LocateRecordOnChange  TAtmDbComboBoxAtmDbComboBox_TnaeyTashlumLeft� Top� WidthuHeightColorclInfoBk
ItemHeightSorted	TabOrder
DataSource
DS_Mahzeva	DataFieldTnaeyTashlumSugTavlaIndex      TToolBarToolBar1Left TopWidth�HeightAlignalBottomCaptionToolBar1Flat	ImagesDM_AtmCrt.ImageList_CrtButtonsTabOrder TDBNavigatorDBNavigatorLeft Top Width� Height
DataSource
DS_MahzevaFlat	Ctl3DParentCtl3DTabOrder BeforeActionDBNavigatorBeforeAction  TToolButtonToolButton1Left� Top WidthCaptionToolButton1StyletbsSeparator  TToolButtonToolButton2Left� Top ActionDM_AtmCrt.Action_OpenSearch   
TStatusBar
StatusBar1Left Top%Width�HeightPanelsWidth2 Width2  SimplePanel  TDataSource
DS_MahzevaDataSetTbl_MahzevaOnStateChangeDS_MahzevaStateChangeLeft� Top  TAtmRxQueryTbl_MahzevaTagCachedUpdates	AfterInsertTbl_MahzevaAfterInsert	AfterPostTbl_MahzevaAfterPostDatabaseName	DB_AtmCrtRequestLive	SQL.StringsSelect * From MAHZEVAOrder By %OrderFields  
UpdateModeupWhereChangedUpdateObjectUpdateSQL_MahzevaMacrosDataTypeftStringNameOrderFields	ParamTypeptInputValueCodeMahzeva  IndexFieldNamesCODEMAHZEVAIndexAutoSort	MacroForIndexOrderFieldsAllartBeforeSaveAutoValidateLookupFieldsDeleteDetailRecordsConfirmDeleteDetail	AutoDefaults	AutoApplyUpdates	Left� Top TIntegerFieldTbl_MahzevaCodeMahzevaDisplayLabel	��� �����	FieldNameCodeMahzeva  TIntegerFieldTbl_MahzevaGroupMahzevaDefaultExpression0	FieldNameGroupMahzeva  TStringFieldTbl_MahzevaNameMahzeva	FieldNameNameMahzevaSize(  TStringFieldTbl_MahzevaAddress	FieldNameAddressSize(  TStringFieldTbl_MahzevaCity	FieldNameCity  TIntegerFieldTbl_MahzevaZipCode	FieldNameZipCode  TStringFieldTbl_MahzevaPhone1	FieldNamePhone1Size  TStringFieldTbl_MahzevaPhone2	FieldNamePhone2Size  TStringFieldTbl_MahzevaFax	FieldNameFaxSize  	TBCDFieldTbl_MahzevaDiscountPercent	FieldNameDiscountPercent	Precision Size  	TBCDFieldTbl_MahzevaMasBamakor	FieldName
MasBamakor	Precision Size  TIntegerFieldTbl_MahzevaTnaeyTashlumDefaultExpression0	FieldNameTnaeyTashlum  TStringFieldTbl_MahzevaMisHan	FieldNameMisHan  TIntegerFieldTbl_MahzevaCodeMehironDefaultExpression0	FieldNameCodeMehiron  TDateTimeFieldTbl_MahzevaLastUpdate	FieldName
LastUpdate  TStringFieldTbl_MahzevaMaklidName	FieldName
MaklidName  TStringFieldTbl_MahzevaString_1	FieldNameString_1Size  TStringFieldTbl_MahzevaString_2	FieldNameString_2Size  TStringFieldTbl_MahzevaString_3	FieldNameString_3Size  TStringFieldTbl_MahzevaString_4	FieldNameString_4Size  TStringFieldTbl_MahzevaString_5	FieldNameString_5Size  TIntegerFieldTbl_MahzevaInteger_1	FieldName	Integer_1  TIntegerFieldTbl_MahzevaInteger_2	FieldName	Integer_2  TIntegerFieldTbl_MahzevaInteger_3	FieldName	Integer_3  TIntegerFieldTbl_MahzevaInteger_4	FieldName	Integer_4  TIntegerFieldTbl_MahzevaInteger_5	FieldName	Integer_5  TDateTimeFieldTbl_MahzevaDate_1	FieldNameDate_1  TDateTimeFieldTbl_MahzevaDate_2	FieldNameDate_2  TDateTimeFieldTbl_MahzevaDate_3	FieldNameDate_3  TDateTimeFieldTbl_MahzevaDate_4	FieldNameDate_4  TDateTimeFieldTbl_MahzevaDate_5	FieldNameDate_5  	TBCDFieldTbl_MahzevaBcd_1	FieldNameBcd_1	Precision Size  	TBCDFieldTbl_MahzevaBcd_2	FieldNameBcd_2	Precision Size  	TBCDFieldTbl_MahzevaBcd_3	FieldNameBcd_3	Precision Size  	TBCDFieldTbl_MahzevaBcd_4	FieldNameBcd_4	Precision Size  	TBCDFieldTbl_MahzevaBcd_5	FieldNameBcd_5	Precision Size  TIntegerFieldTbl_MahzevaX	FieldNameX  TIntegerFieldTbl_MahzevaY	FieldNameY  TIntegerFieldTbl_MahzevaNodeNumB	FieldNameNodeNumB  TIntegerFieldTbl_MahzevaNodeSugSystem	FieldNameNodeSugSystem  TIntegerFieldTbl_MahzevaNodeKodCityName	FieldNameNodeKodCityName   TAtmAdvSearchAtmAdvSearch_MahzevaCaption�����KeyIndex SourceDataSetDM_AtmCrt.Qry_TempShowFields.StringsCodeMahzevaNameMahzeva ShowFieldsHeader.Strings	��� ������� ����� ShowFieldsDisplayWidth.Strings100150 Sql.StringsSelect CODEMAHZEVA,NAMEMAHZEVAFrom MAHZEVA  ReturnFieldIndex SeparateChar,IniFileNameFrm_Mahzeva.ini
IniSectionAtmAdvSearch_MahzevaKeepListLeft� Top  
TUpdateSQLUpdateSQL_MahzevaModifySQL.Stringsupdate Mahzevaset  CodeMahzeva = :CodeMahzeva,  GroupMahzeva = :GroupMahzeva,  NameMahzeva = :NameMahzeva,  Address = :Address,  City = :City,  ZipCode = :ZipCode,  Phone1 = :Phone1,  Phone2 = :Phone2,  Fax = :Fax,%  DiscountPercent = :DiscountPercent,  MasBamakor = :MasBamakor,  TnaeyTashlum = :TnaeyTashlum,  MisHan = :MisHan,  CodeMehiron = :CodeMehiron,	  X = :X,	  Y = :Y,  NodeNumB = :NodeNumB,!  NodeSugSystem = :NodeSugSystem,%  NodeKodCityName = :NodeKodCityName,  String_1 = :String_1,  String_2 = :String_2,  String_3 = :String_3,  String_4 = :String_4,  String_5 = :String_5,  Integer_1 = :Integer_1,  Integer_2 = :Integer_2,  Integer_3 = :Integer_3,  Integer_4 = :Integer_4,  Integer_5 = :Integer_5,  Date_1 = :Date_1,  Date_2 = :Date_2,  Date_3 = :Date_3,  Date_4 = :Date_4,  Date_5 = :Date_5,  Bcd_1 = :Bcd_1,  Bcd_2 = :Bcd_2,  Bcd_3 = :Bcd_3,  Bcd_4 = :Bcd_4,  Bcd_5 = :Bcd_5,  LastUpdate = :LastUpdate,  MaklidName = :MaklidNamewhere   CodeMahzeva = :OLD_CodeMahzeva InsertSQL.Stringsinsert into MahzevaK  (CodeMahzeva, GroupMahzeva, NameMahzeva, Address, City, ZipCode, Phone1, P   Phone2, Fax, DiscountPercent, MasBamakor, TnaeyTashlum, MisHan, CodeMehiron, G   X, Y, NodeNumB, NodeSugSystem, NodeKodCityName, String_1, String_2, M   String_3, String_4, String_5, Integer_1, Integer_2, Integer_3, Integer_4, K   Integer_5, Date_1, Date_2, Date_3, Date_4, Date_5, Bcd_1, Bcd_2, Bcd_3, (   Bcd_4, Bcd_5, LastUpdate, MaklidName)valuesI  (:CodeMahzeva, :GroupMahzeva, :NameMahzeva, :Address, :City, :ZipCode, I   :Phone1, :Phone2, :Fax, :DiscountPercent, :MasBamakor, :TnaeyTashlum, O   :MisHan, :CodeMehiron, :X, :Y, :NodeNumB, :NodeSugSystem, :NodeKodCityName, R   :String_1, :String_2, :String_3, :String_4, :String_5, :Integer_1, :Integer_2, K   :Integer_3, :Integer_4, :Integer_5, :Date_1, :Date_2, :Date_3, :Date_4, M   :Date_5, :Bcd_1, :Bcd_2, :Bcd_3, :Bcd_4, :Bcd_5, :LastUpdate, :MaklidName) DeleteSQL.Stringsdelete from Mahzevawhere   CodeMahzeva = :OLD_CodeMahzeva Left� Top!   
object FormWinTudaNew: TFormWinTudaNew
  Left = -4
  Top = -4
  Width = 808
  Height = 580
  Caption = 'FormWinTudaNew'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl_Main: TPageControl
    Left = 0
    Top = 29
    Width = 800
    Height = 486
    ActivePage = TS_Tnua
    Align = alClient
    HotTrack = True
    Style = tsFlatButtons
    TabOrder = 0
    object TS_Tnua: TTabSheet
      Caption = '����� ������'
      ImageIndex = -1
      object Splitter1: TSplitter
        Left = 0
        Top = 76
        Width = 792
        Height = 3
        Cursor = crVSplit
        Align = alTop
        Beveled = True
      end
      object Lbl_Remark: TLabel
        Left = 0
        Top = 0
        Width = 792
        Height = 13
        Align = alTop
        Caption = '�����'
      end
      object Panel_Top: TPanel
        Left = 0
        Top = 13
        Width = 792
        Height = 63
        Align = alTop
        BevelInner = bvLowered
        ParentColor = True
        TabOrder = 0
        object Panel1: TPanel
          Left = 2
          Top = 2
          Width = 788
          Height = 57
          Align = alTop
          BevelInner = bvLowered
          ParentColor = True
          TabOrder = 0
          object Lbl_Hazmana: TLabel
            Left = 589
            Top = 8
            Width = 31
            Height = 13
            Caption = '�����'
          end
          object Lbl_AzmnSugHazmana: TLabel
            Left = 479
            Top = 10
            Width = 41
            Height = 13
            Caption = '�.�����'
          end
          object Lbl_FromAzmnDate: TLabel
            Left = 315
            Top = 10
            Width = 40
            Height = 13
            Caption = '������'
          end
          object Lbl_AzmnFromTime: TLabel
            Left = 185
            Top = 8
            Width = 30
            Height = 13
            Caption = '����'
          end
          object Lbl_ToAzmnDate: TLabel
            Left = 315
            Top = 34
            Width = 50
            Height = 13
            Caption = '�� �����'
          end
          object Lbl_AzmnToTime: TLabel
            Left = 184
            Top = 32
            Width = 40
            Height = 13
            Caption = '�� ���'
          end
          object Lbl_ShemLakoach: TLabel
            Left = 579
            Top = 33
            Width = 41
            Height = 13
            Hint = '���� �����'
            Caption = '<�����>'
          end
          object Lbl_AzmnAsmachta: TLabel
            Left = 80
            Top = 36
            Width = 44
            Height = 13
            Caption = '������'
          end
          object DBText_LakName: TDBText
            Left = 368
            Top = 33
            Width = 151
            Height = 17
            Color = clInfoBk
            DataField = 'LakName'
            DataSource = DM_WinTuda.DS_Azmn
            ParentColor = False
            Visible = False
          end
          object Lbl_InvName: TLabel
            Left = 482
            Top = 33
            Width = 16
            Height = 13
            Hint = '���� �����'
            Caption = '��'
          end
          object AtmDbHEdit_Hazmana: TAtmDbHEdit
            Left = 522
            Top = 6
            Width = 58
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'AzmnNo'
            DataSource = DM_WinTuda.DS_Azmn
            ParentBiDiMode = False
            TabOrder = 0
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_Hazmana
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = True
          end
          object AtmDBDateEdit_AzmnFromDate: TAtmDBDateEdit
            Left = 225
            Top = 6
            Width = 89
            Height = 21
            DataField = 'FromDate'
            DataSource = DM_WinTuda.DS_Azmn
            CheckOnExit = True
            DialogTitle = '��� �����'
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            NumGlyphs = 2
            TabOrder = 4
            StartOfWeek = Sun
            Weekends = [Sat]
            YearDigits = dyFour
            LinkLabel = Lbl_FromAzmnDate
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            F9String = '30/12/1899'
            IsFixed = False
            FixedColor = clBtnFace
            StatusBarPanelNum = -1
          end
          object AtmDBDateEdit_AzmnToDate: TAtmDBDateEdit
            Left = 225
            Top = 31
            Width = 89
            Height = 21
            DataField = 'ToDate'
            DataSource = DM_WinTuda.DS_Azmn
            CheckOnExit = True
            DialogTitle = '��� �����'
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            NumGlyphs = 2
            TabOrder = 5
            StartOfWeek = Sun
            Weekends = [Sat]
            YearDigits = dyFour
            LinkLabel = Lbl_ToAzmnDate
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            F9String = '30/12/1899'
            IsFixed = False
            FixedColor = clBtnFace
            StatusBarLink = StatusBar1
            StatusBarString = 'F5-���� ��� ������'
            StatusBarPanelNum = 4
          end
          object AtmDbHEdit_Lak: TAtmDbHEdit
            Left = 522
            Top = 31
            Width = 58
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'InvLakNo'
            DataSource = DM_WinTuda.DS_Azmn
            ParentBiDiMode = False
            TabOrder = 1
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_ShemLakoach
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_Lako
            SelectNextAfterSearch = True
            StatusBarLink = StatusBar1
            StatusBarString = '���� �����'
            StatusBarPanelNum = 4
            Hebrew = False
            F9String = '1'
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_AzmnAsmachta: TAtmDbHEdit
            Left = 5
            Top = 32
            Width = 71
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Asmcta'
            DataSource = DM_WinTuda.DS_Azmn
            ParentBiDiMode = False
            TabOrder = 8
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_AzmnAsmachta
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object MaskEdit_FromTime: TMaskEdit
            Left = 133
            Top = 6
            Width = 49
            Height = 21
            BiDiMode = bdLeftToRight
            EditMask = '99:99'
            MaxLength = 5
            ParentBiDiMode = False
            TabOrder = 6
            Text = '  :  '
          end
          object MaskEdit_ToTime: TMaskEdit
            Left = 133
            Top = 31
            Width = 49
            Height = 21
            BiDiMode = bdLeftToRight
            EditMask = '99:99'
            MaxLength = 5
            ParentBiDiMode = False
            TabOrder = 7
            Text = '  :  '
          end
          object AtmDbComboBox_azmnType: TAtmDbComboBox
            Left = 367
            Top = 6
            Width = 111
            Height = 21
            TabStop = False
            Color = clInfoBk
            ItemHeight = 13
            Sorted = True
            TabOrder = 3
            DataSource = DM_WinTuda.DS_Azmn
            DataField = 'AzmnType'
            SugTavlaIndex = 0
          end
          object AtmDbHEdit_InvLakName: TAtmDbHEdit
            Left = 369
            Top = 31
            Width = 110
            Height = 21
            TabStop = False
            BiDiMode = bdRightToLeft
            DataField = 'InvName'
            DataSource = DM_WinTuda.DS_Azmn
            ParentBiDiMode = False
            TabOrder = 2
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_InvName
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = True
            LocateRecordOnChange = False
          end
          object Panel_Days: TPanel
            Left = 2
            Top = 2
            Width = 121
            Height = 30
            TabOrder = 9
            object Label8: TLabel
              Left = 106
              Top = 0
              Width = 8
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object Label10: TLabel
              Left = 90
              Top = 0
              Width = 7
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object Label11: TLabel
              Left = 75
              Top = 0
              Width = 6
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object Label12: TLabel
              Left = 58
              Top = 0
              Width = 7
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object Label13: TLabel
              Left = 42
              Top = 0
              Width = 7
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object Label14: TLabel
              Left = 28
              Top = 0
              Width = 5
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object Label15: TLabel
              Left = 7
              Top = 0
              Width = 9
              Height = 13
              Caption = '�'
              Transparent = True
            end
            object DBCheckBox_Day1: TDBCheckBox
              Left = 98
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day1'
              DataField = 'Sunday'
              DataSource = DM_WinTuda.DS_Azmn
              TabOrder = 0
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox_Day2: TDBCheckBox
              Left = 82
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day2'
              Ctl3D = True
              DataField = 'Monday'
              DataSource = DM_WinTuda.DS_Azmn
              ParentCtl3D = False
              TabOrder = 1
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox_Day3: TDBCheckBox
              Left = 66
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day3'
              DataField = 'Tuesday'
              DataSource = DM_WinTuda.DS_Azmn
              TabOrder = 2
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox_Day4: TDBCheckBox
              Left = 50
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day4'
              DataField = 'Wednesday'
              DataSource = DM_WinTuda.DS_Azmn
              TabOrder = 3
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox_Day5: TDBCheckBox
              Left = 34
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day5'
              DataField = 'Thursday'
              DataSource = DM_WinTuda.DS_Azmn
              TabOrder = 4
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox_Day6: TDBCheckBox
              Left = 18
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day6'
              DataField = 'Friday'
              DataSource = DM_WinTuda.DS_Azmn
              TabOrder = 5
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox_Day7: TDBCheckBox
              Left = 2
              Top = 11
              Width = 17
              Height = 18
              TabStop = False
              Caption = 'DBCheckBox_Day7'
              DataField = 'Saturday'
              DataSource = DM_WinTuda.DS_Azmn
              TabOrder = 6
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
        end
      end
      object ToolBar_Tnua: TToolBar
        Left = 0
        Top = 429
        Width = 792
        Height = 26
        Align = alBottom
        AutoSize = True
        ButtonHeight = 24
        Caption = 'ToolBar_Tnua'
        Flat = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = True
        object DBNavigator: TAtmDbNavigator
          Left = 0
          Top = 0
          Width = 198
          Height = 24
          VisibleBtns.nbBkSet = False
          VisibleBtns.nbBkGoto = False
          VisibleBtns.nbBkClear = False
          VisibleBtns.nbPriorSet = False
          VisibleBtns.nbNextSet = False
          VisibleBtns.nbDelete = False
          DataSource = DM_WinTuda.DS_Azmn
          Flat = True
          Ctl3D = False
          Hints.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '����� ������-(Ctrl+Home)'
            '8'
            '����� �����-(PgUp)'
            '����� ����-(PgDn)'
            '11'
            '����� ������-(Ctrl+End)'
            '����� ����-(F11)'
            '14'
            '���� �����'
            '���� �����-(F3)'
            '��� �������(Shift+Esc)'
            '����')
          ParentCtl3D = False
          TabOrder = 1
        end
        object TBtn_NewHeshbon: TToolButton
          Left = 198
          Top = 0
        end
        object TBtn_NewTnua: TToolButton
          Left = 221
          Top = 0
        end
        object Btn_PostNew: TBitBtn
          Left = 244
          Top = 0
          Width = 53
          Height = 24
          Caption = '����'
          ModalResult = 1
          TabOrder = 0
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object ToolButton1: TToolButton
          Left = 297
          Top = 0
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 2
          Style = tbsSeparator
        end
        object SpeedBar1: TSpeedBar
          Left = 305
          Top = 0
          Width = 284
          Height = 24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Position = bpCustom
          Align = alClient
          Options = [sbAllowDrag, sbAllowResize, sbFlatBtns, sbTransparentBtns]
          BtnOffsetHorz = 3
          BtnOffsetVert = 3
          BtnWidth = 27
          BtnHeight = 23
          DragMode = dmAutomatic
          TabOrder = 2
          InternalVer = 1
          object SpeedbarSection2: TSpeedbarSection
            Caption = '������'
          end
          object SpeedbarSection1: TSpeedbarSection
            Caption = '������'
          end
          object SpeedbarSection3: TSpeedbarSection
            Caption = '������'
          end
          object SpeedbarSection4: TSpeedbarSection
            Caption = '�����'
          end
          object SpeedbarSection5: TSpeedbarSection
            Caption = '��������'
          end
          object SpeedItem1: TSpeedItem
            BtnCaption = '��� ��"� ������'
            Caption = '��� ��"� ������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              0000000000000000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C000000001F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C000000001F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              0000000000000000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '��� ��"� ������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem2: TSpeedItem
            BtnCaption = '���� ����� ������'
            Caption = '���� ����� ������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C0F001F7C0F001F7C0F001F7C0F001F7C0F001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              0F000F000F001F7C1F7C1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C0000
              000000000000000000001F7C1F7C000000000000000000000000000000000000
              FF7FE07FFF7FE07F0000000000000000FF7FE07FFF7FE07F00000000FF7F0000
              000000000000000000000000FF7F0000000000000000000000000000FF7FFF7F
              FF7FFF7FFF7F00001F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C0000FF7FEF3D
              EF3DEF3DFF7F00001F7C0000FF7FEF3DEF3DEF3DFF7F00001F7C0000FF7FFF7F
              FF7FFF7FFF7F00001F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C0000FF7FEF3D
              EF3DEF3DFF7F00001F7C0000FF7FEF3DEF3DEF3DFF7F00001F7C0000FF7FFF7F
              FF7FFF7FFF7F00001F7C0000FF7FFF7FFF7FFF7FFF7F00001F7C000000000000
              00000000000000001F7C00000000000000000000000000001F7C}
            Hint = '���� ����� ������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem3: TSpeedItem
            BtnCaption = '����� �����'
            Caption = '����� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF031F0000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C0000FF031F001F0000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0000FF031F001F0000001F7C1F7C1F7C1F7C1F7C1F7C
              00000000000000000000FF031F001F0000001F7C1F7C1F7C1F7C1F7C1F7C0000
              1002100210021002100200001F0000001F7C1F7C1F7C1F7C1F7C1F7C10421002
              FF03FF7FFF03FF7F1002100200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1042FF03
              FF7FFF03FF7FFF03FF7F100200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1042FF7F
              FF03FF7FFF03FF7FFF03100200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1042FF03
              FF7FFF03FF7FFF03FF7F100200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1042FF7F
              FF03FF7FFF03FF7FFF03100200001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042
              FF7FFF03FF7FFF03FF7F00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              104210421042104210421F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� ����� ���  ��� �����'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem4: TSpeedItem
            BtnCaption = '���� �����'
            Caption = '���� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
              00000000000000000000000000000000000000000000000000000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1F7C00001F7CEF3D00000000FF7F0000
              0000000000000000FF7FFF7F0000FF7F000000000000EF3D00000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000FF7F0000
              00000000000000000000FF7F00000000000000000000000000000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000FF7F0000
              000000000000FF7FFF7FFF7F0000FF7F000000000000EF3D00000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1F7C00001F7CEF3D00000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F0000000000000000
              00000000000000000000000000000000000000000000000000000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F0000000000001F7C0000000000000000FF7FFF7F
              FF7FFF7FFF7FFF7FFF7FFF7F000000001F7C1F7C1F7C00000000000000000000
              0000000000000000000000000000000000000000000000000000}
            Hint = '���� �����'
            Spacing = 1
            Left = 3
            Top = 3
            Visible = True
            SectionName = '������'
          end
          object SpeedItem12: TSpeedItem
            BtnCaption = '����� �����'
            Caption = '����� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              0000EF01EF0100000000EF0100001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00000000FF7FFF7F00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C0000FF7FFF7F000000000F3C00000F3C00001F7C1F7C1F7C1F7C1F7C1F7C
              000000000000FF7FFF7FFF7F0000F75E00001F7C1F7C1F7C1F7C1F7C1F7C0000
              F75EF75EFF7FFF7FFF7FFF7F0000F75E00001F7C1F7C1F7C1F7C1F7C1F7C0000
              F75EF75E0000FF7FFF7FFF7FFF7F0000F75E00001F7C1F7C1F7C1F7C1F7C0000
              F75EF75E0000FF7FFF7F00000000F75EF75E00001F7C1F7C1F7C1F7C1F7C0000
              F75EF75EF75E0000FF7FFF7F0000F75EF75EF75E00001F7C1F7C1F7C1F7C1F7C
              0000F75EF75EF75EF75EF75E0000F75E000000001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00000000000000000000EF3DF75EF75E00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� �����'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem13: TSpeedItem
            BtnCaption = '����� �����'
            Caption = '����� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CEF3DEF3D
              EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D1F7C1F7C1F7C0000
              000000001F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C0000
              003C003C003C003C003C003C003C003C003C003C00001F7C1F7C1F7C1F7C007C
              007C007CEF3D007CEF3DEF3D007CEF3D007C007C003C1F7C1F7C1F7C1F7C007C
              FF03FF03007C007C007C007C007C007CFF03FF03003C1F7C1F7C1F7C1F7C007C
              007C007C007C007C007C007C007C007C007C007C007C1F7C1F7C1F7C003C007C
              007CFF03FF7FFF03FF7FFF03FF7FFF03FF7F007C007C003C1F7C1F7C007C1F7C
              007CFF7FFF03FF7FFF03FF7FFF03FF7FFF03007C1F7C007C1F7C1F7C1F7C1F7C
              1F7C007CFF7FFF03FF7FFF03FF7FFF03007C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C007C007C007C007C007C007C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� �����'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem14: TSpeedItem
            BtnCaption = '����� ������'
            Caption = '����� ������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C003C003C003C003C003C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C007C007C007C003CEF3D007C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C003C003CF75EF75EEF3D1F7C1F7C1F7C1F7C1F7C1F7C
              0000000000001F7C1F7C1F7C0000F75EF75E000000001F7C1F7C1F7C1F7C1F7C
              E03DEF3DE03D00000000000000000000F75EF75EF75E00001F7C1F7C1F7C0000
              F75EFF7FFF7FEF3D0000000000000000F75EF75EF75E00001F7C1F7C1F7CE03D
              EF3DFF7FFF7FFF7F000000000000F75EF75EF75EF75EEF3D1F7C1F7C0000F75E
              FF7FFF7FF75EFF7F00000000000000000000F75E000000001F7C1F7CE03DEF3D
              FF7FFF7FF75EFF7F000000000000000000000000F75EEF3D00000000F75EFF7F
              FF7FF75EFF7FFF7FFF7F00000000000000000000000000000000E03DEF3DFF7F
              FF7FF75EFF7FFF7FEF3DFF7F0000000000000000000000001F7C1F7CFF7FFF7F
              F75EFF7FFF7FFF7FF75EFF7FFF7FFF7FF75EFF7FFF7F1F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FEF3DFF7FF75EFF7FF75EF75EFF7F00001F7C1F7C1F7C1F7C1F7C
              1F7C000000000000FF7FFF7FFF7FF75EFF7FFF7F1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C0000FF7FF75EF75EFF7F00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7F1F7C1F7C1F7C1F7C}
            Hint = '����� ������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem15: TSpeedItem
            BtnCaption = '������'
            Caption = '������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem16: TSpeedItem
            BtnCaption = '����� �������'
            Caption = '����� �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7F00001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0F000F000F000F000000FF7FFF7F00001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C000000000000
              00001F7C1F7C1F7C1F7C0F001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7F
              00000F000F000F000F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7F
              00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
              00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� �������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem20: TSpeedItem
            BtnCaption = '������'
            Caption = '������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C7A10F5207A101F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C7A10D02424282A39F5201F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7CB918D0242428212C2A395352D024D024D024D024B9181F7C1F7C1F7C
              B918D0242428212C637063702A3953522A39212C2A39212C53521F7C7A10D024
              24282A392A39637063706370637053522A392A3939672A3939677A102428212C
              2A3953522A39637063706370212C212C535253522A39F5207A102A3963706370
              5352535253526370212C212C212C2A3953522A39EE28B9181F7C212C63706370
              535239673967212C212C212C6370637053522A39F5201F7C1F7C535263706370
              2A39396739672A3963706370637063702A39EE283A081F7C1F7C3967212C212C
              212C535239675352637063706370212CEE283A081F7C1F7C1F7C5352212C212C
              63706370396753522A39EE28EE28B9181F7C1F7C1F7C1F7C1F7CB918436C6370
              63706370212CEE28B9187A101F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CEE28436C
              6370212CF5201A041F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CB918
              F5201A041F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� ������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem18: TSpeedItem
            BtnCaption = '������'
            Caption = '������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
              00000000000000000000000000000000000000000000000000000000FF7FFF7F
              EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F00000000FF7FFF7F
              EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F00000000EF3DEF3D
              EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D00000000FF7FFF7F
              EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F00000000FF7FFF7F
              EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F00000000EF3DEF3D
              EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D00000000FF7FFF7F
              EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F00000000FF7FFF7F
              EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F00000000EF3DEF3D
              EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D000000000F000F00
              0F000F000F000F000F000F000F000F000F000F000F000F00000000000F000F00
              0F000F000F000F000F000F000F000F000F000F000F000F000000000000000000
              00000000000000000000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '������ ������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem17: TSpeedItem
            BtnCaption = '���� 1 �������'
            Caption = '���� 1 �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '���� 1 �������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem6: TSpeedItem
            BtnCaption = '���� 1 + ���� 2 �������'
            Caption = '���� 1 + ���� 2 �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '���� 1 + ���� 2 �������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem7: TSpeedItem
            BtnCaption = '���� 2 �������'
            Caption = '���� 2 �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '���� 2 �������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem8: TSpeedItem
            BtnCaption = '���� 3 �������'
            Caption = '���� 3 �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '���� 3 �������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem9: TSpeedItem
            BtnCaption = '���� 4 �������'
            Caption = '���� 4 �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '���� 4 �������'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem10: TSpeedItem
            BtnCaption = '���� ��"�'
            Caption = '���� ��"�'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '���� ��"�'
            Spacing = 1
            Top = 3
            SectionName = '������'
          end
          object SpeedItem11: TSpeedItem
            BtnCaption = '��� ������'
            Caption = '��� ������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '��� ������'
            Spacing = 1
            Left = 30
            Top = 3
            Visible = True
            SectionName = '������'
          end
          object SpeedItem5: TSpeedItem
            BtnCaption = '�� �������'
            Caption = '�� �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7CE0031F7C
              E0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C1F7C00000000
              0000000000000000000000000000000000000000000000000000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E0000E0030000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E00001F7C0000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E0000E0030000F75E
              EF3D0000F75EEF3D0000F75EEF3D0000F75EEF3D0000F75E00001F7C0000F75E
              FF7F0000F75EFF7F0000F75EFF7F0000F75EFF7F0000F75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75EF75EF75E00001F7C0000F75E
              000000000000000000000000000000000000F75EF75EF75E0000E0030000F75E
              F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E00001F7CE0030000
              000000000000000000000000000000000000000000000000E003E0031F7CE003
              1F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7CE0031F7C}
            Hint = '�� �������'
            Spacing = 1
            Left = 57
            Top = 3
            Visible = True
            SectionName = '������'
          end
          object SpeedItem19: TSpeedItem
            BtnCaption = '����� �������'
            Caption = '����� �������'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C10421042104210421042104210421042104210421F7C1F7C1F7C1F7C0000
              000000000000000000000000000000000000000010421F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000010421F7C1F7C1F7C1F7C0000
              FF7F10421000FF7F1042100010001000FF7F000010421F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000010421F7C1F7C1F7C1F7C0000
              FF7F1000FF7F10001000100010001000FF7F000010421F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000010421F7C1F7C1F7C1F7C0000
              FF7F1000FF7F10001000100010001000FF7F000010421F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000010421F7C1F7C1F7C1F7C0000
              FF7F1000FF7F10001000100010001000FF7F000010421F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000010421F7C1F7C1F7C1F7C0000
              FF7F10421042FF7F1000100010001000FF7F000010421F7C1F7C1F7C1F7C0000
              FF7F10001000FF7FFF7FFF7F00000000000000001F7C1F7C1F7C1F7C1F7C0000
              FF7F10421042FF7F100010000000186300001F7C1F7C1F7C1F7C1F7C1F7C0000
              FF7FFF7FFF7FFF7FFF7FFF7F000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
              00000000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� �������'
            Spacing = 1
            Left = 84
            Top = 3
            Visible = True
            SectionName = '�����'
          end
          object SpeedItem21: TSpeedItem
            BtnCaption = '����� �-� ����� �����'
            Caption = '����� �-� ����� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C
              0000000000001F7C1F7C1F7C00001F7C1F7C1F7C0F3C1F7C1F7C1F7C00000000
              1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C0F3C0F3C1F7C1F7C1F7C1F7C0000
              1F7C00000000000000001F7C1F7C1F7C0F3C0F3C0F3C0F3C1F7C1F7C1F7C0000
              1F7C1F7C1F7C1F7C00001F7C1F7C0F3C0F3C0F3C0F3C0F3C0F3C1F7C00000000
              1F7C1F7C1F7C1F7C0000000000001F7C0F3C0F3C0F3C0F3C0F3C000000000000
              00001F7C1F7C1F7C1F7C00001F7C00001F7C0F3C0F3C1F7C0F3C000000000000
              1F7C1F7C1F7C000000001F7C1F7C00001F7C1F7C0F3C1F7C0F3C000000000000
              00001F7C1F7C1F7C00001F7C1F7C1F7C00001F7C1F7C1F7C0F3C000000000000
              000000000000000000001F7C000000001F7C1F7C1F7C1F7C0F3C1F7C00000000
              000000000000000000001F7C1F7C00001F7C1F7C1F7C0F3C1F7C1F7C1F7C0000
              00000000000000000000000000001F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C00000000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '����� �-� ����� �����'
            Spacing = 1
            Top = 3
            SectionName = '��������'
          end
          object SpeedItem22: TSpeedItem
            BtnCaption = '���� ������� �����'
            Caption = '���� ������� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C
              0000000000001F7C1F7C1F7C00001F7C1F7C1F7C0F3C1F7C1F7C1F7C00000000
              1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C0F3C0F3C1F7C1F7C1F7C1F7C0000
              1F7C00000000000000001F7C1F7C1F7C0F3C0F3C0F3C0F3C1F7C1F7C1F7C0000
              1F7C1F7C1F7C1F7C00001F7C1F7C0F3C0F3C0F3C0F3C0F3C0F3C1F7C00000000
              1F7C1F7C1F7C1F7C0000000000001F7C0F3C0F3C0F3C0F3C0F3C000000000000
              00001F7C1F7C1F7C1F7C00001F7C00001F7C0F3C0F3C1F7C0F3C000000000000
              1F7C1F7C1F7C000000001F7C1F7C00001F7C1F7C0F3C1F7C0F3C000000000000
              00001F7C1F7C1F7C00001F7C1F7C1F7C00001F7C1F7C1F7C0F3C000000000000
              000000000000000000001F7C000000001F7C1F7C1F7C1F7C0F3C1F7C00000000
              000000000000000000001F7C1F7C00001F7C1F7C1F7C0F3C1F7C1F7C1F7C0000
              00000000000000000000000000001F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C00000000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '���� ������� �����'
            Spacing = 1
            Top = 3
            SectionName = '��������'
          end
          object SpeedItem23: TSpeedItem
            BtnCaption = '���� �-� ����� �����'
            Caption = '���� �-� ����� �����'
            Glyph.Data = {
              36020000424D3602000000000000360000002800000010000000100000000100
              10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C
              0000000000001F7C1F7C1F7C00001F7C1F7C1F7C0F3C1F7C1F7C1F7C00000000
              1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C0F3C0F3C1F7C1F7C1F7C1F7C0000
              1F7C00000000000000001F7C1F7C1F7C0F3C0F3C0F3C0F3C1F7C1F7C1F7C0000
              1F7C1F7C1F7C1F7C00001F7C1F7C0F3C0F3C0F3C0F3C0F3C0F3C1F7C00000000
              1F7C1F7C1F7C1F7C0000000000001F7C0F3C0F3C0F3C0F3C0F3C000000000000
              00001F7C1F7C1F7C1F7C00001F7C00001F7C0F3C0F3C1F7C0F3C000000000000
              1F7C1F7C1F7C000000001F7C1F7C00001F7C1F7C0F3C1F7C0F3C000000000000
              00001F7C1F7C1F7C00001F7C1F7C1F7C00001F7C1F7C1F7C0F3C000000000000
              000000000000000000001F7C000000001F7C1F7C1F7C1F7C0F3C1F7C00000000
              000000000000000000001F7C1F7C00001F7C1F7C1F7C0F3C1F7C1F7C1F7C0000
              00000000000000000000000000001F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C00000000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C}
            Hint = '���� �-� ����� �����'
            Spacing = 1
            Top = 3
            SectionName = '��������'
          end
        end
      end
      object PageControl_Tnua: TPageControl
        Left = 0
        Top = 79
        Width = 792
        Height = 109
        ActivePage = TS_Tnua1
        Align = alTop
        HotTrack = True
        Style = tsFlatButtons
        TabOrder = 2
        TabStop = False
        object TS_Tnua1: TTabSheet
          Caption = '���� ������'
          ImageIndex = -1
          object Panel_TsTnuaMain: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 78
            Align = alClient
            Caption = 'Panel_TsTnuaMain'
            TabOrder = 0
            object Panel_GridTnua: TPanel
              Left = 1
              Top = 1
              Width = 782
              Height = 76
              Align = alClient
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object DBGrid_Tnua: TRxDBGrid
                Left = 0
                Top = 0
                Width = 782
                Height = 41
                Align = alClient
                BiDiMode = bdRightToLeft
                DataSource = DM_WinTuda.DS_Shib
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                ParentBiDiMode = False
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    ButtonStyle = cbsEllipsis
                    Expanded = False
                    FieldName = 'SugMetan'
                    Title.Alignment = taCenter
                    Title.Caption = '�.����'
                    Width = 37
                    Visible = True
                  end
                  item
                    ButtonStyle = cbsNone
                    Expanded = False
                    FieldName = 'TeurMitanLook1'
                    Title.Alignment = taCenter
                    Title.Caption = '���� ����'
                    Width = 59
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MaslulCode1'
                    Title.Alignment = taCenter
                    Title.Caption = '�.����'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Maslul1'
                    Title.Alignment = taCenter
                    Title.Caption = '���� ����(�����)'
                    Width = 135
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ShibRem2'
                    Title.Caption = '�����'
                    Width = 106
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ShibTotalKm'
                    Title.Alignment = taCenter
                    Title.Caption = '�"�'
                    Width = 41
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TotalHour'
                    Title.Caption = '����'
                    Width = 69
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AzmnOk'
                    Width = 0
                    Visible = True
                  end>
              end
              object Panel_ShibKind: TPanel
                Left = 0
                Top = 41
                Width = 782
                Height = 35
                Align = alBottom
                BevelWidth = 2
                Caption = '������� �������'
                Font.Charset = HEBREW_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'David'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                Visible = False
              end
            end
          end
        end
        object TS_Perut2: TTabSheet
          Caption = '������ �����'
          ImageIndex = -1
          object DBGrid_Preut2: TRxDBGrid
            Left = 0
            Top = 0
            Width = 632
            Height = 94
            Align = alClient
            DataSource = DM_WinTuda.DS_Shib
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                ButtonStyle = cbsEllipsis
                Expanded = False
                FieldName = 'SugMetan'
                Title.Alignment = taCenter
                Title.Caption = '�.����'
                Width = 40
                Visible = True
              end
              item
                ButtonStyle = cbsNone
                Expanded = False
                FieldName = 'TeurMitanLook1'
                Title.Alignment = taCenter
                Title.Caption = '��� ����'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MaslulCode1'
                Title.Alignment = taCenter
                Title.Caption = '�.����'
                Width = 47
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Maslul1'
                Title.Alignment = taCenter
                Title.Caption = '���� ����(�����)'
                Width = 135
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ShibTotalKm'
                Title.Alignment = taCenter
                Title.Caption = '�"�'
                Width = 41
                Visible = True
              end
              item
                ButtonStyle = cbsEllipsis
                Expanded = False
                FieldName = 'YeMida1'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quntity1'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Price1'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PriceQuntity1'
                Title.Caption = '��"� �'
                Width = 73
                Visible = True
              end
              item
                ButtonStyle = cbsEllipsis
                Expanded = False
                FieldName = 'YeMida2'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quntity2'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Price2'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PriceQuntity2'
                Title.Caption = '��"� �'
                Width = 76
                Visible = True
              end
              item
                ButtonStyle = cbsEllipsis
                Expanded = False
                FieldName = 'YeMida3'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quntity3'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Price3'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PriceQuntity3'
                Title.Caption = '��"� �'
                Width = 78
                Visible = True
              end
              item
                ButtonStyle = cbsEllipsis
                Expanded = False
                FieldName = 'YeMida4'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quntity4'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Price4'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PriceQuntity4'
                Title.Caption = '��"� �'
                Width = 74
                Visible = True
              end>
          end
        end
        object TS_StatusFields: TTabSheet
          Caption = '�������'
          ImageIndex = -1
          object Pnl_TabSheetExtraFields: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 78
            Align = alClient
            BevelInner = bvLowered
            BorderStyle = bsSingle
            TabOrder = 0
            object Label17: TLabel
              Left = 496
              Top = 82
              Width = 107
              Height = 13
              Hint = '��� ����� ���'
              Caption = '��� ����� ���(�����)'
            end
            object Label1: TLabel
              Left = 56
              Top = 106
              Width = 55
              Height = 13
              Hint = '��� ����� ����'
              Caption = '���� ����'
            end
            object Label3: TLabel
              Left = 63
              Top = 82
              Width = 48
              Height = 13
              Hint = '��� ����� ���'
              Caption = '��� ����'
            end
            object DBRG_ShibKind: TDBRadioGroup
              Left = 5
              Top = 4
              Width = 167
              Height = 70
              Caption = '��� �����'
              Columns = 2
              DataField = 'ShibKind'
              DataSource = DM_WinTuda.DS_Shib
              Items.Strings = (
                '�����'
                '���� ����'
                '���� ���'
                '����� ����'
                '���� �����')
              TabOrder = 0
              Values.Strings = (
                '-1'
                '1'
                '2'
                '3'
                '4')
            end
            object GroupBox1: TGroupBox
              Left = 174
              Top = 4
              Width = 432
              Height = 70
              Caption = '������'
              TabOrder = 1
              object Label18: TLabel
                Left = 345
                Top = 14
                Width = 82
                Height = 13
                Hint = '��� ����� ����'
                Caption = '��� ����� ����'
              end
              object Label31: TLabel
                Left = 353
                Top = 46
                Width = 74
                Height = 13
                Hint = '��� ����� ���'
                Caption = '��� ����� ���'
              end
              object Label16: TLabel
                Left = 261
                Top = 30
                Width = 45
                Height = 13
                Hint = '���� �������'
                Caption = '�������'
              end
              object Label19: TLabel
                Left = 167
                Top = 30
                Width = 33
                Height = 13
                Hint = '����� �������'
                Caption = '�����'
              end
              object Label27: TLabel
                Left = 58
                Top = 28
                Width = 46
                Height = 13
                Hint = '���� �����'
                Caption = '�-� ����'
              end
              object AtmDbHEdit_KBLak: TAtmDbHEdit
                Left = 305
                Top = 10
                Width = 40
                Height = 21
                BiDiMode = bdLeftToRight
                DataField = 'LakCodeBizua'
                DataSource = DM_WinTuda.DS_Shib
                ParentBiDiMode = False
                TabOrder = 0
                UseF2ToRunSearch = False
                IsFixed = False
                IsMust = False
                DefaultKind = dkString
                KeyToRunSearch = ksF2
                LinkLabel = Label18
                LabelColorSelect = clActiveCaption
                LabelTextColorSelect = clCaptionText
                FixedColor = clBtnFace
                NormalColor = clWindow
                SelectNextAfterSearch = True
                StatusBarPanelNum = -1
                Hebrew = False
                LocateRecordOnChange = False
              end
              object AtmDbHEdit_KBDriver: TAtmDbHEdit
                Left = 305
                Top = 40
                Width = 40
                Height = 21
                BiDiMode = bdLeftToRight
                DataField = 'DriverCodeBizua'
                DataSource = DM_WinTuda.DS_Shib
                ParentBiDiMode = False
                TabOrder = 1
                UseF2ToRunSearch = False
                IsFixed = False
                IsMust = False
                DefaultKind = dkString
                KeyToRunSearch = ksF2
                LinkLabel = Label31
                LabelColorSelect = clActiveCaption
                LabelTextColorSelect = clCaptionText
                FixedColor = clBtnFace
                NormalColor = clWindow
                SelectNextAfterSearch = True
                StatusBarPanelNum = -1
                Hebrew = False
                LocateRecordOnChange = False
              end
              object AtmDbHEdit_Heshbonit: TAtmDbHEdit
                Left = 202
                Top = 26
                Width = 59
                Height = 21
                Hint = '���� �������'
                TabStop = False
                BiDiMode = bdLeftToRight
                Ctl3D = True
                DataField = 'LakHesbonit'
                DataSource = DM_WinTuda.DS_Shib
                ParentBiDiMode = False
                ParentCtl3D = False
                TabOrder = 2
                UseF2ToRunSearch = False
                IsFixed = False
                IsMust = False
                DefaultKind = dkString
                KeyToRunSearch = ksF2
                LinkLabel = Label16
                LabelColorSelect = clActiveCaption
                LabelTextColorSelect = clCaptionText
                FixedColor = clBtnFace
                NormalColor = clWindow
                SelectNextAfterSearch = True
                StatusBarPanelNum = -1
                Hebrew = False
                LocateRecordOnChange = False
              end
              object AtmDbHEdit7: TAtmDbHEdit
                Left = 107
                Top = 26
                Width = 59
                Height = 21
                Hint = '����� �������'
                TabStop = False
                BiDiMode = bdLeftToRight
                Ctl3D = True
                DataField = 'LakHesbonitDate'
                DataSource = DM_WinTuda.DS_Shib
                ParentBiDiMode = False
                ParentCtl3D = False
                TabOrder = 3
                UseF2ToRunSearch = False
                IsFixed = False
                IsMust = False
                DefaultKind = dkString
                KeyToRunSearch = ksF2
                LinkLabel = Label19
                LabelColorSelect = clActiveCaption
                LabelTextColorSelect = clCaptionText
                FixedColor = clBtnFace
                NormalColor = clWindow
                SelectNextAfterSearch = True
                StatusBarPanelNum = -1
                Hebrew = False
                LocateRecordOnChange = False
              end
              object AtmDbHEdit8: TAtmDbHEdit
                Left = 2
                Top = 24
                Width = 55
                Height = 21
                Hint = '���� �����'
                TabStop = False
                BiDiMode = bdLeftToRight
                Ctl3D = True
                DataField = 'DrishNo'
                DataSource = DM_WinTuda.DS_Shib
                ParentBiDiMode = False
                ParentCtl3D = False
                TabOrder = 4
                UseF2ToRunSearch = False
                IsFixed = False
                IsMust = False
                DefaultKind = dkString
                KeyToRunSearch = ksF2
                LinkLabel = Label27
                LabelColorSelect = clActiveCaption
                LabelTextColorSelect = clCaptionText
                FixedColor = clBtnFace
                NormalColor = clWindow
                SelectNextAfterSearch = True
                StatusBarPanelNum = -1
                Hebrew = False
                LocateRecordOnChange = False
              end
            end
            object AtmDbHEdit5: TAtmDbHEdit
              Left = 455
              Top = 80
              Width = 40
              Height = 21
              BiDiMode = bdLeftToRight
              DataField = 'DriverZikuiNo'
              DataSource = DM_WinTuda.DS_Shib
              ParentBiDiMode = False
              TabOrder = 2
              UseF2ToRunSearch = False
              IsFixed = False
              IsMust = False
              DefaultKind = dkString
              KeyToRunSearch = ksF2
              LinkLabel = Label17
              LabelColorSelect = clActiveCaption
              LabelTextColorSelect = clCaptionText
              FixedColor = clBtnFace
              NormalColor = clWindow
              SelectNextAfterSearch = True
              StatusBarPanelNum = -1
              Hebrew = False
              LocateRecordOnChange = False
            end
            object AtmDbHEdit2: TAtmDbHEdit
              Left = 7
              Top = 102
              Width = 40
              Height = 21
              BiDiMode = bdLeftToRight
              DataField = 'YehusMonth'
              DataSource = DM_WinTuda.DS_Shib
              ParentBiDiMode = False
              TabOrder = 3
              UseF2ToRunSearch = False
              IsFixed = False
              IsMust = False
              DefaultKind = dkString
              KeyToRunSearch = ksF2
              LinkLabel = Label1
              LabelColorSelect = clActiveCaption
              LabelTextColorSelect = clCaptionText
              FixedColor = clBtnFace
              NormalColor = clWindow
              SelectNextAfterSearch = True
              StatusBarPanelNum = -1
              Hebrew = False
              LocateRecordOnChange = False
            end
            object AtmDbHEdit3: TAtmDbHEdit
              Left = 7
              Top = 78
              Width = 40
              Height = 21
              BiDiMode = bdLeftToRight
              DataField = 'YehusYear'
              DataSource = DM_WinTuda.DS_Shib
              ParentBiDiMode = False
              TabOrder = 4
              UseF2ToRunSearch = False
              IsFixed = False
              IsMust = False
              DefaultKind = dkString
              KeyToRunSearch = ksF2
              LinkLabel = Label3
              LabelColorSelect = clActiveCaption
              LabelTextColorSelect = clCaptionText
              FixedColor = clBtnFace
              NormalColor = clWindow
              SelectNextAfterSearch = True
              StatusBarPanelNum = -1
              Hebrew = False
              LocateRecordOnChange = False
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 174
              Top = 74
              Width = 167
              Height = 45
              Caption = '��� ����'
              Columns = 2
              DataField = 'PriceKind'
              DataSource = DM_WinTuda.DS_Shib
              Items.Strings = (
                '����'
                '��� ��"�')
              TabOrder = 5
              Values.Strings = (
                '0'
                '1')
            end
          end
        end
      end
      object Pnl_Mid: TPanel
        Left = 0
        Top = 188
        Width = 792
        Height = 194
        Align = alTop
        ParentColor = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        object Splitter2: TSplitter
          Left = 1
          Top = 117
          Width = 790
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          Beveled = True
          ResizeStyle = rsLine
        end
        object Label4: TLabel
          Left = 179
          Top = 161
          Width = 52
          Height = 13
          Caption = '��������'
          Visible = False
        end
        object Label7: TLabel
          Left = 372
          Top = 149
          Width = 73
          Height = 13
          Hint = '��� ��� �����'
          Caption = '��� ��������'
          Visible = False
        end
        object Label24: TLabel
          Left = 183
          Top = 104
          Width = 47
          Height = 13
          Hint = '��� ��� �����'
          Caption = '��� ����'
          Visible = False
        end
        object Label_Tz: TLabel
          Left = 143
          Top = 161
          Width = 3
          Height = 13
        end
        object Lbl_BeforeSupply: TLabel
          Left = 434
          Top = 114
          Width = 128
          Height = 13
          Caption = '����� ����� ���� �����'
          Visible = False
        end
        object Lbl_AfterSupply: TLabel
          Left = 248
          Top = 114
          Width = 130
          Height = 13
          Caption = '����� ����� ���� �����'
          Visible = False
        end
        object Lbl_DeliveryHour: TLabel
          Left = 92
          Top = 133
          Width = 62
          Height = 13
          Caption = '��� �����'
          Visible = False
        end
        object Panel_Extra: TPanel
          Left = 1
          Top = 120
          Width = 790
          Height = 73
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 3
        end
        object AtmDbHEdit1: TAtmDbHEdit
          Left = 103
          Top = 157
          Width = 72
          Height = 21
          BiDiMode = bdLeftToRight
          DataField = 'QuntMusa'
          DataSource = DM_WinTuda.DS_Shib
          ParentBiDiMode = False
          TabOrder = 0
          Visible = False
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Label4
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object DBRadioGroup_SugTemperature: TDBRadioGroup
          Left = 277
          Top = 140
          Width = 94
          Height = 29
          Columns = 2
          DataField = 'YEnd'
          DataSource = DM_WinTuda.DS_Shib
          Items.Strings = (
            'C'
            'F')
          TabOrder = 1
          Values.Strings = (
            '0'
            '1')
          Visible = False
        end
        object DBRadioGroup_SugKamut: TDBRadioGroup
          Left = 69
          Top = 95
          Width = 109
          Height = 29
          Columns = 2
          DataField = 'XEnd'
          DataSource = DM_WinTuda.DS_Shib
          Items.Strings = (
            '����'
            '����')
          TabOrder = 2
          Values.Strings = (
            '0'
            '1')
          Visible = False
        end
        object AtmDbHEditBeforeSupply: TAtmDbHEdit
          Left = 385
          Top = 112
          Width = 73
          Height = 21
          BiDiMode = bdLeftToRight
          DataField = 'Quntity3'
          DataSource = DM_WinTuda.DS_Shib
          ParentBiDiMode = False
          TabOrder = 4
          Visible = False
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_BeforeSupply
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_Rehev
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object AtmDbHEditAfterSupply: TAtmDbHEdit
          Left = 157
          Top = 131
          Width = 73
          Height = 21
          BiDiMode = bdLeftToRight
          DataField = 'Quntity4'
          DataSource = DM_WinTuda.DS_Shib
          ParentBiDiMode = False
          TabOrder = 5
          Visible = False
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_AfterSupply
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_Rehev
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object AtmDbHEditDeliveryHour: TAtmDbHEdit
          Left = 83
          Top = 131
          Width = 72
          Height = 21
          BiDiMode = bdLeftToRight
          DataField = 'UnLoadTime'
          DataSource = DM_WinTuda.DS_Shib
          ParentBiDiMode = False
          TabOrder = 6
          Visible = False
          UseF2ToRunSearch = True
          IsFixed = False
          IsMust = False
          DefaultKind = dkString
          KeyToRunSearch = ksF2
          LinkLabel = Lbl_DeliveryHour
          LabelColorSelect = clActiveCaption
          LabelTextColorSelect = clCaptionText
          FixedColor = clBtnFace
          NormalColor = clWindow
          SelectNextAfterSearch = True
          StatusBarPanelNum = -1
          Hebrew = False
          LocateRecordOnChange = False
        end
        object Panel3: TPanel
          Left = 224
          Top = 1
          Width = 567
          Height = 116
          Align = alRight
          Caption = 'Panel3'
          TabOrder = 7
          object Lbl_TnuaDate: TLabel
            Left = 529
            Top = 36
            Width = 33
            Height = 13
            Caption = '�����'
          end
          object Lbl_Rehev: TLabel
            Left = 530
            Top = 61
            Width = 32
            Height = 13
            Caption = '<���>'
          end
          object Label9: TLabel
            Left = 301
            Top = 34
            Width = 16
            Height = 13
            Hint = '�� ���'
            Caption = '��'
            Visible = False
          end
          object Lbl_SugHovala: TLabel
            Left = 354
            Top = 58
            Width = 43
            Height = 13
            Hint = '��� ��� �����'
            Caption = '�.�����'
          end
          object Label2: TLabel
            Left = 531
            Top = 10
            Width = 31
            Height = 13
            Caption = '�����'
          end
          object Label6: TLabel
            Left = 361
            Top = 5
            Width = 36
            Height = 26
            Hint = '���� ����'
            Caption = '<����'#13#10'����>'
          end
          object DBText6: TDBText
            Left = 202
            Top = 8
            Width = 98
            Height = 17
            Color = clInfoBk
            DataField = 'ShemLakoach1Look'
            DataSource = DM_WinTuda.DS_Shib
            ParentColor = False
          end
          object Lbl_Teuda: TLabel
            Left = 364
            Top = 83
            Width = 33
            Height = 13
            Caption = '�����'
          end
          object Label5: TLabel
            Left = 532
            Top = 88
            Width = 30
            Height = 13
            Caption = '����'
            Visible = False
          end
          object Lbl_Nehag1: TStaticText
            Left = 363
            Top = 36
            Width = 34
            Height = 17
            Caption = '<���>'
            TabOrder = 0
          end
          object AtmDBDateEdit_TnuaDate: TAtmDBDateEdit
            Left = 417
            Top = 32
            Width = 108
            Height = 21
            DataField = 'ShibAzmnDate'
            DataSource = DM_WinTuda.DS_Shib
            CheckOnExit = True
            DialogTitle = '��� �����'
            ButtonWidth = 20
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            NumGlyphs = 2
            TabOrder = 1
            StartOfWeek = Sun
            Weekends = [Sat]
            YearDigits = dyFour
            LinkLabel = Lbl_TnuaDate
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            IsFixed = False
            FixedColor = clBtnFace
            StatusBarPanelNum = -1
          end
          object AtmDbHEdit_Rehev: TAtmDbHEdit
            Left = 416
            Top = 57
            Width = 108
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'CarNum'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 2
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_Rehev
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_Rehev
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Nehag1: TAtmDbHEdit
            Left = 316
            Top = 32
            Width = 43
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'DriverNo1'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 3
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_Nehag1
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_Nehag
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            F9String = '120'
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_ShemNehag: TAtmDbHEdit
            Left = 202
            Top = 32
            Width = 98
            Height = 21
            BiDiMode = bdRightToLeft
            DataField = 'DriverName'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 4
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label9
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = True
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_HovalaKind: TAtmDbHEdit
            Left = 316
            Top = 56
            Width = 35
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'HovalaKind'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 5
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_SugHovala
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_KodTavla
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_AutoInc: TAtmDbHEdit
            Left = 417
            Top = 6
            Width = 108
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'ShibNo'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 6
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label2
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            F9String = '4353'
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Lakoach: TAtmDbHEdit
            Left = 316
            Top = 6
            Width = 45
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'LakNo1'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 7
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label6
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_Lako
            SelectNextAfterSearch = True
            StatusBarLink = StatusBar1
            StatusBarString = '���� ����'
            StatusBarPanelNum = 4
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbComboBox_SugHovala: TAtmDbComboBox
            Left = 202
            Top = 56
            Width = 98
            Height = 21
            TabStop = False
            Color = clInfoBk
            ItemHeight = 13
            Sorted = True
            TabOrder = 8
            DataSource = DM_WinTuda.DS_Shib
            DataField = 'HovalaKind'
            SugTavlaIndex = 0
          end
          object AtmDbHEdit_Teuda: TAtmDbHEdit
            Left = 262
            Top = 79
            Width = 89
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'ShibTuda'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 9
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Lbl_Teuda
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit4: TAtmDbHEdit
            Left = 418
            Top = 84
            Width = 106
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Mishkal'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 10
            Visible = False
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label5
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
        end
        object ScrollBox1: TScrollBox
          Left = 1
          Top = 1
          Width = 289
          Height = 116
          HorzScrollBar.Style = ssHotTrack
          HorzScrollBar.Tracking = True
          VertScrollBar.Position = 9
          VertScrollBar.Tracking = True
          Align = alLeft
          TabOrder = 8
          object Label20: TLabel
            Left = 241
            Top = 4
            Width = 26
            Height = 26
            Caption = '��'#39#13#10'����'
          end
          object Label21: TLabel
            Left = 241
            Top = 52
            Width = 26
            Height = 13
            Caption = '����'
          end
          object Label22: TLabel
            Left = 242
            Top = 74
            Width = 25
            Height = 13
            Caption = '����'
          end
          object Label23: TLabel
            Left = 243
            Top = 97
            Width = 26
            Height = 13
            Caption = '��"�'
          end
          object Lbl_PriceA: TLabel
            Left = 212
            Top = -8
            Width = 8
            Height = 13
            Caption = '�'
          end
          object Lbl_PriceB: TLabel
            Left = 161
            Top = -8
            Width = 7
            Height = 13
            Caption = '�'
          end
          object Lbl_PriceC: TLabel
            Left = 106
            Top = -8
            Width = 6
            Height = 13
            Caption = '�'
          end
          object Lbl_PriceD: TLabel
            Left = 51
            Top = -8
            Width = 7
            Height = 13
            Caption = '�'
          end
          object DBText1: TDBText
            Left = 187
            Top = 29
            Width = 52
            Height = 17
            Color = clInfoBk
            DataField = 'YeMida1Look'
            DataSource = DM_WinTuda.DS_Shib
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object DBText2: TDBText
            Left = 133
            Top = 29
            Width = 52
            Height = 17
            Color = clInfoBk
            DataField = 'YeMida2Look'
            DataSource = DM_WinTuda.DS_Shib
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object DBText3: TDBText
            Left = 79
            Top = 29
            Width = 52
            Height = 17
            Color = clInfoBk
            DataField = 'YeMida3Look'
            DataSource = DM_WinTuda.DS_Shib
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object DBText4: TDBText
            Left = 24
            Top = 29
            Width = 53
            Height = 17
            Color = clInfoBk
            DataField = 'YeMida4Look'
            DataSource = DM_WinTuda.DS_Shib
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object AtmDbHEdit_YeMida1: TAtmDbHEdit
            Left = 186
            Top = 6
            Width = 53
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'YeMida1'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 0
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label20
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_KodTavla
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Kamut1: TAtmDbHEdit
            Left = 186
            Top = 48
            Width = 53
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Quntity1'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 1
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultString = '1'
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label21
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            F9String = '1'
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Price1: TAtmDbHEdit
            Left = 186
            Top = 70
            Width = 53
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Price1'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 2
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label22
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Total1: TAtmDbHEdit
            Left = 186
            Top = 93
            Width = 53
            Height = 19
            TabStop = False
            BiDiMode = bdRightToLeft
            Color = cl3DLight
            Ctl3D = False
            DataField = 'PriceQuntity1'
            DataSource = DM_WinTuda.DS_Shib
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentBiDiMode = False
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 12
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label23
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = True
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_YeMida2: TAtmDbHEdit
            Left = 132
            Top = 6
            Width = 53
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'YeMida2'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 6
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label20
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_KodTavla
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Kamut2: TAtmDbHEdit
            Left = 132
            Top = 48
            Width = 53
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'Quntity2'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 7
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label21
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Price2: TAtmDbHEdit
            Left = 132
            Top = 70
            Width = 53
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Price2'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 3
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label22
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Total2: TAtmDbHEdit
            Left = 132
            Top = 93
            Width = 53
            Height = 19
            TabStop = False
            BiDiMode = bdRightToLeft
            Color = cl3DLight
            Ctl3D = False
            DataField = 'PriceQuntity2'
            DataSource = DM_WinTuda.DS_Shib
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentBiDiMode = False
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 13
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label23
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = True
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_YeMida3: TAtmDbHEdit
            Left = 78
            Top = 6
            Width = 53
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'YeMida3'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 8
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label20
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_KodTavla
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Kamut3: TAtmDbHEdit
            Left = 78
            Top = 48
            Width = 53
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'Quntity3'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 9
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label21
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Price3: TAtmDbHEdit
            Left = 78
            Top = 70
            Width = 53
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Price3'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 4
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label22
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Total3: TAtmDbHEdit
            Left = 78
            Top = 93
            Width = 53
            Height = 19
            TabStop = False
            BiDiMode = bdRightToLeft
            Color = cl3DLight
            Ctl3D = False
            DataField = 'PriceQuntity3'
            DataSource = DM_WinTuda.DS_Shib
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentBiDiMode = False
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 14
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label23
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = True
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_YeMida4: TAtmDbHEdit
            Left = 24
            Top = 6
            Width = 53
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'YeMida4'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 10
            UseF2ToRunSearch = True
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label20
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SearchComponent = Frm_GlblWinTuda.AtmAdvSearch_KodTavla
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Kamut4: TAtmDbHEdit
            Left = 24
            Top = 48
            Width = 53
            Height = 21
            TabStop = False
            BiDiMode = bdLeftToRight
            DataField = 'Quntity4'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 11
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label21
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Price4: TAtmDbHEdit
            Left = 24
            Top = 70
            Width = 53
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Price4'
            DataSource = DM_WinTuda.DS_Shib
            ParentBiDiMode = False
            TabOrder = 5
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label22
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = False
            LocateRecordOnChange = False
          end
          object AtmDbHEdit_Total4: TAtmDbHEdit
            Left = 24
            Top = 93
            Width = 53
            Height = 19
            TabStop = False
            BiDiMode = bdRightToLeft
            Color = cl3DLight
            Ctl3D = False
            DataField = 'PriceQuntity4'
            DataSource = DM_WinTuda.DS_Shib
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentBiDiMode = False
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 15
            UseF2ToRunSearch = False
            IsFixed = False
            IsMust = False
            DefaultKind = dkString
            KeyToRunSearch = ksF2
            LinkLabel = Label23
            LabelColorSelect = clActiveCaption
            LabelTextColorSelect = clCaptionText
            FixedColor = clBtnFace
            NormalColor = clWindow
            SelectNextAfterSearch = True
            StatusBarPanelNum = -1
            Hebrew = True
            LocateRecordOnChange = False
          end
        end
      end
      object AtmDbHMemo_Remark: TAtmDbHMemo
        Left = 0
        Top = 383
        Width = 792
        Height = 46
        Align = alBottom
        BiDiMode = bdRightToLeft
        DataField = 'ShibRem1'
        DataSource = DM_WinTuda.DS_Shib
        ParentBiDiMode = False
        ScrollBars = ssVertical
        TabOrder = 4
        IsFixed = False
        IsMust = False
        LinkLabel = Lbl_Remark
        LabelColorSelect = clActiveCaption
        LabelTextColorSelect = clCaptionText
        Hebrew = True
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 515
    Width = 800
    Height = 19
    BiDiMode = bdRightToLeft
    Panels = <
      item
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Text = '��"� �����'
        Width = 150
      end
      item
        Text = '��"� �����'
        Width = 150
      end
      item
        Text = '�����'
        Width = 50
      end
      item
        Text = '���� ����'
        Width = 70
      end
      item
        Text = '��� �����'
        Width = 120
      end
      item
        Text = '��� ����'
        Width = 50
      end
      item
        Width = 50
      end>
    ParentBiDiMode = False
    SimplePanel = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 800
    Height = 29
    Caption = 'ToolBar1'
    Images = ImageList_Buttons
    TabOrder = 2
    object ToolButton2: TToolButton
      Left = 0
      Top = 2
      Caption = 'ToolButton2'
      ImageIndex = 21
      OnClick = ToolButton2Click
    end
  end
  object AppEvents1: TAppEvents
    Left = 364
    Top = 65531
  end
  object Scale1: TScale
    Active = False
    X_ScreenWidth = 640
    Y_ScreenHeight = 480
    ScaleGrids = True
    Left = 392
    Top = 65531
  end
  object DynamicControls1: TDynamicControls
    IniFileName = 'FrmWinTuda'
    RegistryKey = 'Software\Atm\Atm2000\Forms\WinTuda'
    SaveIniFileNameToRegistry = True
    ExtraFieldsParent = TS_StatusFields
    ExtraFiledsFileName = 'AzmnShib.Xtr'
    Left = 420
    Top = 65531
  end
  object MainMenu1: TMainMenu
    Images = ImageList_Buttons
    Left = 456
    Top = 65532
    object NFile: TMenuItem
      Caption = '&����'
      object N24: TMenuItem
        Action = Action_ChooseYehus
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object N25: TMenuItem
        Action = Action_ShibKindNormal
        GroupIndex = 1
        RadioItem = True
      end
      object N26: TMenuItem
        Action = Action_ShibKindHiuvNehag
        GroupIndex = 1
        RadioItem = True
      end
      object N47: TMenuItem
        Action = Action_ShibKindKikuyLak
        GroupIndex = 1
      end
      object N57: TMenuItem
        Action = Action_ShibKindZikuyHiyuvLak
        Caption = '���� �����'
        GroupIndex = 1
      end
      object N52: TMenuItem
        Action = Action_PriceKindNoMam
        GroupIndex = 1
      end
      object N55: TMenuItem
        Action = Action_AtmAzmnHatz
        GroupIndex = 1
      end
      object N27: TMenuItem
        Caption = '-'
        GroupIndex = 1
      end
      object N29: TMenuItem
        Action = Action_CopyTnuot
        GroupIndex = 1
      end
      object N7: TMenuItem
        Action = Action_DupRecord
        GroupIndex = 1
      end
      object N56: TMenuItem
        Action = Action_CopyHatzaotToHazmanot
        GroupIndex = 1
      end
      object N3: TMenuItem
        Caption = '-'
        GroupIndex = 1
      end
      object NClose: TMenuItem
        Caption = '����'
        GroupIndex = 1
      end
    end
    object NEdit: TMenuItem
      Caption = '&�����'
      object NOpenSearchWindow: TMenuItem
        Action = Action_OpenSearch
      end
      object NNewHazmana: TMenuItem
        Action = Action_NewHazmana
      end
      object N2: TMenuItem
        Action = Action_CalcTotalTeudaAzmn
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object MenuItemCancelMovement: TMenuItem
        Action = ActionCancelMovement
      end
      object N58: TMenuItem
        Caption = '-'
      end
      object NStartKeyboard: TMenuItem
        Caption = '�����'
        ImageIndex = 5
      end
      object N20: TMenuItem
        Caption = '-'
      end
      object NEditRxSpeedBar: TMenuItem
        Caption = '���� ���� ����'
      end
      object N19: TMenuItem
        Action = Action_ChangeParams
      end
      object N43: TMenuItem
        Caption = '-'
      end
      object N13: TMenuItem
        Action = Action_SelectExtraFile
      end
      object N40: TMenuItem
        Action = Act_EditExtraFields
      end
      object N39: TMenuItem
        Action = Act_EditScreenControls
      end
      object N59: TMenuItem
        Caption = '-'
      end
      object N60: TMenuItem
        Action = NextAzmn
      end
      object N61: TMenuItem
        Action = DataSetPrior1
      end
    end
    object NCrt: TMenuItem
      Caption = '&������'
      object N4: TMenuItem
        Action = Action_CrtOpenLak
      end
      object ActionCrtOpenNehag1: TMenuItem
        Action = Action_CrtOpenNehag
      end
      object N5: TMenuItem
        Action = Action_CrtOpenRehev
      end
      object ActionCrtOpenMehiron1: TMenuItem
        Action = Action_CrtOpenMehiron
      end
      object N12: TMenuItem
        Action = Action_CrtOpenMaslul
      end
      object N28: TMenuItem
        Action = Action_CrtOpenMahzeva
      end
      object N11: TMenuItem
        Action = Action_CrtOpenKodTavla
      end
      object N66: TMenuItem
        Caption = '����� �������'
      end
      object N63: TMenuItem
        Caption = '-'
      end
      object N64: TMenuItem
        Action = Action_Tzfifut
      end
      object N65: TMenuItem
        Action = Action_OpenTzfifutHodshit
      end
    end
    object N1: TMenuItem
      Caption = '&������'
      object N14: TMenuItem
        Action = Action_AllPrices
      end
      object N15: TMenuItem
        Action = Action_Price1
      end
      object N21: TMenuItem
        Action = Action_Price2
      end
      object N32: TMenuItem
        Action = Action_Price3
      end
      object N42: TMenuItem
        Action = Action_Price4
      end
      object N122: TMenuItem
        Action = Action_Price1PlusPrice2
      end
      object N46: TMenuItem
        Action = Action_Price2SubHanForLakFromPrice1
      end
      object N16: TMenuItem
        Action = Action_SubtractMam
      end
      object N62: TMenuItem
        Action = Action_ChangePricePercent
      end
      object N23: TMenuItem
        Action = Action_ChooseMehiron
      end
    end
    object NReports: TMenuItem
      Caption = '�����'
      object N22: TMenuItem
        Action = Action_MeholelReport
      end
    end
    object N30: TMenuItem
      Caption = '������'
      object N33: TMenuItem
        Action = Act_Refresh_Lako
      end
      object N34: TMenuItem
        Action = Act_Refresh_Rehev
      end
      object N35: TMenuItem
        Action = Act_Refresh_Nehag
      end
      object N36: TMenuItem
        Action = Act_Refresh_Maslul
      end
      object N37: TMenuItem
        Action = Act_Refresh_KodTavla
      end
      object NRefreshAllSearchWindows: TMenuItem
        Caption = '���� ������ �����'
        ImageIndex = 3
      end
      object N38: TMenuItem
        Action = Act_Maintaince_CloseMeholel
      end
      object N44: TMenuItem
        Action = Act_Maintaince_UPdatePricesInTnuaFromMehiron
      end
    end
    object N8: TMenuItem
      Caption = '��������'
      object N48: TMenuItem
        Action = Act_HsbZikuyLakPerut
      end
      object N49: TMenuItem
        Action = Act_HsbZikuyLakMake
      end
      object N50: TMenuItem
        Action = Act_HsbZikuyLakCopy
      end
      object N51: TMenuItem
        Action = Act_HsbZikuyLakCancel
      end
      object N67: TMenuItem
        Action = Act_Hesb_Mas_Kabla
      end
      object N53: TMenuItem
        Action = Act_HsbPriceProposal
      end
      object N54: TMenuItem
        Action = Act_HsbPrintHazmana
      end
    end
  end
  object AtmTabSheetBuild1: TAtmTabSheetBuild
    WebMode = False
    ScrFileName = 'TSBWinTuda1.Scr'
    SqlFileName = 'TSBWintuda1.Sql'
    PageControl = PageControl_Main
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = DM_WinTuda.DS_TabSheetBuild
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = DM_WinTuda.RxQuery_TabSheetBuild
    RxDBFilter = DM_WinTuda.RxDBFilter_TabSheetBuild
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    EditBiDiMode = bdLeftToRight
    Left = 484
    Top = 65532
  end
  object TSB2: TAtmTabSheetBuild
    WebMode = False
    ScrFileName = 'TSBWintuda2.Scr'
    SqlFileName = 'TSBWintuda2.Sql'
    PageControl = PageControl_Main
    SectionForFields = 'Fields'
    SectionForWidth = 'Width'
    SectionForFieldMask = 'FieldsMask'
    SectionForEditMask = 'EditMask'
    SectionForPickList = 'PickList'
    SectionForBooleanFields = 'BooleanFields'
    SectionForAlignmentFields = 'AlignmentFields'
    DataSource = DM_WinTuda.DS_TSB2
    SectionForParams = 'Params'
    ParamsKeyForTabSheetCaption = 'TabSheetCaption'
    ParamsKeyForID = 'ID'
    ParamsKeyForPageIndex = 'PageIndex'
    ParamsKeyForSortColor = 'SortColor'
    ParamsKeyForTitleButtons = 'TitleButtons'
    Query = DM_WinTuda.RxQuery_TSB2
    RxDBFilter = DM_WinTuda.RxDBFilter_TSB2
    ParamsKeyForAutoSortMacroName = 'AutoSortMacroName'
    ParamsKeyForDefaultWhereMacro = 'DefaultWhereMacro'
    ParamsKeyForSortAscending = 'SortAsc'
    ParamsKeyForDefaultSort = 'DefaultSort'
    SortAscending = True
    SearchMoreFiles = True
    ParamsKeyForKeepGrid = 'KeepGrid'
    ParamsKeyForNoEditOnGrid = 'NoEditOnGrid'
    EditBiDiMode = bdLeftToRight
    Left = 528
    Top = 65532
  end
  object AtmKeyBoard1: TAtmKeyBoard
    Left = 80
    Top = 40
  end
  object ImageList_Buttons: TImageList
    Left = 216
    Top = 152
    Bitmap = {
      494C010116001800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000006000000001001000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      007C0000000000000000000000000000000000001F0000000000000000000000
      0000007C00000000000000000000EF3D0F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      007CEF3D000000000000000000000000000000001F001F001F00000000000000
      0000007C00000000000000000F000F000F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001F00
      007C0F000000000000000000000000000000000000001F001F001F0000000000
      0000007C00000000EF3D0F000F000F00EF3D0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001F00
      007C0F00EF3D000000000000000000000000000000001F001F001F001F001F00
      0000007C00000F000F000F000F000F0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F001F00
      007C0F000F000000000000000000000000000000000000001F001F001F001F00
      1F00007C0F000F000F000F000F00EF3D00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F001F00
      007C0F000F00EF3D000000000000000000000000000000001F001F001F001F00
      1F00007C0F000F000F000F000F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001F001F001F00
      007C0F000F000F000000000000000000000000000000000000001F001F001F00
      1F00007C0F000F000F000F00EF3D000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001F001F001F00
      007C0F000F000F00EF3D000000000000000000000000000000001F001F001F00
      1F00007C0F000F000F000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F001F001F001F00
      007C0F000F000F000F000000000000000000000000000000000000001F001F00
      1F00007C0F000F000F00EF3D0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F001F001F001F00
      007C0F000F000F000F00EF3D000000000000000000000000000000001F001F00
      1F00007C0F000F000F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F001F001F001F00
      007C0F000F000F000F000F000000000000000000000000000000000000001F00
      1F00007C0F000F00EF3D00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F001F001F001F00
      007C0F000F000F000F000F00EF3D000000000000000000000000000000001F00
      1F00007C0F000F00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000001F001F001F001F001F000000
      007C00000F000F000F000F000F00000000000000000000000000000000000000
      1F00007C0F00EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000001F001F001F00000000000000
      007C00000000EF3D0F000F000F00EF3D00000000000000000000000000000000
      1F00007C0F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001F001F001F000000000000000000
      007C00000000000000000F000F000F0000000000000000000000000000000000
      0000007CEF3D0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001F00000000000000000000000000
      007C00000000000000000000EF3D0F0000000000000000000000000000000000
      0000007C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000007A10F5207A1000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      7A10D02424282A39F52000000000000000000000000000000000000000000000
      0000000000000000000000000F3C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000B918D024
      2428212C2A395352D024D024D024D024B9180000000000000000000000000000
      000000000000000000000F3C0F3C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000B918D0242428212C
      637063702A3953522A39212C2A39212C53520000000000000000000000000000
      00000000000000000F3C0F3C0F3C0F3C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000007A10D02424282A392A396370
      63706370637053522A392A3939672A3939670000000000000000000000000000
      0000000000000F3C0F3C0F3C0F3C0F3C0F3C0000000000000000000000001042
      0000000000000000000000000000000000000000000000000000000000000000
      0000000010420000000000000000000000007A102428212C2A3953522A396370
      63706370212C212C535253522A39F5207A100000000000000000000000000000
      00000000000000000F3C0F3C0F3C0F3C0F3C0000000000000000000000000000
      0000104200000000000000000000000000000000000000000000000000000000
      1042000000000000000000000000000000002A39637063705352535253526370
      212C212C212C2A3953522A39EE28B91800000000000000000000000000000000
      000000000000000000000F3C0F3C00000F3C0000000000000000000000000000
      0000000000001042000000000000000000000000000000000000000010420000
      000000000000000000000000000000000000212C63706370535239673967212C
      212C212C6370637053522A39F520000000000000000000000000000000000000
      0000000000000000000000000F3C00000F3C0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005352637063702A39396739672A39
      63706370637063702A39EE283A08000000000000000000000000000000000000
      000000000000000000000000000000000F3C0000000000000000000000000000
      0000000000001042000000000000000000000000000000000000000010420000
      0000000000000000000000000000000000003967212C212C212C535239675352
      637063706370212CEE283A080000000000000000000000000000000000000000
      000000000000000000000000000000000F3C0000000000000000000000000000
      0000104200000000000000000000000000000000000000000000000000000000
      1042000000000000000000000000000000005352212C212C6370637039675352
      2A39EE28EE28B918000000000000000000000000000000000000000000000000
      00000000000000000000000000000F3C00000000000000000000000000001042
      0000000000000000000000000000000000000000000000000000000000000000
      000000001042000000000000000000000000B918436C637063706370212CEE28
      B9187A1000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EE28436C6370212CF5201A04
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B918F5201A0400000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF7FFF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000F000F000F000F000000FF7FFF7F00000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001042000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0F00000000000000000000000000000000000000FF7FFF7FEF3DFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F0000000000000000FF7F10421000FF7F
      1042100010001000FF7F00001042000000000000000000000000000000000000
      0000FF7FEF3DEF3DEF3DEF3DEF3DFF7F00000000000000000000000000000F00
      0000000000000000000000000000000000000000FF7FFF7FEF3DFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001042000000000000000000000000000000000000
      0000FF7FEF3DFF7FFF7FFF7FEF3DFF7F00000000000000000000000000000F00
      0000000000000000000000000000000000000000EF3DEF3DEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D0000000000000000FF7F1000FF7F1000
      1000100010001000FF7F00001042000000000000000000000000000000000000
      0000FF7FEF3DEF3DEF3DEF3DEF3DFF7F00000000000000000000000000000F00
      0000000000000000000000000000000000000000FF7FFF7FEF3DFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001042000000000F000F000F000F000F000F000F00
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0F00000000000000000000000000000000000000FF7FFF7FEF3DFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F0000000000000000FF7F1000FF7F1000
      1000100010001000FF7F00001042000000000F00EF3DEF3DEF3DEF3DEF3DEF3D
      0000FF7FEF3DEF3D000000000000FF7F00000000000000000000000000000000
      00000F0000000000000000000000000000000000EF3DEF3DEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001042000000000F00000000000000000000000000
      0000FF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000
      000000000F000000000000000000000000000000FF7FFF7FEF3DFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F0000000000000000FF7F1000FF7F1000
      1000100010001000FF7F00001042000000000F00000000000000000000000000
      0000FF7FEF3DEF3D000000000000000000000000000000000000000000000000
      000000000F000000000000000000000000000000FF7FFF7FEF3DFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FEF3DFF7FFF7FFF7F0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001042000000000F00000000000000000000000000
      0000FF7FFF7FFF7FFF7F00000000000000000000000000000000000000000000
      000000000F000000000000000000000000000000EF3DEF3DEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D0000000000000000FF7F10421042FF7F
      1000100010001000FF7F00001042000000000F00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000F00000000000000000000000000000000000F000F000F000F000F000F00
      0F000F000F000F000F000F000F000F000000000000000000FF7F10001000FF7F
      FF7FFF7F00000000000000000000000000000F00000000000000000000000000
      00000000000000000F0000000000000000000000FF7FFF7F00000F000F000F00
      0F000000000000000000000000000000000000000F000F000F000F000F000F00
      0F000F000F000F000F000F000F000F000000000000000000FF7F10421042FF7F
      1000100000001863000000000000000000000F000F000F000F000F000F000F00
      0F000F000F000F000F0000000000000000000000FF7FFF7F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000000000000000000F00FF7F0F000F00FF7F0F000F00
      FF7F0F000F00FF7F0F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000F000F000F000F000F000F000F00
      0F000F000F000F000F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000003C003C003C003C003C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007C007C007C003CEF3D007C0000000000000000000000000F0000000F00
      00000F0000000F0000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EF01EF010000
      0000EF0100000000000000000000000000000000000000000000000000000000
      0000003C003CF75EF75EEF3D00000000000000000000000000000F0000000000
      0000000000000000000000000000000000000000EF3DEF3DEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DEF3D0000000000000000000000000000FF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      000000000000F75EF75E00000000000000000000000000000F000F000F000000
      000000000000000000000F000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      000000000F3C00000F3C0000000000000000000000000000E03DEF3DE03D0000
      0000000000000000F75EF75EF75E0000000000000000000000000F0000000000
      000000000000000000000000000000000000000000000000003C003C003C003C
      003C003C003C003C003C003C000000000000000000000000000000000000FF7F
      FF7FFF7F0000F75E00000000000000000000000000000000F75EFF7FFF7FEF3D
      0000000000000000F75EF75EF75E000000000000000000000000000000000000
      000000000000000000000F0000000000000000000000007C007C007CEF3D007C
      EF3DEF3D007CEF3D007C007C003C00000000000000000000F75EF75EFF7FFF7F
      FF7FFF7F0000F75E0000000000000000000000000000E03DEF3DFF7FFF7FFF7F
      000000000000F75EF75EF75EF75EEF3D00000000000000000000000000000000
      00000000000000000000000000000000000000000000007CFF03FF03007C007C
      007C007C007C007CFF03FF03003C00000000000000000000F75EF75E0000FF7F
      FF7FFF7FFF7F0000F75E000000000000000000000000F75EFF7FFF7FF75EFF7F
      00000000000000000000F75E000000000000000000000000FF7FE07FFF7FE07F
      0000000000000000FF7FE07FFF7FE07F000000000000007C007C007C007C007C
      007C007C007C007C007C007C007C00000000000000000000F75EF75E0000FF7F
      FF7F00000000F75EF75E00000000000000000000E03DEF3DFF7FFF7FF75EFF7F
      000000000000000000000000F75EEF3D00000000FF7F00000000000000000000
      00000000FF7F0000000000000000000000000000003C007C007CFF03FF7FFF03
      FF7FFF03FF7FFF03FF7F007C007C003C0000000000000000F75EF75EF75E0000
      FF7FFF7F0000F75EF75EF75E0000000000000000F75EFF7FFF7FF75EFF7FFF7F
      FF7F000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000007C0000007CFF7FFF03FF7F
      FF03FF7FFF03FF7FFF03007C0000007C00000000000000000000F75EF75EF75E
      F75EF75E0000F75E00000000000000000000E03DEF3DFF7FFF7FF75EFF7FFF7F
      EF3DFF7F00000000000000000000000000000000FF7FEF3DEF3DEF3DFF7F0000
      00000000FF7FEF3DEF3DEF3DFF7F000000000000000000000000007CFF7FFF03
      FF7FFF03FF7FFF03007C00000000000000000000000000000000000000000000
      00000000EF3DF75EF75E00000000000000000000FF7FFF7FF75EFF7FFF7FFF7F
      F75EFF7FFF7FFF7FF75EFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000007C007C
      007C007C007C007C000000000000000000000000000000000000000000000000
      EF3DEF3DEF3DEF3DEF3D0000000000000000000000000000FF7FFF7FFF7FEF3D
      FF7FF75EFF7FF75EF75EFF7F0000000000000000FF7FEF3DEF3DEF3DFF7F0000
      00000000FF7FEF3DEF3DEF3DFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7FF75EFF7FFF7F0000000000000000FF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7FF75EF75EFF7F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF7FFF7F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E0030000E0030000E0030000
      E0030000E0030000E0030000E0030000E0030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      000000000000000000000000000000000000E0030000E0030000E0030000E003
      0000E0030000E0030000E0030000E00300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF031F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FF75E0000
      FF7FFF7F0000000000000000000000000000E0030000F75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000000000000000000000
      000000000000FF031F001F000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FF75E0000FF7F
      EF3DFF7FFF7F00000000000000000000000000000000F75EEF3D0000F75EEF3D
      0000F75EEF3D0000F75EEF3D0000F75E00000000000000000000000000000000
      00000000FF031F001F0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FEF3D0000FF7F0000
      FF7F0000FF7FFF7F00000000000000000000E0030000F75EFF7F0000F75EFF7F
      0000F75EFF7F0000F75EFF7F0000F75E00000000000000000000000000000000
      0000FF031F001F00000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F00000000000000000000000000000000FF7FEF3DEF3DFF7F
      EF3DFF7F0000FF7FFF7F000000000000000000000000F75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000001002100210021002
      100200001F000000000000000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000000000000000000000000000FF7FEF3D0000
      FF7F0000FF7F0000FF7FFF7F000000000000E0030000F75EEF3D0000F75EEF3D
      0000F75EEF3D0000F75EEF3D0000F75E0000000010421002FF03FF7FFF03FF7F
      1002100200000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000000000000000FF7FEF3D
      EF3DFF7FEF3DFF7FEF3DFF7FFF7F0000000000000000F75EFF7F0000F75EFF7F
      0000F75EFF7F0000F75EFF7F0000F75E000000001042FF03FF7FFF03FF7FFF03
      FF7F1002000000000000000000000000000000000F000F000F000F000F000F00
      0F000F000F000F000F000F00000000000000000000000000000000000000FF7F
      EF3D0000FF7F0000FF7F0000EF3DFF7F0000E0030000F75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E000000001042FF7FFF03FF7FFF03FF7F
      FF031002000000000000000000000000000000000F00FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F000000000000000000000000000000000000000000
      FF7FEF3DEF3DFF7FFF7FEF3DFF7F0000000000000000F75E0000000000000000
      00000000000000000000F75EF75EF75E000000001042FF03FF7FFF03FF7FFF03
      FF7F1002000000000000000000000000000000000F00FF7F00000000FF7F0000
      0000FF7F00000000FF7F0F000000000000000000000000000000000000000000
      0000FF7FEF3D0000EF3DFF7F000000000000E0030000F75E0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000F75EF75EF75E000000001042FF7FFF03FF7FFF03FF7F
      FF031002000000000000000000000000000000000F00FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0F000000000000000000000000000000000000000000
      00000000FF7FEF3DFF7F000000000000000000000000F75E0000000000000000
      00000000000000000000F75EF75EF75E0000000000001042FF7FFF03FF7FFF03
      FF7F0000000000000000000000000000000000000F000F000F000F000F000F00
      0F000F000F000F000F000F000000000000000000000000000000000000000000
      000000000000FF7F00000000000000000000E0030000F75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000001042104210421042
      10420000000000000000000000000000000000000F0000000F000F0000000F00
      0F0000000F000F0000000F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E00300000000000000000000
      00000000000000000000000000000000E0030000000000000000000000000000
      00000000000000000000000000000000000000000F000F000F000F000F000F00
      0F000F000F000F000F000F000000000000000000000000000000000000000000
      000000000000000000000000000000000000E0030000E0030000E0030000E003
      0000E0030000E0030000E0030000E00300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EF3D0F000F00
      0F000F000F00EF3D000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3DEF3DEF3D000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000F000F000F000F00
      0F000F000F000F000F0000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F000000000000EF3D000000000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000F000F000F0000000000
      0000000000000F000F000F000000000000000000FF7F00000000000000000000
      FF7FFF7F0000FF7F000000000000EF3D000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EF3D0F000F00000000000000
      00000000000000000F000F00EF3D000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F000000000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F000F000000000000000000
      E001E00100000000EF3D0F000F00000000000000FF7F00000000000000000000
      0000FF7F000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F000F00000000000000E001
      E001E001E001000000000F000F00000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3DEF3DEF3D000000000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F000F00000000000000E001
      E001E001E001000000000F000F00000000000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7F000000000000EF3D000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F000F000000000000000000
      E001E00100000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F000000000000EF3D000000000000FF7FEF3DEF3DEF3DEF3D
      EF3DEF3DEF3DFF7F000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F000F00EF3D000000000000
      000000000000000000000F000F00000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EF3D0F000F00000000000F00
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7F000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000F000F000F0000000F00
      0F000000000000000F00EF3D0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F000000000000000000000000000000000000FF7FFF7F0000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000F000F000F000F00
      0F000F0000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F000000000000000000000000000000000000000000000000FF7FEF3D
      EF3DEF3DEF3DEF3DEF3DEF3DEF3DE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EF3D0F000F00
      0F000F0000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F000000000000000000000000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000F00
      0F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000F00
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000600000000100010000000000000300000000000000000000
      000000000000000000000000FFFFFF00FEFFBF7C00000000FE7F8F7800000000
      FC7FC760F75E0000FC3FC141F75EF75EF83FE001F75EF75EF81FE003F75EF75E
      F01FF00300000000F00FF00700000000E00FF80700000000E007F80F00000000
      C007FC0FFF7F0000C003FC1FFF7FE07F8283FE1FFF7FE07F8EC1FE3FFF7FFF7F
      1EF1FF3F000000007EF9FF7F00000000FFFFFFFFFFFFFFFFFF8FFDFFFFFFFFFF
      FE0F63BBFFFFFFFFF8009B73FFFFFFFFE000D0E1FFFFFFFF8000DEC0F9FFFF9F
      00009E20F87FFE1F00010F52F81FF81F00031CDAF80FF01F00030EEEF81FF81F
      0007009EF87FFE1F001F80DDF9FFFF9F007FC02FFFFFFFFF81FFF80FFFFFFFFF
      C7FFFC1FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF003FFFFFFF0FFFFC003FE00
      FF000000C003FE00FEF00000C003FE00FDFF0000C003FE00FDFF0000C003FE00
      FDFF0000C0030000FEFF0000C003001CFF7F0000C0037E00FFBF0000C0034815
      FFBF0000C0037E03FFBF0000C00348070F7F0000C0077FEF00FF0000C00F000F
      0FFF0000C01F000F0FFFFFFFC03F000FFFFFFFFFFFFFFF83FFFFFFFFFFFFFF03
      F557FFFFE03FFF07F7FF8001F07FE383E3F7C7E3F007E001F7FFC003E00FC001
      FFF7C003C00FC001C0C0C003C00780010000C003C007800000008001C0030000
      0101A005E00700010101F00FF00780030101F81FFC07C0030101FFFFFE0FF007
      0101FFFFFFFFFE070101FFFFFFFFFF8FFFFFFDFFAAAAFFFFFFFBF8FF5555FFE7
      FFFBF07F8000FFC3FFE0E03F0000FF83FFFBC01F8000FF07000B800F0000E00F
      000FC0078000C01F000FE0030000803F000FF0018000803F0007F8000000803F
      8007FC018000803F8007FE030000803F8007FF078000C07F8007FF8B0000E0FF
      A497AADB8000FFFF800755075555FFFFFFFFFFFFFFFFFFFF0000800FFFFFF01F
      0000800FFFFFE00F0014800FFFFFC7C70000800FE00F8FE30000800FF3EF9E63
      0000800FF9FF9C330000800FFCFF9C330000800FFE7F9E7F0014800FFCFF8FF3
      0000800FF9FF8DFF00008000F3EFC4E700008000E00FE07F00088000FFFFF07F
      001CF000FFFFFCFF0000F000FFFFFDFF00000000000000000000000000000000
      000000000000}
  end
  object ActionList1: TActionList
    Images = ImageList_Buttons
    Left = 160
    Top = 155
    object Action_LastPriceListAction: TAction
      Category = 'PriceList'
      Caption = '���� 1 �������'
      Hint = '���� 1 �������'
      ImageIndex = 6
    end
    object Action_OpenSearch: TAction
      Category = 'MainDataSet'
      Caption = '���� �����'
      Hint = '���� �����'
      ImageIndex = 0
      ShortCut = 113
    end
    object Action_NewHazmana: TAction
      Category = 'MainDataSet'
      Caption = '����� ����'
      Hint = '����� ����'
      ImageIndex = 1
      ShortCut = 122
    end
    object Action_CalcTotalTeudaAzmn: TAction
      Category = 'MainDataSet'
      Caption = '��� ��"� ������'
      Hint = '��� ��"� ������'
      ImageIndex = 2
    end
    object Action_Price4: TAction
      Category = 'PriceList'
      Caption = '���� 4 �������'
      Hint = '���� 4 �������'
      ImageIndex = 6
      ShortCut = 16502
    end
    object Action_Price3: TAction
      Category = 'PriceList'
      Caption = '���� 3 �������'
      Hint = '���� 3 �������'
      ImageIndex = 6
      ShortCut = 16503
    end
    object Action_Price1: TAction
      Category = 'PriceList'
      Caption = '���� 1 �������'
      Hint = '���� 1 �������'
      ImageIndex = 6
      ShortCut = 119
    end
    object Action_Price2: TAction
      Category = 'PriceList'
      Caption = '���� 2 �������'
      Hint = '���� 2 �������'
      ImageIndex = 6
      ShortCut = 118
    end
    object Action_Price1PlusPrice2: TAction
      Category = 'PriceList'
      Caption = '���� 1 + ���� 2 �������'
      Hint = '���� 1 + ���� 2 �������'
      ImageIndex = 6
      ShortCut = 117
    end
    object Action_SubtractMam: TAction
      Category = 'PriceList'
      Caption = '���� ��"�'
      Hint = '���� ��"�'
      ImageIndex = 6
      ShortCut = 16500
    end
    object Action_CrtOpenLak: TAction
      Category = '������'
      Caption = '����� ������'
      Hint = '����� ������'
      ImageIndex = 11
    end
    object Action_CrtOpenNehag: TAction
      Category = '������'
      Caption = '����� �����'
      Hint = '����� �����'
      ImageIndex = 10
    end
    object Action_CrtOpenRehev: TAction
      Category = '������'
      Caption = '����� �����'
      Hint = '����� �����'
      ImageIndex = 9
    end
    object Action_ChooseMehiron: TAction
      Caption = '��� ������'
      Hint = '��� ������'
      ImageIndex = 6
    end
    object Action_CrtOpenMehiron: TAction
      Category = '������'
      Caption = '������'
      Hint = '������'
      ImageIndex = 6
    end
    object Action_DupRecord: TAction
      Category = 'MainDataSet'
      Caption = '���� ����� ������'
      Hint = '���� ����� ������'
      ImageIndex = 8
    end
    object Action_CrtOpenKodTavla: TAction
      Category = '������'
      Caption = '������'
      Hint = '������ ������'
      ImageIndex = 13
    end
    object Action_CrtOpenMaslul: TAction
      Category = '������'
      Caption = '����� �������'
      Hint = '����� �������'
      ImageIndex = 12
    end
    object Action_SelectExtraFile: TAction
      Category = '������'
      Caption = '���� ������ ���'
      Hint = '���� ������ ���'
    end
    object Action_AllPrices: TAction
      Category = 'PriceList'
      Caption = '�� �������'
      Hint = '�� �������'
      ImageIndex = 6
      ShortCut = 116
    end
    object Action_SearchCodesFromMehiron: TAction
      Category = 'MainDataSet'
      Caption = '������� ������� ����'
      Hint = '������� ������� ����'
      ShortCut = 8305
    end
    object Action_ChangeParams: TAction
      Caption = '������ �������'
    end
    object Action_NewTnua: TAction
      Category = 'MainDataSet'
      Caption = '����� ����'
      Hint = '����� ����'
      ImageIndex = 4
      ShortCut = 123
    end
    object Action_ChoosePointsFromMsl: TAction
      Category = 'MainDataSet'
      Caption = '��� ����� ��������'
      Hint = '��� ����� ��������'
    end
    object Action_MeholelReport: TAction
      Caption = '����� �������'
      Hint = '����� �������'
      ImageIndex = 14
    end
    object Action_MeholelMain: TAction
      Caption = '����� �����'
      Hint = '����� �����'
      ImageIndex = 15
    end
    object Action_ChooseYehus: TAction
      Category = 'MainDataSet'
      Caption = '����� ���� ����'
    end
    object Action_ShibKindHiuvNehag: TAction
      Category = 'MainDataSet'
      Caption = '����� �����'
    end
    object Action_ShibKindKikuyLak: TAction
      Category = 'MainDataSet'
      Caption = '����� ������'
    end
    object Action_ShibKindNormal: TAction
      Category = 'MainDataSet'
      Caption = '���� ������'
      Checked = True
    end
    object Action_PriceKindNoMam: TAction
      Category = 'MainDataSet'
      Caption = '������ ��� ��"�'
    end
    object Action_CrtOpenMahzeva: TAction
      Category = '������'
      Caption = '������'
      Hint = '����� ������'
      ImageIndex = 16
    end
    object Action_CopyTnuot: TAction
      Category = 'MainDataSet'
      Caption = '����� ������ ������'
    end
    object Act_Refresh_Lako: TAction
      Category = 'Refresh'
      Caption = '���� ���� ������'
      Hint = '���� ���� ������'
      ImageIndex = 3
    end
    object Act_Refresh_Nehag: TAction
      Category = 'Refresh'
      Caption = '���� ���� �����'
      Hint = '���� ���� �����'
      ImageIndex = 3
    end
    object Act_Refresh_Rehev: TAction
      Category = 'Refresh'
      Caption = '���� ���� �����'
      Hint = '���� ���� �����'
      ImageIndex = 3
    end
    object Act_Refresh_Maslul: TAction
      Category = 'Refresh'
      Caption = '���� ���� �������'
      Hint = '���� ���� �������'
      ImageIndex = 3
    end
    object Act_Refresh_KodTavla: TAction
      Category = 'Refresh'
      Caption = '���� ������  �����'
      Hint = '���� ������  �����'
      ImageIndex = 3
    end
    object Act_Maintaince_CloseMeholel: TAction
      Category = '������'
      Caption = '���� ����� �����'
      Hint = '���� ����� �����'
    end
    object Act_EditScreenControls: TAction
      Category = '������'
      Caption = '���� ��� �����'
    end
    object Act_EditExtraFields: TAction
      Category = '������'
      Caption = '���� ���� ������'
      Hint = '���� ���� ������'
    end
    object Action_ChangeTab: TAction
      Caption = 'Action_ChangeTab'
      ShortCut = 16504
    end
    object Act_Maintaince_UPdatePricesInTnuaFromMehiron: TAction
      Category = '������'
      Caption = '����� ������ ������� ��� ������'
    end
    object Action_Price2SubHanForLakFromPrice1: TAction
      Category = 'PriceList'
      Caption = '���� � = ���� � - ���� ����'
      Hint = '���� � = ���� � - ���� ����'
      ImageIndex = 6
    end
    object Act_HsbZikuyLakPerut: TAction
      Category = '��������'
      Caption = '���� ������� �����'
      Hint = '���� ������� �����'
      ImageIndex = 17
    end
    object Act_HsbZikuyLakMake: TAction
      Category = '��������'
      Caption = '����� �-� ����� �����'
      Hint = '����� �-� ����� �����'
      ImageIndex = 17
    end
    object Act_HsbZikuyLakCopy: TAction
      Category = '��������'
      Caption = '���� �-� ����� �����'
      Hint = '���� �-� ����� �����'
      ImageIndex = 17
    end
    object Act_HsbZikuyLakCancel: TAction
      Category = '��������'
      Caption = '����� �-� ����� ����'
      Hint = '����� �-� ����� ����'
    end
    object Act_HsbPriceProposal: TAction
      Category = '��������'
      Caption = '���� ����'
      Hint = '���� ����'
    end
    object Act_HsbPrintHazmana: TAction
      Category = '��������'
      Caption = '�����'
      Hint = '�����'
    end
    object Action_AtmAzmnHatz: TAction
      Category = 'Maz'
      Caption = '����� ����'
      Hint = '����� ����'
      Visible = False
    end
    object Action_CopyHatzaotToHazmanot: TAction
      Category = 'Maz'
      Caption = '����� ���� ���� ������'
      Hint = '����� ���� ���� ������'
      Visible = False
    end
    object Action_ShibKindZikuyHiyuvLak: TAction
      Category = 'MainDataSet'
      Caption = '����� ���� ������'
    end
    object ActionCancelMovement: TAction
      Category = '�����'
      Caption = '��� ����� ������'
    end
    object ActionBringMovementBack: TAction
      Category = '�����'
      Caption = 'ActionBringMovementBack'
    end
    object NextAzmn: TDataSetNext
      Category = 'Dataset'
      Caption = '����� ����'
      Hint = '����� ����'
      ImageIndex = 18
      ShortCut = 16462
      DataSource = DM_WinTuda.DS_Azmn
    end
    object DataSetPrior1: TDataSetPrior
      Category = 'Dataset'
      Caption = '����� �����'
      Hint = '����� �����'
      ImageIndex = 19
      ShortCut = 16464
      DataSource = DM_WinTuda.DS_Azmn
    end
    object Action_ChangePricePercent: TAction
      Category = 'PriceList'
      Caption = '��� ���� �����'
      Hint = '��� ���� �����'
      ImageIndex = 6
      ShortCut = 16501
    end
    object Action_OpenTzfifutHodshit: TAction
      Tag = 2
      Category = '������'
      Caption = '������ ������'
    end
    object Action_Tzfifut: TAction
      Tag = 1
      Category = '������'
      Caption = '����� ������'
    end
    object Action_SearchCodesFromMehironConect: TAction
      Category = 'MainDataSet'
      Caption = ' ������� ������ �������� ������� ����'
      ShortCut = 16497
    end
    object Act_Hesb_Mas_Kabla: TAction
      Category = '��������'
      Caption = '���� ������� �� ����'
    end
  end
  object FormStorage_WinTeuda: TFormStorage
    IniFileName = 'Software\Atm\Atm2000\Forms'
    IniSection = 'WinTuda'
    UseRegistry = True
    StoredProps.Strings = (
      'PageControl_Tnua.ActivePage'
      'DBGrid_Tnua.Columns'
      'DBGrid_Preut2.Columns')
    StoredValues = <>
    Left = 60
    Top = 144
  end
  object PopupMenu_Mehiron: TPopupMenu
    Left = 184
    Top = 14
    object N17: TMenuItem
      Action = Action_AllPrices
    end
    object ActionPrice11: TMenuItem
      Action = Action_Price1
    end
    object ActionPrice21: TMenuItem
      Action = Action_Price2
    end
    object N31: TMenuItem
      Action = Action_Price3
    end
    object N41: TMenuItem
      Action = Action_Price4
    end
    object N121: TMenuItem
      Action = Action_Price1PlusPrice2
    end
    object N45: TMenuItem
      Action = Action_Price2SubHanForLakFromPrice1
    end
    object N18: TMenuItem
      Action = Action_SubtractMam
    end
    object N6: TMenuItem
      Action = Action_ChooseMehiron
    end
  end
end

unit FrmSearch;

interface

uses
  bde, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,StdCtrls, Buttons, DBCtrls,DB, DBTables, Mask,AtmComp, ComCtrls,
  AtmAdvTable, Scale250, RxLookup, AtmLookupCombo, Grids, DBGrids, DBSrtGrd,
  ToolEdit, RXDBCtrl, AtmDBDateEdit;

type
  TFrm_Search = class(TForm)
    Btn_Search: TBitBtn;
    Btn_Replace: TBitBtn;
    Btn_ReplaceAll: TBitBtn;
    Lbl_SearchFor: TLabel;
    Label1: TLabel;
    HebEdit1: TEdit;
    Label2: TLabel;
    HebEdit2: TEdit;
    Btn_SearchLike: TBitBtn;
    Btn_Exit: TBitBtn;
    Table_Temp: TAtmTable;
    StatusBar1: TStatusBar;
    CB_CompareAction: TComboBox;
    GroupBox1: TGroupBox;
    HebLabel1: TLabel;
    HebLabel2: TLabel;
    HebLabel3: TLabel;
    HCB_Filter: TComboBox;
    HEdit_From: TEdit;
    HEdit_To: TEdit;
    Scale1: TScale;
    procedure Btn_SearchLikeClick(Sender: TObject);
    procedure Btn_ReplaceClick(Sender: TObject);
    procedure Btn_SearchClick(Sender: TObject);
    procedure Btn_ReplaceAllClick(Sender: TObject);
    procedure Btn_ExitClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure HebEdit1Enter(Sender: TObject);
    procedure HCB_FilterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Btn_ExitMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FieldFrom,FieldTo :TField;
    StopAction :Boolean;
    Procedure PositionOriginalTable;
    Procedure PositionOriginalTableOld;
    Function SearchOneRecord(SearchAlike :Boolean):Boolean;
    Procedure EnableButtons(ToEnable:Boolean);
  public
    { Public declarations }
    Table_Org :Ttable;           {����� ���� ������}
    StartRecordBookMark: {LongInt}TBookMark;     {����� ��' ������ ���� ������ �� ������}
    Key_Locate_Name : ShortString; {��� ����� �� ���� ���� ������ �� ������}

    ListParam : TStringList;     {����� ����� �������� �� �����}

    Procedure StartSearch;
  end;

Function CompTFieldAndText(TheField :TField; Text :String) :SmallInt;
Function FixMinMaxResult(Value :LongInt):LongInt;
var
  Frm_Search: TFrm_Search;

implementation

uses inifiles;
{$R *.DFM}

var
  IniFile: TIniFile;
//-----------------------------------------------------------------------
//                      ����� ������ ���� ����
procedure TFrm_Search.Btn_SearchLikeClick(Sender: TObject);
Begin
    EnableButtons(False);
    Try
        if Not SearchOneRecord(True) then
        Begin
            if MessageDlg ('�� ����� ������ ������� ��� ����� ������ ����� ������',
                        mtConfirmation,[mbYes,mbNo],0) = mrYes Then
               //Table_Org.RecNo:=StartRecordBookMark;
               Table_Org.GotoBookmark(StartRecordBookMark);
        End
        else
            PositionOriginalTable;
    Finally
           EnableButtons(True);
    End;
end;
//-----------------------------------------------------------------------
//                        ����� ��� ������ �����
procedure TFrm_Search.Btn_ReplaceClick(Sender: TObject);
begin
  EnableButtons(False);
  Try
    Table_Org.Edit;
    Table_Org.FieldByName(Key_Locate_Name).AsString := HebEdit2.Text;
    Table_Org.Post;
  Finally
         EnableButtons(True);
  End;
end;
//-----------------------------------------------------------------------
//                         ����� �� ��� ���
procedure TFrm_Search.Btn_SearchClick(Sender: TObject);
Begin
   EnableButtons(False);
   Try
     if  Not SearchOneRecord(False) then
     Begin
           if MessageDlg ('�� ����� ������ ������� ��� ����� ������ ����� ������',
                      mtConfirmation,[mbYes,mbNo],0) = mrYes Then
             //Table_Org.RecNo:=StartRecordBookMark;
             Table_Temp.GotoBookmark(StartRecordBookMark);
     End
     else
        PositionOriginalTable;
   Finally
          EnableButtons(True);
   End;
end;
//-----------------------------------------------------------------------
//              ����� �� ������� �� ����� ���� "�� ����" ����
procedure TFrm_Search.Btn_ReplaceAllClick(Sender: TObject);
Var
   TotalReplace :LongInt;
begin
  TotalReplace:=0;
  EnableButtons(False);
  StatusBar1.Panels[0].Text:='������ '+IntToStr(TotalReplace)+' ������';
  Try
      While SearchOneRecord(False) Do
      Begin
             Table_Temp.Edit;
             Table_Temp.FieldByName(Key_Locate_Name).AsString := HebEdit2.Text;
             Table_Temp.Post;
             Inc(TotalReplace);
             StatusBar1.Panels[0].Text:='������ '+IntToStr(TotalReplace)+' ������';
      End;
  Finally
         EnableButtons(True);
         dbiSaveChanges (Table_Temp.Handle);
         Table_Org.Refresh;
  End;

end;
//-----------------------------------------------------------------------
procedure TFrm_Search.Btn_ExitClick(Sender: TObject);
begin
   if Btn_Exit.Caption='����' Then
      StopAction:=True
   Else
       Close;
end;
//-----------------------------------------------------------------------

Procedure TFrm_Search.StartSearch;
var
   i : integer;
   Sw_FindField:Boolean;
begin
    Sw_FindField:=False;
    if (Screen.Forms[0].ActiveControl is TAtmDBHEdit) Then
    Begin
        Table_Org:=((Screen.Forms[0].ActiveControl As TAtmDBHEdit).DataSource.DataSet As TAtmTable);
        Key_Locate_Name:=(Screen.Forms[0].ActiveControl As TAtmDBHEdit).DataField;
        Lbl_SearchFor.Caption :='�����/����� ���� '+
                         (Screen.Forms[0].ActiveControl As TAtmDBHEdit).LinkLabel.Caption;
    End
    Else
    if (Screen.Forms[0].ActiveControl is TAtmDBLookupCombo) Then
    Begin
        Table_Org:=((Screen.Forms[0].ActiveControl As TAtmDBLookupCombo).DataSource.DataSet As TAtmTable);
        Key_Locate_Name:=(Screen.Forms[0].ActiveControl As TAtmDBLookupCombo).DataField;
        Lbl_SearchFor.Caption :='�����/����� ���� '+
                         (Screen.Forms[0].ActiveControl As TAtmDBLookupCombo).LinkLabel.Caption;
    End
    Else
    if (Screen.Forms[0].ActiveControl is TDbSrtGrd) Then
    Begin
        Table_Org:=((Screen.Forms[0].ActiveControl As TDbSrtGrd).DataSource.DataSet As TAtmTable);
        Key_Locate_Name:=(Screen.Forms[0].ActiveControl As TDbSrtGrd).SelectedField.FieldName;
        Lbl_SearchFor.Caption :='�����/����� ���� '+
                         (Screen.Forms[0].ActiveControl As TDbSrtGrd).Columns[(Screen.Forms[0].ActiveControl As TDbSrtGrd).SelectedIndex].Title.Caption;

    End
    Else
    if (Screen.Forms[0].ActiveControl is TDbGrid) Then
    Begin
        Table_Org:=((Screen.Forms[0].ActiveControl As TDbGrid).DataSource.DataSet As TAtmTable);
        Key_Locate_Name:=(Screen.Forms[0].ActiveControl As TDbGrid).SelectedField.FieldName;
        Lbl_SearchFor.Caption :='�����/����� ���� '+
                         (Screen.Forms[0].ActiveControl As TDBGrid).Columns[(Screen.Forms[0].ActiveControl As TDBGrid).SelectedIndex].Title.Caption;
    End
    Else
    if (Screen.Forms[0].ActiveControl is TAtmDbDateEdit) Then
    Begin
        Table_Org:=((Screen.Forms[0].ActiveControl As TAtmDbDateEdit).DataSource.DataSet As TAtmTable);
        Key_Locate_Name:=(Screen.Forms[0].ActiveControl As TAtmDbDateEdit).DataField;
        Lbl_SearchFor.Caption :='�����/����� ���� '+
                         (Screen.Forms[0].ActiveControl As TAtmDBDateEdit).LinkLabel.Caption;
    End
    Else
        Begin
            ShowMessage('�� ���� ���� ����� ����� ��');
            Frm_Search:=Nil;
            Free;
            Exit;
        End;

   if (Table_Org.State <> DsBrowse) then
   Begin
        ShowMessage('�� ���� ���� ��� ���� �� ����� ����');
        Frm_Search:=Nil;
        Free;
        Exit;
   End;

   Table_Temp.DatabaseName:=Table_Org.DatabaseName;
   Table_Temp.TableName:=Table_Org.TableName;
   Table_Temp.IndexFieldNames:=Table_Org.IndexFieldNames;



              {����� ��� ������ �� ��� ����� - �� ���� �����}
    for i:= 0 to Table_Org.FieldDefs.Count - 1 do
        if  (Uppercase(Table_Org.FieldDefs[i].Name)) = UpperCase(Key_Locate_Name) then
           Sw_FindField := True;
                    {����� �� ��� ����� - �� ���� �����}
    if Not Sw_FindField then
       begin
         showmessage ('�� ���� ���� �� ����');
         Frm_Search:=Nil;
         Free;
         Exit;
       end;

   CB_CompareAction.ItemIndex:=0;
   EnableButtons(True);
   ListParam := TstringList.Create;

   Table_Temp.Filter:='';
   HCB_Filter.Clear;
   for i:= 0 to Table_Org.Fields.Count - 1 do
       if (Table_Org.Fields[I].FieldKind = fkData) Then
           HCB_Filter.Items.AddObject(Table_Org.Fields[I].DisplayName,Table_Org.Fields[I]);
   if Not Table_Temp.Active Then
      Table_Temp.Active:=True;

   Table_Temp.MasterSource:=Nil;
   Table_Temp.GotoCurrent (Table_Org);
   StartRecordBookMark:=Table_Temp.GetBookmark;
   StopAction:=False;
   Show;
End;

//-----------------------------------------------------------------------
procedure TFrm_Search.FormHide(Sender: TObject);
begin
   IniFile.Free;            {INI - ����� ���� �}
   ListParam.Free;
   if Table_Temp.Active Then
      Table_Temp.Active:=False;

end;
//-----------------------------------------------------------------------
procedure TFrm_Search.HebEdit1Enter(Sender: TObject);
begin
     {showmessage (listparam.strings[hebcombobox1.itemindex]);}
end;


Procedure TFrm_Search.PositionOriginalTableOld;
Var
   MasterTable :TTable;
   NumOfFields,I :longInt;
   MasterFields,DetailFields :TList;
   StrH :ShortString;
   KeyValues:Variant;
   Fields :String;
Begin
     if Table_Org.MasterSource=Nil Then
        Table_Org.GotoCurrent (Table_Temp)
     Else//�� �� ����
     Begin
       MasterFields:=TList.Create;
       DetailFields:=TList.Create;
       Try

          MasterTable:=Table_Org.MasterSource.DataSet As TTable;
          Table_Org.GetDetailLinkFields(MasterFields,DetailFields);

          KeyValues := VarArrayCreate([0, MasterFields.Count-1], varVariant);
          Try
              Fields := '';
              for i := 0 to MasterFields.Count - 1 do
                begin
                  KeyValues[i] :=(Table_Temp.FieldByName(TField(DetailFields[I]).FieldName).Value);
                  if Fields <> '' then
                     Fields := Fields + ';';
                  Fields := Fields + TField(MasterFields[I]).FieldName;
                end;
                if Pos(';',Fields)=0 Then
                Begin
                    if MasterTable.Locate(Fields, KeyValues[0], []) Then
                       {Table_Org.GotoCurrent (Table_Temp);}
                       Table_Org.Locate(Key_Locate_Name,Table_Temp.FieldByName(Key_Locate_Name).Value,[]);
                End
                Else
                    if MasterTable.Locate(Fields, KeyValues, []) Then
                       Table_Org.GotoCurrent (Table_Temp);
          Finally

          End;
       Finally
              MasterFields.Free;
              DetailFields.Free;
       End;
     End;
End;

Procedure TFrm_Search.PositionOriginalTable;
Var
   MasterTable :TTable;
   NumOfFields,I :longInt;
   MasterFields,DetailFields :TList;
   StrH :ShortString;
   KeyValues:Variant;
   Fields :String;
   LastMasterKeyFields :String;
Begin
     if Table_Org.MasterSource=Nil Then
        Table_Org.GotoCurrent (Table_Temp)
     Else//�� �� ����
     Begin
       MasterFields:=TList.Create;
       DetailFields:=TList.Create;
       Try

          MasterTable:=Table_Org.MasterSource.DataSet As TTable;
          Table_Org.GetDetailLinkFields(MasterFields,DetailFields);

          KeyValues := VarArrayCreate([0, MasterFields.Count-1], varVariant);
          Try
              Fields := '';
              for i := 0 to MasterFields.Count - 1 do
                begin
                  KeyValues[i] :=(Table_Temp.FieldByName(TField(DetailFields[I]).FieldName).Value);
                  if Fields <> '' then
                     Fields := Fields + ';';
                  Fields := Fields + TField(MasterFields[I]).FieldName;
                end;
                if Pos(';',Fields)=0 Then
                Begin
                     LastMasterKeyFields:=MasterTable.IndexFieldNames;
                     MasterTable.IndexFieldNames:=Fields;
                     if MasterTable.FindKey([KeyValues[0]]) Then
                       Table_Org.Locate(Key_Locate_Name,Table_Temp.FieldByName(Key_Locate_Name).Value,[]);
                     MasterTable.IndexFieldNames:=LastMasterKeyFields;
                End
                Else
                    if MasterTable.Locate(Fields, KeyValues, []) Then
                       Table_Org.GotoCurrent (Table_Temp);
          Finally

          End;
       Finally
              MasterFields.Free;
              DetailFields.Free;
       End;
     End;
End;

Function TFrm_Search.SearchOneRecord(SearchAlike :Boolean):Boolean;
var
  sw_find : boolean;
  sw_sof_chipus : boolean;
  ConditionCompare :LongInt;
begin
   Try
       Case CB_CompareAction.ItemIndex of
            1:ConditionCompare:=-1;//<=
            2:ConditionCompare:=1//>=
          Else
              ConditionCompare:=0//=
       End; //Case
       Sw_Find :=false;
       sw_sof_chipus :=false;
       Table_Temp.GotoCurrent (Table_Org);
       repeat
          Table_Temp.Next;
          if  Table_Temp.EOF  then
              Table_Temp.First;
          if  (Table_Temp.CompareBookmarks(Table_Temp.GetBookmark,StartRecordBookMark)=0 {Table_Temp.RecNo = StartRecordBookMark})  then
              Sw_sof_Chipus := True;
          StatusBar1.Panels[1].Text:=Table_Temp.FieldByName(Key_Locate_Name).AsString;
          Application.ProcessMessages;
          Case SearchAlike of
              False:
                   {if  (Table_Temp.FieldByName(Key_Locate_Name).AsString = HebEdit1.Text) Then}
                    if (FixMinMaxResult(CompTFieldAndText(Table_Temp.FieldByName(Key_Locate_Name),HebEdit1.Text))=ConditionCompare) Then
                     if (((HEdit_From.Text<>'') And (HEdit_To.Text<>'')) And (
                       (CompTFieldAndText(Table_Temp.FieldByName(FieldFrom.FieldName),HEDit_From.Text) >=0) And
                       (CompTFieldAndText(Table_Temp.FieldByName(FieldTo.FieldName),HEdit_To.Text) <=0))) or
                       ((HEdit_From.Text='') Or (HEdit_To.Text='')) then
                       sw_Find :=True;
              True:
                   if ((Pos (Trim(HebEdit1.Text),Table_Temp.FieldByName(Key_Locate_Name).AsString)) > 0) Then
                      if (((HEdit_From.Text<>'') And (HEdit_To.Text<>'')) And (
                       (CompTFieldAndText(Table_Temp.FieldByName(FieldFrom.FieldName),HEDit_From.Text) >=0) And
                       (CompTFieldAndText(Table_Temp.FieldByName(FieldTo.FieldName),HEdit_To.Text) <=0))) or
                       ((HEdit_From.Text='') Or (HEdit_To.Text='')) then

                      sw_Find :=True;
           End;
       until
          sw_sof_chipus or sw_find Or StopAction;  {Table_Temp.EOF}
       Result:=sw_find;
    Finally
      StatusBar1.Panels[1].Text:='';
    End;
End;

procedure TFrm_Search.HCB_FilterClick(Sender: TObject);
Var
   TheField :TField;
   IsStr:String[1];
begin
     if (HCB_Filter.ItemIndex>-1) And (HEdit_From.Text<>'')
        And (HEdit_To.Text<>'') Then
     Begin
(*          TheField := (HCB_Filter.Items.Objects[HCB_Filter.ItemIndex] AS TField);
          if (TheField.DataType=ftString) or
              (TheField.DataType=ftDateTime) Then
             IsStr:=''''
          Else
              IsStr:='';
          Table_Temp.Filter:=TheField.FieldName+'>='+IsStr
                             +HEdit_From.Text+IsStr+' And '+
                             TheField.FieldName+'<='+IsStr+
                             HEdit_To.Text+IsStr;
*)

         TheField := (HCB_Filter.Items.Objects[HCB_Filter.ItemIndex] AS TField);
         FieldFrom.FieldName:=TheField.FieldName;
         FieldTo.FieldName:=TheField.FieldName;
     End;
end;

procedure TFrm_Search.FormCreate(Sender: TObject);
begin
     FieldFrom:=TNumericField.Create(Self);
     FieldTo:=TNumericField.Create(Self);
end;

procedure TFrm_Search.FormDestroy(Sender: TObject);
begin
     FieldFrom.Free;
     FieldTo.Free;
end;

Function CompTFieldAndText(TheField :TField; Text :String) :SmallInt;
// Return >0 if TheField is Bigger
// 0 if Equal
// <0 Text is Bigger
Var
   RealH :Real;
   StrH  :ShortString;
   DateH :TDateTime;
Begin
   Try
       if TheField is TNumericField Then
       Begin
            RealH:=TheField.AsFloat;
            Result:=Round(RealH-StrToFloat(Text));
            Exit;
       End;

       if TheField is TDateTimeField Then
       Begin
            DateH:=TheField.AsDateTime;
            Result:=Round(DateH-StrToDate(Text));
            Exit;
       End;

       if TheField is TStringField Then
       Begin
            Result:=CompareText(TheField.AsString,Text);
            Exit;
       End;
   Except On Exception Do
     Begin
          ShowMessage('���� ����� ����� ����');
          Result:=1;
     End;
   End;
End;


Procedure TFrm_Search.EnableButtons(ToEnable:Boolean);
Begin
    StopAction:=False;
    Btn_Search.Enabled:=ToEnable;
    Btn_Replace.Enabled:=ToEnable;
    Btn_ReplaceAll.Enabled:=ToEnable;
    Btn_SearchLike.Enabled:=ToEnable;
    if Not ToEnable Then
       Btn_Exit.Caption:='����'
    Else
        Btn_Exit.Caption:='�����';
    if ToEnable Then
       Cursor:=crDefault
    Else
    {Screen.}Cursor:=crHourGlass;
    Application.ProcessMessages;
End;
procedure TFrm_Search.Btn_ExitMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     Cursor:=crDefault;
end;

Function FixMinMaxResult(Value :LongInt):LongInt;
Begin
     if Value<0 Then Result:=-1
     Else
         if Value >0 Then Result:=1
         Else
             Result:=0;
End;


procedure TFrm_Search.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Search:=Nil;
     Action:=caFree;
end;

end.


unit F_LakoachAtm;
{
 ���� ����� ����� �����
 Kod_Kesher=��� ������ ��
 IshKesher=����
 Integer_1=���� ������
 Integer_2=���� ���"�
 Tosefet_Achuz=��' �����
 Sug_Madad_3=���� ����
 Sug_Madad_2=���� ����� ����
 Mutzmad_Achuz_1=���� ��
 String_1=����� ������ ����� ������ ���� �������
 Money_1 = ���� ����� ����� �����
 Money_2 = ���� ����� ������ �����
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, DBCtrls, Db, DBTables, AtmAdvTable, Grids, DBGrids, DBSrtGrd,
  StdCtrls, AtmComp, ToolWin, Mask,
  ExtCtrls, AtmLookCombo, Scale250, AdvSearch, DualList, AtmListDlg,
  Buttons, RXCtrls, RXDBCtrl,MslDllInterface, DBFilter, RxQuery,
  AtmTabSheetBuild, ToolEdit, AtmDBDateEdit, AtmRxQuery, DynamicControls,
  Menus;

type
  TFrm_LakoachAtm = class(TForm)
    Panel3: TPanel;
    HebLabel1: TLabel;
    HebLabel2: TLabel;
    HebLabel3: TLabel;
    HebLabel4: TLabel;
    HebLabel5: TLabel;
    Kod: TAtmDbHEdit;
    AtmDbHEdit2: TAtmDbHEdit;
    Kod_kvutza: TAtmDbHEdit;
    Kod_Miyun: TAtmDbHEdit;
    AtmDbHEdit21: TAtmDbHEdit;
    ToolBar1: TToolBar;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    HebLabel14: TLabel;
    HebLabel15: TLabel;
    HebLabel16: TLabel;
    HebLabel26: TLabel;
    HebLabel27: TLabel;
    HebGroupBox1: TGroupBox;
    HebLabel6: TLabel;
    HebLabel7: TLabel;
    HebLabel8: TLabel;
    HebLabel9: TLabel;
    AtmDbHEdit_City: TAtmDbHEdit;
    AtmDbHEdit5: TAtmDbHEdit;
    AtmDbHEdit6: TAtmDbHEdit;
    AtmDbHEdit_Address: TAtmDbHEdit;
    HebGroupBox2: TGroupBox;
    HebLabel11: TLabel;
    HebLabel12: TLabel;
    HebLabel13: TLabel;
    AtmDbHEdit_Address2: TAtmDbHEdit;
    AtmDbHEdit_City2: TAtmDbHEdit;
    AtmDbHEdit_ZipCode2: TAtmDbHEdit;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmDbHEdit10: TAtmDbHEdit;
    AtmDbHEdit17: TAtmDbHEdit;
    AtmDbHMemo1: TAtmDbHMemo;
    TabSheet2: TTabSheet;
    HebLabel31: TLabel;
    HebLabel32: TLabel;
    HebLabel33: TLabel;
    HebLabel34: TLabel;
    HebLabel36: TLabel;
    HebLabel37: TLabel;
    HebLabel38: TLabel;
    HebLabel39: TLabel;
    HebLabel40: TLabel;
    HebLabel41: TLabel;
    HebLabel42: TLabel;
    AtmDbHEdit27: TAtmDbHEdit;
    AtmDbHEdit28: TAtmDbHEdit;
    AtmDbHEdit29: TAtmDbHEdit;
    AtmDbHEdit30: TAtmDbHEdit;
    AtmDbHEdit31: TAtmDbHEdit;
    AtmDbHEdit32: TAtmDbHEdit;
    AtmDbHEdit33: TAtmDbHEdit;
    AtmDbHEdit34: TAtmDbHEdit;
    AtmDbHEdit36: TAtmDbHEdit;
    AtmDbHEdit37: TAtmDbHEdit;
    TabSheet3: TTabSheet;
    Grid_Lakoach: TDBGrid;
    TS_Extra: TTabSheet;
    Tbl_Lakoach: TAtmRxQuery;
    Tbl_LakoachKod_Lakoach: TIntegerField;
    Tbl_LakoachShem_Lakoach: TStringField;
    Tbl_LakoachKod_Kvutza: TIntegerField;
    Tbl_LakoachKod_Miyun: TIntegerField;
    Tbl_LakoachMis_Han: TStringField;
    Tbl_LakoachKtovet_1: TStringField;
    Tbl_LakoachIr_1: TStringField;
    Tbl_LakoachMikud_1: TIntegerField;
    Tbl_LakoachKtovet_2: TStringField;
    Tbl_LakoachIr_2: TStringField;
    Tbl_LakoachMikud_2: TIntegerField;
    Tbl_LakoachTel_1: TStringField;
    Tbl_LakoachTel_2: TStringField;
    Tbl_LakoachFax: TStringField;
    Tbl_LakoachPle_Phon: TStringField;
    Tbl_LakoachIsh_Kesher: TStringField;
    Tbl_LakoachOsek_Morshe_ID: TStringField;
    Tbl_LakoachKod_Tnai_Tashlum: TIntegerField;
    Tbl_LakoachKod_Eara: TIntegerField;
    Tbl_LakoachSug_Anacha_Amala: TIntegerField;
    Tbl_LakoachKod_Kesher: TIntegerField;
    Tbl_LakoachEarot: TMemoField;
    Tbl_LakoachKod_Banehag: TIntegerField;
    Tbl_LakoachKod_Basapak: TIntegerField;
    Tbl_LakoachKod_Atzmada: TIntegerField;
    Tbl_LakoachKod_mechiron: TIntegerField;
    Tbl_LakoachSug_Koteret: TIntegerField;
    Tbl_LakoachSug_Miyun: TIntegerField;
    Tbl_LakoachMis_Taktziv: TStringField;
    Tbl_LakoachChoze: TStringField;
    Tbl_LakoachSug_Bitachon: TIntegerField;
    Tbl_LakoachTosefet_Melel: TStringField;
    Tbl_LakoachSug_Madad_1: TIntegerField;
    Tbl_LakoachSug_Madad_2: TIntegerField;
    Tbl_LakoachSug_Madad_3: TIntegerField;
    Tbl_LakoachString_1: TStringField;
    Tbl_LakoachString_2: TStringField;
    Tbl_LakoachString_3: TStringField;
    Tbl_LakoachString_4: TStringField;
    Tbl_LakoachString_5: TStringField;
    Tbl_LakoachInteger_1: TIntegerField;
    Tbl_LakoachInteger_2: TIntegerField;
    Tbl_LakoachInteger_3: TIntegerField;
    Tbl_LakoachInteger_4: TIntegerField;
    Tbl_LakoachInteger_5: TIntegerField;
    Tbl_LakoachTaarich_Osek_Morshe: TDateTimeField;
    Tbl_LakoachTaarich_Mas: TDateTimeField;
    Tbl_LakoachTaarich_Atchala: TDateTimeField;
    Tbl_LakoachTaarich_Siyum: TDateTimeField;
    Tbl_LakoachTaarich_Siyun_Bitachon: TDateTimeField;
    Tbl_LakoachTaarich_Madad_1: TDateTimeField;
    Tbl_LakoachTaarich_Madad_2: TDateTimeField;
    Tbl_LakoachTaarich_madad_3: TDateTimeField;
    Tbl_LakoachDate_1: TDateTimeField;
    Tbl_LakoachDate_2: TDateTimeField;
    Tbl_LakoachDate_3: TDateTimeField;
    Tbl_LakoachDate_4: TDateTimeField;
    Tbl_LakoachDate_5: TDateTimeField;
    Tbl_LakoachString_6: TStringField;
    Tbl_LakoachString_7: TStringField;
    Tbl_LakoachString_8: TStringField;
    Tbl_LakoachSchum_Bitachon: TBCDField;
    Tbl_LakoachYatza_Teudat_Mishloach: TBCDField;
    Tbl_LakoachYatza_Cheshbon_Lo_Shula: TBCDField;
    Tbl_LakoachChekim_Dachui_Lo_Nifra: TBCDField;
    Tbl_LakoachMoney_1: TBCDField;
    Tbl_LakoachMoney_2: TBCDField;
    Tbl_LakoachMoney_3: TBCDField;
    Tbl_LakoachMoney_4: TBCDField;
    Tbl_LakoachMoney_5: TBCDField;
    Tbl_LakoachMoney_6: TBCDField;
    Tbl_LakoachMoney_7: TBCDField;
    Tbl_LakoachInt_Kod_Kvutza: TIntegerField;
    Tbl_LakoachInt_Kod_Miyun: TIntegerField;
    Tbl_LakoachInt_Kod_Tnai_Tashlum: TIntegerField;
    Tbl_LakoachMas_Makor: TBCDField;
    Tbl_LakoachAchuz_Anacha_Amala: TBCDField;
    Tbl_LakoachAchuz_Anacha_Amala_2: TBCDField;
    Tbl_LakoachTosefet_Achuz: TBCDField;
    Tbl_LakoachMadad_Basis_1: TBCDField;
    Tbl_LakoachMutzmad_Achuz_1: TBCDField;
    Tbl_LakoachMadad_Basis_2: TBCDField;
    Tbl_LakoachMutzmad_Achuz_2: TBCDField;
    Tbl_LakoachMadad_Basis_3: TBCDField;
    Tbl_LakoachMutzmad_Achuz_3: TBCDField;
    Ds_Lakoach: TDataSource;
    DBNavigator1: TDBNavigator;
    ToolButton1: TToolButton;
    Tbl_LakoachLookKvutza: TStringField;
    Tbl_LakoachLookMiun: TStringField;
    Tbl_LakoachLookRemark: TStringField;
    TBtn_OpenSearch: TToolButton;
    AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox;
    HebLabel19: TLabel;
    HebLabel20: TLabel;
    AtmDbHEdit13: TAtmDbHEdit;
    Scale1: TScale;
    AtmDbComboBox_Group: TAtmDbComboBox;
    AtmDbComboBox_Kod_Miyun: TAtmDbComboBox;
    AtmAdvSearch_Lako: TAtmAdvSearch;
    Tbl_LakoachX: TIntegerField;
    Tbl_LakoachY: TIntegerField;
    DualListDialog1: TDualListDialog;
    Tbl_LakoachSug_Rikuz: TStringField;
    Query_MakavLako: TQuery;
    DS_MakavLako: TDataSource;
    Spb_OpenMslDlgSearchPlace: TSpeedButton;
    Tbl_LakoachNodeNumB: TIntegerField;
    Tbl_LakoachNodeSugSystem: TIntegerField;
    Tbl_LakoachNodeKodCityName: TIntegerField;
    Tbl_LakoachMis_Bait1: TStringField;
    Tbl_LakoachMis_Bait2: TStringField;
    AtmDbHEdit_HouseNum: TAtmDbHEdit;
    HebLabel43: TLabel;
    HebLabel44: TLabel;
    AtmDbHEdit_HouseNum2: TAtmDbHEdit;
    StatusBar1: TStatusBar;
    ToolButton2: TToolButton;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery_TabSheetBuild: TRxQuery;
    RxDBFilter_TabSheetBuild: TRxDBFilter;
    DS_TabSheetBuild: TDataSource;
    ToolButton3: TToolButton;
    TBtn_Report: TToolButton;
    AtmAdvSearch_Mehiron: TAtmAdvSearch;
    AtmDBDateEdit2: TAtmDBDateEdit;
    AtmDBDateEdit4: TAtmDBDateEdit;
    UpdateSQL_Lakoach: TUpdateSQL;
    DynamicControls1: TDynamicControls;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    HebLabel23: TLabel;
    AtmDBDateEdit1: TAtmDBDateEdit;
    HebLabel24: TLabel;
    AtmDBDateEdit3: TAtmDBDateEdit;
    Label1: TLabel;
    AtmDbHEdit3: TAtmDbHEdit;
    Label2: TLabel;
    AtmDbHEdit4: TAtmDbHEdit;
    Label3: TLabel;
    AtmDbHEdit7: TAtmDbHEdit;
    AtmDbComboBox_DatabaseType: TAtmDbComboBox;
    Label4: TLabel;
    AtmDbHEdit8: TAtmDbHEdit;
    AtmDbComboBox_SugHan: TAtmDbComboBox;
    Label5: TLabel;
    AtmDbHEdit9: TAtmDbHEdit;
    DBCheckBox_Pail: TDBCheckBox;
    HebLabel29: TLabel;
    SpeedButton1: TSpeedButton;
    HebLabel18: TLabel;
    AtmDbHEdit24: TAtmDbHEdit;
    AtmDbHEdit19: TAtmDbHEdit;
    HebLabel35: TLabel;
    HebLabel30: TLabel;
    AtmDbHEdit25: TAtmDbHEdit;
    AtmDbHEdit26: TAtmDbHEdit;
    HebLabel21: TLabel;
    HebLabel22: TLabel;
    HebLabel10: TLabel;
    HebLabel28: TLabel;
    AtmDbHEdit11: TAtmDbHEdit;
    AtmDbHEdit12: TAtmDbHEdit;
    AtmDbHEdit15: TAtmDbHEdit;
    AtmDbHEdit_CodeMehiron: TAtmDbHEdit;
    AtmTabSheetBuild2: TAtmTabSheetBuild;
    RxQuery_TSBLakoachAtm: TRxQuery;
    RxDBFilter_TSB_LakoachAtm: TRxDBFilter;
    DS_LakoachAtm: TDataSource;
    UpdateSQL_TSB2: TUpdateSQL;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    TBtn_SumAmoutTnuot: TToolButton;
    Panel_Sherut: TPanel;
    Label6: TLabel;
    AtmDbHEdit16: TAtmDbHEdit;
    DBCheckBox1: TDBCheckBox;
    HebLabel17: TLabel;
    AtmDbHEdit18: TAtmDbHEdit;
    HebLabel25: TLabel;
    AtmDbhedit14: TAtmDbHEdit;
    HebDBText3: TDBText;
    AtmDbHEdit20: TAtmDbHEdit;
    Label7: TLabel;
    Label8: TLabel;
    AtmDbHEdit22: TAtmDbHEdit;
    Label9: TLabel;
    DBText1: TDBText;
    Tbl_LakoachCalc_ItratShaot: TBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBNavigator1BeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_LakoachAfterInsert(DataSet: TDataSet);
    procedure Tbl_LakoachAfterPost(DataSet: TDataSet);
    procedure Kod_Tnai_TashlumBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmDbhedit14BeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_MiyunBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_kvutzaBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Ds_LakoachStateChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure HebLabel28DblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure RxDBGrid_MakavGetBtnParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; var SortMarker: TSortMarker;
      IsDown: Boolean);
    procedure RxDBGrid_MakavGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure RxDBGrid_MakavTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Ds_LakoachDataChange(Sender: TObject; Field: TField);
    procedure Spb_OpenMslDlgSearchPlaceClick(Sender: TObject);
    procedure Tbl_LakoachKtovet_1Validate(Sender: TField);
    procedure Tbl_LakoachAfterScroll(DataSet: TDataSet);
    procedure KodExit(Sender: TObject);
    procedure Kod_MiyunExit(Sender: TObject);
    procedure Tbl_LakoachNewRecord(DataSet: TDataSet);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure RxQuery_TSBLakoachAtmAfterPost(DataSet: TDataSet);
    procedure RxQuery_TSBLakoachAtmAfterInsert(DataSet: TDataSet);
    procedure AtmTabSheetBuild2BeforeExecuteQuery(Sender: TObject);
    procedure RxQuery_TSBLakoachAtmBeforePost(DataSet: TDataSet);
    procedure RxQuery_TSBLakoachAtmAfterDelete(DataSet: TDataSet);
    procedure AtmTabSheetBuild2AfterCreateGrid(Sender: TObject);
    procedure AtmTabSheetBuild2GridEditButtonClick(Sender: TObject);
    procedure RxQuery_TSBLakoachAtmAfterOpen(DataSet: TDataSet);
    procedure TBtn_SumAmoutTnuotClick(Sender: TObject);
    procedure AtmDbHEdit7BeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmDbHEdit8BeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Tbl_LakoachCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    CurSortFieldName:String;
    RerutnToProc:TRerutnToProc;
    OkToUpdateNode :Boolean;   //Put OkToUpdateNode:=False; AfterPost And AfterScroll Events
    procedure ThreadCheckAddressDone(Sender :TObject);
    procedure RunThreadToCheckAddress;
    Procedure PutNodeDataInRecord(TheNode :TNode);
    procedure OpenNewKodTavlaValue(Sender: TField;SugTavla:LongInt);
    Procedure ChangeStatusBarColor;
  public
    { Public declarations }
    Procedure BuildLookupList;
    Procedure ShowForm(FormParams:ShortString);
    procedure RunQryMakav(OrderByFieldName :String);
    procedure RxQuery_TSBLakoachAtmCodeItemValidate(Sender: TField);
  end;

var
  Frm_LakoachAtm: TFrm_LakoachAtm;

implementation
Uses AtmConst,AtmRutin,DMAtmCrt,Crt_Glbl, F_RangeDateCode;
{$R *.DFM}
Const
     PnlNum_DSState=0;
     PnlNum_Text=1;
     TziudTab='TziudTab';
     SoftTab='SoftTab';
     IshKesherTab='IshKesherTab';

procedure TFrm_LakoachAtm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_LakoachAtm:=Nil;
     Action:=caFree;
end;

procedure TFrm_LakoachAtm.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     DynamicControls1.ExtraFiledsFileName:=CrtDirForScripts+DynamicControls1.ExtraFiledsFileName;
     DynamicControls1.Execute;
     BuildLookupList;

     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataSet) And (Components[I].Tag=1) Then
             (Components[I] As TDataSet).Open
          Else
              if (Components[I] is TAtmAdvSearch) Then
                  TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;

     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
     AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
     AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;
     AtmTabSheetBuild1.BuildTabsForGrids;

     AtmTabSheetBuild2.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild2.ScrFileName;
     AtmTabSheetBuild2.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild2.SqlFileName;
     AtmTabSheetBuild2.SqlUpdateFileName:=CrtDirForScripts+AtmTabSheetBuild2.SqlUpdateFileName;
     AtmTabSheetBuild2.SqlDeleteFileName:=CrtDirForScripts+AtmTabSheetBuild2.SqlDeleteFileName;
     AtmTabSheetBuild2.SqlInsertFileName:=CrtDirForScripts+AtmTabSheetBuild2.SqlInsertFileName;
     AtmTabSheetBuild2.BuildTabsForGrids;

     PageControl1.ActivePageIndex:=0;
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_LakoachAtm.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataSet Then
             (Components[I] As TDataSet).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_LakoachAtm.DBNavigator1BeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
//        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
         ((Sender As TDBNavigator).DataSource.DataSet As TAtmRxQuery).SavePressed:=True;
     if Button=nbRefresh Then
     Begin
        (Sender As TDBNavigator).DataSource.DataSet.Close;
        (Sender As TDBNavigator).DataSource.DataSet.Open;
        BuildLookupList;
        Abort;
     End;
end;

procedure TFrm_LakoachAtm.Tbl_LakoachAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_LakoachAtm.Tbl_LakoachAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     OkToUpdateNode:=False;
end;

procedure TFrm_LakoachAtm.Kod_Tnai_TashlumBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_TnaeyTashlum;
end;

procedure TFrm_LakoachAtm.AtmDbhedit14BeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
    ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_CodeRemark;
end;

procedure TFrm_LakoachAtm.Kod_MiyunBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_MiunLako;
end;

procedure TFrm_LakoachAtm.Kod_kvutzaBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
 ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_GroupLako;
end;

procedure TFrm_LakoachAtm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Lakoach);
end;

procedure TFrm_LakoachAtm.Ds_LakoachStateChange(Sender: TObject);
begin
     DM_AtmCrt.Action_OpenSearchUpdate(TBtn_OpenSearch.Action);
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

procedure TFrm_LakoachAtm.SpeedButton1Click(Sender: TObject);
Var
   StrH :String;
   I :longint;
begin
     if DualListDialog1.Execute Then
     Begin
          StrH:='';
          For I:=0 To DualListDialog1.List2.Count-1 Do
          Begin
              StrH:=StrH+DualListDialog1.List2[i];
              if I<DualListDialog1.List2.Count-1 Then StrH:=StrH+',';
          End;
          if Tbl_Lakoach.State=dsBrowse Then
             Tbl_Lakoach.Edit;
          Tbl_Lakoach.FieldByName('Sug_Rikuz').AsString:=StrH;
     End;
end;

Procedure TFrm_LakoachAtm.BuildLookupList;
var
   SList :TStringList;
   I :longInt;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_GroupLako),Tbl_LakoachLookKvutza);
     SList.AddObject(IntToStr(SugTavla_MiunLako),Tbl_LakoachLookMiun);
     SList.AddObject(IntToStr(SugTavla_CodeRemark),Tbl_LakoachLookRemark);

     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_TnaeyTashlum),AtmDbComboBox_TnaeyTashlum);
     SList.AddObject(IntToStr(SugTavla_GroupLako),AtmDbComboBox_Group);
     SList.AddObject(IntToStr(SugTavla_MiunLako),AtmDbComboBox_Kod_Miyun);
     SList.AddObject(IntToStr(SugTavla_DatabaseType),AtmDbComboBox_DatabaseType);
     SList.AddObject(IntToStr(SugTavla_SugInterfaceHan),AtmDbComboBox_SugHan);
     For I:=0 To ComponentCount-1 Do
     Begin
        if Components[i] is TAtmDbComboBox Then
          if TAtmDbComboBox(Components[i]).SugTavlaIndex>0 Then
            SList.AddObject(IntToStr(TAtmDbComboBox(Components[i]).SugTavlaIndex),Components[i]);
     End;

     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;
procedure TFrm_LakoachAtm.HebLabel28DblClick(Sender: TObject);
begin
     OpenAtmCrt(fnMehiron,AtmDbHEdit_CodeMehiron.Text);
end;

Procedure TFrm_LakoachAtm.ShowForm(FormParams:ShortString);
Begin
     Show;
     if FormParams<>'' Then
        Tbl_Lakoach.FindKey([FormParams])
     Else
//        Tbl_Lakoach.Insert;
       SetDatasetState(Tbl_Lakoach,CrtFirstTableState);
End;

procedure TFrm_LakoachAtm.PageControl1Change(Sender: TObject);
begin
     if Query_MakavLako.Active Then
        Query_MakavLako.Active:=False;
{     if PageControl1.ActivePage=TS_GridMakav Then
     Begin
          RunQryMakav('HesbonitNumber');
          DBNavigator1.DataSource:=DS_MakavLako;
     End
     Else
         DBNavigator1.DataSource:=Ds_Lakoach;}
end;

procedure TFrm_LakoachAtm.RunQryMakav(OrderByFieldName :String);
Begin
  Try
     Screen.Cursor:=crHourGlass;
      CurSortFieldName:=OrderByFieldName;
      With Query_MakavLako Do
      Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select MAKAV.*,TotalHesbonitWithMAM-Total_Zikuy Itra From MAKAV');
           Sql.Add('Where CodeLakoach='+Kod.Text);
           Sql.Add('Order By '+OrderByFieldName);
           Active:=True;
      End;
  Finally
         Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_LakoachAtm.RxDBGrid_MakavGetBtnParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor;
  var SortMarker: TSortMarker; IsDown: Boolean);
begin
     if CompareText(CurSortFieldName,Field.FieldName)=0 Then
          SortMarker := smDown;
end;

procedure TFrm_LakoachAtm.RxDBGrid_MakavGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
   if Field<>Nil Then
     if CompareText(Field.FieldName,CurSortFieldName)=0 Then
        Background:=clInfoBk;
end;

procedure TFrm_LakoachAtm.RxDBGrid_MakavTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
     RunQryMakav(Field.FieldName);
end;

procedure TFrm_LakoachAtm.Ds_LakoachDataChange(Sender: TObject;
  Field: TField);
begin
     if Field=Nil Then
        PageControl1Change(Self);
end;

procedure TFrm_LakoachAtm.Spb_OpenMslDlgSearchPlaceClick(Sender: TObject);
Var
   TheNode :TNode;
begin
     if Not MslDllLoaded Then
        Exit;
     if MslFindNodeFromDlgSearchPlace(TheNode,'') Then
     Begin
          if Tbl_Lakoach.State=dsBrowse Then
             Tbl_Lakoach.Edit;
          Tbl_LakoachIr_1.AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,1);
          Tbl_LakoachKtovet_1.AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,2);
          Tbl_LakoachMis_Bait1.AsString:=GetFieldFromSeparateString(TheNode.NameOfPoint,MslSeparator,3);

          PutNodeDataInRecord(TheNode);
     End;
end;

procedure TFrm_LakoachAtm.RunThreadToCheckAddress;
Begin
   if (Not ThreadCheckAddressEnded) And (ThreadCheckAddress<>Nil) Then
   Begin
        ThreadCheckAddress.Terminate;
        RerutnToProc:=RunThreadToCheckAddress;
        StatusBar1.Panels[PnlNum_Text].Text:='���� ������ ����� ����';
        Exit;
   End;
   ThreadCheckAddress:= TThreadCheckAddress.Create(AtmDbHEdit_City.Text,AtmDbHEdit_Address.Text,AtmDbHEdit_HouseNum.Text);
   ThreadCheckAddress.OnTerminate := ThreadCheckAddressDone;
   StatusBar1.Panels[PnlNum_Text].Text:='���� ����� ��������...';
   RerutnToProc:=Nil;
   OkToUpdateNode:=True;
End;

procedure TFrm_LakoachAtm.ThreadCheckAddressDone(Sender :TObject);
Begin
     if (ThreadCheckAddressEnded) And (ThreadCheckAddressRes<>faCityNotFound) Then
     Begin
          StatusBar1.Panels[PnlNum_Text].Text:='����� ����� '+ThreadCheckAddressReturnNode.NameOfPoint;
          if OkToUpdateNode Then
             PutNodeDataInRecord(ThreadCheckAddressReturnNode);
     End
     Else
     Begin
        if ThreadCheckAddressRes=faLibraryNotLoaded Then
           StatusBar1.Panels[PnlNum_Text].Text:=''
        Else
        Begin
            StatusBar1.Panels[PnlNum_Text].Text:='����� �� ����� ��������';
            Beep;
        End;
     End;
     ThreadCheckAddress:=Nil;
     if @RerutnToProc<>Nil Then RerutnToProc;
End;

Procedure TFrm_LakoachAtm.PutNodeDataInRecord(TheNode :TNode);
Begin
    Tbl_LakoachX.AsInteger:=TheNode.Crd.X;
    Tbl_LakoachY.AsInteger:=TheNode.Crd.Y;
    Tbl_LakoachNodeNumB.AsInteger:=TheNode.Numb;
    Tbl_LakoachNodeSugSystem.AsInteger:=TheNode.SugSystem;
    Tbl_LakoachNodeKodCityName.AsInteger:=TheNode.KodCityName;
End;

procedure TFrm_LakoachAtm.Tbl_LakoachKtovet_1Validate(Sender: TField);
begin
     if (Not Tbl_LakoachKtovet_1.IsNull) And (Not Tbl_LakoachMis_Bait1.IsNull)
        And (Not Tbl_LakoachIr_1.IsNull) Then
        RunThreadToCheckAddress;
end;

procedure TFrm_LakoachAtm.Tbl_LakoachAfterScroll(DataSet: TDataSet);
begin
    OkToUpdateNode:=False;
    if AtmTabSheetBuild1.Query.Active Then
       AtmTabSheetBuild1.RefreshQry(Nil);
    if AtmTabSheetBuild2.Query.Active Then
       AtmTabSheetBuild2.RefreshQry(Nil);
{    if PageControl1.ActivePage=TS_GridMakav Then
    Begin
         if Query_MakavLako.Active Then
            Query_MakavLako.Active:=False;
         RunQryMakav('HesbonitNumber');
         DBNavigator1.DataSource:=DS_MakavLako;
    End;}
    ChangeStatusBarColor;
end;

procedure TFrm_LakoachAtm.KodExit(Sender: TObject);
begin
  Try
     if Tbl_Lakoach.State<>dsInsert Then
        Exit;
     Screen.Cursor:=crHourGlass;
     if Tbl_LakoachKod_Lakoach.IsNull Then
     Begin
          DM_AtmCrt.Qry_Temp.Active:=False;
          With DM_AtmCrt.Qry_Temp.Sql Do
          Begin
               Clear;
               Add('Select Max(Kod_Lakoach) MaxCode');
               Add('From Lakoach');
          End;
          DM_AtmCrt.Qry_Temp.Active:=True;
          Tbl_LakoachKod_Lakoach.AsInteger:=DM_AtmCrt.Qry_Temp.FieldByName('MaxCode').AsInteger+1;

     End;
  Finally
         DM_AtmCrt.Qry_Temp.Active:=False;
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_LakoachAtm.Kod_MiyunExit(Sender: TObject);
begin
     if (Not Tbl_LakoachKod_Miyun.IsNull) And (Tbl_LakoachLookMiun.IsNull) Then
        OpenNewKodTavlaValue(Tbl_LakoachKod_Miyun,SugTavla_MiunLako);
end;

procedure TFrm_LakoachAtm.OpenNewKodTavlaValue(Sender: TField;SugTavla:LongInt);
Var
   StrH:String;
begin
     StrH:=Sender.AsString;
     OpenAtmCrt(fnTavla,IntToStr(SugTavla)+','+StrH);
     Sender.Clear;
     BuildLookupList;
     Sender.AsString:=StrH;
end;

procedure TFrm_LakoachAtm.Tbl_LakoachNewRecord(DataSet: TDataSet);
Var
   MaxLak :LongInt;
begin
     With DM_AtmCrt Do
     Begin
          Qry_Temp.Active:=False;
          With Qry_Temp.Sql Do
          Begin
               Clear;
               Add('Select Max(Kod_Lakoach) ML From Lakoach');
          End;
          Qry_Temp.Active:=True;
          MaxLak:=Qry_Temp.FieldByName('ML').AsInteger+1;
          Qry_Temp.Active:=False;
          Tbl_LakoachKod_lakoach.AsInteger:=MaxLak;
     End;
end;

procedure TFrm_LakoachAtm.AtmTabSheetBuild1BeforeExecuteQuery(
  Sender: TObject);
begin
     if TQuery(AtmTabSheetBuild1.Query).Params.FindParam('PLakNum')<>Nil Then
        TQuery(AtmTabSheetBuild1.Query).ParamByName('PLakNum').AsInteger:=Tbl_LakoachKod_Lakoach.AsInteger;
end;

procedure TFrm_LakoachAtm.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msLakoach],False,CrtMeholelHandle);
end;

Procedure TFrm_LakoachAtm.ChangeStatusBarColor;
Begin
  if (Not Tbl_Lakoach.FieldByName('Taarich_Siyum').IsNull) And
     (Tbl_Lakoach.FieldByName('Taarich_Siyum').AsDateTime<Now) Then
  Begin
    Panel_Sherut.Color:=clRed;
    Panel_Sherut.Caption:='���� �� �����';
  End
  Else
  if (Not Tbl_Lakoach.FieldByName('Sug_Madad_3').IsNull) And
     (Tbl_Lakoach.FieldByName('Sug_Madad_3').AsInteger=0) Then
  Begin
    Panel_Sherut.Color:=clYellow;
    Panel_Sherut.Caption:='���� �� ����';
  End
  Else
  Begin
    Panel_Sherut.Color:=clLime;
    Panel_Sherut.Caption:='���� �����';
  End;

End;
procedure TFrm_LakoachAtm.RxQuery_TSBLakoachAtmAfterPost(
  DataSet: TDataSet);
begin
  RxQuery_TSBLakoachAtm.Database.ApplyUpdates([RxQuery_TSBLakoachAtm]);
end;

procedure TFrm_LakoachAtm.RxQuery_TSBLakoachAtmAfterInsert(
  DataSet: TDataSet);
begin
  if RxQuery_TSBLakoachAtm.FindField('CodeMaster')<>Nil Then
    RxQuery_TSBLakoachAtm.FieldByName('CodeMaster').AsInteger:=Tbl_Lakoach.FieldByName('Kod_Lakoach').AsInteger;
  if RxQuery_TSBLakoachAtm.FindField('MaklidName')<>Nil Then
    RxQuery_TSBLakoachAtm.FieldByName('MaklidName').AsString:=CrtCurUserName;
  if RxQuery_TSBLakoachAtm.FindField('LastUpdate')<>Nil Then
    RxQuery_TSBLakoachAtm.FieldByName('LastUpdate').AsDateTime:=Now;
end;

procedure TFrm_LakoachAtm.AtmTabSheetBuild2BeforeExecuteQuery(
  Sender: TObject);
begin
     if TQuery(AtmTabSheetBuild2.Query).Params.FindParam('PLakNum')<>Nil Then
        TQuery(AtmTabSheetBuild2.Query).ParamByName('PLakNum').AsInteger:=Tbl_LakoachKod_Lakoach.AsInteger;
end;

procedure TFrm_LakoachAtm.RxQuery_TSBLakoachAtmBeforePost(
  DataSet: TDataSet);
begin
//  if RxQuery_TSBLakoachAtm.FindField('CodeItem')<>Nil Then
//    if RxQuery_TSBLakoachAtm.FieldByName('CodeItem').IsNull Then
    if AtmTabSheetBuild2.CurRxDbGrid.Fields[0].IsNull Then
    Begin
      RxQuery_TSBLakoachAtm.Cancel;
      Abort;
    End;
end;

procedure TFrm_LakoachAtm.RxQuery_TSBLakoachAtmAfterDelete(
  DataSet: TDataSet);
begin
  RxQuery_TSBLakoachAtm.Database.ApplyUpdates([RxQuery_TSBLakoachAtm]);
end;

procedure TFrm_LakoachAtm.AtmTabSheetBuild2AfterCreateGrid(
  Sender: TObject);
Var
  I :LongInt;
begin
  if (CompareText(TAtmTabSheetBuild(Sender).CurID,TziudTab)=0) Or
     (CompareText(TAtmTabSheetBuild(Sender).CurID,SoftTab)=0) Then
  Begin
    For I:=0 To TAtmTabSheetBuild(Sender).CurRxDbGrid.Columns.Count-1 Do
      if (CompareText(TAtmTabSheetBuild(Sender).CurRxDbGrid.Columns[i].FieldName,'CodeItem')=0) Then
        TAtmTabSheetBuild(Sender).CurRxDbGrid.Columns[i].ButtonStyle:=cbsEllipsis;
  End;
end;

procedure TFrm_LakoachAtm.AtmTabSheetBuild2GridEditButtonClick(
  Sender: TObject);
begin
  if CompareText(AtmTabSheetBuild2.CurRxDbGrid.SelectedField.FieldName,'CodeItem')=0 Then
    if AtmAdvSearch_Maslul.Execute Then
    Begin
      if AtmTabSheetBuild2.CurRxDbGrid.SelectedField.DataSet.State=dsBrowse Then
        AtmTabSheetBuild2.CurRxDbGrid.SelectedField.DataSet.Edit;
      AtmTabSheetBuild2.CurRxDbGrid.SelectedField.AsString:=AtmAdvSearch_Maslul.ReturnString;
    End;
end;

procedure TFrm_LakoachAtm.RxQuery_TSBLakoachAtmAfterOpen(
  DataSet: TDataSet);
begin
  if (CompareText(AtmTabSheetBuild2.CurID,TziudTab)=0) Or
     (CompareText(AtmTabSheetBuild2.CurID,SoftTab)=0) Then
    if (RxQuery_TSBLakoachAtm.FindField('CodeItem')<>Nil) And
       (RxQuery_TSBLakoachAtm.FindField('Teur')<>Nil) Then
    Begin
      RxQuery_TSBLakoachAtm.FieldByName('CodeItem').OnValidate:=RxQuery_TSBLakoachAtmCodeItemValidate;
    End;

end;

procedure TFrm_LakoachAtm.RxQuery_TSBLakoachAtmCodeItemValidate(Sender: TField);
Begin
  if Sender.AsString='' Then
    Exit;
  With DM_AtmCrt.Qry_Temp Do
  Begin
    Active:=False;
    Sql.Clear;
    Sql.Add('Select NAME From MASLUL Where CODE_MASLUL='+Sender.AsString);
    Try
      Active:=True;
      First;
      if Not Eof Then
        RxQuery_TSBLakoachAtm.FieldByName('Teur').AsString:=FieldByName('NAME').AsString
      Else
      Begin
        MessageDlg('��� �� ���� ��������',mtError,[mbOk],0);
        Cancel;
        Abort;
      End;
    Finally
      Active:=False;
    End;
  End;

End;
procedure TFrm_LakoachAtm.TBtn_SumAmoutTnuotClick(Sender: TObject);
begin
  Frm_RangeDateCode := TFrm_RangeDateCode.Create(Nil);
  Try
    Frm_RangeDateCode.ShowModal;
    if Frm_RangeDateCode.ModalResult = mrOk Then
    Begin
      Screen.Cursor:=crSqlWait;
      With DM_AtmCrt.Qry_Temp Do
      Begin
        Active:=False;
        Sql.Clear;
        Sql.Add('Select Sum(Quntity1) TQ, Count(*) Cnt From ATMSHIB');
        Sql.Add('Where ShibAzmnDate Between '''+DateToSqlStr(Frm_RangeDateCode.DateEdit_From.Date,'mm/dd/yyyy')+'''');
        Sql.Add(' And '''+DateToSqlStr(Frm_RangeDateCode.DateEdit_To.Date,'mm/dd/yyyy')+'''');
        Sql.Add('And MaslulCode1 Between '+Frm_RangeDateCode.ComboEdit_FromCode.Text);
        Sql.Add('And '+Frm_RangeDateCode.ComboEdit_ToCode.Text);
        Active:=True;
        ShowMessage('��"� ����='+FieldByName('TQ').AsString+
                    #13'��"� ������='+FieldByName('Cnt').AsString);
        Active:=False;
      End;
    End;
  Finally
    Frm_RangeDateCode.Free;
    Frm_RangeDateCode:=Nil;
    Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_LakoachAtm.AtmDbHEdit7BeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_DatabaseType;
end;

procedure TFrm_LakoachAtm.AtmDbHEdit8BeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugInterfaceHan;
end;

procedure TFrm_LakoachAtm.Tbl_LakoachCalcFields(DataSet: TDataSet);
begin
  Tbl_LakoachCalc_ItratShaot.AsFloat:=Tbl_LakoachMoney_1.AsFloat - Tbl_LakoachMoney_2.AsFloat;
end;

end.

{ 16/03/99 11:23:05 > [Aviran on AVIRAN] check in: (0.0)  / None }
unit Frm_WinTuda;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, Mask, ExtCtrls, AtmComp, ComCtrls,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, Grids, DBGrids, RXSplit, Placemnt,
  DBTables, AtmAdvTable, Buttons, ToolWin, ActnList, ImgList, Menus,
  AdvSearch, KeyBoard, AppEvent, Scale250, SpeedBar, AtmLookCombo,IniFiles,
  AtmTabSheetBuild, DynamicControls, RxQuery, DBFilter,AtmRxQuery,TypInfo,
  dbnav797, ExeMenu;

type
  TFrmWinTuda = class(TForm)
    Panel_Main: TPanel;
    PageControl_Main: TPageControl;
    TS_Tnua: TTabSheet;
    StatusBar1: TStatusBar;
    Panel_Top: TPanel;
    Lbl_Hazmana: TLabel;
    AtmDbHEdit_Hazmana: TAtmDbHEdit;
    Lbl_AzmnSugHazmana: TLabel;
    Lbl_FromAzmnDate: TLabel;
    AtmDBDateEdit_AzmnFromDate: TAtmDBDateEdit;
    Lbl_AzmnFromTime: TLabel;
    Lbl_ToAzmnDate: TLabel;
    AtmDBDateEdit_AzmnToDate: TAtmDBDateEdit;
    Lbl_AzmnToTime: TLabel;
    Lbl_ShemLakoach: TLabel;
    AtmDbHEdit_Lak: TAtmDbHEdit;
    Pnl_Mid: TPanel;
    Lbl_TnuaDate: TLabel;
    AtmDBDateEdit_TnuaDate: TAtmDBDateEdit;
    Lbl_Rehev: TLabel;
    AtmDbHEdit_Rehev: TAtmDbHEdit;
    Lbl_Nehag1: TStaticText;
    AtmDbHEdit_Nehag1: TAtmDbHEdit;
    Label9: TLabel;
    AtmDbHEdit_ShemNehag: TAtmDbHEdit;
    Lbl_SugHovala: TLabel;
    AtmDbHEdit_HovalaKind: TAtmDbHEdit;
    FormStorage_WinTeuda: TFormStorage;
    PageControl_Tnua: TPageControl;
    TS_Tnua1: TTabSheet;
    Panel_TsTnuaMain: TPanel;
    Lbl_AzmnAsmachta: TLabel;
    AtmDbHEdit_AzmnAsmachta: TAtmDbHEdit;
    Label2: TLabel;
    AtmDbHEdit_AutoInc: TAtmDbHEdit;
    ScrollBox1: TScrollBox;
    Label20: TLabel;
    AtmDbHEdit_YeMida1: TAtmDbHEdit;
    Label21: TLabel;
    AtmDbHEdit_Kamut1: TAtmDbHEdit;
    Label22: TLabel;
    AtmDbHEdit_Price1: TAtmDbHEdit;
    Label23: TLabel;
    AtmDbHEdit_Total1: TAtmDbHEdit;
    Lbl_PriceA: TLabel;
    AtmDbHEdit_YeMida2: TAtmDbHEdit;
    AtmDbHEdit_Kamut2: TAtmDbHEdit;
    AtmDbHEdit_Price2: TAtmDbHEdit;
    AtmDbHEdit_Total2: TAtmDbHEdit;
    Lbl_PriceB: TLabel;
    AtmDbHEdit_YeMida3: TAtmDbHEdit;
    AtmDbHEdit_Kamut3: TAtmDbHEdit;
    AtmDbHEdit_Price3: TAtmDbHEdit;
    AtmDbHEdit_Total3: TAtmDbHEdit;
    Lbl_PriceC: TLabel;
    AtmDbHEdit_YeMida4: TAtmDbHEdit;
    AtmDbHEdit_Kamut4: TAtmDbHEdit;
    AtmDbHEdit_Price4: TAtmDbHEdit;
    AtmDbHEdit_Total4: TAtmDbHEdit;
    Lbl_PriceD: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    ImageList_Buttons: TImageList;
    ActionList1: TActionList;
    Action_NewHazmana: TAction;
    Action_OpenSearch: TAction;
    MainMenu1: TMainMenu;
    NFile: TMenuItem;
    NClose: TMenuItem;
    NNewHazmana: TMenuItem;
    N3: TMenuItem;
    NEdit: TMenuItem;
    NOpenSearchWindow: TMenuItem;
    DBText_LakName: TDBText;
    NRefreshAllSearchWindows: TMenuItem;
    Action_CalcTotalTeudaAzmn: TAction;
    N2: TMenuItem;
    TS_StatusFields: TTabSheet;
    Pnl_TabSheetExtraFields: TPanel;
    AtmKeyBoard1: TAtmKeyBoard;
    NStartKeyboard: TMenuItem;
    AppEvents1: TAppEvents;
    PopupMenu_Mehiron: TPopupMenu;
    Action_Price1: TAction;
    ActionPrice11: TMenuItem;
    Action_LastPriceListAction: TAction;
    Action_Price2: TAction;
    ActionPrice21: TMenuItem;
    Action_Price1PlusPrice2: TAction;
    Action_SubtractMam: TAction;
    Action_Price3: TAction;
    N31: TMenuItem;
    N121: TMenuItem;
    Action_Price4: TAction;
    N41: TMenuItem;
    Scale1: TScale;
    NCrt: TMenuItem;
    Action_CrtOpenLak: TAction;
    N4: TMenuItem;
    Action_CrtOpenNehag: TAction;
    Action_CrtOpenRehev: TAction;
    ActionCrtOpenNehag1: TMenuItem;
    N5: TMenuItem;
    Action_ChooseMehiron: TAction;
    N6: TMenuItem;
    Action_CrtOpenMehiron: TAction;
    ActionCrtOpenMehiron1: TMenuItem;
    MaskEdit_FromTime: TMaskEdit;
    MaskEdit_ToTime: TMaskEdit;
    TS_Perut2: TTabSheet;
    DBGrid_Preut2: TRxDBGrid;
    Action_DupRecord: TAction;
    N7: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    ToolBar_Tnua: TToolBar;
    DBNavigator: TAtmDbNavigator;
    Btn_PostNew: TBitBtn;
    TBtn_NewHeshbon: TToolButton;
    ToolButton1: TToolButton;
    SpeedBar1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    NEditRxSpeedBar: TMenuItem;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem12: TSpeedItem;
    SpeedItem13: TSpeedItem;
    SpeedItem14: TSpeedItem;
    SpeedItem15: TSpeedItem;
    Label6: TLabel;
    AtmDbHEdit_Lakoach: TAtmDbHEdit;
    DBText6: TDBText;
    AtmDbComboBox_SugHovala: TAtmDbComboBox;
    Lbl_Teuda: TLabel;
    AtmDbHEdit_Teuda: TAtmDbHEdit;
    Panel_Extra: TPanel;
    AtmDbHMemo_Remark: TAtmDbHMemo;
    Lbl_Remark: TLabel;
    Action_CrtOpenKodTavla: TAction;
    N11: TMenuItem;
    Action_CrtOpenMaslul: TAction;
    N12: TMenuItem;
    SpeedItem16: TSpeedItem;
    DynamicControls1: TDynamicControls;
    Action_SelectExtraFile: TAction;
    N13: TMenuItem;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    Action_AllPrices: TAction;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem17: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    SpeedItem5: TSpeedItem;
    N1: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N21: TMenuItem;
    N32: TMenuItem;
    N42: TMenuItem;
    N122: TMenuItem;
    SpeedItem18: TSpeedItem;
    N16: TMenuItem;
    Panel_GridTnua: TPanel;
    DBGrid_Tnua: TRxDBGrid;
    N17: TMenuItem;
    N18: TMenuItem;
    Action_SearchCodesFromMehiron: TAction;
    Action_ChangeParams: TAction;
    N19: TMenuItem;
    N20: TMenuItem;
    TBtn_NewTnua: TToolButton;
    Action_NewTnua: TAction;
    Action_ChoosePointsFromMsl: TAction;
    Action_MeholelReport: TAction;
    NReports: TMenuItem;
    N22: TMenuItem;
    SpeedbarSection4: TSpeedbarSection;
    SpeedItem19: TSpeedItem;
    Action_MeholelMain: TAction;
    N23: TMenuItem;
    Action_ChooseYehus: TAction;
    N24: TMenuItem;
    Action_ShibKindHiuvNehag: TAction;
    Action_ShibKindNormal: TAction;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    Action_CrtOpenMahzeva: TAction;
    SpeedItem20: TSpeedItem;
    N28: TMenuItem;
    DBRG_ShibKind: TDBRadioGroup;
    Action_CopyTnuot: TAction;
    N29: TMenuItem;
    AtmDbComboBox_azmnType: TAtmDbComboBox;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label31: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    AtmDbHEdit_KBLak: TAtmDbHEdit;
    AtmDbHEdit_KBDriver: TAtmDbHEdit;
    AtmDbHEdit_Heshbonit: TAtmDbHEdit;
    AtmDbHEdit7: TAtmDbHEdit;
    Label17: TLabel;
    AtmDbHEdit5: TAtmDbHEdit;
    Label27: TLabel;
    AtmDbHEdit8: TAtmDbHEdit;
    Act_Refresh_Lako: TAction;
    Act_Refresh_Nehag: TAction;
    Act_Refresh_Rehev: TAction;
    Act_Refresh_Maslul: TAction;
    Act_Refresh_KodTavla: TAction;
    N30: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    Act_Maintaince_CloseMeholel: TAction;
    N38: TMenuItem;
    Splitter1: TSplitter;
    Act_EditScreenControls: TAction;
    N39: TMenuItem;
    Splitter2: TSplitter;
    Act_EditExtraFields: TAction;
    N40: TMenuItem;
    N43: TMenuItem;
    AtmDbHEdit_InvLakName: TAtmDbHEdit;
    Lbl_InvName: TLabel;
    Panel_Days: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBCheckBox_Day1: TDBCheckBox;
    DBCheckBox_Day2: TDBCheckBox;
    DBCheckBox_Day3: TDBCheckBox;
    DBCheckBox_Day4: TDBCheckBox;
    DBCheckBox_Day5: TDBCheckBox;
    DBCheckBox_Day6: TDBCheckBox;
    DBCheckBox_Day7: TDBCheckBox;
    Action_ChangeTab: TAction;
    Act_Maintaince_UPdatePricesInTnuaFromMehiron: TAction;
    N44: TMenuItem;
    Action_Price2SubHanForLakFromPrice1: TAction;
    N45: TMenuItem;
    N46: TMenuItem;
    Label1: TLabel;
    Label3: TLabel;
    AtmDbHEdit2: TAtmDbHEdit;
    AtmDbHEdit3: TAtmDbHEdit;
    Action_ShibKindKikuyLak: TAction;
    N47: TMenuItem;
    Act_HsbZikuyLakPerut: TAction;
    Act_HsbZikuyLakMake: TAction;
    Act_HsbZikuyLakCopy: TAction;
    SpeedbarSection5: TSpeedbarSection;
    SpeedItem21: TSpeedItem;
    SpeedItem22: TSpeedItem;
    SpeedItem23: TSpeedItem;
    N8: TMenuItem;
    N48: TMenuItem;
    N49: TMenuItem;
    N50: TMenuItem;
    Act_HsbZikuyLakCancel: TAction;
    N51: TMenuItem;
    Panel_ShibKind: TPanel;
    Action_PriceKindNoMam: TAction;
    N52: TMenuItem;
    TSB2: TAtmTabSheetBuild;
    DBRadioGroup1: TDBRadioGroup;
    Act_HsbPriceProposal: TAction;
    N53: TMenuItem;
    Act_HsbPrintHazmana: TAction;
    N54: TMenuItem;
    Action_AtmAzmnHatz: TAction;
    N55: TMenuItem;
    Action_CopyHatzaotToHazmanot: TAction;
    N56: TMenuItem;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AtmDbHEdit_YeMida1BeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Action_NewHazmanaExecute(Sender: TObject);
    procedure Action_NewHazmanaUpdate(Sender: TObject);
    procedure Action_OpenSearchExecute(Sender: TObject);
    procedure Action_OpenSearchUpdate(Sender: TObject);
    procedure NRefreshAllSearchWindowsClick(Sender: TObject);
    procedure DBGrid_TnuaColEnter(Sender: TObject);
    procedure Action_CalcTotalTeudaAzmnExecute(Sender: TObject);
    procedure ToolBar_TnuaEnter(Sender: TObject);
    procedure DBGrid_TnuaEnter(Sender: TObject);
    procedure NStartKeyboardClick(Sender: TObject);
    procedure AtmDbHEdit_HazmanaExit(Sender: TObject);
    procedure AppEvents1ActiveControlChange(Sender: TObject);
    procedure ActionList1Execute(Action: TBasicAction;
      var Handled: Boolean);
    procedure Action_Price1Execute(Sender: TObject);
    procedure Action_Price2Execute(Sender: TObject);
    procedure Action_LastPriceListActionUpdate(Sender: TObject);
    procedure Action_Price1PlusPrice2Execute(Sender: TObject);
    procedure Action_Price3Execute(Sender: TObject);
    procedure Action_Price4Execute(Sender: TObject);
    procedure DBGrid_TnuaEditButtonClick(Sender: TObject);
    procedure AtmDbHEdit_YeMida1DblClick(Sender: TObject);
    procedure Btn_PostNewClick(Sender: TObject);
    procedure ToolBar_TnuaExit(Sender: TObject);
    procedure Action_CrtOpenLakExecute(Sender: TObject);
    procedure Action_CrtOpenNehagExecute(Sender: TObject);
    procedure Action_CrtOpenRehevExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Action_ChooseMehironExecute(Sender: TObject);
    procedure Action_CrtOpenMehironExecute(Sender: TObject);
    procedure MaskEdit_FromTimeExit(Sender: TObject);
    procedure MaskEdit_ToTimeExit(Sender: TObject);
    procedure Action_DupRecordExecute(Sender: TObject);
    procedure AtmDBDateEdit_TnuaDateExit(Sender: TObject);
    procedure NEditRxSpeedBarClick(Sender: TObject);
    procedure AtmDbHEdit_LakoachEnter(Sender: TObject);
    procedure Pnl_MidEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Action_CrtOpenKodTavlaExecute(Sender: TObject);
    procedure Action_CrtOpenMaslulExecute(Sender: TObject);
    procedure Label6DblClick(Sender: TObject);
    procedure Action_SelectExtraFileExecute(Sender: TObject);
    procedure RxQuery_TabSheetBuildAfterScroll(DataSet: TDataSet);
    procedure NCloseClick(Sender: TObject);
    procedure AtmDbHEdit_HovalaKindBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Action_AllPricesExecute(Sender: TObject);
    procedure AtmDbHEdit_Price1Enter(Sender: TObject);
    procedure MaskEdit_FromTimeChange(Sender: TObject);
    procedure Action_SubtractMamExecute(Sender: TObject);
    procedure Action_SubtractMamUpdate(Sender: TObject);
    procedure AtmDBDateEdit_AzmnToDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid_TnuaGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure AtmTabSheetBuild1GridDblClick(Sender: TObject;
      ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
    procedure Action_SearchCodesFromMehironExecute(Sender: TObject);
    procedure Action_ChangeParamsExecute(Sender: TObject);
    procedure Action_NewTnuaExecute(Sender: TObject);
    procedure DBGrid_Preut2ColEnter(Sender: TObject);
    procedure Action_ChoosePointsFromMslExecute(Sender: TObject);
    procedure DBGrid_TnuaColExit(Sender: TObject);
    procedure DBGrid_TnuaExit(Sender: TObject);
    procedure Action_MeholelReportExecute(Sender: TObject);
    procedure Panel_TopExit(Sender: TObject);
    procedure AtmDBDateEdit_AzmnFromDateExit(Sender: TObject);
    procedure Action_MeholelMainExecute(Sender: TObject);
    procedure AtmDbHEdit_LakoachChange(Sender: TObject);
    procedure Action_ChooseYehusExecute(Sender: TObject);
    procedure Action_ShibKindHiuvNehagExecute(Sender: TObject);
    procedure Action_ShibKindNormalExecute(Sender: TObject);
    procedure AtmDbHEdit_Nehag1Change(Sender: TObject);
    procedure Action_CrtOpenMahzevaExecute(Sender: TObject);
    procedure Action_CopyTnuotExecute(Sender: TObject);
    procedure Act_Refresh_LakoExecute(Sender: TObject);
    procedure Act_Refresh_NehagExecute(Sender: TObject);
    procedure Act_Refresh_RehevExecute(Sender: TObject);
    procedure Act_Refresh_MaslulExecute(Sender: TObject);
    procedure Act_Refresh_KodTavlaExecute(Sender: TObject);
    procedure Act_Maintaince_CloseMeholelExecute(Sender: TObject);
    procedure Act_EditScreenControlsExecute(Sender: TObject);
    procedure Act_EditExtraFieldsExecute(Sender: TObject);
    procedure DBGrid_TnuaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Action_ChangeTabExecute(Sender: TObject);
    procedure Act_Maintaince_UPdatePricesInTnuaFromMehironExecute(
      Sender: TObject);
    procedure Action_Price2SubHanForLakFromPrice1Execute(Sender: TObject);
    procedure Action_ShibKindKikuyLakExecute(Sender: TObject);
    procedure Act_HsbZikuyLakPerutExecute(Sender: TObject);
    procedure Act_HsbZikuyLakMakeExecute(Sender: TObject);
    procedure Act_HsbZikuyLakCopyExecute(Sender: TObject);
    procedure Act_HsbZikuyLakCancelExecute(Sender: TObject);
    procedure Act_HsbZikuyLakPerutUpdate(Sender: TObject);
    procedure Action_PriceKindNoMamExecute(Sender: TObject);
    procedure DBGrid_TnuaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AtmDbHEdit_HazmanaBeforeAutoLocateRecord(Sender: TObject;
      var ContinueOperation: Boolean);
    procedure DBNavigatorBeforeAction(Sender: TObject; Button: TmoNavBtns);
    procedure Act_HsbPriceProposalExecute(Sender: TObject);
    procedure Act_HsbPrintHazmanaExecute(Sender: TObject);
    procedure Action_AtmAzmnHatzExecute(Sender: TObject);
    procedure Action_CopyHatzaotToHazmanotExecute(Sender: TObject);
    procedure AtmDbNavigator1BeforeAction(Sender: TObject;
      Button: TmoNavBtns);
  private
    { private declarations }
    CurMehironCode:longInt;
    LastGridFieldIndex :LongInt;
    Procedure BuildLookupList;
    Procedure CopyValuesFromAzmn;
    Procedure WriteMehironTypeToStatusBar;
  public
    { public declarations }
    Procedure ChangeDSForNavigator(TheNavi :TAtmDBNavigator);
    Procedure CopyMidPnlFields;
    Procedure PutPriceInField(PriceOpt,CodeMehiron :LongInt;TheFieldName:String = '');
    Procedure SetFocusOnActiveGrid;
    Procedure ShowHelpForField(TheField :TField);
    Procedure WriteYehusStatusBar;

  end;

var
  FrmWinTuda: TFrmWinTuda;


implementation

{$R *.DFM}

uses DMWinTuda,F_GlblWinTuda,AtmRutin,AtmConst, FrmSearch,F_WinTudaParam,
  MslDllInterface, F_ChooseYehus, F_CopyTnua, F_UpdPriceFromMehiron;

Const
  SizeBorder          =2;
  sc_SizeLeft         =$F001;
  sc_SizeRight        =$F002;
  sc_SizeTop          =$F003;
  sc_SizeTopLeft      =$F004;
  sc_SizeTopRight     =$F005;
  sc_SizeBottom       =$F006;
  sc_SizeBottomLeft   =$F007;
  sc_SizeBottomRight  =$F008;
  sc_DragMove         =$F012;

procedure TFrmWinTuda.FormCreate(Sender: TObject);
Var
   Day :Word;
begin
  Screen.Cursor:=crHourGlass;
  Try
     Application.Title:='���� 2000';
     Application.BiDiMode:=bdRightToLeft;
     Application.BiDiKeyboard:=HebBidiKeyboard;
     if Frm_GlblWinTuda=Nil Then
        Frm_GlblWinTuda:=TFrm_GlblWinTuda.Create(Self);
     if DM_WinTuda=Nil Then
        DM_WinTuda:=TDM_WinTuda.Create(Self);

     WinTudaShibKind:=skNormal;
     WinTudaPriceKind:=pkShekel;

     DecodeDate(Now,WinTudaYehusYear,WinTudaYehusMonth,Day);
     WriteYehusStatusBar;

  {         AtmTabSheetBuild1.SqlFileName:=DirectoryForScripts+AtmTabSheetBuild1.SqlFileName;
     AtmTabSheetBuild1.ScrFileName:=DirectoryForScripts+AtmTabSheetBuild1.ScrFileName;}
     AtmTabSheetBuild1.ScriptsDir:=DirectoryForScripts;
     AtmTabSheetBuild1.FilterDir:=DirectoryForCurUser;
     AtmTabSheetBuild1.BuildTabsForGrids;

     ReadF4ValuesToIni(Self,DirectoryForCurUser+Name+'.Ini','FixedFields');
     ActiveControl:=FindFirstControlInTabOrder(Self);

     DynamicControls1.ExtraFiledsFileName:=DirectoryForScripts+DynamicControls1.ExtraFiledsFileName;
     DynamicControls1.Execute;
     BuildLookupList;

    AtmDbComboBox_azmnType.Clear;
    AtmDbComboBox_azmnType.FreeListObjects;
    Case WinTudaSugSystem Of
      ssMaz    : Begin
                   AtmDbComboBox_azmnType.AddToList('�����',0);
                   AtmDbComboBox_azmnType.AddToList('���� ����',1);
                   AtmDbComboBox_azmnType.AddToList('�����',2);//�� ���� ����
                   AtmDbComboBox_azmnType.AddToList('�� ���� �����',3);
                   AtmDbComboBox_azmnType.AddToList('�����',4);
                   {AtmDbComboBox_azmnType.AddToList('����"�',5);}
                   AtmDbComboBox_azmnType.AddToList('���� ����',atHiuvNehag);
                   AtmDbComboBox_azmnType.AddToList('����� ����',atZikuyLak);

                   Action_CopyHatzaotToHazmanot.Visible:=True;
                   Action_AtmAzmnHatz.Visible:=True;
                 End;
      ssTovala : Begin
                   AtmDbComboBox_azmnType.AddToList('�����',0);
                   AtmDbComboBox_azmnType.AddToList('������ ������',1);
                   AtmDbComboBox_azmnType.AddToList('������',2);//�� ���� ����
                   AtmDbComboBox_azmnType.AddToList('�� ���� �����',3);
                   AtmDbComboBox_azmnType.AddToList('�����',4);
                   AtmDbComboBox_azmnType.AddToList('����"�',5);
                   AtmDbComboBox_azmnType.AddToList('���� ���',atHiuvNehag);
                   AtmDbComboBox_azmnType.AddToList('����� ����',atZikuyLak);
                 End;
    End; //Case

     Scale1.DoScaleNow;
  Finally
        Screen.Cursor:=crDefault;
  End;
end;

procedure TFrmWinTuda.FormDestroy(Sender: TObject);
begin
     SaveF4ValuesToIni(Self,DirectoryForCurUser+Name+'.ini','FixedFields');
     AtmKeyboard1.CloseKeyboard;
     if Frm_GlblWinTuda<>Nil Then
     Begin
          Frm_GlblWinTuda.Release;
          Frm_GlblWinTuda:=Nil;
     End;
     if DM_WinTuda<>Nil Then
     Begin
          DM_WinTuda.Free;
          DM_WinTuda:=Nil;
     End;
end;

procedure TFrmWinTuda.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var
   TheDS :TDataSet;
   TheDataSource :TDataSource;
begin
  if Not Visible Then Exit;
  TheDataSource:=GetDataSourceFromTComponent(ActiveControl);
  //     TheDs:=GetDataSetFromTComponent(ActiveControl);
  TheDs:=Nil;
  if TheDataSource<>Nil Then
    TheDs:=TheDataSource.DataSet;
  if TheDs<>Nil Then
    KeyboardManageForTableAction(ActiveControl,Key,Shift,TheDs)
  Else
    KeyboardManageForTableAction(ActiveControl,Key,Shift,DM_WinTuda.Tbl_Azmn);
end;


procedure TFrmWinTuda.AtmDbHEdit_YeMida1BeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_YeMida;
end;
Procedure TFrmWinTuda.ChangeDSForNavigator(TheNavi :TAtmDBNavigator);
Var
   TheDS :TDataSource;
begin
     if TheNavi=Nil Then Exit;
     if Not Visible Then Exit;
     TheDs:=GetDataSourceFromTComponent(ActiveControl);
     if (TheDs<>Nil) And (TheNavi.DataSource<>TheDs) Then
        TheNavi.DataSource:=TheDs;
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText(TheNavi.DataSource.DataSet);
End;
procedure TFrmWinTuda.Action_NewHazmanaExecute(Sender: TObject);
begin
     DM_WinTuda.Tbl_Azmn.SavePressed:=True;
     DM_WinTuda.Tbl_Shib.SavePressed:=True;
     if DM_WinTuda.Tbl_Azmn.State in [dsinsert,dsEdit] Then
       DM_WinTuda.Tbl_Azmn.Post;
     if DM_WinTuda.Tbl_Shib.State in [dsinsert,dsEdit] Then
       DM_WinTuda.Tbl_Shib.Post;
     DM_WinTuda.Tbl_Azmn.Append;
end;

procedure TFrmWinTuda.Action_NewHazmanaUpdate(Sender: TObject);
begin
//     (Sender As TAction).Enabled:=DM_WinTuda.Tbl_Azmn.State<>dsInsert;
end;

procedure TFrmWinTuda.Action_OpenSearchExecute(Sender: TObject);
Var
   Bool :Boolean;
Begin
     if ActiveControl is TAtmDbHEdit Then
         if (ActiveControl As TAtmDbHEdit).SearchComponent<>Nil Then
         Begin
              if Assigned((ActiveControl As TAtmDbHEdit).BeforeExecuteSearch) Then
                 (ActiveControl As TAtmDbHEdit).BeforeExecuteSearch(ActiveControl,Bool);
              if (ActiveControl As TAtmDbHEdit).SearchComponent.Execute Then
              Begin
                 if (ActiveControl As TAtmDbHEdit).DataSource.DataSet.State=dsBrowse Then
                    (ActiveControl As TAtmDbHEdit).DataSource.DataSet.Edit;
                 (ActiveControl As TAtmDbHEdit).Text:=(ActiveControl As TAtmDbHEdit).SearchComponent.ReturnString;
              End;
              SelectNext(ActiveControl,True,True);
              Exit;
         End;
     if ActiveControl is TDBGrid Then
     Begin
        if CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'MaslulCode1')=0 Then
           if Frm_GlblWinTuda.AtmAdvSearch_Maslul.Execute Then
           Begin
               if (ActiveControl As TDbGrid).DataSource.DataSet.State=dsBrowse Then
                  (ActiveControl As TDbGrid).DataSource.DataSet.Edit;
              Dm_WinTuda.Tbl_ShibMaslulCode1.AsString:=Frm_GlblWinTuda.AtmAdvSearch_Maslul.ReturnString;
              (ActiveControl As TDbGrid).SelectedIndex:=(ActiveControl As TDbGrid).SelectedIndex+1;
              Exit;
           End;

        if CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'Maslul1')=0 Then
        Begin
           Action_ChoosePointsFromMslExecute(Action_ChoosePointsFromMsl);
           Exit;
        End;

        if ((ActiveControl As TDbGrid).Columns[(ActiveControl As TDbGrid).SelectedIndex].ButtonStyle=cbsEllipsis) Then
        Begin
           DBGrid_TnuaEditButtonClick(ActiveControl);
           (ActiveControl As TDbGrid).SelectedIndex:=(ActiveControl As TDbGrid).SelectedIndex+1;
           Exit;
        End;
     End;
end;

procedure TFrmWinTuda.Action_OpenSearchUpdate(Sender: TObject);
begin
     if Not Visible Then Exit;
     if ActiveControl is TAtmDbHEdit Then
        (Sender As TAction).Enabled:=(ActiveControl As TAtmDbHEdit).SearchComponent<>Nil
     Else
     if (ActiveControl = DBGrid_Tnua) Or (ActiveControl = DBGrid_Preut2) Then
        (Sender As TAction).Enabled:=(CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'MaslulCode1')=0) Or
                                     (CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'Maslul1')=0)  
                                     Or ((ActiveControl As TDbGrid).Columns[(ActiveControl As TDbGrid).SelectedIndex].ButtonStyle=cbsEllipsis)

     Else
         (Sender As TAction).Enabled:=False;
end;

procedure TFrmWinTuda.NRefreshAllSearchWindowsClick(Sender: TObject);
Var
   I :LongInt;
begin
     For I:=0 To Frm_GlblWinTuda.ComponentCount-1 Do
         if Frm_GlblWinTuda.Components[I] is TAtmAdvSearch Then
            if (Frm_GlblWinTuda.Components[I] As TAtmAdvSearch).KeepList Then
               (Frm_GlblWinTuda.Components[I] As TAtmAdvSearch).ClearList;
end;

procedure TFrmWinTuda.DBGrid_TnuaColEnter(Sender: TObject);
begin
     if (Sender As TDBGrid).SelectedIndex>=(Sender As TDBGrid).Columns.Count-1 Then
     Begin
          if LastGridFieldIndex=0 Then
          Begin
             SelectNext(ActiveControl,False,True);
             (Sender As TDBGrid).SelectedIndex:=0;
          End
          Else
          Begin
//            AtmDbHEdit_YeMida1.SetFocus;
             SelectNext(ActiveControl,True,True);
            (Sender As TDBGrid).SelectedIndex:=(Sender As TDBGrid).SelectedIndex-1;
          End;
     End;
     Action_OpenSearchUpdate(Action_OpenSearch);
     ShowHelpForField((Sender As TDBGrid).SelectedField);
     LastGridFieldIndex:=(Sender As TDBGrid).SelectedIndex;
end;


procedure TFrmWinTuda.Action_CalcTotalTeudaAzmnExecute(Sender: TObject);
Var
   RealH :Real;
   StrH :String;
begin
     Screen.Cursor:=crHourGlass;
     StatusBar1.Panels[PnlNum_Help].Text:='���� ��"� �� ����';
     Application.ProcessMessages;
     Try
         if DM_WinTuda.Tbl_Shib.State<>dsBrowse Then
            DM_WinTuda.Tbl_Shib.Post;
         RealH:=Frm_GlblWinTuda.SumTotalForHazmana(DM_WinTuda.Tbl_Shib.FieldByName('ShibAzmnNo').AsInteger);
         StrH:=Format('%f ��"� �����',[RealH]);
         StatusBar1.Panels[PnlNum_TotalHazmana].Text:=StrH;

         if WinTudaSugSystem=ssMaz Then
         Begin
           RealH:=RealH*WinTudaMAM/100;
           StrH:=Format('%f ��"�',[RealH]);
         End
         Else
         Begin
           RealH:=Frm_GlblWinTuda.SumTotalForTuda(DM_WinTuda.Tbl_Shib.FieldByName('ShibTuda').AsInteger);
           StrH:=Format('%f ��"� �����',[RealH]);
         End;
         StatusBar1.Panels[PnlNum_TotalTuda].Text:=StrH;
     Finally
        StatusBar1.Panels[PnlNum_Help].Text:='';
        Screen.Cursor:=crDefault;
     End;
end;

Procedure TFrmWinTuda.CopyMidPnlFields;
Var
   I :LongInt;
   TheField :TField;
   TheText :ShortString;
Begin
  Try
    Screen.Cursor:=crSQLWait;
    DM_WinTuda.Tbl_ShibCarNum.OnValidate:=Nil;
    DM_WinTuda.Tbl_ShibDriverNo1.OnValidate:=Nil;
     For I:=0 To Pnl_Mid.ControlCount-1 Do
     Begin
          TheField:=Nil;
          if Pnl_Mid.Controls[i] is TAtmDbHEdit Then
          Begin
             TheField:=(Pnl_Mid.Controls[i] As TAtmDbHEdit).Field;
             TheText:=(Pnl_Mid.Controls[i] As TAtmDbHEdit).F9String;
          End
          Else
              if Pnl_Mid.Controls[i] is TAtmDbDateEdit Then
              Begin
                 TheField:=(Pnl_Mid.Controls[i] As TAtmDbDateEdit).Field;
                 TheText:=(Pnl_Mid.Controls[i] As TAtmDbDateEdit).F9String;
              End;
          Try
             if (TheField<>Nil) And (TheField.DataType<>ftAutoInc) Then
               if TheField.AsString='' Then
                  TheField.AsString:=TheText;
          Except On Exception Do;
          End;
     End;
  Finally
    DM_WinTuda.Tbl_ShibCarNum.OnValidate:=DM_WinTuda.Tbl_ShibCarNumValidate;
    DM_WinTuda.Tbl_ShibDriverNo1.OnValidate:=DM_WinTuda.Tbl_ShibDriverNo1Validate;
    Screen.Cursor:=crDefault;
  End;
End;
procedure TFrmWinTuda.ToolBar_TnuaEnter(Sender: TObject);
begin
{     if DM_WinTuda.Tbl_Shib.State=dsInsert Then
     Begin
          DoSaveOnTable(DM_WinTuda.Tbl_Shib);
          DM_WinTuda.Tbl_Shib.Post;
     End;}
     Btn_PostNew.Default:=True;
end;

procedure TFrmWinTuda.DBGrid_TnuaEnter(Sender: TObject);
begin
     ChangeDSForNavigator(DBNavigator);
     if Sender is TDBGrid Then
        (Sender As TDBGrid).SelectedIndex:=0;
end;

procedure TFrmWinTuda.NStartKeyboardClick(Sender: TObject);
begin
    AtmKeyboard1.Execute;
end;

procedure TFrmWinTuda.AtmDbHEdit_HazmanaExit(Sender: TObject);
begin
     if AtmDbHEdit_Hazmana.DataSource.State<>dsBrowse Then
       if AtmDbHEdit_Hazmana.Text='' Then
          DM_WinTuda.FindNextHazmanaNum;
end;

procedure TFrmWinTuda.AppEvents1ActiveControlChange(Sender: TObject);
begin
     Action_OpenSearchUpdate(Action_OpenSearch);
     Action_LastPriceListActionUpdate(Action_LastPriceListAction);
     ChangeDSForNavigator(DBNavigator);
end;

procedure TFrmWinTuda.ActionList1Execute(Action: TBasicAction;
  var Handled: Boolean);
begin
     if (Action is TAction) Then
        if (TAction(Action)<>Action_LastPriceListAction) And
           (CompareText(TAction(Action).Category,'PriceList')=0) Then
        Begin
             Action_LastPriceListAction.OnExecute:=TAction(Action).OnExecute;
             Action_LastPriceListAction.Hint:=TAction(Action).Hint;
             Action_LastPriceListAction.ShortCut:=TAction(Action).ShortCut;
        End;
end;

procedure TFrmWinTuda.Action_Price1Execute(Sender: TObject);
Begin
     PutPriceInField(poPrice1,-1);
end;

procedure TFrmWinTuda.Action_Price2Execute(Sender: TObject);
begin
     PutPriceInField(poPrice2,-1);
end;

procedure TFrmWinTuda.Action_LastPriceListActionUpdate(Sender: TObject);
Var
   I:LongInt;
begin
     if DM_WinTuda=Nil Then Exit;
          (Sender As TAction).Enabled:=(
                                        (ActiveControl = AtmDbHEdit_Price1) Or
                                        (ActiveControl = AtmDbHEdit_Price2) Or
                                        (ActiveControl = AtmDbHEdit_Price3) Or
                                        (ActiveControl = AtmDbHEdit_Price4));
          if (ActiveControl Is TDbGrid) Then
             (Sender As TAction).Enabled:=(
                                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice1) Or
                                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice2) Or
                                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice3) Or
                                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice4)
                                          );
          (Sender As TAction).Enabled:=((Sender As TAction).Enabled) And
                                       ((Not DM_WinTuda.Tbl_ShibSugMetan.IsNull) And (Not DM_WinTuda.Tbl_ShibMaslulCode1.IsNull) And(Not DM_WinTuda.Tbl_ShibShibAzmnDate.IsNull));
    For I:=0 To ActionList1.ActionCount-1 Do
        if CompareText(ActionList1.Actions[I].Category,'PriceList')=0 Then
           TAction(ActionList1.Actions[I]).Enabled:=(Sender As TAction).Enabled;
end;


procedure TFrmWinTuda.PutPriceInField(PriceOpt,CodeMehiron :LongInt;TheFieldName:String = '');
Var
   P1,P2,P3,P4 :Real;
   StrH :String;
   CodeMaslulForMehiron :LongInt;
   EV :TFieldNotifyEvent;
begin
  Try
     if AtmDbHEdit_Lakoach.Text='' Then
     Begin
          ShowMessage('��� ��� ����');
          Exit;
     End;
     Screen.Cursor:=crHourGlass;

     if WinTudaShibKind=skHiuvNehag Then //���� ������ ����
     Begin
          CodeMehiron:=0;
          if DM_WinTuda.Tbl_Nehag.FindKey([DM_WinTuda.Tbl_ShibDriverNo1.AsInteger]) Then
             CodeMehiron:=DM_WinTuda.Tbl_Nehag.FieldByName('Kod_Mechiron').AsInteger;

          if (CurMehironCode=0) And (CodeMehiron=0) Then
          Begin
               Exit;
          End;
     End;

     if CurMehironCode>0 Then
        CodeMehiron:=CurMehironCode;
     //����� ��� ����� ������
     CodeMaslulForMehiron:=DM_WinTuda.Tbl_ShibMaslulCode1.AsInteger;
     if DM_WinTuda.Tbl_Maslul.FindKey([CodeMaslulForMehiron]) Then
     Begin
        if (Not Dm_WinTuda.Tbl_Maslul.FieldByName('MaslulForMhiron').IsNull) And
           (Dm_WinTuda.Tbl_Maslul.FieldByName('MaslulForMhiron').AsInteger>0) Then
               CodeMaslulForMehiron:=Dm_WinTuda.Tbl_Maslul.FieldByName('MaslulForMhiron').AsInteger;
     End;

     DM_WinTuda.Tbl_Mehiron.Open;
     Try
         //����� ������
         if Frm_GlblWinTuda.FindMehiron(StrToInt(AtmDbHEdit_Lakoach.Text),CodeMehiron,DM_WinTuda.Tbl_ShibSugMetan.AsInteger,
                                        CodeMaslulForMehiron,StrToDate(AtmDBDateEdit_TnuaDate.Text)) Then
         Begin
           if DM_WinTuda.Tbl_Mehiron.FieldByName('SUG_HAVARA_TNUA').AsInteger=1 Then //���� ��� ����
              Frm_GlblWinTuda.MehironBuildRecordForEachPrice2(DM_WinTuda.Tbl_Mehiron.FieldByName('MEHIRON_LINK_KEY').AsInteger,
                                                             DM_WinTuda.Tbl_ShibSugMetan.AsInteger,
                                                             CodeMaslulForMehiron,
                                                             DM_WinTuda.Tbl_Mehiron.FieldbyName('SUG_LINK').AsInteger,
                                                             DM_WinTuda.Tbl_Shib);

           if DM_WinTuda.Tbl_Mehiron.FieldByName('SUG_HAVARA_TNUA').AsInteger=0 Then
           Begin //����� ���
    {          Frm_GlblWinTuda.MehironPrice(DM_WinTuda.Tbl_MehironMEHIRON_LINK_KEY.AsInteger,
                                           DM_WinTuda.Tbl_ShibSugMetan.AsInteger,
                                           DM_WinTuda.Tbl_ShibMaslulCode1.AsInteger,
                                           DM_WinTuda.Tbl_MehironSUG_LINK.AsInteger,
                                           DM_WinTuda.Tbl_Shib,P1,P2,P3,P4);
    }
               GetMehironPrice(DM_WinTuda.Tbl_Mehiron.FieldByName('MEHIRON_LINK_KEY').AsInteger,
                                   DM_WinTuda.Tbl_ShibSugMetan.AsInteger,
                                   CodeMaslulForMehiron,
                                   DM_WinTuda.Tbl_Mehiron.FieldByName('SUG_LINK').AsInteger,
                                   DM_WinTuda.Tbl_Shib,Frm_GlblWinTuda.Qry_Generic,P1,P2,P3,P4);
              if TheFieldName = '' Then
                StrH:=GetDataDataFieldNameFromTControl(ActiveControl)
              Else
                StrH:=TheFieldName;
              if (StrH<>'') Or (ActiveControl is TDBGrid) Then
              Begin
                 if DM_WinTuda.Tbl_Shib.State=dsBrowse Then
                    DM_WinTuda.Tbl_Shib.Edit;

                 EV:=Nil;
                 Try
                   if PriceOpt In [poPrice1,poPrice2,poPrice3,poPrice4,poPrice1PlusPrice2] Then
                   Begin
                     EV:=DM_WinTuda.Tbl_Shib.FieldByName(StrH).OnValidate;
                     DM_WinTuda.Tbl_Shib.FieldByName(StrH).OnValidate:=Nil
                   End;

{                   if PriceOpt in [poPrice1,poPrice2,poPrice3,poPrice4,poPrice1PlusPrice2] Then
                   Begin
                     EV:=DM_WinTuda.Tbl_Shib.FieldByName('StrH').OnValidate;
                     DM_WinTuda.Tbl_Shib.FieldByName('StrH').OnValidate:=Nil;
                   End;}

                   Case PriceOpt of
                      poPrice1: DM_WinTuda.Tbl_Shib.FieldByName(StrH).AsFloat:=P1;
                      poPrice2: DM_WinTuda.Tbl_Shib.FieldByName(StrH).AsFloat:=P2;
                      poPrice3: DM_WinTuda.Tbl_Shib.FieldByName(StrH).AsFloat:=P3;
                      poPrice4: DM_WinTuda.Tbl_Shib.FieldByName(StrH).AsFloat:=P4;
                      poPrice1PlusPrice2:DM_WinTuda.Tbl_Shib.FieldByName(StrH).AsFloat:=P1+P2;
                      poAllPrices :Begin
                                      EV:=DM_WinTuda.Tbl_Shib.FieldByName('Price1').OnValidate;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price1').OnValidate:=Nil;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price1').AsFloat:=P1;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price1').OnValidate:=EV;

                                      EV:=DM_WinTuda.Tbl_Shib.FieldByName('Price2').OnValidate;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price2').OnValidate:=Nil;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price2').AsFloat:=P2;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price2').OnValidate:=EV;

                                      EV:=DM_WinTuda.Tbl_Shib.FieldByName('Price3').OnValidate;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price3').OnValidate:=Nil;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price3').AsFloat:=P3;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price3').OnValidate:=EV;

                                      EV:=DM_WinTuda.Tbl_Shib.FieldByName('Price4').OnValidate;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price4').OnValidate:=Nil;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price4').AsFloat:=P4;
                                      DM_WinTuda.Tbl_Shib.FieldByName('Price4').OnValidate:=EV;
                                   End;
                   End; //Case
{                   if PriceOpt in [poPrice1,poPrice2,poPrice3,poPrice4,poPrice1PlusPrice2] Then
                     DM_WinTuda.Tbl_Shib.FieldByName('StrH').OnValidate:=EV;}
                Finally
                  if PriceOpt In [poPrice1,poPrice2,poPrice3,poPrice4,poPrice1PlusPrice2] Then
                    DM_WinTuda.Tbl_Shib.FieldByName(StrH).OnValidate:=EV;
                  DM_WinTuda.CalcShibTotalPrices;
                End;
              End;
           End; //if DM_WinTuda.Tbl_MehironSUG_HAVARA_TNUA.AsInteger=0
         End;
  {     Else
           ShowMessage('�� ���� ������');}
     Finally
        DM_WinTuda.Tbl_Mehiron.Close;
     End;
  Finally
         CurMehironCode:=0;
         WriteMehironTypeToStatusBar;
         Screen.Cursor:=crDefault;
  End;
End;
procedure TFrmWinTuda.Action_Price1PlusPrice2Execute(Sender: TObject);
begin
     PutPriceInField(poPrice1PlusPrice2,-1);
end;

procedure TFrmWinTuda.Action_Price3Execute(Sender: TObject);
begin
    PutPriceInField(poPrice3,-1);
end;

procedure TFrmWinTuda.Action_Price4Execute(Sender: TObject);
begin
    PutPriceInField(poPrice4,-1);
end;

procedure TFrmWinTuda.DBGrid_TnuaEditButtonClick(Sender: TObject);
begin
     if Sender Is TDBGrid Then
     Begin
         if TDBGrid(Sender){DBGrid_Tnua}.SelectedField=DM_WinTuda.Tbl_ShibSugMetan Then
         Begin
              (Frm_GlblWinTuda.AtmAdvSearch_KodTavla.SourceDataSet As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugMitan;
              if Frm_GlblWinTuda.AtmAdvSearch_KodTavla.Execute Then
              Begin
                   if DM_WinTuda.Tbl_Shib.State=dsBrowse Then
                      DM_WinTuda.Tbl_Shib.Edit;
                   DM_WinTuda.Tbl_ShibSugMetan.AsString:=Frm_GlblWinTuda.AtmAdvSearch_KodTavla.ReturnString;
              End;
         End;
         if (TDBGrid(Sender).SelectedField=DM_WinTuda.Tbl_ShibYeMida1) or
            (TDBGrid(Sender).SelectedField=DM_WinTuda.Tbl_ShibYeMida2) or
            (TDBGrid(Sender).SelectedField=DM_WinTuda.Tbl_ShibYeMida3) or
            (TDBGrid(Sender).SelectedField=DM_WinTuda.Tbl_ShibYeMida4) Then
         Begin
              (Frm_GlblWinTuda.AtmAdvSearch_KodTavla.SourceDataSet As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_YeMida;
              if Frm_GlblWinTuda.AtmAdvSearch_KodTavla.Execute Then
                 TDBGrid(Sender).SelectedField.AsString:=Frm_GlblWinTuda.AtmAdvSearch_KodTavla.ReturnString;
         End;
     End;
end;


procedure TFrmWinTuda.AtmDbHEdit_YeMida1DblClick(Sender: TObject);
begin
//     ShowMessage(IntToLenStr(AtmDbHEdit_YeMida1.Field.AsInteger,5,'0'));
end;

procedure TFrmWinTuda.Btn_PostNewClick(Sender: TObject);
Var
   WasInInsert :Boolean;
begin
     if DBNavigator.DataSource.DataSet.State=dsBrowse Then
        Exit;
     WasInInsert:=DM_WinTuda.Tbl_Shib.State=dsInsert;
     MarkSaveOnTable(DBNavigator.DataSource.DataSet);
     (DBNavigator.DataSource.DataSet).Post;

     if (WasInInsert) Then
         Case ActAfterPostNew Of
             apnNewShib :Begin
                              DM_WinTuda.Tbl_Shib.Append;
                              SetFocusOnActiveGrid;
                         End;
             apnNewAzmn :Dm_WinTuda.Tbl_Azmn.Append;
             apnNone    :;
         End;//Case
end;

Procedure TFrmWinTuda.SetFocusOnActiveGrid;
Begin
    if (PageControl_Tnua.ActivePage=TS_Tnua1) Then
    Begin
         DBGrid_Tnua.SetFocus;
         DBGrid_Tnua.SelectedIndex:=0;
    End;
    if (PageControl_Tnua.ActivePage=TS_Perut2) Then
    Begin
        DBGrid_Preut2.SetFocus;
        DBGrid_Preut2.SelectedIndex:=0;
    End;
End;

procedure TFrmWinTuda.ToolBar_TnuaExit(Sender: TObject);
begin
     Btn_PostNew.Default:=False;
end;

procedure TFrmWinTuda.Action_CrtOpenLakExecute(Sender: TObject);
begin
    OpenCrtDll(fnLakoach,AtmDbHEdit_Lak.Text);
end;

procedure TFrmWinTuda.Action_CrtOpenNehagExecute(Sender: TObject);
begin
    OpenCrtDll(fnNehag,AtmDbHEdit_Nehag1.Text);
end;

procedure TFrmWinTuda.Action_CrtOpenRehevExecute(Sender: TObject);
begin
    OpenCrtDll(fnRehev,AtmDbHEdit_Rehev.Text);
end;

procedure TFrmWinTuda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     FrmWinTuda:=Nil;
     Action:=caFree;
end;

procedure TFrmWinTuda.Action_ChooseMehironExecute(Sender: TObject);
begin
     if Frm_GlblWinTuda.AtmAdvSearch_Mehiron.Execute Then
     Begin
        CurMehironCode:=StrToInt(Frm_GlblWinTuda.AtmAdvSearch_Mehiron.ReturnString);
        StatusBar1.Panels[PnlNum_TotalTuda].Text:='������ '+Frm_GlblWinTuda.AtmAdvSearch_Mehiron.ReturnString;
     End;
//        ShowMessage(Frm_GlblWinTuda.AtmAdvSearch_Mehiron.ReturnString);
end;

procedure TFrmWinTuda.Action_CrtOpenMehironExecute(Sender: TObject);
begin
    OpenCrtDll(fnMehiron,'');
end;

procedure TFrmWinTuda.MaskEdit_FromTimeExit(Sender: TObject);
begin
  Try
     if StrToDate(DateToStr(DM_WinTuda.Tbl_AzmnFromDate.AsDateTime))+StrToTime(MaskEdit_FromTime.Text)<>DM_WinTuda.Tbl_AzmnFromDate.AsDateTime Then
     Begin
         if DM_WinTuda.Tbl_Azmn.State=dsBrowse Then
            DM_WinTuda.Tbl_Azmn.Edit;
            DM_WinTuda.Tbl_AzmnFromDate.AsDateTime:=StrToDate(DateToStr(DM_WinTuda.Tbl_AzmnFromDate.AsDateTime))+StrToTime(MaskEdit_FromTime.Text);
     End;
   Except
   End;
end;

procedure TFrmWinTuda.MaskEdit_ToTimeExit(Sender: TObject);
begin
   Try
     if StrToDate(DateToStr(DM_WinTuda.Tbl_AzmnToDate.AsDateTime))+StrToTime(MaskEdit_ToTime.Text)<>DM_WinTuda.Tbl_AzmnToDate.AsDateTime Then
     Begin
         if DM_WinTuda.Tbl_Azmn.State=dsBrowse Then
            DM_WinTuda.Tbl_Azmn.Edit;
         DM_WinTuda.Tbl_AzmnToDate.AsDateTime:=StrToDate(DateToStr(DM_WinTuda.Tbl_AzmnToDate.AsDateTime))+StrToTime(MaskEdit_ToTime.Text);
     End;
   Except
   End;
end;

procedure TFrmWinTuda.Action_DupRecordExecute(Sender: TObject);
begin
     DuplicateOneRecord(DBNavigator.DataSource.DataSet,False,True);
end;

procedure TFrmWinTuda.AtmDBDateEdit_TnuaDateExit(Sender: TObject);
begin
     if (AtmDBDateEdit_TnuaDate.Field.IsNull) And (AtmDBDateEdit_TnuaDate.DataSource.DataSet.State<>dsBrowse) Then
        AtmDBDateEdit_TnuaDate.Field.AsDateTime:=DM_WinTuda.Tbl_AzmnFromDate.AsDateTime;
end;

procedure TFrmWinTuda.NEditRxSpeedBarClick(Sender: TObject);
begin
     SpeedBar1.Customize(0);
end;

procedure TFrmWinTuda.AtmDbHEdit_LakoachEnter(Sender: TObject);
begin
     CopyValuesFromAzmn;
end;

Procedure TFrmWinTuda.CopyValuesFromAzmn;
Begin
     With Dm_WinTuda Do
     Begin
         if (Tbl_Shib.State<>dsBrowse) Then
         Begin
            if (Tbl_ShibLakNo1.IsNull) Then
               Tbl_ShibLakNo1.AsInteger:=Tbl_AzmnInvLakNo.AsInteger;
            if Tbl_ShibShibBeginTime.IsNull Then
               Tbl_ShibShibBeginTime.AsDateTime:=Tbl_AzmnFromDate.AsDateTime;
            if Tbl_ShibShibEndTime.IsNull Then
               Tbl_ShibShibEndTime.AsDateTime:=Tbl_AzmnToDate.AsDateTime;
         End;
     End;//With
End;

procedure TFrmWinTuda.Pnl_MidEnter(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  Try
     if (DM_WinTuda.Tbl_Azmn.State=dsInsert)Then
     Begin
//        DM_WinTuda.Tbl_Azmn.SavePressed:=True;
//        DM_WinTuda.Tbl_Azmn.Post;
        if (DM_WinTuda.Tbl_Shib.State<>dsInsert) Then
            DM_WinTuda.Tbl_Shib.Append;
     End;
     With Dm_WinTuda Do
     Begin
          if Tbl_ShibShibNo.IsNull Then
          Begin
               if DM_WinTuda.Tbl_Shib.State=dsBrowse Then
                  DM_WinTuda.Tbl_Shib.Edit;
               PutDefaultsOnShib;
          End;
     End;
     SetAtmDbEditF4State(Pnl_Mid);
  Finally
    Screen.Cursor:=crDefault;
  End;
end;

procedure TFrmWinTuda.FormShow(Sender: TObject);
begin
  AtmDbHEdit_Lakoach.TabStop:=Not WinTudaLakMastBeEqual;
  Try
    SpeedBar1.Width:=SpeedBar1.Parent.Width-SpeedBar1.Left-5;
//    DM_WinTuda.Tbl_Azmn.Last;
//    ShowMessage('���� ���� ����� '+IntToStr(WinTudaYehusMonth)+'/'+IntToStr(WinTudaYehusYear));
    Action_ChooseYehusExecute(Action_ChooseYehus);
  //        Ads.Database.TransIsolation:=tiDirtyRead;
    Case WinTudaFirstTableState Of
      tsInsert:DM_WinTuda.Tbl_Azmn.Append;
      tsFirst :DM_WinTuda.Tbl_Azmn.First;
      tsLast  :DM_WinTuda.Tbl_Azmn.Last;
    End; //Case
  Except
  //Ignor
  End;
end;

procedure TFrmWinTuda.Action_CrtOpenKodTavlaExecute(Sender: TObject);
begin
     OpenCrtDll(fnTavla,'');
end;

procedure TFrmWinTuda.Action_CrtOpenMaslulExecute(Sender: TObject);
begin
     OpenCrtDll(fnMaslul,'');
end;

procedure TFrmWinTuda.Label6DblClick(Sender: TObject);
begin
     OpenCrtDll(fnLakoach,AtmDbHEdit_Lakoach.Text);
end;

procedure TFrmWinTuda.Action_SelectExtraFileExecute(Sender: TObject);
Var
   OpenDialog:TOpenDialog;
begin
     OpenDialog:=TOpenDialog.Create(Self);
     Try
         OpenDialog.InitialDir:=ExtractFileDir(DynamicControls1.IniFileName);
         OpenDialog.FileName:=ExtractFileName(DynamicControls1.IniFileName);
         if OpenDialog.Execute Then
         Begin
             DynamicControls1.IniFileName:=OpenDialog.FileName;
             ShowMessage('������� ����� ����� ������ ���� ���� ��');
             //DynamicControls1.Execute;
         End
         Else
         Begin
             if MessageDlg('��� ���� �� ������ ����',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
                DynamicControls1.IniFileName:='';
         End;
     Finally
            OpenDialog.Free;
     End;
end;

procedure TFrmWinTuda.RxQuery_TabSheetBuildAfterScroll(DataSet: TDataSet);
begin
//     RxQuery_TabSheetBuild.Edit;

end;

procedure TFrmWinTuda.NCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmWinTuda.AtmDbHEdit_HovalaKindBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugHovala;
end;

procedure TFrmWinTuda.Action_AllPricesExecute(Sender: TObject);
begin
     PutPriceInField(poAllPrices,-1);
end;

procedure TFrmWinTuda.AtmDbHEdit_Price1Enter(Sender: TObject);
begin
     With DM_WinTuda Do
     Begin
          if (Tbl_Shib.State=dsInsert) And ((Tbl_ShibPrice1.IsNull) Or (Tbl_ShibPrice1.AsInteger=0)) Then
             PutPriceInField(poAllPrices,-1);
     End;
end;

procedure TFrmWinTuda.MaskEdit_FromTimeChange(Sender: TObject);
begin
     if Trim(TMaskEdit(Sender).Text)=':' Then
        TMaskEdit(Sender).Text:='00:00';
end;

Procedure TFrmWinTuda.BuildLookupList;
Var
   SList:TStringList;
Begin
  SList:=TStringList.Create;
  Try
    SList.AddObject(IntToStr(SugTavla_SugHovala),AtmDbComboBox_SugHovala);
    FillKodTavlaComboList(DM_WinTuda.Tbl_KodTavla,SList);
  Finally
        SList.Free;
  End;
End;

procedure TFrmWinTuda.Action_SubtractMamExecute(Sender: TObject);
Var
   RH :Real;
   StrH :String;
begin
  if (ActiveControl is TDBEdit) And (TDBEdit(ActiveControl).Text<>'') Then
  Begin
    if TDBEdit(ActiveControl).DataSource.DataSet.State=dsBrowse Then
       TDBEdit(ActiveControl).DataSource.DataSet.Edit;
    RH:=StrToFloat(TDBEdit(ActiveControl).Text);
    RH:=RH/(1+WinTudaMAM/100);
    TDBEdit(ActiveControl).Text:=FloatToStr(RH);
  End;

  if (ActiveControl = DBGrid_Preut2) And (
                                           (CompareText(DBGrid_Preut2.SelectedField.FieldName,'Price1')=0) Or
                                           (CompareText(DBGrid_Preut2.SelectedField.FieldName,'Price2')=0) Or
                                           (CompareText(DBGrid_Preut2.SelectedField.FieldName,'Price3')=0) Or
                                           (CompareText(DBGrid_Preut2.SelectedField.FieldName,'Price4')=0)
                                         )
                                            Then
  Begin
    if (DBGrid_Preut2.InplaceEditor<>Nil) And (DBGrid_Preut2.InplaceEditor.Text<>'') Then
      StrH:=DBGrid_Preut2.InplaceEditor.Text
    Else
      StrH:=DBGrid_Preut2.SelectedField.AsString;

    if DBGrid_Preut2.DataSource.DataSet.State=dsBrowse Then
       DBGrid_Preut2.DataSource.DataSet.Edit;
    RH:=StrToFloat(StrH);
    RH:=RH/(1+WinTudaMAM/100);
    DBGrid_Preut2.SelectedField.AsString:=FloatToStr(RH);
  End;
end;

procedure TFrmWinTuda.Action_SubtractMamUpdate(Sender: TObject);
begin
  if DM_WinTuda=Nil Then Exit;
//  With DM_WinTuda Do
    (Sender As TAction).Enabled:=
                      (
                        (ActiveControl = AtmDbHEdit_Price1) Or
                        (ActiveControl = AtmDbHEdit_Price2) Or
                        (ActiveControl = AtmDbHEdit_Price3) Or
                        (ActiveControl = AtmDbHEdit_Price4) Or
                        (
                          (ActiveControl Is TDbGrid) And
                          (
                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice1) Or
                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice2) Or
                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice3) Or
                          (TDbGrid(ActiveControl).SelectedField=DM_WinTuda.Tbl_ShibPrice4)
                          )
                        )
                      );
end;

procedure TFrmWinTuda.AtmDBDateEdit_AzmnToDateKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key=VK_F5 Then
     Begin
          if AtmDBDateEdit_AzmnToDate.DataSource.DataSet.State=dsBrowse Then
             AtmDBDateEdit_AzmnToDate.DataSource.DataSet.Edit;
          AtmDBDateEdit_AzmnToDate.Date:=AtmDBDateEdit_AzmnFromDate.Date;
     End;
end;

procedure TFrmWinTuda.DBGrid_TnuaGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     if {(Field<>TDbGrid(Sender).SelectedField) And }(DM_WinTuda.Tbl_Shib.FieldByName('PriceIsGlobal').AsInteger>0{=1}) Then
        Background:=cl3DLight;

end;

procedure TFrmWinTuda.AtmTabSheetBuild1GridDblClick(Sender: TObject;
  ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
begin
   if CompareText(TAtmTabSheetBuild(Sender).CurId,'ShibTab')=0 Then
      With DM_WinTuda Do
      Begin
           Tbl_Azmn.FindKey([ARxDbGrid.DataSource.DataSet.FieldByName('ShibAzmnNo').AsInteger]);
           PageControl_Main.ActivePage:=Ts_Tnua;
      End;
end;

procedure TFrmWinTuda.Action_SearchCodesFromMehironExecute(
  Sender: TObject);
begin
     if ActiveControl is TDBGrid Then
     Begin
        if CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'MaslulCode1')=0 Then
           if Frm_GlblWinTuda.AtmAdvSearch_MaslulMehiron.Execute Then
           Begin
               if (ActiveControl As TDbGrid).DataSource.DataSet.State=dsBrowse Then
                  (ActiveControl As TDbGrid).DataSource.DataSet.Edit;
              (ActiveControl As TDbGrid).SelectedField.AsString:=Frm_GlblWinTuda.AtmAdvSearch_MaslulMehiron.ReturnString;
           End;
        if CompareText((ActiveControl As TDbGrid).SelectedField.FieldName,'SugMetan')=0 Then
           if Frm_GlblWinTuda.AtmAdvSearch_SugTavlaMehiron.Execute Then
           Begin
               if (ActiveControl As TDbGrid).DataSource.DataSet.State=dsBrowse Then
                  (ActiveControl As TDbGrid).DataSource.DataSet.Edit;
              (ActiveControl As TDbGrid).SelectedField.AsString:=Frm_GlblWinTuda.AtmAdvSearch_SugTavlaMehiron.ReturnString;
           End;
     End;
end;

procedure TFrmWinTuda.Action_ChangeParamsExecute(Sender: TObject);
begin
     With TFrm_WinTudaParams.Create(Self) Do
          ShowModal;
     // Free is done in the form onclose
end;

procedure TFrmWinTuda.Action_NewTnuaExecute(Sender: TObject);
begin
     With DM_WinTuda Do
     Begin
          Tbl_Azmn.SavePressed:=True;
          Tbl_Shib.SavePressed:=True;
          Tbl_Shib.Append;
          SetFocusOnActiveGrid;
     End;
end;

procedure TFrmWinTuda.DBGrid_Preut2ColEnter(Sender: TObject);
begin
    With DM_WinTuda Do
    if DBGrid_Preut2.SelectedField=Tbl_ShibPrice1 Then
    Begin
         if (Tbl_Shib.State=dsInsert) And (Tbl_ShibPrice1.IsNull) Then
            PutPriceInField(poAllPrices,-1);
    End;

    Action_OpenSearchUpdate(Action_OpenSearch);
    Action_LastPriceListActionUpdate(Action_LastPriceListAction);
    ChangeDSForNavigator(DBNavigator);
end;

procedure TFrmWinTuda.Action_ChoosePointsFromMslExecute(Sender: TObject);
Var
   TheNodeList :TNodeList;
   I :LongInt;
   TotalDist:Real;
begin
     TheNodeList:=TNodeList.Create;
     TotalDist:=0;
     Try
        if RunDllOpenMslPointForm('AtmCrt.dll','OpenMslPointForm',TheNodeList,False) Then
        Begin
             if Dm_WinTuda.Tbl_Shib.State=dsBrowse Then
               Dm_WinTuda.Tbl_Shib.Edit;
             Dm_WinTuda.Tbl_ShibShibRem1.Clear;
             For I:=0 To TheNodeList.Count-1 Do
                 with TNodeClass(TheNodeList.Items[i]) do
                 Begin
                      TotalDist:=TotalDist+LDist;
                      Dm_WinTuda.Tbl_ShibShibRem1.AsString:=Dm_WinTuda.Tbl_ShibShibRem1.AsString+Trim(LNode.NameOfPoint);
                      if I < TheNodeList.Count-1 Then
                         Dm_WinTuda.Tbl_ShibShibRem1.AsString:=Dm_WinTuda.Tbl_ShibShibRem1.AsString+', ';
                 End;
             Dm_WinTuda.Tbl_ShibShibTotalKm.AsFloat:=(TotalDist /1000);
             if TheNodeList.Count>=2 Then
             Begin
                With Dm_WinTuda,Frm_GlblWinTuda Do
                Begin
                     Tbl_ShibMaslul1.AsString:=Trim(TNodeClass(TheNodeList.Items[0]).LNode.NameOfPoint)+MslSeparator+Trim(TNodeClass(TheNodeList.Items[TheNodeList.Count-1]).LNode.NameOfPoint);

                     //����� ��� �����
                     Qry_Generic.Active:=False;
                     Qry_Generic.Sql.Clear;
                     Qry_Generic.Sql.Add('Select Code_Maslul From Maslul Where');
                     Qry_Generic.Sql.Add('Motza='''+Trim(TNodeClass(TheNodeList.Items[0]).LNode.NameOfPoint)+''' And');
                     Qry_Generic.Sql.Add('YAAD='''+Trim(TNodeClass(TheNodeList.Items[TheNodeList.Count-1]).LNode.NameOfPoint)+'''');
                     Qry_Generic.Active:=True;
                     if Qry_Generic.RecordCount>0 Then
                     Begin
                          Qry_Generic.First;
                          Tbl_ShibMaslulCode1.AsInteger:=Qry_Generic.FieldByName('Code_Maslul').AsInteger;
                     End;
                     Qry_Generic.Active:=False;
                End; //With

             End;

        End;
     Finally
            TheNodeList.Free;
     End;

end;

Procedure TFrmWinTuda.ShowHelpForField(TheField :TField);
Var
   StrH :String;
Begin
    StrH:='';
    With DM_WinTuda Do
    Begin
         if TheField=Tbl_ShibSugMetan Then
            StrH:='F2 - ���� ���� | Shift+F2 - ���� ���� ������� ����';
         if TheField=Tbl_ShibMaslulCode1 Then
            StrH:='F2 - ���� ���� | Shift+F2 - ���� ���� ������� ����';
         if TheField=Tbl_ShibMaslul1 Then
            StrH:='F2 - ����� ���� ���� ��������';
    End;
    StatusBar1.Panels[PnlNum_Help].Text:=StrH;
End;
procedure TFrmWinTuda.DBGrid_TnuaColExit(Sender: TObject);
begin
     StatusBar1.Panels[PnlNum_Help].Text:='';
end;

procedure TFrmWinTuda.DBGrid_TnuaExit(Sender: TObject);
begin
     StatusBar1.Panels[PnlNum_Help].Text:='';
end;

procedure TFrmWinTuda.Action_MeholelReportExecute(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,WinTudaCurUserName,CurAliasName,False,WinTudaMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msTnua],False,WinTudaMeholelHandle);
end;

procedure TFrmWinTuda.Panel_TopExit(Sender: TObject);
begin
     if Not DM_WinTuda.Tbl_Azmn.ValidateLookupFields Then
        AtmDbHEdit_Hazmana.SetFocus;
end;

procedure TFrmWinTuda.AtmDBDateEdit_AzmnFromDateExit(Sender: TObject);
begin
     if (AtmDBDateEdit_AzmnFromDate.DataSource.DataSet.State=dsInsert) And
        (AtmDBDateEdit_AzmnFromDate.Field.IsNull) Then
         AtmDBDateEdit_AzmnFromDate.Field.AsDateTime:=Now;
     MaskEdit_FromTimeExit(AtmDBDateEdit_AzmnFromDate);
end;

procedure TFrmWinTuda.Action_MeholelMainExecute(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,WinTudaCurUserName,CurAliasName,False,WinTudaMeholelHandle);
     RunDllNoParam('Meholel.Dll','ShowMain',False,WinTudaMeholelHandle);
end;

procedure TFrmWinTuda.AtmDbHEdit_LakoachChange(Sender: TObject);
begin
     CurMehironCode:=0;
     WriteMehironTypeToStatusBar;
end;

procedure TFrmWinTuda.Action_ChooseYehusExecute(Sender: TObject);
begin
     With TFrm_ChooseYehus.Create(Nil) Do
     Begin
       Try
         MaskEdit_YehusMonth.Text:=IntToStr(WinTudaYehusMonth);
         MaskEdit_YehusYear.Text:=IntToStr(WinTudaYehusYear);
         if ShowModal=mrOk Then
         Begin
              WinTudaYehusMonth:=StrToInt(Trim(MaskEdit_YehusMonth.Text));
              WinTudaYehusYear:=StrTOInt(MaskEdit_YehusYear.Text);
              WriteYehusStatusBar;
         End;
       Finally
          Free;
       End;
     End;

end;

Procedure TFrmWinTuda.WriteYehusStatusBar;
Begin
     StatusBar1.Panels[PnlNum_Yehus].Text:=IntToStr(WinTudaYehusMonth)+'/'+IntToStr(WinTudaYehusYear);
End;
procedure TFrmWinTuda.Action_ShibKindHiuvNehagExecute(Sender: TObject);
begin
     MessageDlg('����� �����',mtInformation,[mbOk],0);
     WinTudaShibKind:=skHiuvNehag;
     Caption:='����� �����';
     Action_ShibKindNormal.Checked:=False;
     Action_ShibKindKikuyLak.Checked:=False;
//     Action_ShibKindNoMam.Checked:=False;
     Action_ShibKindHiuvNehag.Checked:=True;
     WriteMehironTypeToStatusBar;

     Panel_ShibKind.Caption:='����� �����';
     Panel_ShibKind.Color:=clLime;
     Panel_ShibKind.Visible:=True;
     AtmDbComboBox_azmnType.Enabled:=False;
     DM_WinTuda.Tbl_Azmn.Close;
     DM_WinTuda.Tbl_Azmn.MacroByName('AzmnTypeFilter').AsString:='AzmnType = '+IntToStr(atHiuvNehag);
     DM_WinTuda.Tbl_Azmn.Open;
end;

procedure TFrmWinTuda.Action_ShibKindNormalExecute(Sender: TObject);
begin
     MessageDlg('����� ������',mtInformation,[mbOk],0);
     WinTudaShibKind:=skNormal;
     Caption:='������ ����';
     Action_ShibKindHiuvNehag.Checked:=False;
     Action_ShibKindKikuyLak.Checked:=False;
//     Action_ShibKindNoMam.Checked:=False;
     Action_ShibKindNormal.Checked:=True;
     WriteMehironTypeToStatusBar;

     Panel_ShibKind.Visible:=False;
     AtmDbComboBox_azmnType.Enabled:=True;
     DM_WinTuda.Tbl_Azmn.Close;
     DM_WinTuda.Tbl_Azmn.MacroByName('AzmnTypeFilter').AsString:='AzmnType < '+IntToStr(atHiuvNehag);
     DM_WinTuda.Tbl_Azmn.Open;
end;

Procedure TFrmWinTuda.WriteMehironTypeToStatusBar;
Begin
    Case WinTudaShibKind of
         skNormal   :Begin
                         StatusBar1.Panels[PnlNum_TotalTuda].Text:='������ ����';
                     End;
         skHiuvNehag:Begin
                         StatusBar1.Panels[PnlNum_TotalTuda].Text:='������ ���';
                     End;
         skZikuyLak :Begin
                         StatusBar1.Panels[PnlNum_TotalTuda].Text:='������ ����';
                     End;
    End;
End;
procedure TFrmWinTuda.AtmDbHEdit_Nehag1Change(Sender: TObject);
begin
     CurMehironCode:=0;
     WriteMehironTypeToStatusBar;
end;

procedure TFrmWinTuda.Action_CrtOpenMahzevaExecute(Sender: TObject);
begin
    OpenCrtDll(fnMahzeva,'');
end;

procedure TFrmWinTuda.Action_CopyTnuotExecute(Sender: TObject);
begin
  if Frm_CopyTnua=Nil Then
    Frm_CopyTnua:=TFrm_CopyTnua.Create(Nil);
  Frm_CopyTnua.Show;
end;

procedure TFrmWinTuda.Act_Refresh_LakoExecute(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  DM_WinTuda.Tbl_Lako.Active:=False;
  DM_WinTuda.Tbl_Lako.Active:=True;
  Screen.Cursor:=crDefault;
end;

procedure TFrmWinTuda.Act_Refresh_NehagExecute(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  DM_WinTuda.Tbl_Nehag.Active:=False;
  DM_WinTuda.Tbl_Nehag.Active:=True;
  Screen.Cursor:=crDefault;
end;

procedure TFrmWinTuda.Act_Refresh_RehevExecute(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  DM_WinTuda.Tbl_Rehev.Active:=False;
  DM_WinTuda.Tbl_Rehev.Active:=True;
  Screen.Cursor:=crDefault;
end;

procedure TFrmWinTuda.Act_Refresh_MaslulExecute(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  DM_WinTuda.Tbl_Maslul.Active:=False;
  DM_WinTuda.Tbl_Maslul.Active:=True;
  Screen.Cursor:=crDefault;

end;

procedure TFrmWinTuda.Act_Refresh_KodTavlaExecute(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  DM_WinTuda.Tbl_KodTavla.Active:=False;
  DM_WinTuda.Tbl_KodTavla.Active:=True;
  Screen.Cursor:=crDefault;
end;

procedure TFrmWinTuda.Act_Maintaince_CloseMeholelExecute(Sender: TObject);
begin
  if WinTudaMeholelHandle > 0 Then
    RunDllNoParam('Meholel.Dll','CloseMeholel',True,WinTudaMeholelHandle);
end;


procedure TFrmWinTuda.Act_EditScreenControlsExecute(Sender: TObject);
begin
  if MessageDlg('����� ���� ������ ����� �� ���� ������'+
                #13'�� ����� ������� ����� ��� ����� ���� ����� ����� ����� ��'+
                #13'���� ������ �� ���� ������� ���� ���� �� - ���� �� ���� ������ ������'+
                #13'��� ������ ������',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
  Begin
    With TSaveDialog.Create(Nil) Do
    Begin
      Try
        Title:='��� ���� ������';
        InitialDir:=ExtractFileDir(DynamicControls1.IniFileName);
        FileName:=ExtractFileName(DynamicControls1.IniFileName);
        if Execute Then
        Begin
          DynamicControls1.IniFileName:=FileName;
          DynamicControls1.ResizeAll;
        End;
      Finally
        Free;
      End;
    End;
  End;
end;

procedure TFrmWinTuda.Act_EditExtraFieldsExecute(Sender: TObject);
Var
   OpenDialog:TSaveDialog;
   TempDynamicControls :TDynamicControls;
   I :LongInt;
begin
     if Screen.ActiveForm=Nil Then Exit;
     TempDynamicControls:=Nil;
     For I:=0 To Screen.ActiveForm.ComponentCount-1 Do
         if Screen.ActiveForm.Components[i] is TDynamicControls Then
         Begin
              TempDynamicControls:=TDynamicControls(Screen.ActiveForm.Components[i]);
              Break;
         End;
     if TempDynamicControls=Nil Then Exit;

     Begin
       OpenDialog:=TSaveDialog.Create(Self);
       Try
           OpenDialog.InitialDir:=ExtractFileDir(TempDynamicControls.IniFileName);
           OpenDialog.FileName:=ExtractFileName(TempDynamicControls.IniFileName);
           if OpenDialog.Execute Then
           Begin
               TempDynamicControls.IniFileName:=OpenDialog.FileName;
           End
           Else
            TempDynamicControls.IniFileName:='';
       Finally
              OpenDialog.Free;
       End;
     End;
     if TempDynamicControls.IniFileName<>'' Then
     Begin
        if TempDynamicControls.ExtraFieldsParent is TTabSheet Then
          TTabSheet(TempDynamicControls.ExtraFieldsParent).PageControl.ActivePage:=TTabSheet(TempDynamicControls.ExtraFieldsParent)
        Else
          TempDynamicControls.ExtraFieldsParent.BringToFront;
        TempDynamicControls.EditExtraFields;
     End;
end;

procedure TFrmWinTuda.DBGrid_TnuaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (gdFocused in State) And (DBGrid_Tnua.Canvas.Brush.Color<>DBGrid_Tnua.Color) Then
  Begin
    DBGrid_Tnua.Canvas.Brush.Color:=clNavy;
    DBGrid_Tnua.Canvas.FillRect(Rect);
    DBGrid_Tnua.Canvas.Font.Color:=clWhite;
    DBGrid_Tnua.Canvas.TextRect(Rect,Rect.Left+1,Rect.Top+1,Column.Field.AsString);
  End;
end;

procedure TFrmWinTuda.Action_ChangeTabExecute(Sender: TObject);
begin
  if (PageControl_Main.PageCount>1) Then
    if PageControl_Main.ActivePageIndex=0 Then
      PageControl_Main.ActivePageIndex:=1
    Else
      if (PageControl_Main.ActivePageIndex=1) And (CompareText(AtmTabSheetBuild1.CurId,'ShibTab')=0) Then
        AtmTabSheetBuild1GridDblClick(AtmTabSheetBuild1,PageControl_Main.ActivePage,AtmTabSheetBuild1.CurRxDbGrid);
end;

procedure TFrmWinTuda.Act_Maintaince_UPdatePricesInTnuaFromMehironExecute(
  Sender: TObject);
begin
  Frm_UpdatePriceFromMehiron:=TFrm_UpdatePriceFromMehiron.Create(Nil);
  Frm_UpdatePriceFromMehiron.ShowModal;
  Frm_UpdatePriceFromMehiron.Free;
  Frm_UpdatePriceFromMehiron:=Nil;
end;

procedure TFrmWinTuda.Action_Price2SubHanForLakFromPrice1Execute(Sender: TObject);
Var
  StrH :String;
  PercentHan :Real;
  R1:Real;
begin
  if DM_WinTuda.Tbl_Lako.FindKey([DM_WinTuda.Tbl_Shib.FieldByName('LakNo1').AsInteger]) Then
  Begin
    PercentHan:=DM_WinTuda.Tbl_Lako.FieldByName('Achuz_Anacha_Amala').AsFloat;
    R1:=DM_WinTuda.Tbl_Shib.FieldByName('Price1').AsFloat;
    R1:=R1-(R1*PercentHan/100); //���� � - ���� ����

    if DM_WinTuda.Tbl_Nehag.FindKey([DM_WinTuda.Tbl_Shib.FieldByName('DriverNo1').AsInteger]) Then
    Begin
      if DM_WinTuda.Tbl_Nehag.FieldByName('Sug_Anacha_Amala').AsInteger=1 Then
      Begin
        PercentHan:=DM_WinTuda.Tbl_Nehag.FieldByName('Achuz_Anacha_Amala').AsFloat;
        R1:=R1-(R1*PercentHan/100);//���� � - ���� ���� - ���� ���
      End;
    End;

    if DM_WinTuda.Tbl_Shib.State=dsBrowse Then
      DM_WinTuda.Tbl_Shib.Edit;
    DM_WinTuda.Tbl_Shib.FieldByName('Price2').AsFloat:=R1;
  End;
End;

procedure TFrmWinTuda.Action_ShibKindKikuyLakExecute(Sender: TObject);
begin
  MessageDlg('������� �������',mtInformation,[mbOk],0);
  WinTudaShibKind:=skZikuyLak;
  Caption:='������� �������';
  Action_ShibKindNormal.Checked:=False;
  Action_ShibKindHiuvNehag.Checked:=False;
//  Action_ShibKindNoMam.Checked:=False;
  Action_ShibKindKikuyLak.Checked:=True;
  WriteMehironTypeToStatusBar;

  Panel_ShibKind.Caption:='������� �������';
  AtmDbComboBox_azmnType.Enabled:=False;
  Panel_ShibKind.Color:=clRed;
  Panel_ShibKind.Visible:=True;
  DM_WinTuda.Tbl_Azmn.Close;
  DM_WinTuda.Tbl_Azmn.MacroByName('AzmnTypeFilter').AsString:='AzmnType = '+IntToStr(atZikuyLak);
  DM_WinTuda.Tbl_Azmn.Open;
end;

procedure TFrmWinTuda.Act_HsbZikuyLakPerutExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(ZikuyShekel)),DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger,True,H);
end;

procedure TFrmWinTuda.Act_HsbZikuyLakMakeExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll2IntParam('WinHshbn.Dll','MakeHshbonit',Ord(TSugHesbonit(ZikuyShekel)),DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger,True,H);
end;

procedure TFrmWinTuda.Act_HsbZikuyLakCopyExecute(Sender: TObject);
Var
   H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','CopyHshbonit',Ord(TSugHesbonit(ZikuyShekel)),DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger,True,H);
end;

procedure TFrmWinTuda.Act_HsbZikuyLakCancelExecute(Sender: TObject);
Var
   H :THandle;
begin
     H:=0;
     RunDll1IntParam('WinHshbn.Dll','BitulHshbonit',Ord(TSugHesbonit(ZikuyShekel)),True,H);
end;

procedure TFrmWinTuda.Act_HsbZikuyLakPerutUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled:=DM_WinTuda.Tbl_Shib.FieldByName('ShibKind').AsInteger=skZikuyLak;
end;

procedure TFrmWinTuda.Action_PriceKindNoMamExecute(Sender: TObject);
begin
  Action_PriceKindNoMam.Checked:=Not Action_PriceKindNoMam.Checked;
  if Action_PriceKindNoMam.Checked Then
  Begin
    MessageDlg('������ ��� ��"�',mtInformation,[mbOk],0);
    Caption:='������ ��� ��"�';
    WinTudaPriceKind:=pkLeloMam;
  End
  Else
  Begin
    MessageDlg('������ ������',mtInformation,[mbOk],0);
    Caption:='������ ������';
    WinTudaPriceKind:=pkShekel;
  End;
  Panel_ShibKind.Caption:=Caption;
  Panel_ShibKind.Visible:=Action_PriceKindNoMam.Checked;
end;

procedure TFrmWinTuda.DBGrid_TnuaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var
  IntH :LongInt;
begin
  if Key=VK_F9 Then
  Begin
    if TDbGrid(Sender).SelectedField.FieldKind=fkData Then
    With DM_WinTuda.RxQuery_Temp DO
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Max(SHIBNO) SN From '+WinTudaAtmShibFileName+' Where SHIBAZMNNO >='+IntToStr(DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger-1));
      Sql.Add('And SHIBAZMNNO < '+IntToStr(RangeAzmnTo));
      Open;
      IntH:=FieldByName('SN').AsInteger;
      Close;
      if IntH<>0 Then
      Begin
        Sql.Clear;
        Sql.Add('Select '+TDbGrid(Sender).SelectedField.FieldName);
        Sql.Add('From '+WinTudaAtmShibFileName+' Where SHIBNO='+IntToStr(IntH));
        Open;
        if Not Eof And BOF Then
        Begin
          TDbGrid(Sender).SelectedField.AsString:=FieldByName(TDbGrid(Sender).SelectedField.FieldName).AsString;
          TDbGrid(Sender).SelectedIndex:=TDbGrid(Sender).SelectedIndex+1;
        End;
        Close;
      End;
    End;
  End;
end;

procedure TFrmWinTuda.AtmDbHEdit_HazmanaBeforeAutoLocateRecord(
  Sender: TObject; var ContinueOperation: Boolean);
Var
  StrH :String;
begin
{  if DM_WinTuda.Tbl_Azmn.GetOnlyOneRecord Then
  Begin
    ContinueOperation:=False;
    With DM_WinTuda.Tbl_Azmn Do
    Begin
      StrH:=TAtmDbHEdit(Sender).Text;
      Cancel;
      Close;
      MacroByName(MacroForWhere).AsString:='AzmnNo = '+StrH;
      Open;
    End;
  End;}
end;

procedure TFrmWinTuda.DBNavigatorBeforeAction(Sender: TObject;
  Button: TmoNavBtns);
begin
  if Not Visible Then Exit;
  ChangeDSForNavigator(TAtmDBNavigator(Sender));

  Case Button of
  nbaPost,nbaInsert:MarkSaveOnTable((Sender As TAtmDBNavigator).DataSource.DataSet);
  nbaRefresh:
             Begin
                DM_WinTuda.RefreshMainQuerys;
                BuildLookupList;
                Abort;
             End;
  End; //Case;

{  if (TDbNavigator(Sender).DataSource.DataSet is TAtmRxQuery) And
    (TAtmRxQuery(TDbNavigator(Sender).DataSource.DataSet)).GetOnlyOneRecord Then
  Begin
  Case Button Of
    nbNext :Begin
              TAtmRxQuery(TDbNavigator(Sender).DataSource.DataSet).Next;
              Abort;
            End;
    nbLast :Begin
              TAtmRxQuery(TDbNavigator(Sender).DataSource.DataSet).Last;
              Abort;
            End;
    nbPrior :Begin
              TAtmRxQuery(TDbNavigator(Sender).DataSource.DataSet).Prior;
              Abort;
            End;
    nbFirst :Begin
              TAtmRxQuery(TDbNavigator(Sender).DataSource.DataSet).First;
              Abort;
            End;
  End; //Case
  End;
}
end;

procedure TFrmWinTuda.Act_HsbPriceProposalExecute(Sender: TObject);
Var
  H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(AtzatMechir)),DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger,True,H);
end;

procedure TFrmWinTuda.Act_HsbPrintHazmanaExecute(Sender: TObject);
Var
  H :THandle;
begin
  H:=0;
  RunDll2IntParam('WinHshbn.Dll','Perot_To_Lak',Ord(TSugHesbonit(Azmana)),DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger,True,H);
end;

procedure TFrmWinTuda.Action_AtmAzmnHatzExecute(Sender: TObject);
begin
  TAction(Sender).Checked:= Not TAction(Sender).Checked;
  if TAction(Sender).Checked Then //����� ����
  Begin
    WinTudaAtmAzmnFileName:='ATMAZMNHATZ';
    WinTudaAtmShibFileName:='ATMSHIBHATZ';
    DM_WinTuda.Tbl_Shib.Close;
    DM_WinTuda.Tbl_Shib.MacroByName('TableName').AsString:='ATMSHIBHATZ';
    DM_WinTuda.Tbl_Shib.Open;
    DM_WinTuda.Tbl_Azmn.Close;
    DM_WinTuda.Tbl_Azmn.MacroByName('TableName').AsString:='ATMAZMNHATZ';
    DM_WinTuda.Tbl_Azmn.Open;
    DM_WinTuda.Tbl_AzmnAzmnType.DefaultExpression:='1'; //���� ����
    Caption:='����� ����';
    Panel_ShibKind.Caption:='����� ����';
    Panel_ShibKind.Color:=clLime;
    Panel_ShibKind.Visible:=True;
  End
  Else
  Begin
    WinTudaAtmAzmnFileName:='ATMAZMN';
    WinTudaAtmShibFileName:='ATMSHIB';
    DM_WinTuda.Tbl_Shib.Close;
    DM_WinTuda.Tbl_Shib.MacroByName('TableName').AsString:='ATMSHIB';
    DM_WinTuda.Tbl_Shib.Open;
    DM_WinTuda.Tbl_Azmn.Close;
    DM_WinTuda.Tbl_Azmn.MacroByName('TableName').AsString:='ATMAZMN';
    DM_WinTuda.Tbl_Azmn.Open;
    DM_WinTuda.Tbl_AzmnAzmnType.DefaultExpression:='2'; //�����
    Caption:='������ ����';
    Panel_ShibKind.Visible:=False;
  End
end;

procedure TFrmWinTuda.Action_CopyHatzaotToHazmanotExecute(Sender: TObject);
Var
  CurHazmana :LongInt;
begin
  if Action_AtmAzmnHatz.Checked Then
  Begin
    CurHazmana:=DM_WinTuda.Tbl_Azmn.FieldByName('AzmnNo').AsInteger;
    Action_AtmAzmnHatzExecute(Action_AtmAzmnHatz);
    if Frm_CopyTnua=Nil Then
      Frm_CopyTnua:=TFrm_CopyTnua.Create(Nil);
    Frm_CopyTnua.AtmTabSheetBuild1.SqlFileName:='TSBCopyTnua2.Sql';
    Frm_CopyTnua.AtmTabSheetBuild1.ScrFileName:='TSBCopyTnua2.Scr';
    Frm_CopyTnua.CopyMethod:=1; //����� ���� ������� ��������
    Frm_CopyTnua.HatzaaNumberToProcess:=CurHazmana;
    Frm_CopyTnua.Show;
  End;



end;

procedure TFrmWinTuda.AtmDbNavigator1BeforeAction(Sender: TObject;
  Button: TmoNavBtns);
begin

  Case Button of
    nbaPost,nbaInsert : MarkSaveOnTable((Sender As TAtmDBNavigator).DataSource.DataSet);
  End;

end;

end.

unit Frm_Rehev;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, AtmAdvTable, DBCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    DBNavigator: TDBNavigator;
    Tbl_Rehev: TAtmTable;
    Tbl_RehevMis_Rishui: TStringField;
    Tbl_RehevInt_Kod_Kvutza: TIntegerField;
    Tbl_RehevKod_Kvutza: TIntegerField;
    Tbl_RehevShem_Kvutza_Prm: TStringField;
    Tbl_RehevInt_Kod_Miyun: TIntegerField;
    Tbl_RehevKod_Miyun: TIntegerField;
    Tbl_RehevKod_Miyun_Prm: TStringField;
    Tbl_RehevInt_Kod_Nephach: TIntegerField;
    Tbl_RehevKod_Nephach: TIntegerField;
    Tbl_RehevKod_Nepach_Prm: TStringField;
    Tbl_RehevInt_Kod_Yatzran: TIntegerField;
    Tbl_RehevKod_yatzran: TIntegerField;
    Tbl_RehevShem_Yatzran_Prm: TStringField;
    Tbl_RehevShnat_Itzur: TIntegerField;
    Tbl_RehevKod_Nehag: TIntegerField;
    Tbl_RehevShem_Nehag_Prm: TStringField;
    Tbl_RehevInt_Sug_Rehev: TIntegerField;
    Tbl_RehevSug_Rehev: TIntegerField;
    Tbl_RehevSug_Rehev_Prm: TStringField;
    Tbl_RehevSug_Delek: TIntegerField;
    Tbl_RehevInt_Sug_Delek: TIntegerField;
    Tbl_RehevSug_Delek_Prm: TStringField;
    Tbl_RehevMis_Pnimi: TIntegerField;
    Tbl_RehevInt_Mis_Pnimi: TIntegerField;
    Tbl_RehevMis_Pnimi_Prm: TStringField;
    Tbl_RehevMis_Shilda: TStringField;
    Tbl_RehevMis_Polisa: TStringField;
    Tbl_RehevMis_Han: TStringField;
    Tbl_RehevKvutzat_Mas: TStringField;
    Tbl_RehevMis_Kesher: TStringField;
    Tbl_RehevMis_Tag: TStringField;
    Tbl_RehevPle_Phon: TStringField;
    Tbl_RehevShovi_Shimush: TStringField;
    Tbl_RehevSug_Atzmada: TIntegerField;
    Tbl_RehevHetken: TStringField;
    Tbl_RehevTzeva: TStringField;
    Tbl_RehevChevrat_Bituach: TStringField;
    Tbl_RehevAsmachta: TStringField;
    Tbl_RehevMaslul_Bmp: TStringField;
    Tbl_RehevString_1: TStringField;
    Tbl_RehevString_2: TStringField;
    Tbl_RehevString_3: TStringField;
    Tbl_RehevString_4: TStringField;
    Tbl_RehevString_5: TStringField;
    Tbl_RehevInteger_1: TIntegerField;
    Tbl_RehevInteger_2: TIntegerField;
    Tbl_RehevInteger_3: TIntegerField;
    Tbl_RehevInteger_4: TIntegerField;
    Tbl_RehevInteger_5: TIntegerField;
    Tbl_RehevRishyon_Bmp: TStringField;
    Tbl_RehevLast_Spido: TIntegerField;
    Tbl_RehevMis_Rehev: TStringField;
    Tbl_RehevStatus_Rehev: TIntegerField;
    Tbl_RehevInt_Status_Rehev: TIntegerField;
    Tbl_RehevStatus_Rehev_Prm: TStringField;
    Tbl_RehevBealut: TStringField;
    Tbl_RehevShem_Sochen: TStringField;
    Tbl_RehevTel_Sochen: TStringField;
    Tbl_RehevPax_Sochen: TStringField;
    Tbl_RehevInt_Sug_Bituach: TIntegerField;
    Tbl_RehevSug_Bituach: TIntegerField;
    Tbl_RehevSug_Bituach_Prm: TStringField;
    Tbl_RehevShem_Nehag_Yachid: TStringField;
    Tbl_RehevMis_Rehev_Num: TIntegerField;
    Tbl_RehevEara: TStringField;
    Tbl_RehevTaarich_Knia: TDateTimeField;
    Tbl_RehevTaarich_Mechira: TDateTimeField;
    Tbl_RehevTaarich_Heskem: TDateTimeField;
    Tbl_RehevTaarich_Rishui: TDateTimeField;
    Tbl_RehevGmar_Bituach: TDateTimeField;
    Tbl_RehevTaarich_Last_Spido: TDateTimeField;
    Tbl_RehevChova_Taarich_Siyum: TDateTimeField;
    Tbl_RehevRechush_Taarich_Atchala: TDateTimeField;
    Tbl_RehevRechush_Taarich_Siyum: TDateTimeField;
    Tbl_RehevDate_1: TDateTimeField;
    Tbl_RehevDate_2: TDateTimeField;
    Tbl_RehevDate_3: TDateTimeField;
    Tbl_RehevDate_4: TDateTimeField;
    Tbl_RehevDate_5: TDateTimeField;
    Tbl_RehevString_6: TStringField;
    Tbl_RehevString_7: TStringField;
    Tbl_RehevString_8: TStringField;
    Tbl_RehevInt_Degem: TIntegerField;
    Tbl_RehevDegem: TIntegerField;
    Tbl_RehevDegem_Prm: TStringField;
    Tbl_RehevAlut_Achzaka: TBCDField;
    Tbl_RehevTotal: TBCDField;
    Tbl_RehevTotal_Teunot: TBCDField;
    Tbl_RehevTotal_Aluyot: TBCDField;
    Tbl_RehevMechir_Mechira: TBCDField;
    Tbl_RehevRadio: TBCDField;
    Tbl_RehevTosefet_Erech: TBCDField;
    Tbl_RehevChova_Premia: TBCDField;
    Tbl_RehevRechush_Premia: TBCDField;
    Tbl_RehevIshtatfut_Atzmit: TBCDField;
    Tbl_RehevIshtatfut_Musach: TBCDField;
    Tbl_RehevMoney_1: TBCDField;
    Tbl_RehevMoney_2: TBCDField;
    Tbl_RehevMoney_3: TBCDField;
    Tbl_RehevMoney_4: TBCDField;
    Tbl_RehevMoney_5: TBCDField;
    Tbl_RehevMoney_6: TBCDField;
    Tbl_RehevMoney_7: TBCDField;
    Tbl_RehevRehev_Pail: TSmallintField;
    Tbl_RehevShmashot: TSmallintField;
    Tbl_RehevGrira: TSmallintField;
    Tbl_RehevRehev_Chilufi: TSmallintField;
    Tbl_RehevTzad_G: TSmallintField;
    Tbl_RehevChova_Taarich_Atchala: TDateTimeField;
    Tbl_RehevKol_Nehag: TSmallintField;
    Tbl_RehevMeal_21: TSmallintField;
    Tbl_RehevMeal_24: TSmallintField;
    Tbl_RehevShabat: TSmallintField;
    Tbl_RehevAlut_Rehev: TBCDField;
    Tbl_RehevKibolet: TBCDField;
    Tbl_RehevMishkal_Motar: TBCDField;
    Tbl_RehevMishkal_Kolel: TBCDField;
    Tbl_RehevTiztrochet_Memutzaat: TBCDField;
    Tbl_RehevMadad_Basis: TBCDField;
    Tbl_RehevAchuz_maam_Mukar: TBCDField;
    Ds_Rehev: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

end.

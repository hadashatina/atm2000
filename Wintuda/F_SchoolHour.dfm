�
 TFRM_SCHOOLHOUR 0�L  TPF0TFrm_SchoolHourFrm_SchoolHourLeftTopXWidthUHeightKBiDiModebdRightToLeftCaption
����� ����Color	clBtnFace
ParentFont	
KeyPreview	OldCreateOrder	ParentBiDiModePositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroy	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel2Left Top WidthMHeightAlignalClient
BevelInner	bvLoweredBorderWidthCaptionPanel2TabOrder  TPageControlPageControl1LeftTopWidthAHeight
ActivePage	TabSheet1AlignalClientTabOrder  	TTabSheet	TabSheet1Caption����� TLabelLabel1Left�TopWidth7HeightCaption	��� �����FocusControlEditSCHOOL_YEAR  TLabelLabel2Left�TopWidth2HeightCaption��� ����FocusControlEditSEMEL_MOSAD  TLabelLabel3Left� TopWidthGHeightCaption����� �����  TLabelLabel4Left� TopWidth<HeightCaption
����� ����  TLabelLabel5Left�Top-Width7HeightCaption	���� ����FocusControlEditCODE_CLASS  TLabelLabel6LeftUTop-Width>HeightCaption
��� ������FocusControlEditCODE_MAKBILA  TLabelLabel7LeftTop-Width1HeightCaption��� ����FocusControlEditCODE_MEGAMA  TLabelLabel11Left�TopRWidth6HeightCaption	���� ����FocusControlEditDATA_SOURCE  TLabelLabel12Left�TopRWidthAHeightCaption����� �����FocusControlEditLAST_UPDATE  TDBTextDBText1LeftHTopWidth_HeightColorclInfoBk	DataFieldLookMosadName
DataSourceDS_ChoolHourParentColor  TAtmDbHEditEditSCHOOL_YEARLeft�TopWidth7HeightBiDiModebdRightToLeft	DataFieldSCHOOL_YEAR
DataSourceDS_ChoolHourParentBiDiModeTabOrder UseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel1LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange	  TAtmDbHEditEditSEMEL_MOSADLeft�TopWidth7HeightBiDiModebdRightToLeft	DataFieldSEMEL_MOSAD
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearch	IsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel2LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSearchComponentAtmAdvSearch_MosadSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditCODE_CLASSLeft�Top<Width7HeightBiDiModebdRightToLeft	DataField
CODE_CLASS
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearch	IsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel5LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSearchComponentDM_AtmCrt.AtmAdvSearch_KodTavlaSelectNextAfterSearch	StatusBarPanelNum�HebrewBeforeExecuteSearch!EditCODE_CLASSBeforeExecuteSearchLocateRecordOnChange  TAtmDbHEditEditCODE_MAKBILALeft\Top<Width7HeightBiDiModebdRightToLeft	DataFieldCODE_MAKBILA
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel6LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditCODE_MEGAMALeftTop;Width7HeightBiDiModebdRightToLeft	DataFieldCODE_MEGAMA
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearch	IsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel7LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSearchComponentDM_AtmCrt.AtmAdvSearch_KodTavlaSelectNextAfterSearch	StatusBarPanelNum�HebrewBeforeExecuteSearch"EditCODE_MEGAMABeforeExecuteSearchLocateRecordOnChange  TAtmDbHEditEditDATA_SOURCELeft�TopaWidth7HeightBiDiModebdRightToLeftColor	clBtnFace	DataFieldDATA_SOURCE
DataSourceDS_ChoolHourEnabledParentBiDiModeReadOnly	TabOrder	UseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel11LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditEditLAST_UPDATELeftgTopaWidthxHeightTabStopBiDiModebdLeftToRightColor	clBtnFace	DataFieldLAST_UPDATE
DataSourceDS_ChoolHourEnabledParentBiDiModeReadOnly	TabOrder
UseF2ToRunSearchIsFixedIsMustDefaultKinddkStringKeyToRunSearchksF2	LinkLabelLabel12LabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  	TGroupBoxGroupBox_HoursLeft Top� Width9HeightjAlignalBottomCaption���� ����� ����� ��� ��� �����Color	clBtnFaceParentColorTabOrder TLabelLabel8Left�TopWidth7HeightCaption	��� �����  TLabel
Lbl_PickupLeft�Top4Width=HeightCaption	��� �����  TLabelLbl_DisposeLeftTopNWidth2HeightCaption��� ����  TDBCheckBox	DBCB_Day1Left�TopWidthHeightCaption�	DataFieldDAY1
DataSourceDS_ChoolHourTabOrder ValueChecked1ValueUnchecked0  TDBCheckBox	DBCB_Day2LeftzTopWidthHeightCaption�	DataFieldDAY2
DataSourceDS_ChoolHourTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	DBCB_Day3Left.TopWidthHeightCaption�	DataFieldDAY3
DataSourceDS_ChoolHourTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	DBCB_Day4Left� TopWidthHeightCaption�	DataFieldDAY4
DataSourceDS_ChoolHourTabOrder	ValueChecked1ValueUnchecked0  TDBCheckBox	DBCB_Day5Left� TopWidthHeightCaption�	DataFieldDAY5
DataSourceDS_ChoolHourTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	DBCB_Day6LeftTTopWidthHeightCaption�	DataFieldDAY6
DataSourceDS_ChoolHourTabOrderValueChecked1ValueUnchecked0  TAtmDbHEditDBEdit_Pic1Left�Top0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC1
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Dis1Left�TopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS1
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Pic2LeftjTop0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC2
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Dis2LeftjTopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS2
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Pic3Left"Top0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC3
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Pic4Left� Top0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC4
DataSourceDS_ChoolHourParentBiDiModeTabOrder
UseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Pic5Left� Top0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC5
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Pic6LeftJTop0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC6
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Dis3Left"TopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS3
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Dis4Left� TopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS4
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Dis5Left� TopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS5
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDBEdit_Dis6LeftJTopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS6
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaption�	DataFieldDAY7
DataSourceDS_ChoolHourTabOrderValueChecked1ValueUnchecked0  TAtmDbHEditDbEdit_Pic7LeftTop0WidthDHeightBiDiModebdLeftToRight	DataFieldPIC7
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabel
Lbl_PickupLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange  TAtmDbHEditDbEdit_Dis7LeftTopJWidthDHeightBiDiModebdLeftToRight	DataFieldDIS7
DataSourceDS_ChoolHourParentBiDiModeTabOrderUseF2ToRunSearchIsFixedIsMustDefaultKinddkTimeKeyToRunSearchksF2	LinkLabelLbl_DisposeLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionText
FixedColor	clBtnFaceNormalColorclWindowSelectNextAfterSearch	StatusBarPanelNum�HebrewLocateRecordOnChange   TAtmDBDateEditAtmDBDateEdit1Left� TopWidthnHeight	DataField
START_DATE
DataSourceDS_ChoolHourBiDiModebdLeftToRightParentBiDiMode	NumGlyphsTabOrderStartOfWeekSunWeekendsSat 
YearDigitsdyFourLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionTextF9String
10/11/1999IsFixed
FixedColor	clBtnFaceStatusBarPanelNum�  TAtmDBDateEditAtmDBDateEdit2LeftdTopWidthnHeight	DataFieldEND_DATE
DataSourceDS_ChoolHourBiDiModebdLeftToRightParentBiDiMode	NumGlyphsTabOrderStartOfWeekSunWeekendsSat 
YearDigitsdyFourLabelColorSelectclActiveCaptionLabelTextColorSelectclCaptionTextF9String
12/12/1999IsFixed
FixedColor	clBtnFaceStatusBarPanelNum�  TAtmDbComboBoxAtmDbComboBox_CODE_CLASSLeft�Top<WidthfHeightTabStopColorclInfoBk
ItemHeightSorted	TabOrder
DataSourceDS_ChoolHour	DataField
CODE_CLASSSugTavlaIndex   TAtmDbComboBoxAtmDbComboBox_CODE_MEGAMALeft� Top;WidthfHeightTabStopColorclInfoBk
ItemHeightSorted	TabOrder
DataSourceDS_ChoolHour	DataFieldCODE_MEGAMASugTavlaIndex      TToolBarToolBar1Left TopWidthMHeightAlignalBottomCaptionToolBar1Flat	ImagesDM_AtmCrt.ImageList_CrtButtonsTabOrder TDBNavigatorDBNavigatorLeft Top Width� Height
DataSourceDS_ChoolHourFlat	Ctl3DHints.Strings����� ����������� �����
����� ��������� ������
����� ����	��� �����
���� �����
���� �������� ����������� ParentCtl3DTabOrder BeforeActionDBNavigatorBeforeAction  TToolButtonToolButton1Left� Top WidthCaptionToolButton1StyletbsSeparator  TToolButtonToolButton2Left� Top ActionDM_AtmCrt.Action_OpenSearch  TToolButtonTBtn_ReportLeftTop Hint�����CaptionTBtn_Report
ImageIndexOnClickTBtn_ReportClick   TDataSourceDS_ChoolHourDataSetTbl_SchoolHourLeft� Top  	TAtmTableTbl_SchoolHourAfterInsertTbl_SchoolHourAfterInsert
BeforePostTbl_SchoolHourBeforePost	AfterPostTbl_SchoolHourAfterPostDatabaseName	DB_AtmCrtIndexFieldNamesAUTOINC	TableNameSCHLHOURTableCheckInterval�TableChangeNotifyAutoFlashToDisk	AllartBeforeSaveAutoValidateLookupFields	DeleteDetailRecordsConfirmDeleteDetail	Left� Top TAutoIncFieldTbl_SchoolHourAUTOINC	FieldNameAUTOINCReadOnly	  TIntegerFieldTbl_SchoolHourSCHOOL_YEAR	FieldNameSCHOOL_YEARRequired	  TIntegerFieldTbl_SchoolHourSEMEL_MOSAD	FieldNameSEMEL_MOSADRequired	
OnValidate!Tbl_SchoolHourSEMEL_MOSADValidate  TDateTimeFieldTbl_SchoolHourSTART_DATE	FieldName
START_DATE
OnValidateTbl_SchoolHourEND_DATEValidateDisplayFormat
dd/mm/yyyyEditMask!99/99/0000;1;_  TDateTimeFieldTbl_SchoolHourEND_DATE	FieldNameEND_DATE
OnValidateTbl_SchoolHourEND_DATEValidateDisplayFormat
dd/mm/yyyyEditMask!99/99/0000;1;_  TIntegerFieldTbl_SchoolHourCODE_CLASS	FieldName
CODE_CLASS  TIntegerFieldTbl_SchoolHourCODE_MAKBILA	FieldNameCODE_MAKBILA  TIntegerFieldTbl_SchoolHourCODE_MEGAMA	FieldNameCODE_MEGAMA  TIntegerFieldTbl_SchoolHourDATA_SOURCE	FieldNameDATA_SOURCE  TDateTimeFieldTbl_SchoolHourLAST_UPDATE	FieldNameLAST_UPDATE  TIntegerFieldTbl_SchoolHourDAY1DefaultExpression1	FieldNameDAY1OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC1DefaultExpression'08:00'DisplayLabel��� ����� �	FieldNamePIC1DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS1DefaultExpression'12:00'DisplayLabel��� ����� �	FieldNameDIS1DisplayFormatHH:MMEditMask
!90:00;1;_  TIntegerFieldTbl_SchoolHourDAY2DefaultExpression1	FieldNameDAY2OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC2DefaultExpression'08:00'DisplayLabel��� ����� �	FieldNamePIC2DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS2DefaultExpression'12:00'DisplayLabel��� ����� �	FieldNameDIS2DisplayFormatHH:MMEditMask
!90:00;1;_  TIntegerFieldTbl_SchoolHourDAY3DefaultExpression1	FieldNameDAY3OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC3DefaultExpression'08:00'DisplayLabel��� ����� �	FieldNamePIC3DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS3DefaultExpression'12:00'DisplayLabel��� ����� �	FieldNameDIS3DisplayFormatHH:MMEditMask
!90:00;1;_  TIntegerFieldTbl_SchoolHourDAY4DefaultExpression1	FieldNameDAY4OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC4DefaultExpression'08:00'DisplayLabel��� ����� �	FieldNamePIC4DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS4DefaultExpression'12:00'DisplayLabel��� ����� �	FieldNameDIS4DisplayFormatHH:MMEditMask
!90:00;1;_  TIntegerFieldTbl_SchoolHourDAY5DefaultExpression1	FieldNameDAY5OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC5DefaultExpression'08:00'DisplayLabel��� ����� �	FieldNamePIC5DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS5DefaultExpression'12:00'DisplayLabel��� ����� �	FieldNameDIS5DisplayFormatHH:MMEditMask
!90:00;1;_  TIntegerFieldTbl_SchoolHourDAY6DefaultExpression1	FieldNameDAY6OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC6DefaultExpression'08:00'DisplayLabel��� ����� �	FieldNamePIC6DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS6DefaultExpression'12:00'DisplayLabel��� ����� �	FieldNameDIS6DisplayFormatHH:MMEditMask
!90:00;1;_  TIntegerFieldTbl_SchoolHourDAY7DefaultExpression0	FieldNameDAY7OnChangeTbl_SchoolHourDAY1Change
OnValidateTbl_SchoolHourDAY1Validate  TDateTimeFieldTbl_SchoolHourPIC7DisplayLabel��� ����� �	FieldNamePIC7DisplayFormatHH:MMEditMask
!90:00;1;_  TDateTimeFieldTbl_SchoolHourDIS7	FieldNameDIS7DisplayFormatHH:MMEditMask
!90:00;1;_  TStringFieldTbl_SchoolHourLookMosadName	FieldKindfkLookup	FieldNameLookMosadNameLookupDataSet	Tbl_MosadLookupKeyFieldsSEMEL_MOSADLookupResultField
NAME_MOSAD	KeyFieldsSEMEL_MOSADLookupCache	Lookup	  TStringFieldTbl_SchoolHourLookClass	FieldKindfkLookup	FieldName	LookClass	KeyFields
CODE_CLASSLookupCache	Lookup	  TStringFieldTbl_SchoolHourLookMegama	FieldKindfkLookup	FieldName
LookMegama	KeyFieldsCODE_MEGAMALookupCache	Lookup	  TStringFieldTbl_SchoolHourNAME_OF_DAYS	FieldNameNAME_OF_DAYSSize   TTable	Tbl_MosadDatabaseName	DB_AtmCrtIndexFieldNamesSEMEL_MOSAD	TableNameMOSADLeft:Top  TTableTbl_KodTavlaDatabaseName	DB_AtmCrtIndexFieldNamesSug_Tavla;Kod_Tavla	TableNameKODTAVLALeftlTop  TAtmAdvSearchAtmAdvSearch_MosadCaption����� ������KeyIndex SourceDataSet	Tbl_MosadShowFields.StringsSEMEL_MOSAD
NAME_MOSAD ShowFieldsHeader.Strings��� ������ ���� ShowFieldsDisplayWidth.Strings100150 ReturnFieldIndex SeparateChar,KeepListLeftTop   
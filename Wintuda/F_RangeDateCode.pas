unit F_RangeDateCode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AdvSearch, StdCtrls, Buttons, ToolEdit, Mask,Registry;

type
  TFrm_RangeDateCode = class(TForm)
    DateEdit_From: TDateEdit;
    DateEdit_To: TDateEdit;
    ComboEdit_FromCode: TComboEdit;
    ComboEdit_ToCode: TComboEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    procedure ComboEdit_FromCodeButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_RangeDateCode: TFrm_RangeDateCode;

implementation
Uses AtmConst;
{$R *.DFM}

procedure TFrm_RangeDateCode.ComboEdit_FromCodeButtonClick(
  Sender: TObject);
begin
  if AtmAdvSearch_Maslul.Execute Then
    TComboEdit(Sender).Text:=AtmAdvSearch_Maslul.ReturnString;
end;

procedure TFrm_RangeDateCode.FormCreate(Sender: TObject);
Var
  R :TRegistry;
begin
  R:=TRegistry.Create;
  Try
    R.RootKey:=HKEY_CURRENT_USER;
    R.OpenKey(RegKeyForAtm2000+'\Forms\'+Name,True);
    if R.ValueExists('FromDate') Then
      DateEdit_From.Text:=R.ReadString('FromDate');
    if R.ValueExists('ToDate') Then
      DateEdit_To.Text:=R.ReadString('ToDate');
    if R.ValueExists('FromCode') Then
      ComboEdit_FromCode.Text:=R.ReadString('FromCode');
    if R.ValueExists('ToCode') Then
      ComboEdit_ToCode.Text:=R.ReadString('ToCode');
  Finally
    R.Free;
  End;
end;

procedure TFrm_RangeDateCode.FormClose(Sender: TObject;
  var Action: TCloseAction);
Var
  R :TRegistry;
begin
  if ModalResult=mrOk Then
  Begin
    R:=TRegistry.Create;
    Try
      R.RootKey:=HKEY_CURRENT_USER;
      R.OpenKey(RegKeyForAtm2000+'\Forms\'+Name,True);
      R.WriteString('FromDate',DateEdit_From.Text);
      R.WriteString('ToDate',DateEdit_To.Text);
      R.WriteString('FromCode',ComboEdit_FromCode.Text);
      R.WriteString('ToCode',ComboEdit_ToCode.Text);
    Finally
      R.Free;
    End;

  End;
end;

end.

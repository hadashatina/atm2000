unit F_SchoolHour;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, DBTables, Mask, ExtCtrls, ComCtrls,
  AtmAdvTable, AtmComp, ToolEdit, RXDBCtrl, AtmDBDateEdit, ToolWin,
  AdvSearch, AtmLookCombo;

type
  TFrm_SchoolHour = class(TForm)
    Tbl_SchoolHourSCHOOL_YEAR: TIntegerField;
    Tbl_SchoolHourSEMEL_MOSAD: TIntegerField;
    Tbl_SchoolHourSTART_DATE: TDateTimeField;
    Tbl_SchoolHourEND_DATE: TDateTimeField;
    Tbl_SchoolHourCODE_CLASS: TIntegerField;
    Tbl_SchoolHourCODE_MAKBILA: TIntegerField;
    Tbl_SchoolHourCODE_MEGAMA: TIntegerField;
    Tbl_SchoolHourDATA_SOURCE: TIntegerField;
    Tbl_SchoolHourLAST_UPDATE: TDateTimeField;
    DS_ChoolHour: TDataSource;
    Panel2: TPanel;
    Tbl_SchoolHour: TAtmTable;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EditSCHOOL_YEAR: TAtmDbHEdit;
    EditSEMEL_MOSAD: TAtmDbHEdit;
    EditCODE_CLASS: TAtmDbHEdit;
    EditCODE_MAKBILA: TAtmDbHEdit;
    EditCODE_MEGAMA: TAtmDbHEdit;
    EditDATA_SOURCE: TAtmDbHEdit;
    EditLAST_UPDATE: TAtmDbHEdit;
    Tbl_SchoolHourDAY1: TIntegerField;
    Tbl_SchoolHourPIC1: TDateTimeField;
    Tbl_SchoolHourDIS1: TDateTimeField;
    Tbl_SchoolHourDAY2: TIntegerField;
    Tbl_SchoolHourPIC2: TDateTimeField;
    Tbl_SchoolHourDIS2: TDateTimeField;
    Tbl_SchoolHourDAY3: TIntegerField;
    Tbl_SchoolHourPIC3: TDateTimeField;
    Tbl_SchoolHourDIS3: TDateTimeField;
    Tbl_SchoolHourDAY4: TIntegerField;
    Tbl_SchoolHourPIC4: TDateTimeField;
    Tbl_SchoolHourDIS4: TDateTimeField;
    Tbl_SchoolHourDAY5: TIntegerField;
    Tbl_SchoolHourPIC5: TDateTimeField;
    Tbl_SchoolHourDIS5: TDateTimeField;
    Tbl_SchoolHourDAY6: TIntegerField;
    Tbl_SchoolHourPIC6: TDateTimeField;
    GroupBox_Hours: TGroupBox;
    Label8: TLabel;
    Lbl_Pickup: TLabel;
    Lbl_Dispose: TLabel;
    DBCB_Day1: TDBCheckBox;
    DBCB_Day2: TDBCheckBox;
    DBCB_Day3: TDBCheckBox;
    DBCB_Day4: TDBCheckBox;
    DBCB_Day5: TDBCheckBox;
    DBCB_Day6: TDBCheckBox;
    DBEdit_Pic1: TAtmDbHEdit;
    DBEdit_Dis1: TAtmDbHEdit;
    DBEdit_Pic2: TAtmDbHEdit;
    DBEdit_Dis2: TAtmDbHEdit;
    DBEdit_Pic3: TAtmDbHEdit;
    DBEdit_Pic4: TAtmDbHEdit;
    DBEdit_Pic5: TAtmDbHEdit;
    DBEdit_Pic6: TAtmDbHEdit;
    DBEdit_Dis3: TAtmDbHEdit;
    DBEdit_Dis4: TAtmDbHEdit;
    DBEdit_Dis5: TAtmDbHEdit;
    DBEdit_Dis6: TAtmDbHEdit;
    Tbl_SchoolHourAUTOINC: TAutoIncField;
    Tbl_SchoolHourDIS6: TDateTimeField;
    Tbl_SchoolHourDAY7: TIntegerField;
    Tbl_SchoolHourPIC7: TDateTimeField;
    Tbl_SchoolHourDIS7: TDateTimeField;
    Tbl_Mosad: TTable;
    Tbl_SchoolHourLookClass: TStringField;
    DBText1: TDBText;
    Tbl_SchoolHourLookMosadName: TStringField;
    Tbl_KodTavla: TTable;
    Tbl_SchoolHourLookMegama: TStringField;
    DBCheckBox1: TDBCheckBox;
    DbEdit_Pic7: TAtmDbHEdit;
    DbEdit_Dis7: TAtmDbHEdit;
    AtmDBDateEdit1: TAtmDBDateEdit;
    AtmDBDateEdit2: TAtmDBDateEdit;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    AtmAdvSearch_Mosad: TAtmAdvSearch;
    AtmDbComboBox_CODE_CLASS: TAtmDbComboBox;
    AtmDbComboBox_CODE_MEGAMA: TAtmDbComboBox;
    Tbl_SchoolHourNAME_OF_DAYS: TStringField;
    TBtn_Report: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Tbl_SchoolHourAfterInsert(DataSet: TDataSet);
    procedure Tbl_SchoolHourAfterPost(DataSet: TDataSet);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_SchoolHourBeforePost(DataSet: TDataSet);
    procedure EditCODE_CLASSBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure EditCODE_MEGAMABeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Tbl_SchoolHourDAY1Validate(Sender: TField);
    procedure Tbl_SchoolHourEND_DATEValidate(Sender: TField);
    procedure Tbl_SchoolHourSEMEL_MOSADValidate(Sender: TField);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure Tbl_SchoolHourDAY1Change(Sender: TField);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Frm_SchoolHour: TFrm_SchoolHour;

implementation
Uses Crt_Glbl,AtmRutin,AtmConst,DMAtmCrt;
{$R *.DFM}

procedure TFrm_SchoolHour.FormCreate(Sender: TObject);
var
   i:longint;
   SList :TStringList;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_DargatClass),Tbl_SchoolHourLookClass);
     SList.AddObject(IntToStr(SugTavla_Megama),Tbl_SchoolHourLookMegama);
     FillKodTavlaLookupList(Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_DargatClass),AtmDbComboBox_CODE_CLASS);
     SList.AddObject(IntToStr(SugTavla_Megama),AtmDbComboBox_CODE_MEGAMA);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;



     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
end;

procedure TFrm_SchoolHour.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_SchoolHour.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_SchoolHour);
     DM_AtmCrt.Crt_GlobalActionList.UpdateAction(DM_AtmCrt.Action_OpenSearch);
end;

procedure TFrm_SchoolHour.Tbl_SchoolHourAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;

end;

procedure TFrm_SchoolHour.Tbl_SchoolHourAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_SchoolHour.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
end;

procedure TFrm_SchoolHour.Tbl_SchoolHourBeforePost(DataSet: TDataSet);
Var
   I:LongInt;
   StrH :String;
begin
     Tbl_SchoolHour.FieldByName('LAST_UPDATE').Value:=Now;
     StrH:='';
     For I:=1 To 6 Do
     Begin
         if Tbl_SchoolHour.FieldByName('Day'+IntToStr(I)).Value=1 Then
            StrH:=StrH+Chr(Ord('�')-1+I)+' '
         Else
             StrH:=StrH+'  ';
     End;
     if Tbl_SchoolHour.FieldByName('Day7').Value=1 Then
        StrH:=StrH+'� '
     Else
         StrH:=StrH+'  ';

     Tbl_SchoolHour.FieldByName('NAME_OF_DAYS').AsString:=StrH;
end;

procedure TFrm_SchoolHour.EditCODE_CLASSBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_DargatClass;
end;

procedure TFrm_SchoolHour.EditCODE_MEGAMABeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
    ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Megama;
end;

procedure TFrm_SchoolHour.FormShow(Sender: TObject);
Var
   MyControl :TWinControl;
begin
     MyControl:=FindFirstControlInTabOrder(Self);
     if MyControl.CanFocus Then
        MyControl.SetFocus
     Else
         SelectNext(MyControl,True,True);
end;

procedure TFrm_SchoolHour.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_SchoolHour:=Nil;
     Action:=caFree;
end;

procedure TFrm_SchoolHour.Tbl_SchoolHourDAY1Validate(Sender: TField);
Var
   StrH :String;
begin
     StrH:=Copy(Sender.FieldName,4,1);
     Tbl_SchoolHour.FieldByName('Pic'+StrH).Required:=Sender.AsInteger=1;
     Tbl_SchoolHour.FieldByName('Dis'+StrH).Required:=Sender.AsInteger=1;
     Try
        TDbEdit(FindComponent('DBEdit_Pic'+StrH)).Enabled:=Sender.AsInteger=1;
        TDbEdit(FindComponent('DBEdit_Dis'+StrH)).Enabled:=Sender.AsInteger=1;
     Except
     End;
end;

procedure TFrm_SchoolHour.Tbl_SchoolHourEND_DATEValidate(Sender: TField);
begin
     if (Not Tbl_SchoolHourEnd_Date.IsNull) And (Not Tbl_SchoolHourStart_Date.IsNull) And
        (Tbl_SchoolHourEnd_Date.AsDateTime<Tbl_SchoolHourStart_Date.AsDateTime) Then
     Begin
          MessageDlg('����� ���� ��� ������ �����',mtError,[mbOk],0);
          Abort;
     End;
end;

procedure TFrm_SchoolHour.Tbl_SchoolHourSEMEL_MOSADValidate(
  Sender: TField);
begin
     if Not Tbl_Mosad.FindKey([Tbl_SchoolHourSemel_Mosad.Value]) Then
     Begin
         MessageDlg('���� �� ����',mtError,[mbOk],0);
         Abort;
     End;
end;

procedure TFrm_SchoolHour.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msSchoolHour],False,CrtMeholelHandle);

end;

procedure TFrm_SchoolHour.Tbl_SchoolHourDAY1Change(Sender: TField);
Var
   StrH:String;
begin
     StrH:=Copy(Sender.FieldName,4,1);
     Try
        TDbEdit(FindComponent('DBEdit_Pic'+StrH)).Enabled:=Sender.AsInteger=1;
        TDbEdit(FindComponent('DBEdit_Dis'+StrH)).Enabled:=Sender.AsInteger=1;
     Except
     End;
end;

end.

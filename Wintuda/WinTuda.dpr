{ 16/03/99 11:23:08 > [Aviran on AVIRAN] check in: (0.0)  / None }
library WinTuda;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ExceptionLog,
  SysUtils,
  Classes,
  Windows,
  DMWinTuda in 'DMWinTuda.pas' {DM_WinTuda: TDataModule},
  Frm_WinTuda in 'Frm_WinTuda.pas' {FrmWinTuda},
  AtmRutin in 'AtmRutin.pas',
  AtmConst in 'AtmConst.pas',
  MslDllInterface in 'MslDllInterface.pas',
  FrmSearch in 'FrmSearch.pas' {Frm_Search},
  F_Progress in 'F_Progress.pas' {Frm_Progress},
  MainTovala in 'MainTovala.pas' {Frm_MainWinTuda},
  aboutWintuda in 'AboutWintuda.pas' {AboutBox},
  F_WinTudaParam in 'F_WinTudaParam.pas' {Frm_WinTudaParams},
  F_ChooseYehus in 'F_ChooseYehus.pas' {Frm_ChooseYehus},
  F_CopyTnua in 'F_CopyTnua.pas' {Frm_CopyTnua},
  F_UpdPriceFromMehiron in 'F_UpdPriceFromMehiron.pas' {Frm_UpdatePriceFromMehiron},
  F_WinTudaAtmLak in 'F_WinTudaAtmLak.pas' {Frm_AtmShibLak},
  RptAtmShibLak in 'RptAtmShibLak.pas' {Frm_RptAtmShibLak},
  F_GlblWinTuda in '\\almog-ds\AtmPtojects\Atm2000\Wintuda\F_GlblWinTuda.pas' {Frm_GlblWinTuda};

{$R *.RES}

Procedure OpenWinTuda(HazmanaNum :LongInt);
Begin
     //ShowWinTuda(HazmanaNum);
     StartTovala(False);
End;

Procedure InitWinTuda(AliasName,DirForIni:ShortString);
Begin
{     CurAliasName:=AliasName;
     DirectoryForIni:=DirForIni;
}
   InitVariables(AliasName,DirForIni);
//     CreateGlobalForm;
End;

Procedure DoneWinTuda;
Begin
     FreeDllHandle(WinTudaMeholelHandle);
     FreeDllHandle(WinTudaKupaHandle);
     DestroyWinTuda;
End;

Procedure MyDllProc(Reason :Integer);
Begin
     if Reason = DLL_PROCESS_DETACH Then
        DoneWinTuda;
End;

Exports OpenWinTuda,
        InitWinTuda,
        DoneWinTuda;
begin
     DLLProc:=@MyDLLProc;
end.

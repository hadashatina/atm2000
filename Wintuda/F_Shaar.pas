unit F_Shaar;

interface

uses
  Bde, DBTables, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, DB, StdCtrls, DBCtrls,ExtCtrls, Buttons, Scale250, Grids,
  DBGrids,AtmAdvTable, RxQuery, AtmRxQuery, RXDBCtrl;

type
  TFrm_Shaar = class(TForm)
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    Scale1: TScale;
    RxDBGrid1: TRxDBGrid;
    AtmRxQuery_Shaar: TAtmRxQuery;
    DataSource1: TDataSource;
    UpdateSQL1: TUpdateSQL;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ShowForm(MainValue:String);
  end;

var
  Frm_Shaar: TFrm_Shaar;

const
  eKeyViol = 9729;               {���� ����}

implementation

uses Crt_Glbl, AtmRutin;


{$R *.DFM}
procedure TFrm_Shaar.FormCreate(Sender: TObject);
Var
   I :longInt;
begin
   For I:=0 To ComponentCount-1 Do
   Begin
        if Components[I] is TDataset Then
           (Components[I] As TDataset).Open;
   End;
end;

procedure TFrm_Shaar.FormDestroy(Sender: TObject);
Var
   I :longInt;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataset).Close;
     End;
end;


procedure TFrm_Shaar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Shaar:=Nil;
     Action:=caFree;
end;

procedure TFrm_Shaar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyboardManageForTableAction(Sender,Key,Shift,AtmRxQuery_Shaar);
end;

Procedure TFrm_Shaar.ShowForm(MainValue:String);
Begin
  Show;
End;

end.

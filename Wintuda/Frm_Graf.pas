unit Frm_Graf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, Menus, HebMainMenu, Db, DBTables, ComCtrls;

type
  TF_Graf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    SGr_Left: TStringGrid;
    Men_Main: THebMainMenu;
    N1: TMenuItem;
    N1_Close: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    SBr_Down: TStatusBar;
    Scbox: TScrollBox;
    SGr_Right: TStringGrid;
    procedure SGr_RightDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure DrawRightGridHeader(Canv:TCanvas;Rect: TRect);
    procedure FormCreate(Sender: TObject);
    procedure InitGrids;
    procedure SGr_LeftClick(Sender: TObject);
    procedure SGr_RightClick(Sender: TObject);
    procedure SGr_LeftTopLeftChanged(Sender: TObject);
    procedure SGr_RightTopLeftChanged(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N1_CloseClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure SGr_RightMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SGr_RightMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SGr_RightMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    Dragging : boolean;
    ShibMembStartDrag,
    CarMembStartDrag,
    ShibMembEndDrag,
    CarMembEndDrag  : longint;
    DraggingType : integer;
  end;

var
  F_Graf: TF_Graf;
implementation
  uses Grf_data, Grf_Opt,UniSadran;
{$R *.DFM}
      { ************************ }
procedure TF_Graf.DrawRightGridHeader(Canv:TCanvas;Rect: TRect);
var HelpStr : shortstring;
    ix1 : longint;

begin

  with Canv do
  begin
    with Rect do
    begin
      Font.Size:= (Bottom - Top) div 4;
      Pen.Color:=clBlack;
      for ix1:=1 to round((EndPoint - BegPoint)/ MajorDeltaT) do
      begin
        case SugMajor of
          1: {hour}
            begin
              LongTimeFormat:='HH:MM';
              HelpStr:= timetostr(BegPoint + MajorDeltaT * (ix1-1));
            end;
          2: {day}
            begin
              LongDateFormat:='DD/MM/YY';
              HelpStr:= Datetostr(BegPoint + MajorDeltaT * (ix1-1));
            end;
        end;{case}
        textout(Left+ MajorDeltaX* (ix1-1)+
           ( MajorDeltaX - TextWidth(HelpStr))div 2 ,Top,HelpStr);
        MoveTo(Left+ MajorDeltaX* ix1 ,Top );
        LineTo(Left+ MajorDeltaX* ix1,Bottom  div 2 );
      end;
      MoveTo(Left,Top + (Bottom - Top) div 2 );
      LineTo(Right,Top + (Bottom - Top) div 2 );
    end;
  end;
end;
  { ****************** }
procedure TF_Graf.SGr_RightDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var MyRect : TRect;
    ShibMemb : longint;
    ix1:longint;
    MisRishui : shortstring;
    LineWidth : longint;
    HelpStr : shortstring;
begin
  with SGr_Right.Canvas do
  begin
    if ARow = 0 then
      DrawRightGridHeader(SGr_Right.Canvas,Rect);
    if ARow <> 0 then
    with ShibList do
    begin
      LineWidth:=Rect.Right - Rect.Left;
      MisRishui:=SGr_Left.cells[0,ARow];
      ShibMemb:=FindFirstShib(MisRishui);
      if ShibMemb <> (-1) then
      for ix1:= ShibMemb to (Count -1) do
      begin
        ShibObject:=Items[ix1];
        with ShibObject do
        begin
          if LNo_Rishui <> MisRishui then
           break;

          MyRect.Left:=Rect.Left+round(ShibObject.LBegPoint * LineWidth);
          MyRect.Right:=Rect.Left+round(ShibObject.LEndPoint * LineWidth);
          MyRect.Top:=Rect.Top+round((Rect.Bottom - Rect.Top)/DrawRectWidth);
          MyRect.Bottom:=Rect.Bottom-round((Rect.Bottom - Rect.Top)/DrawRectWidth);
          if pos(TextOfEmpty,LNo_Rishui) <> 0 then
            Brush.Color:=clRed
          else
            Brush.Color:=clGreen;
          FillRect(MyRect);
        end;
      end;
    end;
  end;
end;
   { ********************* }
procedure TF_Graf.FormCreate(Sender: TObject);
begin
  Dragging:=FALSE;
  ShibMembStartDrag:=-1;
  CarMembStartDrag:=-1;
  ShibMembEndDrag:=-1;
  CarMembEndDrag:=-1;
  InitLists;
  InitGrids;
end;
   { ******************** }
procedure TF_Graf.InitGrids;
var ix1 : longint;
begin
  SGr_Left.RowCount:=CarList.Count+1;
  SGr_Left.ColCount:=3;SGr_Right.ColCount:=1;
  SGr_Right.RowCount:=SGr_Left.RowCount;
  SGr_Right.DefaultColWidth:=DrawColWidth;
  SGr_Right.Width:=DrawColWidth;
  with SGr_Left do
  begin
    cells[0,0]:='�����';cells[1,0]:='�����';cells[2,0]:='���';
    for ix1:= 1 to CarList.Count  do
    begin
      CarObject:=CarList.Items[ix1-1];
      with CarObject do
      begin
        cells[0,ix1]:=LNo_Rishui;
        cells[1,ix1]:=inttostr(LNo_Order);
        cells[2,ix1]:=inttostr(LNo_Group);
      end;
    end;
  end;
end;
       { ******************** }
procedure TF_Graf.SGr_LeftClick(Sender: TObject);
begin
  Sgr_Right.Row:=SGr_Left.Row;
end;

procedure TF_Graf.SGr_RightClick(Sender: TObject);
begin
  Sgr_Left.Row:=SGr_Right.Row;
end;

procedure TF_Graf.SGr_LeftTopLeftChanged(Sender: TObject);
begin
  SGr_Right .TopRow := SGr_Left .TopRow;
end;

procedure TF_Graf.SGr_RightTopLeftChanged(Sender: TObject);
begin
  SGr_Left .TopRow := SGr_Right .TopRow;
end;
        { **************** }
procedure TF_Graf.FormDestroy(Sender: TObject);
begin
  DoneLists;
end;

procedure TF_Graf.N1_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TF_Graf.N3Click(Sender: TObject);
begin
  Grf_options:=TGrf_options.Create(Self);
  Grf_options.ShowModal;
  if Grf_options.ModalResult = mrOK then
  with Grf_options do
  begin
    MajorDeltaX:=SpE_Length.Value;
    if Cbx_MajorScale.text='���' then
    begin
      MajorDeltaT:=strtodatetime('01:00')* SpE_Mafor.Value; {���}
      SugMajor:=1;
    end;
    if Cbx_MajorScale.text='���' then
    begin
      MajorDeltaT:=1* SpE_Mafor.Value; {day}
      SugMajor:=2;
    end;
    if Cbx_MajorScale.text='����' then
    begin
      MajorDeltaT:=31* SpE_Mafor.Value; {month}
      SugMajor:=4;
    end;
    DrawColWidth := round((EndPoint - BegPoint)/ MajorDeltaT) * MajorDeltaX;
  end;
  InitGrids;
  Grf_options.Free;
end;

procedure TF_Graf.SGr_RightMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var ACol,ARow : longint;
    ix1,ix2 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    XRel : real;
begin
  XRel:=X / SGr_Right.Width;
  with SGr_Right do
  begin
    ARow:= SGr_Right.MouseCoord(X,Y).Y;
    ACol:= SGr_Right.MouseCoord(X,Y).X;
    if ARow > 0 then
    begin
      CarObject:=CarList.Items[ARow-1];
      with CarObject do
      begin
        MisRishui:=LNo_Rishui;
        ShibMemb:=ShibList.FindFirstShib(MisRishui);
        if ShibMemb <> (-1) then
        for ix1:=  ShibMemb to (ShibList.Count -1) do
        begin
          ShibObject:=ShibList.Items[ix1];
          with ShibObject do
          begin
            if LNo_Rishui = MisRishui then
            if (LBegPoint <= XRel) and
                 (LEndPoint >= XRel) then
            begin
                SBr_Down.SimpleText:=
                  ' '+  timetostr(LBegTime)+ ' '+  timetostr(LEndTime);
                break;
            end
            else
              SBr_Down.SimpleText:='';
          end;
        end
        else
          SBr_Down.SimpleText:=MisRishui;
      end;
    end;
  end;
end;

procedure TF_Graf.SGr_RightMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var ACol,ARow : longint;
    ix1 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    XRel : real;
begin
  if Button = mbRight then
  begin
    XRel:=X / SGr_Right.Width;
    with SGr_Right do
    begin
      ARow:= SGr_Right.MouseCoord(X,Y).Y;
      ACol:= SGr_Right.MouseCoord(X,Y).X;
      if ARow > 0 then
      begin
        CarMembStartDrag:=ARow-1;
        CarObject:=CarList.Items[  CarMembStartDrag];
        with CarObject do
       begin
          MisRishui:=LNo_Rishui;
          ShibMemb:=ShibList.FindFirstShib(MisRishui);
          if ShibMemb <> (-1) then
          for ix1:=  ShibMemb to (ShibList.Count -1) do
          begin
            ShibObject:=ShibList.Items[ix1];
            with ShibObject do
            if LNo_Rishui = MisRishui then
            begin
              if (LBegPoint <= XRel) and
                 (LEndPoint >= XRel) then
              begin
                F_Graf.Dragging:=TRUE;
                if (XRel - LBegPoint) < (LEndPoint-LBegPoint)*0.25 then
                begin
                  DraggingType:=(-1);
                  Screen.Cursor:=crSizeWE;
                end
                else
                  if (XRel - LBegPoint) > (LEndPoint-LBegPoint)*0.75 then
                  begin
                    DraggingType:=1;
                    Screen.Cursor:=crSizeWE;
                  end
                  else
                  begin
                    DraggingType:=0;
                    Screen.Cursor:=crSize;
                  end;
                ShibMembStartDrag:=ix1;
                break;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TF_Graf.SGr_RightMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var ACol,ARow : longint;
    ix1 :longint;
    MisRishui : shortstring;
    ShibMemb  : longint;
    XRel,DeltaOld : real;
begin
  if (Dragging) and (Button = mbRight) then
  begin
    Dragging:=FALSE;
    XRel:=X / SGr_Right.Width;
    with SGr_Right do
    begin
      ARow:= SGr_Right.MouseCoord(X,Y).Y;
      ACol:= SGr_Right.MouseCoord(X,Y).X;
      if ARow > 0 then
      begin
        CarMembEndDrag:=ARow-1;
        CarObject:=CarList.Items[  CarMembEndDrag];
        with CarObject do
        begin
          MisRishui:=LNo_Rishui;
          if (CarMembEndDrag = CarMembStartDrag) and
             (ShibMembStartDrag <> (-1)) then
          begin
            ShibObject:=ShibList.Items[ShibMembStartDrag];
            with ShibObject do
            begin
              Screen.Cursor:=crDefault;
              if DraggingType=(-1) then  {Left}
              begin
                LBegPoint := XRel;
                LBegTime:= BegPoint + LBegPoint /(EndPoint - BegPoint);
              end;
              if DraggingType=(+1) then  {Right}
              begin
                LEndPoint := XRel;
                LEndTime:= EndPoint + LEndPoint /(EndPoint - BegPoint);
              end;
              if DraggingType=0 then  {Center}
              begin
                DeltaOld:=LEndPoint-LBegPoint;
                LBegPoint := XRel - DeltaOld/2 ;
                LBegTime:= BegPoint + LBegPoint /(EndPoint - BegPoint);
                LEndPoint := XRel + DeltaOld/2;
                LEndTime:= EndPoint + LEndPoint /(EndPoint - BegPoint);
              end;
              L_Status:=1;
              InitGrids;
            end;
          end;
        end;
      end;
    end;
    ShibMembStartDrag:=-1;
    ShibMembEndDrag:=-1;
    CarMembStartDrag:=-1;
    CarMembEndDrag:=-1;
  end;
end;

end.

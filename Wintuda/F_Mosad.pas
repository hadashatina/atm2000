unit F_Mosad;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, Mask, ComCtrls, DBTables, ExtCtrls,
  AtmAdvTable, AtmComp, ToolWin, AtmLookCombo, Scale250,MslDllInterface,
  Grids, DBGrids, RXDBCtrl, GSwitch, AdvSearch;

type
  TFrm_Mosad = class(TForm)
    Tbl_MosadSEMEL_MOSAD: TIntegerField;
    Tbl_MosadNAME_MOSAD: TStringField;
    Tbl_MosadSEMEL_YESHUV: TIntegerField;
    Tbl_MosadCITY_NAME: TStringField;
    Tbl_MosadSTREET: TStringField;
    Tbl_MosadHOUSE_NUM: TIntegerField;
    Tbl_MosadZIP_CODE: TIntegerField;
    Tbl_MosadCODE_SUG_CHINUH: TIntegerField;
    Tbl_MosadCODE_CALANDER: TIntegerField;
    Tbl_MosadPRINCIPLE_NAME: TStringField;
    Tbl_MosadPHONE_1: TStringField;
    Tbl_MosadPHONE_2: TStringField;
    Tbl_MosadFAX: TStringField;
    Tbl_MosadX: TIntegerField;
    Tbl_MosadY: TIntegerField;
    DS_Mosad: TDataSource;
    Panel2: TPanel;
    Tbl_Mosad: TAtmTable;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    Tbl_MosadMOSAD_MUKAR: TIntegerField;
    Tbl_MosadFROM_CLASS: TIntegerField;
    Tbl_MosadTO_CLASS: TIntegerField;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ToolButton1: TToolButton;
    TBtn_OpenSearch: TToolButton;
    Scale1: TScale;
    Tbl_MosadSUG_PIKUACH: TIntegerField;
    Tbl_MosadMOSAD_ACTIVE: TIntegerField;
    StatusBar1: TStatusBar;
    Tbl_MosadNodeNumB: TIntegerField;
    Tbl_MosadNodeSugSystem: TIntegerField;
    Tbl_MosadNodeKodCityName: TIntegerField;
    TS_SqlGridMosad: TTabSheet;
    RxDBGrid1: TRxDBGrid;
    Query_Mosad: TQuery;
    DS_QryMosad: TDataSource;
    Panel1: TPanel;
    Lbl_SemelMosad: TLabel;
    Lnl_NameMosad: TLabel;
    Lbl_SemelYeshuv: TLabel;
    Lbl_City: TLabel;
    Lbl_Street: TLabel;
    Lbl_HouseNum: TLabel;
    Lbl_zipCode: TLabel;
    Lbl_CodeSugChinuch: TLabel;
    Lbl_CodeCalendar: TLabel;
    Lbl_PrincName: TLabel;
    Lbl_Phone1: TLabel;
    Lbl_Phone2: TLabel;
    Lbl_Fax: TLabel;
    Lbl_X: TLabel;
    Lbl_Y: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EditSEMEL_MOSAD: TAtmDbHEdit;
    EditNAME_MOSAD: TAtmDbHEdit;
    EditSEMEL_YESHUV: TAtmDbHEdit;
    EditCITY_NAME: TAtmDbHEdit;
    EditSTREET: TAtmDbHEdit;
    EditHOUSE_NUM: TAtmDbHEdit;
    EditZIP_CODE: TAtmDbHEdit;
    EditPRINCIPLE_NAME: TAtmDbHEdit;
    EditPHONE_: TAtmDbHEdit;
    EditPHONE_2: TAtmDbHEdit;
    Edit_Fax: TAtmDbHEdit;
    EditX: TAtmDbHEdit;
    EditY: TAtmDbHEdit;
    DBRadioGroup1: TDBRadioGroup;
    AtmDbHEdit_SugPikuach: TAtmDbHEdit;
    AtmDbComboBox_CodeCalendar: TAtmDbComboBox;
    AtmDbComboBox_CodeSugChinuh: TAtmDbComboBox;
    AtmDbComboBox_FromClass: TAtmDbComboBox;
    AtmDbComboBox_ToClass: TAtmDbComboBox;
    DBRadioGroup2: TDBRadioGroup;
    AtmDbComboBox_SugPikuach: TAtmDbComboBox;
    Panel3: TPanel;
    Switch_MosadActive: TSwitch;
    Label4: TLabel;
    UpdateSQL1: TUpdateSQL;
    TBtn_MslChoosePoint: TToolButton;
    AtmAdvSearch_CityCode: TAtmAdvSearch;
    TBtn_Report: TToolButton;
    AtmAdvSearch_Mosad: TAtmAdvSearch;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MosadAfterInsert(DataSet: TDataSet);
    procedure Tbl_MosadAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure AtmDbHEdit_FromClassBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DS_MosadStateChange(Sender: TObject);
    procedure Tbl_MosadCITY_NAMEValidate(Sender: TField);
    procedure Tbl_MosadSTREETValidate(Sender: TField);
    procedure Tbl_MosadAfterScroll(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure RxDBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RxDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Switch_MosadActiveClick(Sender: TObject);
    procedure RxDBGrid1DblClick(Sender: TObject);
    procedure TBtn_MslChoosePointClick(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
  private
    { private declarations }
    RerutnToProc:TRerutnToProc;
    OkToUpdateNode :Boolean; //Put OkToUpdateNode:=False; AfterPost And AfterScroll Events
    CurSortFieldName :String;
    procedure ThreadCheckAddressDone(Sender :TObject);
    procedure RunThreadToCheckAddress;
    Procedure PutNodeDataInRecord(TheNode :TNode);
  public
    { public declarations }
    Procedure BuildLookupList;
    procedure RunQryMosad(OrderByFieldName :String);
    Procedure ShowForm(FormParams:ShortString);
  end;

var
  Frm_Mosad: TFrm_Mosad;

implementation
Uses Crt_Glbl,DMAtmCrt,AtmRutin,AtmConst;
{$R *.DFM}

procedure TFrm_Mosad.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
     Screen.Cursor:=crHourGlass;
     TBtn_MslChoosePoint.Enabled:=MslDllLoaded;
     BuildLookupList;

     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;
{     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_DargatClass),Tbl_MosadLookClassFrom);
     SList.AddObject(IntToStr(SugTavla_DargatClass),Tbl_MosadLookClassTo);
     SList.AddObject(IntToStr(SugTavla_SugHinuch),Tbl_MosadLookSugHinuch);
     SList.AddObject(IntToStr(SugTavla_SugCalendar),Tbl_MosadLookSugCalendar);
     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
}

     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
     PageControl1.ActivePage:=TS_Main;
  Finally
         Screen.Cursor:=crDefault;
  End;

end;

procedure TFrm_Mosad.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_Mosad.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Mosad);
end;

procedure TFrm_Mosad.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if (Button=nbPost) And ((Sender As TDBNavigator).DataSource.DataSet Is TAtmTable) Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet As TAtmTable);
     if Button=nbRefresh Then
        BuildLookupList;
end;

procedure TFrm_Mosad.Tbl_MosadAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_Mosad.Tbl_MosadAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     OkToUpdateNode:=False;
end;

procedure TFrm_Mosad.FormShow(Sender: TObject);
begin
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_Mosad.AtmDbHEdit_FromClassBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
{     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_DargatClass;}
end;

procedure TFrm_Mosad.DS_MosadStateChange(Sender: TObject);
begin
     DM_AtmCrt.Action_OpenSearchUpdate(TBtn_OpenSearch.Action);
end;

Procedure TFrm_Mosad.BuildLookupList;
Var
   SList:TStringList;
Begin
     SList:=TStringList.Create;

     SList.AddObject(IntToStr(SugTavla_DargatClass),AtmDbComboBox_ToClass);
     SList.AddObject(IntToStr(SugTavla_DargatClass),AtmDbComboBox_FromClass);
     SList.AddObject(IntToStr(SugTavla_SugHinuch),AtmDbComboBox_CodeSugChinuh);
     SList.AddObject(IntToStr(SugTavla_SugCalendar),AtmDbComboBox_CodeCalendar);
     SList.AddObject(IntToStr(SugTavla_SugPikuach),AtmDbComboBox_SugPikuach);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;

procedure TFrm_Mosad.RunThreadToCheckAddress;
Begin
   if (Not ThreadCheckAddressEnded) And (ThreadCheckAddress<>Nil) Then
   Begin
        ThreadCheckAddress.Terminate;
        RerutnToProc:=RunThreadToCheckAddress;
        StatusBar1.SimpleText:='���� ������ ����� ����';
        Exit;
   End;
   ThreadCheckAddress:= TThreadCheckAddress.Create(EditCITY_NAME.Text,EditSTREET.Text,EditHOUSE_NUM.Text);
   ThreadCheckAddress.OnTerminate := ThreadCheckAddressDone;
   StatusBar1.SimpleText:='���� ����� ��������...';
   OkToUpdateNode:=True;
   RerutnToProc:=Nil;
End;

procedure TFrm_Mosad.ThreadCheckAddressDone(Sender :TObject);
Begin
     if (ThreadCheckAddressEnded) And (ThreadCheckAddressRes<>faCityNotFound) Then
     Begin
          StatusBar1.SimpleText:='����� ����� '+ThreadCheckAddressReturnNode.NameOfPoint;
          if OkToUpdateNode Then
             PutNodeDataInRecord(ThreadCheckAddressReturnNode);
     End
     Else
     Begin
        if ThreadCheckAddressRes=faLibraryNotLoaded Then
           StatusBar1.SimpleText:=''
        Else
        Begin
            StatusBar1.SimpleText:='����� �� ����� ��������';
            Beep(200,100);;
        End;
     End;
     ThreadCheckAddress:=Nil;
     if @RerutnToProc<>Nil Then RerutnToProc;
End;

Procedure TFrm_Mosad.PutNodeDataInRecord(TheNode :TNode);
Begin
    Tbl_MosadX.AsInteger:=TheNode.Crd.X;
    Tbl_MosadY.AsInteger:=TheNode.Crd.Y;
    Tbl_MosadNodeNumB.AsInteger:=TheNode.Numb;
    Tbl_MosadNodeSugSystem.AsInteger:=TheNode.SugSystem;
    Tbl_MosadNodeKodCityName.AsInteger:=TheNode.KodCityName;
End;

procedure TFrm_Mosad.Tbl_MosadCITY_NAMEValidate(Sender: TField);
begin
     if Not Tbl_MosadCITY_NAME.IsNull Then
        RunThreadToCheckAddress;
end;

procedure TFrm_Mosad.Tbl_MosadSTREETValidate(Sender: TField);
begin
     if (Not Tbl_MosadCITY_NAME.IsNull) And (Not Tbl_MosadSTREET.IsNull)
        And (Not Tbl_MosadHOUSE_NUM.IsNull) Then
        RunThreadToCheckAddress;
end;

procedure TFrm_Mosad.Tbl_MosadAfterScroll(DataSet: TDataSet);
begin
     OkToUpdateNode:=False;
end;



procedure TFrm_Mosad.PageControl1Change(Sender: TObject);
begin
     if PageControl1.ActivePage=TS_SqlGridMosad Then
     Begin
          RunQryMosad('SEMEL_MOSAD');
          DBNavigator.DataSource:=DS_QryMosad;
     End
     Else
     Begin
         if Query_Mosad.Active Then
            Query_Mosad.Active:=False;
         DBNavigator.DataSource:=DS_Mosad;
     End;

end;

procedure TFrm_Mosad.RunQryMosad(OrderByFieldName :String);
Begin
  Try
     Screen.Cursor:=crHourGlass;
      CurSortFieldName:=OrderByFieldName;
      Query_Mosad.DisableControls;
      With Query_Mosad Do
      Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From '+Tbl_Mosad.TableName);
           if Switch_MosadActive.Checked Then
              Sql.Add('Where MOSAD_ACTIVE = 1');
           Sql.Add('Order By '+OrderByFieldName);
           Active:=True;
      End;
  Finally
         Query_Mosad.EnableControls;
         Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_Mosad.RxDBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     if CompareText(Field.FieldName,CurSortFieldName)=0 Then
        Background:=clInfoBk;
end;

procedure TFrm_Mosad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Mosad:=Nil;
     Action:=caFree;
end;

procedure TFrm_Mosad.RxDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
     RunQryMosad(Field.FieldName);
end;

Procedure TFrm_Mosad.ShowForm(FormParams:ShortString);
Begin
     if FormParams<>'' Then
          Tbl_Mosad.FindKey([FormParams]);
     Show;
End;

procedure TFrm_Mosad.Switch_MosadActiveClick(Sender: TObject);
begin
    RunQryMosad(CurSortFieldName);
end;

procedure TFrm_Mosad.RxDBGrid1DblClick(Sender: TObject);
begin
     if Tbl_Mosad.FindKey([Query_Mosad.FieldByName('SEMEL_MOSAD').Value]) Then
     Begin
        PageControl1.ActivePage:=TS_Main;
        DBNavigator.DataSource:=DS_Mosad;
     End;
end;

procedure TFrm_Mosad.TBtn_MslChoosePointClick(Sender: TObject);
Var
   TheNode :TNode;
begin
     if Not MslDllLoaded Then
        Exit;
     StatusBar1.SimpleText:='���� �������...';
     Application.ProcessMessages;
     if (Tbl_MosadX.AsInteger>0) And (Tbl_MosadY.AsInteger>0) Then
        MslGotoXY(Tbl_MosadX.AsInteger,Tbl_MosadY.AsInteger);
     Application.ProcessMessages;
     if MslChoosePoint(TheNode) Then
     Begin
          if Tbl_Mosad.State=dsBrowse Then
              Tbl_Mosad.Edit;
          StatusBar1.SimpleText:=TheNode.NameOfPoint;
          PutNodeDataInRecord(TheNode);
     End;
     StatusBar1.SimpleText:='';
end;

procedure TFrm_Mosad.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msMosad],False,CrtMeholelHandle);

end;

end.

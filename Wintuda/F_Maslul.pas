unit F_Maslul;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,Dialogs,
  StdCtrls, Forms, DBCtrls, DB, DBTables, Mask, ExtCtrls, AtmComp, ComCtrls,
  AtmAdvTable, Scale250, ToolWin, Menus, GIFImage,Registry, Buttons,
  ToolEdit, AtmLookCombo, ActnList,MslDllInterface, AdvSearch,
  DynamicControls, DBFilter, RxQuery, AtmTabSheetBuild, AtmRxQuery;

type
  TFrm_Maslul = class(TForm)
    DS_Maslul: TDataSource;
    Panel2: TPanel;
    Tbl_Maslul: TAtmRxQuery;
    PageControl_Maslul: TPageControl;
    TS_Maslul: TTabSheet;
    TS_Picture: TTabSheet;
    Scale1: TScale;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    Panel1: TPanel;
    OpenDialog1: TOpenDialog;
    AtmDbHEdit1: TAtmDbHEdit;
    Spb_OpenFile: TSpeedButton;
    Label7: TLabel;
    DirectoryEdit1: TDirectoryEdit;
    Panel_Main: TPanel;
    Lbl_CodeMaslul: TLabel;
    Lbl_Group: TLabel;
    Lbl_Name: TLabel;
    Lbl_Motza: TLabel;
    Lbl_Yaad: TLabel;
    Lbl_Pratim: TLabel;
    Lbl_Distance: TLabel;
    Lbl_time: TLabel;
    Lbl_HourStart: TLabel;
    Lbl_HourEnd: TLabel;
    EditCODE_MASLUL: TAtmDbHEdit;
    EditGROUP: TAtmDbHEdit;
    EditNAMEMaslul: TAtmDbHEdit;
    EditMOTZA: TAtmDbHEdit;
    EditYAAD: TAtmDbHEdit;
    EditPRATIM: TAtmDbHEdit;
    EditDISTANCE_MASLUL: TAtmDbHEdit;
    EditTIME_MASLUL: TAtmDbHEdit;
    EditHOUR_START: TAtmDbHEdit;
    EditHOUR_END: TAtmDbHEdit;
    GB_Days: TGroupBox;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    AtmDbComboBox_SugMaslul: TAtmDbComboBox;
    Lbl_SugMaslul: TLabel;
    AtmDbComboBox_Ifyun: TAtmDbComboBox;
    Lbl_Ifun: TLabel;
    Tbl_MaslulCODE_MASLUL: TIntegerField;
    Tbl_MaslulCODE_PIZUL: TIntegerField;
    Tbl_MaslulCODE_TKUFA: TIntegerField;
    Tbl_MaslulCODE_KAV: TIntegerField;
    Tbl_MaslulGROUP_MASLUL: TIntegerField;
    Tbl_MaslulCODE_CLIENT: TIntegerField;
    Tbl_MaslulNAME: TStringField;
    Tbl_MaslulMOTZA: TStringField;
    Tbl_MaslulYAAD: TStringField;
    Tbl_MaslulPRATIM: TStringField;
    Tbl_MaslulSUG_MASLUL: TIntegerField;
    Tbl_MaslulDISTANCE_MASLUL: TBCDField;
    Tbl_MaslulTIME_MASLUL: TIntegerField;
    Tbl_MaslulKAMUT: TBCDField;
    Tbl_MaslulHOUR_START: TDateTimeField;
    Tbl_MaslulHOUR_END: TDateTimeField;
    Tbl_MaslulDATE_START_MASLUL: TDateTimeField;
    Tbl_MaslulDATE_END_MASLUL: TDateTimeField;
    Tbl_MaslulIFYUN: TIntegerField;
    Tbl_MaslulSUG_PIKUACH: TIntegerField;
    Tbl_MaslulHIUV_MISRAD_HINUCH: TIntegerField;
    Tbl_MaslulSUG_HINUCH: TIntegerField;
    Tbl_MaslulTEUR_MUGBAL: TStringField;
    Tbl_MaslulCODE_LUACH: TIntegerField;
    Tbl_MaslulSCHOOL_YEAR: TIntegerField;
    Tbl_MaslulDAY1: TIntegerField;
    Tbl_MaslulDAY2: TIntegerField;
    Tbl_MaslulDAY3: TIntegerField;
    Tbl_MaslulDAY4: TIntegerField;
    Tbl_MaslulDAY5: TIntegerField;
    Tbl_MaslulDAY6: TIntegerField;
    Tbl_MaslulDAY7: TIntegerField;
    Tbl_MaslulPRICE_GLOBAL: TBCDField;
    Tbl_MaslulPRICE_PER_HOUR: TBCDField;
    Tbl_MaslulPRICE_KM: TBCDField;
    Tbl_MaslulMAP_FILENAME: TStringField;
    Tbl_MaslulCODE_MEHIRON: TIntegerField;
    Tbl_MaslulTOTAL_ADULT: TIntegerField;
    Tbl_MaslulTOTAL_YOUNG: TIntegerField;
    Tbl_MaslulTOTAL_TEACHER: TIntegerField;
    Tbl_MaslulTOTAL_MUSAIM: TIntegerField;
    Tbl_MaslulSUG_REHEV1: TIntegerField;
    Tbl_MaslulSUG_REHEV2: TIntegerField;
    Tbl_MaslulSUG_REHEV3: TIntegerField;
    Tbl_MaslulKAMUT_REHEV1: TIntegerField;
    Tbl_MaslulKAMUT_REHEV2: TIntegerField;
    Tbl_MaslulKAMUT_REHEV3: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL1: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL2: TIntegerField;
    Tbl_MaslulSUG_REHEV_ACTUAL3: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL1: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL2: TIntegerField;
    Tbl_MaslulKAMUT_REHEV_ACTUAL3: TIntegerField;
    Tbl_MaslulSTRING_1: TStringField;
    Tbl_MaslulSTRING_2: TStringField;
    Tbl_MaslulSTRING_3: TStringField;
    Tbl_MaslulSTRING_4: TStringField;
    Tbl_MaslulSTRING_5: TStringField;
    Tbl_MaslulSTRING_6: TStringField;
    Tbl_MaslulSTRING_7: TStringField;
    Tbl_MaslulSTRING_8: TStringField;
    Tbl_MaslulSTRING_9: TStringField;
    Tbl_MaslulSTRING_10: TStringField;
    Tbl_MaslulBCD_1: TBCDField;
    Tbl_MaslulBCD_2: TBCDField;
    Tbl_MaslulBCD_3: TBCDField;
    Tbl_MaslulBCD_4: TBCDField;
    Tbl_MaslulBCD_5: TBCDField;
    Tbl_MaslulBCD_6: TBCDField;
    Tbl_MaslulBCD_7: TBCDField;
    Tbl_MaslulBCD_8: TBCDField;
    Tbl_MaslulBCD_9: TBCDField;
    Tbl_MaslulBCD_10: TBCDField;
    Tbl_MaslulMaslulAzmnNo: TIntegerField;
    Tbl_MaslulMaslulCarNo: TIntegerField;
    Tbl_MaslulMaslulDriverNo: TIntegerField;
    Tbl_MaslulPricePerMusa: TBCDField;
    Tbl_MaslulPriceConst: TBCDField;
    Tbl_MaslulPayedPrice: TBCDField;
    Tbl_MaslulXBegin: TIntegerField;
    Tbl_MaslulYBegin: TIntegerField;
    Tbl_MaslulNodeNumBBegin: TIntegerField;
    Tbl_MaslulNodeSugSystemBegin: TIntegerField;
    Tbl_MaslulNodeKodCityNameBegin: TIntegerField;
    Tbl_MaslulXEnd: TIntegerField;
    Tbl_MaslulYEnd: TIntegerField;
    Tbl_MaslulNodeNumBEnd: TIntegerField;
    Tbl_MaslulNodeSugSystemEnd: TIntegerField;
    Tbl_MaslulNodeKodCityNameEnd: TIntegerField;
    Tbl_MaslulMaslulForMhiron: TIntegerField;
    Tbl_MaslulDays: TStringField;
    Tbl_MaslulMaxMusa: TIntegerField;
    Tbl_MaslulRem: TMemoField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    Action_ChooseFromMaslulon: TAction;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    TBtn_DistanceFromMsl: TToolButton;
    DBText1: TDBText;
    Tbl_MaslulLookKvutza: TStringField;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    DynamicControls1: TDynamicControls;
    Lbl_MaslulForMehiron: TLabel;
    AtmDbHEdit_MaslulForMehiron: TAtmDbHEdit;
    Tbl_MaslulLookup: TAtmRxQuery;
    Tbl_MaslulLookMaslulName: TStringField;
    DBText_LookMaslul: TDBText;
    TBtn_Report: TToolButton;
    GIFImage1: TGIFImage;
    UpdateSQL_Maslul: TUpdateSQL;
    TS_ExtraFields: TTabSheet;
    N2: TMenuItem;
    Act_ShowMslMap: TAction;
    SpeedButton1: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure EditHOUR_ENDDblClick(Sender: TObject);
    procedure DBRG_IfyunEnter(Sender: TObject);
    procedure DBRG_IfyunExit(Sender: TObject);
    procedure GB_DaysEnter(Sender: TObject);
    procedure GB_DaysExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MaslulAfterPost(DataSet: TDataSet);
    procedure Tbl_MaslulAfterInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PageControl_MaslulChange(Sender: TObject);
    procedure Tbl_MaslulAfterScroll(DataSet: TDataSet);
    procedure Spb_OpenFileClick(Sender: TObject);
    procedure DirectoryEdit1AfterDialog(Sender: TObject; var Name: String;
      var Action: Boolean);
    procedure EditCODE_MASLULExit(Sender: TObject);
    procedure Action_ChooseFromMaslulonExecute(Sender: TObject);
    procedure Action_ChooseFromMaslulonUpdate(Sender: TObject);
    procedure EditMOTZAEnter(Sender: TObject);
    procedure TBtn_DistanceFromMslClick(Sender: TObject);
    procedure DS_MaslulStateChange(Sender: TObject);
    procedure EditGROUPBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure EditNAMEMaslulEnter(Sender: TObject);
    procedure EditNAMEMaslulKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Tbl_MaslulValidateLookupField(Sender: TObject;
      AField: TField; var IsValid: Boolean);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure Tbl_MaslulBeforePost(DataSet: TDataSet);
    procedure Act_ShowMslMapExecute(Sender: TObject);
  private
    { private declarations }
    Procedure CalcAllMaslul;
    Procedure ShowMapImage;
    Procedure PutNodeDataInRecord(TheNode :TNode);
  public
    { public declarations }
    Procedure ShowForm(FormParams:ShortString);
    Procedure BuildLookupList;
  end;

var
  Frm_Maslul: TFrm_Maslul;

implementation
Uses Crt_Glbl,AtmRutin,AtmConst, DMAtmCrt;
{$R *.DFM}
Const
     PnlNum_DSState=0;

procedure TFrm_Maslul.FormCreate(Sender: TObject);
var
   i:longint;
Begin
  Try
     Screen.Cursor:=crHourGlass;
     PageControl_Maslul.ActivePage:=TS_Maslul;
     DirectoryEdit1.InitialDir:=CrtMapImagePath;
     DirectoryEdit1.Text:=CrtMapImagePath;

     DynamicControls1.ExtraFiledsFileName:=CrtDirForScripts+DynamicControls1.ExtraFiledsFileName;
     DynamicControls1.Execute;
     BuildLookupList;
     Scale1.DoScaleNow;

     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataSet) And (Components[I].Tag=1)Then
             (Components[I] As TDataSet).Open
          Else
          if Components[i] is TAtmAdvSearch Then
                  TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
  Finally
     Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_Maslul.EditHOUR_ENDDblClick(Sender: TObject);
begin
     SelectFirst;
end;

procedure TFrm_Maslul.DBRG_IfyunEnter(Sender: TObject);
begin
     (Sender As TDBRadioGroup).Font.Color:=clActiveCaption;
end;

procedure TFrm_Maslul.DBRG_IfyunExit(Sender: TObject);
begin
     (Sender As TDBRadioGroup).Font.Color:=clBlack;
end;

procedure TFrm_Maslul.GB_DaysEnter(Sender: TObject);
begin
     (Sender As TGroupBox).Font.Color:=clActiveCaption;
end;

procedure TFrm_Maslul.GB_DaysExit(Sender: TObject);
begin
     (Sender As TGroupBox).Font.Color:=clBlack;
end;

procedure TFrm_Maslul.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataSet).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_Maslul.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Maslul);
end;

procedure TFrm_Maslul.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Button=nbPost Then
        DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet);
     if Button=nbRefresh Then
     Begin
        (Sender As TDBNavigator).DataSource.DataSet.Close;
        (Sender As TDBNavigator).DataSource.DataSet.Open;
        BuildLookupList;
        Abort;
     End;
end;

procedure TFrm_Maslul.Tbl_MaslulAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_Maslul.Tbl_MaslulAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_Maslul.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Maslul:=Nil;
     Action:=caFree;
end;

Procedure TFrm_Maslul.ShowForm(FormParams:ShortString);
Begin
     Show;
     if FormParams<>'' Then
          Tbl_Maslul.FindKey([FormParams])
     Else
//         Tbl_Maslul.Insert;
       SetDatasetState(Tbl_Maslul,CrtFirstTableState);
End;

Procedure TFrm_Maslul.ShowMapImage;
Var
   StrH,FileExt :String;

Begin
  Image1.Visible:=False;
  GifImage1.Visible:=False;
  StrH:=Tbl_Maslul.FieldByName('MAP_FILENAME').AsString;
  StrH:=CrtMapImagePath+StrH;
  if FileExists(StrH) Then
  Begin
      FileExt:=ExtractFileExtention(StrH);
      FileExt:=UPPERCASE(FileExt);
      if FileExt='BMP' Then
      Begin
           Image1.Picture.LoadFromFile(StrH);
           Image1.Visible:=True;
      End
      Else
          if (FileExt='GIF') Or (FileExt='MSL') Then
          Begin
               GifImage1.LoadFromFile(StrH);
               GifImage1.Visible:=True;
          End;
  End;
End;



procedure TFrm_Maslul.PageControl_MaslulChange(Sender: TObject);
begin
     if PageControl_Maslul.ActivePage=TS_Picture Then
        ShowMapImage;
end;

procedure TFrm_Maslul.Tbl_MaslulAfterScroll(DataSet: TDataSet);
begin
     if PageControl_Maslul.ActivePage=TS_Picture Then
        ShowMapImage;

end;

procedure TFrm_Maslul.Spb_OpenFileClick(Sender: TObject);
Var
   StrH :String;
begin
     OpenDialog1.InitialDir:=CrtMapImagePath;
     if OpenDialog1.Execute Then
     Begin
          StrH:=ExtractFilePath(OpenDialog1.FileName);
          if CompareText(StrH,CrtMapImagePath)<>0 Then
          Begin
             ShowMessage('�� �������� ����� ���� �� ������ �����');
             Exit;
          End
          Else
          Begin
               if Tbl_Maslul.State=dsBrowse Then
                  Tbl_Maslul.Edit;
               Tbl_Maslul.FieldByName('MAP_FILENAME').AsString:=ExtractFileName(OpenDialog1.FileName);
               ShowMapImage;
          End;
     End;
end;

procedure TFrm_Maslul.DirectoryEdit1AfterDialog(Sender: TObject;
  var Name: String; var Action: Boolean);
begin
    WriteMapImagePath(Name);
    CrtMapImagePath:=ReadMapImagePath;
    DirectoryEdit1.InitialDir:=CrtMapImagePath;
    DirectoryEdit1.Text:=CrtMapImagePath;
end;

procedure TFrm_Maslul.EditCODE_MASLULExit(Sender: TObject);
begin
  Try
     if Tbl_Maslul.State<>dsInsert Then
        Exit;
     Screen.Cursor:=crHourGlass;
     if EditCODE_MASLUL.Text='' Then
     Begin
          DM_AtmCrt.Qry_Temp.Active:=False;
          With DM_AtmCrt.Qry_Temp.Sql Do
          Begin
               Clear;
               Add('Select Max('+EditCODE_MASLUL.DataField+') MaxCode');
               Add('From MASLUL');
          End;
          DM_AtmCrt.Qry_Temp.Active:=True;
          Tbl_Maslul.FieldByName(EditCODE_MASLUL.DataField).AsInteger:=DM_AtmCrt.Qry_Temp.FieldByName('MaxCode').AsInteger+1;
     End;
  Finally
         DM_AtmCrt.Qry_Temp.Active:=False;
         Screen.Cursor:=crDefault;
  End;
end;

Procedure TFrm_Maslul.BuildLookupList;
Var
   SList:TStringList;
   I :longInt;
Begin
     SList:=TStringList.Create;
     Try
         SList.AddObject(IntToStr(SugTavla_GroupMaslul),Tbl_MaslulLookKvutza);
         FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);

         SList.Clear;
         SList.AddObject(IntToStr(SugTavla_SugMaslul),AtmDbComboBox_SugMaslul);
         SList.AddObject(IntToStr(SugTavla_IfyunMaslul),AtmDbComboBox_Ifyun);
         For I:=0 To ComponentCount-1 Do
         Begin
            if Components[i] is TAtmDbComboBox Then
              if TAtmDbComboBox(Components[i]).SugTavlaIndex>0 Then
                SList.AddObject(IntToStr(TAtmDbComboBox(Components[i]).SugTavlaIndex),Components[i]);
         End;
         FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     Finally
            SList.Free;
     End;
End;

Procedure TFrm_Maslul.PutNodeDataInRecord(TheNode :TNode);
Begin
    if ActiveControl=EditMOTZA Then
    Begin
         Tbl_MaslulXBegin.AsInteger:=TheNode.Crd.X;
         Tbl_MaslulYBegin.AsInteger:=TheNode.Crd.Y;
         Tbl_MaslulNodeNumBBegin.AsInteger:=TheNode.Numb;
         Tbl_MaslulNodeSugSystemBegin.AsInteger:=TheNode.SugSystem;
         Tbl_MaslulNodeKodCityNameBegin.AsInteger:=TheNode.KodCityName;
    End
    ELse
    Begin
         Tbl_MaslulXEnd.AsInteger:=TheNode.Crd.X;
         Tbl_MaslulYEnd.AsInteger:=TheNode.Crd.Y;
         Tbl_MaslulNodeNumBEnd.AsInteger:=TheNode.Numb;
         Tbl_MaslulNodeSugSystemEnd.AsInteger:=TheNode.SugSystem;
         Tbl_MaslulNodeKodCityNameEnd.AsInteger:=TheNode.KodCityName;
    End;
End;

procedure TFrm_Maslul.Action_ChooseFromMaslulonExecute(Sender: TObject);
Var
   TheNode :TNode;
begin
     if Not MslDllLoaded Then
        Exit;
     if MslFindNodeFromDlgSearchPlace(TheNode,'') Then
     Begin
          if Tbl_Maslul.State=dsBrowse Then
             Tbl_Maslul.Edit;
          if ActiveControl=EditMOTZA Then
             Tbl_MaslulMOTZA.AsString:=TheNode.NameOfPoint
          Else
             Tbl_MaslulYAAD.AsString:=TheNode.NameOfPoint;

          PutNodeDataInRecord(TheNode);
     End;
end;

procedure TFrm_Maslul.Action_ChooseFromMaslulonUpdate(Sender: TObject);
begin
     Action_ChooseFromMaslulon.Enabled:=(ActiveControl=EditMOTZA) Or (ActiveControl=EditYAAD);
end;

procedure TFrm_Maslul.EditMOTZAEnter(Sender: TObject);
begin
     Action_ChooseFromMaslulonUpdate(Sender);
end;

procedure TFrm_Maslul.TBtn_DistanceFromMslClick(Sender: TObject);
Var
   TempNode :TNode;
   TempNodeObj :TNodeClass;
   ListOfNodes :TList;
   Dist,Zman :DWord;
begin
     if (Tbl_MaslulNodeNumBBegin.IsNull) Or (Tbl_MaslulNodeNumBBegin.AsInteger<=0) Then
     Begin
          ShowMessage('���� ���� �� ����');
          Exit;
     End;
     if (Tbl_Maslul.FieldByName('NodeNumBEnd').IsNull) Or (Tbl_Maslul.FieldByName('NodeNumBEnd').AsInteger<=0) Then
     Begin
          ShowMessage('���� ��� �� ����');
          Exit;
     End;

     Try
        ListOfNodes:=TList.Create;
       //����
       TempNode.Numb:=Tbl_MaslulNodeNumBBegin.AsInteger;
       TempNode.SugSystem:=Tbl_MaslulNodeSugSystemBegin.AsInteger;
       TempNode.KodCityName:=Tbl_MaslulNodeKodCityNameBegin.AsInteger;
       TempNode.Crd.X:=Tbl_MaslulXBegin.AsInteger;
       TempNode.Crd.Y:=Tbl_MaslulYBegin.AsInteger;
       TempNodeObj:=TNodeClass.Create(TempNode);
       ListOfNodes.Add(TempNodeObj);
       //���
       TempNode.Numb:=Tbl_Maslul.FieldByName('NodeNumBEnd').AsInteger;
       TempNode.SugSystem:=Tbl_Maslul.FieldByName('NodeSugSystemEnd').AsInteger;
       TempNode.KodCityName:=Tbl_Maslul.FieldByName('NodeKodCityNameEnd').AsInteger;
       TempNode.Crd.X:=Tbl_Maslul.FieldByName('XEnd').AsInteger;
       TempNode.Crd.Y:=Tbl_Maslul.FieldByName('YEnd').AsInteger;
       TempNodeObj:=TNodeClass.Create(TempNode);
       ListOfNodes.Add(TempNodeObj);
       if MslCalcMaslul(ListOfNodes,Dist,Zman,'�') Then
       Begin
          if Tbl_Maslul.State=dsBrowse Then
             Tbl_Maslul.EDit;
          Tbl_MaslulDISTANCE_MASLUL.AsFloat:=Dist/1000;
       End
       Else
           ShowMessage('����� ����');
     Finally
         //Free TList
         While ListOfNodes.Count>0 Do
         Begin
              TempNodeObj:=TNodeClass(ListOfNodes.Items[0]);
              ListOfNodes.Delete(0);
              TempNodeObj.Free;
         End;
         ListOfNodes.Free;
     End;

end;

procedure TFrm_Maslul.DS_MaslulStateChange(Sender: TObject);
begin
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

procedure TFrm_Maslul.EditGROUPBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_GroupMaslul;
end;

procedure TFrm_Maslul.EditNAMEMaslulEnter(Sender: TObject);
begin
  if EditNAMEMaslul.DataSource.DataSet.State=dsInsert Then
    EditNAMEMaslul.Text:=EditMOTZA.Text+' '+EditYAAD.Text;
end;

procedure TFrm_Maslul.EditNAMEMaslulKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key=VK_F5 Then
     Begin
        if EditNAMEMaslul.DataSource.DataSet.State=dsBrowse Then
           EditNAMEMaslul.DataSource.DataSet.Edit;
        EditNAMEMaslul.Text:=EditMOTZA.Text+' '+EditYAAD.Text;
     End;
end;


procedure TFrm_Maslul.Tbl_MaslulValidateLookupField(Sender: TObject;
  AField: TField; var IsValid: Boolean);
begin
     if (AField=Tbl_MaslulLookMaslulName) And (AField.AsString='') Then
        IsValid:=True
end;

procedure TFrm_Maslul.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msMaslul],False,CrtMeholelHandle);
end;

procedure TFrm_Maslul.Tbl_MaslulBeforePost(DataSet: TDataSet);
begin
   if Trim(Tbl_Maslul.FieldByName('CODE_MASLUL').AsString)='' Then
   Begin
     MessageDlg('��� ����� ���� ���� ���',mtError,[mbOk],0);
     Abort;
   End;
  if EditNAMEMaslul.DataSource.DataSet.State=dsInsert Then
    EditNAMEMaslul.Text:=EditMOTZA.Text+' '+EditYAAD.Text;

end;

procedure TFrm_Maslul.Act_ShowMslMapExecute(Sender: TObject);
begin
  CalcAllMaslul;
  MslShowLastMaslul;
end;

Procedure TFrm_Maslul.CalcAllMaslul;
var
  VarNode:TNode;
  TempNodeObj :TNodeClass;
  ListOfNodes :TList;
  Dist,Zman:DWord;
  FindNode:boolean;
begin
   begin
     ListOfNodes:=TList.Create;
     FillChar(VarNode,SizeOf(VarNode),#0);
     TRY
        Begin
           with VarNode,Tbl_Maslul do
            begin
              Numb:=FieldByName('NodeNumBBegin').Asinteger;
              Crd.X:=FieldByName('XBegin').AsInteger;
              Crd.Y:=FieldByName('YBegin').AsInteger;
              if (Numb <= 0) and (Crd.X > 0) and (Crd.Y > 0) then
               FindNode:=MslFindNodeFromCrd(Crd.X,Crd.Y,VarNode);
              SugSystem:=FieldByName('NodeSugSystemBegin').AsInteger;
              KodCityName :=FieldByName('NodeKodCityNameBegin').AsInteger;
              TempNodeObj:=TNodeClass.Create(VarNode);
              ListOfNodes.Add(TempNodeObj);

              Numb:=FieldByName('NodeNumBEnd').Asinteger;
              Crd.X:=FieldByName('XEnd').AsInteger;
              Crd.Y:=FieldByName('YEnd').AsInteger;
              if (Numb <= 0) and (Crd.X > 0) and (Crd.Y > 0) then
               FindNode:=MslFindNodeFromCrd(Crd.X,Crd.Y,VarNode);
              SugSystem:=FieldByName('NodeSugSystemEnd').AsInteger;
              KodCityName :=FieldByName('NodeKodCityNameEnd').AsInteger;
              TempNodeObj:=TNodeClass.Create(VarNode);
              ListOfNodes.Add(TempNodeObj);
            end;
        end;
     Finally
       if Not(MslCalcMaslul(ListOfNodes,Dist,Zman,'�')) Then
        ShowMessage('����� ����');
   //Free TList
       While ListOfNodes.Count>0 Do
        Begin
          TempNodeObj:=TNodeClass(ListOfNodes.Items[0]);
          ListOfNodes.Delete(0);
          TempNodeObj.Free;
        End;
       ListOfNodes.Free;
     End;
   End;
end;
end.

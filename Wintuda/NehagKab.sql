Select H.hesbonitNumber ,
       H.HesbonitKind ,
       H.CodeLakoach,
       H.hesbonitDate,
       H.TotalHesbonitWithMAM ,H.Mamhesbonit,
       H.mampercent , H.PratimToHan ,
       H.YehusMonth ,h.codehaavaratohan ,
       H.YehusYear,
       H.TotalSumFromTnua,
       H.TotalSumPriceKamut,
       H.DatePiraon,
       H.DateForFirstPayment,
       H.MisHaavaraToHan,
       H.Incom_Type,
       H.HanForLak,H.HanForMam,
       H.String_1,H.TotalMasMakor,
       H.Status,
       H.Mis_Kabala1,H.OrderNum,
       H.Bcd_1,H.Bcd_2,

       N.Mis_Han,
       N.Shem_nehag As Shem_Lakoach

from
  MakavNhg  H ,
  Nehag  N

where (H.HesbonitKind In(11,15))
      And
      (H.CodeLakoach = N.Kod_Nehag)
      And
      (H.codeLakoach >= :PSapF)
      and
      (H.codeLakoach <= :PSapT)
      and
      (H.hesbonitNumber >= :PtnuF)
      and
      (H.hesbonitNumber <= :Ptnut)
      And
      (H.YehusYear = :PShnatmas)
      and
      (H.hesbonitDate >= :PdateF)
      and
      (H.hesbonitDate <= :Pdatet)
      and
      (H.TotalHesbonitWithMAM<>0)
      and
( (
      (H.CodeHaavaraToHan = 0)
      And
      (H.Status <> 9)
   )
OR
   (
      (H.Status = 9)
      And
      (H.CodeHaavaraToHan = 2)
    )
)

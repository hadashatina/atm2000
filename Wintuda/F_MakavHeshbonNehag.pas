unit F_MakavHeshbonNehag;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, Forms, DBCtrls, DB, ToolWin, ComCtrls, DBTables, Mask, ExtCtrls,
  AtmAdvTable, AtmComp, Scale250, AtmLookCombo, AdvSearch, Grids, DBGrids,
  RXDBCtrl, ToolEdit, DBFilter, RxQuery, AtmTabSheetBuild, AtmDBDateEdit,
  Dialogs;

type
  TFrm_MakavHeshbonNehag = class(TForm)
    DS_Makav: TDataSource;
    Tbl_Makav: TAtmTable;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    PageControl1: TPageControl;
    TS_Main: TTabSheet;
    TS_Zikuy: TTabSheet;
    Panel2: TPanel;
    Scale1: TScale;
    Tbl_Nehag: TAtmTable;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    TS_GridMakav: TTabSheet;
    RxDBGrid_Makav: TRxDBGrid;
    Query_MakavNehag: TQuery;
    DS_MakavNehag: TDataSource;
    Panel3: TPanel;
    Label11: TLabel;
    Label_NameNehag: TLabel;
    ComboEdit_CodeNehag: TComboEdit;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    QRY_Grigs: TRxQuery;
    RxDBFilter1: TRxDBFilter;
    DS_Grids: TDataSource;
    ToolButton3: TToolButton;
    GroupBox3: TGroupBox;
    Tbl_NehagKod_Nehag: TIntegerField;
    Tbl_NehagKod_Kvutza: TIntegerField;
    Tbl_NehagShem_Nehag: TStringField;
    Tbl_NehagKod_Tnai_tashlum: TIntegerField;
    Label1: TLabel;
    Lbl_CodeLakoach: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    Label28: TLabel;
    Label25: TLabel;
    Label2: TLabel;
    Label36: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label18: TLabel;
    EditHesbonitNumber: TAtmDbHEdit;
    EditCodeLakoach: TAtmDbHEdit;
    EditTotalHesbonitWithMAM: TAtmDbHEdit;
    EditLakoachGroup: TAtmDbHEdit;
    EditDateForFirstPayment: TAtmDBDateEdit;
    AtmDbComboBox_TnaeyTashlum: TAtmDbComboBox;
    AtmDbHEdit2: TAtmDbHEdit;
    EditTotal_Zikuy: TAtmDbHEdit;
    AtmDBDateEdit_HeshbonitDate: TAtmDBDateEdit;
    AtmDBDateEdit1: TAtmDBDateEdit;
    AtmDbHEdit3: TAtmDbHEdit;
    AtmDBDateEdit2: TAtmDBDateEdit;
    AtmDbHEdit4: TAtmDbHEdit;
    Panel4: TPanel;
    DBRadioGroup_HesbonitKind: TDBRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    GroupBox4: TGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label27: TLabel;
    Label48: TLabel;
    Label17: TLabel;
    Label23: TLabel;
    Label29: TLabel;
    Label32: TLabel;
    Label6: TLabel;
    EditTotalSumFromTnua: TAtmDbHEdit;
    EditTotalSumPriceKamut: TAtmDbHEdit;
    EditMAMHesbonit: TAtmDbHEdit;
    EditMAMPercent: TAtmDbHEdit;
    EditMAMRound: TAtmDbHEdit;
    EditAchuz_Anacha_Lakoach: TAtmDbHEdit;
    EditAchuz_Anacha_Klali: TAtmDbHEdit;
    EditNumberOfPayments: TAtmDbHEdit;
    EditLast_Update: TAtmDbHEdit;
    EditMaklidName: TAtmDbHEdit;
    EditYehusMonth: TAtmDbHEdit;
    EditOrderNum: TAtmDbHEdit;
    Panel_KodLak: TPanel;
    Label12: TLabel;
    Label_NameLak: TLabel;
    ComboEdit_CodeLak: TComboEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    EditMisHaavaraToHan: TAtmDbHEdit;
    EditCodeHaavaraToHan: TAtmDbHEdit;
    EditDate_Havara: TAtmDbHEdit;
    EditPratimToHan: TAtmDbHEdit;
    DBEdit_IncomType: TAtmDbHEdit;
    EditDatePiraon: TAtmDBDateEdit;
    AtmDBDateEdit_DateHafaka: TAtmDBDateEdit;
    Tbl_MakavHesbonitKind: TIntegerField;
    Tbl_MakavYehusYear: TIntegerField;
    Tbl_MakavHesbonitNumber: TIntegerField;
    Tbl_MakavCodeLakoach: TIntegerField;
    Tbl_MakavMerakez: TStringField;
    Tbl_MakavLakoachGroup: TIntegerField;
    Tbl_MakavHesbonitDate: TDateTimeField;
    Tbl_MakavTotalHesbonitWithMAM: TBCDField;
    Tbl_MakavMAMHesbonit: TBCDField;
    Tbl_MakavMAMPercent: TBCDField;
    Tbl_MakavMAMRound: TBCDField;
    Tbl_MakavDiscountWork: TBCDField;
    Tbl_MakavDiscountParts: TBCDField;
    Tbl_MakavAchuz_Anacha_Lakoach: TBCDField;
    Tbl_MakavAchuz_Anacha_Klali: TBCDField;
    Tbl_MakavPratimToHan: TStringField;
    Tbl_MakavCodeHaavaraToHan: TIntegerField;
    Tbl_MakavMaklidName: TStringField;
    Tbl_MakavDateHafakatHeshbonit: TDateTimeField;
    Tbl_MakavCodeMeasher: TIntegerField;
    Tbl_MakavSumPaied: TBCDField;
    Tbl_MakavHanForMam: TStringField;
    Tbl_MakavHanForLak: TStringField;
    Tbl_MakavYehusMonth: TIntegerField;
    Tbl_MakavPaymentCondition: TIntegerField;
    Tbl_MakavStatus: TIntegerField;
    Tbl_MakavNumberOfPayments: TIntegerField;
    Tbl_MakavDateForFirstPayment: TDateTimeField;
    Tbl_MakavOrderNum: TIntegerField;
    Tbl_MakavTotalSumFromTnua: TBCDField;
    Tbl_MakavTotalSumPriceKamut: TBCDField;
    Tbl_MakavDatePiraon: TDateTimeField;
    Tbl_MakavKvutzaLeChiyuv: TIntegerField;
    Tbl_MakavMisHaavaraToHan: TIntegerField;
    Tbl_MakavDate_Havara: TDateTimeField;
    Tbl_MakavTotal_Zikuy: TBCDField;
    Tbl_MakavDate_Zikuy: TDateTimeField;
    Tbl_MakavMis_Kabala1: TIntegerField;
    Tbl_MakavMis_Kabala2: TIntegerField;
    Tbl_MakavMis_Kabala3: TIntegerField;
    Tbl_MakavMis_Kabala4: TIntegerField;
    Tbl_MakavMis_Kabala5: TIntegerField;
    Tbl_MakavDate_Kabala1: TDateTimeField;
    Tbl_MakavDate_Kabala2: TDateTimeField;
    Tbl_MakavDate_Kabala3: TDateTimeField;
    Tbl_MakavDate_Kabala4: TDateTimeField;
    Tbl_MakavDate_Kabala5: TDateTimeField;
    Tbl_MakavIncom_Type: TIntegerField;
    Tbl_MakavLast_Update: TDateTimeField;
    Tbl_MakavCheckNum: TMemoField;
    Tbl_MakavSug_Anacha_Amala: TIntegerField;
    Tbl_MakavShaarDolar: TBCDField;
    Tbl_MakavRemFactor: TStringField;
    Tbl_MakavRem1: TMemoField;
    Tbl_MakavTotalAmalaForNehag: TBCDField;
    Tbl_MakavTotalMasMakor: TBCDField;
    Tbl_MakavAchuzMasMakor: TBCDField;
    Tbl_MakavSugPrice: TIntegerField;
    Tbl_MakavKod_sapak: TIntegerField;
    Tbl_MakavSugCalcMasMakor: TIntegerField;
    Tbl_MakavInteger_1: TIntegerField;
    Tbl_MakavInteger_2: TIntegerField;
    Tbl_MakavInteger_3: TIntegerField;
    Tbl_MakavInteger_4: TIntegerField;
    Tbl_MakavInteger_5: TIntegerField;
    Tbl_MakavInteger_6: TIntegerField;
    Tbl_MakavInteger_7: TIntegerField;
    Tbl_MakavInteger_8: TIntegerField;
    Tbl_MakavInteger_9: TIntegerField;
    Tbl_MakavInteger_10: TIntegerField;
    Tbl_MakavBcd_1: TBCDField;
    Tbl_MakavBcd_2: TBCDField;
    Tbl_MakavBcd_3: TBCDField;
    Tbl_MakavBcd_4: TBCDField;
    Tbl_MakavBcd_5: TBCDField;
    Tbl_MakavBcd_6: TBCDField;
    Tbl_MakavBcd_7: TBCDField;
    Tbl_MakavBcd_8: TBCDField;
    Tbl_MakavBcd_9: TBCDField;
    Tbl_MakavBcd_10: TBCDField;
    Tbl_MakavString_1: TStringField;
    Tbl_MakavString_2: TStringField;
    Tbl_MakavString_3: TStringField;
    Tbl_MakavString_4: TStringField;
    Tbl_MakavString_5: TStringField;
    Tbl_MakavString_6: TStringField;
    Tbl_MakavString_7: TStringField;
    Tbl_MakavString_8: TStringField;
    Tbl_MakavString_9: TStringField;
    Tbl_MakavString_10: TStringField;
    Tbl_MakavDate_1: TDateTimeField;
    Tbl_MakavDate_2: TDateTimeField;
    Tbl_MakavDate_3: TDateTimeField;
    Tbl_MakavDate_4: TDateTimeField;
    Tbl_MakavDate_5: TDateTimeField;
    Tbl_MakavDate_6: TDateTimeField;
    Tbl_MakavDate_7: TDateTimeField;
    Tbl_MakavDate_8: TDateTimeField;
    Tbl_MakavDate_9: TDateTimeField;
    Tbl_MakavDate_10: TDateTimeField;
    Tbl_MakavYitra: TCurrencyField;
    Tbl_MakavLookShemNehag: TStringField;
    Label20: TLabel;
    Label21: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_MakavAfterInsert(DataSet: TDataSet);
    procedure Tbl_MakavAfterPost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Change(Sender: TObject);
    procedure RxDBGrid_MakavTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure RxDBGrid_MakavGetBtnParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; var SortMarker: TSortMarker;
      IsDown: Boolean);
    procedure RxDBGrid_MakavGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure EditCodeNehagBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure ComboEdit_CodeNehagButtonClick(Sender: TObject);
    procedure RxDBGrid_MakavDblClick(Sender: TObject);
    procedure Lbl_CodeLakoachDblClick(Sender: TObject);
    procedure Tbl_MakavCalcFields(DataSet: TDataSet);
    procedure Tbl_MakavCodeNehagValidate(Sender: TField);
    procedure EditHesbonitNumberBeforeAutoLocateRecord(Sender: TObject;
      var ContinueOperation: Boolean);
  private
    { private declarations }
    CurSortFieldName :ShortString;
  public
    { public declarations }
    Procedure BuildLookupList;
    procedure RunQryMakav(OrderByFieldName :String);
    Procedure ShowForm(MainValue :ShortString);
  end;

var
  Frm_MakavHeshbonNehag: TFrm_MakavHeshbonNehag;

implementation
Uses DMAtmCrt,AtmConst,AtmRutin,Crt_Glbl;
{$R *.DFM}

procedure TFrm_MakavHeshbonNehag.FormCreate(Sender: TObject);
Var
   I:LongInt;
Begin
  Try
     Screen.Cursor:=crHourGlass;
     BuildLookupList;
     PageControl1.ActivePage:=TS_Main;
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Open;
     End;
     ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');

{     AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
     AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;}
     AtmTabSheetBuild1.ScriptsDir:=CrtDirForScripts;
     AtmTabSheetBuild1.FilterDir:=CrtDirForCurUser;
     AtmTabSheetBuild1.BuildTabsForGrids;
  Finally
         Screen.Cursor:=crDefault;
  End;
end;

procedure TFrm_MakavHeshbonNehag.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TTable Then
             (Components[I] As TTable).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');
end;

procedure TFrm_MakavHeshbonNehag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Frm_MakavHeshbonNehag:=Nil;
     Action:=caFree;
end;

procedure TFrm_MakavHeshbonNehag.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     if Not Visible Then Exit;
//     ChangeDSForNavigator(TDBNavigator(Sender));
     if Button=nbPost Then
       ((Sender As TDBNavigator).DataSource.DataSet As TAtmTable).SavePressed:=True;
end;

procedure TFrm_MakavHeshbonNehag.Tbl_MakavAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
end;

procedure TFrm_MakavHeshbonNehag.Tbl_MakavAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_MakavHeshbonNehag.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(ActiveControl,Key,Shift,Tbl_Makav);
end;

Procedure TFrm_MakavHeshbonNehag.BuildLookupList;
Var
   SList :TStringList;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_TnaeyTashlum),AtmDbComboBox_TnaeyTashlum);
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;

procedure TFrm_MakavHeshbonNehag.PageControl1Change(Sender: TObject);
begin
     if Query_MakavNehag.Active Then
        Query_MakavNehag.Active:=False;
     if PageControl1.ActivePage=TS_GridMakav Then
     Begin
          ComboEdit_CodeNehag.Text:=Tbl_Makav.FieldByName('CodeLakoach').AsString;
          Label_NameNehag.Caption:=Tbl_MakavLookShemNehag.AsString;
          RunQryMakav('HesbonitNumber');
          DBNavigator.DataSource:=DS_MakavNehag;
     End
     Else
         DBNavigator.DataSource:=DS_Makav;

end;

procedure TFrm_MakavHeshbonNehag.RunQryMakav(OrderByFieldName :String);
Begin
  Try
     Screen.Cursor:=crHourGlass;
      CurSortFieldName:=OrderByFieldName;
      With Query_MakavNehag Do
      Begin
           Active:=False;
           Sql.Clear;
           Sql.Add('Select * From '+Tbl_Makav.TableName);
           Sql.Add('Where CodeLakoach='+ComboEdit_CodeNehag.Text);
           Sql.Add('Order By '+OrderByFieldName);
           Active:=True;
      End;
  Finally
         Screen.Cursor:=crDefault;
  End;
End;
procedure TFrm_MakavHeshbonNehag.RxDBGrid_MakavTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
     RunQryMakav(Field.FieldName);
end;

procedure TFrm_MakavHeshbonNehag.RxDBGrid_MakavGetBtnParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor;
  var SortMarker: TSortMarker; IsDown: Boolean);
begin
     if CompareText(CurSortFieldName,Field.FieldName)=0 Then
          SortMarker := smDown;
end;

procedure TFrm_MakavHeshbonNehag.RxDBGrid_MakavGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     if CompareText(Field.FullName,CurSortFieldName)=0 Then
        Background:=clInfoBk;
end;

procedure TFrm_MakavHeshbonNehag.EditCodeNehagBeforeExecuteSearch(
  Sender: TObject; var ContinueExecute: Boolean);
begin
     EditCodeLakoach.SearchComponent.ReturnFieldIndex:=0;
end;

procedure TFrm_MakavHeshbonNehag.ComboEdit_CodeNehagButtonClick(Sender: TObject);
begin
     AtmAdvSearch_Nehag.ReturnFieldIndex:=-1;
     if AtmAdvSearch_Nehag.Execute Then
     Begin
          ComboEdit_CodeNehag.Text:=GetFieldFromSeparateString(AtmAdvSearch_Nehag.ReturnString,AtmAdvSearch_Nehag.SeparateChar,1);
          Label_NameNehag.Caption:=GetFieldFromSeparateString(AtmAdvSearch_Nehag.ReturnString,AtmAdvSearch_Nehag.SeparateChar,3);
          RunQryMakav(CurSortFieldName);
     End;
end;

procedure TFrm_MakavHeshbonNehag.RxDBGrid_MakavDblClick(Sender: TObject);
begin
     Tbl_Makav.FindKey([Query_MakavNehag.FieldByName('HesbonitNumber').Value]);
     PageControl1.ActivePage:=TS_Main;
end;

procedure TFrm_MakavHeshbonNehag.Lbl_CodeLakoachDblClick(Sender: TObject);
begin
     OpenAtmCrt(fnNehag,EditCodeLakoach.Text);
end;

procedure TFrm_MakavHeshbonNehag.Tbl_MakavCalcFields(DataSet: TDataSet);
begin
     Tbl_MakavYitra.AsFloat:=Tbl_MakavTotalHesbonitWithMAM.AsFloat-Tbl_MakavTotal_Zikuy.AsFloat;
end;

Procedure TFrm_MakavHeshbonNehag.ShowForm(MainValue :ShortString);
Begin
     Show;
End;
procedure TFrm_MakavHeshbonNehag.Tbl_MakavCodeNehagValidate(
  Sender: TField);
begin
     if Tbl_Nehag.FindKey([Tbl_MakavCodeLakoach.Value]) Then
     Begin
        if Tbl_Makav.State=dsInsert Then
          Tbl_MakavPaymentCondition.AsInteger:=Tbl_NehagKod_Tnai_Tashlum.AsInteger;
     End
     ELse
     Begin
          MessageDlg('��� �� ����',mtError,[mbOk],0);
          Abort;
     End;
end;

procedure TFrm_MakavHeshbonNehag.EditHesbonitNumberBeforeAutoLocateRecord(
  Sender: TObject; var ContinueOperation: Boolean);
Var
  St1,St2,St3 :String;
begin
  St1:=Tbl_Makav.FieldByName('HesbonitKind').AsString;
  St2:=Tbl_Makav.FieldByName('YehusYear').AsString;
  St3:=EditHesbonitNumber.Text;
  Tbl_Makav.Cancel;
  Tbl_Makav.FindKey([St1,St2,St3]);
  ContinueOperation:=False;
end;

end.


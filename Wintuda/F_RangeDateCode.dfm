object Frm_RangeDateCode: TFrm_RangeDateCode
  Left = 158
  Top = 185
  BiDiMode = bdRightToLeft
  BorderStyle = bsDialog
  Caption = '������'
  ClientHeight = 94
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 312
    Top = 10
    Width = 40
    Height = 13
    Caption = '������'
  end
  object Label2: TLabel
    Left = 128
    Top = 10
    Width = 50
    Height = 13
    Caption = '�� �����'
  end
  object Label3: TLabel
    Left = 310
    Top = 40
    Width = 26
    Height = 13
    Caption = '����'
  end
  object Label4: TLabel
    Left = 128
    Top = 40
    Width = 36
    Height = 13
    Caption = '�� ���'
  end
  object DateEdit_From: TDateEdit
    Left = 186
    Top = 6
    Width = 121
    Height = 21
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    NumGlyphs = 2
    StartOfWeek = Sun
    Weekends = [Sat]
    YearDigits = dyFour
    TabOrder = 0
  end
  object DateEdit_To: TDateEdit
    Left = 2
    Top = 6
    Width = 121
    Height = 21
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    NumGlyphs = 2
    StartOfWeek = Sun
    Weekends = [Sat]
    YearDigits = dyFour
    TabOrder = 1
  end
  object ComboEdit_FromCode: TComboEdit
    Left = 186
    Top = 36
    Width = 121
    Height = 22
    Glyph.Data = {
      36020000424D3602000000000000360000002800000010000000100000000100
      10000000000000020000000000000000000000000000000000001F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1F7C00001F7CEF3D00000000FF7F0000
      0000000000000000FF7FFF7F0000FF7F000000000000EF3D00000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000FF7F0000
      00000000000000000000FF7F00000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000EF3DEF3DEF3DEF3DEF3D00000000FF7F0000
      000000000000FF7FFF7FFF7F0000FF7F000000000000EF3D00000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1F7C00001F7CEF3D00000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7F0000000000000000
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000001F7C0000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F000000001F7C1F7C1F7C00000000000000000000
      0000000000000000000000000000000000000000000000000000}
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    NumGlyphs = 1
    TabOrder = 2
    OnButtonClick = ComboEdit_FromCodeButtonClick
  end
  object ComboEdit_ToCode: TComboEdit
    Left = 2
    Top = 36
    Width = 121
    Height = 22
    ButtonHint = '���� �����'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDD00000000000000000FFFFFFFF08888800FFFFFFFF0FD0D800F00000FF0F0
      00800FFFFFFFF0FFFFF00F000000F00000000FFFFFFFF08888800F0000FFF0F0
      00800FFFFFFFF0FD0D800FFFFFFFF0FFFFF000000000000000000FFFFFFFF000
      00000FFFFFFFF000D0000FFFFFFFF00DDD000000000000000000}
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    NumGlyphs = 1
    TabOrder = 3
    OnButtonClick = ComboEdit_FromCodeButtonClick
  end
  object BitBtn1: TBitBtn
    Left = 94
    Top = 66
    Width = 75
    Height = 24
    Caption = '�����'
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 186
    Top = 66
    Width = 75
    Height = 24
    Caption = '�����'
    TabOrder = 5
    Kind = bkCancel
  end
  object AtmAdvSearch_Maslul: TAtmAdvSearch
    Caption = '�����'
    KeyIndex = 0
    SourceDataSet = DM_AtmCrt.Qry_Temp
    ShowFields.Strings = (
      'Name'
      'CODE_MASLUL'
      'Motza'
      'Yaad'
      'Pratim')
    ShowFieldsHeader.Strings = (
      '��'
      '��� �����'
      '����'
      '���'
      '�����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '60'
      '100'
      '100'
      '100')
    Sql.Strings = (
      'Select CODE_MASLUL,Name,Motza,Yaad,Pratim'
      'From Maslul')
    ReturnFieldIndex = 1
    SeparateChar = ','
    KeepList = True
    Left = 303
    Top = 61
  end
end

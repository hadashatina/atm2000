unit F_Rehev;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, AtmAdvTable, DBCtrls, ExtCtrls, ToolEdit, RXDBCtrl,
  AtmDBDateEdit, StdCtrls, Mask, AtmComp,
  ComCtrls, Buttons, Grids, DBGrids, DBSrtGrd, ExtDlgs, DBFilter,
  ToolWin, AtmLookCombo, Scale250, RxQuery, AtmTabSheetBuild, Menus,
  DynamicControls, AdvSearch, AtmRxQuery, Spin, Yearplan;

type
  TFrm_Rehev = class(TForm)
    Tbl_Rehev: TAtmRxQuery;
    Ds_Rehev: TDataSource;
    Panel3: TPanel;
    HebLabel11: TLabel;
    HebLabel12: TLabel;
    HebLabel13: TLabel;
    HebLabel14: TLabel;
    HebLabel15: TLabel;
    Kod_Kvutza: TAtmDbHEdit;
    Kod_Miyun: TAtmDbHEdit;
    Kod_Nehag: TAtmDbHEdit;
    Mis_Pnimi: TAtmDbHEdit;
    TBL_TZIUDTnua: TAutoIncField;
    TBL_TZIUDMis_Rishui: TStringField;
    TBL_TZIUDKod: TIntegerField;
    TBL_TZIUDKod_Prm: TStringField;
    TBL_TZIUDTeur: TStringField;
    TBL_TZIUDMaslul_Bmp: TStringField;
    ds_Tziud: TDataSource;
    OpenPictureDialog1: TOpenPictureDialog;
    Tbl_Nehag: TAtmRxQuery;
    Ds_Nehag: TDataSource;
    DBLookupComboBox_ShemNehag: TDBLookupComboBox;
    HebDBText18: TDBText;
    PageControl1: TPageControl;
    TS_Rehev1: TTabSheet;
    KodNepach: TLabel;
    KodYatzran: TLabel;
    KodDelek: TLabel;
    SugRehev: TLabel;
    HebLabel1: TLabel;
    HebLabel3: TLabel;
    HebLabel4: TLabel;
    TaarichMechira: TLabel;
    HebLabel6: TLabel;
    HebLabel16: TLabel;
    HebLabel22: TLabel;
    HebLabel23: TLabel;
    HebLabel42: TLabel;
    HebLabel43: TLabel;
    HebLabel44: TLabel;
    HebLabel19: TLabel;
    HebLabel20: TLabel;
    HebLabel2: TLabel;
    HebLabel17: TLabel;
    HebLabel7: TLabel;
    HebLabel5: TLabel;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmDbHEdit3: TAtmDbHEdit;
    Shnat_Itzur: TAtmDbHEdit;
    AtmDbHEdit8: TAtmDbHEdit;
    AtmDbHEdit14: TAtmDbHEdit;
    AtmDbHEdit15: TAtmDbHEdit;
    AtmDbHEdit9: TAtmDbHEdit;
    AtmDbHEdit42: TAtmDbHEdit;
    AtmDbHEdit2: TAtmDbHEdit;
    AtmDbHEdit7: TAtmDbHEdit;
    HebDBRadioGroup1: TDBRadioGroup;
    AtmDBDateEdit1: TAtmDBDateEdit;
    Taarich_Mechira: TAtmDBDateEdit;
    Taarich_Knia: TAtmDBDateEdit;
    AtmDBDateEdit2: TAtmDBDateEdit;
    AtmDbHEdit6: TAtmDbHEdit;
    TS_Rehev2: TTabSheet;
    HebLabel24: TLabel;
    HebLabel25: TLabel;
    HebLabel28: TLabel;
    HebLabel29: TLabel;
    HebLabel30: TLabel;
    HebLabel31: TLabel;
    HebLabel32: TLabel;
    HebLabel33: TLabel;
    HebLabel38: TLabel;
    HebLabel39: TLabel;
    HebLabel40: TLabel;
    HebLabel45: TLabel;
    HebLabel46: TLabel;
    HebLabel47: TLabel;
    HebLabel48: TLabel;
    HebLabel8: TLabel;
    AtmDbHEdit16: TAtmDbHEdit;
    AtmDbHEdit27: TAtmDbHEdit;
    AtmDbHEdit28: TAtmDbHEdit;
    AtmDbHEdit29: TAtmDbHEdit;
    AtmDbHEdit30: TAtmDbHEdit;
    AtmDbHEdit31: TAtmDbHEdit;
    AtmDbHEdit32: TAtmDbHEdit;
    AtmDbHEdit33: TAtmDbHEdit;
    AtmDbHEdit38: TAtmDbHEdit;
    AtmDbHEdit39: TAtmDbHEdit;
    AtmDbHEdit40: TAtmDbHEdit;
    AtmDbHEdit43: TAtmDbHEdit;
    AtmDbHEdit49: TAtmDbHEdit;
    AtmDbHEdit50: TAtmDbHEdit;
    AtmDBDateEdit3: TAtmDBDateEdit;
    AtmDbHEdit5: TAtmDbHEdit;
    HebDBRadioGroup2: TDBRadioGroup;
    TS_Tools: TTabSheet;
    Bevel1: TBevel;
    Image_Tools: TImage;
    DBSrtGrd_Tools: TDBSrtGrd;
    DBEdit1: TDBEdit;
    Btn_BrowseToolsPic: TBitBtn;
    TS_ExtraFields: TTabSheet;
    TS_Picture: TTabSheet;
    Image_Rehev: TImage;
    Bevel3: TBevel;
    DBEdit2: TDBEdit;
    Btn_BrowseRehevPic: TBitBtn;
    ToolBar1: TToolBar;
    DBNavigator: TDBNavigator;
    ToolButton1: TToolButton;
    TBtn_OpenSearch: TToolButton;
    AtmDbHEdit_Rishuy: TAtmDbHEdit;
    AtmDbComboBox_Nefah: TAtmDbComboBox;
    AtmDbComboBox_Yazran: TAtmDbComboBox;
    AtmDbComboBox_SugDelek: TAtmDbComboBox;
    AtmDbComboBox_SugRehev: TAtmDbComboBox;
    AtmDbComboBox_Degem: TAtmDbComboBox;
    AtmDbComboBox_StatusRehev: TAtmDbComboBox;
    Scale1: TScale;
    AtmDbHEdit_KodNepach: TAtmDbHEdit;
    AtmDbHEditKod_Yatzran: TAtmDbHEdit;
    AtmDbHEdit_Sug_Delek: TAtmDbHEdit;
    AtmDbHEdit_Sug_Rehev: TAtmDbHEdit;
    AtmDbHEdit_Degem: TAtmDbHEdit;
    AtmDbHEdit_Status_Rehev: TAtmDbHEdit;
    AtmDbComboBox_KodMiun: TAtmDbComboBox;
    AtmDbComboBox_Pnimi: TAtmDbComboBox;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery_TabSheetBuild: TRxQuery;
    RxDBFilter_TabSheetBuild: TRxDBFilter;
    DS_TabSheetBuild: TDataSource;
    ToolButton2: TToolButton;
    StatusBar1: TStatusBar;
    DynamicControls1: TDynamicControls;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    TBtn_Report: TToolButton;
    UpdateSQL_Rehev: TUpdateSQL;
    Tbl_RehevMis_Rishui: TStringField;
    Tbl_RehevLookKvutza: TStringField;
    Tbl_RehevLookMiun: TStringField;
    Tbl_RehevLookPnimi: TStringField;
    Tbl_RehevLookNefah: TStringField;
    Tbl_RehevLookYatzran: TStringField;
    Tbl_RehevLookSugDelek: TStringField;
    Tbl_RehevLookSugRehev: TStringField;
    Tbl_RehevLookDegem: TStringField;
    Tbl_RehevLookStatus: TStringField;
    Tbl_RehevLookNehag: TStringField;
    Tbl_RehevInt_Kod_Kvutza: TIntegerField;
    Tbl_RehevKod_Kvutza: TIntegerField;
    Tbl_RehevInt_Kod_Miyun: TIntegerField;
    Tbl_RehevKod_Miyun: TIntegerField;
    Tbl_RehevInt_Kod_Nephach: TIntegerField;
    Tbl_RehevKod_Nephach: TIntegerField;
    Tbl_RehevInt_Kod_Yatzran: TIntegerField;
    Tbl_RehevKod_yatzran: TIntegerField;
    Tbl_RehevShnat_Itzur: TIntegerField;
    Tbl_RehevKod_Nehag: TIntegerField;
    Tbl_RehevInt_Sug_Rehev: TIntegerField;
    Tbl_RehevSug_Rehev: TIntegerField;
    Tbl_RehevSug_Delek: TIntegerField;
    Tbl_RehevInt_Sug_Delek: TIntegerField;
    Tbl_RehevMis_Pnimi: TIntegerField;
    Tbl_RehevInt_Mis_Pnimi: TIntegerField;
    Tbl_RehevMis_Shilda: TStringField;
    Tbl_RehevMis_Polisa: TStringField;
    Tbl_RehevMis_Han: TStringField;
    Tbl_RehevKvutzat_Mas: TStringField;
    Tbl_RehevMis_Kesher: TStringField;
    Tbl_RehevMis_Tag: TStringField;
    Tbl_RehevPle_Phon: TStringField;
    Tbl_RehevShovi_Shimush: TStringField;
    Tbl_RehevSug_Atzmada: TIntegerField;
    Tbl_RehevHetken: TStringField;
    Tbl_RehevTzeva: TStringField;
    Tbl_RehevChevrat_Bituach: TStringField;
    Tbl_RehevAsmachta: TStringField;
    Tbl_RehevMaslul_Bmp: TStringField;
    Tbl_RehevString_1: TStringField;
    Tbl_RehevString_2: TStringField;
    Tbl_RehevString_3: TStringField;
    Tbl_RehevString_4: TStringField;
    Tbl_RehevString_5: TStringField;
    Tbl_RehevInteger_1: TIntegerField;
    Tbl_RehevInteger_2: TIntegerField;
    Tbl_RehevInteger_3: TIntegerField;
    Tbl_RehevInteger_4: TIntegerField;
    Tbl_RehevInteger_5: TIntegerField;
    Tbl_RehevDate_1: TDateTimeField;
    Tbl_RehevDate_2: TDateTimeField;
    Tbl_RehevDate_3: TDateTimeField;
    Tbl_RehevDate_4: TDateTimeField;
    Tbl_RehevDate_5: TDateTimeField;
    Tbl_RehevString_6: TStringField;
    Tbl_RehevString_7: TStringField;
    Tbl_RehevString_8: TStringField;
    Tbl_RehevMoney_1: TBCDField;
    Tbl_RehevMoney_2: TBCDField;
    Tbl_RehevMoney_3: TBCDField;
    Tbl_RehevMoney_4: TBCDField;
    Tbl_RehevMoney_5: TBCDField;
    Tbl_RehevMoney_6: TBCDField;
    Tbl_RehevMoney_7: TBCDField;
    Tbl_RehevRishyon_Bmp: TStringField;
    Tbl_RehevLast_Spido: TIntegerField;
    Tbl_RehevMis_Rehev: TStringField;
    Tbl_RehevStatus_Rehev: TIntegerField;
    Tbl_RehevInt_Status_Rehev: TIntegerField;
    Tbl_RehevBealut: TStringField;
    Tbl_RehevShem_Sochen: TStringField;
    Tbl_RehevTel_Sochen: TStringField;
    Tbl_RehevPax_Sochen: TStringField;
    Tbl_RehevInt_Sug_Bituach: TIntegerField;
    Tbl_RehevSug_Bituach: TIntegerField;
    Tbl_RehevShem_Nehag_Yachid: TStringField;
    Tbl_RehevMis_Rehev_Num: TIntegerField;
    Tbl_RehevEara: TStringField;
    Tbl_RehevTaarich_Knia: TDateTimeField;
    Tbl_RehevTaarich_Mechira: TDateTimeField;
    Tbl_RehevTaarich_Heskem: TDateTimeField;
    Tbl_RehevTaarich_Rishui: TDateTimeField;
    Tbl_RehevGmar_Bituach: TDateTimeField;
    Tbl_RehevTaarich_Last_Spido: TDateTimeField;
    Tbl_RehevChova_Taarich_Siyum: TDateTimeField;
    Tbl_RehevRechush_Taarich_Atchala: TDateTimeField;
    Tbl_RehevRechush_Taarich_Siyum: TDateTimeField;
    Tbl_RehevInt_Degem: TIntegerField;
    Tbl_RehevDegem: TIntegerField;
    Tbl_RehevTotal: TBCDField;
    Tbl_RehevTotal_Teunot: TBCDField;
    Tbl_RehevTotal_Aluyot: TBCDField;
    Tbl_RehevMechir_Mechira: TBCDField;
    Tbl_RehevRadio: TBCDField;
    Tbl_RehevTosefet_Erech: TBCDField;
    Tbl_RehevChova_Premia: TBCDField;
    Tbl_RehevRechush_Premia: TBCDField;
    Tbl_RehevIshtatfut_Atzmit: TBCDField;
    Tbl_RehevIshtatfut_Musach: TBCDField;
    Tbl_RehevRehev_Pail: TSmallintField;
    Tbl_RehevShmashot: TSmallintField;
    Tbl_RehevGrira: TSmallintField;
    Tbl_RehevRehev_Chilufi: TSmallintField;
    Tbl_RehevTzad_G: TSmallintField;
    Tbl_RehevChova_Taarich_Atchala: TDateTimeField;
    Tbl_RehevKol_Nehag: TSmallintField;
    Tbl_RehevMeal_21: TSmallintField;
    Tbl_RehevMeal_24: TSmallintField;
    Tbl_RehevShabat: TSmallintField;
    Tbl_RehevAlut_Rehev: TBCDField;
    Tbl_RehevKibolet: TBCDField;
    Tbl_RehevMishkal_Motar: TBCDField;
    Tbl_RehevMishkal_Kolel: TBCDField;
    Tbl_RehevTiztrochet_Memutzaat: TBCDField;
    Tbl_RehevMadad_Basis: TBCDField;
    Tbl_RehevAchuz_maam_Mukar: TBCDField;
    Tbl_RehevAlut_Achzaka: TBCDField;
    AtmAdvSearch_Rehev: TAtmAdvSearch;
    N2: TMenuItem;
    TS_Calendar: TTabSheet;
    YearPlanner1: TYearPlanner;
    Panel1: TPanel;
    Shape_Color7: TShape;
    Shape_Color8: TShape;
    Shape_Color5: TShape;
    Shape_Color6: TShape;
    Shape_Color3: TShape;
    Shape_Color4: TShape;
    Shape_Color1: TShape;
    Shape_Color2: TShape;
    Label_Color1: TLabel;
    Label_Color2: TLabel;
    Label_Color3: TLabel;
    Label_Color4: TLabel;
    Label_Color5: TLabel;
    Label_Color6: TLabel;
    Label_Color7: TLabel;
    Label_Color8: TLabel;
    SpinEdit_Year: TSpinEdit;
    PopupMenu_SugDay: TPopupMenu;
    AtmRxQuery_Calendar: TAtmRxQuery;
    TBL_TZIUD: TAtmTable;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    RxDBFilter1: TRxDBFilter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure Tbl_RehevAfterInsert(DataSet: TDataSet);
    procedure Tbl_RehevAfterPost(DataSet: TDataSet);
    procedure Tbl_RehevAfterScroll(DataSet: TDataSet);
    procedure Btn_BrowseRehevPicClick(Sender: TObject);
    procedure Btn_BrowseToolsPicClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Kod_NepachBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_YatzranBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Sug_DelekBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Sug_RehevBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure DegemBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Status_RehevBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Mis_PnimiBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_MiyunBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure Kod_KvutzaBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmDbHEdit_RishuyExit(Sender: TObject);
    procedure Ds_RehevStateChange(Sender: TObject);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure Ds_NehagStateChange(Sender: TObject);
    procedure TBtn_ReportClick(Sender: TObject);
    procedure AtmTabSheetBuild1GridDblClick(Sender: TObject;
      ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
    procedure Tbl_RehevMis_RishuiSetText(Sender: TField;
      const Text: String);
    procedure Tbl_RehevMis_RishuiValidate(Sender: TField);
    procedure Tbl_TziudAfterScroll(DataSet: TDataSet);
    procedure Tbl_RehevBeforePost(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure SpinEdit_YearChange(Sender: TObject);
    procedure DBSrtGrd_ToolsEditButtonClick(Sender: TObject);
  private
    { Private declarations }
    Procedure LoadBitmaps;
    Procedure BuildSugDayMenu;
  public
    { Public declarations }
    Procedure ShowForm(FormParams:ShortString);
    Procedure ShowFormNoNehag(FormParams:ShortString);
    Procedure BuildLookupList;

    Procedure LoadCalendarAndUpdateView;
    Procedure DaySelected(Sender :TObject);
    Function  SugDayToColor(SugDay :LongInt):TColor;
  end;

var
  Frm_Rehev: TFrm_Rehev;

implementation
Uses Crt_Glbl,DMAtmCrt,AtmConst,Atmrutin;
{$R *.DFM}
Const
     PnlNum_DSState=0;

procedure TFrm_Rehev.FormCreate(Sender: TObject);
var
   i:longint;
begin
  Try
    Try
       Screen.Cursor:=crHourGlass;
       PageControl1.ActivePage:=PageControl1.Pages[0];
       DynamicControls1.ExtraFiledsFileName:=CrtDirForScripts+DynamicControls1.ExtraFiledsFileName;
       DynamicControls1.Execute;

       BuildLookupList;
       Scale1.DoScaleNow;

       AtmTabSheetBuild1.ScriptsDir:=CrtDirForScripts;
       AtmTabSheetBuild1.FilterDir:=CrtDirForCurUser;
{       AtmTabSheetBuild1.ScrFileName:=CrtDirForScripts+AtmTabSheetBuild1.ScrFileName;
       AtmTabSheetBuild1.SqlFileName:=CrtDirForScripts+AtmTabSheetBuild1.SqlFileName;}
       AtmTabSheetBuild1.BuildTabsForGrids;

       BuildSugDayMenu;
       ReadF4ValuesToIni(Self,CrtDirForCurUser+Name+'.Ini','FixedFields');
    Finally
           Screen.Cursor:=crDefault;
    End;
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
end;

procedure TFrm_Rehev.FormDestroy(Sender: TObject);
var
   i:longint;
begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if Components[I] is TDataset Then
             (Components[I] As TDataset).Close;
     End;
     SaveF4ValuesToIni(Self,CrtDirForCurUser+Name+'.ini','FixedFields');

end;

procedure TFrm_Rehev.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     KeyboardManageForTableAction(Sender,Key,Shift,Tbl_Rehev);
end;

procedure TFrm_Rehev.DBNavigatorBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
     Case Button of
         nbPost,nbInsert: DoSaveOnTable((Sender As TDBNavigator).DataSource.DataSet);
         nbRefresh:Begin
                      (Sender As TDBNavigator).DataSource.DataSet.Close;
                      (Sender As TDBNavigator).DataSource.DataSet.Open;
                      BuildLookupList;
                      Abort;
                   End;
     End;//Case
end;

procedure TFrm_Rehev.Tbl_RehevAfterInsert(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
     FindFirstControlInTabOrder(Self).SetFocus;
     Tbl_Rehev.FieldByName('Kod_Kvutza').AsInteger:=0;
end;

procedure TFrm_Rehev.Tbl_RehevAfterPost(DataSet: TDataSet);
begin
     SetAtmDbEditF4State(Self);
end;

procedure TFrm_Rehev.Tbl_RehevAfterScroll(DataSet: TDataSet);
begin
     LoadBitmaps;
     if AtmTabSheetBuild1.Query.Active Then
        AtmTabSheetBuild1.RefreshQry(Nil);
     if PageControl1.ActivePage=TS_Calendar Then
        LoadCalendarAndUpdateView;
end;

Procedure TFrm_Rehev.LoadBitmaps;
Begin
  if PageControl1.ActivePage=Ts_Picture Then
  Begin
      try
      if (Tbl_Rehev.FieldByName('Maslul_Bmp').isnull) or
         (Tbl_Rehev.FieldByName('Maslul_Bmp').Text = '') then
         begin
           Image_Rehev.Picture := nil;
         end
      else
         begin
           Image_Rehev.Picture.LoadFromFile(Tbl_Rehev.FieldByName('Maslul_Bmp').Text);
         end;
      except
         Image_Rehev.Picture := nil;
      End;
      Exit;
  End;

  if PageControl1.ActivePage=Ts_Tools Then
  Begin
      try
      if (Tbl_TziudMaslul_Bmp.isnull) or
         (Tbl_TziudMaslul_Bmp.Text = '') then
         begin
           Image_Tools.Picture := nil;
         end
      else
         begin
           Image_Tools.Picture.LoadFromFile(Tbl_TziudMaslul_Bmp.Text);
         end;
      except
         Image_Tools.Picture := nil;
      end;
      Exit;
  End;
End;
procedure TFrm_Rehev.Btn_BrowseRehevPicClick(Sender: TObject);
begin
    if OpenPictureDialog1.Execute  then
      begin
        Tbl_Rehev.Edit;
        Tbl_Rehev.FieldByName('Maslul_Bmp').Text := OpenPictureDialog1.FileName;
      end;
    LoadBitmaps;
end;

procedure TFrm_Rehev.Btn_BrowseToolsPicClick(Sender: TObject);
begin
    if OpenPictureDialog1.Execute  then
      begin
        Tbl_Tziud.Edit;
        Tbl_TziudMaslul_Bmp.Text := OpenPictureDialog1.FileName;
      end;
    LoadBitmaps;
end;

procedure TFrm_Rehev.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Frm_Rehev:=Nil;
     Action:=caFree;
end;

procedure TFrm_Rehev.Kod_NepachBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Nefah;
end;

procedure TFrm_Rehev.Kod_YatzranBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Yatzran;
end;

procedure TFrm_Rehev.Sug_DelekBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugDelek;
end;

procedure TFrm_Rehev.Sug_RehevBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_SugRehev;
end;

procedure TFrm_Rehev.DegemBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Degem;
end;

procedure TFrm_Rehev.Status_RehevBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_StatusRehev;
end;

procedure TFrm_Rehev.Mis_PnimiBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
    ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_PnimiRehev;
end;

procedure TFrm_Rehev.Kod_MiyunBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
   ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_MiunRehev;
end;

procedure TFrm_Rehev.Kod_KvutzaBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
    ((TAtmDbHEdit(Sender).SearchComponent.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_GroupRehev;
end;

procedure TFrm_Rehev.AtmDbHEdit_RishuyExit(Sender: TObject);
begin
    (Sender As TAtmDBHEdit).Invalidate;
end;

Procedure TFrm_Rehev.BuildLookupList;
Var
   SList:TStringList;
   I :LongInt;
Begin
     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_GroupRehev),Tbl_Rehev.FieldByName('LookKvutza'));
     SList.AddObject(IntToStr(SugTavla_MiunRehev),Tbl_Rehev.FieldByName('LookMiun'));
     SList.AddObject(IntToStr(SugTavla_PnimiRehev),Tbl_Rehev.FieldByName('LookPnimi'));
     SList.AddObject(IntToStr(SugTavla_Nefah),Tbl_Rehev.FieldByName('LookNefah'));
     SList.AddObject(IntToStr(SugTavla_Yatzran),Tbl_Rehev.FieldByName('LookYatzran'));
     SList.AddObject(IntToStr(SugTavla_SugDelek),Tbl_Rehev.FieldByName('LookSugDelek'));
     SList.AddObject(IntToStr(SugTavla_SugRehev),Tbl_Rehev.FieldByName('LookSugRehev'));
     SList.AddObject(IntToStr(SugTavla_Degem),Tbl_Rehev.FieldByName('LookDegem'));
     SList.AddObject(IntToStr(SugTavla_StatusRehev),Tbl_Rehev.FieldByName('LookStatus'));
     SList.AddObject(IntToStr(SugTavla_Tziud),Tbl_TziudKod_Prm);


     FillKodTavlaLookupList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;

     SList:=TStringList.Create;
     SList.AddObject(IntToStr(SugTavla_Nefah),AtmDbComboBox_Nefah);
     SList.AddObject(IntToStr(SugTavla_Yatzran),AtmDbComboBox_Yazran);
     SList.AddObject(IntToStr(SugTavla_SugDelek),AtmDbComboBox_SugDelek);
     SList.AddObject(IntToStr(SugTavla_SugRehev),AtmDbComboBox_SugRehev);
     SList.AddObject(IntToStr(SugTavla_Degem),AtmDbComboBox_Degem);
     SList.AddObject(IntToStr(SugTavla_StatusRehev),AtmDbComboBox_StatusRehev);
     SList.AddObject(IntToStr(SugTavla_MiunRehev),AtmDbComboBox_KodMiun);
     SList.AddObject(IntToStr(SugTavla_PnimiRehev),AtmDbComboBox_Pnimi);
     For I:=0 To ComponentCount-1 Do
     Begin
        if Components[i] is TAtmDbComboBox Then
          if TAtmDbComboBox(Components[i]).SugTavlaIndex>0 Then
            SList.AddObject(IntToStr(TAtmDbComboBox(Components[i]).SugTavlaIndex),Components[i]);
     End;
     FillKodTavlaComboList(DM_AtmCrt.Tbl_KodTavla,SList);
     SList.Free;
End;

procedure TFrm_Rehev.Ds_RehevStateChange(Sender: TObject);
begin
     DM_AtmCrt.Action_OpenSearchUpdate(TBtn_OpenSearch.Action);
end;

Procedure TFrm_Rehev.ShowForm(FormParams:ShortString);
Var
   I :LongInt;
Begin
     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataset) And (Components[i].Tag=1)Then
             (Components[I] As TDataset).Open
          Else
          if (Components[I] is TAtmAdvSearch) Then
              TAtmAdvSearch(Components[i]).IniFileName:=CrtDirForCurUser+Name+'.Ini';
     End;

     Show;
     if FormParams<>'' Then
          Tbl_Rehev.FindKey([FormParams])
     ELse
//          Tbl_Rehev.Insert;
       SetDatasetState(Tbl_Rehev,CrtFirstTableState);
End;

Procedure TFrm_Rehev.ShowFormNoNehag(FormParams:ShortString);
Var
   I :LongInt;
Begin
//     Tbl_RehevShem_Nehag_Prm.LookupKeyFields:='Kod_Tavla';
//     Tbl_RehevShem_Nehag_Prm.LookupResultField:='Teur_Tavla';
     Tbl_Rehev.FieldByName('LookNehag').LookupKeyFields:='Kod_Tavla';
     Tbl_Rehev.FieldByName('LookNehag').LookupResultField:='Teur_Tavla';

     //Tbl_Nehag.TableName:='KODTAVLA';
     //Tbl_Nehag.Filter:='Sug_Tavla=14';//���� �����
     Tbl_Nehag.Active:=False;
     Tbl_Nehag.Sql.Clear;
     Tbl_Nehag.Sql.Add('Select * From KODTAVLA Where Sug_Tavla=14');
     Tbl_Nehag.Sql.Add('Order By %OrderFields');
     Tbl_Nehag.IndexFieldNames:='Kod_Tavla';
     For I:=0 To ComponentCount-1 Do
     Begin
          if (Components[I] is TDataset) And (Components[i].Tag=1)Then
             (Components[I] As TDataset).Open;
     End;
     if FormParams<>'' Then
          Tbl_Rehev.FindKey([FormParams]);
     Show;
End;
procedure TFrm_Rehev.AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
begin
  if TQuery(AtmTabSheetBuild1.Query).Params.FindParam('PRehevNum')<>Nil Then
    TQuery(AtmTabSheetBuild1.Query).ParamByName('PRehevNum').AsString:=Tbl_Rehev.FieldByName('Mis_Rishui').AsString;
end;

procedure TFrm_Rehev.Ds_NehagStateChange(Sender: TObject);
begin
     StatusBar1.Panels[PnlNum_DSState].Text:=ConvertDSStateToText((Sender As TDataSource).DataSet);
end;

procedure TFrm_Rehev.TBtn_ReportClick(Sender: TObject);
begin
     RunDllOpenMeholel('Meholel.Dll','OpenMeholel',9999,CrtCurUserName,CurCrtAliasName,False,CrtMeholelHandle);
     RunDllProcPCharParam('Meholel.Dll','OpenReportList',[msRehev],False,CrtMeholelHandle);
end;

procedure TFrm_Rehev.AtmTabSheetBuild1GridDblClick(Sender: TObject;
  ATabSheet: TTabSheet; ARxDbGrid: TRxDBGrid);
begin
     if CompareText(AtmTabSheetBuild1.CurID,'GridTavla')=0 Then
     Begin
         if Tbl_Rehev.FindKey([AtmTabSheetBuild1.Query.FieldByName('Mis_Rishui').Value]) Then
         Begin
            PageControl1.ActivePage:=PageControl1.Pages[0];
//            DBNavigator.DataSource:=DS_Mosad;
         End;
     End;
end;

procedure TFrm_Rehev.Tbl_RehevMis_RishuiSetText(Sender: TField;
  const Text: String);
begin
     Sender.AsString:=ConvertToRishuy(Text);
end;

procedure TFrm_Rehev.Tbl_RehevMis_RishuiValidate(Sender: TField);
begin
   Tbl_Rehev.FieldByName('Mis_Rehev').AsString:=DelChars(Tbl_Rehev.FieldByName('Mis_Rishui').AsString,['-',' ']);
   Try
      Tbl_Rehev.FieldByName('Mis_Rehev_Num').AsInteger:=Tbl_Rehev.FieldByName('Mis_Rehev').AsInteger;
   Except
   End;
end;

procedure TFrm_Rehev.Tbl_TziudAfterScroll(DataSet: TDataSet);
begin
     LoadBitmaps;
end;

procedure TFrm_Rehev.Tbl_RehevBeforePost(DataSet: TDataSet);
Var
  H :THandle;
begin
   H:=0;
{   if RunDllFunc2PCharParamBool('Permit.Dll','CheckTableWrite',CrtCurUserName,'Tbl_Rehev',True,H) Then
    ShowMessage('True')
   Else
    ShowMessage('False');}
   if Trim(Tbl_Rehev.FieldByName('Mis_Rishui').AsString)='' Then
   Begin
     MessageDlg('���� ���� ����� ���� ���� ���',mtError,[mbOk],0);
     Abort;
   End;

  if Tbl_Rehev.FieldByName('Int_Status_Rehev').IsNull Then
    Tbl_Rehev.FieldByName('Int_Status_Rehev').AsInteger:=27
end;

Procedure TFrm_Rehev.BuildSugDayMenu;
Var
  TM :TMenuItem;
  StrH :String;
Begin
    With DM_AtmCrt.Qry_Temp Do //����� ���� �����
    Begin
      TM:=TMenuItem.Create(PopupMenu_SugDay);
      TM.Caption:='��� ����� ����';
      TM.Tag:=-1;
      TM.OnClick:=DaySelected;
      PopupMenu_SugDay.Items.Add(TM);
      Active:=False;
      Sql.Clear;
      Sql.Add('Select * From KODTAVLA');
      Sql.Add('Where Sug_Tavla='+IntToStr(SugTavla_SugDay));
      Try
        Active:=True;
        First;
        While Not Eof Do
        Begin
          TM:=TMenuItem.Create(PopupMenu_SugDay);
          TM.Caption:=FieldByName('Teur_Tavla').AsString;
          TM.Tag:=FieldByName('Kod_Tavla').AsInteger;
          TM.OnClick:=DaySelected;
          PopupMenu_SugDay.Items.Add(TM);

          StrH:='Label_Color'+FieldByName('Kod_Tavla').AsString;
          if Self.FindComponent(Trim(StrH))<>Nil Then
          Begin
            TWinControl(Self.FindComponent('Shape_Color'+FieldByName('Kod_Tavla').AsString)).Visible:=True;
            TWinControl(Self.FindComponent('Label_Color'+FieldByName('Kod_Tavla').AsString)).Visible:=True;
            TLabel(Self.FindComponent('Label_Color'+FieldByName('Kod_Tavla').AsString)).Caption:=FieldByName('Teur_Tavla').AsString;
          End;

          Next;
        End;
      Finally
        Active:=False;
      End;
    End;
End;


procedure TFrm_Rehev.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePage=TS_Calendar Then
  Begin
    YearPlanner1.Year:=SpinEdit_Year.Value;
    LoadCalendarAndUpdateView;
  End;

end;

Procedure TFrm_Rehev.LoadCalendarAndUpdateView;
Var
  D,M,Y :Word;
  I,J :LongInt;
Begin
    For I:=1 To 12 Do
      For J:=1 To 31 Do
      Begin
        YearPlanner1.CellData[I,J].CellHint:='';
        YearPlanner1.CellData[I,J].CustomColor:=False;
        YearPlanner1.CellData[I,J].CellColor:=YearPlanner1.DayColor;
        YearPlanner1.CellData[I,J].CellTag:=-1;
      End;

    With AtmRxQuery_Calendar Do //���� ��� ����
    Begin
      Active:=False;
      ParamByName('PYear').AsInteger:=SpinEdit_Year.Value;
      ParamByName('PCodeCalendar').AsString:=Tbl_Rehev.FieldByName('Mis_Rishui').AsString;
      Screen.Cursor:=crSqlWait;
      Active:=True;
      Try
        First;
        While Not Eof Do
        Begin
          DecodeDate(FieldByName('DAY_DATE').AsDateTime,y,m,d);
          YearPlanner1.CellData[m,d].CellTag:=FieldByName('CODE_SUG_DAY').AsInteger;
          YearPlanner1.CellData[m,d].CellColor:=SugDayToColor(FieldByName('CODE_SUG_DAY').AsInteger);
          YearPlanner1.CellData[m,d].CustomColor:=True;
          For I:=0 To PopupMenu_SugDay.Items.Count-1 Do
            if TMenuItem(PopupMenu_SugDay.Items[i]).Tag=FieldByName('CODE_SUG_DAY').AsInteger Then
            Begin
              YearPlanner1.CellData[m,d].CellHint:=TMenuItem(PopupMenu_SugDay.Items[i]).Caption;
              Break;
            End;
          Next;
        End;
      Finally
        Active:=False;
        Screen.Cursor:=crDefault;
      End;
    End;
    YearPlanner1.Invalidate;
End;

Procedure TFrm_Rehev.DaySelected(Sender :TObject);
Var
  m,d :Word;
  CurDate:TDateTime;
  CurDateString :String;
Begin
  d:=YearPlanner1.CurrentDate.Day;
  m:=YearPlanner1.CurrentDate.Month;
  if YearPlanner1.CellData[M,D].CellTag=TMenuItem(Sender).Tag Then
    Exit;
  CurDate:=EncodeDate(SpinEdit_Year.Value,M,D);
  CurDateString:=DateToSqlStr(CurDate,'mm/dd/yyyy');

  if YearPlanner1.CellData[M,D].CellTag>-1 Then //��� ��� ����
  Begin
    if TMenuItem(Sender).Tag=-1 Then //����� ���� ���� - ����� ������
    With DM_AtmCrt.Qry_Temp Do
    Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Delete From CALREHEV');
      Sql.Add('Where CAL_YEAR='+SpinEdit_Year.Text);
      Sql.Add('And CODE_CALENDER = '''+Tbl_Rehev.FieldByName('Mis_Rishui').AsString+'''');
      Sql.Add('And DAY_DATE='''+CurDateString+'''');
      ExecSQL;
    End
    Else
    Begin //����� ���
      With DM_AtmCrt.Qry_Temp Do
      Begin
        Active:=False;
        Sql.Clear;
        Sql.Add('Update CALREHEV');
        Sql.Add('Set DESCRIPTION='''+TMenuItem(Sender).Caption+''',');
        Sql.Add('DAY_OF_WEEK='''+ShortDayNames[DayOfWeek(EncodeDate(SpinEdit_Year.Value,M,D))]+''',');
        Sql.Add('CODE_SUG_DAY='+IntToStr(TMenuItem(Sender).Tag));
        Sql.Add('Where CAL_YEAR='+SpinEdit_Year.Text);
        Sql.Add('And CODE_CALENDER = '''+Tbl_Rehev.FieldByName('Mis_Rishui').AsString+'''');
        Sql.Add('And DAY_DATE='''+CurDateString+'''');
        ExecSQL;
      End;
    End;
  End
  Else //��� ��� ���� �� ������ ���
  Begin
    With DM_AtmCrt.Qry_Temp Do
    Begin
      Active:=False;
      Sql.Clear;
      Sql.Add('Insert into CALREHEV');
      Sql.Add('(CAL_YEAR,CODE_CALENDER,DAY_DATE,DESCRIPTION,DAY_OF_WEEK,CODE_SUG_DAY)');
      Sql.Add('Values');
      Sql.Add('('+SpinEdit_Year.Text+',');
      Sql.Add(''''+Tbl_Rehev.FieldByName('Mis_Rishui').AsString+''',');
      Sql.Add(''''+CurDateString+''',');
      Sql.Add(''''+TMenuItem(Sender).Caption+''',');
      Sql.Add(''''+ShortDayNames[DayOfWeek(CurDate)]+''',');
      Sql.Add(IntToStr(TMenuItem(Sender).Tag)+')');
      ExecSQL;
    End;
  End;

  YearPlanner1.CellData[m,d].CellTag:=TMenuItem(Sender).Tag;
  YearPlanner1.CellData[m,d].CellColor:=SugDayToColor(TMenuItem(Sender).Tag);
  YearPlanner1.CellData[m,d].CustomColor:=TMenuItem(Sender).Tag<>-1;
  YearPlanner1.CellData[m,d].CellHint:=TMenuItem(Sender).Caption;
  YearPlanner1.Invalidate;

End;

Function TFrm_Rehev.SugDayToColor(SugDay :LongInt):TColor;
Begin
  Case SugDay Of
    1:Result:=clRed; //�����
    2:Result:=clFuchsia;//�����
    3:Result:=clYellow;//�������
    4:Result:=clTeal;//��� ���
    5:Result:=clAqua;//�����
    6:Result:=clLime;//����� ������
    7:Result:=clPurple//����
  Else
    Result:=clOlive;
  End;//Case

End;


procedure TFrm_Rehev.SpinEdit_YearChange(Sender: TObject);
begin
  YearPlanner1.Year:=SpinEdit_Year.Value;
  LoadCalendarAndUpdateView;
end;

procedure TFrm_Rehev.DBSrtGrd_ToolsEditButtonClick(Sender: TObject);
begin
  ((DM_AtmCrt.AtmAdvSearch_KodTavla.SourceDataSet) As TQuery).ParamByName('PSugTavla').AsInteger:=SugTavla_Tziud;
  if DM_AtmCrt.AtmAdvSearch_KodTavla.Execute Then
  Begin
    if TBL_TZIUD.State=dsBrowse Then
      TBL_TZIUD.Edit;
    TBL_TZIUD.FieldbyName('Kod').AsString:=DM_AtmCrt.AtmAdvSearch_KodTavla.ReturnString;
  End;
end;

end.

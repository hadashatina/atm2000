object DM_Kupa: TDM_Kupa
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 3
  Top = 51
  Height = 401
  Width = 637
  object Database_Kupa: TDatabase
    AliasName = 'Atm'
    DatabaseName = 'DB_Kupa'
    KeepConnection = False
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    AfterConnect = Database_KupaAfterConnect
    OnLogin = Database_KupaLogin
    Left = 32
    Top = 24
  end
  object Qry_Temp: TQuery
    DatabaseName = 'DB_Kupa'
    UniDirectional = True
    UpdateMode = upWhereChanged
    Left = 100
    Top = 22
  end
  object AtmRxQuery_ZikuyAv: TAtmRxQuery
    CachedUpdates = True
    BeforeInsert = AtmRxQuery_ZikuyAvBeforeInsert
    AfterInsert = AtmRxQuery_ZikuyAvAfterInsert
    BeforePost = AtmRxQuery_ZikuyAvBeforePost
    BeforeDelete = AtmRxQuery_ZikuyAvBeforeDelete
    BeforeScroll = AtmRxQuery_ZikuyAvBeforeScroll
    AfterScroll = AtmRxQuery_ZikuyAvAfterScroll
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From ZikuyAv'
      'Where YehusYear = :PYehusYear'
      'Order By %OrderFields'
      ' '
      ' ')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_ZikuyAv
    Macros = <
      item
        DataType = ftString
        Name = 'OrderFields'
        ParamType = ptInput
        Value = 'KabalaNumber'
      end>
    IndexFieldNames = 'KabalaNumber'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = False
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = False
    Left = 34
    Top = 90
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end>
    object AtmRxQuery_ZikuyAvYehusYear: TIntegerField
      FieldName = 'YehusYear'
      Origin = 'DB_KUPA."Shipment.DB".YehusYear'
    end
    object AtmRxQuery_ZikuyAvKabalaNumber: TIntegerField
      FieldName = 'KabalaNumber'
      Origin = 'DB_KUPA."Shipment.DB".KabalaNumber'
    end
    object AtmRxQuery_ZikuyAvKabalaKind: TIntegerField
      FieldName = 'KabalaKind'
      Origin = 'DB_KUPA."Shipment.DB".KabalaKind'
    end
    object AtmRxQuery_ZikuyAvDateKabala: TDateTimeField
      FieldName = 'DateKabala'
      Origin = 'DB_KUPA."Shipment.DB".DateKabala'
      OnValidate = AtmRxQuery_ZikuyAvDateKabalaValidate
    end
    object AtmRxQuery_ZikuyAvCodeLakoach: TIntegerField
      FieldName = 'CodeLakoach'
      Origin = 'DB_KUPA."Shipment.DB".CodeLakoach'
      OnValidate = AtmRxQuery_ZikuyAvCodeLakoachValidate
    end
    object AtmRxQuery_ZikuyAvShemLakoach: TStringField
      FieldName = 'ShemLakoach'
      Origin = 'DB_KUPA."Shipment.DB".ShemLakoach'
      Size = 30
    end
    object AtmRxQuery_ZikuyAvKtovet: TStringField
      FieldName = 'Ktovet'
      Origin = 'DB_KUPA."Shipment.DB".Ktovet'
      Size = 30
    end
    object AtmRxQuery_ZikuyAvHesbonChiuv: TStringField
      FieldName = 'HesbonChiuv'
      Origin = 'DB_KUPA."Shipment.DB".HesbonChiuv'
      Size = 10
    end
    object AtmRxQuery_ZikuyAvHesbonZikuy: TStringField
      FieldName = 'HesbonZikuy'
      Origin = 'DB_KUPA."Shipment.DB".HesbonZikuy'
      Size = 10
    end
    object AtmRxQuery_ZikuyAvHesbonMasMakor: TStringField
      FieldName = 'HesbonMasMakor'
      Origin = 'DB_KUPA."Shipment.DB".HesbonMasMakor'
      Size = 10
    end
    object AtmRxQuery_ZikuyAvKodBItzua: TIntegerField
      DefaultExpression = '0'
      FieldName = 'KodBItzua'
      Origin = 'DB_KUPA."Shipment.DB".KodBItzua'
    end
    object AtmRxQuery_ZikuyAvMisparHavara: TIntegerField
      FieldName = 'MisparHavara'
      Origin = 'DB_KUPA."Shipment.DB".MisparHavara'
    end
    object AtmRxQuery_ZikuyAvTotalSumKabla: TBCDField
      FieldName = 'TotalSumKabla'
      Origin = 'DB_KUPA."Shipment.DB".TotalSumKabla'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ZikuyAvPratim: TStringField
      FieldName = 'Pratim'
      Origin = 'DB_KUPA."Shipment.DB".Pratim'
      Size = 50
    end
    object AtmRxQuery_ZikuyAvHaavaraBankait: TIntegerField
      FieldName = 'HaavaraBankait'
      Origin = 'DB_KUPA."Shipment.DB".HaavaraBankait'
    end
    object AtmRxQuery_ZikuyAvShuraKopa: TIntegerField
      FieldName = 'ShuraKopa'
      Origin = 'DB_KUPA."Shipment.DB".ShuraKopa'
      OnValidate = AtmRxQuery_ZikuyAvShuraKopaValidate
    end
    object AtmRxQuery_ZikuyAvCodeHaavaraToHan: TIntegerField
      FieldName = 'CodeHaavaraToHan'
      Origin = 'DB_KUPA."Shipment.DB".CodeHaavaraToHan'
    end
    object AtmRxQuery_ZikuyAvTotalZikuy: TBCDField
      FieldName = 'TotalZikuy'
      Origin = 'DB_KUPA."Shipment.DB".TotalZikuy'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ZikuyAvLastUpdate: TDateTimeField
      FieldName = 'LastUpdate'
      Origin = 'DB_KUPA."Shipment.DB".LastUpdate'
    end
    object AtmRxQuery_ZikuyAvMaklidName: TStringField
      FieldName = 'MaklidName'
      Origin = 'DB_KUPA."Shipment.DB".MaklidName'
      Size = 15
    end
    object AtmRxQuery_ZikuyAvTotalVat: TBCDField
      FieldName = 'TotalVat'
      Origin = 'DB_KUPA."Shipment.DB".TotalVat'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ZikuyAvTotalPlusVat: TBCDField
      FieldName = 'TotalPlusVat'
      Origin = 'DB_KUPA."Shipment.DB".TotalPlusVat'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ZikuyAvMamPercent: TBCDField
      FieldName = 'MamPercent'
      Origin = 'DB_KUPA."Shipment.DB".MamPercent'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_ZikuyAvDatePiraon: TDateTimeField
      FieldName = 'DatePiraon'
      Origin = 'ATMSQL.ZikuyAv.DatePiraon'
    end
    object AtmRxQuery_ZikuyAvKodMatbea: TIntegerField
      FieldName = 'KodMatbea'
      Origin = 'ATMSQL.ZikuyAv.KodMatbea'
    end
    object AtmRxQuery_ZikuyAvLookKvoLak: TStringField
      FieldKind = fkLookup
      FieldName = 'LookKvoLak'
      KeyFields = 'ShuraKopa'
      LookupCache = True
      Lookup = True
    end
  end
  object DS_ZikuyAv: TDataSource
    DataSet = AtmRxQuery_ZikuyAv
    Left = 32
    Top = 150
  end
  object Qry_AtmIndexKabala: TQuery
    CachedUpdates = True
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From AtmIndex'
      'Where Key_String = :PKey_String'
      'And YehusYear=:PYehusYear')
    UniDirectional = True
    UpdateObject = UpdateSQL_AtmIndex
    Left = 342
    Top = 13
    ParamData = <
      item
        DataType = ftString
        Name = 'PKey_String'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PYehusYear'
        ParamType = ptUnknown
      end>
  end
  object UpdateSQL_AtmIndex: TUpdateSQL
    ModifySQL.Strings = (
      'update AtmIndex'
      'set'
      '  Key_String = :Key_String,'
      '  YehusYear = :YehusYear,'
      '  Last_Number = :Last_Number,'
      '  LastUpDate = :LastUpDate,'
      '  MaklidName = :MaklidName'
      'where'
      '  Key_String = :OLD_Key_String and'
      '  YehusYear = :OLD_YehusYear')
    InsertSQL.Strings = (
      'insert into AtmIndex'
      '  (Key_String, YehusYear, Last_Number, LastUpDate, MaklidName)'
      'values'
      
        '  (:Key_String, :YehusYear, :Last_Number, :LastUpDate, :MaklidNa' +
        'me)')
    DeleteSQL.Strings = (
      'delete from AtmIndex'
      'where'
      '  Key_String = :OLD_Key_String and'
      '  YehusYear = :OLD_YehusYear')
    Left = 342
    Top = 76
  end
  object DS_KabHes: TDataSource
    DataSet = AtmRxQuery_KabHes
    Left = 338
    Top = 184
  end
  object Qry_Mname: TQuery
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'Select * From MName')
    UpdateMode = upWhereChanged
    Left = 159
    Top = 19
  end
  object AtmRxQuery_KabHes: TAtmRxQuery
    CachedUpdates = True
    AfterOpen = AtmRxQuery_KabHesAfterOpen
    AfterInsert = AtmRxQuery_KabHesAfterInsert
    BeforeEdit = AtmRxQuery_KabHesBeforeEdit
    BeforePost = AtmRxQuery_KabHesBeforePost
    AfterPost = AtmRxQuery_KabHesAfterPost
    BeforeDelete = AtmRxQuery_KabHesBeforeDelete
    AfterDelete = AtmRxQuery_KabHesAfterPost
    DatabaseName = 'DB_Kupa'
    RequestLive = True
    SQL.Strings = (
      'Select * From Zikuy'
      'Where KabalaNumber = :PKabalaNumber'
      'And KabalaYear = :PKabalaYear'
      ' ')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_Zikuy
    Macros = <>
    IndexFieldNames = 'KabalaNumber'
    IndexAutoSort = True
    MacroForIndex = 'OrderFields'
    MacroForWhere = 'MWhereIndex'
    AllartBeforeSave = False
    AutoValidateLookupFields = False
    DeleteDetailRecords = False
    ConfirmDeleteDetail = True
    AutoDefaults = True
    AutoApplyUpdates = True
    GetOnlyOneRecord = False
    Left = 337
    Top = 134
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PKabalaNumber'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PKabalaYear'
        ParamType = ptUnknown
      end>
    object AtmRxQuery_KabHesTnua: TIntegerField
      FieldName = 'Tnua'
      Origin = 'DB_KUPA."ShipTnua.DB".Tnua'
    end
    object AtmRxQuery_KabHesKabalaNumber: TIntegerField
      FieldName = 'KabalaNumber'
      Origin = 'DB_KUPA."ShipTnua.DB".KabalaNumber'
    end
    object AtmRxQuery_KabHesKabalaYear: TIntegerField
      FieldName = 'KabalaYear'
      Origin = 'DB_KUPA."ShipTnua.DB".KabalaYear'
    end
    object AtmRxQuery_KabHesSchumHesbonit: TBCDField
      FieldName = 'SchumHesbonit'
      Origin = 'DB_KUPA."ShipTnua.DB".SchumHesbonit'
      OnValidate = AtmRxQuery_KabHesSchumHesbonitValidate
      DisplayFormat = '#.00'
      EditFormat = '#.00'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesDateHesbonit: TDateTimeField
      FieldName = 'DateHesbonit'
      Origin = 'DB_KUPA."ShipTnua.DB".DateHesbonit'
      DisplayFormat = 'dd/mm/yyyy'
      EditMask = '!99/99/0000;1;_'
    end
    object AtmRxQuery_KabHesKamutHesbonit: TBCDField
      FieldName = 'KamutHesbonit'
      Origin = 'DB_KUPA."ShipTnua.DB".KamutHesbonit'
      OnChange = AtmRxQuery_KabHesKamutHesbonitChange
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesPratimHesbonit: TStringField
      FieldName = 'PratimHesbonit'
      Origin = 'DB_KUPA."ShipTnua.DB".PratimHesbonit'
      Size = 40
    end
    object AtmRxQuery_KabHesTotalSchum: TBCDField
      FieldName = 'TotalSchum'
      Origin = 'DB_KUPA."ShipTnua.DB".TotalSchum'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesDateKabala: TDateTimeField
      FieldName = 'DateKabala'
      Origin = 'DB_KUPA."ShipTnua.DB".DateKabala'
    end
    object AtmRxQuery_KabHesCodeLakoach: TIntegerField
      FieldName = 'CodeLakoach'
      Origin = 'DB_KUPA."ShipTnua.DB".CodeLakoach'
    end
    object AtmRxQuery_KabHesShemLakoach: TStringField
      FieldName = 'ShemLakoach'
      Origin = 'DB_KUPA."ShipTnua.DB".ShemLakoach'
      Size = 30
    end
    object AtmRxQuery_KabHesLakoachGroup: TIntegerField
      FieldName = 'LakoachGroup'
      Origin = 'DB_KUPA."ShipTnua.DB".LakoachGroup'
    end
    object AtmRxQuery_KabHesCodebitzua: TIntegerField
      FieldName = 'Codebitzua'
      Origin = 'DB_KUPA."ShipTnua.DB".Codebitzua'
    end
    object AtmRxQuery_KabHesCodemaslul: TIntegerField
      FieldName = 'Codemaslul'
      Origin = 'DB_KUPA."ShipTnua.DB".Codemaslul'
      OnValidate = AtmRxQuery_KabHescodemaslulValidate
    end
    object AtmRxQuery_KabHesFromNum: TStringField
      FieldName = 'FromNum'
      Origin = 'DB_KUPA."ShipTnua.DB".FromNum'
      OnChange = AtmRxQuery_KabHesToNumChange
      Size = 15
    end
    object AtmRxQuery_KabHesToNum: TStringField
      FieldName = 'ToNum'
      Origin = 'DB_KUPA."ShipTnua.DB".ToNum'
      OnChange = AtmRxQuery_KabHesToNumChange
      Size = 15
    end
    object AtmRxQuery_KabHeskamutlemeholel: TBCDField
      FieldName = 'kamutlemeholel'
      Origin = 'DB_KUPA."ShipTnua.DB".Kamutlemeholel'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesLakNehag: TIntegerField
      FieldName = 'LakNehag'
      Origin = 'DB_KUPA."ShipTnua.DB".LakNehag'
    end
    object AtmRxQuery_KabHesMaamPercent: TBCDField
      FieldName = 'MaamPercent'
      Origin = 'DB_KUPA."ShipTnua.DB".MaamPercent'
      Precision = 32
      Size = 3
    end
    object AtmRxQuery_KabHesTotalMaam: TBCDField
      FieldName = 'TotalMaam'
      Origin = 'DB_KUPA."ShipTnua.DB".TotalMaam'
      Precision = 32
      Size = 3
    end
  end
  object UpdateSQL_ZikuyAv: TUpdateSQL
    ModifySQL.Strings = (
      'update ZikuyAv'
      'set'
      '  YehusYear = :YehusYear,'
      '  KabalaNumber = :KabalaNumber,'
      '  KabalaKind = :KabalaKind,'
      '  DateKabala = :DateKabala,'
      '  CodeLakoach = :CodeLakoach,'
      '  ShemLakoach = :ShemLakoach,'
      '  Ktovet = :Ktovet,'
      '  HesbonChiuv = :HesbonChiuv,'
      '  HesbonZikuy = :HesbonZikuy,'
      '  HesbonMasMakor = :HesbonMasMakor,'
      '  KodBItzua = :KodBItzua,'
      '  MisparHavara = :MisparHavara,'
      '  TotalSumKabla = :TotalSumKabla,'
      '  Pratim = :Pratim,'
      '  HaavaraBankait = :HaavaraBankait,'
      '  ShuraKopa = :ShuraKopa,'
      '  CodeHaavaraToHan = :CodeHaavaraToHan,'
      '  TotalZikuy = :TotalZikuy,'
      '  LastUpdate = :LastUpdate,'
      '  MaklidName = :MaklidName,'
      '  TotalVat = :TotalVat,'
      '  TotalPlusVat = :TotalPlusVat,'
      '  MamPercent = :MamPercent,'
      '  DatePiraon = :DatePiraon,'
      '  KodMatbea=:KodMatbea'
      ''
      'where'
      '  YehusYear = :OLD_YehusYear '
      ' and'
      '  KabalaNumber = :OLD_KabalaNumber'
      ' ')
    InsertSQL.Strings = (
      'insert into ZikuyAv'
      '  (YehusYear, KabalaNumber, KabalaKind, DateKabala, CodeLakoach,'
      'ShemLakoach,'
      
        '   Ktovet, HesbonChiuv, HesbonZikuy, HesbonMasMakor, KodBItzua, ' +
        'MisparHavara,'
      
        '   TotalSumKabla, Pratim, HaavaraBankait, ShuraKopa, CodeHaavara' +
        'ToHan,'
      
        '   TotalZikuy, LastUpdate, MaklidName, TotalVat, TotalPlusVat, M' +
        'amPercent '
      ',DatePiraon,KodMatbea)'
      ''
      'values'
      
        '  (:YehusYear, :KabalaNumber, :KabalaKind, :DateKabala, :CodeLak' +
        'oach,'
      ':ShemLakoach,'
      
        '   :Ktovet, :HesbonChiuv, :HesbonZikuy, :HesbonMasMakor, :KodBIt' +
        'zua,'
      ':MisparHavara,'
      
        '   :TotalSumKabla, :Pratim, :HaavaraBankait, :ShuraKopa, :CodeHa' +
        'avaraToHan,'
      
        '   :TotalZikuy, :LastUpdate, :MaklidName, :TotalVat, :TotalPlusV' +
        'at, :MamPercent '
      ',:DatePiraon,:KodMatbea)'
      '')
    DeleteSQL.Strings = (
      'delete from ZikuyAv'
      'where'
      '  YehusYear = :OLD_YehusYear and'
      '  KabalaNumber = :OLD_KabalaNumber ')
    Left = 40
    Top = 224
  end
  object UpdateSQL_Zikuy: TUpdateSQL
    ModifySQL.Strings = (
      'update Zikuy'
      'set'
      '  KabalaNumber = :KabalaNumber,'
      '  KabalaYear = :KabalaYear,'
      '  SchumHesbonit = :SchumHesbonit,'
      '  DateHesbonit = :DateHesbonit,'
      '  KamutHesbonit = :KamutHesbonit,'
      '  PratimHesbonit = :PratimHesbonit,'
      '  TotalSchum = :TotalSchum,'
      '  DateKabala = :DateKabala,'
      '  CodeLakoach = :CodeLakoach,'
      '  ShemLakoach = :ShemLakoach,'
      '  LakoachGroup = :LakoachGroup,'
      '  Codebitzua = :Codebitzua,'
      '  CodeMaslul=:CodeMaslul,'
      '  FromNum = :FromNum,'
      '  ToNum = :ToNum,'
      '  Kamutlemeholel =:Kamutlemeholel,'
      '  LakNehag=:LakNehag,'
      '  MaamPercent=:MaamPercent,'
      '  TotalMaam=:TotalMaam'
      ''
      'where'
      '  Tnua = :OLD_Tnua '
      '  '
      ' ')
    InsertSQL.Strings = (
      'insert into Zikuy'
      
        '  ( KabalaNumber, KabalaYear, SchumHesbonit, DateHesbonit, Kamut' +
        'Hesbonit, '
      
        '   PratimHesbonit, TotalSchum, DateKabala, CodeLakoach, ShemLako' +
        'ach, '
      'LakoachGroup, '
      '   Codebitzua, CodeMaslul,FromNum, ToNum,Kamutlemeholel,'
      '   LakNehag,  MaamPercent,  TotalMaam)'
      ''
      ''
      'values'
      '  (:KabalaNumber, :KabalaYear, :SchumHesbonit, :DateHesbonit, '
      ':KamutHesbonit, '
      
        '   :PratimHesbonit, :TotalSchum, :DateKabala, :CodeLakoach, :She' +
        'mLakoach, '
      
        '   :LakoachGroup, :Codebitzua,:CodeMaslul, :FromNum, :ToNum,:Kam' +
        'utlemeholel,'
      '   :LakNehag, :MaamPercent,  :TotalMaam)'
      ' ')
    DeleteSQL.Strings = (
      'delete from Zikuy'
      'where'
      '  Tnua = :OLD_Tnua')
    Left = 224
    Top = 248
  end
  object Qry_Temp1: TQuery
    DatabaseName = 'DB_Kupa'
    UniDirectional = True
    UpdateMode = upWhereChanged
    Left = 116
    Top = 86
  end
  object Query_InsertStock: TQuery
    CachedUpdates = True
    DatabaseName = 'DB_Kupa'
    SQL.Strings = (
      'Select * from Stock'
      'Where NumberNoteBook = -1')
    UpdateMode = upWhereChanged
    UpdateObject = UpdateSQL_InsertStock
    Left = 245
    Top = 22
  end
  object UpdateSQL_InsertStock: TUpdateSQL
    ModifySQL.Strings = (
      'update Stock'
      'set'
      '  NumberNoteBook = :NumberNoteBook,'
      '  NumberShovar = :NumberShovar,'
      '  DateShovar = :DateShovar,'
      '  QuantityIn = :QuntityIn,'
      '  QuantityOut = :QuntityOut,'
      '  ShibKind = :ShibKind,'
      '  KodParit = :KodParit,'
      '  Tuda = :Tuda,'
      '  Asmcta = :Asmcta,'
      '  LakNo = :LakNo,'
      '  DriverNo=: DriverNo,'
      '  CarNum = :CarNum,'
      '  Kod_Kvutza_Lakoach,'
      '  Hesbonit = :hesbonit,'
      '  Int1 = :Int1,'
      '  Int2 = :Int2,'
      '  Float1 = :Float1,'
      '  Float2 = :Float2,'
      '  String1 = :String1,'
      '  String2 = :String2,'
      '  Date1 = :Date1,'
      '  Date2 = :Date2'
      'where'
      '  NumberNoteBook = :OLD_NumberNoteBook and'
      '  NumberShovar = :OLD_NumberShovar and'
      '  DateShovar = :OLD_DateShovar')
    InsertSQL.Strings = (
      'insert into Stock'
      
        '  (NumberNoteBook, NumberShovar, DateShovar, QuantityIn, Quantit' +
        'yOut, ShibKind, '
      
        '   KodParit, Tuda, Asmcta, LakNo, DriverNo,  CarNum,Kod_Kvutza_L' +
        'akoach, Hesbonit ,'
      '   Int1, Int2, Float1, '
      '   Float2, String1, String2, Date1, Date2)'
      'values'
      
        '  (:NumberNoteBook, :NumberShovar, :DateShovar, :QuantityIn, :Qu' +
        'antityOut, '
      
        '   :ShibKind, :KodParit, :Tuda, :Asmcta, :LakNo, :DriverNo, :Car' +
        'Num,  '
      '   :Kod_Kvutza_Lakoach, :Hesbonit, :Int1, '
      '   :Int2, :Float1, :Float2, :String1, :String2, :Date1, :Date2)')
    DeleteSQL.Strings = (
      'delete from Stock'
      'where'
      '  NumberNoteBook = :OLD_NumberNoteBook and'
      '  NumberShovar = :OLD_NumberShovar and'
      '  DateShovar = :OLD_DateShovar')
    Left = 222
    Top = 76
  end
end

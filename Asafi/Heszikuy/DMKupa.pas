unit DMKupa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, RxQuery, AtmRxQuery,IniFiles;

type
  TDM_Kupa = class(TDataModule)
    Database_Kupa: TDatabase;
    Qry_Temp: TQuery;
    AtmRxQuery_ZikuyAv: TAtmRxQuery;
    DS_ZikuyAv: TDataSource;
    Qry_AtmIndexKabala: TQuery;
    UpdateSQL_AtmIndex: TUpdateSQL;
    DS_KabHes: TDataSource;
    Qry_Mname: TQuery;
    AtmRxQuery_KabHes: TAtmRxQuery;
    UpdateSQL_ZikuyAv: TUpdateSQL;
    AtmRxQuery_ZikuyAvYehusYear: TIntegerField;
    AtmRxQuery_ZikuyAvKabalaNumber: TIntegerField;
    AtmRxQuery_ZikuyAvKabalaKind: TIntegerField;
    AtmRxQuery_ZikuyAvDateKabala: TDateTimeField;
    AtmRxQuery_ZikuyAvCodeLakoach: TIntegerField;
    AtmRxQuery_ZikuyAvShemLakoach: TStringField;
    AtmRxQuery_ZikuyAvKtovet: TStringField;
    AtmRxQuery_ZikuyAvHesbonChiuv: TStringField;
    AtmRxQuery_ZikuyAvHesbonZikuy: TStringField;
    AtmRxQuery_ZikuyAvHesbonMasMakor: TStringField;
    AtmRxQuery_ZikuyAvKodBItzua: TIntegerField;
    AtmRxQuery_ZikuyAvMisparHavara: TIntegerField;
    AtmRxQuery_ZikuyAvTotalSumKabla: TBCDField;
    AtmRxQuery_ZikuyAvPratim: TStringField;
    AtmRxQuery_ZikuyAvHaavaraBankait: TIntegerField;
    AtmRxQuery_ZikuyAvShuraKopa: TIntegerField;
    AtmRxQuery_ZikuyAvCodeHaavaraToHan: TIntegerField;
    AtmRxQuery_ZikuyAvTotalZikuy: TBCDField;
    AtmRxQuery_ZikuyAvLastUpdate: TDateTimeField;
    AtmRxQuery_ZikuyAvMaklidName: TStringField;
    AtmRxQuery_ZikuyAvTotalVat: TBCDField;
    AtmRxQuery_ZikuyAvTotalPlusVat: TBCDField;
    AtmRxQuery_ZikuyAvMamPercent: TBCDField;
    UpdateSQL_Zikuy: TUpdateSQL;
    AtmRxQuery_KabHesTnua: TIntegerField;
    AtmRxQuery_KabHesKabalaNumber: TIntegerField;
    AtmRxQuery_KabHesKabalaYear: TIntegerField;
    AtmRxQuery_KabHesSchumHesbonit: TBCDField;
    AtmRxQuery_KabHesDateHesbonit: TDateTimeField;
    AtmRxQuery_KabHesKamutHesbonit: TBCDField;
    AtmRxQuery_KabHesPratimHesbonit: TStringField;
    AtmRxQuery_KabHesTotalSchum: TBCDField;
    AtmRxQuery_KabHesDateKabala: TDateTimeField;
    AtmRxQuery_KabHesCodeLakoach: TIntegerField;
    AtmRxQuery_KabHesShemLakoach: TStringField;
    AtmRxQuery_KabHesLakoachGroup: TIntegerField;
    AtmRxQuery_KabHesCodebitzua: TIntegerField;
    AtmRxQuery_KabHesCodemaslul: TIntegerField;
    AtmRxQuery_KabHesFromNum: TStringField;
    AtmRxQuery_KabHesToNum: TStringField;
    AtmRxQuery_KabHeskamutlemeholel: TBCDField;
    AtmRxQuery_KabHesLakNehag: TIntegerField;
    AtmRxQuery_KabHesMaamPercent: TBCDField;
    AtmRxQuery_KabHesTotalMaam: TBCDField;
    Qry_Temp1: TQuery;
    AtmRxQuery_ZikuyAvDatePiraon: TDateTimeField;
    AtmRxQuery_ZikuyAvKodMatbea: TIntegerField;
    Query_InsertStock: TQuery;
    UpdateSQL_InsertStock: TUpdateSQL;
    AtmRxQuery_ZikuyAvLookKvoLak: TStringField;

    procedure Database_KupaLogin(Database: TDatabase;
      LoginParams: TStrings);
    procedure DataModuleDestroy(Sender: TObject);
    procedure AtmRxQuery_ZikuyAvAfterScroll(DataSet: TDataSet);
    procedure AtmRxQuery_ZikuyAvCodeLakoachValidate(Sender: TField);
    procedure AtmRxQuery_ZikuyAvBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesBeforePost(DataSet: TDataSet);
    procedure AtmRxQuery_ZikuyAvAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesAfterPost(DataSet: TDataSet);
    procedure AtmRxQuery_ZikuyAvBeforeInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesBeforeEdit(DataSet: TDataSet);
    procedure AtmRxQuery_ZikuyAvBeforeDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure AtmRxQuery_ZikuyAvDateKabalaValidate(Sender: TField);
    procedure Database_KupaAfterConnect(Sender: TObject);
    procedure AtmRxQuery_ZikuyAvBeforeScroll(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesAfterInsert(DataSet: TDataSet);
    procedure AtmRxQuery_KabHesSchumHesbonitValidate(Sender: TField);
    procedure AtmRxQuery_KabHesBeforeDelete(DataSet: TDataSet);
    procedure AtmRxQuery_KabHescodemaslulValidate(Sender: TField);
    procedure AtmRxQuery_KabHesToNumChange(Sender: TField);
    procedure AtmRxQuery_KabHesKamutHesbonitChange(Sender: TField);
    procedure AtmRxQuery_ZikuyAvShuraKopaValidate(Sender: TField);
    procedure AtmRxQuery_KabHesAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    NewKabalaSavedNumber:LongInt;
    ItraForLak :Real;
    KabHesWasInsert :Boolean;
    KaBhesWasDelete :Boolean;
    Procedure FillKabalaFrmLookupFields;    
    Function  GetNextKabalaNumber(Increment :Integer):LongInt;
    Function  GetLastKabalaNumber:LongInt;
    Function  CalcTotalForSgira:Real;
    Function  CalcMaamForSgira:Real;
    Procedure CalcTotalHesbonit;
    Procedure UpdateMlayPritim;
    procedure BuildLineInMakav;
    Procedure BuildLinesInStockForCurChecks;
    Function  HesbonitInKabHes(KabalaNo,KabalaYear,ParitNo :LongInt):Boolean;
    Function  PritInMlay(ParitNo,SnifNo :LongInt):Boolean;
    Function  CalcDateTashlum(FromDate :TDateTime;PayCond :ShortString) : TDateTime;
  end;

//Procedure InitHafkadaForms(AUserName,APassword,AAlias,KabalaType:PChar);
//Procedure DoneHafkadaForms;
  Procedure DoKabala(AUserName,APassword,AAlias,ScriptsDir,TypeKabala:PChar);

  Procedure InitAtmCrt(AliasName,DirForIni:ShortString);External 'AtmCrt.Dll';
  Procedure OpenCrt(FormNum :LongInt;MainValue :ShortString);External 'AtmCrt.Dll';
  Procedure DoneAtmCrt;External 'AtmCrt.Dll';

var
  DM_Kupa: TDM_Kupa;
  KupaAlias,KupaDBUserName,KupaDBPassword:String;
  CurKabalaYear         :LongInt;
  KabalaScriptsDir      :String;
  KupaDllIniFileName    :String;
  KupaDllScriptsDir     :String;
  Kabala_type,KodMatbea,
  Snif,Mispar,LastNum,United,
  Fnum,TNum,TopLines    :Integer;
  Hadpasa,SugHadpasa,Egul : String[1];
  CopysKabala             : String[2];
  TypeOfFile              : Shortstring;
  ShemMadpis              : Shortstring;
  Maam,
  TotalSagur,SumMakor,
  SumEgul               : Real;
  Atzmada,MaamPrit,Schum,
  KamutMin,KamutMlay,
  KamutTon              : Real;
  NewYear               : Boolean;
  Atmini                : Tinifile;

implementation

uses  AtmConst, AtmRutin, F_Kabala;

{$R *.DFM}


Procedure DoKabala(AUserName,APassword,AAlias,ScriptsDir,TypeKabala:PChar);
Var
  FreeDM:Boolean;
Begin
  Try
    Screen.Cursor:=crHourGlass;
    Try
      KupaAlias:=StrPas(AAlias);
      KupaDBUserName:=StrPas(AUserName);
      KupaDBPassword:=StrPas(APassword);
      KabalaScriptsDir:=StrPas(ScriptsDir);
      Kabala_type:=StrToint(StrPas(TypeKabala));
      FreeDM:=False;
      if DM_Kupa=Nil Then
      Begin
        DM_Kupa:=TDm_Kupa.Create(Nil);
        FreeDM:=True;
      End;
      if Frm_Kabala=Nil Then
        Frm_Kabala:=TFrm_Kabala.Create(Nil);

      Frm_Kabala.InitKabala;
      InitAtmCrt(KupaAlias,ExtractFilePath(Application.ExeName));
    Finally
      Screen.Cursor:=crDefault;
    End;
    Frm_Kabala.ShowModal;
    DoneAtmCrt;
    if FreeDM Then
    Begin
      DM_Kupa.Free;
      DM_Kupa:=Nil;
    End;
    if Frm_Kabala<>Nil Then
    Begin
      Frm_Kabala.Free;
      Frm_Kabala:=Nil;
    End;
  Except On E:Exception Do
    ShowMessage(E.Message);
  End;
End;


procedure TDM_Kupa.Database_KupaLogin(Database: TDatabase;
  LoginParams: TStrings);
begin
    LoginParams.Clear;
    LoginParams.Add('USER NAME='+KupaDBUserName);
    LoginParams.Add('PASSWORD='+KupaDBPassword);
end;

procedure TDM_Kupa.DataModuleDestroy(Sender: TObject);
Var
  I :LongInt;
begin
  For I:=0 To ComponentCount-1 Do
    if Components[i] is TDataSet Then
      TDataSet(Components[i]).Close;
end;


procedure TDM_Kupa.AtmRxQuery_ZikuyAvAfterScroll(DataSet: TDataSet);
Var
hstr   :  String;
begin
  NewKabalaSavedNumber:=-1;
  AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_ZikuyAv.FieldByName('MamPercent').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_ZikuyAv.FieldByName('TotalVat').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_ZikuyAv.FieldByName('DateKabala').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_ZikuyAv.FieldByName('Pratim').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_ZikuyAv.FieldByName('ShuraKopa').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('KamutHesbonit').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('FromNum').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;
  AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0;


  AtmRxQuery_KabHes.Close;
  AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsInteger;
  AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsInteger;
  AtmRxQuery_KabHes.Open;
  KabHesWasInsert:=False;

  Frm_Kabala.TotalBeforeMaam.Text:=FloattoStr(
      AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat-
      AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat);

  Case Kabala_type Of
    0 : HStr:='���� ';
    1 : HStr:='����� ';
    2 : HStr:='�� ';
  End;
  Frm_Kabala.Caption:='������� / '+HStr+IntToStr(CurKabalaYear);
end;

procedure TDM_Kupa.AtmRxQuery_ZikuyAvCodeLakoachValidate(Sender: TField);

begin
    With Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
//    If Kabala_type=10 Then
      If TypeOfFile='���' Then
      Begin
        Sql.Add('Select Shem_Nehag,Mis_Han,Kod_Tnai_Tashlum,K.Teur_tavla From Nehag,Kodtavla K');
        Sql.Add('Where Kod_Nehag='+AtmRxQuery_ZikuyAvCodeLakoach.AsString);
        Sql.Add('And sug_tavla=12 And Kod_Tnai_Tashlum=K.Kod_Tavla');
      End
      Else
      Begin
        Sql.Add('Select Shem_Lakoach,Mis_Han,Kod_Atzmada,Kod_Kvutza,Kod_Tnai_Tashlum,K.Teur_tavla From Lakoach,Kodtavla K');
        Sql.Add('Where Kod_Lakoach='+AtmRxQuery_ZikuyAvCodeLakoach.AsString);
        Sql.Add('And sug_tavla=12 And Kod_Tnai_Tashlum=K.Kod_Tavla');
      End;
      Open;
      if Not Eof And Bof Then
      Begin
//      If Kabala_type=10 Then
        If TypeOfFile='���' Then
        Begin
          AtmRxQuery_ZikuyAv.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_Nehag').asstring;
          AtmRxQuery_ZikuyAv.fieldbyname('HesbonMasMakor').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
        End
        Else
        Begin
          Atzmada:=Qry_Temp.fieldbyname('Kod_Atzmada').AsInteger;        
          AtmRxQuery_ZikuyAv.fieldbyname('shemlakoach').asstring:=Qry_Temp.fieldbyname('shem_lakoach').asstring;
          AtmRxQuery_ZikuyAv.fieldbyname('HesbonMasMakor').asstring:=Qry_Temp.fieldbyname('Mis_Han').asstring;
          AtmRxQuery_ZikuyAv.fieldbyname('ShuraKopa').AsInteger:=
            Qry_Temp.fieldbyname('Kod_Kvutza').AsInteger;
        End;
          AtmRxQuery_ZikuyAv.fieldbyname('DatePiraon').Value :=
             CalcDateTashlum(AtmRxQuery_ZikuyAv.fieldbyname('DateKabala').AsDateTime
             ,Qry_Temp.FieldByName('Teur_Tavla').asString);
          Close;
      End
      Else
      Begin
         Close;
         ShowMessage('���� �� ����');
         abort;
      End;
{
      If (Kabala_type=0) And (Atzmada=2) Then
      Begin
        ShowMessage('���� ����� ������ ');
        Abort;
      End;
      If (Kabala_type=1) And (Atzmada<>2) Then
      Begin
        ShowMessage('���� ����� ������');
        Abort;
      End;
}
//   If (Kabala_type=1) Or (Atzmada=2) Then
//      AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=0
//   Else
        AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=Maam;
    End;
end;


Function TDM_Kupa.GetNextKabalaNumber(Increment :Integer):LongInt;
Var
  CurYear  : Word;
  Res,Code : LongInt;
  StrH,Strk: String;
Begin
  Result:=0;
  CurYear:=CurKabalaYear;
  With Qry_AtmIndexKabala Do
  Begin
    If United<>-1 Then StrK:=aiHeshbonitMas+IntToStr(United)
    ELse StrK:=aiHeshbonitMas;

    ParamByName('PYehusYear').AsInteger:=CurYear;
    ParamByName('PKey_String').AsString:=StrK; // aiHeshbonitMas;

    Open;
    if Eof And Bof Then //��� ����
    Begin

    StrH:='15000';
    Repeat
      if Not InputQuery(IntTostr(CurYear)+'��� ���� ', '������ ����� ������� ',StrH) Then
        Exit;     Val(StrH,Res,Code);
    Until Code=0;

      Insert;
      FieldByName('Last_Number').AsInteger:=Res; // 1
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=ParamByName('PKey_String').AsString;
      //aiTeodatMishloach;
      FieldByName('YehusYear').AsInteger:=CurYear;
      Post;
      Result:=1;
    End
    Else
    Begin//�����
      Res:=FieldByName('Last_Number').AsInteger+Increment;
      Edit;
      FieldByName('Last_Number').AsInteger:=Res;
      Post;
      Result:=Res;
    End;
    Qry_AtmIndexKabala.ApplyUpdates;
    Close;
  End;
End;

Function TDM_Kupa.GetLastKabalaNumber:LongInt;
Var
  CurYear : Word;
  Strk    : String;
Begin
  Result:=0;
  With Qry_AtmIndexKabala Do
  Begin
    If United<>-1 Then StrK:=aiHeshbonitMas+IntToStr(United)
    ELse StrK:=aiHeshbonitMas;

    ParamByName('PKey_String').AsString:=StrK;
    ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
    Open;
    Result:=FieldByName('Last_Number').AsInteger;
    Close;
  End;
End;

Procedure TDM_Kupa.AtmRxQuery_ZikuyAvBeforePost(DataSet: TDataSet);
Begin
    if (AtmRxQuery_ZikuyAv.State=dsInsert)
//   And (not AtmRxQuery_ZikuyAvCodeLakoach.IsNull)
    Then
  Begin
    If AtmRxQuery_ZikuyAvCodeLakoach.IsNull Then Abort;
    AtmRxQuery_ZikuyAvDateKabala.AsDateTime:=Trunc(AtmRxQuery_ZikuyAvDateKabala.AsDateTime);
    AtmRxQuery_ZikuyAvKabalaNumber.AsInteger:=GetNextKabalaNumber(+1); //David 
    AtmRxQuery_ZikuyAvYehusYear.AsInteger:=CurKabalaYear;
    AtmRxQuery_ZikuyAvKabalaKind.AsInteger:=Kabala_type;
    AtmRxQuery_ZikuyAvHesbonChiuv.AsString:=TypeOfFile;
    If Kabala_type <> 2 Then
      AtmRxQuery_ZikuyAvMisparHavara.AsInteger:=Snif;
//    :='���'
    NewKabalaSavedNumber:=AtmRxQuery_ZikuyAvKabalaNumber.AsInteger;
  End;
End;

Procedure TDM_Kupa.AtmRxQuery_KabHesBeforePost(DataSet: TDataSet);
Var
  TotalSagur  :Real;
  Kamut       :Real;

Begin
  With AtmRxQuery_KabHes Do
  Begin
    if AtmRxQuery_KabHes.FieldByName('PratimHesbonit').Asstring='' Then
    Begin
      Cancel;
      Abort;
    End;

    If Not AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').IsNull Then
    Begin
      KabHesWasInsert:=(State=dsInsert);
      FieldByName('KabalaNumber').AsInteger:=AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsInteger;
      FieldByName('KabalaYear').AsInteger:=AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsInteger;
      FieldByName('shemlakoach').asstring:=
         AtmRxQuery_ZikuyAv.fieldbyname('shemlakoach').asstring;
      FieldByName('CodeLakoach').AsInteger:=
         AtmRxQuery_ZikuyAv.fieldbyname('CodeLakoach').AsInteger;
      FieldByName('DateKabala').AsDateTime:=
         AtmRxQuery_ZikuyAvDateKabala.AsDateTime;
      If AtmRxQuery_ZikuyAvKabalaKind.AsInteger =1 Then
        Kamut:=FieldByName('KamutHesbonit').AsCurrency*-1
      Else
        Kamut:=FieldByName('KamutHesbonit').AsCurrency;
//    FieldByName('KamutLeMeholel').AsCurrency:=Kamut;
      If TypeOfFile='���' Then FieldByName('LakNehag').AsInteger:=1
      else
         FieldByName('LakNehag').AsInteger:=0;
//    If MaamPrit=0 Then
      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('MaamPercent').AsFloat:=Maam;
//    Else
//      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('MaamPercent').AsFloat:=0;
      Kamut:=
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat*
        (1+DM_Kupa.AtmRxQuery_KabHes.FieldByName('MaamPercent').AsFloat/100);
//    Kamut:=Round((Kamut+0.005)*100)/100 - // 24-3-2002
      Kamut:=Int((Kamut+0.005)*100)/100 -
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat;
//    If SumMakor<>0 Then                           //  KamutHesbonit
//       Kamut:=SumMakor*AtmRxQuery_KabHes.FieldByName('Kamutlemeholel').AsCurrency
//      -DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat;

      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalMaam').AsFloat:=Kamut;
      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('LakoachGroup').Asinteger:=Snif;
    End;
  End;

  if AtmRxQuery_KabHes.State=dsEdit Then
  Begin
//    TotalSagur:=TotalSagur+AtmRxQuery_KabHes.FieldByName('SchumTashlom').AsFloat;
  End;
end;

Function TDM_Kupa.CalcTotalForSgira:Real;
Begin
  Screen.Cursor:=crSQLWait;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(TotalSchum) ST From Zikuy');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsString);
    Open;
    Result:=FieldByName('ST').AsFloat;
    Close;
  End;
  Screen.Cursor:=crDefault;
End;

Function TDM_Kupa.CalcMaamForSgira:Real;
Begin
  Screen.Cursor:=crSQLWait;
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Sum(TotalMaam) ST From Zikuy');
    Sql.Add('Where KabalaNumber = '+AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsString);
    Sql.Add('And KabalaYear = '+AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsString);
    Open;
    Result:=FieldByName('ST').AsFloat;
    Close;
  End;
  Screen.Cursor:=crDefault;
End;

Function  TDM_Kupa.PritInMlay(ParitNo,SnifNo :LongInt):Boolean;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Count(*) C From Maslul');
    Sql.Add('Where Code_Maslul = '+IntToStr(ParitNo));
    Sql.Add('And Code_Pizul = '+IntToStr(SnifNo));
    Open;
    Result:=FieldByName('C').AsInteger > 0;
    Close;
  End;
End;


Function TDM_Kupa.HesbonitInKabHes(KabalaNo,KabalaYear,ParitNo :LongInt):Boolean;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Count(*) C From Zikuy');
    Sql.Add('Where KabalaNumber = '+IntToStr(KabalaNo));
    Sql.Add('And KabalaYear = '+IntToStr(KabalaYear));
    Sql.Add('And CodeMaslul = '+IntToStr(ParitNo));
    Open;
    Result:=FieldByName('C').AsInteger > 0;
    Close;
  End;
End;




procedure TDM_Kupa.AtmRxQuery_ZikuyAvAfterInsert(DataSet: TDataSet);
begin
  ItraForLak:=0;
  FindFirstControlInTabOrder(Frm_Kabala.Panel_Kabala).SetFocus;
  NewKabalaSavedNumber:=-1;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesAfterPost(DataSet: TDataSet);
Var
  StrH :String;
  Sum  :Real;

begin
  Fnum:=0;
  Tnum:=0;
  if (AtmRxQuery_KabHes.FieldByName('Tnua').AsInteger=0) And
     (Not KaBhesWasDelete)
  Then
  Begin
    AtmRxQuery_KabHes.Close;
    AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsInteger;
    AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsInteger;

    AtmRxQuery_KabHes.Open;
    KabHesWasInsert:=False;
    AtmRxQuery_KabHes.Last;
  End;

  DM_Kupa.KaBhesWasDelete:=False;
  AtmRxQuery_ZikuyAv.Edit;
  If AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger=0 Then
     CalcTotalHesbonit;

  if Egul='�' Then Frm_Kabala.EgulMaam; // David 9-4-2002

  AtmRxQuery_ZikuyAv.FieldByName('TotalZikuy').AsFloat:=
       AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat;
//     TotalSagur; //CalcTotalForSgira;
    //����� �����
  AtmRxQuery_ZikuyAv.Post;
end;

Procedure TDM_Kupa.CalcTotalHesbonit;

Begin
  TotalSagur:=CalcTotalForSgira;
//If (Kabala_type=1) Or (Atzmada=2) Then
  If Atzmada=2 Then
  Begin
     AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat:=0;
     AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=0;
     AtmRxQuery_ZikuyAv.FieldByName('KodMatbea').ASinteger:=2;
  End
  Else
  Begin
     AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=Maam;
     AtmRxQuery_ZikuyAv.FieldByName('KodMatbea').ASinteger:=0;
//   If (AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat <> 0) Or
//      (AtmRxQuery_ZikuyAv.FieldByName('TotalVat').IsNull)
//   Then
//   AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat:=(Round((TotalSagur*
//       Maam/100)*100)/100);
     if Egul<>'�' Then
       AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat:=CalcMaamForSgira;
//   TotalSagur;
  End;
  AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat:=TotalSagur+
      AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat;
  Frm_Kabala.TotalBeforeMaam.Text:=FloattoStr(
      AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat-
      AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat);
End;


procedure TDM_Kupa.BuildLineInMakav;
Var
  LakGroup         :LongInt;
  TotalMasBamakor,
  TotalLchiuv      : Real;
  Month,Year,Day   : Word;
Begin
  DecodeDate(AtmRxQuery_Zikuyav.FieldByName('DateKabala').AsDateTime,
             Year,Month,Day);
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Kod_Kvutza From Lakoach');
    Sql.Add('Where Kod_Lakoach = '+AtmRxQuery_ZikuyavCodeLakoach.AsString);
    Open;
    LakGroup:=FieldByName('Kod_Kvutza').AsInteger;
    Close;
    Sql.Clear;
    If AtmRxQuery_ZikuyAvHesbonChiuv.AsString='����' Then
      Sql.Add('Insert into Makav')
    Else
      Sql.Add('Insert into MakavNhg');
//  Sql.Add('(HesbonitNumber,HesbonitKind,YehusYear,CodeLakoach,LakoachGroup,HesbonitDate,TotalHesbonitWithMAM,Status,Total_Zikuy,Date_Zikuy,DateHafakatHeshbonit,HanForLak,HanForMam,String_1,PratimToHan,TotalMasMakor,Last_Update,CodeHaavaraToHan,Mis_Kabala1,OrderNum,DateForFirstPayment)');
    Sql.Add('(');
    Sql.Add('HesbonitNumber,HesbonitKind,YehusYear,CodeLakoach,LakoachGroup,HesbonitDate,TotalHesbonitWithMAM,Status,DateHafakatHeshbonit,');
    Sql.Add('HanForLak,HanForMam,PratimToHan,TotalMasMakor,Last_Update,CodeHaavaraToHan,Mamhesbonit,Mampercent,');
    Sql.Add('TotalSumFromTnua,TotalSumPriceKamut,YehusMonth,Kod_Sapak,DatePiraon,Total_Zikuy)');

    Sql.Add('Values');
    Sql.Add('('+AtmRxQuery_Zikuyav.FieldByName('KabalaNumber').AsString);
    If AtmRxQuery_ZikuyAv.FieldByName('KodMatbea').ASinteger=2
    Then
      Sql.Add(','+IntToStr(HkHiyuvZikuyDolar))//�-� ���� /�����/�� ������
    Else
      Sql.Add(','+IntToStr(HkHiyuvZikuyShekel)); //�-� ���� /�����/�� ������
    Sql.Add(','+AtmRxQuery_Zikuyav.FieldByName('YehusYear').AsString);
    Sql.Add(','+AtmRxQuery_Zikuyav.FieldByName('CodeLakoach').AsString);
    Sql.Add(','+IntToStr(LakGroup));//����� ����
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Zikuyav.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    TotalLchiuv:=AtmRxQuery_Zikuyav.FieldByName('TotalPlusVat').AsCurrency;
    If Kabala_type=1 Then TotalLchiuv:=TotalLchiuv*-1;
    Sql.Add(','+FloatToStr(TotalLchiuv));
    Sql.Add(',0'); //Status
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Zikuyav.FieldByName('DateKabala').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(','+QuotedStr(AtmRxQuery_Zikuyav.FieldByName('HesbonZikuy').AsString));
    Sql.Add(','+QuotedStr(AtmRxQuery_Zikuyav.FieldByName('HesbonMasMakor').AsString));
    Sql.Add(','''+Copy(AddQuatesToString(AtmRxQuery_Zikuyav.FieldByName('Pratim').AsString),1,30)+'''');//PratimHesbonit
    Sql.Add(','+FloatToStr(TotalMasBamakor));
    Sql.Add(','''+DateToSqlStr(Now,'mm/dd/yyyy')+'''');
    Sql.Add(',0');
    TotalLchiuv:=AtmRxQuery_Zikuyav.FieldByName('TotalVat').AsCurrency;
    If Kabala_type=1 Then TotalLchiuv:=TotalLchiuv*-1;
    Sql.Add(','''+FloatToStr(TotalLchiuv)+'''');
    Sql.Add(','''+AtmRxQuery_Zikuyav.FieldByName('MamPercent').AsString+'''');
    TotalLchiuv:=StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
    If Kabala_type=1 Then TotalLchiuv:=TotalLchiuv*-1;
    Sql.Add(','''+FloatToStr(TotalLchiuv)+'''');
    Sql.Add(','''+FloatToStr(TotalLchiuv)+'''');
    Sql.Add(','+FloatToStr(Month));
    If Snif<>-1 Then
       Sql.Add(','+IntToStr(Snif))
    Else
       Sql.Add(',0');
    Sql.Add(','''+DateToSqlStr(AtmRxQuery_Zikuyav.FieldByName('DatePiraon').AsDateTime,'mm/dd/yyyy')+'''');
    Sql.Add(',0'); // Total_zikuy
    Sql.Add(')');
    Sql.SaveToFile('c:\BuildMak.Sql');
    ExecSQL;
  End;
End;



Procedure TDM_Kupa.UpdateMlayPritim;
Begin
   AtmRxQuery_KabHes.First;
   While Not AtmRxQuery_KabHes.EOF Do
   Begin
     With Qry_Temp Do
     Begin
       Close;
       Sql.Clear;       //������ ������
       Qry_Temp1.Close;
       Qry_Temp1.Sql.Clear;
       IF (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=1) Or
          (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=3)
       Then
       Begin
         Sql.Add('Update Maslul Set Day1=Day1 -'+AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsString);
         Qry_Temp1.Sql.Add('Update Maslul Set Day6='+AtmRxQuery_KabHes.FieldByName('Tonum').AsString);
//        Sql.Add(',Day6='+AtmRxQuery_KabHes.FieldByName('Tonum').AsString);
         Sql.Add(',Date_end_Maslul='+
           ''''+DateToSqlStr(AtmRxQuery_KabHes.FieldByName('DateHesbonit').AsDateTime,'mm/dd/yyyy')+'''');
       End
       Else
       Begin
         Sql.Add('Update Maslul Set Day1=Day1 +'+AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsString);
         Qry_Temp1.Sql.Add('Update Maslul Set Day5='+AtmRxQuery_KabHes.FieldByName('Tonum').AsString);
//       Sql.Add(',Day5='+AtmRxQuery_KabHes.FieldByName('Tonum').AsString);
        IF (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=0) Then
          Sql.Add(',Hour_Start='+
          ''''+DateToSqlStr(AtmRxQuery_KabHes.FieldByName('DateHesbonit').AsDateTime,'mm/dd/yyyy')+'''');
       End;
       Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodemaslul.AsString);
       Sql.Add('And Sug_Rehev1=1');
       Sql.Add('And code_pizul=' + IntToStr(Snif) );
       Qry_Temp1.Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodemaslul.AsString);
       Qry_Temp1.Sql.Add('And Sug_Rehev1=1');
       Qry_Temp1.Sql.Add('And code_pizul=' + IntToStr(Snif) );
       IF (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=1) Or
          (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=3)
       Then
          Qry_Temp1.Sql.Add('And Day6<'+AtmRxQuery_KabHes.FieldByName('Tonum').AsString)
       Else
          Qry_Temp1.Sql.Add('And Day5<'+AtmRxQuery_KabHes.FieldByName('Tonum').AsString);
       ExecSql;
       Qry_Temp1.ExecSql;

     End; // Qry_Temp
     AtmRxQuery_KabHes.Next;
   End; // While Not AtmRxQuery_KabHes.EOF
End; // UpdateMlayPritim;


procedure TDM_Kupa.AtmRxQuery_ZikuyAvBeforeInsert(DataSet: TDataSet);
begin
  Fnum:=0;
  Tnum:=0;
  AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').ReadOnly:=False;
  AtmRxQuery_ZikuyAv.FieldByName('MamPercent').ReadOnly:=False;
  AtmRxQuery_ZikuyAv.FieldByName('TotalVat').ReadOnly:=False;
  AtmRxQuery_ZikuyAv.FieldByName('DateKabala').ReadOnly:=False;
  AtmRxQuery_ZikuyAv.FieldByName('Pratim').ReadOnly:=False;
  AtmRxQuery_ZikuyAv.FieldByName('ShuraKopa').ReadOnly:=False;


//AtmRxQuery_ZikuyAv.Edit;
//AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=Maam;
  AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ReadOnly:=False;
  AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
  AtmRxQuery_KabHes.FieldByName('KamutHesbonit').ReadOnly:=False;
end;


procedure TDM_Kupa.AtmRxQuery_KabHesBeforeEdit(DataSet: TDataSet);
begin
  if AtmRxQuery_KabHes.FieldByName('Tnua').AsInteger=0 Then
    if MessageDlg('���� ����� �� ����� ���� �������'+
             '��� ����� ���',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
    Begin
      AtmRxQuery_KabHes.Close;
      AtmRxQuery_KabHes.Open;
      KabHesWasInsert:=False;
      Abort;
    End
    Else
      Abort;
end;

Procedure TDM_Kupa.AtmRxQuery_ZikuyAvBeforeDelete(DataSet: TDataSet);
Var
heskind  :   integer;
Begin
  If AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger =9 Then
  Begin
    Showmessage('!! ������� ������');
    Abort;
  End;
  If AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0 Then
  Begin
    if MessageDlg('���� ���� ��� ���� ������� ��',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
    Begin
      With Qry_Temp Do
      Begin
        If AtmRxQuery_ZikuyAv.FieldByName('KodMatbea').ASinteger=2
        Then
          Heskind:=HkHiyuvZikuyDolar//�-� ���� /�����/�� ������
        Else
          Heskind:=HkHiyuvZikuyShekel; //�-� ���� /�����/�� ������

        Close;
        Sql.Clear;
        If AtmRxQuery_ZikuyAvHesbonChiuv.AsString='����' Then
           Sql.Add('Update Makav')
        Else
           Sql.Add('Update MakavNhg');
        Sql.Add('Set Status=9');
        Sql.Add('Where HesbonitNumber='+AtmRxQuery_Zikuyav.FieldByName('KabalaNumber').AsString);
        Sql.Add('And YehusYear='+AtmRxQuery_Zikuyav.FieldByName('YehusYear').AsString);
        Sql.Add('And HesbonitKind=' + IntTostr(Heskind));
//      Sql.SaveToFile('c:\Mak.Sql');
        ExecSQL;
//����� ��� ����� ����� ���� = -9
        Close;
        Sql.Clear;
        Sql.Add('Update Stock');
        Sql.Add('Set ShibKind=-9');
        Sql.Add('Where NumberNoteBook='+
             AtmRxQuery_Zikuyav.FieldByName('KabalaNumber').AsString);
        ExecSQL;

      End;
      If AtmRxQuery_ZikuyAv.State=dsBrowse Then
        AtmRxQuery_ZikuyAv.Edit;
      AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger:= 9;
      Abort;
      Exit;
    End
    Else
      Abort;
  End;

  if MessageDlg('��� ���� ������� ��',mtConfirmation,[mbYes,mbNo],0)=mrYes Then
  Begin
    AtmRxQuery_KabHes.First;
    While Not ((AtmRxQuery_KabHes.Eof) And (AtmRxQuery_KabHes.BOF)) Do
      AtmRxQuery_KabHes.Delete;
    if AtmRxQuery_ZikuyAv.State=dsBrowse Then
      AtmRxQuery_ZikuyAv.Edit;
    AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger := 9;
    if AtmRxQuery_ZikuyAvKabalaNumber.AsInteger<>GetLastKabalaNumber Then
      Abort
    Else
      GetNextKabalaNumber(-1);
    NewKabalaSavedNumber:=-1;
  End;

end;

procedure TDM_Kupa.DataModuleCreate(Sender: TObject);
Var
  F :TIniFile;
  Strh :String;
  i    :Integer;
begin
  if KupaDllIniFileName<>'' Then
  Begin
    F:=TIniFile.Create(KupaDllIniFileName);
    Try
      Maam:=F.ReadFloat('Main','Mam',0);
      Snif:=F.ReadInteger('Main','Snif',-1);
      United:=F.ReadInteger('Main','United',-1);
      StrH:=F.ReadString(SectionForMain,KeyAliasForMname,'');
      if StrH<>'' Then
        Qry_Mname.DatabaseName:=StrH;
    Finally
      F.Free;
    End;
  End;

  if Trim(KupaAlias)<>'' Then
  Begin
    Database_Kupa.Connected:=False;
    Database_Kupa.AliasName:=KupaAlias;
  End;
  Qry_Mname.Open;
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  ShemMadpis:=Atmini.readString('Hafkada','Hatima','');
  CopysKabala:=Atmini.readString('Hafkada','CopysOfKabala','2');
  TopLines:=Atmini.readInteger('Hafkada','TopLines',10);  
  TypeOfFile:=Atmini.readString('HesZikuy','Sug_File','����');
  Egul:=Atmini.readString('HesZikuy','Sug_Egul','�');  
  Atmini.WriteString('Hafkada','CopysOfKabala',CopysKabala);
  Atmini.free;

end;

procedure TDM_Kupa.AtmRxQuery_ZikuyAvDateKabalaValidate(Sender: TField);
Var
  DD,MM,YY :Word;
begin
  DecodeDate(Sender.AsDateTime,yy,mm,dd);
  if YY<>CurKabalaYear Then
    ShowMessage('����� �������� �� ���� �� ��� ������');
end;

procedure TDM_Kupa.Database_KupaAfterConnect(Sender: TObject);
Var
  StrH :String;
  F:TIniFile;
begin
  F:=TIniFile.Create(KupaDllIniFileName);
  StrH:=F.ReadString(SectionForMain,SQLServerType,'Paradox');
  F.Free;

  if CompareText(StrH,'Oracle')=0 Then
    Database_Kupa.Execute(OracleDateFormatSql);

  if CompareText(StrH,'Sybase')=0 Then
  Begin
    Database_Kupa.StartTransaction;
    Database_Kupa.Execute(SybaseDateFormatSql);
    Database_Kupa.Commit;
  End;
end;

procedure TDM_Kupa.AtmRxQuery_ZikuyAvBeforeScroll(DataSet: TDataSet);
begin
  if (NewKabalaSavedNumber=AtmRxQuery_ZikuyAvKabalaNumber.AsInteger) And
     (AtmRxQuery_ZikuyAvTotalPlusVat.AsFloat=0) And
     (AtmRxQuery_ZikuyAv.RecordCount<>0) Then
    Case MessageDlg('������� ����� ��� ������ ��� ����� ����',mtConfirmation,[mbYes,mbNo,mbCancel],0) Of
      mrYes:AtmRxQuery_ZikuyAv.Delete;
      mrCancel: Abort;
    End; //Case
end;

Procedure TDM_Kupa.AtmRxQuery_KabHesAfterInsert(DataSet: TDataSet);
begin
    Frm_Kabala.RxDBGrid_KabHes.Col:=1;
    AtmRxQuery_Kabhes.FieldByName('Codemaslul').AsInteger:=0;
    AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:='';
    AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=False;
    AtmRxQuery_KabHes.FieldByName('DateHesbonit').AsDateTime:=
      AtmRxQuery_ZikuyAv.FieldByName('DateKabala').AsDateTime;
//  AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=True;
end;

Procedure TDM_Kupa.AtmRxQuery_KabHesSchumHesbonitValidate(Sender: TField);
Var
Kamut,KamutLeMlay   :  Real;
begin
     If snif<>-1 Then
       If AtmRxQuery_KabHesSchumHesbonit.AsCurrency < 0 Then
       Begin
         Showmessage('!! ���� ����� ����');
         Abort;
       End;
     If SumMakor=0 then
        SumEgul:=AtmRxQuery_KabHesSchumHesbonit.AsCurrency;

     Frm_Kabala.TotalSchum;
{
     If Sender=AtmRxQuery_KabHes.FieldByName('KamutHesbonit') Then
     Begin
       If  (AtmRxQuery_Kabhes.FieldByName('ToNum').IsNull) And
         (Not DM_Kupa.AtmRxQuery_Kabhes.FieldByName('FromNum').IsNull)
       Then
         AtmRxQuery_Kabhes.FieldByName('ToNum').AsString:=IntToStr(
         AtmRxQuery_Kabhes.FieldByName('FromNum').Asinteger+
         AtmRxQuery_Kabhes.FieldByName('KamutHesbonit').asinteger-1);

         If AtmRxQuery_ZikuyAvKabalaKind.AsInteger =1 Then
           Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency*-1
         Else
           Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency;
         KamutLeMlay:=KamutMlay+Kamut;
         If (KamutMin<>0) And (KamutMin>KamutLeMlay) Then
           ShowMessage('!! ����� ����� ���� ����� ������� ');

     End; //KamutHesbonit
}
End;

procedure TDM_Kupa.AtmRxQuery_KabHesBeforeDelete(DataSet: TDataSet);
begin
  if AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0 Then
  Begin
    ShowMessage('��� ������ ����� ');
    Abort;
  End;
    KaBhesWasDelete:=True;
end;


Procedure TDM_Kupa.AtmRxQuery_KabHescodemaslulValidate(Sender: TField);
Begin
    With Qry_Temp Do
    Begin
//
      SumEgul:=0;
      SumMakor:=0;
      MaamPrit:=0;
      KamutMin:=0;
      KamutMlay:=0;
      LastNum:=9999999;

      Close;
      Sql.Clear;
      Sql.Add('Select Name,Sug_rehev1,PricePerMusa,DAY5,DAY6,Sug_Maslul,Day1,Day3,PayedPrice,Kamut_Rehev1 From Maslul');
      Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodeMaslul.AsString);
      Open;
      if Not Eof And Bof Then
      Begin
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:=
          Qry_Temp.fieldbyname('Name').asstring;
          If Qry_Temp.fieldbyname('Sug_Maslul').AsInteger=1 Then
             MaamPrit:=1;            //Kamut_Rehev2
          Schum:=Qry_Temp.fieldbyname('PayedPrice').AsCurrency;
          KamutTon:=Qry_Temp.fieldbyname('Kamut_Rehev1').AsCurrency;
          SumMakor:=Schum;
          SumEgul:=Schum;
          If MaamPrit=0 Then
          Begin
              SumEgul:=Schum/(1+Maam/100);
              Schum:=Round(SumEgul*100)/100;
//            Schum:=Round(Schum/(1+Maam/100) *100)/100 ;
          End;
          DM_Kupa.AtmRxQuery_Kabhes.FieldByName('SchumHesbonit').AsCurrency:=
           Schum;
      End
      Else
      Begin
        Showmessage('���� �� ���� !!');
        AtmRxQuery_KabHes.Cancel;
        Abort;
        Exit;
      End;

//      DM_Kupa.AtmRxQuery_Kabhes.FieldByName('SchumHesbonit').AsCurrency:=0;

    End;  // With Qry_Temp Do
//    AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=True;
//    AtmRxQuery_KabHes.FieldByName('DateHesbonit').ReadOnly:=True;
//    AtmRxQuery_KabHes.FieldByName('FromNum').ReadOnly:=True;
//  End;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesToNumChange(Sender: TField);
Begin
   If (AtmRxQuery_Kabhes.FieldByName('ToNum').AsString <>'') And
      (AtmRxQuery_KabHes.FieldByName('FromNum').Asstring <>'')
   Then
   AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat:=
      AtmRxQuery_Kabhes.FieldByName('ToNum').AsFloat-
      AtmRxQuery_KabHes.FieldByName('FromNum').AsFloat+1;

   Begin
     AtmRxQuery_KabHes.FieldByName('kamutlemeholel').AsFloat:=
       KamutTon*AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat;
     Frm_Kabala.TotalSchum;
   End;
// If Tnum=0 Then Exit;
// If Kabala_Type < 4 Then Exit;
//   If Tnum<AtmRxQuery_Kabhes.FieldByName('FromNum').AsInteger Then

// Begin
//   Showmessage('���� ���� ����� ������ ������ >>'+IntTostr(Tnum));
//   Abort;
// End;
end;

procedure TDM_Kupa.AtmRxQuery_KabHesKamutHesbonitChange(Sender: TField);
Var
Kamut,KamutLeMlay   :  Real;

Begin
     If AtmRxQuery_KabHes.State<>dsInsert Then
     Begin
       With Qry_Temp Do
       Begin
         Close;
         Sql.Clear;                                   
         Sql.Add('Select Name,Sug_rehev1,PricePerMusa,PayedPrice,Kamut_Rehev1 From Maslul');
         Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodeMaslul.AsString);
         Sql.Add('And code_pizul=' + IntToStr(Snif) );
         Open;
         Schum:=Qry_Temp.fieldbyname('PayedPrice').AsCurrency;
         SumMakor:=Schum;
         SumEgul:=Schum;
         KamutTon:=Qry_Temp.fieldbyname('Kamut_Rehev1').AsCurrency;
       End;
     End;

     If Snif=-1 Then
     Begin
       Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency;     
       Frm_Kabala.TotalSchum;
       Exit;
     End;
     Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency;
     if (Kamut<1) Or (Kamut<>Int(Kamut)) Then
     Begin
       Showmessage('���� �� �����');
       Abort;
     End;
     If (Kabala_Type=0) Or (Kabala_Type=2) Then
     Begin
       IF Kamut > KamutMlay Then
       Begin
         Showmessage('��� ����� �� ����� ������');
         Abort;
       End;
     End;
     AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=False;
     Try
       AtmRxQuery_Kabhes.FieldByName('ToNum').AsString:=IntToStr(
       AtmRxQuery_Kabhes.FieldByName('FromNum').Asinteger+
       AtmRxQuery_Kabhes.FieldByName('KamutHesbonit').asinteger-1);
       AtmRxQuery_KabHes.FieldByName('ToNum').ReadOnly:=True;
     Except On Exception Do;
     End;
     If ((Kabala_Type=0) Or (Kabala_Type=2)) And
       (AtmRxQuery_Kabhes.FieldByName('ToNum').AsInteger > LastNum) Then
     Begin
         Showmessage(IntToStr(LastNum)+' << �� ���� ���� ���� �');
         Abort;
     End;
     If (Kabala_Type=1) And  // ������
       (AtmRxQuery_Kabhes.FieldByName('ToNum').AsInteger > Tnum) Then
     Begin
       Showmessage('���� ���� ����� ������ ������ >>'+IntTostr(Tnum));
       Abort;
     End;

     If (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=0) Or
        (AtmRxQuery_ZikuyAvKabalaKind.AsInteger=2)
     Then
       Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency*-1
     Else
       Kamut:=AtmRxQuery_KabHes.FieldByName('KamutHesbonit').AsCurrency;
     KamutLeMlay:=KamutMlay+Kamut;
     If (KamutMin<>0) And (KamutMin>KamutLeMlay) And (Kabala_Type<>1)
     Then
         ShowMessage('!! ����� ����� ���� ����� ������� ');
     Frm_Kabala.TotalSchum;
end;


Procedure TDM_Kupa.BuildLinesInStockForCurChecks;
Var
FirstShovar,LastShovar,Ind    :  LongInt;
Kamut,PrtimLen                :  Integer;
Pratim1                       :  ShortString;

Begin
  AtmRxQuery_Kabhes.Close;
  AtmRxQuery_Kabhes.Open;
  AtmRxQuery_Kabhes.First;
  while not(AtmRxQuery_Kabhes.EOF) do
  Begin
    FirstShovar:=AtmRxQuery_KabHes.FieldByName('FromNum').AsInteger;
    LastShovar:=AtmRxQuery_KabHes.FieldByName('ToNum').AsInteger;
    With Qry_temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Kamut_Rehev1 From Maslul');
      Sql.Add('Where Code_maslul='+AtmRxQuery_KabHesCodeMaslul.AsString);
//    Sql.Add('And code_pizul=' + IntToStr(Snif) );
      Sql.SaveToFile('c:\mm.sql');
      Open;
      if Not Eof And Bof Then
        Kamut:=Qry_Temp.fieldbyname('Kamut_Rehev1').AsInteger
      Else
        Kamut:=0;
    End;

    For Ind:=FirstShovar To LastShovar Do
    Begin
      Query_InsertStock.Insert;
      if Not AtmRxQuery_ZikuyAv.FieldByName('Pratim').IsNull
      Then
      Begin
        Pratim1:=AtmRxQuery_ZikuyAv.FieldByName('Pratim').AsString;
        Pratim1:=Trim(Copy(Pratim1,1,10));
        Query_InsertStock.FieldByName('Hesbonit').AsInteger:=
          StrToInt(Pratim1)
//        AtmRxQuery_ZikuyAv.FieldByName('Pratim').Asinteger
      End
      Else
        Query_InsertStock.FieldByName('Hesbonit').AsInteger:=0;

      Query_InsertStock.FieldByName('NumberNoteBook').AsInteger:=
        AtmRxQuery_KabHes.FieldByName('KabalaNumber').AsInteger;
      Query_InsertStock.FieldByName('NumberShovar').AsInteger:=Ind;
      Query_InsertStock.FieldByName('DateShovar').Asdatetime:=
        AtmRxQuery_KabHes.FieldByName('DateHesbonit').Asdatetime;
      Query_InsertStock.FieldByName('QuantityIn').AsFloat:=Kamut;
      Query_InsertStock.FieldByName('QuantityOut').AsFloat:=0;
      Query_InsertStock.FieldByName('ShibKind').AsInteger:=0;
      Query_InsertStock.FieldByName('DriverNo').AsInteger:=0;
      Query_InsertStock.FieldByName('KodParit').AsInteger:=
        AtmRxQuery_KabHes.FieldByName('codemaslul').AsInteger;
      Query_InsertStock.FieldByName('LakNo').AsInteger:=
        AtmRxQuery_KabHes.FieldByName('CodeLakoach').AsInteger;
      Query_InsertStock.FieldByName('Kod_Kvutza_Lakoach').AsInteger:=
        AtmRxQuery_ZikuyAv.FieldByName('ShuraKopa').AsInteger;
      Query_InsertStock.Post;
    End;
    AtmRxQuery_Kabhes.Next;
  End //While

End;


//-----------------------------------------------------------------------
//                       ����� ����� ����� �� ����
Function TDM_Kupa.CalcDateTashlum(FromDate :TDateTime;PayCond :ShortString) : TDateTime;
Var
   Day,Month,Year :Word;
   Res            :TDateTime;
   Str1           :ShortString;
   I              :LongInt;
   FoundNum       :Boolean;
   AddValue       :LongInt;
Begin
  DecodeDate(FromDate, Year, Month, Day);
//------------------- ����� �����
  if Pos('����',PayCond)> 0 Then
     begin
       Day := 1;
       if Month < 12 then Inc(Month)
          else begin
               Inc(Year);
               Month := 1;
          end;
       FromDate := EncodeDate(Year, Month, Day);
       FromDate := FromDate -1;
     end;
//------------------- ����� �����
  I:=1;
  FoundNum:=True;
  Str1:='';
  While (FoundNum) And (I<=Length(PayCond)) Do
    Begin
      if (PayCond[I]>='0') And (PayCond[I]<='9') Then
         Str1:=Str1+PayCond[I]
      Else
         if Str1<>''Then
            FoundNum:=False;
      Inc(I);
    End;
//-------------------------
  if Str1 > '' then
     Result:= FromDate + StrToInt(Str1)
  else
     Result:= FromDate;
End;

procedure TDM_Kupa.AtmRxQuery_ZikuyAvShuraKopaValidate(Sender: TField);
begin
  With DM_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select Teur_Tavla From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_GroupLako));
    Sql.Add('And Kod_Tavla = ' +AtmRxQuery_ZikuyAv.FieldByName('ShuraKopa').AsString);
    Open;
    if Not Eof And Bof Then
      AtmRxQuery_ZikuyAv.FieldByName('LookKvoLak').AsString:=
        Qry_Temp.fieldbyname('Teur_Tavla').AsString
    Else
    Begin
      Showmessage('��� ����');
      Sender.AsString:='0';
      Abort;
    End;
  End;

end;

procedure TDM_Kupa.AtmRxQuery_KabHesAfterOpen(DataSet: TDataSet);
begin
  FillKabalaFrmLookupFields;
end;

Procedure TDM_Kupa.FillKabalaFrmLookupFields;
Var
   SList:TStringList;
   I :LongInt;
Begin
  With Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select * From KodTavla Where Sug_Tavla='+IntToStr(SugTavla_GroupLako));
    Open;
    SList:=TStringList.Create;
    SList.AddObject(IntToStr(SugTavla_GroupLako),AtmRxQuery_ZikuyAvLookKvoLak);
    FillKodTavlaLookupList(Qry_Temp,SList);
    SList.Free;
    Close;
  End;

End;

end.

unit F_PrintKabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls;

type
  TFrm_PrintKabala = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRDBText_MisHafkada: TQRDBText;
    QRLabel_Makor: TQRLabel;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel_AddressLak: TQRLabel;
    QRBand5: TQRBand;
    QRBand6: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText14: TQRDBText;
    QRMemo_CompanyDetails: TQRLabel;
    QRLabel19: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    GroupHeaderBand1: TQRBand;
    QRShape36: TQRShape;
    ChildBand1: TQRChildBand;
    QRShape23: TQRShape;
    QRSubDetail2: TQRSubDetail;
    QRShape43: TQRShape;
    GroupFooterBand1: TQRBand;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel40: TQRLabel;
    QRShape38: TQRShape;
    QRShape35: TQRShape;
    QRShape40: TQRShape;
    QRDBText17: TQRDBText;
    QRDBText16: TQRDBText;
    QRShape50: TQRShape;
    QRDBText26: TQRDBText;
    QRShape51: TQRShape;
    QRDBText27: TQRDBText;
    QRShape52: TQRShape;
    QRShape22: TQRShape;
    QRShape53: TQRShape;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRLabel27: TQRLabel;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape56: TQRShape;
    QRShape57: TQRShape;
    QRShape58: TQRShape;
    QRDBText30: TQRDBText;
    QRShape37: TQRShape;
    QRShape41: TQRShape;
    QRLabel37: TQRLabel;
    QRDBText31: TQRDBText;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRDBText1: TQRDBText;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRDBText2: TQRDBText;
    QRLabel8: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel16: TQRLabel;
    QRDBText12: TQRDBText;
    QRShape10: TQRShape;
    QRExpr2: TQRExpr;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel_Osek: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRLabel5Print(sender: TObject; var Value: String);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText_CheckNumberPrint(sender: TObject; var Value: String);
    procedure QRLabel8Print(sender: TObject; var Value: String);
    procedure QuickRep1AfterPrint(Sender: TObject);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure QRMemo_CompanyDetailsPrint(sender: TObject;
      var Value: String);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRLabel21Print(sender: TObject; var Value: String);
    procedure QRDBText17Print(sender: TObject; var Value: String);
    procedure QRDBText18Print(sender: TObject; var Value: String);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure GroupHeaderBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText25Print(sender: TObject; var Value: String);
    procedure QRExpr1Print(sender: TObject; var Value: String);
    procedure GroupFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRExpr2Print(sender: TObject; var Value: String);
    procedure QRLabel37Print(sender: TObject; var Value: String);
    procedure QRLabel39Print(sender: TObject; var Value: String);
    procedure QRDBText29Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    CompayName:String;
  public
    { Public declarations }
  end;

var
  Frm_PrintKabala: TFrm_PrintKabala;
  Total_sum      : Real;
  Kodmatbea      : Integer;
implementation

uses DMKupa, AtmRutin, AtmConst;

{$R *.DFM}

procedure TFrm_PrintKabala.FormCreate(Sender: TObject);
begin
{  DM_Kupa.Qry_Mname.Open;
  CompayName:=DM_Kupa.Qry_Mname.FieldByName('CompanyName').AsString;
  CompayName:=DoEncryption(CompayName,Mname_Key,MnameDecryptKey,False);
  DM_Kupa.Qry_Mname.Close;
}
end;

procedure TFrm_PrintKabala.QRLabel5Print(sender: TObject;
  var Value: String);
begin
  Value:=CompayName;
end;

Procedure TFrm_PrintKabala.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
Var
Bait   :  Shortstring;
Begin
  Case Dm_Kupa.AtmRxQuery_ZikuyAvKabalaKind.AsInteger OF // Kabala_type Of
    0 : QRLabel1.Caption :='������� ���� ����';
    1 : QRLabel1.Caption:='������� ����� ����';
    2 : QRLabel1.Caption:='������� �� ����';
  End;

  QRLabel33.Caption:=ShemMadpis;
  Total_sum:=0;
  With Dm_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
//  If Kabala_type=10 Then
    If Dm_kupa.AtmRxQuery_ZikuyAvHesbonChiuv.AsString='���' Then
    Begin
      Sql.Add('Select Ktovet_1,Ir_1,Mikud_1 From Nehag');
      Sql.Add('Where Kod_Nehag = '+DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').AsString);
    End
    Else
    Begin
      Sql.Add('Select Ktovet_1,Mis_Bait1,Ir_1,Mikud_1,Osek_Morshe_Id From Lakoach');
      Sql.Add('Where Kod_Lakoach = '+DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('CodeLakoach').AsString);
    End;
    Open;
//  If Kabala_type<>10 Then Bait:=FieldByName('Mis_Bait1').AsString
    If Dm_kupa.AtmRxQuery_ZikuyAvHesbonChiuv.AsString='����' Then
       Bait:=FieldByName('Mis_Bait1').AsString
    Else
       Bait:='';

    QRLabel_AddressLak.Caption:=FieldByName('Ktovet_1').AsString+' '+
                                Bait+' '+
                                FieldByName('Ir_1').AsString+' '+
                                FieldByName('Mikud_1').AsString+' ';

    QRLabel_Osek.Caption:=FieldByName('Osek_Morshe_Id').AsString;
    Close;
  End;
end;

procedure TFrm_PrintKabala.QRDBText6Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
end;

procedure TFrm_PrintKabala.QRDBText_CheckNumberPrint(sender: TObject;
  var Value: String);
begin
  if TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsInteger=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QRLabel8Print(sender: TObject;
  var Value: String);
begin
  if CompareText('����',QRLabel_Makor.Caption)=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QuickRep1AfterPrint(Sender: TObject);
begin
  //DM_Kupa.Qry_Mname.Close;
end;

procedure TFrm_PrintKabala.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRDBText14Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRMemo_CompanyDetailsPrint(sender: TObject;
  var Value: String);
begin
  Value :=DoEncryption(DM_Kupa.Qry_Mname.FieldByName('CompanyDetail').AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
Var
OkFlag    :   boolean;
begin
//  DM_Kupa.Qry_Mname.Open;
    QuickRep1.Page.TopMargin:=TopLines;
    Kodmatbea:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('Kodmatbea').AsInteger;
    if CompareText('����',QRLabel_Makor.Caption)=0 Then OkFlag:=False
    Else OkFlag:=True;
    QRLabel10.Enabled:=OkFlag;
    QRLabel11.Enabled:=OkFlag;
    QRLabel13.Enabled:=OkFlag;
    QRDBText5.Enabled:=OkFlag;
    QRDBText6.Enabled:=OkFlag;
    QRDBText7.Enabled:=OkFlag;
end;

procedure TFrm_PrintKabala.QRLabel21Print(sender: TObject;
  var Value: String);
Var
Matbea   :   Integer;
begin
  If Kodmatbea{Kabala_type}=0 Then Matbea:=0 Else Matbea:=2;
  Value:=NumberToHebWords(DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat,True,True,Matbea);
end;

procedure TFrm_PrintKabala.QRDBText17Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText18Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRSubDetail1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.GroupHeaderBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;
end;

procedure TFrm_PrintKabala.QRDBText25Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRExpr1Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
      Value:=FormatFloat('0.00',StrToFloat(Value))
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.GroupFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
   If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Or (Hadpasa='N')
      Then PrintBand:=False;

end;

procedure TFrm_PrintKabala.QRExpr2Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

procedure TFrm_PrintKabala.QRLabel37Print(sender: TObject;
  var Value: String);
Var
Sum   :   real;

begin
    Sum:=DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').ASfloat-
         DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumTashlom').ASfloat;
    Value:=FormatFloat('0.00',(Sum));
    Total_sum:=Total_sum+sum;
end;

procedure TFrm_PrintKabala.QRLabel39Print(sender: TObject;
  var Value: String);
begin
    Value:=FormatFloat('0.00',(Total_Sum));
end;

procedure TFrm_PrintKabala.QRDBText29Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

end.

library HesZikuy;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ExceptionLog,
  SysUtils,
  Classes,
  AtmRutin in 'K:\TIMCHUR\COMMON\AtmRutin.pas',
  MslDllInterface in 'MslDllInterface.pas',
  AtmConst in 'K:\TIMCHUR\COMMON\AtmConst.pas',
  F_Kabala in 'F_Kabala.pas' {Frm_Kabala},
  F_PrintKabala in 'F_PrintKabala.pas' {Frm_PrintKabala},
  DMKupa in 'DMKupa.pas' {DM_Kupa: TDataModule};

{$R *.RES}
{
Procedure Hafkada(AUserName,APassword,AAlias,Kabalatype:PChar);
Begin
  InitHafkadaForms(AUserName,APassword,AAlias,KabalaType);
  DoneHafkadaForms;
//Kabala_type:=2 ����
//0 = ���
End;

}

Procedure InitKupaDll(IniFileName,ScriptsDir :PChar);
Begin
  KupaDllIniFileName:=StrPas(IniFileName);//(c:\Program files\atm\atm2000\AtmCrt.ini) �� ���� ������
  KupaDllScriptsDir:=StrPas(ScriptsDir);
End;

Exports
  InitKupaDll,
  DoKabala;
begin
  KupaDllIniFileName:='';
  KupaDllScriptsDir:='';
end.

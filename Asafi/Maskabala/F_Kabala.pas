unit F_Kabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, ToolWin, StdCtrls, Mask, DBCtrls, AtmComp, AdvSearch,
  ToolEdit, RXDBCtrl, AtmDBDateEdit, Menus, Grids, DBGrids, ActnList, DB,
  ImgList, Buttons, DBTables, Scale250, DBFilter, RxQuery,
  AtmTabSheetBuild,inifiles;

type
  TFrm_Kabala = class(TForm)
    PageControl1: TPageControl;
    ToolBar1: TToolBar;
    TS_Main: TTabSheet;
    Panel_Kabala: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    AtmDbHEdit_KabalaNo: TAtmDbHEdit;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    AtmDBDateEdit1: TAtmDBDateEdit;
    Label2: TLabel;
    Label_CodeLakoach: TLabel;
    KabCodeLakoach: TAtmDbHEdit;
    AtmDbHEdit1: TAtmDbHEdit;
    AtmAdvSearch_lakoach: TAtmAdvSearch;
    Lbl_kodbitzua: TLabel;
    KodBitzou: TAtmDbHEdit;
    DBNavigator1: TDBNavigator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ActionList1: TActionList;
    Action_duplicateRecord: TAction;
    Action_SumTotals: TAction;
    Action_OpenSearch: TAction;
    Action_SelectNextControl: TAction;
    Action_SelectPrevControl: TAction;
    N2: TMenuItem;
    Panel2: TPanel;
    RxDBGrid_KabHes: TRxDBGrid;
    Act_SelectOneHeshbonitForClose: TAction;
    Act_CloseHeshbonitSequence: TAction;
    Btn_Edkun: TBitBtn;
    Act_PrintKabala: TAction;
    N3: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Act_NewKabala: TAction;
    ToolButton3: TToolButton;
    N7: TMenuItem;
    N21: TMenuItem;
    Act_ChangeYehusYear: TAction;
    N8: TMenuItem;
    BitBtn2: TBitBtn;
    Scale1: TScale;
    Act_UpdateFiles: TAction;
    ToolButton4: TToolButton;
    Act_CloseOpenHeshboniot: TAction;
    Act_SelectOnlyOpenKabalot: TAction;
    AtmAdvSearch_Nehag: TAtmAdvSearch;
    Label4: TLabel;
    Label5: TLabel;
    DBEdt_maam: TDBEdit;
    DBedt_totalPlusVat: TDBEdit;
    DBEdt_MamPercent: TDBEdit;
    Label7: TLabel;
    TotalBeforeMaam: TEdit;
    AtmTabSheetBuild1: TAtmTabSheetBuild;
    RxQuery_Tavla: TRxQuery;
    RxDBFilter1: TRxDBFilter;
    Ds_tavla1: TDataSource;
    N10: TMenuItem;
    Act_SugHadpasa: TAction;
    Act_SugFile: TAction;
    ActSugFile1: TMenuItem;
    AtmAdvSearch_Shipment: TAtmAdvSearch;
    AtmAdvSearch_Returns1: TAtmAdvSearch;
    Lbl_zkut: TLabel;
    ChebZikuy: TAtmDbHEdit;
    AtmAdvSearchenZikuy: TAtmAdvSearch;
    Lbl_DatePiron: TLabel;
    AtmDBDateEdit2: TAtmDBDateEdit;
    Lbl_chiuv: TLabel;
    ChenChiuv: TAtmDbHEdit;
    Label3: TLabel;
    Pratim: TAtmDbHEdit;
    AtmAdvSearch_Remarks: TAtmAdvSearch;
    Label6: TLabel;
    AtmDbHEdit3: TAtmDbHEdit;
    AtmDbHEdit4: TAtmDbHEdit;
    AtmAdvSearch_KvoLak: TAtmAdvSearch;
    Act_egul1: TMenuItem;
    Act_egul: TAction;
    Label8: TLabel;
    AtmAdvSearch_Maslul: TAtmAdvSearch;
    AtmAdvSearch_Returns: TAtmAdvSearch;
    procedure Action_OpenSearchExecute(Sender: TObject);
    procedure Action_OpenSearchUpdate(Sender: TObject);
    procedure N2Click(Sender: TObject);
//  procedure Act_ShowOpenHeshbonitForLakExecute(Sender: TObject);
    procedure Panel_KabalaExit(Sender: TObject);
    procedure Panel_KabalaEnter(Sender: TObject);
//  procedure Act_SelectOneHeshbonitForCloseExecute(Sender: TObject);
//  procedure Act_CloseHeshbonitSequenceExecute(Sender: TObject);
    procedure RxDBGrid_KabHesEnter(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RxDBGrid_KabHesExit(Sender: TObject);
    procedure Act_PrintKabalaExecute(Sender: TObject);
    procedure DBNavigator1BeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure KodBitzouChange(Sender: TObject);
    procedure Label_CodeLakoachDblClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure Act_NewKabalaExecute(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure Act_ChangeYehusYearExecute(Sender: TObject);
    procedure Act_UpdateFilesExecute(Sender: TObject);
    procedure Action_duplicateRecordUpdate(Sender: TObject);
    procedure Action_duplicateRecordExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RxDBGrid_KabHesGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure KabCodeLakoachBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure RxDBGrid_KabHesColEnter(Sender: TObject);
    procedure RxDBGrid_KabHesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdt_maamExit(Sender: TObject);
    procedure DBEdt_maamKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdt_maamEnter(Sender: TObject);
    procedure DBEdt_MamPercentEnter(Sender: TObject);
    procedure DBEdt_MamPercentKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdt_MamPercentExit(Sender: TObject);
    procedure AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
    procedure N9Click(Sender: TObject);
//  procedure Act_SugHadpasaExecute(Sender: TObject);
    procedure Act_SugFileExecute(Sender: TObject);
    procedure RxDBGrid_KabHesDblClick(Sender: TObject);
    procedure AtmAdvSearch_ReturnsAfterExecute(Sender: TObject);
    procedure MisparHavaraEnter(Sender: TObject);
    procedure ChebZikuyBeforeExecuteSearch(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure ChebZikuyEnter(Sender: TObject);
    procedure ChebZikuyExit(Sender: TObject);
    procedure AtmAdvSearch_RemarksAfterExecute(Sender: TObject);
    procedure PratimEnter(Sender: TObject);
    procedure AtmDbHEdit3Enter(Sender: TObject);
    procedure PratimExit(Sender: TObject);
    procedure Act_egulExecute(Sender: TObject);
    procedure AtmAdvSearch_MaslulAfterExecute(Sender: TObject);
    procedure AtmAdvSearch_MaslulBeforeExecute(Sender: TObject;
      var ContinueExecute: Boolean);
    procedure AtmAdvSearch_ReturnsBeforeExecute(Sender: TObject;
      var ContinueExecute: Boolean);
  private
    { Private declarations }
    Procedure FillComboHeshbonHiuv;
  public
    { Public declarations }
    Procedure InitKabala;
    Procedure UpdateAllFiles(KodBitzua:LongInt);
    Procedure DoUpdateInFiles;
    Procedure PrintCurKabala;
    Procedure TotalSchum;
    Procedure EgulMaam;
    Procedure UpdateHazmana;
  end;

var
  Frm_Kabala: TFrm_Kabala;
//  Hadpasa   : String[1];

implementation

uses Dmkupa, AtmConst, AtmRutin, F_PrintKabala;

{$R *.DFM}

Procedure TFrm_Kabala.FillComboHeshbonHiuv;
Begin
End;

Function  NoMean(St1 : String) : Boolean;
  Var
    Hb: Boolean;
    I: Integer;
  Begin
    Hb:=True;
    for I:=1 to Length(St1) do
      if (St1[I]<>' ') and (St1[I]<>#0) then
        Hb:=False;
    Result:=Hb;
  End;

procedure TFrm_Kabala.Action_OpenSearchExecute(Sender: TObject);
Var
   Bool :Boolean;
Begin
     if ActiveControl is TAtmDbHEdit Then
       (ActiveControl As TAtmDbHEdit).ExecSearch
     Else
       if ActiveControl=RxDBGrid_Kabhes Then
       Begin
          if RxDBGrid_Kabhes.SelectedField=DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul') Then
            AtmAdvSearch_Maslul.Execute;      // FromNum
//        if RxDBGrid_Kabhes.SelectedField=DM_Kupa.AtmRxQuery_Kabhes.FieldByName('KamutHesbonit') Then
//          AtmAdvSearch_Returns.Execute;
       End;
end;


procedure TFrm_Kabala.Action_OpenSearchUpdate(Sender: TObject);
Begin
  Try
     if ActiveControl is TAtmDbHEdit Then
        (Sender As TAction).Enabled:=(ActiveControl As TAtmDbHEdit).EnableSearch
     Else
      if (ActiveControl=RxDBGrid_Kabhes) Then
      Begin
        (Sender As TAction).Enabled:=(RxDbGrid_Kabhes.SelectedField = DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul')) ;
      End
      Else
         (Sender As TAction).Enabled:=False;
  Except On Exception Do;
  End;
end;

Procedure TFrm_Kabala.InitKabala;
Var
  yy,mm,dd :Word;
  HStr     :shortstring;
Begin
  DecodeDate(Now,YY,MM,DD);
  CurKabalaYear:=YY;

  DM_Kupa.AtmRxQuery_ZikuyAv.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_ZikuyAv.Open;
  DM_Kupa.AtmRxQuery_ZikuyAv.Last;
  Case Kabala_type Of
    0 : HStr:='�� ';
    1 : HStr:='���� ';
    2 : HStr:='����� ';
  End;

  Caption:='������� / '+HStr+IntToStr(CurKabalaYear);
  If TypeOfFile='���' Then
    Label_CodeLakoach.Caption:='<���>'
  Else
    Label_CodeLakoach.Caption:='<����>';
End;

procedure TFrm_Kabala.N2Click(Sender: TObject);
begin
  Close;
end;
{
procedure TFrm_Kabala.Act_ShowOpenHeshbonitForLakExecute(Sender: TObject);
begin
  DM_Kupa.ShowOpenHeshbonitForLak(DM_Kupa.AtmRxQuery_ZikuyAvCodeLakoach.AsInteger,Spb_ShowOpenHeshbonitForLakGroup.Down);
end;
}

procedure TFrm_Kabala.Panel_KabalaExit(Sender: TObject);
Var
  NewKabala  : Boolean;
  Sug_kabala : Integer;
Begin
  If Dm_kupa.AtmRxQuery_ZikuyAvCodeLakoach.IsNull Then
  Begin
     Showmessage('��� ��� ����');
     Dm_kupa.AtmRxQuery_ZikuyAvCodeLakoach.FocusControl;
     Abort;
  End;
//
  If (Dm_kupa.AtmRxQuery_ZikuyAv.State<>dsInsert) Then Sug_kabala:=
      Dm_kupa.AtmRxQuery_ZikuyAvKabalaKind.AsInteger
  Else
      Sug_kabala:=Kabala_type;

  NewKabala:=DM_Kupa.AtmRxQuery_ZikuyAv.State=dsInsert;
  if DM_Kupa.AtmRxQuery_ZikuyAv.State in [dsInsert,dsEdit] Then
    DM_Kupa.AtmRxQuery_ZikuyAv.Post;

end;

procedure TFrm_Kabala.Panel_KabalaEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_ZikuyAv;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesEnter(Sender: TObject);
begin
  DBNavigator1.DataSource:=DM_Kupa.DS_KabHes;
  RxDBGrid_Kabhes.Col:=1;
  DM_Kupa.AtmRxQuery_Kabhes.Edit;
//DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Codemaslul').AsInteger:=0;
  StatusBar1.SimpleText:='F6 ��� ���� ,F2 ����� ������� ,F5 ����� ���"�' ;
  DM_Kupa.KaBhesWasDelete:=False;

  if DM_Kupa.KabHesWasInsert Then
  Begin
    if DM_Kupa.AtmRxQuery_KabHes.State in [dsEdit,dsInsert] Then
      DM_Kupa.AtmRxQuery_KabHes.Post;
    DM_Kupa.AtmRxQuery_KabHes.Close;
    DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsInteger;
    DM_Kupa.AtmRxQuery_KabHes.ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsInteger;
    DM_Kupa.AtmRxQuery_KabHes.Open;
    DM_Kupa.KabHesWasInsert:=False;
  End;

end;

procedure TFrm_Kabala.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyboardManageForTableAction(Self,Key,Shift,DBNavigator1.DataSource.DataSet);
  if Key=VK_F6 Then
  Begin
    If ActiveControl.Name='RxDBGrid_KabHes' Then
    Begin
       Dm_kupa.AtmRxQuery_ZikuyAvPratim.FocusControl;
       Exit;
    End;
    If ActiveControl.Name='Pratim' Then
    Begin
       RxDBGrid_KabHes.SetFocus;
       Exit;
    End;
//  SelectNext(ActiveControl,Not (ssShift in Shift),True);
  End;
end;

Procedure TFrm_Kabala.UpdateAllFiles(KodBitzua:LongInt);
Begin
  DM_Kupa.Query_InsertStock.Open;
  DM_Kupa.Database_Kupa.StartTransaction;
  Try
    DoUpdateInFiles;
  Except On E:Exception Do
    Begin
      ShowMessage(e.Message);
      DM_Kupa.Database_Kupa.Rollback;
      DM_Kupa.Query_InsertStock.Close;
      Raise;
    End;
  End;
  DM_Kupa.Query_InsertStock.ApplyUpdates;
  DM_Kupa.Database_Kupa.Commit;
  DM_Kupa.Query_InsertStock.Close;  
  //����� ��� ����� 2
  DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
  DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger:=KodBitzua;
  DM_Kupa.AtmRxQuery_ZikuyAv.SavePressed:=True;
  DM_Kupa.AtmRxQuery_ZikuyAv.Post;
  If (DM_Kupa.AtmRxQuery_ZikuyAvKabalaKind.AsInteger=0) And
       (Not Dm_kupa.AtmRxQuery_ZikuyAvShuraKopa.IsNull) Then
       UpdateHazmana;
End;


Procedure TFrm_Kabala.DoUpdateInFiles;
Begin
  if DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger > 0 Then
    Exit;
  Screen.Cursor:=crSQLWait;
  Try
    DM_Kupa.BuildLineInMakav;
    DM_Kupa.BuildLinesInStockForCurChecks;    
    If Snif<>-1 Then
       DM_Kupa.UpdateMlayPritim;
  Finally
    Screen.Cursor:=crDefault;
  End;
End;

procedure TFrm_Kabala.RxDBGrid_KabHesExit(Sender: TObject);
begin
  if TCustomDbGrid(Sender).DataSource.DataSet.State in [dsInsert,dsEdit] Then
    TCustomDbGrid(Sender).DataSource.DataSet.Post;
end;

Procedure TFrm_Kabala.PrintCurKabala;
Var
  StrH :String;
  NumOfCopy,Code :LongInt;
Begin
(*
  StrH :=InputBox('����� ����','���� ������','2');
  Val(StrH,NumOfCopy,Code);
  if Code<>0 Then Exit;
  Frm_kabprt:=Tfrm_kabprt.Create(Nil);
  Try
//    If Frm_copy.showModal = mrOk Then
    Begin
     With DM_Kupa Do
     Begin
{      Frm_kabprt.QRLabel_CompayName.Caption:=ComName;
      Frm_kabprt.QrLbl_CompNum.Caption:=ComNum;
      Frm_kabprt.Qrlbl_OsekMorshe.Caption := OsekMorske;
      Frm_kabprt.QR_title.Caption:=Detail;}
      If (AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger=0) Or
         (AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').IsNull) Or
         (AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger=2)
      Then
      Begin
        Frm_kabprt.QRLabel4.Caption:='����';
        Print_Kabala;
        Frm_kabprt.QRLabel4.Caption:='����';
      End
      Else
         If Tbl_KabalaKodBItzua.AsInteger=9
      Then
         Frm_kabprt.QRLabel4.Caption:='�����'
      Else
         Frm_kabprt.QRLabel4.Caption:='����';
    // ����� ������
       For Ix1:=1 To NumOfCopy Do
          Frm_kabprt.QuickRep1.Preview; {Print}{preview;}
       If (AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger=0) Or
          (AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').IsNull) Then
             UpdateAllFiles(cbkPrintSource);
     End; {With Frm_DbKab}
    End;      //mrOk
  Finally
    Frm_kabprt.Free;
    Frm_kabprt:=Nil;
  End;

*)
End;

procedure TFrm_Kabala.Act_PrintKabalaExecute(Sender: TObject);
Var
  StrH           : String;
  NumOfCopy,Code : LongInt;
  Hefresh        : Real;
Begin
  IF (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Then
  Begin
     Showmessage('��� ������ ������');
     Abort;
     Exit;
  End;

  If  DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('Pratim').IsNull ThEN
  Begin
     Showmessage('��� ���� ����');
     Pratim.SetFocus ;
     Abort;
     Exit;
  End;

  Repeat
    StrH:=CopysKabala ; //'1';
    if Not InputQuery('����� �������','���� ������',StrH) Then
      Exit;    Val(StrH,NumOfCopy,Code);
  Until Code=0;
  DM_Kupa.AtmRxQuery_ZikuyAvAfterScroll(Nil); //refresh all qreries
  Frm_PrintKabala:=TFrm_PrintKabala.Create(Nil);
  Try
    Case DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger Of
      0:
        Begin
          StatusBar1.SimpleText:='����� �����...';
          Frm_PrintKabala.QRLabel_Makor.Caption:='����';
          UpdateAllFiles(cbkPrintSource);
          NumOfCopy:=NumOfCopy+1;
        End;
      1 : Frm_PrintKabala.QRLabel_Makor.Caption:='����';
      2 :    //����� ��� ����� 2 �-1
        Begin
          Frm_PrintKabala.QRLabel_Makor.Caption:='����';
          DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
          DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger:=1;
          DM_Kupa.AtmRxQuery_ZikuyAv.SavePressed:=True;
          DM_Kupa.AtmRxQuery_ZikuyAv.Post;
        End;
      9 : Frm_PrintKabala.QRLabel_Makor.Caption:='�����';
    End;

    For Code:=1 To NumOfCopy Do
    Begin
      Frm_PrintKabala.QuickRep1.Print;  // Preview; //Print;    David
      Frm_PrintKabala.QRLabel_Makor.Caption:='����';
    End;
    DM_Kupa.AtmRxQuery_ZikuyAv.Append;
  Finally
    Frm_PrintKabala.Release; //  Free;
    Frm_PrintKabala:=Nil;
    StatusBar1.SimpleText:='';
  End;
end;

procedure TFrm_Kabala.DBNavigator1BeforeAction(Sender: TObject;
  Button: TNavigateBtn);
Begin
  Case Button Of
    nbRefresh:Begin
                if DBNavigator1.DataSource.DataSet is TQuery Then
                Begin
                  TQuery(DBNavigator1.DataSource.DataSet).Close;
                  if TQuery(DBNavigator1.DataSource.DataSet).Params.FindParam('PKabalaNumber')<>Nil Then
                    TQuery(DBNavigator1.DataSource.DataSet).ParamByName('PKabalaNumber').AsInteger:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsInteger;
                  if TQuery(DBNavigator1.DataSource.DataSet).Params.FindParam('PKabalaYear')<>Nil Then
                    TQuery(DBNavigator1.DataSource.DataSet).ParamByName('PKabalaYear').AsInteger:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('YehusYear').AsInteger;
                  TQuery(DBNavigator1.DataSource.DataSet).Open;
                  Abort;
                End;
              End;
  End //Case;
end;

procedure TFrm_Kabala.KodBitzouChange(Sender: TObject);
begin
  Act_UpdateFiles.Enabled:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger=0;
  Act_CloseOpenHeshboniot.Enabled:=DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger in [1,2];
end;

procedure TFrm_Kabala.Label_CodeLakoachDblClick(Sender: TObject);
begin

//  If Kabala_type=10 Then
  If TypeOfFile='���' Then
    OpenCrt(fnNehag,KabCodeLakoach.Text)
  Else
    OpenCrt(fnLakoach,KabCodeLakoach.Text);
end;

procedure TFrm_Kabala.N4Click(Sender: TObject);
begin
  OpenCrt(fnMakav,'');
end;

procedure TFrm_Kabala.N5Click(Sender: TObject);
begin
  OpenCrt(fnLakoach,'');
end;

procedure TFrm_Kabala.N6Click(Sender: TObject);
begin
  OpenCrt(fnNehag,'');
end;

procedure TFrm_Kabala.Act_NewKabalaExecute(Sender: TObject);
begin
  DM_Kupa.AtmRxQuery_ZikuyAv.Append;
end;

procedure TFrm_Kabala.N7Click(Sender: TObject);
begin
  OpenCrt(fnTavla,'');
end;

procedure TFrm_Kabala.N21Click(Sender: TObject);
begin
  OpenCrt(fnTavla2,'');
end;

procedure TFrm_Kabala.Act_ChangeYehusYearExecute(Sender: TObject);
Var
  StrH,HStr :String;
  Code      :longInt;
begin
  Repeat
    StrH:=IntToStr(CurKabalaYear);
    if Not InputQuery('����� ��� �����','��� �����',StrH) Then
      Exit;
    Val(StrH,CurKabalaYear,Code);
  Until Code=0;

  DM_Kupa.AtmRxQuery_ZikuyAv.Close;
  DM_Kupa.AtmRxQuery_ZikuyAv.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DM_Kupa.AtmRxQuery_ZikuyAv.Open;
  Case Kabala_type Of
    0 : HStr:='���� ';
    1 : HStr:='����� ';
    2 : HStr:='�� ';
  End; // Case
//If Kabala_type MOD 10=0 Then HStr:='������ ' Else HStr:='����� ';
  Caption:='������� / '+HStr+IntToStr(CurKabalaYear);
end;

procedure TFrm_Kabala.Act_UpdateFilesExecute(Sender: TObject);
Var
Hefresh  :  Real ;
Begin
  IF (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Then
  Begin
     Showmessage('��� ������ ������');
     Abort;
     Exit;
  End;

  If  DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('Pratim').IsNull ThEN
  Begin
     Showmessage('��� ���� ����');
     Pratim.SetFocus;
     Abort;
     Exit;
  End;

  DM_Kupa.AtmRxQuery_ZikuyAvAfterScroll(Nil); //refresh all qreries
  UpdateAllFiles(cbkUpdateOnly);
  DM_Kupa.AtmRxQuery_ZikuyAv.Append;
end;

procedure TFrm_Kabala.Action_duplicateRecordUpdate(Sender: TObject);
begin
//Action_duplicateRecord.Enabled:=ActiveControl=RxDBGrid_Checks;
end;

procedure TFrm_Kabala.Action_duplicateRecordExecute(Sender: TObject);
begin
  if (Action_duplicateRecord.Enabled) And (DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodBItzua').AsInteger=0) Then
    DuplicateOneRecord(DM_Kupa.AtmRxQuery_Kabhes,False,True);
end;

procedure TFrm_Kabala.FormCreate(Sender: TObject);
Var
//Atmini   : Tinifile;
  CurYear     : Word;
  Res,Code    : LongInt;
  StrH,Strk   : String;
  yy,mm,dd    : Word;

begin
  AtmTabSheetBuild1.ScrFileName:=KupaDllScriptsDir+AtmTabSheetBuild1.ScrFileName;
  AtmTabSheetBuild1.SqlFileName:=KupaDllScriptsDir+AtmTabSheetBuild1.SqlFileName;
  AtmTabSheetBuild1.BuildTabsForGrids;
  Strh:=ExtractFilePath(Application.ExeName)+'Frm_GlbHesZikuy.Ini';
  AtmAdvSearch_lakoach.IniFileName:=Strh;
  AtmAdvSearch_Nehag.IniFileName:=Strh;
  AtmAdvSearch_Maslul.IniFileName:=Strh;
  AtmAdvSearchenZikuy.IniFileName:=Strh;
  AtmAdvSearch_Remarks.IniFileName:=Strh;
  AtmAdvSearch_KvoLak.IniFileName:=Strh;
Try
  except
  on E:exception do
      showmessage(E.Message);
  end;
  NewYear:=False;
  DM_Kupa.AtmRxQuery_ZikuyAv.Filtered:=True;
  DM_Kupa.AtmRxQuery_ZikuyAv.Filter:='KabalaKind='+IntToStr(Kabala_type);

  DecodeDate(Now,YY,MM,DD);
  CurKabalaYear:=YY;
//DM_Kupa.GetNextKabalaNumber(+0);
//Result:=0;
  CurYear:=CurKabalaYear;
  With DM_Kupa.Qry_AtmIndexKabala Do
  Begin
    If United<>-1 Then StrK:=aiHeshbonitMas+IntToStr(United)
    ELse StrK:=aiHeshbonitMas;

    ParamByName('PYehusYear').AsInteger:=CurYear;
    ParamByName('PKey_String').AsString:=StrK; //aiHeshbonitMas;

    Open;
    if Eof And Bof Then //��� ����
    Begin
    StrH:='15000';
    NewYear:=True;
    Repeat
      if Not InputQuery(IntTostr(CurYear)+' ��� ���� ', '������ ����� ������� ',StrH) Then
        Exit;     Val(StrH,Res,Code);
    Until Code=0;
      Insert;
      FieldByName('Last_Number').AsInteger:=Res-1; // 1
      FieldByName('LastUpDate').AsDateTime:=Now;
      FieldByName('Key_String').AsString:=ParamByName('PKey_String').AsString;
      FieldByName('YehusYear').AsInteger:=CurYear;
      Post;
    End
  End;
end;


Procedure TFrm_Kabala.FormShow(Sender: TObject);
Var
  NewKabala: Boolean;
  LastKab  : Integer;
begin
  DM_Kupa.AtmRxQuery_ZikuyAv.ParamByName('PYehusYear').AsInteger:=CurKabalaYear;
  DBNavigator1.DataSource:=DM_Kupa.DS_ZikuyAv;
  DM_Kupa.AtmRxQuery_ZikuyAv.Last;
//DM_Kupa.AtmRxQuery_KabHes.Last;

//Showmessage(DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').Asstring);
  NewKabala:=True;
// if (DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalSumKabla').AsFloat=0) And
    If (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) And (Not NewYear) Then
    NewKabala:=MessageDlg(
      '�������� ������� ����� ��� ������ ��� ������ ������ ������ ����'
      ,mtInformation,[mbYes,mbNo],0)=mrNo;
  Try
  if NewKabala Then
  Begin
    DM_Kupa.AtmRxQuery_ZikuyAv.Append;
  End;
  except
    on E:exception do
       showmessage(E.Message);
    end;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
//  if RxDBGrid_KabHes.DataSource.DataSet.FieldByName('CodeBitzua').AsInteger=1 Then
//    Background:=clRed;
end;


procedure TFrm_Kabala.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  With DM_Kupa Do
  Begin
    if (NewKabalaSavedNumber=AtmRxQuery_ZikuyAvKabalaNumber.AsInteger) And
       (DM_Kupa.AtmRxQuery_KabHes.RecordCount=0) Then
      Case MessageDlg('������� ����� ��� ������ ��� ����� ����',mtConfirmation,[mbYes,mbNo,mbCancel],0) Of
        mrYes:AtmRxQuery_ZikuyAv.Delete;
        mrCancel:Action:=caNone;
      End; //Case
  End;
end;

procedure TFrm_Kabala.KabCodeLakoachBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
  If TypeOfFile='���' Then
     KabCodeLakoach.SearchComponent:=AtmAdvSearch_Nehag
   Else
     KabCodeLakoach.SearchComponent:=AtmAdvSearch_lakoach;
end;

Procedure TFrm_Kabala.TotalSchum;
Begin
//  DM_Kupa.AtmRxQuery_Kabhes.Edit;
   If DM_Kupa.AtmRxQuery_Kabhes.State<>dsInsert Then
   Begin
     With DM_Kupa.Qry_Temp Do
     Begin
       Close;
       Sql.Clear;
       Sql.Add('Select Kamut_Rehev1 From Maslul');
       Sql.Add('Where Code_maslul='+DM_Kupa.AtmRxQuery_KabHesCodeMaslul.AsString);
       Open;
       KamutTon:=fieldbyname('Kamut_Rehev1').AsCurrency;
       SumEgul:=DM_Kupa.AtmRxQuery_KabHesSchumHesbonit.AsCurrency;
//     SumMakor:=0;//SumEgul;
     End; // With Qry_Temp
   End;

    DM_Kupa.AtmRxQuery_Kabhes.FieldByName('TotalSchum').AsFloat:=
    Round(                               // Kamuthesbonit
     DM_Kupa.AtmRxQuery_Kabhes.FieldByName('kamutlemeholel').AsFloat *
     SumEgul*100)/100;

//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat *
//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Schumhesbonit').AsFloat;
//  DM_Kupa.AtmRxQuery_Kabhes.Post;
    DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
    DM_Kupa.CalcTotalHesbonit;
    DM_Kupa.AtmRxQuery_ZikuyAv.Post;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesColEnter(Sender: TObject);
Var
HelpStr   :  String;
begin
    HelpStr:=' F6 ��� ���� ';
    StatusBar1.SimpleText:=HelpStr;
    If TDbGrid(Sender).SelectedField.FieldName='Codemaslul' Then
      StatusBar1.SimpleText:=' F2 ����� ������� ,F5 ����� ���"�,'+HelpStr;
    If TDbGrid(Sender).SelectedField.FieldName='SchumHesbonit' Then
       StatusBar1.SimpleText:='F5 ����� ���� ��"�,'+HelpStr;
    If TDbGrid(Sender).SelectedField.FieldName='KamutHesbonit' Then
    Begin
    If Not (NoMean(DM_Kupa.AtmRxQuery_Kabhes.FieldByName('ToNum').AsString))
      And
       Not (NoMean(DM_Kupa.AtmRxQuery_Kabhes.FieldByName('FromNum').AsString))
      Then
      Try
        DM_Kupa.AtmRxQuery_Kabhes.Edit;
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat:=
        DM_Kupa.AtmRxQuery_Kabhes.FieldByName('ToNum').AsFloat-
        DM_Kupa.AtmRxQuery_KabHes.FieldByName('FromNum').AsFloat+1;
        If KamutTon<>0 Then
          DM_Kupa.AtmRxQuery_KabHes.FieldByName('kamutlemeholel').AsFloat:=
            KamutTon*DM_Kupa.AtmRxQuery_Kabhes.FieldByName('Kamuthesbonit').AsFloat;
      Except On Exception Do;
      End;
    End;
end;

procedure TFrm_Kabala.RxDBGrid_KabHesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
Var
  IntH :LongInt;

begin
//if (Key=VK_F2) And
//  (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_KabHes.FieldByName('FromNum'))
//Then
//   AtmAdvSearch_Returns.Execute;

  if (Key=VK_F5) And
   (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit'))
  Then
  Begin
    Try
      DM_Kupa.AtmRxQuery_Kabhes.Post;
    Except
    End; //Try
    DM_Kupa.AtmRxQuery_Kabhes.Edit;
    DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').AsCurrency:=
    Round(
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('SchumHesbonit').AsCurrency/
     (1+DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency/100)
      *100)/100 ;
  End;
  if Key=VK_F9 Then
  Begin
    if TDbGrid(Sender).SelectedField.FieldKind=fkData Then
    With DM_Kupa.Qry_Temp DO
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Max(TNUA) SN From Zikuy');
      Open;
      IntH:=FieldByName('SN').AsInteger;
      Close;
      if IntH<>0 Then
      Begin
        Sql.Clear;
        Sql.Add('Select '+TDbGrid(Sender).SelectedField.FieldName);
        Sql.Add('From Zikuy Where TNUA='+IntToStr(IntH));
        Open;
        if Not Eof Or BOF Then
        Begin
          TDbGrid(Sender).SelectedField.AsString:=FieldByName(TDbGrid(Sender).SelectedField.FieldName).AsString;
          TDbGrid(Sender).SelectedIndex:=TDbGrid(Sender).SelectedIndex+1;
        End;
        Close;
      End; // if IntH<>0
    End;  //With DM_Kupa.Qry_Temp
  End;   //Vk-F9
  if (Key=VK_F5) And
   (TDbGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_KabHes.FieldByName('CodeMaslul'))
  Then
    DBEdt_maam.SetFocus;

end;

procedure TFrm_Kabala.DBEdt_maamExit(Sender: TObject);
begin
  StatusBar1.SimpleText:='';
  DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
  DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat:=
      StrToFloat(Frm_Kabala.TotalBeforeMaam.Text)+
      DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat;
  DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalZikuy').AsCurrency:=
      DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat;

end;

procedure TFrm_Kabala.DBEdt_maamKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F5 Then
    EgulMaam;
end;

procedure TFrm_Kabala.DBEdt_maamEnter(Sender: TObject);
begin
     StatusBar1.SimpleText:='F5 ����� ��"� ��������';
end;

procedure TFrm_Kabala.DBEdt_MamPercentEnter(Sender: TObject);
begin
     StatusBar1.SimpleText:='F6 ����� ���"� F5 ������ ���"�';

end;

procedure TFrm_Kabala.DBEdt_MamPercentKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_F6 Then
  Begin
     DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
     DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat:=0;
     DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsFloat:=0;
     DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat:=
           StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
     DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalZikuy').AsFloat:=
        DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat;
  End; // Vk_F6

  If Key=VK_F5 Then
  Begin
     DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
//   If (Kabala_type=1) Or (Atzmada=2) Then
     If Atzmada=2 Then
     Begin
       DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=0;
       DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodMatbea').ASinteger:=2;
     End
     Else
     Begin
       DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsCurrency:=Maam;
       DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KodMatbea').ASinteger:=0;
     End;  
     EgulMaam;
  End; //Vk_F5
end;

Procedure TFrm_Kabala.EgulMaam;
Begin
     DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
     DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat:=
     Round(
       StrToFloat(Frm_Kabala.TotalBeforeMaam.Text)*
       (1+DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('MamPercent').AsFloat/100))-
//     DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat)-
       StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
       DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat:=
           DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalVat').AsFloat+
           StrToFloat(Frm_Kabala.TotalBeforeMaam.Text);
       DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalZikuy').AsCurrency:=
         DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('TotalPlusVat').AsFloat;
end;

procedure TFrm_Kabala.DBEdt_MamPercentExit(Sender: TObject);
begin
     StatusBar1.SimpleText:='';
end;


procedure TFrm_Kabala.AtmTabSheetBuild1BeforeExecuteQuery(Sender: TObject);
begin
     If RxQuery_Tavla.Params.FindParam('PyehusYear') <> Nil then
        RxQuery_Tavla.ParamByName('PyehusYear').AsInteger :=CurKabalaYear;
end;

procedure TFrm_Kabala.N9Click(Sender: TObject);
begin
  OpenCrt(fnPritim,'');
end;


procedure TFrm_Kabala.Act_SugFileExecute(Sender: TObject);
Var
  StrH,HStr :String;

begin
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  Strh:=Atmini.readString('HesZikuy','Sug_File','����');
  Repeat
   if Not InputQuery('����� ����� [����/���]','����� �����',Strh) Then
      Exit;
  Until (Strh='����') Or (Strh='���');
  Atmini.WriteString('HesZikuy','Sug_File',Strh);
  Atmini.free;
  TypeOfFile:=Strh;
  If TypeOfFile='���' Then
    Label_CodeLakoach.Caption:='<���>'
  Else
    Label_CodeLakoach.Caption:='<����>';

end;

procedure TFrm_Kabala.RxDBGrid_KabHesDblClick(Sender: TObject);
begin
     if Sender Is TDBGrid Then
     Begin
       if TDBGrid(Sender).SelectedField=DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul')
         Then OpenCrt(fnPritim,DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul').Asstring);
     End;

end;


Procedure TFrm_Kabala.UpdateHazmana;
Begin
    With Dm_kupa.Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Update Shipment');
      Sql.Add('Set HaavaraBankait=1');
      Sql.Add('Where KabalaNumber='+Dm_kupa.AtmRxQuery_ZikuyAvShuraKopa.AsString);
      ExecSql;
    End;
End;


procedure TFrm_Kabala.AtmAdvSearch_ReturnsAfterExecute(Sender: TObject);
Var
Teuda   :   String;
begin
  Fnum:=0;
  Tnum:=0;
  if AtmAdvSearch_Returns.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Kabhes.State=dsBrowse Then
      DM_Kupa.AtmRxQuery_Kabhes.Edit;
//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:=AtmAdvSearch_Maslul.ReturnString;
    Teuda:=AtmAdvSearch_Returns.ReturnString;
    With DM_Kupa.Qry_Temp Do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select Semel_yeshuv,Code_station,Total_young');
      Sql.Add('From MsllStat');
      Sql.Add('Where  Total_Adult='+Teuda);
      Open;
      Dm_kupa.AtmRxQuery_KabHesCodemaslul.OnValidate:=nil;
      Dm_kupa.AtmRxQuery_KabHesSchumHesbonit.OnValidate:=nil;
      Dm_kupa.AtmRxQuery_KabHesKamutHesbonit.OnChange:=Nil;
      Dm_kupa.AtmRxQuery_KabHesToNum.OnChange:=Nil;
      DM_Kupa.AtmRxQuery_KabHes.AfterPost:=Nil;

      Fnum:=FieldByName('Semel_yeshuv').Asinteger;
      TNum:=FieldByName('Code_station').Asinteger;

      DM_Kupa.AtmRxQuery_KabHes.FieldByName('KamutHesbonit').Asinteger:=
        FieldByName('Total_young').Asinteger;
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('FromNum').Asinteger:=
        FieldByName('Semel_yeshuv').Asinteger;
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('ToNum').Asinteger:=
        FieldByName('Code_station').Asinteger;
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('CodeBitzua').Asinteger:=
        DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('KabalaNumber').AsInteger;
      DM_Kupa.AtmRxQuery_KabHes.FieldByName('TotalSchum').AsFloat:=
        DM_Kupa.AtmRxQuery_KabHes.FieldByName('Kamuthesbonit').AsFloat*
        DM_Kupa.AtmRxQuery_KabHes.FieldByName('Schumhesbonit').AsFloat;

      Dm_kupa.AtmRxQuery_KabHesCodemaslul.OnValidate:=
         Dm_kupa.AtmRxQuery_KabHescodemaslulValidate;
      Dm_kupa.AtmRxQuery_KabHesSchumHesbonit.OnValidate:=
         Dm_kupa.AtmRxQuery_KabHesSchumHesbonitValidate;
      Dm_kupa.AtmRxQuery_KabHesKamutHesbonit.OnChange:=
         Dm_kupa.AtmRxQuery_KabHesKamutHesbonitChange;
      Dm_kupa.AtmRxQuery_KabHesToNum.OnChange:=
         Dm_kupa.AtmRxQuery_KabHesToNumChange;
      DM_Kupa.AtmRxQuery_KabHes.AfterPost:=
         DM_Kupa.AtmRxQuery_KabHesAfterPost;

    End; // With Qry_Temp
  End ; //AtmAdvSearch_Returns.Success
end;

procedure TFrm_Kabala.MisparHavaraEnter(Sender: TObject);
begin
   StatusBar1.SimpleText:=' ����� ������ F2 ��� '
end;

procedure TFrm_Kabala.ChebZikuyBeforeExecuteSearch(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     ChebZikuy.SearchComponent:=AtmAdvSearchenZikuy;
end;

procedure TFrm_Kabala.ChebZikuyEnter(Sender: TObject);
begin
    StatusBar1.SimpleText:='���/� �-� ������ ������ , F2 ����� �����';
end;

procedure TFrm_Kabala.ChebZikuyExit(Sender: TObject);
begin
    StatusBar1.SimpleText:='';
end;

procedure TFrm_Kabala.AtmAdvSearch_RemarksAfterExecute(Sender: TObject);
begin
  if AtmAdvSearch_Remarks.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_ZikuyAv.State=dsBrowse Then
      DM_Kupa.AtmRxQuery_ZikuyAv.Edit;
//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:=AtmAdvSearch_Maslul.ReturnString;
    DM_Kupa.AtmRxQuery_ZikuyAv.FieldByName('Pratim').AsString:=AtmAdvSearch_Remarks.ReturnString;
  End;

end;

procedure TFrm_Kabala.PratimEnter(Sender: TObject);
begin
  StatusBar1.SimpleText:='F6 - ���� �������';

end;

procedure TFrm_Kabala.AtmDbHEdit3Enter(Sender: TObject);
begin
    StatusBar1.SimpleText:='���/� ���� ���� , F2 ����� �����';
end;

procedure TFrm_Kabala.PratimExit(Sender: TObject);
begin
  StatusBar1.SimpleText:='';
end;

procedure TFrm_Kabala.Act_egulExecute(Sender: TObject);
Var
  StrH,HStr :String;
begin
  Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Atmcfg.ini');
  Strh:=Atmini.readString('HesZikuy','Sug_Egul','�');
  Repeat
   if Not InputQuery('����� ���� [�/�]','����� ���� �������',Strh) Then
      Exit;
  Until (Strh='�') Or (Strh='�');
  Egul:=Strh;
  Atmini.WriteString('HesZikuy','Sug_Egul',Strh);
  Atmini.free;

end;

procedure TFrm_Kabala.AtmAdvSearch_MaslulAfterExecute(Sender: TObject);
begin
  if AtmAdvSearch_Maslul.Success Then
  Begin
    if DM_Kupa.AtmRxQuery_Kabhes.State=dsBrowse Then
      DM_Kupa.AtmRxQuery_Kabhes.Edit;
//  DM_Kupa.AtmRxQuery_Kabhes.FieldByName('PratimHesbonit').AsString:=AtmAdvSearch_Maslul.ReturnString;
    DM_Kupa.AtmRxQuery_Kabhes.FieldByName('CodeMaslul').AsString:=AtmAdvSearch_Maslul.ReturnString;
  End;

end;

procedure TFrm_Kabala.AtmAdvSearch_MaslulBeforeExecute(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     AtmAdvSearch_Maslul.Sql.Clear;
     AtmAdvSearch_Maslul.Sql.Add('Select Code_maslul,Name,Motza');
     AtmAdvSearch_Maslul.Sql.Add('From Maslul');
     If Snif<>-1 Then
     Begin
       AtmAdvSearch_Maslul.Sql.Add('Where sug_rehev1=1');
       AtmAdvSearch_Maslul.Sql.Add(' And code_pizul=' + IntToStr(Snif) );
     End;

end;

procedure TFrm_Kabala.AtmAdvSearch_ReturnsBeforeExecute(Sender: TObject;
  var ContinueExecute: Boolean);
begin
     AtmAdvSearch_Returns.Sql.Clear;
     AtmAdvSearch_Returns.Sql.Add('Select Total_Adult,Semel_yeshuv,Total_young,');
     AtmAdvSearch_Returns.Sql.Add('Code_station,Time_Arival_to_station');
     AtmAdvSearch_Returns.Sql.Add('From MsllStat');
     AtmAdvSearch_Returns.Sql.Add('Where Position_In_maslul='+ IntToStr(Snif) );
     AtmAdvSearch_Returns.Sql.Add(' And code_Maslul=' +
       DM_Kupa.AtmRxQuery_KabHes.FieldByName('CodeMaslul').Asstring);
     AtmAdvSearch_Returns.Sql.Add(' And code_station>Semel_yeshuv');

end;

end.


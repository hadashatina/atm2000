unit F_PrintKabala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, StdCtrls;

type
  TFrm_PrintKabala = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRDBText_MisHafkada: TQRDBText;
    QRLabel_Makor: TQRLabel;
    QRBand2: TQRBand;
    QRDBText9: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel_AddressLak: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText7: TQRDBText;
    QRBand4: TQRBand;
    QRBand3: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText_KodBank: TQRDBText;
    QRDBText_SnifNumber: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape11: TQRShape;
    QRShape1: TQRShape;
    QRBand5: TQRBand;
    QRDBText8: TQRDBText;
    QRLabel3: TQRLabel;
    QRBand6: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRShape13: TQRShape;
    QRShape15: TQRShape;
    QRShape6: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape14: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape12: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRDBText2: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText14: TQRDBText;
    QRMemo_CompanyDetails: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel22: TQRLabel;
    QRDBText15: TQRDBText;
    QRSubDetail1: TQRSubDetail;
    GroupHeaderBand1: TQRBand;
    QRShape36: TQRShape;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    ChildBand1: TQRChildBand;
    QRDBText17: TQRDBText;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRSubDetail2: TQRSubDetail;
    ChildBand2: TQRChildBand;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    GroupHeaderBand2: TQRBand;
    QRShape34: TQRShape;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel46: TQRLabel;
    QRShape42: TQRShape;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRDBText25: TQRDBText;
    QRShape43: TQRShape;
    QRShape41: TQRShape;
    QRShape35: TQRShape;
    QRLabel30: TQRLabel;
    QRShape40: TQRShape;
    QRLabel33: TQRLabel;
    QRDBText16: TQRDBText;
    QRShape50: TQRShape;
    QRDBText26: TQRDBText;
    QRShape51: TQRShape;
    QRDBText27: TQRDBText;
    QRShape52: TQRShape;
    procedure FormCreate(Sender: TObject);
    procedure QRLabel5Print(sender: TObject; var Value: String);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText_CheckNumberPrint(sender: TObject; var Value: String);
    procedure QRLabel8Print(sender: TObject; var Value: String);
    procedure QuickRep1AfterPrint(Sender: TObject);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure QRMemo_CompanyDetailsPrint(sender: TObject;
      var Value: String);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRLabel21Print(sender: TObject; var Value: String);
    procedure QRDBText17Print(sender: TObject; var Value: String);
    procedure QRDBText18Print(sender: TObject; var Value: String);
    procedure QRDBText25Print(sender: TObject; var Value: String);
    procedure QRExpr1Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    CompayName:String;
  public
    { Public declarations }
  end;

var
  Frm_PrintKabala: TFrm_PrintKabala;

implementation

uses DMKupa, AtmRutin, AtmConst;

{$R *.DFM}

procedure TFrm_PrintKabala.FormCreate(Sender: TObject);
begin
{  DM_Kupa.Qry_Mname.Open;
  CompayName:=DM_Kupa.Qry_Mname.FieldByName('CompanyName').AsString;
  CompayName:=DoEncryption(CompayName,Mname_Key,MnameDecryptKey,False);
  DM_Kupa.Qry_Mname.Close;
}
end;

procedure TFrm_PrintKabala.QRLabel5Print(sender: TObject;
  var Value: String);
begin
  Value:=CompayName;
end;

procedure TFrm_PrintKabala.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
Var
Bait   :  Shortstring;
begin
  With Dm_Kupa.Qry_Temp Do
  Begin
    Close;
    Sql.Clear;
    If Kabala_type=10 Then
    Begin
      Sql.Add('Select Ktovet_1,Ir_1,Mikud_1 From Nehag');
      Sql.Add('Where Kod_Nehag = '+DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
    End
    Else
    Begin
      Sql.Add('Select Ktovet_1,Mis_Bait1,Ir_1,Mikud_1 From Lakoach');
      Sql.Add('Where Kod_Lakoach = '+DM_Kupa.AtmRxQuery_Kabala.FieldByName('CodeLakoach').AsString);
    End;
    Open;
    If Kabala_type<>10 Then Bait:=FieldByName('Mis_Bait1').AsString
    Else Bait:='';

    QRLabel_AddressLak.Caption:=FieldByName('Ktovet_1').AsString+' '+
                                Bait+' '+
                                FieldByName('Ir_1').AsString+','+
                                FieldByName('Mikud_1').AsString+' ';
    Close;
  End;
end;

procedure TFrm_PrintKabala.QRDBText6Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  If Kodmatbea{Kabala_type}=0 Then
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
  Else
     Value:=FormatFloat('"$"0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat)
end;

procedure TFrm_PrintKabala.QRDBText_CheckNumberPrint(sender: TObject;
  var Value: String);
begin
  if TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsInteger=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QRLabel8Print(sender: TObject;
  var Value: String);
begin
  if CompareText('����',QRLabel_Makor.Caption)=0 Then
    Value:='';
end;

procedure TFrm_PrintKabala.QuickRep1AfterPrint(Sender: TObject);
begin
  //DM_Kupa.Qry_Mname.Close;
end;

procedure TFrm_PrintKabala.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRDBText14Print(sender: TObject;
  var Value: String);
begin
  Value:=DoEncryption(TQRDBText(Sender).DataSet.FieldByName(TQRDBText(Sender).DataField).AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QRMemo_CompanyDetailsPrint(sender: TObject;
  var Value: String);
begin
  Value :=DoEncryption(DM_Kupa.Qry_Mname.FieldByName('CompanyDetail').AsString,Mname_Key,MnameDecryptKey,False);
end;

procedure TFrm_PrintKabala.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
//  DM_Kupa.Qry_Mname.Open;
end;

procedure TFrm_PrintKabala.QRLabel21Print(sender: TObject;
  var Value: String);
Var
Matbea   :   Integer;
begin
  If Kodmatbea{Kabala_type}=0 Then Matbea:=0 Else Matbea:=2;
  Value:=NumberToHebWords(DM_Kupa.AtmRxQuery_Kabala.FieldByName('TotalSumKabla').AsFloat,True,True,Matbea);
end;

procedure TFrm_PrintKabala.QRDBText17Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText18Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRDBText25Print(sender: TObject;
  var Value: String);
begin
     Value:=FormatFloat('0.00',TQrDbText(Sender).DataSet.FieldByName(TQrDbText(Sender).DataField).AsFloat);
end;

procedure TFrm_PrintKabala.QRExpr1Print(sender: TObject;
  var Value: String);
begin
      Value:=FormatFloat('0.00',StrToFloat(Value));
end;

end.
